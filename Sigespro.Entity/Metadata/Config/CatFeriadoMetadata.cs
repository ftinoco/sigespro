﻿using Sigespro.Entity.Resource;
using System;
using System.ComponentModel.DataAnnotations;

namespace Sigespro.Entity.Metadata.Config
{
    public class CatFeriadosMetadata
    {
        public int IdFeriado { get; set; } 

        [Display(Name = "MotivoFeriado", ResourceType = typeof(Labels))]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string MotivoFeriado { get; set; }

        [Display(Name = "FechaFeriado", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public DateTime FechaFeriado { get; set; }
         
        public int IdUsuarioIns { get; set; }
         
        public DateTime FechaIns { get; set; } 
    }
}
