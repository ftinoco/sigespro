﻿using Sigespro.Web.Mvc.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sigespro.Web.Mvc.Models.Configuration
{
    public class ProgramViewModel
    {
        public int IdPrograma { get; set; }
        
        [Display(Name = "ProgramName", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string NombrePrograma { get; set; }

        [Display(Name = "Description", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string Descripcion { get; set; }

        [Display(Name = "Active", ResourceType = typeof(Labels))]
        public bool Activo { get; set; }

        [Display(Name = "UserInsert", ResourceType = typeof(Labels))]
        public int IdUsuarioIns { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "DateInsert", ResourceType = typeof(Labels))]
        public DateTime FechaIns { get; set; }

        [Display(Name = "UserEdit", ResourceType = typeof(Labels))]
        public int? IdUsuarioUpd { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "DateEdit", ResourceType = typeof(Labels))]
        public DateTime? FechaUpd { get; set; }

        public bool Selected { get; set; }
    }

    public class RptProjectPrograms
    {
        public int IdPrograma { get; set; }
        
        public string NombrePrograma { get; set; }
        
        public string Descripcion { get; set; }
        
        public bool Activo { get; set; }
    }


}