﻿using Sigespro.DAL.Interface;
using Sigespro.Entity;
using Common.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Common.Helpers;

namespace Sigespro.DAL.Implementations
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private SigesproEntities context;

        private readonly IDbSet<T> dbset;

        public Repository(DatabaseFactory databaseFactory)
        {
            DatabaseFactory = databaseFactory;
            dbset = DataContext.Set<T>();
        }

        public IDatabaseFactory DatabaseFactory
        {
            get;
            private set;
        }

        public SigesproEntities DataContext
        {
            get { return context ?? (context = DatabaseFactory.Get()); }
        }

        public virtual void Add(T entity)
        {
            try
            {
                dbset.Add(entity);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                throw ex;
            }
        }

        public virtual long Count()
        {
            return dbset.LongCount();
        }

        public virtual long Count(Expression<Func<T, bool>> where)
        {
            return dbset.Where(where).LongCount();
        }

        public virtual void Delete(Expression<Func<T, bool>> where)
        {
            IEnumerable<T> objects = dbset.Where<T>(where).AsEnumerable();
            foreach (T item in objects)
                dbset.Remove(item);
            //context.Entry(item).State = EntityState.Deleted;
        }

        public virtual void Delete(T entity)
        {
            dbset.Remove(entity);
        }

        public virtual bool Any(Expression<Func<T, bool>> where)
        {
            return dbset.Any(where);
        }

        public virtual T Get(Expression<Func<T, bool>> where)
        {
            return dbset.AsNoTracking().Where(where).FirstOrDefault<T>();
        }

        public virtual Task<T> GetAsync(Expression<Func<T, bool>> where)
        {
            return dbset.AsNoTracking().Where(where).FirstOrDefaultAsync<T>();
        }

        public virtual IEnumerable<T> GetAll()
        {
            return dbset.ToList();
        }

        public virtual T GetById(object Id, object Id2 = null)
        {
            if (Id2 == null)
                return dbset.Find(Id);
            else
                return dbset.Find(Id, Id2);
        }

        public virtual IEnumerable<T> GetMany(Expression<Func<T, bool>> where)
        { 
            return dbset.Where(where).ToList();
        }

        public virtual IEnumerable<T> GetMany(Expression<Func<T, bool>> where, bool lazyLoadingEnabled)
        {
            context.Configuration.LazyLoadingEnabled = lazyLoadingEnabled;

            if (lazyLoadingEnabled)
                return dbset.Where(where).ToList();
            else
                return dbset.AsNoTracking().Where(where).ToList();
        }

        public virtual IEnumerable<T> GetMany(Expression<Func<T, bool>> where, string orderBy, SortingOrder sortOrder)
        {
            if (sortOrder == SortingOrder.ASC)
                return dbset.Where(where).OrderBy(orderBy).ToList();
            else
                return dbset.Where(where).OrderByDescending(orderBy).ToList();
        }

        public virtual IEnumerable<T> GetManyOnDemand(Expression<Func<T, bool>> where, string orderBy, int pageIni, int rowsPerPage, SortingOrder sortOrder)
        {
            context.Configuration.LazyLoadingEnabled = false;

            if (sortOrder == SortingOrder.ASC)
                return dbset.AsNoTracking().Where(where).OrderBy(orderBy).Skip(pageIni * rowsPerPage).Take(rowsPerPage).ToList();
            else
                return dbset.AsNoTracking().Where(where).OrderByDescending(orderBy).Skip(pageIni * rowsPerPage).Take(rowsPerPage).ToList();
        }

        public virtual void Update(T entity)
        {
            dbset.Attach(entity);
            context.Entry(entity).State = EntityState.Modified;
        }
    }
}
