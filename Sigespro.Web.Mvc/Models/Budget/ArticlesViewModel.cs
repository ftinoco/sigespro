﻿using Sigespro.Web.Mvc.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sigespro.Web.Mvc.Models.Budget
{
    public class ArticlesViewModel
    {
        public string IdArticulo { get; set; }

        [Display(Name = "Entry", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string IdRubro { get; set; }

        public string Rubro { get; set; }

        [Display(Name = "MeasureUnit", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public int IdUnidadMedida { get; set; }

        public string UnidadMedida { get; set; }

        [Display(Name = "Currency", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public int IdMoneda { get; set; }

        public string Moneda { get; set; }

        public string Simbolo { get; set; }

        [Display(Name = "Price", ResourceType = typeof(Labels))]
        [DisplayFormat(DataFormatString = "{0:#,##0.0000}", ApplyFormatInEditMode = true)]
        //[Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        //[RegularExpression(" ^[0-9]{2}[:][0-9]{2}$ ", ErrorMessageResourceName = "OnlyNumber", ErrorMessageResourceType = typeof(Messages))]
        //[RegularExpression("(\\d{1,3},?(\\d{3},?)*\\d{3}(.\\d{0,3})?|\\d{1,3}(.\\d{2})?)", ErrorMessageResourceName = "OnlyNumber", ErrorMessageResourceType = typeof(Messages))]
        public decimal Precio { get; set; }

        [Display(Name = "Description", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string Descripcion { get; set; }

        [Display(Name = "Active", ResourceType = typeof(Labels))]
        public bool Activo { get; set; }

        public int IdUsuarioIns { get; set; }

        public DateTime FechaIns { get; set; }

        public int? IdUsuarioUpd { get; set; }

        public DateTime? FechaUpd { get; set; }
    }
}