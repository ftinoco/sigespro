﻿using Common.Helpers;
using Common.Log;
using Newtonsoft.Json;
using Sigespro.Entity;
using Sigespro.Service.Admin;
using Sigespro.Web.Mvc.Resources;
using Sigespro.Web.Mvc.Utilities.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace Sigespro.Web.Mvc.Controllers.Administration
{
    [RoutePrefix("Administration/Logger")]
    public class LoggerController : Controller
    {
        private readonly IUserSession _userSession;

        public LoggerService Service { get; set; }

        public LoggerController()
        {
            Service = new LoggerService();
            _userSession = new UserSession();
        }

        [Route]
        public ActionResult Index(QueryOptions<IEnumerable<vwLogger>> queryOpt)
        {
            string message = string.Empty;
            IEnumerable<vwLogger> model = new List<vwLogger>();

            if (string.IsNullOrEmpty(queryOpt.SortField))
                queryOpt.SortOrder = SortingOrder.DESC;

            string strSerialize = JsonConvert.SerializeObject(queryOpt);

            QueryOptions<IEnumerable<vwLogger>> filter = JsonConvert.DeserializeObject<QueryOptions<IEnumerable<vwLogger>>>(strSerialize);
            Service.GetLogsForList(ref filter);

            if (filter.ResultData.MessageType == MessageType.ERROR)
            {
                message = filter.ResultData.Message;
                Logger.Log(filter.ResultData.MessageType, filter.ResultData.DetailException, _userSession.Username,
                           MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }
            else
            {
                strSerialize = JsonConvert.SerializeObject(filter);

                queryOpt = JsonConvert.DeserializeObject<QueryOptions<IEnumerable<vwLogger>>>(strSerialize);

                model = queryOpt.ResultData.Data;
            }

            if (!string.IsNullOrEmpty(message))
                ModelState.AddModelError(Messages.AlertDanger, message);

            ViewBag.Filter = true;
            ViewBag.QueryOptions = queryOpt;

            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }
    }
}