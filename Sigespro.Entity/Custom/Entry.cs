﻿using Sigespro.Entity;
using System;

namespace Sigespro.Entity.Custom
{
    public class Entry
    {
        public string IdRubro { get; set; }
        
        public string Nombre { get; set; }

        public DetCatalogo TipoRubro { get; set; }

        public CatRubros RubroParent { get; set; }

        public bool Activo { get; set; }

        public int IdUsuarioIns { get; set; }

        public DateTime FechaIns { get; set; }

        public int? IdUsuarioUpd { get; set; }

        public DateTime? FechaUpd { get; set; }

    }
}
