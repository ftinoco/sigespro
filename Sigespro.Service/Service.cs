﻿using Sigespro.DAL.Implementations;
using Sigespro.Entity;
using Sigespro.Service.Admin;
using System;

namespace Sigespro.Service
{
    public interface IService: IDisposable
    {

    }

    public abstract class Service: IService
    {
        protected UnitOfWork _unitOfWork;

        public Service()
        {
            _unitOfWork = new UnitOfWork(); 
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
        }
    }
}
