﻿using Sigespro.Entity.Resource;
using System.ComponentModel.DataAnnotations;

namespace Sigespro.Entity.Metadata.Config
{
    public class CatArticulosMetadata
    {
        [Required( ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string IdRubro { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public int IdUnidadMedida { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public int IdMoneda { get; set; }

        [Display(Name = "Descripcion", ResourceType = typeof(Labels))]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string Descripcion { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public decimal Precio { get; set; } 
    }
}
