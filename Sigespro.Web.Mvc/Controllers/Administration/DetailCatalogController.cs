﻿using Common.Helpers;
using Common.Log;
using Newtonsoft.Json;
using Sigespro.Entity;
using Sigespro.Service.Admin;
using Sigespro.Web.Mvc.Models.Administration;
using Sigespro.Web.Mvc.Resources;
using Sigespro.Web.Mvc.Utilities.Attributes.Security;
using Sigespro.Web.Mvc.Utilities.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Sigespro.Web.Mvc.Controllers.Administration
{
    [RoutePrefix("Administration/Catalog/Details")] 
    public class DetailCatalogController : Controller
    {
        private readonly IUserSession _userSession;

        public DetailCatalogService Service { get; set; }

        //HttpClientApi httpClientApi = new HttpClientApi(ConfigurationManager.AppSettings["SigesproApi"]);

        public DetailCatalogController()
        {
            Service = new DetailCatalogService();
            _userSession = new UserSession();
        }

        [Route]
        public ActionResult Index(QueryOptions<IEnumerable<vwDetCatalogo>> queryOpt)
        {
            string message = string.Empty;
            IEnumerable<vwDetCatalogo> model = new List<vwDetCatalogo>();

            if (string.IsNullOrEmpty(queryOpt.SortField))
                queryOpt.SortOrder = SortingOrder.DESC;

            //HttpResponseMessage result = await httpClientApi.PostAsync("Api/DetailCatalog/List", queryOpt);
            string strSerialize = JsonConvert.SerializeObject(queryOpt);

            QueryOptions<IEnumerable<vwDetCatalogo>> filter = JsonConvert.DeserializeObject<QueryOptions<IEnumerable<vwDetCatalogo>>>(strSerialize);
            Service.GetDetailCatalogsForList(ref filter);

            if (filter.ResultData.MessageType == MessageType.ERROR)
            {
                message = filter.ResultData.Message;
                Logger.Log(filter.ResultData.MessageType, filter.ResultData.DetailException, _userSession.Username,
                           MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }
            else
            {/*
                strSerialize = JsonConvert.SerializeObject(filter);

                queryOpt = JsonConvert.DeserializeObject<QueryOptions<IEnumerable<DetCatalogViewModel>>>(strSerialize);
                */
                queryOpt = filter;
                model = filter.ResultData.Data;// queryOpt.ResultData.Data;
            }

            if (!string.IsNullOrEmpty(message))
                ModelState.AddModelError(Messages.AlertDanger, message);

            ViewBag.Filter = true;
            ViewBag.QueryOptions = queryOpt;

            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [Route("Add")]
        public ActionResult AddDetailCatalog()
        {
            return AddEditCatalog();
        }

        [Route("Edit/{detailCatalogId}/{catalogId}")]
        public ActionResult EditDetailCatalog(int detailCatalogId, int catalogId)
        {
            return AddEditCatalog(detailCatalogId, catalogId);
        }

        [NonAction]
        private ActionResult AddEditCatalog(int detailCatalogId = 0, int catalogId = 0)
        {
            string message = string.Empty;

            DetCatalogViewModel model = new DetCatalogViewModel();

            try
            {
                CatalogService catalogService = new CatalogService();
                var resultCatalog = catalogService.GetCatalogs(); // httpClientApi.GetAsyncObject<List<CatalogViewModel>>("Api/Catalog/");

                List<SelectListItem> selectListItem = new List<SelectListItem>();

                selectListItem.Add(new SelectListItem { Value = "0", Text = Labels.Select });

                if (resultCatalog.MessageType == MessageType.SUCCESS)
                {
                    resultCatalog.Data.ToList().ForEach(c => selectListItem.Add(
                        new SelectListItem
                        {
                            Value = c.IdCatalogo.ToString(),
                            Text = c.Descripcion
                        }
                    ));

                    if (detailCatalogId > 0 && catalogId > 0)
                    {
                        var resultModel = Service.GetDetailCatalogById(detailCatalogId, catalogId);

                        if (resultModel.MessageType == MessageType.ERROR)
                        {
                            message = resultModel.Message;
                            Logger.Log(resultModel.MessageType, resultModel.DetailException, _userSession.Username,
                                       MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                        }
                        else
                        {
                            // string strSerialize = JsonConvert.SerializeObject(resultModel.Data);

                            model = new DetCatalogViewModel
                            {
                                Activo = resultModel.Data.Activo,
                                Descripcion = resultModel.Data.Descripcion,
                                FechaIns = resultModel.Data.FechaIns,
                                FechaUpd = resultModel.Data.FechaUpd,
                                IdDetCatalogo = resultModel.Data.IdDetCatalogo,
                                IdMstCatalogo = resultModel.Data.IdMstCatalogo,
                                IdUsuarioIns = resultModel.Data.IdUsuarioIns,
                                IdUsuarioUpd = resultModel.Data.IdUsuarioUpd,
                                Valor = resultModel.Data.Valor
                            }; // JsonConvert.DeserializeObject<DetCatalogViewModel>(strSerialize); //httpClientApi.GetAsyncObject<DetCatalogViewModel>("Api/DetailCatalog/" + detailCatalogId + "/" + catalogId);
                        }
                    }
                }
                else
                {
                    message = resultCatalog.Message;
                    Logger.Log(resultCatalog.MessageType, resultCatalog.DetailException, _userSession.Username,
                               MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }

                ViewBag.SelectList = new SelectList(selectListItem, "Value", "Text");
            }
            catch (Exception ex)
            {
                message = Messages.ErrorApi;
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                           MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            if (!string.IsNullOrEmpty(message))
                ModelState.AddModelError(Messages.AlertDanger, message);

            if (detailCatalogId == 0 && catalogId == 0)
            {
                ViewBag.IsEdit = false;
                ViewBag.Action = "SaveDetailCatalog";
                ViewBag.Title = "Nuevo Detalle Catálogo";

                return View("AddDetailCatalog");
            }
            else
            {
                ViewBag.IsEdit = true;
                ViewBag.Action = "UpdateDetailCatalog";
                ViewBag.Title = "Editar Detalle Catálogo";

                return View("AddDetailCatalog", model);
            }
        }

        [AjaxOnly]
        [HttpPost]
        [Route("Save")]
        [AllowAnonymous]
        public ActionResult SaveDetailCatalog(DetCatalogViewModel model)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                model.IdUsuarioIns = _userSession.UserId;
                string strSerialize = JsonConvert.SerializeObject(model);

                var result = Service.InsertDetailCatalog(JsonConvert.DeserializeObject<DetCatalogo>(strSerialize)); 

                if (result.MessageType != MessageType.SUCCESS)
                { 
                    message = result.Message;
                    status = result.MessageType.ToString();
                    Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                    message = string.Format(Messages.SaveSuccess, "detalle catálogo");
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                             MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message, action = Url.Action("Index", "DetailCatalog") },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [AjaxOnly]
        [HttpPost]
        [AllowAnonymous]
        [Route("Update")]
        public ActionResult UpdateDetailCatalog(DetCatalogViewModel model)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                model.IdUsuarioUpd = _userSession.UserId;
                string strSerialize = JsonConvert.SerializeObject(model);

                var result = Service.UpdateDetailCatalog(JsonConvert.DeserializeObject<DetCatalogo>(strSerialize));

                if (result.MessageType != MessageType.SUCCESS)
                {
                    message = result.Message;
                    status = result.MessageType.ToString();
                    Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                    message = string.Format(Messages.UpdateSuccess, "detalle catálogo");
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                             MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message, action = Url.Action("Index", "DetailCatalog") },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }
}
