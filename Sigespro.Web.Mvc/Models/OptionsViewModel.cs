﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sigespro.Web.Mvc.Models
{
    public class OptionsViewModel
    {
        public int ActionId { get; set; }
        public string Url { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public int RoleId { get; set; }
        public int UserId { get; set; }
        public int SystemId { get; set; }
        public int OptionId { get; set; }
        public string OptionName { get; set; }
        public string UserName { get; set; }
    }
}