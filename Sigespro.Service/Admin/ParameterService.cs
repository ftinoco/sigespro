﻿using Common.Extensions;
using Common.Helpers;
using Sigespro.DAL.Implementations;
using Sigespro.Entity;
using Sigespro.Entity.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Common.Helpers.Utilities;

namespace Sigespro.Service.Admin
{
    public class ParameterService : Service
    {
        private Repository<CatParametros> Repository { get; set; }

        public ParameterService() : base()
        {
            Repository = _unitOfWork.GetRepository<CatParametros>();
        }

        public Result<IEnumerable<CatParametros>> GetParameters()
        {
            Result<IEnumerable<CatParametros>> result = new Result<IEnumerable<CatParametros>>();

            try
            {
                result.Data = Repository.GetAll().OrderBy(d => d.NombreParametro);

                result.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<CatParametros> GetParameterByName(string parameterName)
        {
            Result<CatParametros> result = new Result<CatParametros>();

            try
            {
                result.Data = Repository.Get(d => d.NombreParametro.ToLower().Trim() == parameterName.ToLower().Trim());

                result.Message = Messages.SuccessGetResult;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<CatParametros> GetParameterById(int parameterId)
        {
            Result<CatParametros> result = new Result<CatParametros>();

            try
            {
                result.Data = Repository.GetById(parameterId);
                result.Message = Messages.SuccessGetResult;
                result.MessageType = MessageType.INFO;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public void GetParametersForList(ref QueryOptions<IEnumerable<CatParametros>> queryOpt)
        {
            Result<IEnumerable<CatParametros>> result = new Result<IEnumerable<CatParametros>>();

            try
            {
                string orderBy = "NombreParametro";

                var predicate = PredicateBuilder.True<CatParametros>();

                var filter = queryOpt.Filters.FirstOrDefault(f => f.Name.Equals("Search"));

                if (!IsNull(filter) && !string.IsNullOrEmpty(filter.Value))
                {
                    int intFilter; 

                    predicate = predicate.And(p => p.NombreParametro.ToLower().Trim().Contains(filter.Value.ToLower().Trim()) || 
                                                   p.ValorParametro.ToLower().Trim().Contains(filter.Value.ToLower().Trim()) ||
                                                   p.DescParametro.ToLower().Trim().Contains(filter.Value.ToLower().Trim()));

                    if (int.TryParse(filter.Value, out intFilter))
                    {
                        predicate = predicate.Or(p => p.IdPerametro == intFilter);
                    }

                    queryOpt.TotalRecords = _unitOfWork.GetRepository<CatParametros>().Count(predicate);
                }
                else
                    queryOpt.TotalRecords = _unitOfWork.GetRepository<CatParametros>().Count();

                if (!string.IsNullOrEmpty(queryOpt.SortField))
                    orderBy = queryOpt.SortField;

                result.Data = _unitOfWork.GetRepository<CatParametros>().GetManyOnDemand(predicate, orderBy, queryOpt.CurrentPageIndex, queryOpt.RowsPerPage, queryOpt.SortOrder);

                result.Message = Messages.SuccessGetResultSet;

                result.MessageType = MessageType.INFO;
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorGetResultSet, "registro de parámetro");
                result.MessageType = MessageType.ERROR;
                result.DetailException = ex;
            }

            queryOpt.ResultData = result;
        }

        public Result<IEnumerable<CatParametros>> GetParameters(string filter)
        {
            string filterToLower = filter.Trim().ToLower();

            Result<IEnumerable<CatParametros>> result = new Result<IEnumerable<CatParametros>>();

            try
            {
                result.Data = Repository.GetMany(p => p.NombreParametro.Trim().ToLower().Contains(filterToLower) ||
                                                      p.DescParametro.Trim().ToLower().Contains(filterToLower) ||
                                                      p.ValorParametro.Trim().ToLower().Contains(filterToLower))
                                        .OrderBy(d => d.NombreParametro);

                result.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase InsertParameter(CatParametros parameter)
        {
            ResultBase result = new ResultBase();

            try
            {
                if (parameter.IdPerametro == 0)
                { 
                    if (string.IsNullOrEmpty(parameter.NombreParametro) || string.IsNullOrEmpty(parameter.ValorParametro))
                    {
                        result.Message = Messages.RequiredData;
                        result.MessageType = MessageType.WARNING;
                        result.DetailException = new Exception(result.Message);
                    }
                    else
                    { 
                        Repository.Add(parameter);
                         
                        _unitOfWork.Commit();
                    }
                }

                result.Message = Messages.SuccessSaveData;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorSaveData, "programa");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase UpdateParameter(CatParametros parameter)
        {
            ResultBase result = new ResultBase();

            try
            {
                var temp = Repository.Get(p => p.IdPerametro == parameter.IdPerametro);

                if (IsNull(temp))
                {
                    result.Message = Messages.NotExist;
                    result.MessageType = MessageType.WARNING;
                    result.DetailException = new Exception(result.Message);
                }
                else
                {
                    if (string.IsNullOrEmpty(parameter.NombreParametro) || string.IsNullOrEmpty(parameter.ValorParametro))
                    {
                        result.Message = Messages.RequiredData;
                        result.MessageType = MessageType.INFO;
                        result.DetailException = new Exception(result.Message);
                    }
                    else
                    { 
                        Repository.Update(parameter);

                        _unitOfWork.Commit();
                    }
                }

                result.Message = Messages.SuccessUpdateData;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorUpdateData, "parámetro");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

    }
}
