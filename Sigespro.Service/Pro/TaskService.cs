﻿using Common.Helpers;
using Sigespro.DAL.Implementations;
using Sigespro.Entity;
using Sigespro.Entity.Custom;
using Sigespro.Entity.Resource;
using Sigespro.Service.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigespro.Service.Pro
{
    public class TaskService : Service
    {
        private List<MstTareas> Tasks = new List<MstTareas>();

        private Repository<MstTareas> Repository { get; set; }

        public TaskService() : base()
        {
            Repository = _unitOfWork.GetRepository<MstTareas>();
        }

        /// <summary>
        /// Obtiene un registro de tarea por identificador de proyecto, entregable y tarea
        /// </summary>
        /// <param name="taskID">Identificador de tarea</param>
        /// <param name="deliverableID">Identificador de entregable</param>
        /// <param name="projectID">Identificador de proyecto</param>
        /// <returns>Objeto de tipo Result. 
        ///    Message: Mensaje del proceso
        ///    DetailException: Excepción si la hubo
        ///    MessageType: Tipo de mensaje (Enumerable MessageType)
        ///    Data: Objeto de tipo «MstTareas»</returns>
        public Result<MstTareas> GetTaskById(int taskID, int deliverableID, int projectID)
        {
            Result<MstTareas> result = new Result<MstTareas>();

            try
            {
                result.MessageType = MessageType.INFO;
                result.Data = Repository.GetMany(t => t.IdTarea == taskID && t.IdEntregable == deliverableID && t.IdProyecto == projectID).SingleOrDefault();
                result.Message = Messages.SuccessGetResult;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        /// <summary>
        /// Obtiene el listado de tareas por proyecto
        /// </summary>
        /// <param name="projectID">Identificador de proyecto</param>
        /// <returns>Objeto de tipo Result. 
        ///    Message: Mensaje del proceso
        ///    DetailException: Excepción si la hubo
        ///    MessageType: Tipo de mensaje (Enumerable MessageType)
        ///    Data: Lista tipada de objetos «MstTareas»</returns>
        public Result<List<MstTareas>> GetTaskByProjectId(int projectID)
        {
            Result<List<MstTareas>> result = new Result<List<MstTareas>>();

            try
            {
                result.MessageType = MessageType.INFO;
                result.Data = Repository.GetMany(t => t.IdProyecto == projectID, false).ToList();
                result.Message = Messages.SuccessGetResult;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        /// <summary>
        ///  Obtiene el listado (Simplificado) de tareas por proyecto
        /// </summary>
        /// <param name="projectID">Identificador de proyecto</param>
        /// <returns>Objeto de tipo Result. 
        ///    Message: Mensaje del proceso
        ///    DetailException: Excepción si la hubo
        ///    MessageType: Tipo de mensaje (Enumerable MessageType)
        ///    Data: Lista tipada de objetos «vwTareasPorProyecto»</returns>
        public Result<List<vwTareasPorProyecto>> GetVwTaskByProject(int projectID)
        {
            Result<List<vwTareasPorProyecto>> result = new Result<List<vwTareasPorProyecto>>();

            try
            {
                result.MessageType = MessageType.INFO;

                var deliverableRep = _unitOfWork.GetRepository<vwTareasPorProyecto>();

                result.Data = deliverableRep.GetMany(p => p.IdProyecto == projectID).ToList();

                result.Message = Messages.SuccessGetResult;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<List<vwTareasRetrasadas>> GetOverdueTaskByProject(int projectID)
        {
            Result<List<vwTareasRetrasadas>> result = new Result<List<vwTareasRetrasadas>>();

            try
            {
                result.MessageType = MessageType.INFO;

                var deliverableRep = _unitOfWork.GetRepository<vwTareasRetrasadas>();

                result.Data = deliverableRep.GetMany(p => p.IdProyecto == projectID).ToList();

                result.Message = Messages.SuccessGetResult;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<List<TimeLine>> GetTimeLineProject(int projectId)
        {
            Result<List<TimeLine>> result = new Result<List<TimeLine>>();
            try
            {
                var binnacleRep = _unitOfWork.GetRepository<BitacoraTareas>();

                List<TimeLine> lst = binnacleRep.GetAll()
                                        .Join(Repository.GetAll(), b => b.IdTarea,
                                                                   t => t.IdTarea,
                                        (b, t) => new { b, t })
                                        .Where(p => p.t.IdProyecto == projectId && p.b.IdEstado == 30 && p.t.TimeLine == true)
                                        .Select(x => new TimeLine
                                        {
                                            Tarea = x.t.Nombre,
                                            FechaEjecucion = x.b.Fecha,
                                            Comentarios = x.b.Comentarios
                                        }).OrderByDescending(x => x.FechaEjecucion).ToList(); 

                if (lst != null)
                {
                    result.MessageType = MessageType.INFO;
                    result.Data = lst;
                    result.Message = Messages.SuccessGetResult;
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }
            return result;
        }

        /// <summary>
        /// Obtiene la cantidad de tareas por proyecto y fecha
        /// </summary>
        /// <param name="projectID">Identificador del proyecto</param>
        /// <param name="date">Fecha a buscar las tareas</param>
        /// <returns>Objeto de tipo Result. 
        ///    Message: Mensaje del proceso
        ///    DetailException: Excepción si la hubo
        ///    MessageType: Tipo de mensaje (Enumerable MessageType)
        ///    Data: Diccionario con llave string y valor entero
        ///          Posible valores: Task «Cantidad de tareas»
        ///                           Finished «Cantidad de tareas ejecutada»
        ///                           Canceled «Cantidad de tarea canceladas»
        ///                           InAction «Cantidad de tareas en ejecución»</returns>
        public Result<Dictionary<string, int>> GetCountTaskByProjectAndDate(int projectID, DateTime date)
        {
            Result<Dictionary<string, int>> result = new Result<Dictionary<string, int>>();

            try
            {
                Dictionary<string, int> dic = new Dictionary<string, int>();
                result.MessageType = MessageType.INFO;

                var viewRepository = _unitOfWork.GetRepository<vwBitacoraTareas>();

                var tasks = viewRepository.GetMany(p => p.IdProyecto == projectID).Where(x => x.IdEstado.HasValue).ToList();

                DetailCatalogService catalogService = new DetailCatalogService();
                var catalogResult = catalogService.GetDetailsByTable("EstadoTareas");

                if (catalogResult.MessageType == MessageType.INFO)
                {
                    int finished = catalogResult.Data.FirstOrDefault(x => x.Valor == "Ejecutada").IdDetCatalogo;
                    int canceledId = catalogResult.Data.FirstOrDefault(x => x.Valor == "Cancelada").IdDetCatalogo;
                    int inActionId = catalogResult.Data.FirstOrDefault(x => x.Valor == "En Ejecución").IdDetCatalogo;

                    int i = 0, j = 0, k = 0, l = 0;
                    foreach (var item in tasks)
                    {
                        if (tasks.Count(x => x.IdTarea == item.IdTarea) > 1)
                            item.IdEstado = tasks.Where(x => x.IdTarea == item.IdTarea).OrderByDescending(x => x.Fecha).FirstOrDefault().IdEstado;

                        if (item.FechaInicio.Date.CompareTo(date.Date) == 0)
                            i++;

                        if (item.IdEstado == finished && item.Finalizado.Value && item.Fecha.Date.CompareTo(date.Date) == 0)
                            j++;

                        if (item.IdEstado == canceledId && item.Fecha.Date.CompareTo(date.Date) == 0)
                            k++;

                        if (item.IdEstado == inActionId && item.Fecha.Date.CompareTo(date.Date) == 0)
                            l++;
                    }

                    dic.Add("Task", i);
                    dic.Add("Finished", j);
                    dic.Add("Canceled", k);
                    dic.Add("InAction", l);
                }

                result.Data = dic;
                result.Message = Messages.SuccessGetResult;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        /// <summary>
        /// Obtiene la cantidad de tareas retrasadas
        /// </summary>
        /// <param name="projectID">Identificador del proyecto</param>
        /// <returns>Objeto de tipo Result. 
        ///    Message: Mensaje del proceso
        ///    DetailException: Excepción si la hubo
        ///    MessageType: Tipo de mensaje (Enumerable MessageType)
        ///    Data: Lista tipada de objetos «vwTareasPorProyecto»</returns>
        public ResultByValue<int> GetCountOverdueTask(int projectID)
        {
            ResultByValue<int> result = new ResultByValue<int>();

            try
            {
                result.MessageType = MessageType.INFO;

                var viewRepository = _unitOfWork.GetRepository<vwTareasPorProyecto>();

                DetailCatalogService catalogService = new DetailCatalogService();
                var catalogResult = catalogService.GetDetailsByTableAndValue("EstadoTareas", "Ejecutada");

                if (catalogResult.MessageType == MessageType.INFO)
                    result.Data = viewRepository.GetMany(p => p.IdProyecto == projectID && p.FechaFin < DateTime.Now
                                                                && p.IdEstado != catalogResult.Data.IdDetCatalogo).ToList().Count;

                result.Message = Messages.SuccessGetResult;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        /// <summary>
        /// Obtiene el listado de tareas por entregable (Fase)
        /// </summary>
        /// <param name="deliverableID">Identificador del entregable</param>
        /// <returns>Objeto de tipo Result. 
        ///    Message: Mensaje del proceso
        ///    DetailException: Excepción si la hubo
        ///    MessageType: Tipo de mensaje (Enumerable MessageType)
        ///    Data: Lista tipada de objetos «vwTareasPorEntregable»</returns>
        public Result<List<TaskInfo>> GetTasksByDeliverable(int deliverableID)
        {
            Result<List<TaskInfo>> result = new Result<List<TaskInfo>>();

            try
            {
                var deliverableRep = _unitOfWork.GetRepository<MstEntregables>();

                List<TaskInfo> lst = deliverableRep.GetAll()
                                        .Join(Repository.GetAll(), e => e.IdEntregable,
                                                                   t => t.IdEntregable,
                                        (e, t) => new { e, t })
                                        .Where(p => p.e.IdEntregable == deliverableID)
                                        .Select(x => new TaskInfo
                                        {
                                            TaskID = x.t.IdTarea,
                                            TaskName = x.t.Nombre
                                        }).ToList();

                //(p => p.IdEntregable == deliverableID).ToList();
                if (lst != null)
                {
                    result.MessageType = MessageType.INFO;
                    result.Data = lst;
                    result.Message = Messages.SuccessGetResult;
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase InsertTask(MstTareas task)
        {
            ResultBase result = new ResultBase();

            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
            {

                try
                {
                    MstProyectos project = new MstProyectos();

                    var resultValidate = ValidateTaskData(task, ref project);

                    if (resultValidate.MessageType == MessageType.ERROR)
                        result = resultValidate;
                    else
                    {
                        var temp = Repository.Get(t => t.Nombre.ToLower() == task.Nombre.ToLower() && t.IdProyecto == task.IdProyecto &&
                                                       t.IdEntregable == task.IdEntregable);

                        if (temp == null)
                        {
                            DetailCatalogService catalogService = new DetailCatalogService();
                            var catalogResult = catalogService.GetDetailsByTableAndValue("EstadoTareas", "Registrada");

                            if (catalogResult.MessageType == MessageType.INFO)
                            {
                                task.IdEstado = catalogResult.Data.IdDetCatalogo;

                                if (!task.TareaPeriodica)
                                    result = SaveTask(task, project);
                                else
                                {
                                    var delRep = _unitOfWork.GetRepository<MstEntregables>();
                                    var deliverable = delRep.GetById(task.IdEntregable, task.IdProyecto);

                                    result = CalculateFrequency(task, project, deliverable.FechaInicio);
                                }

                                scope.Complete();
                            }
                            else
                            {
                                result.Message = "La tarea ya se encuentra registrada para la fase indicada.";
                                result.DetailException = new Exception(result.Message);
                                result.MessageType = MessageType.ERROR;
                            }
                        }
                        else
                        {
                            // validar valor catalogo no encontrado
                            result.Message = "La tarea ya existe";
                            result.DetailException = new Exception(result.Message);
                            result.MessageType = MessageType.WARNING;
                        }
                    }
                }
                catch (System.Data.Entity.Infrastructure.DbUpdateConcurrencyException ex)
                {
                    //saveFailed = true;

                    // Update original values from the database 
                    var entry = ex.Entries.Single();
                    entry.OriginalValues.SetValues(entry.GetDatabaseValues());
                }
                catch (Exception ex)
                {
                    result.Message = string.Format(Messages.ErrorSaveData, "tarea");
                    result.MessageType = MessageType.ERROR;

                    if (ex.GetType().Name == "DbEntityValidationException")
                        result.DetailException = new Exception(Utilities.GetEntityValidationErrors(ex as System.Data.Entity.Validation.DbEntityValidationException));
                    else
                        result.DetailException = ex;
                }
            }

            return result;
        }

        public ResultBase UpdateTask(MstTareas task)
        {
            ResultBase result = new ResultBase();

            try
            {
                MstProyectos project = new MstProyectos();

                var resultValidate = ValidateTaskData(task, ref project);

                if (resultValidate.MessageType == MessageType.ERROR)
                    result = resultValidate;
                else
                {
                    var tempTask = Repository.Get(t => t.IdTarea == task.IdTarea);

                    if (tempTask != null)
                    {
                        if (!tempTask.Nombre.ToLower().Equals(task.Nombre.ToLower()))
                        {
                            var taskByName = Repository.Get(t => t.Nombre.ToLower().Equals(task.Nombre.ToLower()) && t.IdProyecto == task.IdProyecto &&
                                                   t.IdEntregable == task.IdEntregable);
                            if (taskByName != null)
                            {
                                result.Message = "Ya existe una tarea con el nombre: " + task.Nombre;
                                result.DetailException = new Exception(result.Message);
                                result.MessageType = MessageType.ERROR;
                            }
                            else
                                tempTask.Nombre = task.Nombre;
                        }


                        tempTask.Nota = task.Nota;

                        tempTask.FechaUpd = DateTime.Now;

                        if (tempTask.FechaInicio != task.FechaInicio || tempTask.FechaFin != task.FechaFin)
                        {
                            // Se calculan la duración en días y horas de la tarea
                            result = CalculateEstimatedDurationInDaysAndHours(ref task, project);
                            if (result.MessageType != MessageType.SUCCESS)
                                return result;

                            tempTask.FechaInicio = task.FechaInicio;
                            tempTask.FechaFin = task.FechaFin;

                            tempTask.DuracionEstimadaDias = task.DuracionEstimadaDias;
                            tempTask.DuracionEstimadaHoras = task.DuracionEstimadaHoras;
                        }
                        Repository.Update(tempTask);

                        InsertPersonalTask(task.DetTareaPersonal, task.IdTarea);

                        _unitOfWork.Commit();
                    }
                    else
                    {
                        result.Message = "No se encontró tarea con el identificador especificado.";
                        result.DetailException = new Exception(result.Message);
                        result.MessageType = MessageType.ERROR;
                    }
                }
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateConcurrencyException ex)
            {
                // Update original values from the database 
                var entry = ex.Entries.Single();
                entry.OriginalValues.SetValues(entry.GetDatabaseValues());
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorSaveData, "tarea");
                result.MessageType = MessageType.ERROR;

                if (ex.GetType().Name == "DbEntityValidationException")
                    result.DetailException = new Exception(Utilities.GetEntityValidationErrors(ex as System.Data.Entity.Validation.DbEntityValidationException));
                else
                    result.DetailException = ex;
            }

            return result;
        }

        public ResultBase UpdateStatus(int taskID, int statusID, int userID, string comments = "", decimal? progress = null)
        {
            ResultBase result = new ResultBase();

            try
            {
                var tempTask = Repository.Get(t => t.IdTarea == taskID);

                if (tempTask != null)
                {
                    tempTask.IdEstado = statusID;
                    tempTask.IdUsuarioUpd = userID;

                    Repository.Update(tempTask);

                    // Se registra la creación de la tarea en la bitacora
                    BinnacleService binnacleService = new BinnacleService(_unitOfWork);
                    var resultBinnacle = binnacleService.InsertBinnacle(new BitacoraTareas
                    {
                        IdTarea = taskID,
                        IdEntregable = tempTask.IdEntregable,
                        IdProyecto = tempTask.IdProyecto,
                        IdUsuario = userID,
                        Creacion = false,
                        IdEstado = statusID,
                        Progreso = progress,
                        Comentarios = (string.IsNullOrWhiteSpace(comments) ? null : comments)
                    }, false);

                    if (resultBinnacle.MessageType == MessageType.SUCCESS)
                        _unitOfWork.Commit();
                    else
                        result = resultBinnacle;
                }
                else
                {
                    result.Message = "No se encontró tarea con el identificador especificado.";
                    result.DetailException = new Exception(result.Message);
                    result.MessageType = MessageType.ERROR;
                }
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateConcurrencyException ex)
            {
                // Update original values from the database 
                var entry = ex.Entries.Single();
                entry.OriginalValues.SetValues(entry.GetDatabaseValues());
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorSaveData, "tarea");
                result.MessageType = MessageType.ERROR;

                if (ex.GetType().Name == "DbEntityValidationException")
                    result.DetailException = new Exception(Utilities.GetEntityValidationErrors(ex as System.Data.Entity.Validation.DbEntityValidationException));
                else
                    result.DetailException = ex;
            }

            return result;
        }

        public ResultBase DeleteTask(int taskID)
        {
            ResultBase result = new ResultBase();

            try
            {
                if (taskID > 0)
                {
                    MstTareas task = Repository.Get(t => t.IdTarea == taskID);

                    if (task != null)
                    {
                        // Se verifica el estado del proyecto 
                        var project = _unitOfWork.GetRepository<MstProyectos>().GetById(task.IdProyecto);

                        if (project != null)
                        {
                            var det = _unitOfWork.GetRepository<DetCatalogo>().Get(d => d.IdDetCatalogo == project.IdEstado);

                            if (det != null && !det.Valor.Equals(Catalog.ProjectStatusInPlanning))
                            {
                                result.Message = "No se puede eliminar la tarea debido a que el proyecto no se encuentra en planificación";
                                result.DetailException = new Exception(result.Message);
                                result.MessageType = MessageType.ERROR;
                                return result;
                            }

                            Repository.Delete(t => t.IdTarea == taskID);
                            _unitOfWork.Commit();

                            result.Message = "La tarea ha sido eliminada exitosamente";
                        }
                        else // NUNCA DEBERIA ENTRAR AQUI
                        {
                            result.Message = "No se encontró el registro de proyecto";
                            result.DetailException = new Exception(result.Message);
                            result.MessageType = MessageType.ERROR;
                        }
                    }
                    else
                    {
                        result.Message = "No se encontró la tarea con el identificador especificado";
                        result.DetailException = new Exception(result.Message);
                        result.MessageType = MessageType.ERROR;
                    }
                }
                else
                {
                    result.Message = "Se debe proporcionar un identificador válido de tarea";
                    result.DetailException = new Exception(result.Message);
                    result.MessageType = MessageType.ERROR;
                }
            }
            catch (Exception ex)
            {
                result.Message = "Ocurrió un error al intentar eliminar la tarea";
                result.MessageType = MessageType.ERROR;

                if (ex.GetType().Name == "DbEntityValidationException")
                    result.DetailException = new Exception(Utilities.GetEntityValidationErrors(ex as System.Data.Entity.Validation.DbEntityValidationException));
                else
                    result.DetailException = ex;
            }

            return result;
        }

        /// <summary>
        /// Obtiene la cantidad de tareas ejecutadas agrupadas por entregables en la ultima semana
        /// </summary>
        /// <param name="projectID">Identificador del proyecto</param>
        /// <param name="lstDays">Listado de dias (Parámetro de referencia)</param>
        /// <returns>Objeto de tipo Result. 
        ///    Message: Mensaje del proceso
        ///    DetailException: Excepción si la hubo
        ///    MessageType: Tipo de mensaje (Enumerable MessageType)
        ///    Data: Diccionario con llave string y valor listado de enteros
        ///          donde la llave corresponde a la descripción del entregable y el listado de enteros
        ///          es la cantidad de tareas finalizadas por dia</returns>
        public Result<Dictionary<string, List<int>>> GetTasksFinishedWeekly(int projectID, ref List<string> lstDays)
        {
            Result<Dictionary<string, List<int>>> result = new Result<Dictionary<string, List<int>>>();

            try
            {
                Dictionary<string, List<int>> dictionary = new Dictionary<string, List<int>>();
                DetailCatalogService catalogService = new DetailCatalogService();

                var catalogResult = catalogService.GetDetailsByTableAndValue("EstadoTareas", "Ejecutada");
                if (catalogResult.MessageType == MessageType.INFO)
                {
                    int countMaxDays = 0;
                    ProjectService projectService = new ProjectService();
                    var resultWorkingDaysPerWeek = projectService.GetWorkingDaysPerWeek(projectID, ref countMaxDays);

                    if (resultWorkingDaysPerWeek.MessageType == MessageType.INFO)
                    {
                        var dic = resultWorkingDaysPerWeek.Data;
                        var viewRepository = _unitOfWork.GetRepository<vwBitacoraTareas>();

                        // Se cargan en un listado de dias de la semana
                        List<DayOfWeek> lstDaysOfWeek = new List<DayOfWeek>();

                        foreach (KeyValuePair<DayOfWeek, string> kvp in dic)
                        {
                            lstDaysOfWeek.Add(kvp.Key);
                        }

                        DeliverableService deliverableService = new DeliverableService();
                        var deliverablesResult = deliverableService.GetDeliverablesByProjectID(projectID);

                        if (deliverablesResult.MessageType == MessageType.INFO)
                        {
                            int j = 0;
                            foreach (var item in deliverablesResult.Data)
                            {
                                string deliverable = item.Descripcion;
                                List<int> tasksFinished = new List<int>();

                                for (int i = countMaxDays; i >= 0; i--)
                                {
                                    DateTime currentDay = DateTime.Now.AddDays(i * -1);
                                    // Si el dia de la semana no está en la lista de dias de la semana laborables se obtiene el dia anterior
                                    if (!lstDaysOfWeek.Contains(currentDay.DayOfWeek))
                                        continue; //currentDay = currentDay.AddDays(1);

                                    if (j == 0)
                                        lstDays.Add(dic.Where(x => x.Key == currentDay.DayOfWeek).FirstOrDefault().Value);

                                    var tasksCount = viewRepository.GetMany(p => p.IdProyecto == projectID && p.IdEntregable == item.IdEntregable &&
                                                                       System.Data.Entity.DbFunctions.TruncateTime(p.Fecha) == currentDay.Date &&
                                                                       p.IdEstado == catalogResult.Data.IdDetCatalogo && p.Finalizado.Value).ToList().Count;
                                    tasksFinished.Add(tasksCount);
                                }

                                dictionary.Add(deliverable, tasksFinished);
                                j++;
                            }

                            result.Data = dictionary;
                            result.Message = Messages.SuccessGetResult;
                        }
                        else
                        {
                            result.MessageType = MessageType.ERROR;
                            result.DetailException = deliverablesResult.DetailException;
                            result.Message = "No se pudo recuperar la información de tareas";
                        }
                    }
                    else
                    {
                        result.MessageType = MessageType.ERROR;
                        result.DetailException = resultWorkingDaysPerWeek.DetailException;
                        result.Message = "No se pudo recuperar la información de tareas";
                    }
                }
                else
                {
                    result.MessageType = MessageType.ERROR;
                    result.DetailException = catalogResult.DetailException;
                    result.Message = "No se pudo recuperar la información de tareas";
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        #region Otros Métodos

        #region Cálculos

        private ResultBase CalculateEstimatedDurationInDaysAndHours(ref MstTareas task, MstProyectos project)
        {
            ResultBase result = new ResultBase();

            try
            {
                // Se obtiene la cantidad de horas laborales para un horario de medio tiempo
                var parameter = _unitOfWork.GetRepository<CatParametros>().Get(p => p.NombreParametro.Equals("CANT_HORAS_LABORALES_MT"));
                double hoursMiddleTimeCount = double.Parse(parameter.ValorParametro);

                if (project != null)
                {
                    int workingDaysPerWeekID = project.DiasLabSem;

                    // Se obtiene las jornada laborales
                    var detailCatalogService = new DetailCatalogService();

                    var resultDetailCatalog = detailCatalogService.GetDetailsByTable("JornadaLaboralSemanal");

                    if (resultDetailCatalog.MessageType != MessageType.INFO)
                        throw resultDetailCatalog.DetailException;

                    var workday = resultDetailCatalog.Data.FirstOrDefault(w => w.IdDetCatalogo == workingDaysPerWeekID);

                    int strHorarioId = int.Parse(workday.Valor);

                    // Se obtienen los días laborales que el horario posee 
                    var workdayCount = _unitOfWork.GetRepository<DetHorario>().GetMany(s => s.IdHorario == strHorarioId);

                    // Se cargan en un listado de dias de la semana
                    Dictionary<DayOfWeek, double> lstDaysOfWeek = new Dictionary<DayOfWeek, double>();
                    foreach (var day in workdayCount)
                    {
                        double workingHours = day.HoraSalida.Subtract(day.HoraEntrada).TotalHours;

                        // Si el horario excede la cantidad de horas de medio tiempo se le resta una hora (Almuerzo)
                        if (workingHours > hoursMiddleTimeCount)
                            workingHours = workingHours - 1;

                        lstDaysOfWeek.Add((DayOfWeek)day.Dia, workingHours);
                    }

                    // Calculando los dias laborables
                    double estimatedDurationInDays = 0, estimatedDurationInHours = 0;
                    TimeSpan ts = task.FechaFin - task.FechaInicio;

                    for (int i = 0; i < ts.Days; i++)
                    {
                        // Si el dia de la semana está en la lista de dias de la semana laborables, se suma un día
                        if (lstDaysOfWeek.Keys.Contains((task.FechaInicio.AddDays(i)).DayOfWeek))
                        {
                            double workingHours = lstDaysOfWeek[(task.FechaInicio.AddDays(i)).DayOfWeek];

                            // Si el día tiene horario de tiempo completo
                            if (workingHours > hoursMiddleTimeCount)
                                estimatedDurationInDays = estimatedDurationInDays + 1; // Se le suma un día
                            else
                                estimatedDurationInDays = estimatedDurationInDays + 0.5; // De lo contrario se le suma medio dia

                            // Se suman las horas
                            estimatedDurationInHours = estimatedDurationInHours + lstDaysOfWeek[(task.FechaInicio.AddDays(i)).DayOfWeek];
                        }
                    }

                    // Restando los días feriados 
                    var listHolidayActive = _unitOfWork.GetRepository<CatFeriados>().GetMany(h => h.Activo);

                    // Se recorren todos los días feriado
                    foreach (var holiday in listHolidayActive)
                    {
                        // Se arma una nueva fecha con el dia del mes feriado pero con el año actual
                        DateTime newDate = new DateTime(DateTime.Now.Year, holiday.FechaFeriado.Month, holiday.FechaFeriado.Day);

                        // Si el día feriado está entre las fechas del inicio y fin del proyecto, se resta el día y las horas laborables de ese día
                        if ((newDate.CompareTo(task.FechaInicio) == 0 || newDate.CompareTo(task.FechaInicio) > 0) &&
                            (newDate.CompareTo(task.FechaFin) == 0 || newDate.CompareTo(task.FechaFin) < 0))
                        {
                            estimatedDurationInDays = estimatedDurationInDays - 1;
                            estimatedDurationInHours = estimatedDurationInHours + lstDaysOfWeek[newDate.DayOfWeek];
                        }
                    }

                    // Se establece la duración estimada en días y horas del proyecto
                    task.DuracionEstimadaDias = Convert.ToDecimal(estimatedDurationInDays);
                    task.DuracionEstimadaHoras = Convert.ToDecimal(estimatedDurationInHours);

                    //// Se actualiza el registro de proyecto
                    //Repository.Update(project);
                }
                else
                {
                    result.Message = string.Format(Messages.NotExist, "proyecto");
                    result.DetailException = new Exception("No se encontró registro de proyecto con el id: " + project.IdProyecto);
                    result.MessageType = MessageType.ERROR;
                }
            }
            catch (Exception ex)
            {
                result.Message = "Ocurrió un error al intentar calcular la duración estimada en días de la tarea";
                result.MessageType = MessageType.ERROR;
                result.DetailException = ex;
            }

            return result;
        }

        #endregion

        private ResultBase ValidateTaskData(MstTareas task, ref MstProyectos project, bool isUpdate = false)
        {
            ResultBase result = new ResultBase();

            /***************************************** VALIDACION DE LA INFORMACIÓN DE LA TAREA ******************************************/

            if (task == null)
            {
                result.MessageType = MessageType.ERROR;
                result.Message = string.Format(Messages.WithoutValidate, "proyecto");
                result.DetailException = new Exception(result.Message + ". Debido a que el objeto MstProyectos es nulo");
                goto @return;
            }

            if (task.IdEntregable == 0)
            {
                result.MessageType = MessageType.ERROR;
                result.Message = "Se debe proporcionar el identificador de la fase a la cual pertenece la tarea";
                result.DetailException = new Exception(result.Message);
                goto @return;
            }
            else
            {
                if (task.IdProyecto == 0)
                {
                    result.MessageType = MessageType.ERROR;
                    result.Message = "Se debe proporcionar el identificador del proyecto al cual pertenece la tarea";
                    result.DetailException = new Exception(result.Message);
                    goto @return;
                }
                else
                {
                    var projectRep = _unitOfWork.GetRepository<MstProyectos>();
                    project = projectRep.GetById(task.IdProyecto);
                }
            }

            if (string.IsNullOrWhiteSpace(task.Nombre))
            {
                result.MessageType = MessageType.ERROR;
                result.Message = "Se debe proporcionar el nombre de la tarea";
                result.DetailException = new Exception(result.Message);
                goto @return;
            }


            if (task.Hito)
            {
                if (task.FechaInicio.CompareTo(task.FechaFin) != 0)
                {
                    result.MessageType = MessageType.ERROR;
                    result.Message = "La fecha inicio y la fecha fin del hito deben ser la misma";
                    result.DetailException = new Exception(result.Message);
                    goto @return;
                }
            }
            else
            {
                if (task.FechaFin.CompareTo(task.FechaInicio) < 0)
                {
                    result.MessageType = MessageType.ERROR;
                    result.Message = "La fecha fin debe ser mayor a la fecha inicio";
                    result.DetailException = new Exception(result.Message);
                    goto @return;
                }

                if (task.DetTareaPersonal != null && task.DetTareaPersonal.Count > 0)
                {
                    if (!task.DetTareaPersonal.ToList().Any(p => p.TipoPersonal == "R"))
                    {
                        result.MessageType = MessageType.ERROR;
                        result.Message = "Se debe proporcionar una persona responsable de la tarea";
                        result.DetailException = new Exception(result.Message);
                        goto @return;
                    }
                }

                // VALIDAR PRESUPUESTO (DEBE VENIR UNO)
            }

        @return:
            return result;
        }

        private ResultBase InsertPersonalTask(ICollection<DetTareaPersonal> personalTask, int taskID)
        {
            ResultBase result = new ResultBase();

            try
            {
                List<int> lstPersonalID = new List<int>();
                Repository<DetTareaPersonal> detRepository = _unitOfWork.GetRepository<DetTareaPersonal>();

                foreach (var item in personalTask)
                {
                    var dtPersonal = detRepository.Get(p => p.IdPersonal == item.IdPersonal && p.IdTarea == taskID);

                    if (dtPersonal == null)
                        detRepository.Add(item);
                    else
                    {
                        if (!dtPersonal.TipoPersonal.ToLower().Equals(item.TipoPersonal.ToLower()))
                        {
                            item.TipoPersonal = dtPersonal.TipoPersonal;
                            detRepository.Update(item);
                        }
                    }

                    lstPersonalID.Add(item.IdPersonal);
                }
                var a = detRepository.GetMany(p => p.IdTarea == taskID).ToList();
                a.ForEach(x =>
                {
                    if(!lstPersonalID.Contains(x.IdPersonal))
                        detRepository.Delete(x);
                });
                //detRepository.Delete(x => !lstPersonalID.Contains(x.IdPersonal) && x.IdTarea == taskID);
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorSaveData, "tarea");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        private ResultBase CalculateFrequency(MstTareas task, MstProyectos project, DateTime deliverableStartDate)
        {
            ResultBase result = new ResultBase();

            if (task.DetConfiguracionTareaFrecuencia != null && task.DetConfiguracionTareaFrecuencia.Count > 0)
            {
                var detFrecuency = task.DetConfiguracionTareaFrecuencia.First();

                if (detFrecuency.DetIntervaloFrecuencia != null && detFrecuency.DetIntervaloFrecuencia.Count > 0)
                {
                    var detConfFrecuency = detFrecuency.DetIntervaloFrecuencia.FirstOrDefault();

                    if (detConfFrecuency.FechaInicio.CompareTo(deliverableStartDate) < 0)
                    {
                        result.MessageType = MessageType.ERROR;
                        result.Message = "La fecha inicio de la tarea periodica debe ser mayor a la fecha inicio de la fase";
                        result.DetailException = new Exception(result.Message);
                    }
                    else
                    {
                        int workingDaysPerWeekID = project.DiasLabSem;

                        // Se obtiene las jornada laborales
                        var detailCatalogService = new DetailCatalogService();

                        var resultDetailCatalog = detailCatalogService.GetDetailsByTable("JornadaLaboralSemanal");

                        if (resultDetailCatalog.MessageType != MessageType.INFO)
                        {
                            result.MessageType = resultDetailCatalog.MessageType;
                            result.Message = resultDetailCatalog.Message;
                            result.DetailException = resultDetailCatalog.DetailException;
                        }
                        else
                        {
                            var workday = resultDetailCatalog.Data.FirstOrDefault(w => w.IdDetCatalogo == workingDaysPerWeekID);

                            int strHorarioId = int.Parse(workday.Valor);

                            // Se obtienen los días laborales que el horario posee 
                            var workdayCount = _unitOfWork.GetRepository<DetHorario>().GetMany(s => s.IdHorario == strHorarioId);

                            // Se cargan en un listado de dias de la semana
                            List<DayOfWeek> lstDaysOfWeek = new List<DayOfWeek>();
                            workdayCount.ToList().ForEach(d => lstDaysOfWeek.Add((DayOfWeek)d.Dia));

                            // Se obtienen los días feriados
                            var listHolidayActive = _unitOfWork.GetRepository<CatFeriados>().GetMany(h => h.Activo);

                            // Si la frecuencia es diaria
                            if (detFrecuency.TipoFrecuencia == 1)
                            {
                                // Se obtiene el intervalo de tiempo
                                TimeSpan ts = new TimeSpan();

                                // Con el final del proyecto
                                if (detConfFrecuency.TipoFinalizacion == 1)
                                    ts = project.FechaFinEstimada - detConfFrecuency.FechaInicio;

                                // Después de una cantidad definida de repeticiones
                                else if (detConfFrecuency.TipoFinalizacion == 2)
                                    ts = detConfFrecuency.FechaInicio.AddDays(detConfFrecuency.CantidadRepeticiones.Value) - detConfFrecuency.FechaInicio;

                                // Fecha en específico
                                else if (detConfFrecuency.TipoFinalizacion == 3)
                                    ts = detConfFrecuency.FechaFin.Value - detConfFrecuency.FechaInicio;

                                int days = ts.Days;

                                for (int i = 0; i < days; i++)
                                {
                                    // daysCount = daysCount + i;

                                    var currentDay = detConfFrecuency.FechaInicio.AddDays(i);

                                    // Si el dia de la semana no está en la lista de dias de la semana laborables se pasa al siguiente día
                                    if (!lstDaysOfWeek.Contains(currentDay.DayOfWeek))
                                    {
                                        days = days + 1;
                                        continue;
                                    }

                                    // Se recorren todos los días feriado
                                    foreach (var holiday in listHolidayActive)
                                    {
                                        // Se arma una nueva fecha con el dia del mes feriado pero con el año de la fecha actual
                                        DateTime newDate = new DateTime(currentDay.Year, holiday.FechaFeriado.Month, holiday.FechaFeriado.Day);

                                        // Si el dia es feriado se pasa a validar el siguiente dia
                                        if (currentDay.CompareTo(newDate) == 0)
                                        {
                                            days = days + 1;
                                            continue;
                                        }
                                    }

                                    // Se crea la tarea
                                    var newTask = new MstTareas
                                    {
                                        FechaInicio = currentDay,
                                        Nota = task.Nota,
                                        Nombre = task.Nombre,
                                        FechaFin = currentDay,//.AddDays(detFrecuency.CantidadDias.Value),
                                        //DetTareaPersonal = task.DetTareaPersonal.ToList(),
                                        //MstPresupuesto = task.MstPresupuesto.ToList(),
                                        IdEntregable = task.IdEntregable,
                                        IdProyecto = task.IdProyecto,
                                        IdEstado = task.IdEstado,
                                        IdUsuarioIns = task.IdUsuarioIns
                                    };
                                    newTask.DetTareaPersonal = new List<DetTareaPersonal>();
                                    task.DetTareaPersonal.ToList().ForEach(p => newTask.DetTareaPersonal.Add(new DetTareaPersonal
                                    {
                                        IdEntregable = p.IdEntregable,
                                        IdProyecto = p.IdProyecto,
                                        IdPersonal = p.IdPersonal,
                                        TipoPersonal = p.TipoPersonal
                                    }));

                                    //newTask.MstPresupuesto = new List<MstPresupuesto>();
                                    //task.MstPresupuesto.ToList().ForEach(p => newTask.MstPresupuesto.Add(new MstPresupuesto
                                    //{
                                    //    Activo = true,
                                    //    FechaIns = DateTime.Now, 
                                    //    IdEntregable = p.IdEntregable,
                                    //    IdProyecto = p.IdProyecto,
                                    //    IdUsuarioIns = p.IdUsuarioIns,
                                    //    PresupuestoAsignado = 0,
                                    //    PresupuestoActual = 0
                                    //}));

                                    result = SaveTask(newTask, project, ((ts.Days - 1 == i) ? true : false));

                                }
                            }
                            // Si el tipo de frecuencia es semanal
                            else if (detFrecuency.TipoFrecuencia == 2)
                            {
                                // Se valida que la cantidad de dias de duración de la tarea no sea mayor a 7
                                if (detFrecuency.DuracionDias.Value > 7)
                                {
                                    result.MessageType = MessageType.ERROR;
                                    result.Message = "La duración en días de la tarea no puede ser mayor a 7";
                                    result.DetailException = new Exception(result.Message);
                                    return result;
                                }

                                // Se obtiene el intervalo de tiempo
                                TimeSpan ts = new TimeSpan();

                                // Con el final del proyecto
                                if (detConfFrecuency.TipoFinalizacion == 1)
                                    ts = project.FechaFinEstimada - detConfFrecuency.FechaInicio;

                                // Después de una cantidad definida de repeticiones
                                else if (detConfFrecuency.TipoFinalizacion == 2)
                                {
                                    int daysPerWeeks = ((detConfFrecuency.CantidadRepeticiones.Value * detFrecuency.CantidadSemanas.Value) * 7);
                                    ts = detConfFrecuency.FechaInicio.AddDays(daysPerWeeks) - detConfFrecuency.FechaInicio;
                                }

                                // Fecha en específico
                                else if (detConfFrecuency.TipoFinalizacion == 3)
                                    ts = detConfFrecuency.FechaFin.Value - detConfFrecuency.FechaInicio;

                                int i = 0;
                                int weeksCount = Convert.ToInt32(Math.Truncate((decimal)ts.Days / 7));

                                while (weeksCount > i)
                                {
                                    // Revisar aqui porque puede ser que se necesite sumarle un dia para que caiga en el dia que corresponde
                                    DateTime day = detConfFrecuency.FechaInicio.AddDays(i * 7);

                                    DayOfWeek dayOfWeek = day.DayOfWeek;
                                    DayOfWeek selectedDay = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), detFrecuency.DiaInicio);

                                    if (selectedDay < dayOfWeek)
                                    {
                                        // saltar a la siguiente iteración
                                    }

                                repeatHoliday:
                                    // Se recorren todos los días feriado
                                    foreach (var holiday in listHolidayActive)
                                    {
                                        // Se arma una nueva fecha con el dia del mes feriado pero con el año de la fecha actual
                                        DateTime newDate = new DateTime(day.Year, holiday.FechaFeriado.Month, holiday.FechaFeriado.Day);

                                        // Si el dia es feriado se pasa a validar el siguiente dia
                                        if (day.CompareTo(newDate) == 0)
                                        {
                                            day = day.AddDays(1);
                                            goto repeatHoliday;
                                        }
                                    }

                                    // Se crea la tarea
                                    var newTask = new MstTareas
                                    {
                                        FechaInicio = day,
                                        Nota = task.Nota,
                                        Nombre = task.Nombre,
                                        FechaFin = day.AddDays(detFrecuency.DuracionDias.Value),//.AddDays(detFrecuency.CantidadDias.Value),
                                        //DetTareaPersonal = task.DetTareaPersonal.ToList(),
                                        //MstPresupuesto = task.MstPresupuesto.ToList(),
                                        IdEntregable = task.IdEntregable,
                                        IdProyecto = task.IdProyecto,
                                        IdEstado = task.IdEstado,
                                        IdUsuarioIns = task.IdUsuarioIns
                                    };
                                    newTask.DetTareaPersonal = new List<DetTareaPersonal>();
                                    task.DetTareaPersonal.ToList().ForEach(p => newTask.DetTareaPersonal.Add(new DetTareaPersonal
                                    {
                                        IdEntregable = p.IdEntregable,
                                        IdProyecto = p.IdProyecto,
                                        IdPersonal = p.IdPersonal,
                                        TipoPersonal = p.TipoPersonal
                                    }));

                                    // Se crea la tarea
                                    result = SaveTask(newTask, project);

                                    i = i + detFrecuency.CantidadSemanas.Value;
                                }
                            }
                            // Si el tipo de frecuenca es mensual
                            else if (detFrecuency.TipoFrecuencia == 2)
                            {
                                int cantidadMeses = 0, i = 0;

                                // Se valida que la cantidad de dias de duración de la tarea no sea mayor a 31
                                if (detConfFrecuency.DetConfiguracionTareaFrecuencia.CantidadDias.Value > 31)
                                {
                                    result.MessageType = MessageType.ERROR;
                                    result.Message = "La duración en días de la tarea no puede ser mayor a 31";
                                    result.DetailException = new Exception(result.Message);
                                    return result;
                                }

                                // Con el final del proyecto
                                if (detConfFrecuency.TipoFinalizacion == 1)
                                    cantidadMeses = project.FechaFinEstimada.Month - detConfFrecuency.FechaInicio.Month;

                                // Después de una cantidad definida de repeticiones
                                else if (detConfFrecuency.TipoFinalizacion == 2)
                                    cantidadMeses = (detConfFrecuency.FechaInicio.AddDays(detConfFrecuency.CantidadRepeticiones.Value)).Month - detConfFrecuency.FechaInicio.Month;

                                // Fecha en específico
                                else if (detConfFrecuency.TipoFinalizacion == 3)
                                    cantidadMeses = detConfFrecuency.FechaFin.Value.Month - detConfFrecuency.FechaInicio.Month;

                                while (cantidadMeses > i)
                                {
                                    int durationInDays = detConfFrecuency.DetConfiguracionTareaFrecuencia.CantidadDias.Value;

                                    DateTime dateAddMonth = detConfFrecuency.FechaInicio.AddMonths(i);

                                    // Se valida que el dia seleccionado este en el mes especificado (Eje: 30 en febrero)
                                    int daysPerMonth = DateTime.DaysInMonth(dateAddMonth.Year, dateAddMonth.Month);

                                    // Si el dia seleccionado en la frecuencia no existe en el mes de la iteración se establece el último dia dicho mes
                                    if (daysPerMonth < detFrecuency.DiaDelMes.Value)
                                        detFrecuency.DiaDelMes = daysPerMonth;

                                    // Si la duración en días excede la cantidad de dias del mes, se establece todos los dias del como duración
                                    if (daysPerMonth < durationInDays)
                                        durationInDays = daysPerMonth;

                                    DateTime dayInMonth = new DateTime(dateAddMonth.Year, dateAddMonth.Month, detFrecuency.DiaDelMes.Value);

                                repeatDaysOfWeek:
                                    // Si el dia de la semana no está en la lista de dias de la semana laborables se pasa al siguiente día
                                    if (!lstDaysOfWeek.Contains(dayInMonth.DayOfWeek))
                                    {
                                        dayInMonth = dayInMonth.AddDays(1);
                                        goto repeatDaysOfWeek;
                                    }

                                    //repeatHoliday:
                                    //// Se recorren todos los días feriado
                                    //foreach (var holiday in listHolidayActive)
                                    //{
                                    //    // Se arma una nueva fecha con el dia del mes feriado pero con el año de la fecha actual
                                    //    DateTime newDate = new DateTime(dayInMonth.Year, holiday.FechaFeriado.Month, holiday.FechaFeriado.Day);

                                    //    // Si el dia es feriado se pasa a validar el siguiente dia
                                    //    if (dayInMonth.CompareTo(newDate) == 0)
                                    //    {
                                    //        dayInMonth = dayInMonth.AddDays(1);
                                    //        goto repeatHoliday;
                                    //    }
                                    //}
                                    ValidateHoliday(ref dayInMonth);

                                    // Se crea la tarea
                                    result = SaveTask(new MstTareas
                                    {
                                        FechaInicio = dayInMonth,
                                        Nota = task.Nota,
                                        Nombre = task.Nombre,
                                        FechaFin = dayInMonth.AddDays(durationInDays),
                                        DetTareaPersonal = task.DetTareaPersonal.ToList(),
                                        // MstPresupuesto = task.MstPresupuesto.ToList(),
                                        IdEntregable = task.IdEntregable,
                                        IdProyecto = task.IdProyecto,
                                        IdEstado = task.IdEstado,
                                        IdUsuarioIns = task.IdUsuarioIns
                                    }, project);


                                    i = i + detFrecuency.CantidadMeses.Value;
                                }
                            }
                        }
                    }
                }

            }
            return result;
        }

        private ResultBase SaveTask(MstTareas task, MstProyectos project, bool last = true)
        {
            ResultBase result = new ResultBase();

            task.FechaIns = DateTime.Now;

            ////Obteniendo el identificador de la tarea
            //var sequencesService = new SequenceService(_unitOfWork.GetRepository<MstSecuencias>());

            //MstSecuencias seq = sequencesService.GetNextSequenceByTableName("[pro].[MstTareas]");

            //if (last)
            //{
            //    seq.SecuencialTabla = seq.SecuencialTabla + TaskIDCount;
            //    sequencesService.InsertSequence(seq);

            //    task.IdTarea = seq.SecuencialTabla;
            //}
            //else
            //    task.IdTarea = seq.SecuencialTabla + TaskIDCount;

            // Se calculan la duración en días y horas de la tarea
            result = CalculateEstimatedDurationInDaysAndHours(ref task, project);
            if (result.MessageType != MessageType.SUCCESS)
                return result;

            // Guardando el nuevo registro
            Repository.Add(task);
            _unitOfWork.Commit();

            // Se registra la creación de la tarea en la bitacora
            BinnacleService binnacleService = new BinnacleService(_unitOfWork);
            var resultBinnacle = binnacleService.InsertBinnacle(new BitacoraTareas
            {
                IdTarea = task.IdTarea,
                IdEntregable = task.IdEntregable,
                IdProyecto = task.IdProyecto,
                IdUsuario = task.IdUsuarioIns,
                Creacion = true,
                IdEstado = task.IdEstado
            }, false);

            if (resultBinnacle.MessageType != MessageType.SUCCESS)
                result = resultBinnacle;
            else
                _unitOfWork.Commit();
            // TaskIDCount++;

            //// Se inserta el personal vinculado a la tarea
            //result = InsertPersonalTask(task.DetTareaPersonal, task.IdTarea);
            //if (result.MessageType != MessageType.SUCCESS)
            //    return result;


            //if (task.MstPresupuesto != null)
            //{
            //    // Se inserta el presupuesto asignado a la tarea
            //    result = InsertBudgetTask(task.MstPresupuesto.FirstOrDefault());
            //    if (result.MessageType != MessageType.SUCCESS)
            //        return result;
            //}

            return result;
        }

        private void ValidateHoliday(ref DateTime date)
        {
            // Se obtienen los días feriados
            var listHolidayActive = _unitOfWork.GetRepository<CatFeriados>().GetMany(h => h.Activo);

            // Se recorren todos los días feriado
            foreach (var holiday in listHolidayActive)
            {
                // Se arma una nueva fecha con el dia del mes feriado pero con el año de la fecha actual
                DateTime newDate = new DateTime(date.Year, holiday.FechaFeriado.Month, holiday.FechaFeriado.Day);

                // Si el dia es feriado se pasa a validar el siguiente dia
                if (date.CompareTo(newDate) == 0)
                {
                    date = date.AddDays(1);
                    ValidateHoliday(ref date);
                    break;
                }
            }
        }

        #endregion
    }
}
