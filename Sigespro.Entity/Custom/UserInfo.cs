﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigespro.Entity.Custom
{
    public class UserInfo
    {
        public int UserId { get; set; }

        public string UserName { get; set; } 

        public string FullName { get; set; }

        public string Email { get; set; }

        public int ProgramId { get; set; }

        public int PersonalId { get; set; }

        public bool ChangePassword { get; set; }

        public short ExpirationSession { get; set; }
    }
}
