﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Helpers
{
    public class Result<T> : ResultBase where T : class  
    {
        public Result() : base()
        {
        }

        public T Data { get; set; }
        
    }
}
