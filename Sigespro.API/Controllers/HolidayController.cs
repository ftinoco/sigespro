﻿using Common.Extensions;
using Common.Log;
using Common.Helpers;
using Sigespro.Entity;
using Sigespro.Entity.Resource;
using Sigespro.Service;
using Sigespro.Service.Admin;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Sigespro.API.Controllers
{
    [RoutePrefix("Api/Holiday")]
    public class HolidayController : ApiController
    {
        HolidayService _service;

        public HolidayController(Func<string, IService> serviceFactory)
        {
            _service = (HolidayService)serviceFactory("Holiday");
        }

        [HttpGet]
        [Route("")]
        public IEnumerable<CatFeriados> Get()
        {
            var result = _service.GetHolidays();

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data;
        }

        [HttpGet]
        [Route("{HolidayId}")]
        public CatFeriados GetHoliday(int HolidayId)
        {
            var result = _service.GetHolidayById(HolidayId);

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data;
        }

        [HttpGet]
        [Route("Filter/{Filter}")]
        public IEnumerable<CatFeriados> GetHolidays(string Filter)
        {
            var result = _service.GetHolidays(Filter);

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data;
        }
        
        [HttpPost]
        [Route("List")]
        public HttpResponseMessage ListHolidays(QueryOptions<IEnumerable<CatFeriados>> filter)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            _service.GetHolidaysForList(ref filter);

            if (filter.ResultData.MessageType == MessageType.INFO)
                response = Request.CreateResponse(HttpStatusCode.OK, filter, "application/json");
            else
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, filter.ResultData.Message);
                Logger.Log(filter.ResultData.MessageType, filter.ResultData.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());
            }

            return response;
        }
        
        [HttpPost]
        [Route("")]
        public HttpResponseMessage Post(CatFeriados holiday)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            if (ModelState.IsValid)
            {
                ResultBase result = new ResultBase();

                try
                {
                    result = _service.InsertHoliday(holiday);

                    if (result.MessageType == MessageType.SUCCESS)
                    {
                        response = Request.CreateResponse(HttpStatusCode.Created, holiday);

                        //string uri = Url.Link("DefaultApi", new { id = holiday.IdFeriado });

                        //response.Headers.Location = new Uri(uri);
                    }
                    else
                    {
                        response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, result.Message);

                        throw result.DetailException;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log(result.MessageType, ex, "Yo xD", GetType().Name, Request.GetClientIpAddress());
                }
            }
            else
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.RequiredData);

            return response;
        }

        [HttpPut]
        [Route("{HolidayId}")]
        public HttpResponseMessage Put(int HolidayId, CatFeriados holiday)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            if (!ModelState.IsValid)
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.RequiredData);
            else
            {
                ResultBase result = new ResultBase();

                try
                {
                    if (HolidayId != holiday.IdFeriado)
                        response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "La información no coincide con el identificador proporcionado.");
                    else
                    {
                        result = _service.UpdateHoliday(holiday);

                        if (result.MessageType == MessageType.SUCCESS)
                            response = Request.CreateResponse(HttpStatusCode.OK);
                        else
                        {
                            if (result.MessageType == MessageType.WARNING)
                                response = Request.CreateErrorResponse(HttpStatusCode.NotFound, result.Message);

                            else if (result.MessageType == MessageType.ERROR)
                                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, result.Message);

                            throw result.DetailException;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log(result.MessageType, ex, "Yo xD", GetType().Name, Request.GetClientIpAddress());
                }
            }

            return response;
        }
    }
}
