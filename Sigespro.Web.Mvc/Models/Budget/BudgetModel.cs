﻿using Sigespro.Web.Mvc.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sigespro.Web.Mvc.Models.Budget
{
    public class BudgetModel
    {
        public int BudgetParentID { get; set; }

        public int BudgetID { get; set; }
         
        public int BudgetPK { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public int TaskID { get; set; }

        public string TaskName { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string EntryID { get; set; }

        public string EntryName { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public decimal BudgetMount { get; set; }

        public bool IsEditable { get; set; }
        
        public bool Deleted { get; set; }
    }
}