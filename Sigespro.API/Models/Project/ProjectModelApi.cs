﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sigespro.API.Models.Project
{
    public class ProjectModelApi
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public string Justification { get; set; }

        public int ProjectType { get; set; }

        public int BeneficiaryType { get; set; }

        public string Beneficiary { get; set; }

        public int StatusId { get; set; }

        public decimal Budget { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime DeadLine { get; set; }
    }
}