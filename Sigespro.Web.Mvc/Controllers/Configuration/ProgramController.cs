﻿using Common.Helpers;
using Common.Log;
using Newtonsoft.Json;
using Sigespro.Entity;
using Sigespro.Service.Config;
using Sigespro.Web.Mvc.Models.Configuration;
using Sigespro.Web.Mvc.Resources;
using Sigespro.Web.Mvc.Utilities.Attributes.Security;
using Sigespro.Web.Mvc.Utilities.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Sigespro.Web.Mvc.Controllers.Configuration
{
    [RoutePrefix("Configuration/Programs")] 
    public class ProgramController : Controller
    {
        private readonly IUserSession _userSession;

        public ProgramService Service { get; set; }

        public ProgramController()
        {
            Service = new ProgramService();
            _userSession = new UserSession();
        }

        [Route]
        public ActionResult Index(QueryOptions<IEnumerable<ProgramViewModel>> queryOpt)
        {
            string message = string.Empty;
            IEnumerable<ProgramViewModel> model = new List<ProgramViewModel>();

            try
            {
                string strSerialize = JsonConvert.SerializeObject(queryOpt);

                QueryOptions<IEnumerable<MstPrograma>> filter = JsonConvert.DeserializeObject<QueryOptions<IEnumerable<MstPrograma>>>(strSerialize);
                Service.GetProgramsForList(ref filter);

                if (filter.ResultData.MessageType != MessageType.INFO)
                {
                    message = filter.ResultData.Message;
                    Logger.Log(filter.ResultData.MessageType, filter.ResultData.DetailException, _userSession.Username,
                               MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                {
                    strSerialize = JsonConvert.SerializeObject(filter);

                    queryOpt = JsonConvert.DeserializeObject<QueryOptions<IEnumerable<ProgramViewModel>>>(strSerialize);

                    model = queryOpt.ResultData.Data;
                }
            }
            catch (Exception ex)
            {
                message = Messages.LoadPageError;
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            if (!string.IsNullOrEmpty(message))
                ModelState.AddModelError(Messages.AlertDanger, message);

            ViewBag.Filter = true;
            ViewBag.QueryOptions = queryOpt;

            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [Route("Add")]
        public ActionResult AddProgram()
        {
            ViewBag.IsEdit = false;
            ViewBag.Action = "SaveProgram";
            ViewBag.Title = "Nuevo Programa";

            ProgramViewModel model = new ProgramViewModel();

            model.FechaIns = DateTime.Now;

            model.IdUsuarioIns = 1;

            return View(model);
        }

        [Route("Edit/{programId}")]
        public ActionResult EditProgram(int programId)
        {
            string message = string.Empty;
            ProgramViewModel model = new ProgramViewModel();

            try
            {
                var result = Service.GetProgramById(programId);

                if (result.MessageType != MessageType.INFO)
                    message = result.Message;
                else
                {
                    model = new ProgramViewModel
                    {
                        Activo = result.Data.Activo,
                        Descripcion = result.Data.Descripcion,
                        FechaIns = result.Data.FechaIns,
                        FechaUpd = result.Data.FechaUpd,
                        IdPrograma = result.Data.IdPrograma,
                        IdUsuarioIns = result.Data.IdUsuarioIns,
                        NombrePrograma = result.Data.NombrePrograma,
                        IdUsuarioUpd = result.Data.IdUsuarioUpd
                    };
                }
            }
            catch (Exception ex)
            {
                message = Messages.ErrorApi;
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                           MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            if (!string.IsNullOrEmpty(message))
                ModelState.AddModelError(Messages.AlertDanger, message);

            ViewBag.IsEdit = true;
            ViewBag.Action = "UpdateProgram";
            ViewBag.Title = "Editar Programa";

            if (!model.FechaUpd.HasValue)
                model.FechaUpd = DateTime.Now;

            if (!model.IdUsuarioUpd.HasValue)
                model.IdUsuarioUpd = 1;

            return View("AddProgram", model);
        }

        [AjaxOnly]
        [HttpPost]
        [Route("Save")]
        [AllowAnonymous]
        public ActionResult SaveProgram(ProgramViewModel model)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                model.IdUsuarioIns = _userSession.UserId;

                string strSerialize = JsonConvert.SerializeObject(model);

                var result = Service.InsertProgram(JsonConvert.DeserializeObject<MstPrograma>(strSerialize));

                if (result.MessageType != MessageType.SUCCESS)
                {
                    message = result.Message;
                    status = result.MessageType.ToString();
                    Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                    message = string.Format(Messages.SaveSuccess, "programa");
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                             MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message, action = Url.Action("Index", "Program") },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [AjaxOnly]
        [HttpPost]
        [AllowAnonymous]
        [Route("Update")]
        public ActionResult UpdateProgram(ProgramViewModel model)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                model.IdUsuarioUpd = _userSession.UserId;
                string strSerialize = JsonConvert.SerializeObject(model);

                var result = Service.UpdateProgram(JsonConvert.DeserializeObject<MstPrograma>(strSerialize));

                if (result.MessageType != MessageType.SUCCESS)
                {
                    message = result.Message;
                    status = result.MessageType.ToString();
                    Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                    message = string.Format(Messages.UpdateSuccess, "programa");
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                             MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message, action = Url.Action("Index", "Program") },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }
}