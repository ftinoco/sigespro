﻿using Common.Helpers;
using Common.Log;
using Newtonsoft.Json;
using Sigespro.Entity;
using Sigespro.Service.Admin;
using Sigespro.Service.Pre;
using Sigespro.Web.Mvc.Models.Administration;
using Sigespro.Web.Mvc.Models.Budget;
using Sigespro.Web.Mvc.Resources;
using Sigespro.Web.Mvc.Utilities;
using Sigespro.Web.Mvc.Utilities.Attributes.Security;
using Sigespro.Web.Mvc.Utilities.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Sigespro.Web.Mvc.Controllers.Budget
{
    [RoutePrefix("Budget/Entries")]
    public class EntryController : Controller
    {
        private readonly IUserSession _userSession;

        public EntryService Service { get; set; }

        public EntryController()
        {
            Service = new EntryService();
            _userSession = new UserSession();
        }

        [Route]
        public ActionResult Index(QueryOptions<IEnumerable<EntriesViewModel>> queryOpt)
        {
            string message = string.Empty;
            IEnumerable<EntriesViewModel> model = new List<EntriesViewModel>();

            try
            {
                string strSerialize = JsonConvert.SerializeObject(queryOpt);

                QueryOptions<IEnumerable<vwRubros>> filter = JsonConvert.DeserializeObject<QueryOptions<IEnumerable<vwRubros>>>(strSerialize);
                Service.GetEntriesForList(ref filter);

                if (filter.ResultData.MessageType != MessageType.INFO)
                {
                    message = filter.ResultData.Message;
                    Logger.Log(filter.ResultData.MessageType, filter.ResultData.DetailException, _userSession.Username,
                               MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                {
                    strSerialize = JsonConvert.SerializeObject(filter);

                    queryOpt = JsonConvert.DeserializeObject<QueryOptions<IEnumerable<EntriesViewModel>>>(strSerialize);

                    model = queryOpt.ResultData.Data;
                }
            }
            catch (Exception ex)
            {
                message = Messages.LoadPageError;
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            if (!string.IsNullOrEmpty(message))
                ModelState.AddModelError(Messages.AlertDanger, message);

            ViewBag.Filter = true;
            ViewBag.QueryOptions = queryOpt;

            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [Route("Add")]
        public ActionResult AddEntry()
        {
            ViewBag.IsEdit = false;
            ViewBag.Action = "SaveEntry";
            string message = string.Empty;
            ViewBag.Title = "Agregar Rubro";

            EntriesViewModel model = new EntriesViewModel();
            model.FechaIns = DateTime.Now;
            model.IdUsuarioIns = _userSession.UserId;

            try
            {
                List<SelectListItem> selectListItem = new List<SelectListItem>();

                // Se obtienen los rubro que sean de tipo "Grupo de Rubros"
                var result = Service.GetEntriesByType(0.ToString(), "GrupoRubro");

                if (result.MessageType != MessageType.INFO)
                    message = result.Message;
                else
                {
                    selectListItem.Add(new SelectListItem { Value = "0", Text = Labels.Select });

                    result.Data.ToList().ForEach(c => selectListItem.Add(
                        new SelectListItem
                        {
                            Value = c.IdRubro.ToString(),
                            Text = c.IdRubro + " - " + c.Nombre
                        }
                    ));
                }

                ViewBag.EntryGroups = selectListItem; // new SelectList(selectListItem, "Value", "Text");

                // Se obtiene los tipos de rubros
                DetailCatalogService dcService = new DetailCatalogService();

                var entryTypesResult = dcService.GetDetailsByTable("TipoRubro"); // httpClientApi.GetAsyncObject<List<DetCatalogViewModel>>(string.Format(Constants.API_DETAILCATALOG + "{0}", "GetByTable/TipoRubro"));

                if (entryTypesResult.MessageType != MessageType.INFO)
                    message = entryTypesResult.Message;
                else
                {
                    selectListItem = new List<SelectListItem>();

                    selectListItem.Add(new SelectListItem { Value = "0", Text = Labels.Select });

                    entryTypesResult.Data.ToList().ForEach(c => selectListItem.Add(
                        new SelectListItem
                        {
                            Value = c.IdDetCatalogo.ToString(),
                            Text = c.Descripcion
                        }
                    ));
                }

                ViewBag.EntryTypes = new SelectList(selectListItem, "Value", "Text");
            }
            catch (Exception ex)
            {
                message = Messages.ErrorApi;
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            if (!string.IsNullOrEmpty(message))
                ModelState.AddModelError(Messages.AlertDanger, message);

            return View(model);
        }

        [Route("Edit/{entryId}")]
        public ActionResult EditEntry(string entryId)
        {
            string message = string.Empty;

            EntriesViewModel model = new EntriesViewModel();

            ViewBag.EntryGroups = new List<SelectListItem>();
            List<SelectListItem> selectListItem = new List<SelectListItem>();

            try
            {
                // SE OBTIENE EL REGISTRO RUBRO POR ID
                var result = Service.GetEntryById(entryId);

                if (result.MessageType != MessageType.INFO)
                    message = result.Message;
                else
                {
                    //string strSerialize = JsonConvert.SerializeObject(result.Data);

                    model = new EntriesViewModel
                    {
                        Activo = result.Data.Activo,
                        FechaIns = result.Data.FechaIns,
                        FechaUpd = result.Data.FechaUpd,
                        IdRubro = result.Data.IdRubro,
                        IdRubroParent = result.Data.IdRubroParent,
                        IdTipoRubro = result.Data.IdTipoRubro,
                        IdUsuarioIns = result.Data.IdUsuarioIns,
                        IdUsuarioUpd = result.Data.IdUsuarioUpd,
                        Nombre = result.Data.Nombre,
                        RubroParent = result.Data.Nombre
                    };

                    // Se obtiene los tipos de rubros
                    DetailCatalogService dcService = new DetailCatalogService();

                    var entryTypesResult = dcService.GetDetailsByTable("TipoRubro");
                    if (entryTypesResult.MessageType != MessageType.INFO)
                        message = entryTypesResult.Message;
                    else
                    {
                        foreach (var item in entryTypesResult.Data)
                        {
                            if (item.IdDetCatalogo == model.IdTipoRubro)
                                selectListItem.Add(new SelectListItem { Value = item.IdDetCatalogo.ToString(), Text = item.Descripcion });
                        }
                    }

                    ViewBag.EntryTypes = new SelectList(selectListItem, "Value", "Text");
                }

                selectListItem = new List<SelectListItem>();
                if (model.IdRubroParent != "00000")
                { 
                    // Se obtienen el rubro padre
                    var resultParent = Service.GetEntryById(model.IdRubroParent);

                    if (resultParent.MessageType != MessageType.INFO)
                        message = resultParent.Message;
                    else
                    {
                        var entryGroup = new EntriesViewModel
                        {
                            Activo = resultParent.Data.Activo,
                            FechaIns = resultParent.Data.FechaIns,
                            FechaUpd = resultParent.Data.FechaUpd,
                            IdRubro = resultParent.Data.IdRubro,
                            IdRubroParent = resultParent.Data.IdRubroParent,
                            IdTipoRubro = resultParent.Data.IdTipoRubro,
                            IdUsuarioIns = resultParent.Data.IdUsuarioIns,
                            IdUsuarioUpd = resultParent.Data.IdUsuarioUpd,
                            Nombre = resultParent.Data.Nombre
                        };

                        selectListItem.Add(new SelectListItem { Value = entryGroup.IdRubro, Text = entryGroup.IdRubro + " - " + entryGroup.Nombre });
                    }
                }
                else
                    selectListItem.Add(new SelectListItem { Value = "0", Text = Labels.Select });

                ViewBag.EntryGroups = selectListItem; // new SelectList(selectListItem, "Value", "Text");  
            }
            catch (Exception ex)
            {
                message = Messages.ErrorApi;
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                           MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            if (!string.IsNullOrEmpty(message))
                ModelState.AddModelError(Messages.AlertDanger, message);

            ViewBag.IsEdit = true;
            ViewBag.Action = "UpdateEntry";
            ViewBag.Title = "Editar Rubro";

            if (!model.FechaUpd.HasValue)
                model.FechaUpd = DateTime.Now;

            if (!model.IdUsuarioUpd.HasValue)
                model.IdUsuarioUpd = 1;

            return View("AddEntry", model);
        }

        [AjaxOnly]
        [HttpPost]
        [Route("Save")]
        [AllowAnonymous]
        public ActionResult SaveEntry(EntriesViewModel model)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                model.IdUsuarioIns = _userSession.UserId;

                string strSerialize = JsonConvert.SerializeObject(model);

                var result = Service.InsertEntry(JsonConvert.DeserializeObject<CatRubros>(strSerialize));

                if (result.MessageType != MessageType.SUCCESS)
                {
                    message = result.Message;
                    status = result.MessageType.ToString();
                    Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                    message = string.Format(Messages.SaveSuccess, "rubro");
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message, action = Url.Action("Index", "Entry") },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

        [AjaxOnly]
        [HttpPost]
        [AllowAnonymous]
        [Route("Update")]
        public ActionResult UpdateEntry(EntriesViewModel model)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                model.IdUsuarioUpd = _userSession.UserId;
                string strSerialize = JsonConvert.SerializeObject(model);

                var result = Service.UpdateEntry(JsonConvert.DeserializeObject<CatRubros>(strSerialize));

                if (result.MessageType != MessageType.SUCCESS)
                {
                    message = result.Message;
                    status = result.MessageType.ToString();
                    Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                    message = string.Format(Messages.UpdateSuccess, "rubro");
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                             MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message, action = Url.Action("Index", "Entry") },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }
}