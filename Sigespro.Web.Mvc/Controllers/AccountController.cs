﻿using Common.Log;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using Sigespro.Service.Security;
using Sigespro.Web.Mvc.Models;
using Sigespro.Web.Mvc.Utilities.Attributes;
using Sigespro.Web.Mvc.Utilities.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace Sigespro.Web.Mvc.Controllers
{
    [RoutePrefix("Account")]
    public class AccountController : Controller
    {
        private readonly IUserSession _userSession;

        public AccountController()
        {
            _userSession = new UserSession();
        }

        #region Login

        [HttpGet]
        [AllowAnonymous]
        [Route("Login")]
        [ImportModelState]
        public ActionResult Login()
        {
            Request.GetOwinContext().Authentication.SignOut("ApplicationCookie");

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("Login")]
        [ExportModelState]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.ContainsKey("LoginError"))
                ModelState["LoginError"].Errors.Clear();

            try
            {
                if (ModelState.IsValid)
                {
                    UserService service = new UserService();

                    var result = service.Authenticate(model.UserName, model.Password, "1");

                    if (result.MessageType == Common.Helpers.MessageType.SUCCESS)
                    {
                        AuthenticationProperties options = new AuthenticationProperties();

                        options.AllowRefresh = true;
                        options.IsPersistent = true;
                        options.ExpiresUtc = DateTime.UtcNow.AddSeconds(result.Data.ExpirationSession);

                        var claims = new[]
                        {
                            new Claim(ClaimTypes.Name, result.Data.UserName),
                            new Claim(ClaimTypes.Email, result.Data.Email),
                            new Claim("FullName", result.Data.FullName),
                            new Claim("UserId", result.Data.UserId.ToString()),
                            new Claim("ProgramId", result.Data.ProgramId.ToString()),
                            new Claim("PersonalId", result.Data.PersonalId.ToString()),
                            new Claim("ChangePassword", result.Data.ChangePassword.ToString())
                        };

                        var identity = new ClaimsIdentity(claims, "ApplicationCookie");

                        Request.GetOwinContext().Authentication.SignIn(options, identity);

                        if (Url.IsLocalUrl(returnUrl))
                            return Redirect(returnUrl);
                        else
                            return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ModelState.AddModelError("LoginError", result.Message);
                        Logger.Log(Common.Helpers.MessageType.ERROR, result.DetailException, model.UserName,
                          MethodBase.GetCurrentMethod().Name + " - " + GetType().Name,
                          Request.UserHostAddress);
                    }
                        
                }
            }
            catch (System.Threading.Tasks.TaskCanceledException ex)
            {
                Logger.Log(Common.Helpers.MessageType.ERROR, ex, model.UserName, 
                            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, 
                            Request.UserHostAddress);

                ModelState.AddModelError("ResetError", Resources.Messages.GeneralError);
            }

            return RedirectToAction("Login");
        }

        [AllowAnonymous]
        [Route("LogOut")]
        public ActionResult LogOut()
        {
            Request.GetOwinContext().Authentication.SignOut("ApplicationCookie");

            return RedirectToAction("Login");
        }

        #endregion

        #region Recuperar Contraseña

        [HttpGet]
        [AllowAnonymous]
        [ImportModelState]
        [Route("ResetPassword")]
        public ActionResult ResetPassword()
        {
            Request.GetOwinContext().Authentication.SignOut("ApplicationCookie");

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ExportModelState]
        [Route("ResetPassword")]
        public ActionResult ResetPassword(ResetPasswordModel model)
        {
            if (ModelState.ContainsKey("ResetError"))
                ModelState["ResetError"].Errors.Clear();

            try
            {
                if (ModelState.IsValid)
                {
                    UserService service = new UserService();

                    var result = service.GetUserByName(model.UserName);

                    if (result.MessageType == Common.Helpers.MessageType.INFO)
                    {
                        if (result.Data.Email == model.EmailAddress)
                        {
                            var resultReset = service.ResetPassword(result.Data.UserId);

                            if (resultReset.MessageType == Common.Helpers.MessageType.SUCCESS)
                            {
                                ModelState.AddModelError("LoginError", resultReset.Message);
                                return RedirectToActionPermanent("Login");
                            }
                            else
                                ModelState.AddModelError("ResetError", resultReset.Message);
                        }
                        else
                            ModelState.AddModelError("ResetError", "El correo proporcionado no está vinculado al usuario ingresado");
                    }
                    else
                        ModelState.AddModelError("ResetError", result.Message);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(Common.Helpers.MessageType.ERROR, ex, _userSession.Username,
                            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name,
                            Request.UserHostAddress);

                ModelState.AddModelError("ResetError", Resources.Messages.GeneralError);
            }

            return RedirectToAction("ResetPassword");
        }


        #endregion

        #region Acciones de cuenta cuando el usuario esta logueado

        [AllowAnonymous]
        [Route("Profile")]
        public ActionResult UserProfile()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ChangePassword()
        {
            return View(_userSession.UserId);
        }

        [AllowAnonymous]
        [HttpPost, Utilities.Attributes.Security.AjaxOnly]
        public ActionResult ChangePassword(int userID, string currentPassword, string newPassword, string newPasswordRepeat)
        {
            UserService service = new UserService();

            var result = service.ChangePassword(userID, currentPassword, newPassword, newPasswordRepeat);

            return new JsonResult
            {
                Data = new
                {
                    action = Url.Action("LogOut", "Account"),
                    status = result.MessageType == Common.Helpers.MessageType.ERROR ? "ERROR" : "SUCCESS",
                    message = result.Message
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        #endregion
    }
}