﻿using Common.Extensions;
using Common.Helpers;
using Common.Log;
using Sigespro.DAL.Implementations;
using Sigespro.Entity;
using Sigespro.Entity.Custom;
using Sigespro.Entity.Resource;
using Sigespro.Service.Admin;
using Sigespro.Service.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Common.Helpers.Utilities;

namespace Sigespro.Service.Pro
{
    public class PersonalService : Service
    {
        private Repository<DetPersonal> Repository { get; set; }

        public PersonalService() : base()
        {
            Repository = _unitOfWork.GetRepository<DetPersonal>();
        }

        public Result<IEnumerable<object>> GetPersonal()
        {
            Result<IEnumerable<object>> result = new Result<IEnumerable<object>>();

            try
            {
                var repositoryDetCatalogo = _unitOfWork.GetRepository<DetCatalogo>();

                result.Data = repositoryDetCatalogo.GetAll().Join(
                                    Repository.GetAll(),
                                    det => det.IdDetCatalogo,
                                    p => p.IdCargo,
                                    (det, p) => new
                                    {
                                        IdPersonal = p.IdPersonal,
                                        IdPersona = p.IdPersona,
                                        IdInstitucion = p.IdInstitucion,
                                        IdPrograma = p.IdPrograma,
                                        IdCargo = p.IdCargo,
                                        Cargo = det.Descripcion,
                                        MstInstituciones = p.MstInstituciones,
                                        MstPersonas = p.MstPersonas,
                                        MstPrograma = p.MstPrograma
                                    }
                        );

                result.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }
        
        public Result<List<vwListadoPersonal>> GetAllPersonal()
        {
            Result<List<vwListadoPersonal>> result = new Result<List<vwListadoPersonal>>();

            try
            {
                result.Data = _unitOfWork.GetRepository<vwListadoPersonal>().GetAll().ToList();
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorGetResultSet, "persona");
                result.MessageType = MessageType.ERROR;
                result.DetailException = ex;
            }

            return result;
        }
        
        public Result<DetPersonal> GetPersonalById(int idPersonal)
        {
            Result<DetPersonal> result = new Result<DetPersonal>();

            try
            {
                result.Data = Repository.GetById(idPersonal);
                result.Message = Messages.SuccessGetResult;
                result.MessageType = MessageType.INFO;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<IEnumerable<Program>> GetPersonal(string filter)
        {
            Result<IEnumerable<Program>> result = new Result<IEnumerable<Program>>();

            try
            {
                string filterToLower = filter.Trim().ToLower();

                var repositoryDetCatalogo = _unitOfWork.GetRepository<DetCatalogo>();

                var anonymous = repositoryDetCatalogo.GetAll().Join(
                                    Repository.GetAll(),
                                    det => det.IdDetCatalogo,
                                    p => p.IdCargo,
                                    (det, p) => new Program
                                    {
                                        IdPersonal = p.IdPersonal,
                                        IdPersona = p.IdPersona,
                                        IdInstitucion = p.IdInstitucion,
                                        IdPrograma = p.IdPrograma,
                                        IdCargo = p.IdCargo,
                                        Cargo = det.Descripcion,
                                        MstInstituciones = p.MstInstituciones,
                                        MstPersonas = p.MstPersonas,
                                        MstPrograma = p.MstPrograma
                                    }
                            );

                if (filterToLower == Catalog.Active || filterToLower == Catalog.Inactive)
                    result.Data = anonymous.Where(a => a.MstPersonas.Activo == (filterToLower == Catalog.Active ? true : false));
                else
                {
                    result.Data = anonymous.Where(a => a.MstPersonas.NombreCompleto.Trim().ToLower().Contains(filterToLower) ||
                                                a.MstPersonas.Identificacion.Trim().ToLower().Contains(filterToLower) ||
                                                (a.MstInstituciones == null ? a.MstPrograma.Descripcion : a.MstInstituciones.Nombre).Trim().ToLower().Contains(filterToLower) ||
                                                a.Cargo.Trim().ToLower().Contains(filterToLower));
                }

                result.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<IEnumerable<Program>> GetPersonalByPrograms(int[] programsId)
        {
            Result<IEnumerable<Program>> result = new Result<IEnumerable<Program>>();

            try
            {
                var repositoryDetCatalogo = _unitOfWork.GetRepository<DetCatalogo>();

                var anonymous = repositoryDetCatalogo.GetAll().Join(
                                    Repository.GetAll(),
                                    det => det.IdDetCatalogo,
                                    p => p.IdCargo,
                                    (det, p) => new Program
                                    {
                                        IdPersonal = p.IdPersonal,
                                        IdPersona = p.IdPersona,
                                        IdInstitucion = p.IdInstitucion,
                                        IdPrograma = p.IdPrograma,
                                        IdCargo = p.IdCargo,
                                        Cargo = det.Descripcion,
                                        MstInstituciones = p.MstInstituciones,
                                        MstPersonas = p.MstPersonas,
                                        MstPrograma = p.MstPrograma
                                    }
                            ).Where(a => programsId.Contains(a.MstPrograma.IdPrograma));

                result.Data = anonymous; //.Where(a => programsId.Contains(a.MstPrograma.IdPrograma));

                result.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public void GetPersonalForList(ref QueryOptions<IEnumerable<vwListadoPersonal>> queryOpt)
        {
            Result<IEnumerable<vwListadoPersonal>> result = new Result<IEnumerable<vwListadoPersonal>>();

            try
            {
                string orderBy = "Nombre";

                var predicate = PredicateBuilder.True<vwListadoPersonal>();

                var filter = queryOpt.Filters.FirstOrDefault(f => f.Name.Equals("Search"));

                if (!IsNull(filter) && !string.IsNullOrEmpty(filter.Value))
                {
                    int intFilter;

                    // AGREGAR VALIDACIONES DE FILTROS
                    predicate = predicate.And(p => p.Nombre.ToLower().Trim().Contains(filter.Value.ToLower().Trim()) || 
                                                   p.Identificacion.ToLower().Trim().Contains(filter.Value.ToLower().Trim()) ||
                                                   p.NombrePrograma.ToLower().Trim().Contains(filter.Value.ToLower().Trim()) || 
                                                   p.NombreInstitucion.ToLower().Trim().Contains(filter.Value.ToLower().Trim()) ||
                                                   p.Cargo.ToLower().Trim().Contains(filter.Value.ToLower().Trim()));

                    if (int.TryParse(filter.Value, out intFilter))
                        predicate = predicate.Or(p => p.IdPersona == intFilter);

                }
                else
                    queryOpt.TotalRecords = _unitOfWork.GetRepository<vwListadoPersonal>().Count();

                if (!string.IsNullOrEmpty(queryOpt.SortField))
                    orderBy = queryOpt.SortField;

                result.Data = _unitOfWork.GetRepository<vwListadoPersonal>().GetManyOnDemand(predicate, orderBy, queryOpt.CurrentPageIndex, queryOpt.RowsPerPage, queryOpt.SortOrder);

                result.Message = Messages.SuccessGetResultSet;

                result.MessageType = MessageType.INFO;
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorGetResultSet, "persona");
                result.MessageType = MessageType.ERROR;
                result.DetailException = ex;
            }

            queryOpt.ResultData = result;
        }
        
        public Result<List<vwListadoPersonal>> GetPersonalByProjectID(int projectID)
        {
            Result<List<vwListadoPersonal>> result = new Result<List<vwListadoPersonal>>();

            try
            {
                result.Data = _unitOfWork.GetRepository<vwListadoPersonal>().GetAll()
                                         .Join(_unitOfWork.GetRepository<DetProgramaProyecto>().GetAll(),
                                                vw => vw.IdPrograma,
                                                pp => pp.IdPrograma,
                                                (vw, pp) => new { vw, pp })
                                         .Where(p => p.pp.IdProyecto == projectID)
                                         .Select(p => p.vw).ToList();
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorGetResultSet, "persona");
                result.MessageType = MessageType.ERROR;
                result.DetailException = ex;
            }

            return result;
        }

        public ResultBase InsertPersonal(DetPersonal personal)
        {
            ResultBase result = new ResultBase();

            try
            {
                var resultValidate = ValidatePersonalData(personal);

                if (resultValidate.MessageType != MessageType.SUCCESS)
                    result = resultValidate;
                else
                {
                    PersonService personService = new PersonService(_unitOfWork);

                    var resultPerson = personService.InsertPerson(personal.MstPersonas);

                    if (resultPerson.MessageType == MessageType.SUCCESS)
                    {
                        var sequencesService = new SequenceService(_unitOfWork.GetRepository<MstSecuencias>());

                        personal.IdPersona = personal.MstPersonas.IdPersona;

                        MstSecuencias seq = sequencesService.GetNextSequenceByTableName("[pro].[DetPersonal]");

                        personal.IdPersonal = seq.SecuencialTabla;

                        sequencesService.InsertSequence(seq);

                        Repository.Add(personal);

                        _unitOfWork.Commit();

                        result.Message = Messages.SuccessSaveData;
                    }
                    else
                        result = resultPerson;
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorSaveData, "personal");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase UpdatePersonal(DetPersonal personal)
        {
            ResultBase result = new ResultBase();

            try
            {
                var resultValidate = ValidatePersonalData(personal, true);

                if (resultValidate.MessageType != MessageType.SUCCESS)
                    result = resultValidate;
                else
                {
                    var temp = Repository.Get(p => p.IdPersonal == personal.IdPersonal);

                    if (temp != null)
                    {
                        PersonService personService = new PersonService(_unitOfWork);

                        var resultPerson = personService.UpdatePerson(personal.MstPersonas);

                        if (resultPerson.MessageType == MessageType.SUCCESS)
                        {
                            Repository.Update(personal);

                            _unitOfWork.Commit();

                            result.Message = Messages.SuccessUpdateData;
                        }
                        else
                            result = resultPerson;
                    }
                    else
                    {
                        result.MessageType = MessageType.ERROR;
                        result.Message = "La persona con el identificador especificado no existe.";
                        result.DetailException = new Exception(result.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorUpdateData, "personal");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        private ResultBase ValidatePersonalData(DetPersonal personal, bool update = false)
        {
            ResultBase result = new ResultBase();

            if (personal.IdPersona == 0 && update)
            {
                result.MessageType = MessageType.ERROR;
                result.Message = string.Format(Messages.WithoutValidate, "personal");
                result.DetailException = new Exception(result.Message + ". Debido a que el objeto DetPersonal es nulo");
                return result;
            }

            if (personal.IdPrograma == 0 && personal.IdInstitucion == 0)
            {
                result.MessageType = MessageType.WARNING;
                result.Message = "Se debe proporcionar el programa al cual pertenece la persona";
                result.DetailException = new Exception(result.Message);
                return result;
            }

            if (personal.IdCargo == 0)
            {
                result.MessageType = MessageType.WARNING;
                result.Message = "Se debe proporcionar el cargo que ejerce la persona en el programa";
                result.DetailException = new Exception(result.Message);

            }
            return result;
        }

        /// <summary>
        /// Valida si una persona es coordinadora de algún programa
        /// </summary>
        /// <param name="personalId">Id del personal (Tabla:DetPersonal Campo: IdPersonal)</param>
        /// <param name="programsID">Arreglo con los ids de programas a buscar</param>
        /// <returns>
        /// Objeto ResultBase:
        ///     - MessageType => INFO (No es coordinador)
        ///     - MessageType => SUCCESS (Es coordinador)
        ///     - MessageType => ERROR (Error no controlado)
        /// </returns>
        public ResultBase IsCoordinator(int personalId, int[] programsID)
        {
            ResultBase result = new ResultBase();

            try
            {
                // Se obtiene el codigo de coordinador
                var detailCatalogService = new DetailCatalogService();

                var resultCoordinator = detailCatalogService.GetDetailByValue("Coordinador");

                if (resultCoordinator.MessageType != MessageType.SUCCESS)
                    throw resultCoordinator.DetailException;

                //Se verifica que la persona tenga el rol de coordinador en alguno de los programas seleccionado 
                if (!Repository.Any(p => p.IdPersonal == personalId && p.IdCargo == resultCoordinator.Data.IdDetCatalogo && programsID.Contains(p.IdPrograma.Value)))
                {
                    result = new ResultBase
                    {
                        MessageType = MessageType.INFO,
                        Message = "La persona no es coordinador"
                    };
                }
                else
                    result.Message = "La persona es coordinador";
            }
            catch (Exception ex)
            {
                result.Message = "Ocurrió un error al intentar validar los datos de persona";
                result.MessageType = MessageType.ERROR;
                result.DetailException = ex;
            }

            return result;
        }
    }
}
