﻿'use strict'
app.controller('CatalogController', ['$scope', '$location', '$timeout', 'Catalog',
    function ($scope, $location, $timeout, Catalog) {
        var bookmark;

        $scope.filter = {
            options: {
                debounce: 500
            }
        };

        $scope.query = {
            order: 'Nombre',
            filter: '',
            limit: 10,
            page: 1
        };

        $scope.load = function () {
            $scope.error = null;

            if ($scope.query.filter !== '') {
                $scope.promise = Catalog.filter.query({
                    action: "Filter",
                    parameter: $scope.query.filter
                }, success, error).$promise;
            }
            else
                $scope.promise = Catalog.resource.query(success, error).$promise
        }

        $scope.removeFilter = function () {
            $scope.filter.show = false;
            $scope.query.filter = '';

            if ($scope.filter.form.$dirty) {
                $scope.filter.form.$setPristine();
            }
        };

        $scope.$watch('query.filter', function (newValue, oldValue) {
            if (!oldValue)
                bookmark = $scope.query.page;

            if (newValue !== oldValue)
                $scope.query.page = 1;

            if (!newValue)
                $scope.query.page = bookmark;

            $scope.load();
        });

        $scope.addItem = function () {
            $location.path('/catalog/add');
        };

        $scope.edit = function (id) {
            $location.path('/catalog/edit/' + id);
        };

        function success(catalogs) {
            console.log(catalogs);
            if (catalogs.length > 0)
                $scope.catalogs = catalogs;
            else {
                if ($scope.query.filter !== '')
                    $scope.error = "No se encontraron datos catálogos para el filtro especificado.";
                else
                    $scope.error = "No hay registros catálogos.";
            }
        }

        function error(response) {
            if (response.data == null)
                $scope.error = "Ocurrió un error al intentar obtener los datos.";
            else
                $scope.error = "No se encontraron datos catálogos para el filtro especificado.";
        }
    }
])
.controller('CatalogAddController', ['$scope', '$mdToast', '$mdDialog', '$location', 'Catalog',
    function ($scope, $mdToast, $mdDialog, $location, Catalog) {

        $scope.catalog = {};

        $scope.icon = "add";
        $scope.title = "Agregar Catálogo";

        $scope.cancel = function () {
            $location.path('/catalog/');
        };

        $scope.saveCatalog = function (catalog, e) {
            Catalog.resource.save(catalog, function () {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(false)
                        .title('Mensaje de Información')
                        .textContent('Los datos fueron registrados exitosamente.')
                        .ok('Aceptar')
                        .targetEvent(e)
                    ).then(function () {
                        $location.path('/catalog/');
                    });
            }, function (response) {
                $mdToast.show(
                      $mdToast.simple()
                        .textContent(response.data.Message)
                        .position('top right')
                        .hideDelay(3000)
                    );
            });
        };
    }
])
.controller('CatalogEditController', ['$scope', '$mdToast', '$mdDialog', '$location', '$routeParams', 'Catalog',
    function ($scope, $mdToast, $mdDialog, $location, $routeParams, Catalog) {

        $scope.icon = "edit";
        $scope.title = "Editar Catálogo";

        $scope.cancel = function () {
            $location.path('/catalog/');
        };

        Catalog.resource.get({ CatalogId: $routeParams.id }, getData);

        function getData(data) {
            $scope.catalog = {
                IdCatalogo: data.IdCatalogo,
                Nombre: data.Nombre,
                Descripcion: data.Descripcion,
                Activo: data.Activo,
                IdUsuarioIns: data.IdUsuarioIns,
                FechaIns: data.FechaIns,
                IdUsuarioUpd: data.IdUsuarioUpd,
                FechaUpd: data.FechaUpd
            }
        }

        $scope.saveCatalog = function (catalog, e) {
            Catalog.resource.update({ CatalogId: catalog.IdCatalogo }, catalog, function (data) {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(false)
                        .title('Mensaje de Información')
                        .textContent('Los datos fue actualizado exitosamente.')
                        .ok('Aceptar')
                        .targetEvent(e)
                    ).then(function () {
                        $location.path('/catalog/');
                    });
            }, function (response) {
                $mdToast.show(
                      $mdToast.simple()
                        .textContent(response.data.Message)
                        .position('top right')
                        .hideDelay(3000)
                    );
            });
        };
    }
]);