﻿using Common.Helpers;
using Common.Log;
using Newtonsoft.Json;
using Sigespro.Entity;
using Sigespro.Service.Admin;
using Sigespro.Service.Pro;
using Sigespro.Service.Security;
using Sigespro.Web.Mvc.Models.Administration;
using Sigespro.Web.Mvc.Resources;
using Sigespro.Web.Mvc.Utilities.Attributes.Security;
using Sigespro.Web.Mvc.Utilities.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace Sigespro.Web.Mvc.Controllers.Administration
{
    [RoutePrefix("Administration/Users")]
    public class UserController : Controller
    {
        private readonly IUserSession _userSession;

        public UserService Service { get; set; }

        public UserController()
        {
            _userSession = new UserSession();
            Service = new UserService();
        }

        [Route]
        public ActionResult Index(QueryOptions<IEnumerable<UserViewModel>> queryOpt)
        {
            string message = string.Empty;
            IEnumerable<UserViewModel> model = new List<UserViewModel>();

            string strSerialize = JsonConvert.SerializeObject(queryOpt);

            QueryOptions<IEnumerable<Users>> filter = JsonConvert.DeserializeObject<QueryOptions<IEnumerable<Users>>>(strSerialize);
            Service.GetUsersForList(ref filter);

            if (filter.ResultData.MessageType != MessageType.INFO)
            {
                message = filter.ResultData.Message;
                Logger.Log(filter.ResultData.MessageType, filter.ResultData.DetailException, _userSession.Username,
                           MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }
            else
            {
                strSerialize = JsonConvert.SerializeObject(filter);

                queryOpt = JsonConvert.DeserializeObject<QueryOptions<IEnumerable<UserViewModel>>>(strSerialize);

                model = queryOpt.ResultData.Data;
            }

            if (!string.IsNullOrEmpty(message))
                ModelState.AddModelError(Messages.AlertDanger, message);

            ViewBag.Filter = true;
            ViewBag.QueryOptions = queryOpt;

            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [Route("Add")]
        public ActionResult AddUser()
        {

            UserViewModel model = new UserViewModel();

            return AddEditUser(model);
        }

        [Route("Edit/{userID}")]
        public ActionResult EditUser(int userID)
        {
            UserViewModel model = new UserViewModel();

            try
            {
                var userModel = Service.GetUserByID(userID);

                if (userModel.MessageType == MessageType.INFO)
                {
                    model = new UserViewModel
                    {
                        Active = userModel.Data.Active,
                        Email = userModel.Data.Email,
                        ExpirationSession = userModel.Data.ExpirationSession,
                        FullName = userModel.Data.FullName,
                        PhoneNumber = userModel.Data.PhoneNumber,
                        ReferenceId = userModel.Data.ReferenceId,
                        RoleId = userModel.Data.UserRoles.First().RoleId,
                        UserId = userModel.Data.UserId,
                        UserName = userModel.Data.UserName
                    };
                }
                else
                {
                    ModelState.AddModelError(Messages.AlertDanger, userModel.Message);
                    Logger.Log(MessageType.ERROR, userModel.DetailException, _userSession.Username,
                               MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                    
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(Messages.AlertDanger, "Ocurrió un error al intentar cargar la página");
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                           MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return AddEditUser(model);
        }

        [NonAction]
        private ActionResult AddEditUser(UserViewModel model)
        {
            string message = string.Empty;

            try
            {
                PersonalService personalService = new PersonalService();
                var resultPersona = personalService.GetAllPersonal();

                List<SelectListItem> selectListItem = new List<SelectListItem>();

                selectListItem.Add(new SelectListItem { Value = "0", Text = Labels.Select });

                if (resultPersona.MessageType == MessageType.SUCCESS)
                {
                    resultPersona.Data.ToList().ForEach(c => selectListItem.Add(
                       new SelectListItem
                       {
                           Value = c.IdPersonal.ToString(),
                           Text = c.Nombre
                       }
                   ));

                    ViewBag.SelectList = new SelectList(selectListItem, "Value", "Text");
                }

                RoleService roleService = new RoleService();
                var resultRole = roleService.GetRoles();

                if (resultRole.MessageType == MessageType.SUCCESS)
                {
                    selectListItem = new List<SelectListItem>();
                    selectListItem.Add(new SelectListItem { Value = "0", Text = Labels.Select });

                    resultRole.Data.ToList().ForEach(c => selectListItem.Add(
                      new SelectListItem
                      {
                          Value = c.RoleId.ToString(),
                          Text = c.RoleName
                      }
                  ));

                    ViewBag.SelectListRole = new SelectList(selectListItem, "Value", "Text");
                }
            }
            catch (Exception ex)
            {
                message = Messages.GeneralError;
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                           MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            if (!string.IsNullOrEmpty(message))
                ModelState.AddModelError(Messages.AlertDanger, message);

            if (model.UserId == 0)
            {
                model.ExpirationSession = 600;
                model.Active = true;

                ViewBag.IsEdit = false;
                ViewBag.Action = "SaveUser";
                ViewBag.Title = "Nuevo Usuario";
            }
            else
            {
                ViewBag.IsEdit = true;
                ViewBag.Action = "UpdateUser";
                ViewBag.Title = "Editar Usuario";
            }

            if (Request.IsAjaxRequest())
                return PartialView("AddUser", model);
            else
                return View("AddUser", model);
        }

        [AjaxOnly]
        [AllowAnonymous]
        public ActionResult SelectPerson(UserViewModel model)
        {
            try
            {
                ModelState.Clear();

                if (model.ReferenceId.HasValue && model.ReferenceId.Value > 0)
                {
                    PersonalService personService = new PersonalService();
                    var personResult = personService.GetPersonalById(model.ReferenceId.Value);

                    if (personResult.MessageType == MessageType.INFO)
                    {
                        var personalObj = personResult.Data;

                        if (personalObj.MstPersonas != null)
                        {
                            if (personalObj.MstPersonas.DetPersonaEmail != null)
                                model.Email = personalObj.MstPersonas.DetPersonaEmail.FirstOrDefault().Email;

                            if (personalObj.MstPersonas.DetPersonaTelefono != null)
                                model.PhoneNumber = personalObj.MstPersonas.DetPersonaTelefono.FirstOrDefault().NumeroTelefono;
                        }

                        model.FullName = personalObj.MstPersonas.NombreCompleto;
                    }
                    else
                        Logger.Log(MessageType.ERROR, personResult.DetailException, _userSession.Username,
                                   MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }

            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                           MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return AddEditUser(model);
        }

        [AjaxOnly]
        [AllowAnonymous]
        [Route("ResetPassword/{userID}")]
        public ActionResult ResetPassword(int userID)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                if (userID > 0)
                {
                    var result = Service.ResetPassword(userID);

                    if (result.MessageType != MessageType.SUCCESS)
                    {
                        status = result.MessageType.ToString();  
                        Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                                MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                    }

                    message = result.Message;
                }
                else
                {
                    status = "ERROR";
                    message = "Se debe proporcionar un identificador valido de usuario";
                }
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        
        [AjaxOnly]
        [AllowAnonymous]
        public ActionResult SaveUser(UserViewModel model)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                if (ModelState.IsValid)
                {
                    model.UserInsert = _userSession.UserId;

                    string strSerialize = JsonConvert.SerializeObject(model);

                    var result = Service.InsertUser(JsonConvert.DeserializeObject<Users>(strSerialize), model.RoleId);

                    if (result.MessageType != MessageType.SUCCESS)
                    {
                        message = result.Message;
                        status = result.MessageType.ToString();
                        Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                                MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                    }
                    else
                        message = string.Format(Messages.SaveSuccess, "usuario"); 
                }
                else
                {
                    status = "ERROR";
                    message = Messages.DataRequired;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message, action = Url.Action("Index", "User") },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [AjaxOnly]
        [AllowAnonymous]
        public ActionResult UpdateUser(UserViewModel model)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                if (ModelState.IsValid)
                {
                    model.UserUpdate = _userSession.UserId;

                    string strSerialize = JsonConvert.SerializeObject(model);

                    var result = Service.UpdateUser(JsonConvert.DeserializeObject<Users>(strSerialize), model.RoleId);

                    if (result.MessageType != MessageType.SUCCESS)
                    {
                        message = result.Message;
                        status = result.MessageType.ToString();
                        Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                                MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                    }
                    else
                        message = string.Format(Messages.UpdateSuccess, "usuario");
                }
                else
                {
                    status = "ERROR";
                    message = Messages.DataRequired;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message, action = Url.Action("Index", "User") },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

    }
}