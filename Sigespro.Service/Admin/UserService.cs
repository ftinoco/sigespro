﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sigespro.DAL;
using Sigespro.DAL.Implementations;
using Sigespro.Entity;

using Sigespro.Entity.Resource;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using Common.Helpers;

namespace Sigespro.Service.Admin
{
    public class UserService : Service
    {
        private Repository<MstUsuarios> Repository { get; set; }

     //   private UserManager<IdentityUser> _userManager;

        public UserService() : base()
        {
            Repository = _unitOfWork.GetRepository<MstUsuarios>();

            //_userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(_unitOfWork.));
        }

        public Result<MstUsuarios> Authenticate(string username, string password)
        {
            Result<MstUsuarios> result = new Result<MstUsuarios>();

            try
            {
                var user = Repository.Get(u => u.NombreUsuario == username && u.Contrasenia == password);

                if (user != null && user.IdUsuario > 0)
                {
                    result.Data = user;
                    result.Message = Messages.SuccessGetResult;
                }
                else
                {
                    result.Message = Messages.AuthenticateError;
                    result.MessageType = MessageType.INFO;
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public async Task<MstUsuarios> FindUser(string username, string password)
        {
            try
            {
                MstUsuarios user = await Repository.GetAsync(u => u.NombreUsuario == username && u.Contrasenia == password);

                return user;
            }
            catch (Exception)
            {
                throw;
            }
        }

        //public async Task<IdentityUser> FindUser(string userName, string password)
        //{
        //    try
        //    {

        //        IdentityUser user = await _userManager.FindAsync(userName, password);

        //        return user;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        //private Repository<MstUsuarios> _repository; // = new Repository<MstUsuarios>(); 

        //string[] _result = new string[] { "0", "Correcto" };

        //public UsuarioService():base()
        //{
        //    _repository = UnitOfWork.GetRepository<MstUsuarios>();
        //    //UnitOfWork = new UnitOfWork();
        //    //Repository = UnitOfWork.GetRepository<MstUsuarios>();

        //    //var sequenceRepository = UnitOfWork.GetRepository<MstSecuencias>();

        //    //_sequencesService = new SequenceService(sequenceRepository);

        //}

        //public IEnumerable<MstUsuarios> GetUsers()
        //{
        //    try
        //    {
        //        return _repository.GetAll();
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Log(Logger.MessageType.ERROR, ex, "", GetType().Name, IpAddress);

        //        _result = new string[] { "2", "Ocurrió un error interno en el sistema. Por favor, contacte al administrador!!" };
        //    }

        //    return null;
        //}

        //public MstUsuarios GetUserById(int idUser)
        //{
        //    return _repository.GetById(idUser);
        //}

        //public async Task<MstUsuarios> GetUserByName(string name)
        //{
        //    return _repository.Get(u => u.NombreUsuario == name);
        //}

        //public string[] InsertUser(MstUsuarios user)
        //{
        //    try
        //    {
        //      //  _unitOfWork = new UnitOfWork(_repository.DatabaseFactory);

        //        if (user.IdUsuario == 0)
        //            _repository.Add(user);
        //        else
        //            UpdateUser(user);

        //        UnitOfWork.Commit();
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Log(Logger.MessageType.ERROR, ex, "", GetType().Name, IpAddress);

        //        _result = new string[] { "2", "Ocurrió un error interno en el sistema. Por favor, contacte al administrador." };
        //    }

        //    return _result;
        //}

        //public string[] UpdateUser(MstUsuarios user)
        //{
        //    try
        //    {
        //        //_unitOfWork = new UnitOfWork(_repository.DatabaseFactory);

        //        if (user.IdUsuario > 0)
        //            _repository.Update(user);

        //        UnitOfWork.Commit();
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Log(Logger.MessageType.ERROR, ex, "", GetType().Name, IpAddress);

        //        _result = new string[] { "2", "Ocurrió un error interno en el sistema. Por favor, contacte al administrador!!" };
        //    }

        //    return _result;
        //}

    }
}
