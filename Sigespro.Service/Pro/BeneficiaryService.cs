﻿using Common.Helpers;
using Common.Log;
using Sigespro.DAL.Implementations;
using Sigespro.Entity;
using Sigespro.Entity.Resource;
using Sigespro.Service.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigespro.Service.Pro
{
    public class BeneficiaryService  : Service
    {
        public bool CommitExternal { get; set; }

        private Repository<MstBeneficiarios> Repository { get; set; }

        public BeneficiaryService() : this(new UnitOfWork())
        {
            Repository = _unitOfWork.GetRepository<MstBeneficiarios>();
        }

        public BeneficiaryService(UnitOfWork UnitOfWork) : base()
        {
            _unitOfWork = UnitOfWork;
        }

        public Result<IEnumerable<MstBeneficiarios>> GetBeneficiaries()
        {
            Result<IEnumerable<MstBeneficiarios>> result = new Result<IEnumerable<MstBeneficiarios>>();

            try
            {
                    result.Data = Repository.GetAll();
               
                result.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<MstBeneficiarios> GetBeneficiaryById(int idBeneficiary)
        {
            Result<MstBeneficiarios> result = new Result<MstBeneficiarios>();

            try
            {
                result.MessageType = MessageType.INFO;
                result.Message = Messages.SuccessGetResult;
                result.Data = Repository.GetById(idBeneficiary); 
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase InsertBeneficiary(MstBeneficiarios beneficiary)
        {
            ResultBase result = new ResultBase();

            try
            { 
                    var resultValidate = Utilities.ValidateEntity(beneficiary);

                    if (resultValidate.MessageType == MessageType.WARNING)
                        result = resultValidate; 
                    else
                    {
                        var temp = Repository.Get(b => b.Nombre.Equals(beneficiary.Nombre));

                        if (temp == null)
                        {
                            var sequencesService = new SequenceService(_unitOfWork.GetRepository<MstSecuencias>());

                            MstSecuencias seq = sequencesService.GetNextSequenceByTableName("[pro].[MstBeneficiarios]");

                            //beneficiary.IdUsuarioIns = 1;
                            beneficiary.FechaIns = DateTime.Now;

                            beneficiary.IdBeneficiario = seq.SecuencialTabla;

                            Repository.Add(beneficiary);

                            sequencesService.InsertSequence(seq);

                            if (!CommitExternal)
                                _unitOfWork.Commit();
                        }
                        else
                        { 
                            result.MessageType = MessageType.WARNING;
                            result.Message = "El beneficiario especificado ya existe.";
                            result.DetailException = new Exception(result.Message);
                        }
                    } 

                result.Message = Messages.SuccessSaveData;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorSaveData, "beneficiario");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase UpdateBeneficiary(MstBeneficiarios beneficiary)
        {
            ResultBase result = new ResultBase();

            try
            {
                    var temp = Repository.Get(b => b.IdBeneficiario == beneficiary.IdBeneficiario);

                    if (temp != null)
                    {

                        var resultValidate = Utilities.ValidateEntity(beneficiary);

                        if (resultValidate.MessageType == MessageType.WARNING)
                            result = resultValidate;
                        else
                        {
                        beneficiary.IdUsuarioUpd = 1;
                        beneficiary.FechaUpd = DateTime.Now;

                           Repository.Update(beneficiary); 

                            _unitOfWork.Commit();
                        }
                    }
                    
                result.Message = Messages.SuccessUpdateData;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorUpdateData, "beneficiario");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }
    }
}
