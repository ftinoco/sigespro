﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sigespro.Web.Mvc.Utilities.Attributes.Security;
using Sigespro.Web.Mvc.Utilities.Security;
using Sigespro.Service.Admin;
using Common.Helpers;
using Sigespro.Web.Mvc.Models.Administration;
using Common.Log;
using Newtonsoft.Json;
using Sigespro.Entity;
using System.Reflection;
using Sigespro.Web.Mvc.Resources;
using Sigespro.Service.Security;
using Sigespro.Entity.Custom;

namespace Sigespro.Web.Mvc.Controllers.Administration
{
    [RoutePrefix("Administration/Roles")]
    public class RoleController : Controller
    {
        private readonly IUserSession _userSession;

        public RoleService Service { get; set; }

        public RoleController()
        {
            Service = new RoleService();
            _userSession = new UserSession();
        }

        [Route]
        public ActionResult Index(QueryOptions<IEnumerable<RoleViewModel>> queryOpt)
        {
            string message = string.Empty;
            IEnumerable<RoleViewModel> model = new List<RoleViewModel>();

            if (string.IsNullOrEmpty(queryOpt.SortField))
                queryOpt.SortOrder = SortingOrder.DESC;

            string strSerialize = JsonConvert.SerializeObject(queryOpt);

            QueryOptions<IEnumerable<Roles>> filter = JsonConvert.DeserializeObject<QueryOptions<IEnumerable<Roles>>>(strSerialize);
            Service.GetRolesForList(ref filter);

            if (filter.ResultData.MessageType == MessageType.ERROR)
            {
                message = filter.ResultData.Message;
                Logger.Log(filter.ResultData.MessageType, filter.ResultData.DetailException, _userSession.Username,
                           MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }
            else
            {
                strSerialize = JsonConvert.SerializeObject(filter);

                queryOpt = JsonConvert.DeserializeObject<QueryOptions<IEnumerable<RoleViewModel>>>(strSerialize);

                model = queryOpt.ResultData.Data;
            }

            if (!string.IsNullOrEmpty(message))
                ModelState.AddModelError(Messages.AlertDanger, message);

            ViewBag.Filter = true;
            ViewBag.QueryOptions = queryOpt;

            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [Route("Add")]
        public ActionResult AddRole()
        {
            ViewBag.IsEdit = false;
            ViewBag.Action = "SaveRole";
            ViewBag.Title = "Nuevo Rol";

            return View();
        }

        [Route("Edit/{RoleId}")]
        public ActionResult EditRole(int RoleId)
        {
            string message = string.Empty;
            RoleViewModel model = new RoleViewModel();

            try
            {
                var result = Service.GetRolesById(RoleId);

                if (result.MessageType == MessageType.ERROR)
                {
                    message = result.Message;
                    Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                                       MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                {
                    model = new RoleViewModel
                    {
                        Description = result.Data.Description,
                        RoleId = result.Data.RoleId,
                        RoleName = result.Data.RoleName,
                        Active = result.Data.Active
                    };
                }
            }
            catch (Exception ex)
            {
                message = Messages.ErrorApi;
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                           MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            if (!string.IsNullOrEmpty(message))
                ModelState.AddModelError(Messages.AlertDanger, message);

            ViewBag.IsEdit = true;
            ViewBag.Action = "UpdateRole";
            ViewBag.Title = "Editar Rol";

            return View("AddRole", model);
        }

        [Route("Add/Actions/{RoleId}")]
        public ActionResult AddActions(int RoleId)
        {
            string message = string.Empty;
            List<RoleAssignmentViewModel> listAsignment = new List<RoleAssignmentViewModel>();

            try
            {
                SecurityService secService = new SecurityService();

                var resultActions = secService.GetAllActions();

                if (resultActions.MessageType != MessageType.INFO)
                {
                    //if (resultActions.DetailException != null)
                    //{
                    //    Logger.Log(MessageType.ERROR, resultActions.DetailException, _userSession.Username,
                    //             MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                    //    message = Messages.LoadPageError;
                    //}
                    //goto @return;
                }

                var resultActionsByRole = secService.GetActionListByRole(RoleId);

                if (resultActionsByRole.MessageType != MessageType.SUCCESS)
                {
                    //if (resultActionsByRole.DetailException != null)
                    //{
                    //    Logger.Log(MessageType.ERROR, resultActionsByRole.DetailException, _userSession.Username,
                    //                 MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);

                    //    message = Messages.LoadPageError;
                    //}
                    //goto @return;
                }

                foreach (var item in resultActions.Data)
                {
                    RoleAssignmentViewModel assignment = new RoleAssignmentViewModel
                    {
                        AssignmentID = item.ActionId,
                        Description = item.Description,
                        RoleID = RoleId
                    };

                    if (resultActionsByRole.Data != null && resultActionsByRole.Data.Any(r => r.ActionId == item.ActionId))
                        assignment.IsChecked = true;

                    listAsignment.Add(assignment);
                }

            }
            catch (Exception ex)
            {
                message = Messages.ErrorApi;
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                             MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

        //@return:
            ViewBag.Title = "Vincular Acciones";
            ViewBag.Action = "SaveActions";

            if (!string.IsNullOrEmpty(message))
                ModelState.AddModelError(Messages.AlertDanger, message);

            return View("AddAssignment", listAsignment);
        }

        [Route("Add/Options/{RoleId}")]
        public ActionResult AddOptions(int RoleId)
        {
            string message = string.Empty;
            List<RoleAssignmentViewModel> listAsignment = new List<RoleAssignmentViewModel>();

            try
            {
                SecurityService secService = new SecurityService();

                var resultActions = secService.GetAllMenuItem();

                if (resultActions.MessageType != MessageType.INFO)
                {
                    //if (resultActions.DetailException != null)
                    //{
                    //    Logger.Log(MessageType.ERROR, resultActions.DetailException, _userSession.Username,
                    //            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                    //    message = Messages.LoadPageError;
                    //}
                    //goto @return;
                }

                var resultMenuItemByRole = secService.GetMenuItemByRole(RoleId);

                if (resultMenuItemByRole.MessageType != MessageType.SUCCESS)
                {
                    //if (resultMenuItemByRole.DetailException != null)
                    //{
                    //    Logger.Log(MessageType.ERROR, resultMenuItemByRole.DetailException, _userSession.Username,
                    //             MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);

                    //    message = Messages.LoadPageError;
                    //}

                    //goto @return;
                }

                foreach (var item in resultActions.Data)
                {
                    RoleAssignmentViewModel assignment = new RoleAssignmentViewModel
                    {
                        AssignmentID = item.MenuItemId,
                        Description = item.Label,
                        RoleID = RoleId
                    };

                    if (resultMenuItemByRole.Data != null && resultMenuItemByRole.Data.Any(r => r.MenuItemId == item.MenuItemId))
                        assignment.IsChecked = true;

                    listAsignment.Add(assignment);
                }

            }
            catch (Exception ex)
            {
                message = Messages.ErrorApi;
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                             MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

        //@return:
            ViewBag.Title = "Vincular Opciones de Menú";
            ViewBag.Action = "SaveOptions";

            if (!string.IsNullOrEmpty(message))
                ModelState.AddModelError(Messages.AlertDanger, message);

            return View("AddAssignment", listAsignment);
        }

        [Route("Save"), HttpPost]
        [AllowAnonymous, AjaxOnly]
        public ActionResult SaveRole(RoleViewModel model)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                model.UserInsert = _userSession.UserId;
                string strSerialize = JsonConvert.SerializeObject(model);

                var result = Service.InsertRole(JsonConvert.DeserializeObject<Roles>(strSerialize));

                if (result.MessageType != MessageType.SUCCESS)
                {
                    message = result.Message;
                    status = result.MessageType.ToString();
                    Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                    message = string.Format(Messages.SaveSuccess, "rol");
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                             MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message, action = Url.Action("Index", "Role") },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [AllowAnonymous, HttpPost]
        [Route("Update"), AjaxOnly]
        public ActionResult UpdateRole(RoleViewModel model)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                model.UserUpdate = _userSession.UserId;
                string strSerialize = JsonConvert.SerializeObject(model);

                var result = Service.UpdateRole(JsonConvert.DeserializeObject<Roles>(strSerialize));

                if (result.MessageType != MessageType.SUCCESS)
                {
                    message = result.Message;
                    status = result.MessageType.ToString();
                    Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                    message = string.Format(Messages.UpdateSuccess, "rol");
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                             MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message, action = Url.Action("Index", "Role") },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [AllowAnonymous, HttpPost]
        [Route("Save/Actions"), AjaxOnly]
        public ActionResult SaveActions(List<RoleAssignmentViewModel> model)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                string strSerialize = JsonConvert.SerializeObject(model);

                // Se pone en 1 el parametro del id del sistema porque solamente un registro deberia existir
                var result = Service.InsertRoleAction(JsonConvert.DeserializeObject<List<RoleAssignment>>(strSerialize), 1);

                if (result.MessageType != MessageType.SUCCESS)
                {
                    message = result.Message; 
                    status = result.MessageType.ToString();
                    Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                    message = "Las acciones fueron vinculada al rol exitosamente";
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                             MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message, action = Url.Action("Index", "Role") },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [AllowAnonymous, HttpPost]
        [Route("Save/Options"), AjaxOnly]
        public ActionResult SaveOptions(List<RoleAssignmentViewModel> model)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                string strSerialize = JsonConvert.SerializeObject(model);

                // Se pone en 1 el parametro del id del sistema porque solamente un registro deberia existir
                var result = Service.InsertRoleMenuItem(JsonConvert.DeserializeObject<List<RoleAssignment>>(strSerialize), 1);

                if (result.MessageType != MessageType.SUCCESS)
                {
                    message = result.Message;
                    status = result.MessageType.ToString();
                    Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                    message = "Las opciones de menú fueron vinculada al rol exitosamente";
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                             MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message, action = Url.Action("Index", "Role") },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

    }
}