﻿using System;
using System.Text.RegularExpressions;

namespace Common.Extensions
{
    public static class StringExtension
    {
        public static bool IsDate(this string str)
        {
            DateTime Temp;
           
            if (DateTime.TryParse(str, out Temp))
                return true;
            else
                return false;
        }

        public static bool CompareWith(this string str, string strToCompare)
        {
            string cleanStr = str.ToLower().Trim().Replace(" ", string.Empty);

            string cleanStrToCompare = strToCompare.ToLower().Trim().Replace(" ", string.Empty);

            return cleanStr.Equals(cleanStrToCompare);
        }

        public static bool IsEmail(this string email)
        {
            Regex rgx = new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$");
            return rgx.IsMatch(email.Replace("-", string.Empty));
        }

        public static string ClearString(this string str)
        {
            string clearString = str.Replace('á', 'a').Replace('Á', 'A')
                                    .Replace('é', 'e').Replace('É', 'E');

            clearString = clearString.Replace('í', 'i').Replace('Í', 'I')
                                     .Replace('ó', 'o').Replace('Ó', 'O');

            return clearString.Replace('ú', 'u').Replace('Ú', 'U')
                              .Replace('ñ', 'n').Replace('Ñ', 'N');
        }
    }
}
