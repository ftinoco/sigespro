﻿using Sigespro.Entity;
using System;

namespace Sigespro.Entity.Custom
{
    public class Project
    {
        public int IdProyecto { get; set; }
         
        public string TituloProyecto { get; set; }
         
        public string DescProyecto { get; set; }
         
        public string Justificacion { get; set; }

        public DateTime FechaInicio { get; set; }

        public DateTime FechaFinEstimada { get; set; }

        public DateTime FechaLimite { get; set; }

        public decimal DuraciónEstimadaDias { get; set; }

        public decimal DuracionEstimadaHoras { get; set; }

        public int HorasDedicadas { get; set; }

        public bool Externo { get; set; }

        public decimal Presupuesto { get; set; }

        public decimal PresupuestoActual { get; set; }

        public decimal GastoEstimado { get; set; }

        public DetCatalogo TipoProyecto { get; set; }

        public DetCatalogo Estado { get; set; }

        public int IdUsuarioIns { get; set; }

        public DateTime FechaIns { get; set; }

        public int? IdUsuarioUpd { get; set; }

        public DateTime? FechaUpd { get; set; }

        public string Beneficiario { get; set; }

        public virtual DetProyectoBeneficiarios DetProyectoBeneficiarios { get; set; }

    }
}
