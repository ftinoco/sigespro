﻿using Sigespro.Web.Mvc.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sigespro.Web.Mvc.Models
{
    public class LoginModel
    {
        /// <summary>
        /// Get/Set for password
        /// </summary>
        [Display(Name = "Password", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string Password { get; set; }
        /// <summary>
        /// Get/Set for UserName
        /// </summary>
        [Display(Name = "UserName", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string UserName { get; set; }
    }
}