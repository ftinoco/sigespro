﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace Sigespro.Web.Mvc
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Content/Scripts/libs/jquery/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Content/Scripts/libs/jquery/jquery.validate*",
                        "~/Content/Scripts/libs/jquery/jquery.unobtrusive*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Content/Scripts/libs/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Content/Scripts/libs/bootstrap.js",
                      "~/Content/Scripts/libs/respond.min.js",
                      "~/Content/Scripts/libs/moment-with-locales.min.js",
                      "~/Content/Scripts/js/ripples/ripples.min.js",
                      "~/Content/Scripts/js/material/material.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/custom").Include(
                        "~/Content/Scripts/app/app.js",
                        "~/Content/Scripts/app/sigespro.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular_src").Include(
                      "~/Content/Scripts/app/angular-src/runtime.js",
                      "~/Content/Scripts/app/angular-src/polyfills.js",
                      "~/Content/Scripts/app/angular-src/styles.js",
                      "~/Content/Scripts/app/angular-src/vendor.js",
                      "~/Content/Scripts/app/angular-src/main.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/Styles/fonts.css",
                        "~/Content/Styles/bootstrap.min.css",
                        "~/Content/Styles/sidebar.css",
                        "~/Content/Styles/material/bootstrap-material-design.css",
                        "~/Content/Styles/ripples/ripples.css",
                        "~/Content/Styles/site.css",
                        "~/Content/Styles/animate.css",
                        "~/Content/Styles/select2/select2.min.css",
                        "~/Content/Styles/bootstrap-datepicker/bootstrap-datepicker3.standalone.min.css",
                        "~/Content/Styles/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css"));


            BundleTable.EnableOptimizations = false;
        }
    }
}