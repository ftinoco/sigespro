﻿using System;
using System.Collections.Generic;
using System.Configuration;
using static Common.Helpers.Utilities;

namespace Common.Helpers
{
    public class QueryOptions<T> where T : class
    {
        public QueryOptions()
        {
            CurrentPageIndex = 0;

            SortOrder = SortingOrder.ASC;

            RowsPerPage = Convert.ToInt32(ConfigurationManager.AppSettings["RowsPerPage"]);

            if (IsNull(Filters))
                Filters = new List<QueryOptionsFilter>();
        }

        public long PageCount
        {
            get { return Convert.ToInt64(Math.Ceiling(((double)TotalRecords / (double)RowsPerPage))); }
        }

        public bool IsPaging { get; set; }

        public int RowsPerPage { get; set; }

        public string SortField { get; set; }

        public long TotalRecords { get; set; }

        public QueryOptions(string sortField) : this()
        {
            SortField = sortField;
        }

        public Result<T> ResultData { get; set; }

        public int CurrentPageIndex { get; set; }

        public SortingOrder SortOrder { get; set; }

        public IList<QueryOptionsFilter> Filters { get; set; }

    }

    public class QueryOptionsFilter
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }
}
