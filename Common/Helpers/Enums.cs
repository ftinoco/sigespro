﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Common.Helpers
{
    public enum SortingOrder
    {
        ASC,
        DESC
    }

    public enum MessageType
    {
        INFO,
        ERROR,
        SUCCESS,
        WARNING
    }
}
