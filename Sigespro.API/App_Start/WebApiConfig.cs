﻿using Newtonsoft.Json;
//using Sigespro.API.Utilities.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Sigespro.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            //config.Filters.Add(new ApiAuthenticationFilter());

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}/{id2}",
                defaults: new { id = RouteParameter.Optional, id2 = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "ActionApi",
                routeTemplate: "api/{controller}/{action}/{id}/{id2}",
                defaults: new { id = RouteParameter.Optional, id2 = RouteParameter.Optional },
                constraints: new { action = @"^[a-z]+$" }
            );

            config.Routes.MapHttpRoute(
                name: "Pseudonym",
                routeTemplate: "api/{controller}/{pseudonym}/{action}/{id}/{id2}",
                defaults: new { id = RouteParameter.Optional, id2 = RouteParameter.Optional },
                constraints: new { pseudonym = "\\[a-zA-Z] +", action = @"^[a-z]+$" }
            );

            config.Routes.MapHttpRoute(
                name: "Custom",
                routeTemplate: "api/{controller}/{module}/{idmodule}/{submodule}/{action}/{idsubmodule}",
                defaults: new { action = RouteParameter.Optional, idsubmodule = RouteParameter.Optional },
                constraints: new { module = "\\[a-zA-Z] +", submodule = "\\[a-zA-Z] +" }
           );

           // config.Filters.Add(new Utilities.AuthCustomAttribute());
            //config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            config.Formatters.Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter);
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            UnityConfig.RegisterComponents(config);

        }
    }
}
