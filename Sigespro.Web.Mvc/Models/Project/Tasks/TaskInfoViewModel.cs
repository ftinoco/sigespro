﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sigespro.Web.Mvc.Models.Project.Tasks
{
    public class TaskInfoViewModel: Entity.Custom.TaskInfo
    {  
        public HttpPostedFileBase[] files { get; set; }
    }
}