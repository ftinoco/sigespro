﻿using Common.Helpers;
using Common.Log;
using Newtonsoft.Json;
using Sigespro.Entity;
using Sigespro.Service.Pre;
using Sigespro.Service.Pro;
using Sigespro.Web.Mvc.Models.Project.Deliverables;
using Sigespro.Web.Mvc.Resources;
using Sigespro.Web.Mvc.Utilities.Attributes.Security;
using Sigespro.Web.Mvc.Utilities.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace Sigespro.Web.Mvc.Controllers.Project
{
    [RoutePrefix("Phase")]
    public class DeliverablesController : Controller
    {
        private readonly IUserSession _userSession;

        public DeliverableService Service { get; set; }

        public DeliverablesController()
        {
            Service = new DeliverableService();
            _userSession = new UserSession();
        }

        [AllowAnonymous]
        [Route("Add/{projectID}")]
        public ActionResult AddPartial(int projectID)
        {
            DateTime startDate = DateTime.Now, endDate = DateTime.Now;
            DeliverableViewModel model = new DeliverableViewModel();

            model.Nota = null;
            model.Descripcion = string.Empty;

            model.IdProyecto = projectID;

            model.FechaInicio = DateTime.Now;

            try
            {
                ProjectService prService = new ProjectService();
                var resultProject = prService.GetProjectById(projectID);

                // Se establece por defecto la fecha inicio del proyecto
                model.FechaInicio = resultProject.Data.FechaInicio;

                startDate = resultProject.Data.FechaInicio;
                endDate = resultProject.Data.FechaLimite;

                // Se obtiene el listado de entregables por id de proyecto
                var result = Service.GetDeliverablesByProjectID(projectID);

                if (result.MessageType == MessageType.INFO)
                {
                    if (result.Data != null)
                    {
                        if (result.Data.Count() > 0)
                        {
                            var last = result.Data.ToList().OrderByDescending(e => e.IdEntregable).FirstOrDefault();

                            // Si la fecha fin del último entregable es viernes, se le suman 3 día para el inicio del nuevo entregable
                            if (last.FechaFin.DayOfWeek == DayOfWeek.Friday)
                                model.FechaInicio = last.FechaFin.AddDays(3);
                            // Si es sabado se le agregan dos días
                            else if (last.FechaFin.DayOfWeek == DayOfWeek.Saturday)
                                model.FechaInicio = last.FechaFin.AddDays(2);
                            else // De lo contrario se le agrega un día
                                model.FechaInicio = last.FechaFin.AddDays(1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            model.FechaFin = model.FechaInicio;

            ViewBag.StartDate = startDate;
            ViewBag.EndDate = endDate;
            ViewBag.Action = "Save";

            return PartialView("Partials/Deliverables/_addDeliverable", model);
        }

        [AllowAnonymous]
        [Route("Edit/{deliverableID}/{projectID}")]
        public ActionResult EditPartial(int deliverableID, int projectID)
        {
            DeliverableViewModel model = new DeliverableViewModel();
            DateTime startDate = DateTime.Now, endDate = DateTime.Now;

            try
            {
                var result = Service.GetDeliverableByID(deliverableID, projectID);

                if (result.MessageType == MessageType.INFO)
                {
                    model = new DeliverableViewModel
                    {
                        Descripcion = result.Data.Descripcion,
                        DuracionEstimadaDias = result.Data.DuracionEstimadaDias,
                        DuracionEstimadaHoras = result.Data.DuracionEstimadaHoras,
                        FechaFin = result.Data.FechaFin,
                        FechaInicio = result.Data.FechaInicio,
                        FechaIns = result.Data.FechaIns,
                        FechaUpd = result.Data.FechaUpd,
                        HorasDedicadas = result.Data.HorasDedicadas,
                        IdEntregable = result.Data.IdEntregable,
                        IdEstado = result.Data.IdEstado,
                        IdProyecto = result.Data.IdProyecto,
                        IdUsuarioIns = result.Data.IdUsuarioIns,
                        IdUsuarioUpd = result.Data.IdUsuarioUpd,
                        Nota = result.Data.Nota,
                        Presupuesto = result.Data.MstPresupuesto.FirstOrDefault().PresupuestoAsignado
                    };

                    ProjectService prService = new ProjectService();
                    var resultProject = prService.GetProjectById(projectID);

                    startDate = resultProject.Data.FechaInicio;
                    endDate = resultProject.Data.FechaLimite;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            ViewBag.StartDate = startDate;
            ViewBag.EndDate = endDate;
            ViewBag.Action = "Update";

            return PartialView("Partials/Deliverables/_addDeliverable", model);
        }

        [AjaxOnly]
        [HttpPost]
        [Route("Save")]
        [AllowAnonymous]
        public ActionResult Save(DeliverableViewModel model)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                if (ModelState.IsValid)
                {
                    model.IdEstado = 1;
                    model.IdUsuarioIns = _userSession.UserId;

                    string strSerialize = JsonConvert.SerializeObject(model);

                    var result = Service.InsertDeliverable(JsonConvert.DeserializeObject<Entity.Custom.Deliverable>(strSerialize));

                    if (result.MessageType != MessageType.SUCCESS)
                    {
                        message = result.Message;
                        status = result.MessageType.ToString();
                        Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                                MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                    }
                    else
                        message = string.Format(Messages.SaveSuccess, "fase de proyecto");
                }
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [AjaxOnly]
        [HttpPost]
        [Route("Update")]
        [AllowAnonymous]
        public ActionResult Update(DeliverableViewModel model)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                if (ModelState.IsValid)
                {
                    model.IdUsuarioUpd = _userSession.UserId;

                    string strSerialize = JsonConvert.SerializeObject(model);

                    var result = Service.UpdateDeliverable(JsonConvert.DeserializeObject<Entity.Custom.Deliverable>(strSerialize));

                    if (result.MessageType != MessageType.SUCCESS)
                    {
                        message = result.Message;
                        status = result.MessageType.ToString();
                        Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                                MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                    }
                    else
                        message = string.Format(Messages.UpdateSuccess, "fase de proyecto");
                }
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [AjaxOnly]
        [HttpPost]
        [AllowAnonymous]
        [Route("Delete/{deliverableID}/{projectID}")]
        public ActionResult Delete(int deliverableID, int projectID)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                var result = Service.DeleteDeliverable(deliverableID, projectID);

                if (result.MessageType != MessageType.SUCCESS)
                {
                    message = result.Message;
                    status = result.MessageType.ToString();
                    Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                    message = string.Format(Messages.DeleteSuccess, "fase de proyecto");
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        #region Presupuesto

        [AllowAnonymous]
        [Route("Budget/{deliverableID}/{projectID}")]
        public ActionResult Budget(int deliverableID, int projectID)
        {
            DeliverableViewModel model = new DeliverableViewModel();
            ViewBag.Title = "Presupuestar Fase de Proyecto";

            try
            {
                var result = Service.GetDeliverableByID(deliverableID, projectID);

                if (result.MessageType == MessageType.INFO)
                {
                    var deliverable = result.Data;

                    model = new DeliverableViewModel
                    {
                        Descripcion = deliverable.Descripcion,
                        DuracionEstimadaDias = deliverable.DuracionEstimadaDias,
                        DuracionEstimadaHoras = deliverable.DuracionEstimadaHoras,
                        FechaFin = deliverable.FechaFin,
                        FechaInicio = deliverable.FechaInicio,
                        FechaIns = deliverable.FechaIns,
                        FechaUpd = deliverable.FechaUpd,
                        HorasDedicadas = deliverable.HorasDedicadas,
                        IdEntregable = deliverable.IdEntregable,
                        IdEstado = deliverable.IdEstado,
                        IdProyecto = deliverable.IdProyecto,
                        IdUsuarioIns = deliverable.IdUsuarioIns,
                        IdUsuarioUpd = deliverable.IdUsuarioUpd,
                        Nota = deliverable.Nota
                    };

                    if (deliverable.MstPresupuesto != null && deliverable.MstPresupuesto.Count > 0)
                    {
                        var budget = deliverable.MstPresupuesto.FirstOrDefault();

                        model.Presupuesto = budget.PresupuestoAsignado;
                        model.PresupuestoID = budget.IdPresupuesto;
                        model.FinancialBudget = new List<Models.Budget.BudgetModel>();
                        model.PhysicalBudget = new List<Models.Budget.PhysicalBudget>();

                        int i = 1;
                        if (budget.MstPresupuestoFinanciero != null && budget.MstPresupuestoFinanciero.Count > 0)
                        {
                            foreach (var item in budget.MstPresupuestoFinanciero)
                            {
                                model.FinancialBudget.Add(new Models.Budget.BudgetModel()
                                {
                                    BudgetMount = item.MontoPresupuestario,
                                    BudgetParentID = budget.IdPresupuesto,
                                    BudgetPK = item.IdPresupuestoFinanciero,
                                    EntryID = item.IdRubro,
                                    EntryName = item.IdRubro + " - " + item.CatRubros.Nombre,
                                    TaskID = item.IdTarea,
                                    TaskName = item.MstTareas.Nombre,
                                    BudgetID = i,
                                    Deleted = !item.Activo
                                });
                                i++;
                            }
                        }

                        i = 1;
                        if (budget.MstPresupuestoFisico != null && budget.MstPresupuestoFisico.Count > 0)
                        {
                            foreach (var item in budget.MstPresupuestoFisico)
                            {
                                model.PhysicalBudget.Add(new Models.Budget.PhysicalBudget()
                                {
                                    BudgetID = i,
                                    ArticleID = item.IdArticulo,
                                    ArticleName = item.CatArticulos.Descripcion,
                                    BudgetMount = (item.CantidadArticulos * item.CostoArticulos),
                                    BudgetParentID= budget.IdPresupuesto,
                                    BudgetPK = item.IdPresupuestoFisico,
                                    Cost = item.CostoArticulos,
                                    EntryID = item.IdRubro,
                                    EntryName = item.IdRubro + " - " + item.CatRubros.Nombre,
                                    TaskID = item.IdTarea,
                                    TaskName = item.MstTareas.Nombre,
                                    Quantity = item.CantidadArticulos,
                                    Deleted = !item.Activo
                                });
                            i++;
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, 
                    MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return View(model);
        }

        [AjaxOnly]
        [HttpPost]
        [AllowAnonymous]
        [Route("Budget/save")]
        public ActionResult SaveBudget(DeliverableViewModel model)
        {
            string message = string.Empty, status = "SUCCESS";
            try
            {
                if (ModelState.IsValid)
                {
                    List<MstPresupuestoFisico> lstPhysicalBudget = new List<MstPresupuestoFisico>(); ;
                    List<MstPresupuestoFinanciero> lstFinancialBudget = new List<MstPresupuestoFinanciero>(); ;

                    if (model.FinancialBudget != null && model.FinancialBudget.Count > 0)
                    {
                        foreach (var item in model.FinancialBudget)
                        {
                            if (item.BudgetPK > 0)
                            {
                                lstFinancialBudget.Add(new MstPresupuestoFinanciero
                                {
                                    IdPresupuestoFinanciero = item.BudgetPK,
                                    Activo = !item.Deleted,
                                    IdEntregable = model.IdEntregable,
                                    IdPresupuesto = model.PresupuestoID,
                                    IdProyecto = model.IdProyecto,
                                    IdRubro = item.EntryID,
                                    IdTarea = item.TaskID,
                                    IdUsuarioUpd = _userSession.UserId,
                                    MontoPresupuestario = item.BudgetMount
                                });
                            }
                            else
                            {
                                if (!item.Deleted)
                                {
                                    lstFinancialBudget.Add(new MstPresupuestoFinanciero
                                    {
                                        IdEntregable = model.IdEntregable,
                                        IdPresupuesto = model.PresupuestoID,
                                        IdProyecto = model.IdProyecto,
                                        IdRubro = item.EntryID,
                                        IdTarea = item.TaskID,
                                        IdUsuarioIns = _userSession.UserId,
                                        MontoPresupuestario = item.BudgetMount
                                    });
                                }
                            }
                        }
                    }

                    if (model.PhysicalBudget != null && model.PhysicalBudget.Count > 0)
                    {
                        foreach (var item in model.PhysicalBudget)
                        {
                            if (item.BudgetPK > 0)
                            {
                                lstPhysicalBudget.Add(new MstPresupuestoFisico
                                {
                                    IdPresupuestoFisico = item.BudgetPK,
                                    Activo = !item.Deleted,
                                    IdEntregable = model.IdEntregable,
                                    IdPresupuesto = model.PresupuestoID,
                                    IdProyecto = model.IdProyecto,
                                    IdRubro = item.EntryID,
                                    IdTarea = item.TaskID,
                                    IdUsuarioUpd = _userSession.UserId,
                                    CantidadArticulos = item.Quantity,
                                    CostoArticulos = item.Cost,
                                    IdArticulo = item.ArticleID
                                });
                            }
                            else
                            {
                                if (!item.Deleted)
                                {
                                    lstPhysicalBudget.Add(new MstPresupuestoFisico
                                    {
                                        IdEntregable = model.IdEntregable,
                                        IdPresupuesto = model.PresupuestoID,
                                        IdProyecto = model.IdProyecto,
                                        IdRubro = item.EntryID,
                                        IdTarea = item.TaskID,
                                        IdUsuarioIns = _userSession.UserId,
                                        CantidadArticulos = item.Quantity,
                                        CostoArticulos = item.Cost,
                                        IdArticulo = item.ArticleID
                                    });
                                }
                            }
                        }
                    }

                    BudgetService _service = new BudgetService();
                    var result = _service.InsertBudgetDatail(model.IdEntregable, model.IdProyecto, lstFinancialBudget, lstPhysicalBudget);

                    if (result.MessageType != MessageType.SUCCESS)
                    {
                        message = result.Message;
                        status = result.MessageType.ToString();
                        Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                                MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                    }
                    else
                        message = string.Format(Messages.SaveSuccess, "presupuesto");
                }
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                    MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new
                {
                    status,
                    message,
                    action = Url.Action("Budget", "Deliverables", new
                    {
                        deliverableID = model.IdEntregable,
                        projectID = model.IdProyecto
                    })
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        
        #region Presupuesto Financiero

        [AllowAnonymous]
        public ActionResult FinancialBudget(DeliverableViewModel model)
        {
            return PartialView("Partials/Deliverables/_financialBudget", model);
        }

        [AllowAnonymous]
        public ActionResult AddFinancialBudget(DeliverableViewModel model, int budgetParentID = 0, int budgetID = 0, bool saved = false)
        {
            if (model.FinancialBudget == null)
                model.FinancialBudget = new List<Models.Budget.BudgetModel>();

            if (!model.FinancialBudget.Any(f => f.IsEditable))
            {
                if (budgetParentID > 0 && budgetID > 0)
                {
                    var budget = model.FinancialBudget.FirstOrDefault(f => f.BudgetParentID == budgetParentID && f.BudgetID == budgetID);

                    if (budget != null)
                        budget.IsEditable = !saved;
                }
                else
                {
                    if (model.FinancialBudget.Count > 0)
                        budgetID = model.FinancialBudget.Max(p => p.BudgetID) + 1;
                    else
                        budgetID = 1;

                    model.FinancialBudget.Add(new Models.Budget.BudgetModel
                    {
                        BudgetParentID = model.PresupuestoID,
                        BudgetID = budgetID,
                        IsEditable = true
                    });
                }
            }

            return PartialView("Partials/Deliverables/_financialBudget", model);
        }

        [AllowAnonymous]
        public ActionResult EditFinancialBudget(DeliverableViewModel model, int index)
        {
            var taskService = new TaskService();
            List<SelectListItem> selectListItem = new List<SelectListItem>();
            var budget = model.FinancialBudget[index];

            // Cargar combos
            var tasksByDeliverable = taskService.GetTasksByDeliverable(model.IdEntregable);

            if (tasksByDeliverable.MessageType == MessageType.INFO)
            {
                int taskID = tasksByDeliverable.Data.ToList().FirstOrDefault().TaskID;
                foreach (var t in tasksByDeliverable.Data.ToList())
                {
                    if (t.TaskID == budget.TaskID)
                        taskID = budget.TaskID;

                    budget.TaskName = t.TaskName;

                    selectListItem.Add(new SelectListItem
                    {
                        Value = t.TaskID.ToString(),
                        Text = t.TaskName
                    });
                }

                ViewBag.ListOfTasks = new SelectList(selectListItem, "Value", "Text", taskID);
            }

            selectListItem = new List<SelectListItem>();

            // Se obtienen los rubro que sean de tipo "Grupo de Rubros"
            Service.Pre.EntryService eService = new Service.Pre.EntryService();
            var result = eService.GetEntriesByType(0.ToString(), "OtrosRubros");

            if (result.MessageType == MessageType.INFO)
            {
                string entryID = string.Empty;
                foreach (var item in result.Data.ToList())
                {
                    if (item.IdRubro == budget.EntryID)
                        entryID = budget.EntryID;
                    else if (item.IdRubro == result.Data.ToList().FirstOrDefault().IdRubro)
                        entryID = item.IdRubro;

                    budget.EntryName = item.Nombre;
                    selectListItem.Add(new SelectListItem
                    {
                        Value = item.IdRubro.ToString(),
                        Text = item.IdRubro + " - " + item.Nombre
                    });
                }
                ViewBag.EntryGroups = new SelectList(selectListItem, "Value", "Text", entryID);
            }

            ViewBag.Index = index;

            return PartialView("Partials/Deliverables/_addfinancialBudget", model);
        }

        [AllowAnonymous]
        public ActionResult DeleteFinancialBudget(DeliverableViewModel model, int budgetParentID, int budgetID)
        {
            var budget = model.FinancialBudget.FirstOrDefault(f => f.BudgetParentID == budgetParentID && f.BudgetID == budgetID);

            if (budget != null)
                budget.Deleted = true;

            return PartialView("Partials/Deliverables/_financialBudget", model);
        }

        #endregion

        #region Presupuesto Físico

        [AllowAnonymous]
        public ActionResult PhysicalBudget(DeliverableViewModel model)
        {
            return PartialView("Partials/Deliverables/_physicalBudget", model);
        }

        [AllowAnonymous]
        public ActionResult AddPhysicalBudget(DeliverableViewModel model, int budgetParentID = 0, int budgetID = 0, bool saved = false)
        {
            if (model.PhysicalBudget == null)
                model.PhysicalBudget = new List<Models.Budget.PhysicalBudget>();

            if (!model.PhysicalBudget.Any(f => f.IsEditable))
            {
                if (budgetParentID > 0 && budgetID > 0)
                {
                    var budget = model.PhysicalBudget.FirstOrDefault(f => f.BudgetParentID == budgetParentID && f.BudgetID == budgetID);

                    if (budget != null)
                        budget.IsEditable = !saved;
                }
                else
                {
                    if (model.PhysicalBudget.Count > 0)
                        budgetID = model.PhysicalBudget.Max(p => p.BudgetID) + 1;
                    else
                        budgetID = 1;

                    model.PhysicalBudget.Add(new Models.Budget.PhysicalBudget
                    {
                        BudgetParentID = model.PresupuestoID,
                        BudgetID = budgetID,
                        IsEditable = true
                    });
                }
            }

            return PartialView("Partials/Deliverables/_physicalBudget", model);
        }

        [AllowAnonymous]
        public ActionResult EditPhysicalBudget(DeliverableViewModel model, int index)
        {
            var taskService = new TaskService();
            List<SelectListItem> selectListItem = new List<SelectListItem>();
            var budget = model.PhysicalBudget[index];

            // Cargar combos
            var tasksByDeliverable = taskService.GetTasksByDeliverable(model.IdEntregable);

            if (tasksByDeliverable.MessageType == MessageType.INFO)
            {
                int taskID = tasksByDeliverable.Data.ToList().FirstOrDefault().TaskID;
                foreach (var t in tasksByDeliverable.Data.ToList())
                {
                    if (t.TaskID == budget.TaskID)
                        taskID = budget.TaskID;

                    budget.TaskName = t.TaskName;

                    selectListItem.Add(new SelectListItem
                    {
                        Value = t.TaskID.ToString(),
                        Text = t.TaskName
                    });
                }

                ViewBag.ListOfTasks = new SelectList(selectListItem, "Value", "Text", taskID);
            }

            selectListItem = new List<SelectListItem>();

            // Se obtienen los rubro que sean de tipo "Grupo de Rubros"
            Service.Pre.EntryService eService = new Service.Pre.EntryService();
            var result = eService.GetEntriesByType(0.ToString(), "RubroArticulo");

            string entryID = string.Empty;
            if (result.MessageType == MessageType.INFO)
            {
                foreach (var item in result.Data.ToList())
                {
                    if (item.IdRubro == budget.EntryID)
                        entryID = budget.EntryID;
                    else if (item.IdRubro == result.Data.ToList().FirstOrDefault().IdRubro)
                        entryID = item.IdRubro;

                    budget.EntryName = item.Nombre;
                    selectListItem.Add(new SelectListItem
                    {
                        Value = item.IdRubro.ToString(),
                        Text = item.IdRubro + " - " + item.Nombre
                    });
                }
                ViewBag.EntryGroups = new SelectList(selectListItem, "Value", "Text", entryID);
            }

            selectListItem = new List<SelectListItem>();

            // Se obtienen los artículos que pertenecen a el rubro seleccionado
            Sigespro.Service.Pre.ArticleService artService = new Sigespro.Service.Pre.ArticleService();
            var resultArt = artService.GetArticleByEnty(entryID);

            if (resultArt.MessageType == MessageType.INFO)
            {
                string articleCode = string.Empty;
                foreach (var item in resultArt.Data.ToList())
                {
                    if (item.IdArticulo == budget.ArticleID)
                        articleCode = budget.ArticleID;
                    else if (item.IdArticulo == resultArt.Data.ToList().FirstOrDefault().IdArticulo)
                        articleCode = item.IdArticulo;

                    budget.ArticleName = item.Descripcion;
                    selectListItem.Add(new SelectListItem
                    {
                        Value = articleCode,
                        Text = item.Descripcion
                    });

                    if (budget.ArticleName != string.Empty)
                        budget.Cost = item.Precio;
                }
                ViewBag.Articles = new SelectList(selectListItem, "Value", "Text", articleCode);
            }

            ViewBag.Index = index;

            return PartialView("Partials/Deliverables/_addPhysicalBudget", model);
        }

        [AllowAnonymous]
        public ActionResult DeletePhysicalBudget(DeliverableViewModel model, int budgetParentID, int budgetID)
        {
            var budget = model.PhysicalBudget.FirstOrDefault(f => f.BudgetParentID == budgetParentID && f.BudgetID == budgetID);

            if (budget != null)
                budget.Deleted = true;

            return PartialView("Partials/Deliverables/_physicalBudget", model);
        }

        #endregion

        #endregion
    }
}