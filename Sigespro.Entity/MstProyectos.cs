//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sigespro.Entity
{
    using System;
    using System.Collections.Generic;
     
    public partial class MstProyectos
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MstProyectos()
        {
            this.DetProgramaProyecto = new HashSet<DetProgramaProyecto>();
            this.DetProyectoResponsable = new HashSet<DetProyectoResponsable>();
            this.MstMensajes = new HashSet<MstMensajes>();
            this.MstEntregables = new HashSet<MstEntregables>();
            this.DetProyectoObjetivos = new HashSet<DetProyectoObjetivos>();
        }
    
        public int IdProyecto { get; set; }
        public string TituloProyecto { get; set; }
        public string DescProyecto { get; set; }
        public string Justificacion { get; set; }
        public System.DateTime FechaInicio { get; set; }
        public System.DateTime FechaFinEstimada { get; set; }
        public System.DateTime FechaLimite { get; set; }
        public decimal DuracionEstimadaHoras { get; set; }
        public int HorasDedicadas { get; set; }
        public bool Externo { get; set; }
        public decimal Presupuesto { get; set; }
        public decimal PresupuestoActual { get; set; }
        public decimal GastoEstimado { get; set; }
        public int IdTipoProyecto { get; set; }
        public int IdEstado { get; set; }
        public int IdUsuarioIns { get; set; }
        public System.DateTime FechaIns { get; set; }
        public Nullable<int> IdUsuarioUpd { get; set; }
        public Nullable<System.DateTime> FechaUpd { get; set; }
        public int DiasLabSem { get; set; }
        public decimal DuracionEstimadaDias { get; set; }
        public int IdMoneda { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetProgramaProyecto> DetProgramaProyecto { get; set; }
        public virtual DetProyectoBeneficiarios DetProyectoBeneficiarios { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetProyectoResponsable> DetProyectoResponsable { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MstMensajes> MstMensajes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MstEntregables> MstEntregables { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetProyectoObjetivos> DetProyectoObjetivos { get; set; }
    }
}
