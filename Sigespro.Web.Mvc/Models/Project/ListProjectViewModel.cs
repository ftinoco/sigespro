﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sigespro.Web.Mvc.Models.Project
{
    public class ListProjectViewModel
    {
        public int IdProyecto { get; set; }

        public System.DateTime FechaInicio { get; set; }

        public System.DateTime FechaLimite { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:#,##0.#0}")]
        public string Presupuesto { get; set; }

        public string DescProyecto { get; set; }

        public string Justificacion { get; set; }

        public string GastoEstimado { get; set; }

        public string TituloProyecto { get; set; }

        public int HorasDedicadas { get; set; }

        public System.DateTime FechaFinEstimada { get; set; }

        public string PresupuestoActual { get; set; }

        public decimal DuracionEstimadaDias { get; set; }

        public decimal DuracionEstimadaHoras { get; set; }

        public int IdTipoProyecto { get; set; }

        public string TipoProyecto { get; set; }

        public int IdEstado { get; set; }

        public string Estado { get; set; }

        public int IdMoneda { get; set; }

        public string GrupoMeta { get; set; }
    }
}