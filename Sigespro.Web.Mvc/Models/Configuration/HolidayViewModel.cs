﻿using Sigespro.Web.Mvc.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sigespro.Web.Mvc.Models.Configuration
{
    public class HolidayViewModel
    {
        public int IdFeriado { get; set; }

        [Display(Name = "Motivo Feriado")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string MotivoFeriado { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Fecha Feriado")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public DateTime FechaFeriado { get; set; }

        [Display(Name = "Active", ResourceType = typeof(Labels))]
        public bool Activo { get; set; }

        [Display(Name = "UserInsert", ResourceType = typeof(Labels))]
        public int IdUsuarioIns { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "DateInsert", ResourceType = typeof(Labels))]
        public DateTime FechaIns { get; set; }

        [Display(Name = "UserEdit", ResourceType = typeof(Labels))]
        public int? IdUsuarioUpd { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "DateEdit", ResourceType = typeof(Labels))]
        public DateTime? FechaUpd { get; set; }
    }
}