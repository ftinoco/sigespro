﻿using Sigespro.Web.Mvc.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sigespro.Web.Mvc.Models.Budget
{
    public class EntriesViewModel
    {
        public string IdRubro { get; set; } 
        
        [Display(Name = "Entry", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string Nombre { get; set; }

        [Display(Name = "EntryType", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public int IdTipoRubro { get; set; }

        public string TipoRubro { get; set; }

        [Display(Name = "EntryGroup", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string IdRubroParent { get; set; }

        public string RubroParent { get; set; }

        [Display(Name = "Active", ResourceType = typeof(Labels))]
        public bool Activo { get; set; }

        [Display(Name = "UserInsert", ResourceType = typeof(Labels))]
        public int IdUsuarioIns { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "DateInsert", ResourceType = typeof(Labels))]
        public DateTime FechaIns { get; set; }

        [Display(Name = "UserEdit", ResourceType = typeof(Labels))]
        public int? IdUsuarioUpd { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "DateEdit", ResourceType = typeof(Labels))]
        public DateTime? FechaUpd { get; set; }
    }
}