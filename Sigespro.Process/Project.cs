﻿using Common.Helpers;
using Common.Log;
using Sigespro.Entity.Resource;
using Sigespro.Service.Pro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Sigespro.Process
{
    public class Project
    {
        public static void UpdateStatusProject()
        {
            try
            {
                ProjectService service = new ProjectService();
                var result = service.GetProjectsPlanned();

                if (result.MessageType != MessageType.INFO)
                {
                    Logger.Log(result.MessageType, result.DetailException, "Proceso",
                            MethodBase.GetCurrentMethod().Name + " - UpdateStatusProject", "SERVER");
                }
                else
                {
                    foreach (var project in result.Data)
                    {
                        if (project.FechaInicio.Date.CompareTo(DateTime.Now.Date) <= 0)
                        { 
                            var resultProjectStatus = service.UpdateProjectStatus(Catalog.ProjectStatusInAction, project);

                            if (resultProjectStatus.MessageType != MessageType.SUCCESS)
                            {
                                Logger.LogWarning("Ocurrió un error al intentar actualizar el estado del proyecto con id: " + project.IdProyecto, "Proceso",
                                    MethodBase.GetCurrentMethod().Name + " - UpdateStatusProject", "SERVER");

                                if (resultProjectStatus.MessageType == MessageType.ERROR)
                                    Logger.Log(MessageType.ERROR, resultProjectStatus.DetailException, "Proceso",
                                        MethodBase.GetCurrentMethod().Name + " - UpdateStatusProject", "SERVER");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, "Proceso",
                    MethodBase.GetCurrentMethod().Name + " - UpdateStatusProject", "SERVER");
            }
        }

    }
}
