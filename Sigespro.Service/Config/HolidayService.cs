﻿using Common.Extensions;
using Common.Helpers;
using Sigespro.DAL.Implementations;
using Sigespro.Entity;
using Sigespro.Entity.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using static Common.Helpers.Utilities;

namespace Sigespro.Service.Admin
{
    public class HolidayService : Service
    {
        private Repository<CatFeriados> Repository { get; set; }

        public HolidayService() : base()
        {
            Repository = _unitOfWork.GetRepository<CatFeriados>();
        }

        public Result<CatFeriados> GetHolidayById(int id)
        {
            Result<CatFeriados> result = new Result<CatFeriados>();

            try
            {
                result.Data = Repository.GetById(id);
                result.MessageType = MessageType.INFO;
                result.Message = Messages.SuccessGetResult;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<IEnumerable<CatFeriados>> GetHolidays()
        {
            Result<IEnumerable<CatFeriados>> result = new Result<IEnumerable<CatFeriados>>();

            try
            {
                result.Data = Repository.GetAll();

                result.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<IEnumerable<CatFeriados>> GetHolidays(string filter)
        {
            Result<IEnumerable<CatFeriados>> result = new Result<IEnumerable<CatFeriados>>();

            try
            {
                DateTime dt;
                //bool permanent = false;

                string filterToLower = filter.Trim().ToLower();

                var repository = Repository;

                //if (Labels.HolidayType.ToLower().Trim().Contains(filterToLower))
                //    permanent = filterToLower == "permanente" ? true : false;

                if (filterToLower.IsDate())
                {
                    dt = DateTime.Parse(filterToLower);
                    result.Data = repository.GetMany(h => h.FechaFeriado.Equals(dt));
                }
                else
                    result.Data = repository.GetMany(h => h.MotivoFeriado.Trim().ToLower().Contains(filterToLower));

                result.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public void GetHolidaysForList(ref QueryOptions<IEnumerable<CatFeriados>> queryOpt)
        {
            Result<IEnumerable<CatFeriados>> result = new Result<IEnumerable<CatFeriados>>();

            try
            {
                string orderBy = "MotivoFeriado";

                var predicate = PredicateBuilder.True<CatFeriados>();

                var filter = queryOpt.Filters.FirstOrDefault(f => f.Name.Equals("Search"));

                if (!IsNull(filter) && !string.IsNullOrEmpty(filter.Value))
                {
                    DateTime dt;
                    int intFilter;

                    predicate = predicate.And(p => p.MotivoFeriado.ToLower().Trim().Contains(filter.Value.ToLower().Trim()));

                    if (DateTime.TryParse(filter.Value, out dt))
                        predicate = predicate.Or(p => p.FechaFeriado == dt);

                    if (int.TryParse(filter.Value, out intFilter))
                        predicate = predicate.Or(p => p.IdFeriado == intFilter);

                    queryOpt.TotalRecords = _unitOfWork.GetRepository<CatFeriados>().Count(predicate);
                }
                else
                    queryOpt.TotalRecords = _unitOfWork.GetRepository<CatFeriados>().Count();

                if (!string.IsNullOrEmpty(queryOpt.SortField))
                    orderBy = queryOpt.SortField;

                result.Data = _unitOfWork.GetRepository<CatFeriados>().GetManyOnDemand(predicate, orderBy, queryOpt.CurrentPageIndex, queryOpt.RowsPerPage, queryOpt.SortOrder);

                result.Message = Messages.SuccessGetResultSet;

                result.MessageType = MessageType.INFO;
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorGetResultSet, "registro de día feriado");
                result.MessageType = MessageType.ERROR;
                result.DetailException = ex;
            }

            queryOpt.ResultData = result;
        }

        public ResultBase InsertHoliday(CatFeriados holiday)
        {
            ResultBase result = new ResultBase();

            try
            {
                if (holiday.IdFeriado == 0)
                {
                    //holiday.IdUsuarioIns = 1;
                    holiday.FechaIns = DateTime.Now;

                    var sequencesService = new SequenceService(_unitOfWork.GetRepository<MstSecuencias>());

                    var resultValidate = Utilities.ValidateEntity(holiday);

                    if (resultValidate.MessageType == MessageType.WARNING)
                        result = resultValidate;
                    else
                    {
                        MstSecuencias seq = sequencesService.GetNextSequenceByTableName("[config].[CatFeriados]");

                        holiday.IdFeriado = seq.SecuencialTabla;
                        Repository.Add(holiday);

                        sequencesService.InsertSequence(seq);

                        _unitOfWork.Commit();

                        result.Message = Messages.SuccessSaveData;
                    }
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorSaveData, "día feriado");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase UpdateHoliday(CatFeriados holiday)
        {
            ResultBase result = new ResultBase();

            try
            {
                var temp = Repository.Get(h => h.IdFeriado == holiday.IdFeriado);

                if (temp == null)
                {
                    result.Message = Messages.NotExist;
                    result.MessageType = MessageType.WARNING;
                    result.DetailException = new Exception(result.Message);
                }
                else
                {
                    var resultValidate = Utilities.ValidateEntity(holiday);

                    if (resultValidate.MessageType == MessageType.WARNING)
                        result = resultValidate;
                    else
                    {
                        //holiday.IdUsuarioUpd = 1;
                        holiday.FechaUpd = DateTime.Now;
                        Repository.Update(holiday);

                        _unitOfWork.Commit();

                        result.Message = Messages.SuccessUpdateData;
                    }
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorUpdateData, "día feriado");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }
    }
}
