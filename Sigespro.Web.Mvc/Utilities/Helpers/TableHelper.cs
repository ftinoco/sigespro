﻿using Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Sigespro.Web.Mvc.Utilities.Helpers
{
    public static class TableHelper
    {
        /// <summary>
        /// Función para generar el paginado de los grid
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="queryOpt"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        public static string SmartPage<T>(this HtmlHelper helper, QueryOptions<T> queryOpt, string form = "frmList") where T : class
        {
            StringBuilder sb = new StringBuilder();
            Func<long, long> pageCalculation = null;

            long nonAdjacentPageCount = 0;
            int intPerPage = queryOpt.RowsPerPage;
            long number_of_pages = queryOpt.PageCount;
            int intCurrentPage = queryOpt.CurrentPageIndex;
            int minPagesForPaging = 2, adjacentPageCount = 5;

            sb.Append("<input id =\"IsPaging\" name=\"IsPaging\" type=\"hidden\" value=\"false\"/>");

            if (queryOpt.ResultData == null || (queryOpt.ResultData != null && queryOpt.ResultData.Data == null ))
                return sb.ToString();

            Type[] arrType = queryOpt.ResultData.Data.GetType().GetGenericArguments();
            int colspan = arrType[0].GetProperties().Length; 

            string pageNumberPrefix = string.Empty, linkUrl = string.Empty, onClick = string.Empty, previousText = string.Empty, nextText = string.Empty;

            string strNextText = nextText;
            string strPreviousText = previousText;

            if (intCurrentPage < 1)
                intCurrentPage = 1;
            else
                intCurrentPage += 1;

            long i = 0;

            if(number_of_pages == 1)
                return sb.ToString();

            if (number_of_pages <= adjacentPageCount)
            {
                sb.Append("<!-- start of 'paging' -->");
                sb.Append("<tfoot><tr><td colspan="+ colspan + ">");
                sb.Append("<ul class=\"pagination\">");

                for (i = 0; i < number_of_pages; i++)
                {
                    if (!(i == intCurrentPage - 1))
                    {
                        long page = i + 1;

                        if (pageCalculation != null)
                            page = pageCalculation((i + 1));

                        sb.Append("<li class=\"page-item\"><a class=\"page-link\" href=\"#\" onclick=\"sigesproApp.paging(event, " + (page - 1).ToString() + ",'" + form + "');\">" + (i + 1) + pageNumberPrefix + "</a></li>");
                    }
                    else
                        sb.Append("<li class=\"page-item active\"><a class=\"page-link\" href=\"#\">" + (i + 1) + pageNumberPrefix + "<span class=\"sr-only\">(current)</a></li>");
                }

                sb.Append("</ul>");
                sb.Append("</td></tr></tfoot>");
                sb.Append("<!-- end of 'paging' -->");
            }
            else
            {
                //hide paging if only one page
                if (number_of_pages > 1)
                {
                    sb.Append("<!-- start of 'paging' -->");
                    sb.Append("<tfoot><tr><td colspan="+ colspan + ">");
                    sb.Append("<ul class=\"pagination\">");

                    //previous record
                    if (!(intCurrentPage == 1))
                    {
                        long page = intCurrentPage - 1;

                        if (pageCalculation != null)
                            page = pageCalculation((intCurrentPage - 1));

                        sb.Append("<li class=\"page-item\"><a aria-label=\"Previous\" class=\"page-link\" href=\"#\" onclick=\"sigesproApp.paging(event, 0,'" + form + "');\"><span aria-hidden=\"true\">&laquo;</span></a></li>");
                        sb.Append("<li class=\"page-item\"><a aria-label=\"Previous\" class=\"page-link\" href=\"#\" onclick=\"sigesproApp.paging(event, " + (page - 1).ToString() + ",'" + form + "');\"><span aria-hidden=\"true\">&lt;</span></a></li>");
                    }
                    else
                        sb.Append("<li class=\"page-item disabled\"><a aria-label=\"Previous\" class=\"page-link\" href=\"#\" onclick=\"sigesproApp.paging(event, 0" + ",'" + form + "');\"><span aria-hidden=\"true\">&laquo;</span></a></li>");


                    if (number_of_pages < minPagesForPaging)
                    {
                        // Display all pages, no paging.
                        for (i = 0; i < number_of_pages; i++)
                        {
                            if (!(i == intCurrentPage - 1))
                            {
                                long page = i + 1;

                                if (pageCalculation != null)
                                    page = pageCalculation((i + 1));

                                sb.Append("<li class=\"page-item\"><a class=\"page-link\" href=\"#\" onclick=\"sigesproApp.paging(event, " + (page - 1).ToString() + ",'" + form + "');\">" + (i + 1) + pageNumberPrefix + "</a></li>");
                            }
                            else
                                sb.Append("<li class=\"page-item active\"><a class=\"page-link\" href=\"#\">" + (i + 1) + pageNumberPrefix + "<span class=\"sr-only\">(current)</a></li>");
                        }
                    }
                    else
                    {
                        if (intCurrentPage < adjacentPageCount)
                        {
                            // Left-most page numbers.
                            for (i = 0; i < adjacentPageCount; i++)
                            {
                                if (!(i == intCurrentPage - 1))
                                {
                                    long page = i + 1;

                                    if (pageCalculation != null)
                                        page = pageCalculation((i + 1));

                                    sb.Append("<li class=\"page-item\"><a class=\"page-link\" href=\"#\" onclick=\"sigesproApp.paging(event, " + (page - 1).ToString() + ",'" + form + "');\">" + (i + 1) + pageNumberPrefix + "</a></li>");
                                }
                                else
                                    sb.Append("<li class=\"page-item active\"><a class=\"page-link\" href=\"#\">" + (i + 1) + pageNumberPrefix + "<span class=\"sr-only\">(current)</a></li>");
                            }

                            sb.Append("<li class=\"page-item\"><a class=\"page-link\" href=\"#\" onclick=\"sigesproApp.paging(event, " + i.ToString() + ",'" + form + "');\">...</a></li>");

                            // Right-most page numbers.
                            for (i = number_of_pages - nonAdjacentPageCount; i < number_of_pages; i++)
                            {
                                if (!(i == intCurrentPage - 1))
                                {
                                    long page = i + 1;

                                    if (pageCalculation != null)
                                        page = pageCalculation((i + 1));

                                    sb.Append("<li class=\"page-item\"><a class=\"page-link\" href=\"#\" onclick=\"sigesproApp.paging(event, " + (page - 1).ToString() + ",'" + form + "');\">" + (i + 1) + pageNumberPrefix + "</a></li>");
                                }
                                else
                                    sb.Append("<li class=\"page-item active\"><a class=\"page-link\" href=\"#\">" + (i + 1) + pageNumberPrefix + "<span class=\"sr-only\">(current)</a></li>");

                            }
                        }
                        else if (intCurrentPage > number_of_pages - (adjacentPageCount - 1))
                        {
                            // Scenario 2: Right-most numbers are active.

                            // Left-most page numbers.
                            for (i = 0; i < nonAdjacentPageCount; i++)
                            {
                                if (!(i == intCurrentPage - 1))
                                {
                                    long page = i + 1;
                                    if (pageCalculation != null)
                                        page = pageCalculation((i + 1));

                                    sb.Append("<li class=\"page-item\"><a class=\"page-link\" href=\"#\" onclick=\"sigesproApp.paging(event, " + (page - 1).ToString() + ",'" + form + "');\">" + (i + 1) + pageNumberPrefix + "</a></li>");
                                }
                                else
                                    sb.Append("<li class=\"page-item active\"><a class=\"page-link\" href=\"#\">" + (i + 1) + pageNumberPrefix + "<span class=\"sr-only\">(current)</a></li>");

                            }

                            sb.Append("<li class=\"page-item\"><a class=\"page-link\" href=\"#\" onclick=\"sigesproApp.paging(event, " + (intCurrentPage - adjacentPageCount).ToString() + ",'" + form + "');\">...</a></li>");

                            // Right-most page numbers.
                            for (i = number_of_pages - adjacentPageCount; i < number_of_pages; i++)
                            {
                                if (!(i == intCurrentPage - 1))
                                {
                                    long page = i + 1;

                                    if (pageCalculation != null)
                                        page = pageCalculation((i + 1));

                                    sb.Append("<li class=\"page-item\"><a class=\"page-link\" href=\"#\" onclick=\"sigesproApp.paging(event, " + (page - 1).ToString() + ",'" + form + "');\">" + (i + 1) + pageNumberPrefix + "</a></li>");
                                }
                                else
                                    sb.Append("<li class=\"page-item active\"><a class=\"page-link\" href=\"#\">" + (i + 1) + pageNumberPrefix + "<span class=\"sr-only\">(current)</a></li>");

                            }
                        }
                        else
                        {
                            // Scenario 3: Middle numbers are active.
                            // Draw left-most numbers.
                            for (i = 0; i < nonAdjacentPageCount; i++)
                            {
                                if (!(i == intCurrentPage - 1))
                                {
                                    long page = i + 1;

                                    if (pageCalculation != null)
                                        page = pageCalculation((i + 1));

                                    sb.Append("<li class=\"page-item\"><a class=\"page-link\" href=\"#\" onclick=\"sigesproApp.paging(event, " + (page - 1).ToString() + ",'" + form + "');\">" + (i + 1) + pageNumberPrefix + "</a></li>");
                                }
                                else
                                    sb.Append("<li class=\"page-item active\"><a class=\"page-link\" href=\"#\">" + (i + 1) + pageNumberPrefix + "<span class=\"sr-only\">(current)</a></li>");
                            }

                            sb.Append("<li class=\"page-item\"><a class=\"page-link\" href=\"#\" onclick=\"sigesproApp.paging(event, " + (intCurrentPage - adjacentPageCount).ToString() + ",'" + form + "');\">...</a></li>");

                            // Draw middle numbers.
                            for (i = intCurrentPage - (adjacentPageCount / 2); i <= intCurrentPage + (adjacentPageCount / 2); i++)
                            {
                                if (i != intCurrentPage)
                                {
                                    long page = i;

                                    if (pageCalculation != null)
                                        page = pageCalculation(i);

                                    sb.Append("<li class=\"page-item\"><a class=\"page-link\" href=\"#\" onclick=\"sigesproApp.paging(event, " + (page - 1).ToString() + ",'" + form + "');\">" + (i + 1) + pageNumberPrefix + "</a></li>");
                                }
                                else
                                    sb.Append("<li class=\"page-item active\"><a class=\"page-link\" href=\"#\">" + (i + 1) + pageNumberPrefix + "<span class=\"sr-only\">(current)</a></li>");
                            }

                            int tempIndexPageNext = (intCurrentPage + (adjacentPageCount / 2) + 1);

                            sb.Append("<li class=\"page-item\"><a class=\"page-link\" href=\"#\" onclick=\"sigesproApp.paging(event, " + tempIndexPageNext.ToString() + ",'" + form + "');\">...</a></li>");

                            // Draw right-most numbers.
                            for (i = number_of_pages - nonAdjacentPageCount; i < number_of_pages; i++)
                            {
                                if (!(i == intCurrentPage - 1))
                                {
                                    long page = i + 1;
                                    if (pageCalculation != null)
                                        page = pageCalculation((i + 1));

                                    sb.Append("<li class=\"page-item\"><a class=\"page-link\" href=\"#\" onclick=\"sigesproApp.paging(event, " + (page - 1).ToString() + ",'" + form + "');\">" + (i + 1) + pageNumberPrefix + "</a></li>");
                                }
                                else
                                    sb.Append("<li class=\"page-item active\"><a class=\"page-link\" href=\"#\">" + (i + 1) + pageNumberPrefix + "<span class=\"sr-only\">(current)</a></li>");
                            }
                        }
                    }

                    //next record
                    if (intCurrentPage != number_of_pages)
                    {
                        long page = intCurrentPage + 1;

                        if (pageCalculation != null)
                            page = pageCalculation((intCurrentPage + 1));
                        
                        sb.Append("<li class=\"page-item\"><a aria-label=\"Next\" class=\"page-link\" href=\"#\" onclick=\"sigesproApp.paging(event, " + (page - 1).ToString() + ",'" + form + "');\"><span aria-hidden=\"true\">&gt;</span></a></li>");
                        sb.Append("<li class=\"page-item\"><a aria-label=\"Next\" class=\"page-link\" href=\"#\" onclick=\"sigesproApp.paging(event, " + (number_of_pages - 1).ToString() + ",'" + form + "');\"><span aria-hidden=\"true\">&raquo;</span></a></li>");

                    }
                    else 
                        sb.Append("<li class=\"page-item disabled\"><a aria-label=\"Next\" class=\"page-link\" href=\"#\" onclick=\"sigesproApp.paging(event, 0" + ",'" + form + "');\"><span aria-hidden=\"true\">&raquo;</span></a></li>");
                    
                    sb.Append("</ul>");
                    sb.Append("</td></tr></tfoot>");
                    sb.Append("<!-- end of 'paging' -->");
                } 
            }

            return sb.ToString();
        }

        /// <summary>
        /// Crea una etiqueta span con el icono de ordenamiento
        /// </summary>
        /// <param name="htmlHelper">Objeto HtmlHelper (Para que el metodo sea una extesion de Html)</param>
        /// <param name="queryOpt">Objeto SortingPagingInfo</param>
        /// <param name="sortField">Nombre del campo vinculado al icono para validar si se esta ordenando por el</param>
        /// <param name="alphabet">Bandera que indica si el icono de ordenamiento sera alfabetico o genérico (Por defecto: true)</param>
        /// <returns>Etiqueta span</returns>
        public static MvcHtmlString BuildSortIcon<T>(this HtmlHelper htmlHelper, QueryOptions<T> queryOpt, string sortField, bool alphabet = true) where T : class
        {
            string rawstring;
            string sortIcon = "sort";

            if (queryOpt.SortField == sortField)
            {
                if (alphabet)
                    sortIcon += "-by-alphabet";
                else
                    sortIcon += "-by-attributes";

                if (queryOpt.SortOrder != SortingOrder.ASC)
                    sortIcon += "-alt";
            }

            rawstring = string.Format("<span class=\"sort {0} {1}{2}\"></span>", "glyphicon", "glyphicon-", sortIcon);

            return new MvcHtmlString(rawstring);
        }

        /// <summary>
        /// Crea una etiqueta hiperlink para ordenar las columnas de tablas
        /// </summary>
        /// <param name="htmlHelper">Objeto HtmlHelper (Para que el metodo sea una extesion de Html)</param>
        /// <param name="fieldName">Nombre del campo que se visualizará en la cabecera de la tabla</param>
        /// <param name="actionName">Nombre de acción, para crear el url del hiperlink (No se usa, se provee por cualquier uso a futuro)</param>
        /// <param name="sortField">Nombre del campo en el modelo que carga la tabla por el cual se ordenará</param>
        /// <param name="queryOpt">Objeto SortingPagingInfo</param>
        /// <param name="iconAlphabet">Bandera que indica si el icono de ordenamiento sera alfabetico o genérico (Por defecto: true)</param>
        /// <param name="form">Nombre del formulario en cual esta contenida la tabla, por defecto si no se asigna este parametro se le hace submit al form con el id => frmList</param>
        /// <returns>Etiqueta hiperlink</returns>
        public static MvcHtmlString HeaderTable<T>(this HtmlHelper htmlHelper, string fieldName, string actionName, string sortField, QueryOptions<T> queryOpt, bool iconAlphabet = true, string form = "") where T : class
        {
            var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);

            string rawElement = "<a href =\"{0}\" class=\"header\" data-sortfield=\"{1}\" ";

            rawElement += "onclick=\"sigesproApp.sortingTable(event";

            if (!string.IsNullOrEmpty(form))
                rawElement += ", '" + form + "'";

            rawElement += ")\" ";

            rawElement += ">{2} {3}</a>";

            return new MvcHtmlString(string.Format(rawElement, urlHelper.Action(actionName), sortField, fieldName, BuildSortIcon(htmlHelper, queryOpt, sortField, iconAlphabet)));
        }

        public static MvcHtmlString HeaderTable<T>(this HtmlHelper htmlHelper, string fieldName, string actionName, string controller, string sortField, QueryOptions<T> queryOpt, bool iconAlphabet = true, string form = "") where T : class
        {
            var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);

            string rawElement = "<a href =\"{0}\" class=\"header\" data-sortfield=\"{1}\" ";

            rawElement += "onclick=\"sigesproApp.sortingTable(event";

            if (!string.IsNullOrEmpty(form))
                rawElement += ", '" + form + "'";

            rawElement += ")\" ";

            rawElement += ">{2} {3}</a>";

            return new MvcHtmlString(string.Format(rawElement, urlHelper.Action(actionName, controller), sortField, fieldName, BuildSortIcon(htmlHelper, queryOpt, sortField, iconAlphabet)));
        }

    }
}