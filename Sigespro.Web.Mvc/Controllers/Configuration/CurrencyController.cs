﻿using Common.Helpers;
using Common.Log;
using Newtonsoft.Json;
using Sigespro.Entity;
using Sigespro.Service.Pre;
using Sigespro.Web.Mvc.Models.Configuration;
using Sigespro.Web.Mvc.Resources;
using Sigespro.Web.Mvc.Utilities.Attributes.Security;
using Sigespro.Web.Mvc.Utilities.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Sigespro.Web.Mvc.Controllers.Configuration
{
    [RoutePrefix("Configuration/Currencies")] 
    public class CurrencyController : Controller
    {
        private readonly IUserSession _userSession;

        public CurrencyService Service { get; set; }

        public CurrencyController()
        {
            Service = new CurrencyService();
            _userSession = new UserSession();
        }

        [Route]
        public ActionResult Index(QueryOptions<IEnumerable<CurrencyViewModel>> queryOpt)
        {
            string message = string.Empty;
            IEnumerable<CurrencyViewModel> model = new List<CurrencyViewModel>();

            try
            {
                string strSerialize = JsonConvert.SerializeObject(queryOpt);

                QueryOptions<IEnumerable<CatMoneda>> filter = JsonConvert.DeserializeObject<QueryOptions<IEnumerable<CatMoneda>>>(strSerialize);
                Service.GetCurrenciesForList(ref filter);

                if (filter.ResultData.MessageType != MessageType.INFO)
                {
                    message = filter.ResultData.Message;
                    Logger.Log(filter.ResultData.MessageType, filter.ResultData.DetailException, _userSession.Username,
                               MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                {
                    strSerialize = JsonConvert.SerializeObject(filter);

                    queryOpt = JsonConvert.DeserializeObject<QueryOptions<IEnumerable<CurrencyViewModel>>>(strSerialize);

                    model = queryOpt.ResultData.Data;
                }
            }
            catch (Exception ex)
            {
                message = Messages.LoadPageError;
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            if (!string.IsNullOrEmpty(message))
                ModelState.AddModelError(Messages.AlertDanger, message);

            ViewBag.Filter = true;
            ViewBag.QueryOptions = queryOpt;

            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [Route("Add")]
        public ActionResult AddCurrency()
        {
            CurrencyViewModel model = new CurrencyViewModel();

            ViewBag.ThereAreLocal = false;
            string message = string.Empty;

            try
            {
                model.FechaIns = DateTime.Now;

                model.IdUsuarioIns = 1;

                var result = Service.GetCurrencies();

                if (result.MessageType != MessageType.INFO)
                    message = result.Message;
                else
                {
                    if (result.Data.Any(c => c.EsLocal))
                        ViewBag.ThereAreLocal = true;
                }

                model.Activo = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                           MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            ViewBag.IsEdit = false;
            ViewBag.Action = "SaveCurrency";
            ViewBag.Title = "Nueva Moneda";

            return View(model);
        }

        [Route("Edit/{currencyId}")]
        public ActionResult EditCurrency(int currencyId)
        {
            ViewBag.ThereAreLocal = false;
            string message = string.Empty;
            CurrencyViewModel model = new CurrencyViewModel();

            try
            {
                // SE OBTIENE EL REGISTRO DE MONEDA POR ID
                var result = Service.GetCurrencyById(currencyId);

                if (result.MessageType != MessageType.INFO)
                    message = result.Message;
                else
                {
                    //string strSerialize = JsonConvert.SerializeObject(result.Data);

                    model = new CurrencyViewModel
                    {
                        Activo = result.Data.Activo,
                        EsLocal = result.Data.EsLocal,
                        FechaIns = result.Data.FechaIns,
                        FechaUpd = result.Data.FechaUpd,
                        IdMoneda = result.Data.IdMoneda,
                        IdUsuarioIns = result.Data.IdUsuarioIns,
                        IdUsuarioUpd = result.Data.IdUsuarioUpd,
                        Nombre = result.Data.Nombre,
                        Simbolo = result.Data.Simbolo
                    }; 
                }

                // SE OBTIENE EL LISTADO DE REGISTRO DE MONEDAS
                var resultCurrencies = Service.GetCurrencies();

                if (resultCurrencies.MessageType != MessageType.INFO)
                    message = resultCurrencies.Message;

                if (resultCurrencies.Data.Any(c => c.EsLocal) && !model.EsLocal)
                    ViewBag.ThereAreLocal = true;

            }
            catch (Exception ex)
            {
                message = Messages.ErrorApi;
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                           MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            if (!string.IsNullOrEmpty(message))
                ModelState.AddModelError(Messages.AlertDanger, message);

            ViewBag.IsEdit = true;
            ViewBag.Action = "UpdateCurrency";
            ViewBag.Title = "Editar Moneda";

            if (!model.FechaUpd.HasValue)
                model.FechaUpd = DateTime.Now;

            if (!model.IdUsuarioUpd.HasValue)
                model.IdUsuarioUpd = 1;

            return View("AddCurrency", model);
        }

        [AjaxOnly]
        [HttpPost]
        [Route("Save")]
        [AllowAnonymous]
        public ActionResult SaveCurrency(CurrencyViewModel model)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                model.IdUsuarioIns = _userSession.UserId;

                string strSerialize = JsonConvert.SerializeObject(model);

                var result = Service.InsertCurrency(JsonConvert.DeserializeObject<CatMoneda>(strSerialize)); 

                if (result.MessageType != MessageType.SUCCESS)
                {
                    message = result.Message;
                    status = result.MessageType.ToString();
                    Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                    message = string.Format(Messages.SaveSuccess, "moneda");
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                             MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message, action = Url.Action("Index", "Currency") },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [AjaxOnly]
        [HttpPost]
        [AllowAnonymous]
        [Route("Update")]
        public ActionResult UpdateCurrency(CurrencyViewModel model)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                model.IdUsuarioUpd = _userSession.UserId;
                string strSerialize = JsonConvert.SerializeObject(model);

                var result = Service.UpdateCurrency(JsonConvert.DeserializeObject<CatMoneda>(strSerialize));

                if (result.MessageType != MessageType.SUCCESS)
                {
                    message = result.Message;
                    status = result.MessageType.ToString();
                    Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                    message = string.Format(Messages.UpdateSuccess, "moneda");
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                             MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message, action = Url.Action("Index", "Currency") },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

    }
}