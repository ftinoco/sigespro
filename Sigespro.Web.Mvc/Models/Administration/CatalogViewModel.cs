﻿using Sigespro.Web.Mvc.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sigespro.Web.Mvc.Models.Administration
{
    public class CatalogViewModel
    {
        public int IdCatalogo { get; set; }

        [Display(Name = "Nombre")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string Nombre { get; set; }

        [Display(Name = "Description", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string Descripcion { get; set; }

        [Display(Name = "Active", ResourceType = typeof(Labels))]
        public bool Activo { get; set; }

        [Display(Name = "UserInsert", ResourceType = typeof(Labels))]
        public int IdUsuarioIns { get; set; }

        [Display(Name = "DateInsert", ResourceType = typeof(Labels))]
        public DateTime FechaIns { get; set; }

        [Display(Name = "UserEdit", ResourceType = typeof(Labels))]
        public int? IdUsuarioUpd { get; set; }

        [Display(Name = "DateEdit", ResourceType = typeof(Labels))]
        public DateTime? FechaUpd { get; set; }
    }
}