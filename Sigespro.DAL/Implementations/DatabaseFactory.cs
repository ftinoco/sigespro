﻿using Sigespro.DAL.Interface;
using Sigespro.Entity;

namespace Sigespro.DAL.Implementations
{
    public class DatabaseFactory : Disposable, IDatabaseFactory
    {
        private SigesproEntities context;

        public SigesproEntities Get()
        {
            return context ?? (context = new SigesproEntities());
        }

        protected override void DisposeCore()
        {
            if (context != null)
                context.Dispose();
        }
    }
}
