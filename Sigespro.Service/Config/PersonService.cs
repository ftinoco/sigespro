﻿using Common.Extensions;
using Common.Helpers;
using Common.Log;

using Sigespro.DAL.Implementations;
using Sigespro.Entity;
using Sigespro.Entity.Resource;
using Sigespro.Service.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigespro.Service.Config
{
    public class PersonService : Service
    {
        private bool CommitExternal { get; set; }

        private Repository<MstPersonas> Repository { get; set; }

        public PersonService() : this(new UnitOfWork())
        {
        }

        public PersonService(UnitOfWork UnitOfWork) : base()
        {
            _unitOfWork = UnitOfWork;

            Repository = UnitOfWork.GetRepository<MstPersonas>();

            CommitExternal = true;
        }

        public Result<IEnumerable<MstPersonas>> GetPersons()
        {
            Result<IEnumerable<MstPersonas>> result = new Result<IEnumerable<MstPersonas>>();

            try
            {
                result.Data = Repository.GetAll();

                result.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<MstPersonas> GetPersonById(int idPerson)
        {
            Result<MstPersonas> result = new Result<MstPersonas>();

            try
            {
                result.Data = Repository.GetById(idPerson);
                result.Message = Messages.SuccessGetResult;
                result.MessageType = MessageType.INFO;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase InsertPerson(MstPersonas person)
        {
            ResultBase result = new ResultBase();

            try
            {
                var resultValidate = ValidatePersonData(person);

                if (resultValidate.MessageType != MessageType.SUCCESS)
                    result = resultValidate;
                else
                {
                    var temp = Repository.Get(p => p.Identificacion == person.Identificacion && p.NombreCompleto == person.NombreCompleto);

                    if (temp == null)
                    {
                        person.FechaIns = DateTime.Now;

                        var sequencesService = new SequenceService(_unitOfWork.GetRepository<MstSecuencias>());

                        MstSecuencias seq = sequencesService.GetNextSequenceByTableName("[config].[MstPersonas]");

                        person.IdPersona = seq.SecuencialTabla;

                        sequencesService.InsertSequence(seq);

                        Repository.Add(person);

                        result = InsertUpdatePersonAddresses(person, _unitOfWork);
                        if (result.MessageType != MessageType.SUCCESS)
                            return result;

                        result = InsertUpdatePersonEmails(person, _unitOfWork);
                        if (result.MessageType != MessageType.SUCCESS)
                            return result;

                        result = InsertUpdatePersonPhones(person, _unitOfWork);
                        if (result.MessageType != MessageType.SUCCESS)
                            return result;

                        if (!CommitExternal)
                            _unitOfWork.Commit();

                        result.Message = Messages.SuccessSaveData;
                    }
                    else
                    {
                        result.MessageType = MessageType.WARNING;
                        result.Message = "La persona con el número de identificación y el nombre especificado ya existe";
                        result.DetailException = new Exception(result.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorSaveData, "persona");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase UpdatePerson(MstPersonas person)
        {
            ResultBase result = new ResultBase();

            try
            {
                var temp = Repository.Get(p => p.IdPersona == person.IdPersona);

                if (temp != null)
                {
                    var resultValidate = ValidatePersonData(person);

                    if (resultValidate.MessageType != MessageType.SUCCESS)
                        result = resultValidate;
                    else
                    {
                        person.FechaIns = temp.FechaIns;
                        person.IdUsuarioIns = temp.IdUsuarioIns;

                        person.FechaUpd = DateTime.Now;

                        // ======= SE CREAN/ACTUALIZAN LOS REGISTROS PROPORCIONADOS EN EMAIL, DIRECCION Y TELEFONO ======= //
                        result = InsertUpdatePersonAddresses(person, _unitOfWork);
                        if (result.MessageType != MessageType.SUCCESS)
                            return result;

                        result = InsertUpdatePersonEmails(person, _unitOfWork);
                        if (result.MessageType != MessageType.SUCCESS)
                            return result;

                        result = InsertUpdatePersonPhones(person, _unitOfWork);
                        if (result.MessageType != MessageType.SUCCESS)
                            return result;

                        // Se actualiza el registro de persona
                        Repository.Update(person);

                        // ======= SE ELIMINAN LOS REGISTROS QUE NO FUERON PROPORCIONADOS EN EMAIL, DIRECCION Y TELEFONO ======= //
                        result = DeletePersonAddresses(person, _unitOfWork);
                        if (result.MessageType != MessageType.SUCCESS)
                            return result;

                        result = DeletePersonEmails(person, _unitOfWork);
                        if (result.MessageType != MessageType.SUCCESS)
                            return result;

                        result = DeletePersonPhones(person, _unitOfWork);
                        if (result.MessageType != MessageType.SUCCESS)
                            return result;

                        if (!CommitExternal)
                            _unitOfWork.Commit();

                        result.Message = Messages.SuccessUpdateData;
                    }
                }
                else
                {
                    result.MessageType = MessageType.WARNING;
                    result.Message = "La persona con el identificador especificado no existe.";
                    result.DetailException = new Exception(result.Message);
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorUpdateData, "persona");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        private ResultBase InsertUpdatePersonEmails(MstPersonas person, UnitOfWork unitOfWork)
        {
            ResultBase result = new ResultBase();

            try
            {
                int count = 0;
                Repository<DetPersonaEmail> repository = unitOfWork.GetRepository<DetPersonaEmail>();
            
                foreach (DetPersonaEmail personEmail in person.DetPersonaEmail)
                { 
                    if (string.IsNullOrEmpty(personEmail.Email))
                    {
                        result.MessageType = MessageType.WARNING;
                        result.Message = "Debe proporcionar el correo electrónico";
                        result.DetailException = new Exception(result.Message);
                    }
                    else
                    {
                        var emailFromList = person.DetPersonaEmail.FirstOrDefault(p => p.Email == personEmail.Email &&
                                                            p.IdPersonaEmail != personEmail.IdPersonaEmail);

                        if (emailFromList == null)
                        {
                            var emailFromDb = repository.Get(p => p.IdPersonaEmail == personEmail.IdPersonaEmail &&
                                                                  p.IdPersona == personEmail.IdPersona);

                            if (emailFromDb == null)
                            {
                                personEmail.Activo = true;
                                personEmail.FechaIns = DateTime.Now;
                                personEmail.IdUsuarioIns = person.IdUsuarioUpd.HasValue ? person.IdUsuarioUpd.Value : person.IdUsuarioIns;

                                var sequencesService = new SequenceService(unitOfWork.GetRepository<MstSecuencias>());

                                MstSecuencias seq = sequencesService.GetNextSequenceByTableName("[config].[DetPersonaEmail]");

                                // Se le suma el contador debido a que estando en la misma transaccion la tabla de secuencia no actualiza el registro
                                personEmail.IdPersonaEmail = seq.SecuencialTabla + count;

                                // Solo se va actualizar hasta que se hayan procesado todos los registros
                                if (person.DetPersonaEmail.Last().Email == personEmail.Email) //count == person.DetPersonaEmail.Count - 1)
                                {
                                    seq.SecuencialTabla = seq.SecuencialTabla + count;
                                    sequencesService.InsertSequence(seq);
                                }
                                     
                                repository.Add(personEmail); 
                                count++;
                            }
                            else
                            {
                                if (emailFromDb.Email != personEmail.Email || emailFromDb.Activo != personEmail.Activo)
                                {
                                    personEmail.FechaUpd = DateTime.Now;
                                    personEmail.IdUsuarioUpd = person.IdUsuarioUpd;

                                    repository.Update(personEmail);
                                }
                            }
                        }
                        else
                        {
                            if (personEmail.IdUsuarioIns != 0)
                            {
                                if (!personEmail.Activo && emailFromList.Activo)
                                    personEmail.Activo = true;
                                else
                                {
                                    result.MessageType = MessageType.WARNING;
                                    result.Message = "El correo electrónico " + personEmail.Email + " ya se encuentra registrado.";
                                    result.DetailException = new Exception(result.Message);
                                }

                                personEmail.FechaUpd = DateTime.Now;
                                personEmail.IdUsuarioUpd = person.IdUsuarioUpd;

                                repository.Update(personEmail);
                            }
                        }

                        result.Message = Messages.SuccessSaveData;
                    }
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorSaveData, "persona");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        private ResultBase InsertUpdatePersonPhones(MstPersonas person, UnitOfWork unitOfWork)
        {
            ResultBase result = new ResultBase();

            try
            {
                int count = 0;
                Repository<DetPersonaTelefono> repository = unitOfWork.GetRepository<DetPersonaTelefono>();

                foreach (DetPersonaTelefono personPhone in person.DetPersonaTelefono)
                {
                    var resultValidate = ValidatePhoneData(personPhone);

                    if (resultValidate.MessageType != MessageType.SUCCESS)
                        result = resultValidate;
                    else
                    {
                        var temp = person.DetPersonaTelefono.FirstOrDefault(p => p.NumeroTelefono == personPhone.NumeroTelefono &&
                                                        p.IdPersonaTelefono != personPhone.IdPersonaTelefono);

                        if (temp == null)
                        {

                            var temp2 = repository.Get(p => p.IdPersonaTelefono == personPhone.IdPersonaTelefono &&
                                                            p.IdPersona == personPhone.IdPersona);

                            if (temp2 == null)
                            {
                                personPhone.Activo = true;
                                personPhone.FechaIns = DateTime.Now;
                                personPhone.IdUsuarioIns = person.IdUsuarioUpd.HasValue ? person.IdUsuarioUpd.Value : person.IdUsuarioIns;

                                var sequencesService = new SequenceService(unitOfWork.GetRepository<MstSecuencias>());

                                MstSecuencias seq = sequencesService.GetNextSequenceByTableName("[config].[DetPersonaTelefono]");

                                // Se le suma el contador debido a que estando en la misma transaccion la tabla de secuencia no actualiza el registro
                                personPhone.IdPersonaTelefono = seq.SecuencialTabla + count;

                                // Solo se va actualizar hasta que se hayan procesado todos los registros
                                if (person.DetPersonaTelefono.Last().NumeroTelefono == personPhone.NumeroTelefono) //count == person.DetPersonaEmail.Count - 1)
                                {
                                    seq.SecuencialTabla = seq.SecuencialTabla + count;
                                    sequencesService.InsertSequence(seq);
                                }

                                repository.Add(personPhone);
                                count++;
                            }
                            else
                            {
                                if (temp2.IdTipoNumero != personPhone.IdTipoNumero || temp2.NumeroTelefono != personPhone.NumeroTelefono ||
                                    temp2.Activo != personPhone.Activo)
                                {
                                    personPhone.FechaUpd = DateTime.Now;
                                    personPhone.IdUsuarioUpd = person.IdUsuarioUpd;

                                    repository.Update(personPhone);
                                }
                            }
                        }
                        else
                        {
                            if (personPhone.IdUsuarioIns != 0)
                            {
                                if ((!personPhone.Activo && temp.Activo) && (temp.IdTipoNumero != personPhone.IdTipoNumero))
                                    personPhone.Activo = true;
                                else
                                {
                                    result.MessageType = MessageType.WARNING;
                                    result.Message = "El número telefónico " + personPhone.NumeroTelefono + " ya se encuentra registrado.";
                                    result.DetailException = new Exception(result.Message);
                                }

                                personPhone.FechaUpd = DateTime.Now;
                                personPhone.IdUsuarioUpd = person.IdUsuarioUpd;

                                repository.Update(personPhone);
                            }
                        }

                        result.Message = Messages.SuccessSaveData;
                    }
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorSaveData, "persona");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        private ResultBase InsertUpdatePersonAddresses(MstPersonas person, UnitOfWork unitOfWork)
        {
            ResultBase result = new ResultBase();

            try
            {
                int count = 0;
                var repository = unitOfWork.GetRepository<DetPersonaDireccion>();

                foreach (DetPersonaDireccion personAddress in person.DetPersonaDireccion)
                {
                    var resultValidate = ValidateAddressData(personAddress);

                    if (resultValidate.MessageType != MessageType.SUCCESS)
                        result = resultValidate;
                    else
                    {
                        var addressFromList = person.DetPersonaDireccion.FirstOrDefault(p => p.Direccion == personAddress.Direccion &&
                                                            p.IdPersonaDireccion != personAddress.IdPersonaDireccion);

                        if (addressFromList == null)
                        {
                            var addressFromDb = repository.Get(p => p.IdPersonaDireccion == personAddress.IdPersonaDireccion &&
                                                                    p.IdPersona == personAddress.IdPersona);

                            if (addressFromDb == null)
                            {
                                personAddress.Activo = true;
                                personAddress.FechaIns = DateTime.Now;
                                personAddress.IdUsuarioIns = person.IdUsuarioUpd.HasValue ? person.IdUsuarioUpd.Value : person.IdUsuarioIns;

                                var sequencesService = new SequenceService(unitOfWork.GetRepository<MstSecuencias>());

                                MstSecuencias seq = sequencesService.GetNextSequenceByTableName("[config].[DetPersonaDireccion]");

                                // Se le suma el contador debido a que estando en la misma transaccion la tabla de secuencia no actualiza el registro
                                personAddress.IdPersonaDireccion = seq.SecuencialTabla + count;

                                // Solo se va actualizar hasta que se hayan procesado todos los registros
                                if (person.DetPersonaDireccion.Last().Direccion == personAddress.Direccion) //count == person.DetPersonaEmail.Count - 1)
                                {
                                    seq.SecuencialTabla = seq.SecuencialTabla + count;
                                    sequencesService.InsertSequence(seq);
                                }

                                repository.Add(personAddress);
                                count++;
                            }
                            else
                            {
                                if (addressFromDb.IdTipoDireccion != personAddress.IdTipoDireccion ||
                                    addressFromDb.Direccion != personAddress.Direccion || addressFromDb.Activo != personAddress.Activo)
                                {
                                    personAddress.FechaUpd = DateTime.Now;
                                    personAddress.IdUsuarioUpd = person.IdUsuarioUpd;

                                    repository.Update(personAddress);
                                }
                            }
                        }
                        else
                        {
                            if (personAddress.IdUsuarioIns != 0)
                            {
                                if ((!personAddress.Activo && addressFromList.Activo) &&
                                    (personAddress.IdTipoDireccion != addressFromList.IdTipoDireccion))
                                    personAddress.Activo = true;
                                else
                                {
                                    result.MessageType = MessageType.WARNING;
                                    result.Message = "La dirección ya se encuentra registrada.";
                                    result.DetailException = new Exception(result.Message);
                                }

                                personAddress.FechaUpd = DateTime.Now;
                                personAddress.IdUsuarioUpd = person.IdUsuarioUpd;

                                repository.Update(personAddress);
                            }
                        }

                        result.Message = Messages.SuccessSaveData;
                    }
                }

            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorSaveData, "persona");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        private ResultBase DeletePersonAddresses(MstPersonas person, UnitOfWork unitOfWork)
        {
            ResultBase result = new ResultBase();

            try
            {
                var repository = unitOfWork.GetRepository<DetPersonaDireccion>();

                List<DetPersonaDireccion> personAddressMem = new List<DetPersonaDireccion>();
                person.DetPersonaDireccion.ToList().ForEach(p => personAddressMem.Add(new DetPersonaDireccion
                {
                    Activo = p.Activo,
                    Direccion = p.Direccion,
                    IdPersonaDireccion = p.IdPersonaDireccion,
                    IdTipoDireccion = p.IdTipoDireccion
                }));

                var personAddresses = repository.GetMany(p => p.IdPersona == person.IdPersona);

                foreach (var item in personAddresses)
                {
                    if (!personAddressMem.Any(p => p.IdTipoDireccion == item.IdTipoDireccion && p.Direccion == item.Direccion && p.IdPersonaDireccion == item.IdPersonaDireccion))
                        repository.Delete(item);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        private ResultBase DeletePersonEmails(MstPersonas person, UnitOfWork unitOfWork)
        {
            ResultBase result = new ResultBase();

            try
            {
                var repository = unitOfWork.GetRepository<DetPersonaEmail>();

                List<DetPersonaEmail> personEmailMem = new List<DetPersonaEmail>();
                person.DetPersonaEmail.ToList().ForEach(p => personEmailMem.Add(new DetPersonaEmail
                {
                    Email = p.Email,
                    IdPersonaEmail = p.IdPersonaEmail
                }));

                var personAddresses = repository.GetMany(p => p.IdPersona == person.IdPersona);

                foreach (var item in personAddresses)
                {
                    if (!personEmailMem.Any(p => p.Email == item.Email && p.IdPersonaEmail == item.IdPersonaEmail))
                        repository.Delete(item);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        private ResultBase DeletePersonPhones(MstPersonas person, UnitOfWork unitOfWork)
        {
            ResultBase result = new ResultBase();

            try
            {
                var repository = unitOfWork.GetRepository<DetPersonaTelefono>();

                List<DetPersonaTelefono> personPhoneMem = new List<DetPersonaTelefono>();
                person.DetPersonaTelefono.ToList().ForEach(p => personPhoneMem.Add(new DetPersonaTelefono
                {
                    Activo = p.Activo,
                    NumeroTelefono = p.NumeroTelefono,
                    IdTipoNumero = p.IdTipoNumero,
                    IdPersonaTelefono = p.IdPersonaTelefono
                }));

                var personPhones = repository.GetMany(p => p.IdPersona == person.IdPersona);

                foreach (var item in personPhones)
                {
                    if (!personPhoneMem.Any(p => p.IdTipoNumero == item.IdTipoNumero && p.NumeroTelefono == item.NumeroTelefono && p.IdPersonaTelefono == item.IdPersonaTelefono))
                        repository.Delete(item);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        #region Métodos privados

        private ResultBase ValidatePersonData(MstPersonas person)
        {
            ResultBase result = new ResultBase();

            if (person != null)
            {
                if (string.IsNullOrEmpty(person.Identificacion))
                {
                    result.MessageType = MessageType.WARNING;
                    result.Message = "Se debe proporcionar el número de identificación de la persona";
                    result.DetailException = new Exception(result.Message);
                    return result;
                }

                if (string.IsNullOrEmpty(person.NombreCompleto))
                {
                    result.MessageType = MessageType.WARNING;
                    result.Message = "Se debe proporcionar el nombre completo de la persona";
                    result.DetailException = new Exception(result.Message);
                    return result;
                }

                if (string.IsNullOrEmpty(person.SexoPersona))
                {
                    result.MessageType = MessageType.WARNING;
                    result.Message = "Se debe proporcionar el género/sexo de la persona";
                    result.DetailException = new Exception(result.Message);
                    return result;
                }

                if (person.DetPersonaEmail == null || person.DetPersonaEmail.Count == 0)
                {
                    result.MessageType = MessageType.WARNING;
                    result.Message = "Se debe proporcionar al menos un correo electrónico";
                    result.DetailException = new Exception(result.Message);
                    return result;
                }

                if (person.DetPersonaTelefono == null || person.DetPersonaTelefono.Count == 0)
                {
                    result.MessageType = MessageType.WARNING;
                    result.Message = "Se debe proporcionar al menos un número telefónico";
                    result.DetailException = new Exception(result.Message);
                    return result;
                }

                if (person.DetPersonaDireccion == null || person.DetPersonaDireccion.Count == 0)
                {
                    result.MessageType = MessageType.WARNING;
                    result.Message = "Se debe proporcionar al menos una dirección";
                    result.DetailException = new Exception(result.Message);
                }
            }
            else
            {
                result.MessageType = MessageType.ERROR;
                result.Message = string.Format(Messages.WithoutValidate, "persona");
                result.DetailException = new Exception(result.Message + ". Debido a que el objeto MstPersonas es nulo");
            }

            return result;
        }

        private ResultBase ValidatePhoneData(DetPersonaTelefono personPhone)
        {
            ResultBase result = new ResultBase();

            if (string.IsNullOrEmpty(personPhone.NumeroTelefono))
            {
                result.MessageType = MessageType.WARNING;
                result.Message = "Se debe proporcionar el número telefónico";
                result.DetailException = new Exception(result.Message);
                return result;
            }

            if (personPhone.IdTipoNumero == 0)
            {
                result.MessageType = MessageType.WARNING;
                result.Message = string.Format("Debe proporcionar el tipo de teléfono para el siguente número: « {0} »", personPhone.NumeroTelefono);
                result.DetailException = new Exception(result.Message);
            }

            return result;
        }

        private ResultBase ValidateAddressData(DetPersonaDireccion personAddress)
        {
            ResultBase result = new ResultBase();

            if (string.IsNullOrEmpty(personAddress.Direccion))
            {
                result.MessageType = MessageType.WARNING;
                result.Message = "Se debe proporcionar la dirección";
                result.DetailException = new Exception(result.Message);
                return result;
            }

            if (personAddress.IdTipoDireccion == 0)
            {
                result.MessageType = MessageType.WARNING;
                result.Message = string.Format("Debe proporcionar el tipo de dirección para la siguente dirección: « {0} »", personAddress.Direccion);
                result.DetailException = new Exception(result.Message);
            }

            return result;
        }

        #endregion


    }
}
