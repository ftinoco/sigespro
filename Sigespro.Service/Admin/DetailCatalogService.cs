﻿using Common.Extensions;
using Common.Helpers;
using Sigespro.DAL.Implementations;
using Sigespro.Entity;
using Sigespro.Entity.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using static Common.Helpers.Utilities;

namespace Sigespro.Service.Admin
{
    public class DetailCatalogService : Service
    {
        private Repository<DetCatalogo> Repository { get; set; }

        public DetailCatalogService() : base()
        {
            Repository = _unitOfWork.GetRepository<DetCatalogo>();
        }
        //    Repository = UnitOfWork.GetRepository<DetCatalogo>();
        //    /*UnitOfWork = new UnitOfWork();

        //    var sequenceRepository = UnitOfWork.GetRepository<MstSecuencias>();

        //    SequencesService = new SequenceService(sequenceRepository);

        //    Result = new string[] { "0", "Correcto" };*/
        //}

        public Result<IEnumerable<DetCatalogo>> GetDetailsCatalog()
        {
            Result<IEnumerable<DetCatalogo>> result = new Result<IEnumerable<DetCatalogo>>();

            try
            {
                result.Data = Repository.GetAll().OrderBy(d => d.MstCatalogo.Descripcion);

                result.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<DetCatalogo> GetDetailByValue(string value)
        {
            Result<DetCatalogo> result = new Result<DetCatalogo>();

            try
            {
                result.Data = Repository.Get(d => d.Valor.ToLower().Trim() == value.ToLower().Trim() && d.Activo);

                result.Message = Messages.SuccessGetResult;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public void GetDetailCatalogsForList(ref QueryOptions<IEnumerable<vwDetCatalogo>> queryOpt)
        {
            Result<IEnumerable<vwDetCatalogo>> result = new Result<IEnumerable<vwDetCatalogo>>();

            try
            {
                string orderBy = "FechaIns";

                var predicate = PredicateBuilder.True<vwDetCatalogo>();

                var filter = queryOpt.Filters.FirstOrDefault(f => f.Name.Equals("Search"));

                if (!IsNull(filter) && !string.IsNullOrEmpty(filter.Value))
                {
                    int intFilter;

                    predicate = predicate.And(p => p.Valor.ToLower().Trim().Contains(filter.Value.ToLower().Trim()) ||
                                                   p.Descripcion.ToLower().Trim().Contains(filter.Value.ToLower().Trim()) ||
                                                   p.Catalogo.ToLower().Trim().Contains(filter.Value.ToLower().Trim()));

                    if (int.TryParse(filter.Value, out intFilter))
                    {
                        predicate = predicate.Or(p => p.IdDetCatalogo == intFilter || p.IdCatalogo == intFilter);
                    }

                    queryOpt.TotalRecords = _unitOfWork.GetRepository<vwDetCatalogo>().Count(predicate);
                }
                else
                    queryOpt.TotalRecords = _unitOfWork.GetRepository<vwDetCatalogo>().Count();

                if (!string.IsNullOrEmpty(queryOpt.SortField))
                    orderBy = queryOpt.SortField;

                var det = _unitOfWork.GetRepository<vwDetCatalogo>().GetManyOnDemand(predicate, orderBy, queryOpt.CurrentPageIndex, 
                                                                                        queryOpt.RowsPerPage, queryOpt.SortOrder);
                /*
                foreach (var item in det)
                {
                    item.MstCatalogo = _unitOfWork.GetRepository<MstCatalogo>().GetById(item.IdMstCatalogo);
                }
                */
                result.Data = det;

                result.Message = Messages.SuccessGetResultSet;

                result.MessageType = MessageType.INFO;
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorGetResultSet, "registro de detalle catálogos");
                result.MessageType = MessageType.ERROR;
                result.DetailException = ex;
            }

            queryOpt.ResultData = result;
        }

        public Result<DetCatalogo> GetDetailCatalogById(int idDetail, int idCatalog)
        {
            Result<DetCatalogo> result = new Result<DetCatalogo>();

            try
            {
                result.Data = Repository.GetById(idDetail, idCatalog);
                result.Message = Messages.SuccessGetResult;
                result.MessageType = MessageType.INFO;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<IEnumerable<DetCatalogo>> GetDetailsCatalog(string filter)
        {
            Result<IEnumerable<DetCatalogo>> result = new Result<IEnumerable<DetCatalogo>>();

            try
            {
                bool active = false;

                string filterToLower = filter.Trim().ToLower();

                var repository = Repository;

                if (filterToLower == "activo" || filterToLower == "inactivo")
                {
                    active = filterToLower == "activo" ? true : false;
                    result.Data = repository.GetMany(d => d.Activo == active);
                }
                else
                {
                    result.Data = repository.GetMany(d => d.Valor.Trim().ToLower().Contains(filterToLower) ||
                                                    d.Descripcion.Trim().ToLower().Contains(filterToLower) ||
                                                    d.MstCatalogo.Descripcion.Trim().ToLower().Contains(filterToLower));
                }

                result.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<IEnumerable<DetCatalogo>> GetDetailsByTable(string table)
        {
            Result<IEnumerable<DetCatalogo>> result = new Result<IEnumerable<DetCatalogo>>();

            try
            {
                result.Data = Repository.GetMany(
                                        d => d.MstCatalogo.Nombre.ToLower().Trim() == table.ToLower().Trim() &&
                                        d.Activo).OrderBy(d => d.Descripcion);
                result.Message = Messages.SuccessGetResultSet;
                result.MessageType = MessageType.INFO;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<DetCatalogo> GetDetailsByTableAndValue(string table, string value)
        {
            Result<DetCatalogo> result = new Result<DetCatalogo>();

            try
            {
                result.Data = Repository.Get(
                                        d => d.MstCatalogo.Nombre.ToLower().Trim() == table.ToLower().Trim() &&
                                        d.Valor.ToLower().Trim() == value.ToLower().Trim() &&
                                        d.Activo);

                result.Message = Messages.SuccessGetResultSet;
                result.MessageType = MessageType.INFO;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase InsertDetailCatalog(DetCatalogo detailCatalog)
        {
            ResultBase result = new ResultBase();

            try
            {
                if (detailCatalog.IdDetCatalogo == 0)
                {
                    var sequencesService = new SequenceService(_unitOfWork.GetRepository<MstSecuencias>());

                    if (string.IsNullOrEmpty(detailCatalog.Valor) || string.IsNullOrEmpty(detailCatalog.Descripcion))
                    {
                        result.Message = Messages.RequiredData;
                        result.MessageType = MessageType.WARNING;
                        result.DetailException = new Exception(result.Message);
                    }
                    else
                    {
                        detailCatalog.FechaIns = DateTime.Now;

                        MstSecuencias seq = sequencesService.GetNextSequenceByTableName("[admin].[DetCatalogo]");

                        detailCatalog.IdDetCatalogo = seq.SecuencialTabla;
                        Repository.Add(detailCatalog);

                        sequencesService.InsertSequence(seq);

                        _unitOfWork.Commit();
                    }
                }

                result.Message = Messages.SuccessSaveData;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorSaveData, "detalle catálogo");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase UpdateDetailCatalog(DetCatalogo detailCatalog)
        {
            ResultBase result = new ResultBase();

            try
            {
                var temp = Repository.Get(d => d.IdDetCatalogo == detailCatalog.IdDetCatalogo); /*&&
                                                d.IdMstCatalogo == detailCatalog.IdMstCatalogo);*/

                if (temp == null)
                {
                    result.Message = Messages.NotExist;
                    result.MessageType = MessageType.WARNING;
                    result.DetailException = new Exception(result.Message);
                }
                else
                {
                    if (string.IsNullOrEmpty(detailCatalog.Valor) || string.IsNullOrEmpty(detailCatalog.Descripcion))
                    {
                        result.Message = Messages.RequiredData;
                        result.MessageType = MessageType.INFO;
                        result.DetailException = new Exception(result.Message);
                    }
                    else
                    {
                        detailCatalog.FechaUpd = DateTime.Now;

                        Repository.Update(detailCatalog);

                        _unitOfWork.Commit();
                    }

                    result.Message = Messages.SuccessUpdateData;
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorUpdateData, "detalle catálogo");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

    }
}
