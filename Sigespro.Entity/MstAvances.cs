//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sigespro.Entity
{
    using System;
    using System.Collections.Generic;
     
    public partial class MstAvances
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MstAvances()
        {
            this.DetAvancePreguntas = new HashSet<DetAvancePreguntas>();
        }
    
        public int IdAvance { get; set; }
        public int IdProyecto { get; set; }
        public int IdEntregable { get; set; }
        public int IdTarea { get; set; }
        public string Descripcion { get; set; }
        public System.DateTime FechaAvance { get; set; }
        public decimal PresupuestoUtilizado { get; set; }
        public int IdUsuarioIns { get; set; }
        public System.DateTime FechaIns { get; set; }
        public Nullable<int> IdUsuarioUpd { get; set; }
        public Nullable<System.DateTime> FechaUpd { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetAvancePreguntas> DetAvancePreguntas { get; set; }
        public virtual MstTareas MstTareas { get; set; }
    }
}
