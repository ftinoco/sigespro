﻿using Common.Extensions;
using Common.Log;
using Common.Helpers;
using Sigespro.Entity;
using Sigespro.Entity.Resource;
using Sigespro.Entity.Custom;
using Sigespro.Service;
using Sigespro.Service.Pro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace Sigespro.API.Controllers
{
    [RoutePrefix("Api/Project")]
    public class ProjectController : ApiController
    {
        ProjectService _service;

        public ProjectController(Func<string, IService> serviceFactory)
        {
            _service = (ProjectService)serviceFactory("Project");
        }

        [HttpGet]
        [Route("")]
        [Route("list")]
        public IEnumerable<Project> GetProjects()
        {
            var result = _service.GetProjects();

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data;
        }

        [HttpGet]
        [Route("{projectId}")]
        public MstProyectos GetProjectById(int projectId)
        {
            var result = _service.GetProjectById(projectId);

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data;
        }

        [HttpGet]
        [Route("list/{filter}")]
        public IEnumerable<Project> GetProjects(string filter)
        {
            var result = _service.GetProjects(filter);

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data;
        }

        //[HttpPost]
        //[Route("")]
        //public HttpResponseMessage AddProject()
        //{
        //    HttpResponseMessage response = new HttpResponseMessage();

        //    return response;
        //}

        [HttpPost]
        [Route("")]
        public HttpResponseMessage AddProject(MstProyectos project)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            response = Request.CreateResponse(HttpStatusCode.Created, project);

            //string uri = Url.Link("DefaultApi", new
            //{
            //    parameter1 = project.IdProyecto,
            //});

            //response.Headers.Location = new Uri(uri);







            //if (ModelState.IsValid)
            //{
            //    ResultBase result = new ResultBase();

            //    try
            //    {
            //        result = _service.InsertProject(project);

            //        if (result.MessageType == MessageType.SUCCESS)
            //        {
            //            response = Request.CreateResponse(HttpStatusCode.Created, project);

            //            string uri = Url.Link("DefaultApi", new
            //            {
            //                parameter1 = project.IdProyecto,
            //            });

            //            response.Headers.Location = new Uri(uri);
            //        }
            //        else
            //        {
            //            if (result.MessageType == MessageType.WARNING)
            //                response = Request.CreateErrorResponse(HttpStatusCode.Conflict, result.Message);
            //            else
            //                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, result.Message);

            //            throw result.DetailException;
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        Logger.Log(result.MessageType, ex, "Yo xD", GetType().Name, Request.GetClientIpAddress());
            //    }
            //}
            //else
            //    response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.RequiredData);

            return response;
        }




        [HttpPut]
        [Route("{projectId}")]
        public HttpResponseMessage UpdateParameter(int projectId, CatParametros parameter)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            //if (ModelState.IsValid)
            //{
            //    ResultBase result = new ResultBase();

            //    try
            //    {
            //        if (!projectId.Equals(parameter.IdPerametro))
            //            response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.NoMatchesUpdate);
            //        else
            //        {
            //            result = _service.u(parameter);

            //            if (result.MessageType == MessageType.SUCCESS)
            //                response = Request.CreateResponse(HttpStatusCode.OK);
            //            else
            //            {
            //                if (result.MessageType == MessageType.WARNING)
            //                    response = Request.CreateErrorResponse(HttpStatusCode.NotFound, result.Message);

            //                else if (result.MessageType == MessageType.ERROR)
            //                    response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, result.Message);

            //                throw result.DetailException;
            //            }
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        Logger.Log(result.MessageType, ex, "Yo xD", GetType().Name, Request.GetClientIpAddress());
            //    }
            //}
            //else
            //    response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.RequiredData);

            return response;
        }
    }
}
