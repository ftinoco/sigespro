﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Mailer
{
    public class ConfigDataMailing
    {
        public string SMTP { get; set; }

        public int Port { get; set; }

        public string EmailAddress { get; set; }

        public string Password { get; set; }

        public bool EnableSSL { get; set; }
    }

    public class EmailAttachment
    {
        public string Name { get; set; }

        public string Type { get; set; }

        public string PathFile { get; set; }

        public MemoryStream File { get; set; }


        public EmailAttachment(string _name, string _type, MemoryStream _file, string _pathFile)
        {
            Name = _name;
            Type = _type;
            File = _file;
            PathFile = _pathFile;
        }
    }

}
