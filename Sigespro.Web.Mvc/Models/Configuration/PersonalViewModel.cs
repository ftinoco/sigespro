﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sigespro.Web.Mvc.Models.Configuration
{
    public class ListPersonalViewModel
    {
        public int IdPersonal { get; set; }

        public int IdPersona { get; set; }

        public string Nombre { get; set; }

        public string Identificacion { get; set; }

        public int? IdPrograma { get; set; }

        public string NombrePrograma { get; set; }

        public int? IdInstitucion { get; set; }

        public string NombreInstitucion { get; set; }

        public int IdCargo { get; set; }

        public string Cargo { get; set; }

        public bool Activo { get; set; }

        public bool Selected { get; set; }
    }

    public class RptProjectManagers
    {
        public int IdPersonal { get; set; }

        public int IdPersona { get; set; }

        public string Nombre { get; set; }

        public string Identificacion { get; set; }

        public int IdPrograma { get; set; }

        public string NombrePrograma { get; set; }
        
        public int IdCargo { get; set; }

        public string Cargo { get; set; }

        public bool Activo { get; set; }
    }
}