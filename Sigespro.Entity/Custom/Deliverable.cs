﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sigespro.Entity.Custom
{
    public class Deliverable
    {
        public int IdEntregable { get; set; }

        public int IdProyecto { get; set; }
        
        public string Descripcion { get; set; }

        public DateTime FechaInicio { get; set; }

        public DateTime FechaFin { get; set; }

        public decimal DuracionEstimadaHoras { get; set; }

        public decimal DuracionEstimadaDias { get; set; }

        public decimal HorasDedicadas { get; set; }

        public int IdEstado { get; set; }

        public string Nota { get; set; }
        
        public decimal Presupuesto { get; set; }

        public int IdUsuarioIns { get; set; }

        public DateTime FechaIns { get; set; }

        public int? IdUsuarioUpd { get; set; }
        
        public DateTime? FechaUpd { get; set; }
    }
}