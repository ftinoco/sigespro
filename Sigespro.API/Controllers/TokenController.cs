﻿using Common.Extensions;
using Common.Log;
using Common.Helpers;
using Sigespro.API.Models;
using Sigespro.Entity;
using Sigespro.Service.Admin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace Sigespro.API.Controllers
{
    [RoutePrefix("Api/Token")]
    public class TokenController : ApiController
    {
        //private readonly ITokenService _tokenServices;

        //public TokenController(ITokenService tokenServices)
        //{
        //    _tokenServices = tokenServices;
        //}

        ///// <summary>
        ///// Authenticates user and returns token with expiry.
        ///// </summary>
        ///// <returns></returns>
        ////[POST("login")]
        ////[POST("authenticate")]
        ////[POST("get/token")]
        //[Route("login")]
        //public HttpResponseMessage Authenticate()
        //{
        //    if (System.Threading.Thread.CurrentPrincipal != null && System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated)
        //    {
        //        var basicAuthenticationIdentity = System.Threading.Thread.CurrentPrincipal.Identity as BasicAuthenticationIdentity;

        //        if (basicAuthenticationIdentity != null)
        //        {
        //            var userId = basicAuthenticationIdentity.UserId;
        //            return GetAuthToken(userId);
        //        }
        //    }

        //    return null;
        //}

        ///// <summary>
        ///// Returns auth token for the validated user.
        ///// </summary>
        ///// <param name="userId"></param>
        ///// <returns></returns>
        //private HttpResponseMessage GetAuthToken(int userId)
        //{
        //    HttpResponseMessage response = new HttpResponseMessage();

        //    Result<Tokens> result = new Result<Tokens>();

        //    try
        //    {
        //        result = _tokenServices.GenerateToken(userId);

        //        if (result.MessageType == MessageType.SUCCESS)
        //        {
        //            var token = result.Data;

        //            response = Request.CreateResponse(HttpStatusCode.OK, "Authorized");

        //            response.Headers.Add("Token", token.AuthToken.ToString());

        //            response.Headers.Add("TokenExpiry", ConfigurationManager.AppSettings["AuthTokenExpiry"]);

        //            response.Headers.Add("Access-Control-Expose-Headers", "Token,TokenExpiry");
        //        }
        //        else
        //        {
        //            response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, result.Message);

        //            throw result.DetailException;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Log(result.MessageType, ex, "Yo xD", GetType().Name, Request.GetClientIpAddress());
        //    }

        //    return response;
        //}
    }
}
