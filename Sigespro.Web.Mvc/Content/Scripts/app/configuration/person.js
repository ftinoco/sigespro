﻿(function (app) {

    var module = {};

    module.addAddress = function (e) {
        e.preventDefault();

        var valid = false;
        var frm = e.target.form;

        var form = $(frm).removeData("validator").removeData("unobtrusiveValidation");

        $.validator.unobtrusive.parse(form);
        var validator = form.validate();

        validator.settings.ignore = '.valid';

        $('input, select').not('#addressContainer input, #addressContainer select').addClass('valid');

        $.each($("input[id*='Direccion']"), function (index, input) {
            if (index > 0)
                valid = validator.element(input) && valid;
            else
                valid = validator.element(input);
        });

        $.each($("select[id*='IdTipoDireccion']"), function (index, select) {
            valid = validator.element(select) && valid;
        });

        if (valid) {
            this.changeAddress(e);
        }
    };

    module.addPhone = function (e) {
        e.preventDefault();

        var valid = false;
        var frm = e.target.form;

        var form = $(frm).removeData("validator").removeData("unobtrusiveValidation");

        $.validator.unobtrusive.parse(form);
        var validator = form.validate();

        validator.settings.ignore = '.valid';

        $('input, select').not("input[id*='__NumeroTelefono'], select[id*='IdTipoNumero']").addClass('valid');

        $.each($("input[id*='__NumeroTelefono']"), function (index, input) {
            if (index > 0)
                valid = validator.element(input) && valid;
            else
                valid = validator.element(input);
        });

        $.each($("select[id*='IdTipoNumero']"), function (index, select) {
            valid = validator.element(select) && valid;
        });

        if (valid) {
            this.changePhone(e);
        }
    };

    module.addEmail = function (e) {
        e.preventDefault();

        var valid = false;
        var frm = e.target.form;

        var form = $(frm).removeData("validator").removeData("unobtrusiveValidation");

        $.validator.unobtrusive.parse(form);
        var validator = form.validate();

        validator.settings.ignore = '.valid';

        $('input, select').not('#emailContainer input').addClass('valid');

        $.each($("input[id*=Email]"), function (index, input) {
            if (index > 0)
                valid = validator.element(input) && valid;
            else
                valid = validator.element(input);
        });

        if (valid) {
            this.changeEmail(e);
        }
    };

    module.changeAddress = function (e) {
        var action = $(e.target).data("action");

        var frmData = new FormData(e.target.form);

        //for (var key of frmData.keys()) {
        //    console.log(key + " => " + frmData.get(key));
        //}

        $.ajax({
            type: "POST",
            url: action,
            data: frmData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                app.toggleLoadingModal();
            },
            success: function (response) {

                $(e.target.form).resetValidation();
                $('input, select').removeClass('valid'); 
                $("#addressContainer").html(response);

            },
            error: function (data) {
                console.log(data.responseText);
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body').html(app.MESSAGE_ERROR);
            },
            statusCode: {
                401: function () {
                    window.location.replace(urlBase + "Account/LogOut");
                }
            }
        }).always(function () {
            app.toggleLoadingModal();
        });
    };

    module.changePhone = function (e) {
        var action = $(e.target).data("action");

        var frmData = new FormData(e.target.form);

        $.ajax({
            type: "POST",
            url: action,
            data: frmData,
            contentType: false,
            processData: false,
            cache: false,
            dataType: 'html',
            beforeSend: function () {
                app.toggleLoadingModal();
            },
            success: function (response) {

                $(e.target.form).resetValidation();
                $('input, select').removeClass('valid'); 
                $("#phoneContainer").html(response);

            },
            error: function (data) {
                console.log(data.responseText);
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body').html(app.MESSAGE_ERROR);
            },
            statusCode: {
                401: function () {
                    window.location.replace(urlBase + "Account/LogOut");
                }
            }
        }).always(function () {
            app.toggleLoadingModal();
        });
    };

    module.changeEmail = function (e) {
        var action = $(e.target).data("action");

        var frmData = new FormData(e.target.form);

        $.ajax({
            type: "POST",
            url: action,
            data: frmData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                app.toggleLoadingModal();
            },
            success: function (response) {

                $(e.target.form).resetValidation();
                $('input, select').removeClass('valid'); 
                $("#emailContainer").html(response);

            },
            error: function (data) {
                console.log(data.responseText);
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body').html(app.MESSAGE_ERROR);
            },
            statusCode: {
                401: function () {
                    window.location.replace(urlBase + "Account/LogOut");
                }
            }
        }).always(function () {
            app.toggleLoadingModal();
        });
    };

    module.deleteAddress = function (e) {
        e.preventDefault();

        this.changeAddress(e);
    };

    module.deletePhone = function (e) {
        e.preventDefault();

        this.changePhone(e);
    };

    module.deleteEmail = function (e) {
        e.preventDefault();

        this.changeEmail(e);
    };

    module.savePerson = function (e) {
        e.preventDefault();
        $.validator.setDefaults({ ignore: null });

        if ($(e.target.form).ajaxValidate()) { 
            $(e.target.form).submit();
        }
    };

    app.person = module;

}(sigesproApp || {}));