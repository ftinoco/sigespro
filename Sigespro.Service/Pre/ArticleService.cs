﻿using Common;
using Common.Extensions;
using Common.Helpers;
using Common.Log;

using Sigespro.DAL.Implementations;
using Sigespro.DAL.Interface;
using Sigespro.Entity;
using Sigespro.Entity.Resource;
using Sigespro.Service.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Common.Helpers.Utilities;

namespace Sigespro.Service.Pre
{
    public class ArticleService : Service
    {
        private Repository<CatArticulos> Repository { get; set; }

        public ArticleService() : base()
        {
            Repository = _unitOfWork.GetRepository<CatArticulos>();
        }

        public Result<IEnumerable<object>> GetArticles()
        {
            Result<IEnumerable<object>> result = new Result<IEnumerable<object>>();

            try
            {
                var repositoryDetCatalogo = _unitOfWork.GetRepository<DetCatalogo>();

                result.Data = repositoryDetCatalogo.GetAll().Join(Repository.GetAll(),
                            det => det.IdDetCatalogo, art => art.IdUnidadMedida,
                            (det, art) => new
                            {
                                IdArticulo = art.IdArticulo,
                                IdRubro = art.IdRubro,
                                IdUnidadMedida = art.IdUnidadMedida,
                                IdMoneda = art.IdMoneda,
                                Descripcion = art.Descripcion,
                                Precio = art.Precio,
                                Activo = art.Activo,
                                IdUsuarioIns = art.IdUsuarioIns,
                                FechaIns = art.FechaIns,
                                IdUsuarioUpd = art.IdUsuarioUpd,
                                FechaUpd = art.FechaUpd,
                                CatMoneda = art.CatMoneda,
                                CatRubros = art.CatRubros,
                                DetCatalogo = det
                            });

                result.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<IEnumerable<object>> GetArticles(string filter)
        {
            Result<IEnumerable<object>> result = new Result<IEnumerable<object>>();

            try
            {
                bool active = false;

                string filterToLower = filter.Trim().ToLower();

                var repositoryDetCatalogo = _unitOfWork.GetRepository<DetCatalogo>();

                var anonymous = repositoryDetCatalogo.GetAll().Join(Repository.GetAll(),
                            det => det.IdDetCatalogo, art => art.IdUnidadMedida,
                            (det, art) => new
                            {
                                IdArticulo = art.IdArticulo,
                                IdRubro = art.IdRubro,
                                IdUnidadMedida = art.IdUnidadMedida,
                                IdMoneda = art.IdMoneda,
                                Descripcion = art.Descripcion,
                                Precio = art.Precio,
                                Activo = art.Activo,
                                IdUsuarioIns = art.IdUsuarioIns,
                                FechaIns = art.FechaIns,
                                IdUsuarioUpd = art.IdUsuarioUpd,
                                FechaUpd = art.FechaUpd,
                                CatMoneda = art.CatMoneda,
                                CatRubros = art.CatRubros,
                                DetCatalogo = det
                            });

                if (filterToLower == "activo" || filterToLower == "inactivo")
                {
                    active = filterToLower == "activo" ? true : false;
                    result.Data = anonymous.Where(a => a.Activo == active);
                }

                result.Data = anonymous.Where(a => a.CatRubros.Nombre.Trim().ToLower().Contains(filterToLower) ||
                                            a.Descripcion.Trim().ToLower().Contains(filterToLower) ||
                                            a.DetCatalogo.Descripcion.Trim().ToLower().Contains(filterToLower) ||
                                            a.Precio.ToString().Trim().ToLower().Contains(filterToLower));

                result.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<CatArticulos> GetArticleById(string idArticle, string idEntry)
        {
            Result<CatArticulos> result = new Result<CatArticulos>();

            try
            {
                result.Data = Repository.GetById(idArticle, idEntry);
                result.Message = Messages.SuccessGetResult;
                result.MessageType = MessageType.INFO;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<List<CatArticulos>> GetArticleByEnty(string entryCode)
        {
            Result<List<CatArticulos>> result = new Result<List<CatArticulos>>();

            try
            {
                result.Data = Repository.GetMany(x => x.IdRubro.Equals(entryCode)).ToList();
                result.Message = Messages.SuccessGetResult;
                result.MessageType = MessageType.INFO;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public void GetArticlesForList(ref QueryOptions<IEnumerable<vwArticulos>> queryOpt)
        {
            Result<IEnumerable<vwArticulos>> result = new Result<IEnumerable<vwArticulos>>();

            try
            {
                string orderBy = "IdArticulo";

                var predicate = PredicateBuilder.True<vwArticulos>();

                var filter = queryOpt.Filters.FirstOrDefault(f => f.Name.Equals("Search"));

                if (!IsNull(filter) && !string.IsNullOrEmpty(filter.Value))
                {
                    decimal decimalFilter;

                        predicate = predicate.And(p => p.IdRubro.ToLower().Trim().Contains(filter.Value.ToLower().Trim()) || 
                                                       p.Moneda.ToLower().Trim().Contains(filter.Value.ToLower().Trim()) ||
                                                       p.IdArticulo.ToLower().Trim().Contains(filter.Value.ToLower().Trim()) || 
                                                       p.Rubro.ToLower().Trim().Contains(filter.Value.ToLower().Trim()) ||
                                                       p.Descripcion.ToLower().Trim().Contains(filter.Value.ToLower().Trim()) || 
                                                      p.UnidadMedida.ToLower().Trim().Contains(filter.Value.ToLower().Trim()));

                    if (decimal.TryParse(filter.Value, out decimalFilter))
                        predicate = predicate.Or(p => p.Precio == decimalFilter);

                    queryOpt.TotalRecords = _unitOfWork.GetRepository<vwArticulos>().Count(predicate);
                }
                else
                    queryOpt.TotalRecords = _unitOfWork.GetRepository<vwArticulos>().Count();

                if (!string.IsNullOrEmpty(queryOpt.SortField))
                    orderBy = queryOpt.SortField;

                result.Data = _unitOfWork.GetRepository<vwArticulos>().GetManyOnDemand(predicate, orderBy, queryOpt.CurrentPageIndex, queryOpt.RowsPerPage, queryOpt.SortOrder);

                result.Message = Messages.SuccessGetResultSet;

                result.MessageType = MessageType.INFO;
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorGetResultSet, "artículos");
                result.MessageType = MessageType.ERROR;
                result.DetailException = ex;
            }

            queryOpt.ResultData = result;
        }

        public ResultBase InsertArticle(CatArticulos article)
        {
            ResultBase result = new ResultBase();

            try
            {
                var sequencesService = new SequenceService(_unitOfWork.GetRepository<MstSecuencias>());

                var resultValidate = Utilities.ValidateEntity(article);

                if (resultValidate.MessageType == MessageType.INFO)
                    result = resultValidate;
                else
                {
                    MstSecuencias seq = sequencesService.GetNextSequenceByTableName("[pre].[CatArticulos]");

                    int idArticle = seq.SecuencialTabla;

                    article.IdArticulo = idArticle.FormatCode(5);
                    article.IdRubro = int.Parse(article.IdRubro).FormatCode(5);

                    var articleTemp = Repository.Get(a => a.IdRubro == article.IdRubro && a.IdArticulo == article.IdArticulo);

                    if (articleTemp == null)
                    {
                        article.FechaIns = DateTime.Now;
                        //article.IdUsuarioIns = 1;

                        Repository.Add(article);

                        sequencesService.InsertSequence(seq);

                        _unitOfWork.Commit();
                    }
                    else
                    {
                        result.MessageType = MessageType.WARNING;
                        result.Message = "El artículo con el identificador especificado ya existe.";
                        result.DetailException = new Exception(result.Message);
                    }
                }

                result.Message = Messages.SuccessSaveData;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message =string.Format( Messages.ErrorSaveData, "artículo");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase UpdateArticle(CatArticulos article)
        {
            ResultBase result = new ResultBase();

            try
            {
                string idRubro;

                if (article.IdRubro.Length == 5)
                    idRubro = article.IdRubro;
                else
                    idRubro = Convert.ToInt32(article.IdRubro).FormatCode(5);

                var articleTemp = Repository.Get(a => a.IdRubro == idRubro && a.IdArticulo == article.IdArticulo);

                if (articleTemp != null)
                {
                    var resultValidate = Utilities.ValidateEntity(article);

                    if (resultValidate.MessageType == MessageType.WARNING)
                        result = resultValidate;
                    else
                    {
                        //article.IdUsuarioUpd = 1;
                        article.IdRubro = idRubro;
                        article.FechaUpd = DateTime.Now;

                        Repository.Update(article);

                        _unitOfWork.Commit();
                    }
                }
                else
                {
                    result.MessageType = MessageType.WARNING;
                    result.Message = string.Format("El artículo con el identificador especificado no existe. IdRubro {0}, IdArticulo {1}", idRubro, article.IdArticulo);
                    result.DetailException = new Exception(result.Message);
                }

                result.Message = Messages.SuccessUpdateData;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorUpdateData, "artículo");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }
    }
}
