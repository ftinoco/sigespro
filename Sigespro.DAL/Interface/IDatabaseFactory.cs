﻿using Sigespro.Entity;
using System;

namespace Sigespro.DAL.Interface
{
    public interface IDatabaseFactory: IDisposable
    {
        SigesproEntities Get();
    }
}
