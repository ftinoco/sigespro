﻿using Common.Helpers;
using Common.Log;
using Newtonsoft.Json;
using Sigespro.Entity;
using Sigespro.Service.Admin;
using Sigespro.Web.Mvc.Models.Configuration;
using Sigespro.Web.Mvc.Resources;
using Sigespro.Web.Mvc.Utilities.Attributes.Security;
using Sigespro.Web.Mvc.Utilities.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Sigespro.Web.Mvc.Controllers.Configuration
{
    [RoutePrefix("Configuration/Holidays")] 
    public class HolidayController : Controller
    {
        private readonly IUserSession _userSession;

        public HolidayService Service { get; set; }

        public HolidayController()
        {
            Service = new HolidayService();
            _userSession = new UserSession();
        }

        [Route]
        public ActionResult Index(QueryOptions<IEnumerable<HolidayViewModel>> queryOpt)
        {
            string message = string.Empty;
            IEnumerable<HolidayViewModel> model = new List<HolidayViewModel>();

            try
            {
                string strSerialize = JsonConvert.SerializeObject(queryOpt);

                QueryOptions<IEnumerable<CatFeriados>> filter = JsonConvert.DeserializeObject<QueryOptions<IEnumerable<CatFeriados>>>(strSerialize);
                Service.GetHolidaysForList(ref filter);

                if (filter.ResultData.MessageType != MessageType.INFO)
                {
                    message = filter.ResultData.Message;
                    Logger.Log(filter.ResultData.MessageType, filter.ResultData.DetailException, _userSession.Username,
                               MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                {
                    strSerialize = JsonConvert.SerializeObject(filter);

                    queryOpt = JsonConvert.DeserializeObject<QueryOptions<IEnumerable<HolidayViewModel>>>(strSerialize);

                    model = queryOpt.ResultData.Data;
                }
            }
            catch (Exception ex)
            {
                message = Messages.LoadPageError;
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            if (!string.IsNullOrEmpty(message))
                ModelState.AddModelError(Messages.AlertDanger, message);

            ViewBag.Filter = true;
            ViewBag.QueryOptions = queryOpt;

            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [Route("Add")]
        public ActionResult AddHoliday()
        {
            HolidayViewModel model = new HolidayViewModel();

            ViewBag.IsEdit = false;
            ViewBag.Action = "SaveHoliday";
            ViewBag.Title = "Nuevo Día Feriado";

            model.IdUsuarioIns = 1;
            model.FechaIns = DateTime.Now;
            model.FechaFeriado = DateTime.Now;

            return View(model);
        }

        [Route("Edit/{holidayId}")]
        public ActionResult EditHoliday(int holidayId)
        {
            string message = string.Empty;
            HolidayViewModel model = new HolidayViewModel();

            try
            {
                var result = Service.GetHolidayById(holidayId);

                if (result.MessageType != MessageType.INFO)
                    message = result.Message;
                else
                {
                    model = new HolidayViewModel
                    {
                        Activo = result.Data.Activo,
                        FechaFeriado = result.Data.FechaFeriado,
                        FechaIns = result.Data.FechaIns,
                        FechaUpd = result.Data.FechaUpd,
                        IdFeriado = result.Data.IdFeriado,
                        IdUsuarioIns = result.Data.IdUsuarioIns,
                        IdUsuarioUpd = result.Data.IdUsuarioUpd,
                        MotivoFeriado = result.Data.MotivoFeriado
                    };
                }
            }
            catch (Exception ex)
            {
                message = Messages.ErrorApi;
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                           MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            if (!string.IsNullOrEmpty(message))
                ModelState.AddModelError(Messages.AlertDanger, message);

            ViewBag.IsEdit = true;
            ViewBag.Action = "UpdateHoliday";
            ViewBag.Title = "Editar Día Feriado";

            if (!model.FechaUpd.HasValue)
                model.FechaUpd = DateTime.Now;

            if (!model.IdUsuarioUpd.HasValue)
                model.IdUsuarioUpd = 1;

            return View("AddHoliday", model);
        }

        [AjaxOnly]
        [HttpPost]
        [Route("Save")]
        [AllowAnonymous]
        public ActionResult SaveHoliday(HolidayViewModel model)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                model.IdUsuarioIns = _userSession.UserId;

                string strSerialize = JsonConvert.SerializeObject(model);

                var result = Service.InsertHoliday(JsonConvert.DeserializeObject<CatFeriados>(strSerialize));

                if (result.MessageType != MessageType.SUCCESS)
                {
                    message = result.Message;
                    status = result.MessageType.ToString();
                    Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                    message = string.Format(Messages.SaveSuccess, "día feriado");
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                             MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message, action = Url.Action("Index", "Holiday") },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [AjaxOnly]
        [HttpPost]
        [AllowAnonymous]
        [Route("Update")]
        public ActionResult UpdateHoliday(HolidayViewModel model)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                model.IdUsuarioUpd = _userSession.UserId;
                string strSerialize = JsonConvert.SerializeObject(model);

                var result = Service.UpdateHoliday(JsonConvert.DeserializeObject<CatFeriados>(strSerialize));

                if (result.MessageType != MessageType.SUCCESS)
                {
                    message = result.Message;
                    status = result.MessageType.ToString();
                    Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                    message = string.Format(Messages.UpdateSuccess, "día feriado");
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                             MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message, action = Url.Action("Index", "Holiday") },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }
}