﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sigespro.Web.Mvc.Utilities
{
    public class Constants
    {
        public const string API_ENTRY = "Api/Entry/";

        public const string API_ARTICLE = "Api/Article/";

        public const string API_CATALOG = "Api/Catalog/";

        public const string API_HOLIDAY = "Api/Holiday/";

        public const string API_PROGRAM = "Api/Program/";

        public const string API_SECURITY = "Api/Security/";

        public const string API_CURRENCY = "Api/Currency/";

        public const string API_PERSONAL = "Api/Personal/";

        public const string API_PARAMETER = "Api/Parameter/";

        public const string API_DETAILCATALOG = "Api/DetailCatalog/";
    }
}