﻿using Sigespro.Entity.Resource;
using System.ComponentModel.DataAnnotations;

namespace Sigespro.Entity.Metadata.Config
{
    public class CatMonedaMetadata
    {
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string Nombre { get; set; }

        [Display(Name = "Simbolo", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string Simbolo { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public bool EsLocal { get; set; }
    }
}
