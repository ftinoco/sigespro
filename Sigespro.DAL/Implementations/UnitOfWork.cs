﻿using Sigespro.DAL.Interface;
using Sigespro.Entity;
using System;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;

namespace Sigespro.DAL.Implementations
{
    public class UnitOfWork: IUnitOfWork, IDisposable
    {
        private readonly IDatabaseFactory databaseFactory;

        private SigesproEntities context;

        public UnitOfWork()
        {
            databaseFactory = new DatabaseFactory();
        }

        protected SigesproEntities DataContext
        {
            get { return context ?? (context = databaseFactory.Get()); }
        }
        
        public Repository<T> GetRepository<T>() where T: class
        {
            Repository<T> repository = new Repository<T>((DatabaseFactory)databaseFactory);

            return repository;
        }
        
        public void Commit()
        { 
            bool saveFailed;
            do
            {
                saveFailed = false;
                try
                {
                    DataContext.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    saveFailed = true;

                    // Update original values from the database
                    var entry = ex.Entries.Single();
                    entry.OriginalValues.SetValues(entry.GetDatabaseValues());
                }

            } while (saveFailed);
            /*
            try
            {
                
                DataContext.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                throw dbEx;
                //foreach (var validationErrors in dbEx.EntityValidationErrors)
                //{
                //    foreach (var validationError in validationErrors.ValidationErrors)
                //    {
                //        Trace.TraceInformation("Class: {0}, Property: {1}, Error: {2}", validationErrors.Entry.Entity.GetType().FullName,
                //                      validationError.PropertyName, validationError.ErrorMessage);
                //    }
                //}
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw ex;
            }*/
        }

        public void Rollback()
        {
            DataContext
                .ChangeTracker
                .Entries()
                .ToList()
                .ForEach(x => x.Reload());
        }

        public void Dispose()
        {
            if (databaseFactory != null)
                databaseFactory.Dispose();
        }
    }
}
