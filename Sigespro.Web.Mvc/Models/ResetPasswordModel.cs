﻿using Sigespro.Web.Mvc.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sigespro.Web.Mvc.Models
{
    public class ResetPasswordModel
    {
        /// <summary>
        /// Get/Set for UserName
        /// </summary>
        [Display(Name = "UserName", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string UserName { get; set; }

        [Display(Name = "Email", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessageResourceName = "EmailInvalid", ErrorMessageResourceType = typeof(Messages))]
        public string EmailAddress { get; set; }
    }
}