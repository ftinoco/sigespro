﻿using Common.Extensions;
using Common.Helpers;
using Common.Log;

using Sigespro.DAL.Implementations;
using Sigespro.Entity;
using Sigespro.Entity.Resource;
using Sigespro.Entity.Custom;
using Sigespro.Service.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using static Common.Helpers.Utilities;

namespace Sigespro.Service.Pre
{
    public class EntryService : Service
    {
        private Repository<CatRubros> Repository { get; set; }

        public EntryService() : base()
        {
            Repository = _unitOfWork.GetRepository<CatRubros>();
        }

        public Result<IEnumerable<Entry>> GetEntries()
        {
            Result<IEnumerable<Entry>> result = new Result<IEnumerable<Entry>>();

            try
            {
                var repositoryDetCatalogo = _unitOfWork.GetRepository<DetCatalogo>();

                result.Data = repositoryDetCatalogo.GetAll()
                            .Join(Repository.GetAll(), det => det.IdDetCatalogo, ent => ent.IdTipoRubro,
                                    (det, ent) => new { det, ent })
                            .GroupJoin(Repository.GetAll(), e => e.ent.IdRubroParent, p => p.IdRubro,
                                    (e, p) => new { e, p })
                            .SelectMany(x => x.p.DefaultIfEmpty(),
                                    (x, y) => new Entry
                                    {
                                        IdRubro = x.e.ent.IdRubro,
                                        Nombre = x.e.ent.Nombre,
                                        TipoRubro = x.e.det,
                                        RubroParent = y,
                                        Activo = x.e.ent.Activo,
                                        IdUsuarioIns = x.e.ent.IdUsuarioIns,
                                        FechaIns = x.e.ent.FechaIns,
                                        IdUsuarioUpd = x.e.ent.IdUsuarioUpd,
                                        FechaUpd = x.e.ent.FechaUpd
                                    });

                result.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<CatRubros> GetEntryById(string idEntry)
        {
            Result<CatRubros> result = new Result<CatRubros>();

            try
            {
                var data = Repository.GetById(idEntry);
                if(data != null)
                {
                    result.Data = data;
                    result.MessageType = MessageType.INFO;
                    result.Message = Messages.SuccessGetResult;
                }
                else
                {
                    result.MessageType = MessageType.WARNING;
                    result.Message = Messages.NoFoundResult;
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<IEnumerable<Entry>> GetEntries(string filter)
        {
            Result<IEnumerable<Entry>> result = new Result<IEnumerable<Entry>>();
            result.Message = Messages.SuccessGetResultSet;

            try
            {
                bool active = false;

                string idParent = string.Empty;

                string filterToLower = filter.Trim().ToLower();

                var resultEntries = GetEntries();

                if (resultEntries.MessageType == MessageType.SUCCESS)
                {
                    var entries = resultEntries.Data;

                    if (filterToLower == "activo" || filterToLower == "inactivo")
                    {
                        active = filterToLower == "activo" ? true : false;
                        goto byStatus;
                    }

                    var parent = Repository.Get(e => e.Nombre.Trim().ToLower().Contains(filterToLower));

                    if (parent != null)
                    {
                        idParent = parent.IdRubro;
                        goto byParent;
                    }

                    result.Data = entries.Where(e => e.IdRubro.Contains(filterToLower) || e.Nombre.Trim().ToLower().Contains(filterToLower)
                                        || e.TipoRubro.Descripcion.Trim().ToLower().Contains(filterToLower));
                    return result;

                    byParent:
                    result.Data = entries.Where(e => (e.RubroParent != null && e.RubroParent.IdRubroParent.Contains(idParent)) || e.IdRubro.Contains(filterToLower) ||
                                       e.Nombre.Trim().ToLower().Contains(filterToLower) || e.TipoRubro.Descripcion.Trim().ToLower().Contains(filterToLower));
                    return result;


                    byStatus:
                    result.Data = entries.Where(e => e.Activo == active);
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        /// <summary>
        /// Obtiene rubros por tipo.
        /// </summary>
        /// <param name="idEntry">Id del rubro que se excluira del filtro.</param>
        /// <param name="type">Tipo de rubro por el cual se filtrara.</param>
        /// <returns></returns>
        public Result<IEnumerable<CatRubros>> GetEntriesByType(string idEntry, string type)
        {
            Result<IEnumerable<CatRubros>> result = new Result<IEnumerable<CatRubros>>();

            try
            {
                DetailCatalogService detService = new DetailCatalogService();

                idEntry = idEntry.Equals(string.Empty) ? "0" : idEntry;

                var resultDet = detService.GetDetailByValue(type);

                if (resultDet.MessageType != MessageType.SUCCESS)
                {
                    result.MessageType = resultDet.MessageType;
                    result.DetailException = resultDet.DetailException;
                    result.Message = "No existen tipos de rubros registrados en el sistema.";
                }
                else
                {
                    var detGroup = resultDet.Data;
                    result.Data = Repository.GetMany(e => e.IdTipoRubro == detGroup.IdDetCatalogo &&
                                                    e.IdRubro.Trim() != idEntry.Trim() &&
                                                    e.Activo == true);
                }

                result.MessageType = MessageType.INFO;
                result.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public void GetEntriesForList(ref QueryOptions<IEnumerable<vwRubros>> queryOpt)
        {
            Result<IEnumerable<vwRubros>> result = new Result<IEnumerable<vwRubros>>();

            try
            {
                string orderBy = "IdRubro";

                var predicate = PredicateBuilder.True<vwRubros>();

                var filter = queryOpt.Filters.FirstOrDefault(f => f.Name.Equals("Search"));

                if (!IsNull(filter) && !string.IsNullOrEmpty(filter.Value))
                {
                    int intFilter;

                        predicate = predicate.And(p => p.IdRubro.ToLower().Trim().Contains(filter.Value.ToLower().Trim()) || 
                                                       p.Nombre.ToLower().Trim().Contains(filter.Value.ToLower().Trim()) ||
                                                       p.RubroParent.ToLower().Trim().Contains(filter.Value.ToLower().Trim()) || 
                                                       p.TipoRubro.ToLower().Trim().Contains(filter.Value.ToLower().Trim()));

                    if (int.TryParse(filter.Value, out intFilter))
                        predicate = predicate.Or(p => p.IdTipoRubro == intFilter);
                     
                    queryOpt.TotalRecords = _unitOfWork.GetRepository<vwRubros>().Count(predicate);
                }
                else
                    queryOpt.TotalRecords = _unitOfWork.GetRepository<vwRubros>().Count();

                if (!string.IsNullOrEmpty(queryOpt.SortField))
                    orderBy = queryOpt.SortField;

                result.Data = _unitOfWork.GetRepository<vwRubros>().GetManyOnDemand(predicate, orderBy, queryOpt.CurrentPageIndex, queryOpt.RowsPerPage, queryOpt.SortOrder);

                result.Message = Messages.SuccessGetResultSet;

                result.MessageType = MessageType.INFO;
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorGetResultSet, "rubros");
                result.MessageType = MessageType.ERROR;
                result.DetailException = ex;
            }

            queryOpt.ResultData = result;
        }

        public ResultBase InsertEntry(CatRubros entry)
        {
            ResultBase result = new ResultBase();

            try
            {
                entry.IdRubroParent = Convert.ToInt32(entry.IdRubroParent).FormatCode(5);

                entry.IdRubro = GenerateKey(entry, _unitOfWork, Repository);

                var resultValidate = Utilities.ValidateEntity(entry);

                if (resultValidate.MessageType == MessageType.WARNING)
                    result = resultValidate;
                else
                {
                    var entryTemp = Repository.Get(e => e.IdRubro.Trim() == entry.IdRubro.Trim());

                    if (entryTemp == null)
                    {
                        //entry.IdUsuarioIns = 1;
                        entry.FechaIns = DateTime.Now;

                        Repository.Add(entry);

                        _unitOfWork.Commit();
                    }
                    else
                    {
                        result.MessageType = MessageType.ERROR;
                        result.Message = "El rubro con el identificador especificado ya existe. IdRubro: " + entry.IdRubro;
                        result.DetailException = new Exception(result.Message);
                    }
                }


                result.Message = Messages.SuccessSaveData;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorSaveData, "rubro");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase UpdateEntry(CatRubros entry)
        {
            ResultBase result = new ResultBase();

            try
            {
                DetailCatalogService detService = new DetailCatalogService();

                var entryTemp = Repository.Get(e => e.IdRubro == entry.IdRubro);

                if (entryTemp != null)
                {
                    var entryChild = Repository.GetMany(e => e.IdRubroParent == entry.IdRubro);

                    var resultDet = detService.GetDetailsCatalog(Catalog.EntryGroup);

                    if (resultDet.MessageType != MessageType.SUCCESS)
                    {
                        result.MessageType = resultDet.MessageType;
                        result.DetailException = resultDet.DetailException;
                        result.Message = "No exiten tipos de rubros registrados en el sistema.";
                    }
                    else
                    {
                        var detGroup = resultDet.Data.FirstOrDefault();

                        if (!entry.Activo && (entry.IdTipoRubro == detGroup.IdDetCatalogo) && entryChild.Count() > 0)
                        {
                            result.Message = Messages.WithDependencies;
                            result.MessageType = MessageType.ERROR;
                            result.DetailException = new Exception(result.Message);
                        }
                        else
                        {
                            entry.IdRubroParent = Convert.ToInt32(entry.IdRubroParent).FormatCode(5);

                            //entry.IdUsuarioUpd = 1;
                            entry.FechaUpd = DateTime.Now;

                            Repository.Update(entry);

                            _unitOfWork.Commit();
                        }
                    }
                }
                else
                {
                    result.Message = Messages.NotExist;
                    result.MessageType = MessageType.ERROR;
                    result.DetailException = new Exception(result.Message);
                }

                result.Message = Messages.SuccessUpdateData;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorUpdateData, "rubro");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        private string GenerateKey(CatRubros entry, UnitOfWork unitOfWork, Repository<CatRubros> repository)
        {
            try
            {
                int idEntry = 0;

                if (entry.IdRubroParent == null)
                {
                    var sequencesService = new SequenceService(unitOfWork.GetRepository<MstSecuencias>());

                    MstSecuencias seq = sequencesService.GetNextSequenceByTableName("[pre].[CatRubros]");

                    idEntry = seq.SecuencialTabla;

                    sequencesService.InsertSequence(seq);
                }
                else
                {
                    var lastEntry = repository.GetMany(e => e.IdRubroParent == entry.IdRubroParent)
                                        .OrderByDescending(e => e.IdRubro)
                                        .FirstOrDefault();

                    if (lastEntry == null)
                        idEntry = Convert.ToInt32(entry.IdRubroParent + "1");
                    else
                        idEntry = Convert.ToInt32(lastEntry.IdRubro) + 1;
                }


                string code = idEntry.FormatCode(5);

                return code;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
