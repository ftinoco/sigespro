﻿using Sigespro.Web.Mvc.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sigespro.Web.Mvc.Models.Configuration
{
    public class PersonalViewModel
    {
        public int IdPersonal { get; set; }

        public int IdPersona { get; set; }

        [Display(Name = "Institution", ResourceType = typeof(Labels))]
        //[Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public int? IdInstitucion { get; set; }
                  
        [Display(Name = "Program", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public int? IdPrograma { get; set; }

        [Display(Name = "Degree", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public int IdCargo { get; set; }

        public bool External { get; set; }

        public PersonViewModel MstPersonas { get; set; }
    }


    public class PersonViewModel
    {
        public int IdPersona { get; set; }

        [Display(Name = "Identification", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string Identificacion { get; set; }

        [Display(Name = "Name", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string NombreCompleto { get; set; }

        [Display(Name = "Gender", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string SexoPersona { get; set; }

        [Display(Name = "Active", ResourceType = typeof(Labels))]
        public bool Activo { get; set; }

        [Display(Name = "UserInsert", ResourceType = typeof(Labels))]
        public int IdUsuarioIns { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "DateInsert", ResourceType = typeof(Labels))]
        public DateTime FechaIns { get; set; }

        [Display(Name = "UserEdit", ResourceType = typeof(Labels))]
        public int? IdUsuarioUpd { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "DateEdit", ResourceType = typeof(Labels))]
        public DateTime? FechaUpd { get; set; }

        public List<PersonPhoneViewModel> DetPersonaTelefono { get; set; }

        public List<PersonEmailViewModel> DetPersonaEmail { get; set; }

        public List<PersonAddressViewModel> DetPersonaDireccion { get; set; }
    }

    public class PersonPhoneViewModel
    {
        public int IdPersonaTelefono { get; set; }

        public int IdPersona { get; set; }

        [Display(Name = "PhoneNumberType", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public int IdTipoNumero { get; set; }

        [Display(Name = "PhoneNumber", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string NumeroTelefono { get; set; }

        [Display(Name = "Active", ResourceType = typeof(Labels))]
        public bool Activo { get; set; }

        [Display(Name = "UserInsert", ResourceType = typeof(Labels))]
        public int IdUsuarioIns { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "DateInsert", ResourceType = typeof(Labels))]
        public DateTime FechaIns { get; set; }

        [Display(Name = "UserEdit", ResourceType = typeof(Labels))]
        public int? IdUsuarioUpd { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "DateEdit", ResourceType = typeof(Labels))]
        public DateTime? FechaUpd { get; set; }
    }

    public class PersonAddressViewModel
    {
        public int IdPersonaDireccion { get; set; }

        public int IdPersona { get; set; }
         
        [Display(Name = "AddressType", ResourceType = typeof(Labels))]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public int IdTipoDireccion { get; set; }

        [Display(Name = "Address", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string Direccion { get; set; }

        [Display(Name = "Active", ResourceType = typeof(Labels))]
        public bool Activo { get; set; }

        [Display(Name = "UserInsert", ResourceType = typeof(Labels))]
        public int IdUsuarioIns { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "DateInsert", ResourceType = typeof(Labels))]
        public DateTime FechaIns { get; set; }

        [Display(Name = "UserEdit", ResourceType = typeof(Labels))]
        public int? IdUsuarioUpd { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "DateEdit", ResourceType = typeof(Labels))]
        public DateTime? FechaUpd { get; set; }
    }

    public class PersonEmailViewModel
    {
        public int IdPersonaEmail { get; set; }

        public int IdPersona { get; set; }

        //[DataType(DataType.EmailAddress)]
        [Display(Name = "Email", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages), AllowEmptyStrings = false)]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessageResourceName = "EmailInvalid", ErrorMessageResourceType = typeof(Messages))]
        public string Email { get; set; }

        [Display(Name = "Active", ResourceType = typeof(Labels))]
        public bool Activo { get; set; }

        [Display(Name = "UserInsert", ResourceType = typeof(Labels))]
        public int IdUsuarioIns { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "DateInsert", ResourceType = typeof(Labels))]
        public DateTime FechaIns { get; set; }

        [Display(Name = "UserEdit", ResourceType = typeof(Labels))]
        public int? IdUsuarioUpd { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "DateEdit", ResourceType = typeof(Labels))]
        public DateTime? FechaUpd { get; set; }
    }

}