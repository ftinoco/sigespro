USE [master]
GO
/****** Object:  Database [SIGESPRO_PRESENTACION]    Script Date: 19/10/13 6:57:29 PM ******/
CREATE DATABASE [SIGESPRO_PRESENTACION]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SIGESPRO_PRESENTACION', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\SIGESPRO_PRESENTACION.mdf' , SIZE = 4288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'SIGESPRO_PRESENTACION_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\SIGESPRO_PRESENTACION_log.ldf' , SIZE = 1088KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SIGESPRO_PRESENTACION].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET ARITHABORT OFF 
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET  ENABLE_BROKER 
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET  MULTI_USER 
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET DELAYED_DURABILITY = DISABLED 
GO
USE [SIGESPRO_PRESENTACION]
GO
/****** Object:  Schema [admin]    Script Date: 19/10/13 6:57:30 PM ******/
CREATE SCHEMA [admin]
GO
/****** Object:  Schema [config]    Script Date: 19/10/13 6:57:30 PM ******/
CREATE SCHEMA [config]
GO
/****** Object:  Schema [pre]    Script Date: 19/10/13 6:57:30 PM ******/
CREATE SCHEMA [pre]
GO
/****** Object:  Schema [pro]    Script Date: 19/10/13 6:57:30 PM ******/
CREATE SCHEMA [pro]
GO
/****** Object:  Schema [Security]    Script Date: 19/10/13 6:57:30 PM ******/
CREATE SCHEMA [Security]
GO
/****** Object:  Table [admin].[CatParametros]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [admin].[CatParametros](
	[IdPerametro] [int] IDENTITY(1,1) NOT NULL,
	[NombreParametro] [varchar](100) NOT NULL,
	[ValorParametro] [varchar](255) NOT NULL,
	[DescParametro] [varchar](255) NULL,
 CONSTRAINT [PK_CatParametros] PRIMARY KEY CLUSTERED 
(
	[IdPerametro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [admin].[DetCatalogo]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [admin].[DetCatalogo](
	[IdDetCatalogo] [int] NOT NULL,
	[IdMstCatalogo] [int] NOT NULL,
	[Valor] [varchar](255) NOT NULL,
	[Descripcion] [varchar](255) NULL,
	[Activo] [bit] NOT NULL,
	[IdUsuarioIns] [int] NOT NULL,
	[FechaIns] [datetime] NOT NULL,
	[IdUsuarioUpd] [int] NULL,
	[FechaUpd] [datetime] NULL,
 CONSTRAINT [PK_DetTablaCatalogo] PRIMARY KEY CLUSTERED 
(
	[IdDetCatalogo] ASC,
	[IdMstCatalogo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [admin].[Logger]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [admin].[Logger](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Thread] [varchar](255) NOT NULL,
	[Level] [varchar](50) NOT NULL,
	[Logger] [varchar](255) NOT NULL,
	[Message] [varchar](4000) NOT NULL,
	[Exception] [varchar](max) NULL,
	[User] [varchar](50) NULL,
	[Class] [varchar](200) NULL,
	[IP] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [admin].[MstCatalogo]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [admin].[MstCatalogo](
	[IdCatalogo] [int] NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Descripcion] [varchar](255) NULL,
	[Activo] [bit] NOT NULL,
	[IdUsuarioIns] [int] NOT NULL,
	[FechaIns] [datetime] NOT NULL,
	[IdUsuarioUpd] [int] NULL,
	[FechaUpd] [datetime] NULL,
 CONSTRAINT [PK_MstTablaCatalogos] PRIMARY KEY CLUSTERED 
(
	[IdCatalogo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [admin].[MstSecuencias]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [admin].[MstSecuencias](
	[NombreTabla] [varchar](100) NOT NULL,
	[SecuencialTabla] [int] NOT NULL,
	[DescTabla] [varchar](255) NULL,
 CONSTRAINT [PK_MstSecuencias] PRIMARY KEY CLUSTERED 
(
	[NombreTabla] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [config].[CatFeriados]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [config].[CatFeriados](
	[IdFeriado] [int] NOT NULL,
	[MotivoFeriado] [varchar](255) NOT NULL,
	[FechaFeriado] [datetime] NOT NULL,
	[Activo] [bit] NOT NULL,
	[IdUsuarioIns] [int] NOT NULL,
	[FechaIns] [datetime] NOT NULL,
	[IdUsuarioUpd] [int] NULL,
	[FechaUpd] [datetime] NULL,
 CONSTRAINT [Key3] PRIMARY KEY CLUSTERED 
(
	[IdFeriado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [config].[DetContactoBeneficiario]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [config].[DetContactoBeneficiario](
	[IdContactoBeneficiario] [int] NOT NULL,
	[IdPersona] [int] NOT NULL,
	[IdBeneficiario] [int] NOT NULL,
 CONSTRAINT [PK_DetContactoBeneficiario] PRIMARY KEY CLUSTERED 
(
	[IdContactoBeneficiario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [config].[DetContactoInstitucion]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [config].[DetContactoInstitucion](
	[IdContactoInstitucion] [int] NOT NULL,
	[IdPersona] [int] NOT NULL,
	[IdInstitucion] [int] NOT NULL,
 CONSTRAINT [Key2] PRIMARY KEY CLUSTERED 
(
	[IdContactoInstitucion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [config].[DetHorario]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [config].[DetHorario](
	[IdDetHorario] [int] NOT NULL,
	[IdHorario] [int] NOT NULL,
	[Dia] [int] NOT NULL,
	[HoraEntrada] [time](7) NOT NULL,
	[HoraSalida] [time](7) NOT NULL,
 CONSTRAINT [PK_DetHorario] PRIMARY KEY CLUSTERED 
(
	[IdDetHorario] ASC,
	[IdHorario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [config].[DetPersonaDireccion]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [config].[DetPersonaDireccion](
	[IdPersonaDireccion] [int] NOT NULL,
	[IdPersona] [int] NOT NULL,
	[IdTipoDireccion] [int] NOT NULL,
	[Direccion] [nvarchar](500) NOT NULL,
	[Activo] [bit] NOT NULL,
	[IdUsuarioIns] [int] NOT NULL,
	[FechaIns] [datetime] NOT NULL,
	[IdUsuarioUpd] [int] NULL,
	[FechaUpd] [datetime] NULL,
 CONSTRAINT [PK_DetPersonaDireccion] PRIMARY KEY CLUSTERED 
(
	[IdPersonaDireccion] ASC,
	[IdPersona] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [config].[DetPersonaEmail]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [config].[DetPersonaEmail](
	[IdPersonaEmail] [int] NOT NULL,
	[IdPersona] [int] NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[Activo] [bit] NOT NULL,
	[IdUsuarioIns] [int] NOT NULL,
	[FechaIns] [datetime] NOT NULL,
	[IdUsuarioUpd] [int] NULL,
	[FechaUpd] [datetime] NULL,
 CONSTRAINT [PK_DetPersonaEmail] PRIMARY KEY CLUSTERED 
(
	[IdPersonaEmail] ASC,
	[IdPersona] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [config].[DetPersonaTelefono]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [config].[DetPersonaTelefono](
	[IdPersonaTelefono] [int] NOT NULL,
	[IdPersona] [int] NOT NULL,
	[IdTipoNumero] [int] NOT NULL,
	[NumeroTelefono] [varchar](20) NOT NULL,
	[Activo] [bit] NOT NULL,
	[IdUsuarioIns] [int] NOT NULL,
	[FechaIns] [datetime] NOT NULL,
	[IdUsuarioUpd] [int] NULL,
	[FechaUpd] [datetime] NULL,
 CONSTRAINT [PK_DetPersonaTelefono] PRIMARY KEY CLUSTERED 
(
	[IdPersonaTelefono] ASC,
	[IdPersona] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [config].[MstInstituciones]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [config].[MstInstituciones](
	[IdInstitucion] [int] NOT NULL,
	[IdTipoInstitucion] [int] NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Siglas] [varchar](10) NULL,
	[SitioWeb] [varchar](50) NULL,
	[Telefono] [char](20) NULL,
	[Logo] [varchar](255) NULL,
	[Activo] [bit] NOT NULL,
	[IdUsuarioIns] [int] NOT NULL,
	[FechaIns] [datetime] NOT NULL,
	[IdUsuarioUpd] [int] NULL,
	[FechaUpd] [datetime] NULL,
 CONSTRAINT [Key1] PRIMARY KEY CLUSTERED 
(
	[IdInstitucion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [config].[MstPersonas]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [config].[MstPersonas](
	[IdPersona] [int] NOT NULL,
	[Identificacion] [varchar](20) NOT NULL,
	[NombreCompleto] [varchar](100) NOT NULL,
	[SexoPersona] [char](1) NOT NULL,
	[Activo] [bit] NOT NULL,
	[IdUsuarioIns] [int] NOT NULL,
	[FechaIns] [datetime] NOT NULL,
	[IdUsuarioUpd] [int] NULL,
	[FechaUpd] [datetime] NULL,
 CONSTRAINT [PK_MstPersonas] PRIMARY KEY CLUSTERED 
(
	[IdPersona] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [config].[MstPrograma]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [config].[MstPrograma](
	[IdPrograma] [int] NOT NULL,
	[NombrePrograma] [varchar](50) NOT NULL,
	[Descripcion] [varchar](255) NOT NULL,
	[Activo] [bit] NOT NULL,
	[IdUsuarioIns] [int] NOT NULL,
	[FechaIns] [datetime] NOT NULL,
	[IdUsuarioUpd] [int] NULL,
	[FechaUpd] [datetime] NULL,
 CONSTRAINT [PK_Programa] PRIMARY KEY CLUSTERED 
(
	[IdPrograma] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [pre].[CatArticulos]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [pre].[CatArticulos](
	[IdArticulo] [char](5) NOT NULL,
	[IdRubro] [char](5) NOT NULL,
	[IdUnidadMedida] [int] NOT NULL,
	[IdMoneda] [int] NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
	[Precio] [decimal](18, 4) NOT NULL,
	[Activo] [bit] NOT NULL,
	[IdUsuarioIns] [int] NOT NULL,
	[FechaIns] [datetime] NOT NULL,
	[IdUsuarioUpd] [int] NULL,
	[FechaUpd] [datetime] NULL,
 CONSTRAINT [PK_CatArticulos] PRIMARY KEY CLUSTERED 
(
	[IdArticulo] ASC,
	[IdRubro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [pre].[CatMoneda]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [pre].[CatMoneda](
	[IdMoneda] [int] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Simbolo] [char](3) NOT NULL,
	[EsLocal] [bit] NOT NULL,
	[Activo] [bit] NOT NULL,
	[IdUsuarioIns] [int] NOT NULL,
	[FechaIns] [datetime] NOT NULL,
	[IdUsuarioUpd] [int] NULL,
	[FechaUpd] [datetime] NULL,
 CONSTRAINT [PK_CatMoneda] PRIMARY KEY CLUSTERED 
(
	[IdMoneda] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [pre].[CatRubros]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [pre].[CatRubros](
	[IdRubro] [char](5) NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[IdTipoRubro] [int] NOT NULL,
	[IdRubroParent] [char](5) NULL,
	[Activo] [bit] NOT NULL,
	[IdUsuarioIns] [int] NOT NULL,
	[FechaIns] [datetime] NOT NULL,
	[IdUsuarioUpd] [int] NULL,
	[FechaUpd] [datetime] NULL,
 CONSTRAINT [PK_CatRubros] PRIMARY KEY CLUSTERED 
(
	[IdRubro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [pre].[CatTasaCambio]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [pre].[CatTasaCambio](
	[IdTasaCambio] [int] NOT NULL,
	[IdMoneda] [int] NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[Valor] [decimal](18, 4) NOT NULL,
 CONSTRAINT [PK_CatTasaCambio] PRIMARY KEY CLUSTERED 
(
	[IdTasaCambio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [pre].[MstPresupuesto]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [pre].[MstPresupuesto](
	[IdPresupuesto] [int] NOT NULL,
	[IdProyecto] [int] NOT NULL,
	[IdEntregable] [int] NOT NULL,
	[IdMoneda] [int] NOT NULL,
	[PresupuestoAsignado] [decimal](18, 4) NOT NULL,
	[PresupuestoActual] [decimal](18, 4) NOT NULL,
	[PresupuestoEjecutado] [decimal](18, 4) NOT NULL,
	[Activo] [bit] NOT NULL,
	[IdUsuarioIns] [int] NOT NULL,
	[FechaIns] [datetime] NOT NULL,
	[IdUsuarioUpd] [int] NULL,
	[FechaUpd] [datetime] NULL,
 CONSTRAINT [PK_MstPresupuesto] PRIMARY KEY CLUSTERED 
(
	[IdPresupuesto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [pre].[MstPresupuestoFinanciero]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [pre].[MstPresupuestoFinanciero](
	[IdPresupuestoFinanciero] [int] IDENTITY(1,1) NOT NULL,
	[IdPresupuesto] [int] NULL,
	[IdProyecto] [int] NOT NULL,
	[IdEntregable] [int] NOT NULL,
	[IdTarea] [int] NOT NULL,
	[IdRubro] [char](5) NULL,
	[MontoPresupuestario] [decimal](18, 4) NOT NULL,
	[Activo] [bit] NOT NULL,
	[IdUsuarioIns] [int] NOT NULL,
	[FechaIns] [datetime] NOT NULL,
	[IdUsuarioUpd] [int] NULL,
	[FechaUpd] [datetime] NULL,
 CONSTRAINT [PK_MtPresupuestoFinanciero] PRIMARY KEY CLUSTERED 
(
	[IdPresupuestoFinanciero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [pre].[MstPresupuestoFisico]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [pre].[MstPresupuestoFisico](
	[IdPresupuestoFisico] [int] IDENTITY(1,1) NOT NULL,
	[IdPresupuesto] [int] NULL,
	[IdProyecto] [int] NOT NULL,
	[IdEntregable] [int] NOT NULL,
	[IdTarea] [int] NOT NULL,
	[IdRubro] [char](5) NOT NULL,
	[IdArticulo] [char](5) NOT NULL,
	[CantidadArticulos] [decimal](18, 4) NOT NULL,
	[CostoArticulos] [decimal](18, 4) NOT NULL,
	[Activo] [bit] NOT NULL,
	[IdUsuarioIns] [int] NOT NULL,
	[FechaIns] [datetime] NOT NULL,
	[IdUsuarioUpd] [int] NULL,
	[FechaUpd] [datetime] NULL,
 CONSTRAINT [PK_MstPresupuestoFisico] PRIMARY KEY CLUSTERED 
(
	[IdPresupuestoFisico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [pro].[ArchivosTareas]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [pro].[ArchivosTareas](
	[IdArchivoTarea] [int] IDENTITY(1,1) NOT NULL,
	[IdTarea] [int] NOT NULL,
	[IdEntregable] [int] NOT NULL,
	[IdProyecto] [int] NOT NULL,
	[NombreArchivo] [varchar](256) NOT NULL,
	[NombreEnRuta] [varchar](256) NOT NULL,
	[Eliminado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdArchivoTarea] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [pro].[BitacoraTareas]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [pro].[BitacoraTareas](
	[IdBitacoraTarea] [int] IDENTITY(1,1) NOT NULL,
	[IdTarea] [int] NOT NULL,
	[IdEntregable] [int] NOT NULL,
	[IdProyecto] [int] NOT NULL,
	[IdUsuario] [int] NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[IdEstado] [int] NULL,
	[EstadoDesc] [varchar](32) NULL,
	[Comentarios] [text] NULL,
	[Progreso] [decimal](5, 2) NULL,
	[NombreArchivo] [varchar](256) NULL,
	[Creacion] [bit] NOT NULL,
	[IdArchivoTarea] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdBitacoraTarea] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [pro].[CatRespuestas]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [pro].[CatRespuestas](
	[IdRespuesta] [int] NOT NULL,
	[IdGrupoRespuesta] [int] NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
	[Valor] [decimal](9, 4) NOT NULL,
 CONSTRAINT [PK_CatRespuestas] PRIMARY KEY CLUSTERED 
(
	[IdRespuesta] ASC,
	[IdGrupoRespuesta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [pro].[DetAvancePreguntas]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [pro].[DetAvancePreguntas](
	[IdAvancePregunta] [int] NOT NULL,
	[IdPregunta] [int] NOT NULL,
	[IdAvance] [int] NOT NULL,
 CONSTRAINT [PK_DetAvancePregunta] PRIMARY KEY CLUSTERED 
(
	[IdAvancePregunta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [pro].[DetConfiguracionTareaFrecuencia]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [pro].[DetConfiguracionTareaFrecuencia](
	[IdConfiguracionFrecuencia] [int] NOT NULL,
	[IdProyecto] [int] NULL,
	[IdEntregable] [int] NULL,
	[IdTarea] [int] NOT NULL,
	[TipoFrecuencia] [int] NOT NULL,
	[CantidadDias] [int] NULL,
	[HoraInicio] [time](7) NULL,
	[HoraFin] [time](7) NULL,
	[CantidadSemanas] [int] NULL,
	[DiaInicio] [varchar](20) NULL,
	[CantidadMeses] [int] NULL,
	[NumeroSemana] [int] NULL,
	[CantidadAnios] [int] NULL,
	[Mes] [varchar](15) NULL,
	[DuracionDias] [int] NULL,
	[DiaDelMes] [int] NULL,
 CONSTRAINT [PK_DetConfiguracionTareaFrecuencia] PRIMARY KEY CLUSTERED 
(
	[IdConfiguracionFrecuencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [pro].[DetIntervaloFrecuencia]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [pro].[DetIntervaloFrecuencia](
	[IdIntervaloFrecuencia] [int] NOT NULL,
	[IdConfiguracionFrecuencia] [int] NULL,
	[FechaInicio] [datetime] NOT NULL,
	[TipoFinalizacion] [int] NOT NULL,
	[CantidadRepeticiones] [int] NULL,
	[FechaFin] [datetime] NULL,
 CONSTRAINT [PK_DetIntervaloFrecuencia] PRIMARY KEY CLUSTERED 
(
	[IdIntervaloFrecuencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [pro].[DetObjetivoPreguntas]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [pro].[DetObjetivoPreguntas](
	[IdObjetivoPregunta] [int] NOT NULL,
	[IdObjetivo] [int] NOT NULL,
	[IdPregunta] [int] NOT NULL,
 CONSTRAINT [PK_DetObjetivoPregunta] PRIMARY KEY CLUSTERED 
(
	[IdObjetivoPregunta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [pro].[DetPersonal]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [pro].[DetPersonal](
	[IdPersonal] [int] NOT NULL,
	[IdPersona] [int] NOT NULL,
	[IdInstitucion] [int] NULL,
	[IdPrograma] [int] NULL,
	[IdCargo] [int] NOT NULL,
 CONSTRAINT [PK_DetProyectoMiembro] PRIMARY KEY CLUSTERED 
(
	[IdPersonal] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [pro].[DetProgramaProyecto]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [pro].[DetProgramaProyecto](
	[IdProgramaProyecto] [int] NOT NULL,
	[IdPrograma] [int] NOT NULL,
	[IdProyecto] [int] NOT NULL,
 CONSTRAINT [PK_DetDivisionProyecto] PRIMARY KEY CLUSTERED 
(
	[IdProgramaProyecto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [pro].[DetProyectoBeneficiarios]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [pro].[DetProyectoBeneficiarios](
	[IdProyecto] [int] NOT NULL,
	[TipoDestinatario] [char](1) NOT NULL,
	[IdBeneficiario] [int] NULL,
	[Descripcion] [varchar](255) NULL,
 CONSTRAINT [PK_DetProyectoBeneficiario] PRIMARY KEY CLUSTERED 
(
	[IdProyecto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [pro].[DetProyectoObjetivos]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [pro].[DetProyectoObjetivos](
	[IdProyectoObjetivo] [int] IDENTITY(3,1) NOT NULL,
	[IdProyecto] [int] NOT NULL,
	[IdObjetivo] [int] NOT NULL,
 CONSTRAINT [PK_DetProyectoObjetivo] PRIMARY KEY CLUSTERED 
(
	[IdProyectoObjetivo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [pro].[DetProyectoResponsable]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [pro].[DetProyectoResponsable](
	[IdProyectoResponsable] [int] NOT NULL,
	[IdProyecto] [int] NOT NULL,
	[IdPersonal] [int] NULL,
 CONSTRAINT [PK_DetProyectoResponsable] PRIMARY KEY CLUSTERED 
(
	[IdProyectoResponsable] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [pro].[DetTareaPersonal]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [pro].[DetTareaPersonal](
	[IdTareaPersonal] [int] IDENTITY(1,1) NOT NULL,
	[IdProyecto] [int] NOT NULL,
	[IdEntregable] [int] NOT NULL,
	[IdTarea] [int] NOT NULL,
	[IdPersonal] [int] NOT NULL,
	[TipoPersonal] [char](1) NOT NULL,
	[Ciudad] [varchar](100) NULL,
	[Lugar] [varchar](255) NULL,
 CONSTRAINT [PK_DetTareaResponsable] PRIMARY KEY CLUSTERED 
(
	[IdTareaPersonal] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [pro].[MstAvances]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [pro].[MstAvances](
	[IdAvance] [int] NOT NULL,
	[IdProyecto] [int] NOT NULL,
	[IdEntregable] [int] NOT NULL,
	[IdTarea] [int] NOT NULL,
	[Descripcion] [text] NOT NULL,
	[FechaAvance] [datetime] NOT NULL,
	[PresupuestoUtilizado] [decimal](9, 2) NOT NULL,
	[IdUsuarioIns] [int] NOT NULL,
	[FechaIns] [datetime] NOT NULL,
	[IdUsuarioUpd] [int] NULL,
	[FechaUpd] [datetime] NULL,
 CONSTRAINT [PK_MstAvances] PRIMARY KEY CLUSTERED 
(
	[IdAvance] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [pro].[MstBeneficiarios]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [pro].[MstBeneficiarios](
	[IdBeneficiario] [int] NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Direccion] [varchar](255) NOT NULL,
	[Telefono] [varchar](20) NULL,
	[Email] [varchar](50) NULL,
	[WebSite] [varchar](100) NULL,
	[Logo] [varchar](255) NULL,
	[IdUsuarioIns] [int] NOT NULL,
	[FechaIns] [datetime] NOT NULL,
	[IdUsuarioUpd] [int] NULL,
	[FechaUpd] [datetime] NULL,
 CONSTRAINT [PK_MstBeneficiarios] PRIMARY KEY CLUSTERED 
(
	[IdBeneficiario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [pro].[MstComentarios]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [pro].[MstComentarios](
	[IdComentario] [int] NOT NULL,
	[IdMensaje] [int] NULL,
	[Descripcion] [text] NOT NULL,
	[IdUsuario] [int] NOT NULL,
	[Fecha] [datetime] NOT NULL,
 CONSTRAINT [PK_MstComentarios] PRIMARY KEY CLUSTERED 
(
	[IdComentario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [pro].[MstEntregables]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [pro].[MstEntregables](
	[IdEntregable] [int] NOT NULL,
	[IdProyecto] [int] NOT NULL,
	[IdEntregableParent] [int] NULL,
	[Descripcion] [varchar](500) NOT NULL,
	[FechaInicio] [datetime] NOT NULL,
	[FechaFin] [datetime] NOT NULL,
	[DuracionEstimadaHoras] [decimal](9, 2) NOT NULL,
	[DuracionEstimadaDias] [decimal](9, 2) NOT NULL,
	[HorasDedicadas] [decimal](9, 2) NOT NULL,
	[IdEstado] [int] NOT NULL,
	[Nota] [text] NULL,
	[IdUsuarioIns] [int] NOT NULL,
	[FechaIns] [datetime] NOT NULL,
	[IdUsuarioUpd] [int] NULL,
	[FechaUpd] [datetime] NULL,
 CONSTRAINT [PK_MstEntregable] PRIMARY KEY CLUSTERED 
(
	[IdEntregable] ASC,
	[IdProyecto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [pro].[MstMensajes]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [pro].[MstMensajes](
	[IdMensaje] [int] NOT NULL,
	[IdProyecto] [int] NULL,
	[Asunto] [varchar](255) NOT NULL,
	[Descripcion] [text] NOT NULL,
	[IdUsuario] [int] NULL,
	[Fecha] [datetime] NOT NULL,
 CONSTRAINT [PK_MstMensajes] PRIMARY KEY CLUSTERED 
(
	[IdMensaje] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [pro].[MstObjetivos]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [pro].[MstObjetivos](
	[IdObjetivo] [int] IDENTITY(3,1) NOT NULL,
	[TipoObjetivo] [char](1) NOT NULL,
	[Descripcion] [varchar](500) NOT NULL,
	[Activo] [bit] NOT NULL,
 CONSTRAINT [PK_MstObjetivos] PRIMARY KEY CLUSTERED 
(
	[IdObjetivo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [pro].[MstPreguntas]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [pro].[MstPreguntas](
	[IdPregunta] [int] NOT NULL,
	[IdRespuesta] [int] NULL,
	[IdGrupoRespuesta] [int] NULL,
	[Descripcion] [varchar](255) NOT NULL,
 CONSTRAINT [PK_MstPreguntas] PRIMARY KEY CLUSTERED 
(
	[IdPregunta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [pro].[MstProyectos]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [pro].[MstProyectos](
	[IdProyecto] [int] NOT NULL,
	[TituloProyecto] [varchar](256) NOT NULL,
	[DescProyecto] [text] NOT NULL,
	[Justificacion] [text] NOT NULL,
	[FechaInicio] [datetime] NOT NULL,
	[FechaFinEstimada] [datetime] NOT NULL,
	[FechaLimite] [datetime] NOT NULL,
	[HorasDedicadas] [int] NOT NULL,
	[DuracionEstimadaHoras] [decimal](9, 2) NOT NULL,
	[DuracionEstimadaDias] [decimal](9, 2) NOT NULL,
	[Externo] [bit] NOT NULL,
	[Presupuesto] [decimal](18, 4) NOT NULL,
	[PresupuestoActual] [decimal](18, 4) NOT NULL,
	[GastoEstimado] [decimal](18, 4) NOT NULL,
	[IdTipoProyecto] [int] NOT NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuarioIns] [int] NOT NULL,
	[FechaIns] [datetime] NOT NULL,
	[IdUsuarioUpd] [int] NULL,
	[FechaUpd] [datetime] NULL,
	[DiasLabSem] [int] NOT NULL,
	[IdMoneda] [int] NOT NULL,
 CONSTRAINT [PK_MstProyectos] PRIMARY KEY CLUSTERED 
(
	[IdProyecto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [pro].[MstTareas]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [pro].[MstTareas](
	[IdTarea] [int] IDENTITY(1,1) NOT NULL,
	[IdEntregable] [int] NOT NULL,
	[IdProyecto] [int] NOT NULL,
	[IdTareaParent] [int] NULL,
	[IdTareaDependencia] [int] NULL,
	[Nombre] [varchar](255) NOT NULL,
	[FechaInicio] [datetime] NOT NULL,
	[FechaFin] [datetime] NOT NULL,
	[DuracionEstimadaDias] [decimal](9, 2) NOT NULL,
	[DuracionEstimadaHoras] [decimal](9, 2) NOT NULL,
	[HorasDedicadas] [decimal](9, 2) NOT NULL,
	[Hito] [bit] NOT NULL,
	[Provisional] [bit] NOT NULL,
	[Revisar] [bit] NOT NULL,
	[IdUsuarioRevisor] [int] NULL,
	[ReportarAvances] [bit] NOT NULL,
	[FecuenciaAvance] [int] NOT NULL,
	[TareaPeriodica] [bit] NOT NULL,
	[IdEstado] [int] NOT NULL,
	[Nota] [text] NULL,
	[IdUsuarioIns] [int] NOT NULL,
	[FechaIns] [datetime] NOT NULL,
	[IdUsuarioUpd] [int] NULL,
	[FechaUpd] [datetime] NULL,
	[TimeLine] [bit] NULL CONSTRAINT [DF_MstTareas_TimeLine]  DEFAULT ((0)),
 CONSTRAINT [PK_MstTareas] PRIMARY KEY CLUSTERED 
(
	[IdTarea] ASC,
	[IdEntregable] ASC,
	[IdProyecto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Security].[Actions]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Security].[Actions](
	[ActionId] [int] NOT NULL,
	[SystemId] [int] NOT NULL,
	[Action] [varchar](50) NULL,
	[Controller] [varchar](50) NULL,
	[Url] [varchar](255) NULL,
	[Description] [varchar](255) NOT NULL,
	[Active] [bit] NOT NULL,
	[UserInsert] [int] NOT NULL,
	[InsertDate] [datetime2](7) NOT NULL,
	[UserUpdate] [int] NULL,
	[UpdateDate] [datetime2](7) NULL,
 CONSTRAINT [Action_PK] PRIMARY KEY CLUSTERED 
(
	[ActionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Security].[ActionsOptions]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Security].[ActionsOptions](
	[ActionOption] [int] NOT NULL,
	[OptionId] [int] NOT NULL,
	[ActionId] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [Security].[LoginHistory]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Security].[LoginHistory](
	[LoginHistoryId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[LoginDate] [datetime2](7) NOT NULL,
	[LogoffDate] [datetime2](7) NULL,
	[IP] [varchar](50) NOT NULL,
 CONSTRAINT [LoginHistory_PK] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LoginHistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Security].[Menu]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Security].[Menu](
	[MenuId] [int] NOT NULL,
	[SystemId] [int] NOT NULL,
	[MenuName] [varchar](100) NOT NULL,
	[Description] [varchar](255) NULL,
	[Active] [bit] NOT NULL,
	[UserInsert] [int] NOT NULL,
	[InsertDate] [datetime2](7) NOT NULL,
	[UserUpdate] [int] NULL,
	[UpdateDate] [datetime2](7) NULL,
 CONSTRAINT [Menu_PK] PRIMARY KEY CLUSTERED 
(
	[MenuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Security].[MenuItems]    Script Date: 19/10/13 6:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Security].[MenuItems](
	[MenuItemId] [int] NOT NULL,
	[MenuId] [int] NOT NULL,
	[ItemParentId] [int] NULL,
	[ActionId] [int] NULL,
	[Label] [varchar](100) NOT NULL,
	[Icon] [varchar](255) NULL,
	[CssClass] [varchar](50) NULL,
	[Active] [bit] NOT NULL,
	[UserInsert] [int] NOT NULL,
	[InsertDate] [datetime2](7) NOT NULL,
	[UserUpdate] [int] NULL,
	[UpdateDate] [datetime2](7) NULL,
 CONSTRAINT [MenuItems_PK] PRIMARY KEY CLUSTERED 
(
	[MenuItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Security].[PasswordHistory]    Script Date: 19/10/13 6:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Security].[PasswordHistory](
	[PasswordHistoryId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[OldPassword] [char](128) NOT NULL,
	[NewPassword] [char](128) NOT NULL,
	[ChangeDate] [datetime2](7) NOT NULL,
	[ReasonChange] [varchar](255) NOT NULL,
 CONSTRAINT [PasswordHistory_PK] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[PasswordHistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Security].[RoleAction]    Script Date: 19/10/13 6:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Security].[RoleAction](
	[RoleActionID] [int] IDENTITY(1,1) NOT NULL,
	[SystemId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
	[ActionId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleActionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Security].[RoleMenu]    Script Date: 19/10/13 6:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Security].[RoleMenu](
	[SystemId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
	[MenuId] [int] NOT NULL,
 CONSTRAINT [RoleMenu_PK] PRIMARY KEY CLUSTERED 
(
	[MenuId] ASC,
	[RoleId] ASC,
	[SystemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Security].[RoleMenuItem]    Script Date: 19/10/13 6:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Security].[RoleMenuItem](
	[RoleMenuItemId] [int] IDENTITY(1,1) NOT NULL,
	[SystemId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
	[MenuId] [int] NOT NULL,
	[MenuItemId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleMenuItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Security].[Roles]    Script Date: 19/10/13 6:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Security].[Roles](
	[RoleId] [int] NOT NULL,
	[RolePerentId] [int] NULL,
	[RoleName] [varchar](100) NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[Active] [bit] NOT NULL,
	[UserInsert] [int] NOT NULL,
	[InsertDate] [datetime2](7) NOT NULL,
	[UserUpdate] [int] NULL,
	[UpdateDate] [datetime2](7) NULL,
 CONSTRAINT [Role_PK] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Security].[SystemRoles]    Script Date: 19/10/13 6:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Security].[SystemRoles](
	[SystemId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [SystemRole_PK] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC,
	[SystemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Security].[Systems]    Script Date: 19/10/13 6:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Security].[Systems](
	[SystemId] [int] NOT NULL,
	[SystemName] [varchar](100) NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[Acronym] [varchar](10) NOT NULL,
	[StartActionId] [varchar](255) NULL,
	[Active] [bit] NOT NULL,
	[UserInsert] [int] NOT NULL,
	[InsertDate] [datetime2](7) NOT NULL,
	[UserUpdate] [int] NULL,
	[UpdateDate] [datetime2](7) NULL,
 CONSTRAINT [System_PK] PRIMARY KEY CLUSTERED 
(
	[SystemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Security].[UserRoles]    Script Date: 19/10/13 6:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Security].[UserRoles](
	[UserRoleId] [int] IDENTITY(1,1) NOT NULL,
	[SystemId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[StartActionId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserRoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Security].[Users]    Script Date: 19/10/13 6:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Security].[Users](
	[UserId] [int] NOT NULL,
	[ReferenceId] [int] NULL,
	[UserName] [varchar](20) NOT NULL,
	[FullName] [varchar](100) NOT NULL,
	[Password] [char](128) NOT NULL,
	[Email] [varchar](100) NOT NULL,
	[PhoneNumber] [varchar](20) NOT NULL,
	[ChangePassword] [bit] NOT NULL,
	[PasswordExpiration] [smallint] NOT NULL,
	[PasswordRepeatPeriod] [smallint] NULL,
	[AttemptsPermitted] [smallint] NOT NULL,
	[FailedAttempts] [smallint] NOT NULL,
	[TypeAccessRecovery] [smallint] NOT NULL,
	[SecurityQuestion] [char](128) NULL,
	[AnswerSecurity] [char](128) NULL,
	[ExpirationSession] [smallint] NOT NULL DEFAULT ((600)),
	[ProfilePhoto] [varchar](100) NULL,
	[Locked] [bit] NOT NULL,
	[Active] [bit] NOT NULL,
	[UserInsert] [int] NOT NULL,
	[InsertDate] [datetime2](7) NOT NULL,
	[UserUpdate] [int] NULL,
	[UpdateDate] [datetime2](7) NULL,
 CONSTRAINT [User_PK] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [admin].[vwDetCatalogo]    Script Date: 19/10/13 6:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [admin].[vwDetCatalogo]
AS
	SELECT	C.IdCatalogo,
			D.IdDetCatalogo,
			C.Descripcion Catalogo,
			D.Valor,
			D.Descripcion,
			D.FechaIns,
			D.Activo
	FROM	[admin].[DetCatalogo] D
			INNER JOIN [admin].[MstCatalogo] C ON D.IdMstCatalogo = C.IdCatalogo



GO
/****** Object:  View [admin].[vwLogger]    Script Date: 19/10/13 6:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [admin].[vwLogger]
AS
	SELECT	*
	FROM	[admin].[Logger]


GO
/****** Object:  View [pre].[vwArticulos]    Script Date: 19/10/13 6:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [pre].[vwArticulos]
AS
	SELECT	A.IdArticulo,
			CR.IdRubro,
			CR.Nombre Rubro,
			DC.IdDetCatalogo IdUnidadMedida,
			DC.Descripcion UnidadMedida,
			M.IdMoneda,
			M.Nombre Moneda,
			M.Simbolo,
			a.Precio,
			A.Descripcion,
			A.Activo,
			A.IdUsuarioIns,
			A.FechaIns,
			A.IdUsuarioUpd,
			A.FechaUpd
	FROM	[pre].[CatArticulos] A
			INNER JOIN [admin].[DetCatalogo] DC ON A.IdUnidadMedida = DC.IdDetCatalogo
			INNER JOIN [pre].[CatRubros] CR ON A.IdRubro = CR.IdRubro
			INNER JOIN [pre].[CatMoneda] M ON A.IdMoneda = M.IdMoneda
	WHERE	CR.Activo = 1
			AND DC.Activo = 1
			AND M.Activo = 1



GO
/****** Object:  View [pre].[vwRubros]    Script Date: 19/10/13 6:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [pre].[vwRubros]
AS
	SELECT	R.IdRubro,
			R.Nombre,
			DC.IdDetCatalogo IdTipoRubro,
			DC.Valor TipoRubro,
			CR.IdRubro IdRubroParent,
			CR.Nombre RubroParent,
			R.Activo,
			R.IdUsuarioIns,
			R.FechaIns,
			R.IdUsuarioUpd,
			R.FechaUpd
	FROM	[pre].[CatRubros] R
			INNER JOIN [admin].[DetCatalogo] DC ON R.IdTipoRubro = DC.IdDetCatalogo
			LEFT JOIN [pre].[CatRubros] CR ON R.IdRubroParent = CR.IdRubro
	--WHERE	CR.Activo = 1



GO
/****** Object:  View [pro].[vwActividad]    Script Date: 19/10/13 6:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pro].[vwActividad]
AS
SELECT	B.IdTarea,
		B.Comentarios,
		B.Creacion,
		B.Fecha,
		B.Progreso,
		B.EstadoDesc,
		T.Nombre Tarea,
		U.FullName NombreCompleto,
		B.IdProyecto,
		A.IdArchivoTarea,
		A.Eliminado,
		A.NombreArchivo
FROM	[pro].[BitacoraTareas] B
		INNER JOIN [pro].[MstTareas] T ON B.IdProyecto = T.IdProyecto AND B.IdEntregable = T.IdEntregable 
																		  AND B.IdTarea = T.IdTarea		
		INNER JOIN [Security].[Users] U ON B.IdUsuario = U.UserId
		LEFT JOIN [pro].[ArchivosTareas] A ON B.IdArchivoTarea = A.IdArchivoTarea

GO
/****** Object:  View [pro].[vwBitacoraTareas]    Script Date: 19/10/13 6:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [pro].[vwBitacoraTareas]
AS
	SELECT	T.IdProyecto,
			T.IdTarea,
			T.Nombre Tarea,
			E.IdEntregable,
			E.Descripcion Entregable,
			T.IdEstado EstadoTarea,
			T.FechaInicio,
			T.FechaFin,
			B.Fecha,
			B.IdEstado,
			CASE WHEN (B.Progreso IS NOT NULL AND B.Progreso = 100) 
				 THEN CONVERT(BIT, 'TRUE') 
				 ELSE CONVERT(BIT, 'FALSE') END Finalizado 
	FROM	[pro].[BitacoraTareas] B
			INNER JOIN [pro].[MstTareas] T ON B.IdProyecto = T.IdProyecto AND B.IdEntregable = T.IdEntregable 
																		  AND B.IdTarea = T.IdTarea																		  
			INNER JOIN [pro].[MstEntregables] E ON E.IdProyecto = T.IdProyecto AND E.IdEntregable = T.IdEntregable  
 



GO
/****** Object:  View [pro].[vwListadoPersonal]    Script Date: 19/10/13 6:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [pro].[vwListadoPersonal]
AS
	SELECT	DP.IdPersonal,
			P.IdPersona,
			P.NombreCompleto Nombre,
			P.Identificacion,
			PR.IdPrograma,
			PR.Descripcion NombrePrograma,
			I.IdInstitucion,
			I.Nombre NombreInstitucion,
			DC.IdDetCatalogo IdCargo,
			DC.Valor Cargo,
			DPT.NumeroTelefono,
			DPE.Email,
			P.Activo 
	FROM	pro.DetPersonal DP
			INNER JOIN config.MstPersonas P ON DP.IdPersona = P.IdPersona
			INNER JOIN admin.DetCatalogo DC ON DP.IdCargo = DC.IdDetCatalogo
			LEFT JOIN [config].[DetPersonaEmail] DPE ON P.IdPersona = DPE.IdPersona
			LEFT JOIN [config].[DetPersonaTelefono] DPT ON P.IdPersona = DPT.IdPersona
			LEFT JOIN config.MstPrograma PR ON DP.IdPrograma = PR.IdPrograma AND PR.Activo = 1
			LEFT JOIN config.MstInstituciones I ON DP.IdInstitucion = I.IdInstitucion AND I.Activo = 1
	WHERE	DC.Activo = 1
			
			



GO
/****** Object:  View [pro].[vwProyectos]    Script Date: 19/10/13 6:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [pro].[vwProyectos]
AS
	SELECT	P.[IdProyecto],
			P.[FechaInicio],
			P.[FechaLimite], 
			CONVERT(VARCHAR, CAST(P.[Presupuesto] as MONEY), 1)
			Presupuesto,
			P.[DescProyecto],
			P.[Justificacion],
			CONVERT(VARCHAR, CAST(P.[GastoEstimado] AS MONEY), 1) GastoEstimado,
			P.[TituloProyecto],
			P.[HorasDedicadas],
			P.[FechaFinEstimada],
			CONVERT(VARCHAR, CAST(P.[PresupuestoActual]AS MONEY), 1) PresupuestoActual,
			P.[DuracionEstimadaDias],
			P.[DuracionEstimadaHoras],
			P.[IdTipoProyecto],
			CD.Valor [TipoProyecto],
			P.[IdEstado],
			CDE.Valor [Estado],
			P.[IdMoneda],
			PB.Descripcion [GrupoMeta]
	FROM	[pro].[MstProyectos] P
			INNER JOIN [admin].[DetCatalogo] CD ON P.IdTipoProyecto = CD.IdDetCatalogo
			INNER JOIN [admin].[DetCatalogo] CDE ON P.IdEstado = CDE.IdDetCatalogo
			INNER JOIN [pro].[DetProyectoBeneficiarios] PB ON P.IdProyecto = PB.IdProyecto 




GO
/****** Object:  View [pro].[vwTareasEnEjecucion]    Script Date: 19/10/13 6:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




 
CREATE VIEW [pro].[vwTareasEnEjecucion]
AS
	SELECT	T.IdTarea,
			T.IdEntregable,
			T.IdProyecto,
			T.Nombre,
			T.IdEstado,
			D.Valor Estado,
			U.FullName Autor,
			TP.IdPersonal,
			TP.TipoPersonal,
			T.TimeLine,
			P.NombreCompleto NombrePersona,
			(SELECT  TOP 1 B.Fecha FROM [pro].[BitacoraTareas] B
			WHERE B.IdTarea = T.IdTarea ORDER BY IdBitacoraTarea DESC) UltimaActualizacion,
			(SELECT  TOP 1 B.Progreso FROM [pro].[BitacoraTareas] B
			WHERE B.IdTarea = T.IdTarea ORDER BY IdBitacoraTarea DESC) Progreso
	FROM	[pro].[MstTareas] T
			INNER JOIN [admin].[DetCatalogo] D ON T.IdEstado = D.IdDetCatalogo
			INNER JOIN [pro].[DetTareaPersonal] TP ON T.IdTarea = TP.IdTarea AND 
											T.IdEntregable = TP.IdEntregable AND
											T.IdProyecto = TP.IdProyecto --AND
											--TP.TipoPersonal = 'R'
			INNER JOIN [pro].[DetPersonal] DP ON TP.IdPersonal = DP.IdPersonal
			INNER JOIN [config].[MstPersonas] P ON DP.IdPersona = P.IdPersona
			--INNER JOIN [Security].[Users] UR ON P.IdPersona = UR.ReferenceId
			INNER JOIN [Security].[Users] U ON T.IdUsuarioIns = U.UserId






GO
/****** Object:  View [pro].[vwTareasPorProyecto]    Script Date: 19/10/13 6:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [pro].[vwTareasPorProyecto]
AS

	SELECT	P.IdProyecto,
			E.Descripcion Entregable,
			T.Nombre Tarea,
			T.IdTarea,
			T.IdEstado,
			T.FechaInicio,
			T.FechaFin,
			ROW_NUMBER() OVER(PARTITION BY E.IdEntregable ORDER BY E.Descripcion ASC) AS Row#
	FROM	[pro].[MstProyectos] P
			INNER JOIN [pro].[MstEntregables] E ON P.IdProyecto = E.IdProyecto
			INNER JOIN [pro].[MstTareas] T ON P.IdProyecto = T.IdProyecto AND E.IdEntregable = T.IdEntregable



GO
/****** Object:  View [pro].[vwTareasRetrasadas]    Script Date: 19/10/13 6:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [pro].[vwTareasRetrasadas]
AS
SELECT TBL.IdTarea,
	   TBL.IdEntregable,
	   TBL.Descripcion,
	   TBL.IdProyecto,
	   TBL.Nombre,
	   TBL.NombrePersona,
	   TBL.FechaInicio,
	   TBL.FechaFin,
	   (TBL.DiasRetrasados * -1) DiasRetrasados 
FROM (	SELECT	T.IdTarea,
				T.IdEntregable,
				E.Descripcion,
				T.IdProyecto,
				T.Nombre,
				P.NombreCompleto NombrePersona,
				T.FechaInicio,
				T.FechaFin,
				(DATEDIFF(DAY, GETDATE(), T.FechaInicio)) DiasRetrasados
		FROM	[pro].[MstTareas] T
				INNER JOIN [pro].[MstEntregables] E ON T.IdEntregable = E.IdEntregable
				INNER JOIN [pro].[DetTareaPersonal] TP ON T.IdTarea = TP.IdTarea AND 
												T.IdEntregable = TP.IdEntregable AND
												T.IdProyecto = TP.IdProyecto AND
											TP.TipoPersonal = 'R'
				INNER JOIN [pro].[DetPersonal] DP ON TP.IdPersonal = DP.IdPersonal
				INNER JOIN [config].[MstPersonas] P ON DP.IdPersona = P.IdPersona
		WHERE	T.FechaFin < GETDATE() AND T.IdEstado != 30) TBL


GO
/****** Object:  View [Security].[vwActionList]    Script Date: 19/10/13 6:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [Security].[vwActionList]
AS
	SELECT	A.ActionId,
			Url,
			Action,
			Controller,
			Description,
			RA.RoleId,
			--ISNULL(U.UserId, 0) UserId,
			RA.SystemId
			--UserName
	FROM	[Security].[Actions] A
			INNER JOIN [Security].[RoleAction] RA ON A.ActionId = RA.ActionId
			--left JOIN [Security].[UserRoles] UR ON RA.RoleId = UR.RoleId AND RA.SystemId = UR.SystemId
			--LEFT JOIN [Security].[Users] U ON UR.UserId = U.UserId
			





GO
/****** Object:  View [Security].[vwMenuItems]    Script Date: 19/10/13 6:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [Security].[vwMenuItems]
AS
	SELECT	M.MenuName,
			MI.MenuItemId,
			MI.MenuId,
			MI.ItemParentId,
			MI.ActionId,
			A.Action, 
			A.Controller,
			A.Url,
			MI.Label, 
			MI.Icon,
			MI.CssClass,
			UR.UserId,
			RMI.SystemId,
			RM.RoleId,
			U.UserName
	FROM	[Security].[Menu] M
			INNER JOIN [Security].[RoleMenu] RM ON M.MenuId = RM.MenuId AND M.SystemId = RM.SystemId
			INNER JOIN [Security].[MenuItems] MI ON M.MenuId = MI.MenuId
			INNER JOIN [Security].[RoleMenuItem] RMI ON MI.MenuItemId = RMI.MenuItemId AND 
														MI.MenuId = RMI.MenuId
			INNER JOIN [Security].[UserRoles] UR ON RM.RoleId = UR.RoleId AND RMI.RoleId = UR.RoleId
			INNER JOIN [Security].[Users] U ON UR.UserId = U.UserId
			LEFT JOIN [Security].[Actions] A ON MI.ActionId = A.ActionId
	WHERE	M.Active = 1
			AND MI.Active = 1
			AND U.Active = 1
			AND (A.ActionId IS NULL OR (A.ActionId IS NOT NULL AND A.ActionId IN (SELECT RA.ActionId FROM [Security].[RoleAction] RA WHERE RA.RoleId = RM.RoleId AND RA.SystemId = RM.SystemId)))


GO
/****** Object:  View [Security].[vwOptionsList]    Script Date: 19/10/13 6:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [Security].[vwOptionsList]
AS
	SELECT	A.ActionId,
			A.Url,
			A.Action,
			A.Controller,
			RA.RoleId,
			UR.UserId,
			RA.SystemId,
			DC.IdDetCatalogo OptionId,
			DC.Valor OptionName,
			U.UserName
	FROM	[Security].[Actions] A
			INNER JOIN [Security].[RoleAction] RA ON A.ActionId = RA.ActionId AND A.SystemId = RA.SystemId
			INNER JOIN [Security].[UserRoles] UR ON RA.RoleId = UR.RoleId AND RA.SystemId = UR.SystemId
			INNER JOIN [Security].[Users] U ON UR.UserId = U.UserId
			INNER JOIN [Security].[ActionsOptions] AO ON A.ActionId = AO.ActionId
			INNER JOIN [admin].[DetCatalogo] DC ON AO.OptionId = DC.IdDetCatalogo
	WHERE	DC.Activo = 1 AND
			A.Active = 1
			AND U.Active = 1

	UNION ALL
	
	SELECT	A.ActionId,
			A.Url,
			A.Action,
			A.Controller,
			RA.RoleId,
			UR.UserId,
			RA.SystemId,
			0 OptionId,
			'' OptionName,
			U.UserName
	FROM	[Security].[Actions] A
			INNER JOIN [Security].[RoleAction] RA ON A.ActionId = RA.ActionId AND A.SystemId = RA.SystemId
			INNER JOIN [Security].[UserRoles] UR ON RA.RoleId = UR.RoleId AND RA.SystemId = UR.SystemId
			INNER JOIN [Security].[Users] U ON UR.UserId = U.UserId
	WHERE	A.Active = 1
			AND U.Active = 1



GO
SET IDENTITY_INSERT [admin].[CatParametros] ON 

INSERT [admin].[CatParametros] ([IdPerametro], [NombreParametro], [ValorParametro], [DescParametro]) VALUES (1, N'ESTADO_PROYECTO_CANCELADO', N'23', N'Identificador de la tabla detalle catálogo que corresponde al estado de proyecto «Cancelado» ')
INSERT [admin].[CatParametros] ([IdPerametro], [NombreParametro], [ValorParametro], [DescParametro]) VALUES (2, N'BACKUP_SCRIPT', N'BACKUP DATABASE [{0}] TO  DISK = N''{1}'' WITH NOFORMAT, NOINIT,  NAME = N''SIGESPRO Full Database Backup'', SKIP, NOREWIND, NOUNLOAD,  STATS = 10', N'Script para realizar resplado de la base de datos')
INSERT [admin].[CatParametros] ([IdPerametro], [NombreParametro], [ValorParametro], [DescParametro]) VALUES (3, N'ESTADO_PROYECTO_FINALIZADO', N'16', N'Identificador de la tabla detalle catálogo que corresponde al estado de proyecto «Finalizado» ')
INSERT [admin].[CatParametros] ([IdPerametro], [NombreParametro], [ValorParametro], [DescParametro]) VALUES (4, N'BACKUP_FOLDER', N'C:\backup\', N'Directorio donde se guardaran los archivo .bak ')
INSERT [admin].[CatParametros] ([IdPerametro], [NombreParametro], [ValorParametro], [DescParametro]) VALUES (5, N'ESTADO_PROYECTO', N'7', N'Identificador de la tabla catálogo que corresponde a «EstadoProyecto»')
INSERT [admin].[CatParametros] ([IdPerametro], [NombreParametro], [ValorParametro], [DescParametro]) VALUES (6, N'PROGRAMA_CENTRO', N'3', N'Identificador de la tabla MstProgramas que corresponde a «Centro Ecumenico Fray Antonio de Valdivieso »')
INSERT [admin].[CatParametros] ([IdPerametro], [NombreParametro], [ValorParametro], [DescParametro]) VALUES (7, N'COORDINADOR', N'2', N'Identificador de la tabla detalle catálogo que corresponde al rol «Coordinador»')
INSERT [admin].[CatParametros] ([IdPerametro], [NombreParametro], [ValorParametro], [DescParametro]) VALUES (8, N'ESTADO_PROYECTO_PEN_PLAN', N'13', N'Identificador de la tabla detalle catálogo que corresponde al estado de proyecto «Pendiente de Planificar»')
INSERT [admin].[CatParametros] ([IdPerametro], [NombreParametro], [ValorParametro], [DescParametro]) VALUES (9, N'CANT_HORAS_LABORALES_MT', N'5', N'Cantidad de horas laborales para horario de medio tiempo')
INSERT [admin].[CatParametros] ([IdPerametro], [NombreParametro], [ValorParametro], [DescParametro]) VALUES (10, N'ROL_DIRECTOR', N'3', N'Identificador del rol de director')
INSERT [admin].[CatParametros] ([IdPerametro], [NombreParametro], [ValorParametro], [DescParametro]) VALUES (11, N'ESTADO_TAREA_EN_EJECUCION', N'29', N'Identificador del catálogo del estado en ejecución de la tarea')
INSERT [admin].[CatParametros] ([IdPerametro], [NombreParametro], [ValorParametro], [DescParametro]) VALUES (12, N'RESTORE_BACKUP', N'RESTORE DATABASE {0} FROM DISK = N''{1}'' WITH REPLACE', N'Script para restraurar la base de datos del sistema')
SET IDENTITY_INSERT [admin].[CatParametros] OFF
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (1, 1, N'Kg', N'Kilogramo', 1, 1, CAST(N'2016-12-27 21:20:14.573' AS DateTime), NULL, NULL)
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (2, 3, N'Colaborador', N'Colaborador de programas', 1, 1, CAST(N'2016-12-29 16:00:58.107' AS DateTime), NULL, NULL)
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (3, 2, N'GrupoRubro', N'Grupo de rubro', 1, 1, CAST(N'2016-12-29 16:04:41.470' AS DateTime), NULL, NULL)
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (4, 2, N'RubroArticulo', N'Rubro árticulos', 1, 1, CAST(N'2016-12-29 16:05:08.820' AS DateTime), NULL, NULL)
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (5, 2, N'OtrosRubros', N'Otros Rubros', 1, 1, CAST(N'2016-12-29 16:05:34.923' AS DateTime), NULL, NULL)
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (6, 6, N'Educativo', N'Proyectos educativos en comunidades rurales.', 1, 1, CAST(N'2016-12-29 16:06:05.633' AS DateTime), NULL, NULL)
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (7, 6, N'Social', N'Proyecto social.', 1, 1, CAST(N'2016-12-29 16:06:38.230' AS DateTime), NULL, NULL)
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (8, 3, N'Coordinador', N'Coordinador de proyecto', 1, 1, CAST(N'2016-12-29 16:08:14.517' AS DateTime), NULL, NULL)
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (9, 4, N'Domiciliar', N'Dirección de domicilio', 1, 1, CAST(N'2016-12-29 16:08:54.093' AS DateTime), NULL, NULL)
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (10, 4, N'Trabajo', N'Dirección de trabajo', 1, 1, CAST(N'2016-12-29 16:09:28.343' AS DateTime), NULL, NULL)
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (11, 5, N'Casa', N'Teléfono domiciliar', 1, 1, CAST(N'2016-12-29 16:10:00.190' AS DateTime), 1, CAST(N'2017-01-14 13:30:32.540' AS DateTime))
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (12, 5, N'Celular', N'Teléfono móvil', 1, 1, CAST(N'2016-12-29 16:10:21.293' AS DateTime), 1, CAST(N'2017-01-14 13:30:11.113' AS DateTime))
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (13, 7, N'Pendiente de Planificar', N'Pendiente de Planificar', 1, 1, CAST(N'2017-01-13 22:59:25.773' AS DateTime), 1, CAST(N'2017-03-12 22:07:50.843' AS DateTime))
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (14, 8, N'21', N'Todos los días', 1, 0, CAST(N'2017-01-13 23:58:49.000' AS DateTime), 1, CAST(N'2018-07-06 22:23:29.460' AS DateTime))
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (15, 8, N'22', N'Solo días laborales', 1, 0, CAST(N'2017-01-13 23:59:05.000' AS DateTime), 1, CAST(N'2018-07-06 22:22:50.250' AS DateTime))
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (16, 7, N'Finalizado', N'Finalizado', 1, 0, CAST(N'2017-03-12 21:38:00.000' AS DateTime), 1, CAST(N'2018-07-06 22:17:04.657' AS DateTime))
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (17, 2, N'Prueba', N'Esta es una prueba', 0, 0, CAST(N'2017-04-24 22:19:23.000' AS DateTime), 1, CAST(N'2017-05-18 23:59:29.020' AS DateTime))
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (18, 10, N'Agregar', N'Acción de agregar', 1, 1, CAST(N'2017-05-21 13:35:11.000' AS DateTime), NULL, NULL)
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (19, 10, N'Editar', N'Acción  de editar en las páginas', 1, 1, CAST(N'2017-05-21 13:38:55.883' AS DateTime), NULL, NULL)
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (20, 10, N'Ver', N'Acción de ver en las páginas', 1, 0, CAST(N'2017-05-21 13:39:19.000' AS DateTime), 1, CAST(N'2017-05-21 13:39:28.740' AS DateTime))
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (21, 11, N'Horario Semana Completa', N'Horario Semana Completa', 1, 0, CAST(N'2018-07-06 21:50:09.000' AS DateTime), 1, CAST(N'2018-07-06 22:09:55.767' AS DateTime))
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (22, 11, N'Horario Regular', N'Horario Regular', 1, 0, CAST(N'2018-07-06 21:50:28.000' AS DateTime), 1, CAST(N'2018-07-06 22:09:22.080' AS DateTime))
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (23, 7, N'Cancelado', N'Cancelado', 1, 1, CAST(N'2018-07-06 22:18:04.333' AS DateTime), NULL, NULL)
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (24, 7, N'En Planificación', N'En Planificación', 1, 0, CAST(N'2018-07-06 22:18:45.000' AS DateTime), 1, CAST(N'2018-09-24 22:39:55.723' AS DateTime))
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (25, 7, N'Planificado', N'Planificado', 1, 1, CAST(N'2018-07-06 22:19:24.817' AS DateTime), NULL, NULL)
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (26, 7, N'En Ejecución', N'En Ejecución', 1, 1, CAST(N'2018-09-24 22:40:22.423' AS DateTime), NULL, NULL)
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (27, 12, N'Cancelada', N'Cancelada', 1, 0, CAST(N'2018-09-30 11:53:16.000' AS DateTime), 1, CAST(N'2018-10-03 05:40:28.833' AS DateTime))
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (28, 12, N'Registrada', N'Registrada', 1, 0, CAST(N'2018-09-30 11:57:05.000' AS DateTime), 1, CAST(N'2018-10-03 05:42:41.703' AS DateTime))
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (29, 12, N'En Ejecución', N'En Ejecución', 1, 1, CAST(N'2018-09-30 11:57:49.087' AS DateTime), NULL, NULL)
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (30, 12, N'Ejecutada', N'Ejecutada', 1, 0, CAST(N'2018-09-30 11:58:10.000' AS DateTime), 1, CAST(N'2018-10-03 05:40:55.840' AS DateTime))
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (31, 1, N'Ltr', N'Litro', 1, 1, CAST(N'2018-10-25 11:03:46.427' AS DateTime), NULL, NULL)
INSERT [admin].[DetCatalogo] ([IdDetCatalogo], [IdMstCatalogo], [Valor], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (32, 1, N'Unidad', N'Unidad', 1, 1, CAST(N'2018-11-04 18:03:28.947' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [admin].[Logger] ON 

INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1, CAST(N'2019-07-04 22:01:11.827' AS DateTime), N'10', N'ERROR', N'Common.Log.Logger', N'A Isaac Rivera ya se le asignó una tarea, por ende no se puede desvincular del proyecto.', N'System.Exception: A Isaac Rivera ya se le asignó una tarea, por ende no se puede desvincular del proyecto.
   at Sigespro.Service.Pro.ProjectService.DeleteManagerProject(MstProyectos project, UnitOfWork unitOfWork) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\ProjectService.cs:line 1229
   at Sigespro.Service.Pro.ProjectService.UpdateProject(MstProyectos project) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\ProjectService.cs:line 518', N'irivera', N'UpdateProject - ProjectController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (2, CAST(N'2019-07-04 23:08:43.383' AS DateTime), N'7', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 484
   at Sigespro.Web.Mvc.Controllers.Project.TaskController.UpdateTask(TaskViewModel model) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Web.Mvc\Controllers\Project\TaskController.cs:line 627', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (3, CAST(N'2019-07-04 23:08:48.457' AS DateTime), N'16', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 484
   at Sigespro.Web.Mvc.Controllers.Project.TaskController.UpdateTask(TaskViewModel model) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Web.Mvc\Controllers\Project\TaskController.cs:line 627', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (4, CAST(N'2019-07-04 23:08:49.933' AS DateTime), N'6', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 484
   at Sigespro.Web.Mvc.Controllers.Project.TaskController.UpdateTask(TaskViewModel model) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Web.Mvc\Controllers\Project\TaskController.cs:line 627', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (5, CAST(N'2019-07-04 23:09:16.490' AS DateTime), N'6', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 484
   at Sigespro.Web.Mvc.Controllers.Project.TaskController.UpdateTask(TaskViewModel model) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Web.Mvc\Controllers\Project\TaskController.cs:line 627', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (6, CAST(N'2019-07-04 23:09:19.083' AS DateTime), N'12', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 484
   at Sigespro.Web.Mvc.Controllers.Project.TaskController.UpdateTask(TaskViewModel model) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Web.Mvc\Controllers\Project\TaskController.cs:line 627', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (7, CAST(N'2019-07-04 23:09:34.037' AS DateTime), N'7', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 484
   at Sigespro.Web.Mvc.Controllers.Project.TaskController.UpdateTask(TaskViewModel model) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Web.Mvc\Controllers\Project\TaskController.cs:line 627', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (8, CAST(N'2019-07-04 23:09:46.850' AS DateTime), N'22', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 484
   at Sigespro.Web.Mvc.Controllers.Project.TaskController.UpdateTask(TaskViewModel model) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Web.Mvc\Controllers\Project\TaskController.cs:line 627', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (9, CAST(N'2019-07-04 23:11:40.093' AS DateTime), N'18', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 484
   at Sigespro.Web.Mvc.Controllers.Project.TaskController.UpdateTask(TaskViewModel model) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Web.Mvc\Controllers\Project\TaskController.cs:line 627', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (10, CAST(N'2019-07-05 22:07:34.137' AS DateTime), N'120', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 484
   at Sigespro.Web.Mvc.Controllers.Project.TaskController.UpdateTask(TaskViewModel model) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Web.Mvc\Controllers\Project\TaskController.cs:line 627', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (11, CAST(N'2019-07-05 23:21:53.783' AS DateTime), N'8', N'INFO', N'Common.Log.Logger', N'Object reference not set to an instance of an object.', N'System.NullReferenceException: Object reference not set to an instance of an object.
   at System.Collections.Specialized.NameObjectCollectionBase.BaseRemove(String name)
   at System.Web.SessionState.SessionStateItemCollection.Remove(String name)
   at System.Web.HttpSessionStateWrapper.Remove(String name)
   at Sigespro.Web.Mvc.Utilities.Attributes.Security.AuthCustomAttribute.IsUserAuthorized() in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Web.Mvc\Utilities\Attributes\Security\AuthCustomAttribute.cs:line 137', N'irivera', N'IsUserAuthorized - AuthCustomAttribute', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (12, CAST(N'2019-07-05 23:28:33.480' AS DateTime), N'6', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 484
   at Sigespro.Web.Mvc.Controllers.Project.TaskController.UpdateTask(TaskViewModel model) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Web.Mvc\Controllers\Project\TaskController.cs:line 627', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (13, CAST(N'2019-07-05 23:31:19.897' AS DateTime), N'17', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 484
   at Sigespro.Web.Mvc.Controllers.Project.TaskController.UpdateTask(TaskViewModel model) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Web.Mvc\Controllers\Project\TaskController.cs:line 627', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (14, CAST(N'2019-07-05 23:37:42.273' AS DateTime), N'6', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 484
   at Sigespro.Web.Mvc.Controllers.Project.TaskController.UpdateTask(TaskViewModel model) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Web.Mvc\Controllers\Project\TaskController.cs:line 627', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (15, CAST(N'2019-07-05 23:42:27.153' AS DateTime), N'39', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 484
   at Sigespro.Web.Mvc.Controllers.Project.TaskController.UpdateTask(TaskViewModel model) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Web.Mvc\Controllers\Project\TaskController.cs:line 627', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (16, CAST(N'2019-07-05 23:51:17.293' AS DateTime), N'48', N'INFO', N'Common.Log.Logger', N'Object reference not set to an instance of an object.', N'System.NullReferenceException: Object reference not set to an instance of an object.
   at System.Collections.Specialized.NameObjectCollectionBase.BaseRemove(String name)
   at System.Web.SessionState.SessionStateItemCollection.Remove(String name)
   at System.Web.HttpSessionStateWrapper.Remove(String name)
   at Sigespro.Web.Mvc.Utilities.Attributes.Security.AuthCustomAttribute.IsUserAuthorized() in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Web.Mvc\Utilities\Attributes\Security\AuthCustomAttribute.cs:line 137', N'irivera', N'IsUserAuthorized - AuthCustomAttribute', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (17, CAST(N'2019-07-05 23:53:10.957' AS DateTime), N'48', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 484
   at Sigespro.Web.Mvc.Controllers.Project.TaskController.UpdateTask(TaskViewModel model) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Web.Mvc\Controllers\Project\TaskController.cs:line 627', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (18, CAST(N'2019-07-06 00:01:29.057' AS DateTime), N'54', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.DAL.Implementations.UnitOfWork.Commit() in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.DAL\Implementations\UnitOfWork.cs:line 50
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 470', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (19, CAST(N'2019-07-06 00:02:09.697' AS DateTime), N'37', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.DAL.Implementations.UnitOfWork.Commit() in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.DAL\Implementations\UnitOfWork.cs:line 50
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 470', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1010, CAST(N'2019-07-08 19:39:05.287' AS DateTime), N'21', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.DAL.Implementations.UnitOfWork.Commit() in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.DAL\Implementations\UnitOfWork.cs:line 50
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 470', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1011, CAST(N'2019-07-08 19:41:53.397' AS DateTime), N'7', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.DAL.Implementations.UnitOfWork.Commit() in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.DAL\Implementations\UnitOfWork.cs:line 50
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 470', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1012, CAST(N'2019-07-08 19:52:39.357' AS DateTime), N'6', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.DAL.Implementations.UnitOfWork.Commit() in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.DAL\Implementations\UnitOfWork.cs:line 50
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 470', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1013, CAST(N'2019-07-08 20:03:31.217' AS DateTime), N'27', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.DAL.Implementations.UnitOfWork.Commit() in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.DAL\Implementations\UnitOfWork.cs:line 50
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 470', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1014, CAST(N'2019-07-08 20:03:42.217' AS DateTime), N'27', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.DAL.Implementations.UnitOfWork.Commit() in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.DAL\Implementations\UnitOfWork.cs:line 50
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 470', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1015, CAST(N'2019-07-08 20:04:23.980' AS DateTime), N'38', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.DAL.Implementations.UnitOfWork.Commit() in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.DAL\Implementations\UnitOfWork.cs:line 50
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 470', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1016, CAST(N'2019-07-08 20:09:20.050' AS DateTime), N'38', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.DAL.Implementations.UnitOfWork.Commit() in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.DAL\Implementations\UnitOfWork.cs:line 50
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 470', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1017, CAST(N'2019-07-08 20:12:29.720' AS DateTime), N'38', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.DAL.Implementations.UnitOfWork.Commit() in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.DAL\Implementations\UnitOfWork.cs:line 50
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 470', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1018, CAST(N'2019-07-08 20:17:36.673' AS DateTime), N'37', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.DAL.Implementations.UnitOfWork.Commit() in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.DAL\Implementations\UnitOfWork.cs:line 50
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 470', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1019, CAST(N'2019-07-08 21:13:57.920' AS DateTime), N'15', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.DAL.Implementations.UnitOfWork.Commit() in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.DAL\Implementations\UnitOfWork.cs:line 50
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 470', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1020, CAST(N'2019-07-08 21:14:51.123' AS DateTime), N'45', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.DAL.Implementations.UnitOfWork.Commit() in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.DAL\Implementations\UnitOfWork.cs:line 50
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 470', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1021, CAST(N'2019-07-08 21:15:31.020' AS DateTime), N'45', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.DAL.Implementations.UnitOfWork.Commit() in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.DAL\Implementations\UnitOfWork.cs:line 50
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 470', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1022, CAST(N'2019-07-08 21:34:12.073' AS DateTime), N'78', N'ERROR', N'Common.Log.Logger', N'La duración en días de la tarea no puede ser mayor a 7', N'System.Exception: La duración en días de la tarea no puede ser mayor a 7', N'irivera', N'SaveTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1023, CAST(N'2019-07-10 20:26:31.733' AS DateTime), N'60', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.DAL.Implementations.UnitOfWork.Commit() in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.DAL\Implementations\UnitOfWork.cs:line 50
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 470', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1024, CAST(N'2019-07-10 20:26:41.023' AS DateTime), N'56', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.DAL.Implementations.UnitOfWork.Commit() in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.DAL\Implementations\UnitOfWork.cs:line 50
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 470', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1025, CAST(N'2019-07-10 20:28:09.580' AS DateTime), N'58', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.DAL.Implementations.UnitOfWork.Commit() in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.DAL\Implementations\UnitOfWork.cs:line 50
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 470', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1026, CAST(N'2019-07-10 20:31:16.147' AS DateTime), N'21', N'ERROR', N'Common.Log.Logger', N'Value cannot be null.
Parameter name: propertyValues', N'System.ArgumentNullException: Value cannot be null.
Parameter name: propertyValues
   at System.Data.Entity.Utilities.Check.NotNull[T](T value, String parameterName)
   at System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(DbPropertyValues propertyValues)
   at Sigespro.DAL.Implementations.UnitOfWork.Commit() in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.DAL\Implementations\UnitOfWork.cs:line 50
   at Sigespro.Service.Pro.TaskService.UpdateTask(MstTareas task) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Pro\TaskService.cs:line 470', N'irivera', N'UpdateTask - TaskController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1027, CAST(N'2019-07-11 21:40:42.367' AS DateTime), N'9', N'ERROR', N'Common.Log.Logger', N'Exception of type ''System.Exception'' was thrown.', N'System.Exception: Exception of type ''System.Exception'' was thrown.', N'irrivera', N'Login - AccountController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1028, CAST(N'2019-07-11 21:40:48.977' AS DateTime), N'4', N'ERROR', N'Common.Log.Logger', N'Exception of type ''System.Exception'' was thrown.', N'System.Exception: Exception of type ''System.Exception'' was thrown.', N'irrivera', N'Login - AccountController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1029, CAST(N'2019-07-11 22:07:11.700' AS DateTime), N'48', N'ERROR', N'Common.Log.Logger', N'Exception of type ''System.Exception'' was thrown.', N'System.Exception: Exception of type ''System.Exception'' was thrown.', N'ftinoco', N'Login - AccountController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1030, CAST(N'2019-07-15 21:25:53.650' AS DateTime), N'6', N'ERROR', N'Common.Log.Logger', N'El proyecto con el título especificado ya existe.', N'System.Exception: El proyecto con el título especificado ya existe.', N'irivera', N'SaveProject - ProjectController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1031, CAST(N'2019-07-15 21:25:55.803' AS DateTime), N'6', N'ERROR', N'Common.Log.Logger', N'El proyecto con el título especificado ya existe.', N'System.Exception: El proyecto con el título especificado ya existe.', N'irivera', N'SaveProject - ProjectController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1032, CAST(N'2019-10-09 21:56:54.443' AS DateTime), N'19', N'ERROR', N'Common.Log.Logger', N'Exception of type ''System.Exception'' was thrown.', N'System.Exception: Exception of type ''System.Exception'' was thrown.', N'ftinoco', N'Login - AccountController', N'192.168.1.18')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1033, CAST(N'2019-10-12 19:32:43.030' AS DateTime), N'32', N'ERROR', N'Common.Log.Logger', N'Exception of type ''System.Exception'' was thrown.', N'System.Exception: Exception of type ''System.Exception'' was thrown.', N'rvaldez', N'Login - AccountController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1034, CAST(N'2019-10-12 19:32:59.417' AS DateTime), N'36', N'ERROR', N'Common.Log.Logger', N'Exception of type ''System.Exception'' was thrown.', N'System.Exception: Exception of type ''System.Exception'' was thrown.', N'rvaldéz', N'Login - AccountController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1035, CAST(N'2019-10-12 19:33:08.180' AS DateTime), N'32', N'ERROR', N'Common.Log.Logger', N'Exception of type ''System.Exception'' was thrown.', N'System.Exception: Exception of type ''System.Exception'' was thrown.', N'rvaldéz', N'Login - AccountController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1036, CAST(N'2019-10-12 19:33:19.913' AS DateTime), N'32', N'ERROR', N'Common.Log.Logger', N'Exception of type ''System.Exception'' was thrown.', N'System.Exception: Exception of type ''System.Exception'' was thrown.', N'rvaldéz', N'Login - AccountController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1037, CAST(N'2019-10-12 19:33:32.580' AS DateTime), N'36', N'ERROR', N'Common.Log.Logger', N'Exception of type ''System.Exception'' was thrown.', N'System.Exception: Exception of type ''System.Exception'' was thrown.', N'rvaldéz', N'Login - AccountController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1038, CAST(N'2019-10-12 19:33:49.013' AS DateTime), N'36', N'ERROR', N'Common.Log.Logger', N'Exception of type ''System.Exception'' was thrown.', N'System.Exception: Exception of type ''System.Exception'' was thrown.', N'rvaldéz', N'Login - AccountController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1039, CAST(N'2019-10-12 19:34:16.620' AS DateTime), N'36', N'ERROR', N'Common.Log.Logger', N'Exception of type ''System.Exception'' was thrown.', N'System.Exception: Exception of type ''System.Exception'' was thrown.', N'rvaldéz', N'Login - AccountController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1040, CAST(N'2019-10-12 19:34:21.557' AS DateTime), N'36', N'ERROR', N'Common.Log.Logger', N'Exception of type ''System.Exception'' was thrown.', N'System.Exception: Exception of type ''System.Exception'' was thrown.', N'rvaldéz', N'Login - AccountController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1041, CAST(N'2019-10-12 19:34:34.473' AS DateTime), N'35', N'ERROR', N'Common.Log.Logger', N'Exception of type ''System.Exception'' was thrown.', N'System.Exception: Exception of type ''System.Exception'' was thrown.', N'rvaldéz', N'Login - AccountController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1042, CAST(N'2019-10-12 19:35:02.163' AS DateTime), N'34', N'ERROR', N'Common.Log.Logger', N'Exception of type ''System.Exception'' was thrown.', N'System.Exception: Exception of type ''System.Exception'' was thrown.', N'ftinoco', N'Login - AccountController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1043, CAST(N'2019-10-12 20:06:50.353' AS DateTime), N'27', N'ERROR', N'Common.Log.Logger', N'The source was not found, but some or all event logs could not be searched.  Inaccessible logs: Security, State.', N'System.Security.SecurityException: The source was not found, but some or all event logs could not be searched.  Inaccessible logs: Security, State.
   at System.Diagnostics.EventLog.FindSourceRegistration(String source, String machineName, Boolean readOnly, Boolean wantToCreate)
   at System.Diagnostics.EventLog.SourceExists(String source, String machineName, Boolean wantToCreate)
   at System.Diagnostics.EventLog.SourceExists(String source)
   at Common.Helpers.Utilities.WriteEventLogEntry(String message, String method, String source) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Common\Helpers\Utilities.cs:line 109
   at Common.Helpers.Utilities.SendEmail(String emailTo, String emailFromDisplayName, String Subject, String BodyEmail, ConfigDataMailing ConfigEmail) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Common\Helpers\Utilities.cs:line 173
   at Sigespro.Service.Security.UserService.InsertUser(Users user, Int32 rolID) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Security\UserService.cs:line 429
The Zone of the assembly that failed was:
MyComputer', N'ftinoco', N'SaveUser - UserController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1044, CAST(N'2019-10-12 20:07:05.120' AS DateTime), N'37', N'ERROR', N'Common.Log.Logger', N'The source was not found, but some or all event logs could not be searched.  Inaccessible logs: Security, State.', N'System.Security.SecurityException: The source was not found, but some or all event logs could not be searched.  Inaccessible logs: Security, State.
   at System.Diagnostics.EventLog.FindSourceRegistration(String source, String machineName, Boolean readOnly, Boolean wantToCreate)
   at System.Diagnostics.EventLog.SourceExists(String source, String machineName, Boolean wantToCreate)
   at System.Diagnostics.EventLog.SourceExists(String source)
   at Common.Helpers.Utilities.WriteEventLogEntry(String message, String method, String source) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Common\Helpers\Utilities.cs:line 109
   at Common.Helpers.Utilities.SendEmail(String emailTo, String emailFromDisplayName, String Subject, String BodyEmail, ConfigDataMailing ConfigEmail) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Common\Helpers\Utilities.cs:line 173
   at Sigespro.Service.Security.UserService.InsertUser(Users user, Int32 rolID) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Security\UserService.cs:line 429
The Zone of the assembly that failed was:
MyComputer', N'ftinoco', N'SaveUser - UserController', N'::1')
INSERT [admin].[Logger] ([Id], [Date], [Thread], [Level], [Logger], [Message], [Exception], [User], [Class], [IP]) VALUES (1045, CAST(N'2019-10-13 16:51:41.903' AS DateTime), N'34', N'ERROR', N'Common.Log.Logger', N'The source was not found, but some or all event logs could not be searched.  Inaccessible logs: Security, State.', N'System.Security.SecurityException: The source was not found, but some or all event logs could not be searched.  Inaccessible logs: Security, State.
   at System.Diagnostics.EventLog.FindSourceRegistration(String source, String machineName, Boolean readOnly, Boolean wantToCreate)
   at System.Diagnostics.EventLog.SourceExists(String source, String machineName, Boolean wantToCreate)
   at System.Diagnostics.EventLog.SourceExists(String source)
   at Common.Helpers.Utilities.WriteEventLogEntry(String message, String method, String source) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Common\Helpers\Utilities.cs:line 109
   at Common.Helpers.Utilities.SendEmail(String emailTo, String emailFromDisplayName, String Subject, String BodyEmail, ConfigDataMailing ConfigEmail) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Common\Helpers\Utilities.cs:line 173
   at Sigespro.Service.Security.UserService.ResetPassword(Int32 userID) in C:\Users\Ftinococ\Dropbox\SIGESPRO\Sigespro\Sigespro.Service\Security\UserService.cs:line 515
The Zone of the assembly that failed was:
MyComputer', N'ftinoco', N'ResetPassword - UserController', N'192.168.1.18')
SET IDENTITY_INSERT [admin].[Logger] OFF
INSERT [admin].[MstCatalogo] ([IdCatalogo], [Nombre], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (1, N'UnidadMedida', N'Unidades de medidas', 1, 1, CAST(N'2016-12-26 13:17:33.227' AS DateTime), NULL, NULL)
INSERT [admin].[MstCatalogo] ([IdCatalogo], [Nombre], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (2, N'TipoRubro', N'Tipos de rubros', 1, 1, CAST(N'2016-12-29 15:58:32.230' AS DateTime), NULL, NULL)
INSERT [admin].[MstCatalogo] ([IdCatalogo], [Nombre], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (3, N'CargosProgramas', N'Cargos de Programas', 1, 1, CAST(N'2016-12-29 15:59:04.613' AS DateTime), 1, CAST(N'2017-04-16 02:07:07.347' AS DateTime))
INSERT [admin].[MstCatalogo] ([IdCatalogo], [Nombre], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (4, N'TipoDireccion', N'Tipo de dirección', 1, 1, CAST(N'2016-12-29 15:59:40.110' AS DateTime), NULL, NULL)
INSERT [admin].[MstCatalogo] ([IdCatalogo], [Nombre], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (5, N'TipoTelefono', N'Tipo de teléfono', 1, 1, CAST(N'2016-12-29 16:00:17.097' AS DateTime), NULL, NULL)
INSERT [admin].[MstCatalogo] ([IdCatalogo], [Nombre], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (6, N'TipoProyecto', N'Tipos de proyectos', 1, 1, CAST(N'2016-12-29 16:02:53.517' AS DateTime), NULL, NULL)
INSERT [admin].[MstCatalogo] ([IdCatalogo], [Nombre], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (7, N'EstadoProyecto', N'Estados de proyectos', 1, 1, CAST(N'2017-01-13 22:58:44.910' AS DateTime), NULL, NULL)
INSERT [admin].[MstCatalogo] ([IdCatalogo], [Nombre], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (8, N'JornadaLaboralSemanal', N'Días laborales por semana', 1, 1, CAST(N'2017-01-13 23:58:09.123' AS DateTime), 1, CAST(N'2017-02-20 23:56:57.223' AS DateTime))
INSERT [admin].[MstCatalogo] ([IdCatalogo], [Nombre], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (10, N'AccionesPaginas', N'Acción de las páginas', 1, 1, CAST(N'2017-05-21 13:22:45.857' AS DateTime), NULL, NULL)
INSERT [admin].[MstCatalogo] ([IdCatalogo], [Nombre], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (11, N'HorarioLaboral', N'Horarios laborales', 1, 1, CAST(N'2018-07-06 21:24:41.573' AS DateTime), NULL, CAST(N'2018-07-06 22:05:46.257' AS DateTime))
INSERT [admin].[MstCatalogo] ([IdCatalogo], [Nombre], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (12, N'EstadoTareas', N'Estado de tareas', 1, 1, CAST(N'2018-09-30 11:51:29.900' AS DateTime), NULL, NULL)
INSERT [admin].[MstSecuencias] ([NombreTabla], [SecuencialTabla], [DescTabla]) VALUES (N'[admin].[DetCatalogo]', 32, NULL)
INSERT [admin].[MstSecuencias] ([NombreTabla], [SecuencialTabla], [DescTabla]) VALUES (N'[admin].[MstCatalogo]', 12, NULL)
INSERT [admin].[MstSecuencias] ([NombreTabla], [SecuencialTabla], [DescTabla]) VALUES (N'[admin].[Tokens]', 13, NULL)
INSERT [admin].[MstSecuencias] ([NombreTabla], [SecuencialTabla], [DescTabla]) VALUES (N'[config].[CatFeriados]', 7, NULL)
INSERT [admin].[MstSecuencias] ([NombreTabla], [SecuencialTabla], [DescTabla]) VALUES (N'[config].[DetPersonaDireccion]', 19, NULL)
INSERT [admin].[MstSecuencias] ([NombreTabla], [SecuencialTabla], [DescTabla]) VALUES (N'[config].[DetPersonaEmail]', 18, NULL)
INSERT [admin].[MstSecuencias] ([NombreTabla], [SecuencialTabla], [DescTabla]) VALUES (N'[config].[DetPersonaTelefono]', 18, NULL)
INSERT [admin].[MstSecuencias] ([NombreTabla], [SecuencialTabla], [DescTabla]) VALUES (N'[config].[MstPersonas]', 18, NULL)
INSERT [admin].[MstSecuencias] ([NombreTabla], [SecuencialTabla], [DescTabla]) VALUES (N'[config].[MstPrograma]', 4, NULL)
INSERT [admin].[MstSecuencias] ([NombreTabla], [SecuencialTabla], [DescTabla]) VALUES (N'[pre].[CatArticulos]', 5, NULL)
INSERT [admin].[MstSecuencias] ([NombreTabla], [SecuencialTabla], [DescTabla]) VALUES (N'[pre].[CatMoneda]', 4, NULL)
INSERT [admin].[MstSecuencias] ([NombreTabla], [SecuencialTabla], [DescTabla]) VALUES (N'[pre].[MstPresupuesto]', 55, NULL)
INSERT [admin].[MstSecuencias] ([NombreTabla], [SecuencialTabla], [DescTabla]) VALUES (N'[pro].[DetPersonal]', 18, NULL)
INSERT [admin].[MstSecuencias] ([NombreTabla], [SecuencialTabla], [DescTabla]) VALUES (N'[pro].[DetProgramaProyecto]', 22, NULL)
INSERT [admin].[MstSecuencias] ([NombreTabla], [SecuencialTabla], [DescTabla]) VALUES (N'[pro].[DetProyectoObjetivos]', 3, NULL)
INSERT [admin].[MstSecuencias] ([NombreTabla], [SecuencialTabla], [DescTabla]) VALUES (N'[pro].[DetProyectoResponsable]', 14, NULL)
INSERT [admin].[MstSecuencias] ([NombreTabla], [SecuencialTabla], [DescTabla]) VALUES (N'[pro].[MstEntregables]', 79, NULL)
INSERT [admin].[MstSecuencias] ([NombreTabla], [SecuencialTabla], [DescTabla]) VALUES (N'[pro].[MstObjetivos]', 4, NULL)
INSERT [admin].[MstSecuencias] ([NombreTabla], [SecuencialTabla], [DescTabla]) VALUES (N'[pro].[MstProyectos]', 11, NULL)
INSERT [admin].[MstSecuencias] ([NombreTabla], [SecuencialTabla], [DescTabla]) VALUES (N'[Security].[Actions]', 4, NULL)
INSERT [admin].[MstSecuencias] ([NombreTabla], [SecuencialTabla], [DescTabla]) VALUES (N'[Security].[ActionsOptions]', 1, NULL)
INSERT [admin].[MstSecuencias] ([NombreTabla], [SecuencialTabla], [DescTabla]) VALUES (N'[Security].[LoginHistory]', 319, NULL)
INSERT [admin].[MstSecuencias] ([NombreTabla], [SecuencialTabla], [DescTabla]) VALUES (N'[Security].[Menu]', 1, NULL)
INSERT [admin].[MstSecuencias] ([NombreTabla], [SecuencialTabla], [DescTabla]) VALUES (N'[Security].[MenuItems]', 1, NULL)
INSERT [admin].[MstSecuencias] ([NombreTabla], [SecuencialTabla], [DescTabla]) VALUES (N'[Security].[PasswordHistory]', 26, NULL)
INSERT [admin].[MstSecuencias] ([NombreTabla], [SecuencialTabla], [DescTabla]) VALUES (N'[Security].[Roles]', 5, NULL)
INSERT [admin].[MstSecuencias] ([NombreTabla], [SecuencialTabla], [DescTabla]) VALUES (N'[Security].[Systems]', 1, NULL)
INSERT [admin].[MstSecuencias] ([NombreTabla], [SecuencialTabla], [DescTabla]) VALUES (N'[Security].[Users]', 16, NULL)
INSERT [config].[CatFeriados] ([IdFeriado], [MotivoFeriado], [FechaFeriado], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (1, N'Primer dia del año', CAST(N'2017-01-01 06:00:00.000' AS DateTime), 1, 1, CAST(N'2016-12-28 18:36:59.743' AS DateTime), NULL, NULL)
INSERT [config].[CatFeriados] ([IdFeriado], [MotivoFeriado], [FechaFeriado], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (2, N'Dias de las madres', CAST(N'2017-05-30 06:00:00.000' AS DateTime), 1, 1, CAST(N'2016-12-28 19:17:38.753' AS DateTime), NULL, NULL)
INSERT [config].[CatFeriados] ([IdFeriado], [MotivoFeriado], [FechaFeriado], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (3, N'Día de la revolución', CAST(N'2017-07-19 06:00:00.000' AS DateTime), 1, 1, CAST(N'2016-12-29 14:35:36.087' AS DateTime), 1, CAST(N'2016-12-29 14:58:43.207' AS DateTime))
INSERT [config].[CatFeriados] ([IdFeriado], [MotivoFeriado], [FechaFeriado], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (4, N'Jueves Santo', CAST(N'2017-04-06 06:00:00.000' AS DateTime), 1, 1, CAST(N'2017-01-14 13:31:26.860' AS DateTime), NULL, NULL)
INSERT [config].[CatFeriados] ([IdFeriado], [MotivoFeriado], [FechaFeriado], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (5, N'Viernes santo', CAST(N'2017-04-07 06:00:00.000' AS DateTime), 1, 1, CAST(N'2017-01-14 13:31:46.650' AS DateTime), NULL, NULL)
INSERT [config].[CatFeriados] ([IdFeriado], [MotivoFeriado], [FechaFeriado], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (6, N'Jueves Santo', CAST(N'2016-04-15 00:00:00.000' AS DateTime), 0, 1, CAST(N'2017-01-14 00:00:00.000' AS DateTime), 1, CAST(N'2017-05-01 20:05:41.883' AS DateTime))
INSERT [config].[CatFeriados] ([IdFeriado], [MotivoFeriado], [FechaFeriado], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (7, N'Día del trabajador', CAST(N'2017-04-30 00:00:00.000' AS DateTime), 1, 1, CAST(N'2017-04-30 22:09:07.353' AS DateTime), NULL, NULL)
INSERT [config].[DetHorario] ([IdDetHorario], [IdHorario], [Dia], [HoraEntrada], [HoraSalida]) VALUES (1, 22, 1, CAST(N'08:00:00' AS Time), CAST(N'17:00:00' AS Time))
INSERT [config].[DetHorario] ([IdDetHorario], [IdHorario], [Dia], [HoraEntrada], [HoraSalida]) VALUES (2, 22, 2, CAST(N'08:00:00' AS Time), CAST(N'17:00:00' AS Time))
INSERT [config].[DetHorario] ([IdDetHorario], [IdHorario], [Dia], [HoraEntrada], [HoraSalida]) VALUES (3, 22, 3, CAST(N'08:00:00' AS Time), CAST(N'17:00:00' AS Time))
INSERT [config].[DetHorario] ([IdDetHorario], [IdHorario], [Dia], [HoraEntrada], [HoraSalida]) VALUES (4, 22, 4, CAST(N'08:00:00' AS Time), CAST(N'17:00:00' AS Time))
INSERT [config].[DetHorario] ([IdDetHorario], [IdHorario], [Dia], [HoraEntrada], [HoraSalida]) VALUES (5, 22, 5, CAST(N'08:00:00' AS Time), CAST(N'17:00:00' AS Time))
INSERT [config].[DetHorario] ([IdDetHorario], [IdHorario], [Dia], [HoraEntrada], [HoraSalida]) VALUES (6, 22, 6, CAST(N'08:00:00' AS Time), CAST(N'12:00:00' AS Time))
INSERT [config].[DetHorario] ([IdDetHorario], [IdHorario], [Dia], [HoraEntrada], [HoraSalida]) VALUES (7, 21, 0, CAST(N'08:00:00' AS Time), CAST(N'12:00:00' AS Time))
INSERT [config].[DetHorario] ([IdDetHorario], [IdHorario], [Dia], [HoraEntrada], [HoraSalida]) VALUES (8, 21, 1, CAST(N'08:00:00' AS Time), CAST(N'17:00:00' AS Time))
INSERT [config].[DetHorario] ([IdDetHorario], [IdHorario], [Dia], [HoraEntrada], [HoraSalida]) VALUES (9, 21, 2, CAST(N'08:00:00' AS Time), CAST(N'17:00:00' AS Time))
INSERT [config].[DetHorario] ([IdDetHorario], [IdHorario], [Dia], [HoraEntrada], [HoraSalida]) VALUES (10, 21, 3, CAST(N'08:00:00' AS Time), CAST(N'17:00:00' AS Time))
INSERT [config].[DetHorario] ([IdDetHorario], [IdHorario], [Dia], [HoraEntrada], [HoraSalida]) VALUES (11, 21, 4, CAST(N'08:00:00' AS Time), CAST(N'17:00:00' AS Time))
INSERT [config].[DetHorario] ([IdDetHorario], [IdHorario], [Dia], [HoraEntrada], [HoraSalida]) VALUES (12, 21, 5, CAST(N'08:00:00' AS Time), CAST(N'17:00:00' AS Time))
INSERT [config].[DetHorario] ([IdDetHorario], [IdHorario], [Dia], [HoraEntrada], [HoraSalida]) VALUES (13, 21, 6, CAST(N'08:00:00' AS Time), CAST(N'17:00:00' AS Time))
INSERT [config].[DetPersonaDireccion] ([IdPersonaDireccion], [IdPersona], [IdTipoDireccion], [Direccion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (1, 1, 9, N'La Primavera', 1, 1, CAST(N'2016-12-29 17:12:37.087' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaDireccion] ([IdPersonaDireccion], [IdPersona], [IdTipoDireccion], [Direccion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (2, 1, 10, N'Xolo', 1, 1, CAST(N'2017-01-28 11:17:05.227' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaDireccion] ([IdPersonaDireccion], [IdPersona], [IdTipoDireccion], [Direccion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (4, 3, 9, N'Colonia 14 de Septiembre', 1, 1, CAST(N'2018-11-01 20:41:48.357' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaDireccion] ([IdPersonaDireccion], [IdPersona], [IdTipoDireccion], [Direccion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (9, 8, 10, N'Centro Ecuménico Fray Antonio de Valdivieso', 1, 1, CAST(N'2018-11-01 20:52:41.680' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaDireccion] ([IdPersonaDireccion], [IdPersona], [IdTipoDireccion], [Direccion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (12, 11, 10, N'CAV', 1, 1, CAST(N'2018-11-27 22:30:16.807' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaDireccion] ([IdPersonaDireccion], [IdPersona], [IdTipoDireccion], [Direccion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (13, 12, 10, N'CAV', 1, 1, CAST(N'2018-11-27 22:32:05.450' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaDireccion] ([IdPersonaDireccion], [IdPersona], [IdTipoDireccion], [Direccion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (14, 13, 10, N'CAV', 1, 1, CAST(N'2018-11-27 22:33:57.943' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaDireccion] ([IdPersonaDireccion], [IdPersona], [IdTipoDireccion], [Direccion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (15, 14, 10, N'CAV', 1, 1, CAST(N'2018-11-27 22:35:43.053' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaDireccion] ([IdPersonaDireccion], [IdPersona], [IdTipoDireccion], [Direccion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (16, 15, 10, N'CAV', 1, 1, CAST(N'2018-11-27 22:36:38.643' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaDireccion] ([IdPersonaDireccion], [IdPersona], [IdTipoDireccion], [Direccion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (17, 16, 10, N'CAV', 1, 1, CAST(N'2019-01-21 22:10:12.423' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaDireccion] ([IdPersonaDireccion], [IdPersona], [IdTipoDireccion], [Direccion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (18, 17, 10, N'CAV', 1, 1, CAST(N'2019-01-21 22:13:44.847' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaDireccion] ([IdPersonaDireccion], [IdPersona], [IdTipoDireccion], [Direccion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (19, 18, 10, N'CAV', 1, 1, CAST(N'2019-01-21 22:18:46.467' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaEmail] ([IdPersonaEmail], [IdPersona], [Email], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (1, 1, N'ftinoco_45@yahoo.com', 1, 1, CAST(N'2016-12-29 17:12:37.117' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaEmail] ([IdPersonaEmail], [IdPersona], [Email], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (3, 3, N'ftinoco_45@yahoo.com', 1, 1, CAST(N'2018-11-01 20:41:48.407' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaEmail] ([IdPersonaEmail], [IdPersona], [Email], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (8, 8, N'kiarafabiomasis@gmail.com', 1, 1, CAST(N'2018-11-01 20:52:41.687' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaEmail] ([IdPersonaEmail], [IdPersona], [Email], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (11, 11, N'kiarafabiomasis@gmail.com', 1, 1, CAST(N'2018-11-27 22:30:16.863' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaEmail] ([IdPersonaEmail], [IdPersona], [Email], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (12, 12, N'kiarafabiomasis@gmail.com', 1, 1, CAST(N'2018-11-27 22:32:05.457' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaEmail] ([IdPersonaEmail], [IdPersona], [Email], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (13, 13, N'ftinococ.45@gmail.com', 1, 1, CAST(N'2018-11-27 22:33:57.950' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaEmail] ([IdPersonaEmail], [IdPersona], [Email], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (14, 14, N'fernandotinoco_45@hotmail.com', 1, 1, CAST(N'2018-11-27 22:35:43.060' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaEmail] ([IdPersonaEmail], [IdPersona], [Email], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (15, 15, N'fernandotinoco_45@hotmail.com', 1, 1, CAST(N'2018-11-27 22:36:38.000' AS DateTime), 15, CAST(N'2018-11-27 22:42:39.297' AS DateTime))
INSERT [config].[DetPersonaEmail] ([IdPersonaEmail], [IdPersona], [Email], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (16, 16, N'ftinoco_45@yahoo.com', 1, 1, CAST(N'2019-01-21 22:10:12.477' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaEmail] ([IdPersonaEmail], [IdPersona], [Email], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (17, 17, N'ftinoco_45@yahoo.com', 1, 1, CAST(N'2019-01-21 22:13:44.853' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaEmail] ([IdPersonaEmail], [IdPersona], [Email], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (18, 18, N'ftinoco_45@yahoo.com', 1, 1, CAST(N'2019-01-21 22:18:46.473' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaTelefono] ([IdPersonaTelefono], [IdPersona], [IdTipoNumero], [NumeroTelefono], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (1, 1, 11, N'22277060', 1, 1, CAST(N'2016-12-29 17:12:37.137' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaTelefono] ([IdPersonaTelefono], [IdPersona], [IdTipoNumero], [NumeroTelefono], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (3, 3, 12, N'88888888', 1, 1, CAST(N'2018-11-01 20:41:48.463' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaTelefono] ([IdPersonaTelefono], [IdPersona], [IdTipoNumero], [NumeroTelefono], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (8, 8, 11, N'22222222', 1, 1, CAST(N'2018-11-01 20:52:41.700' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaTelefono] ([IdPersonaTelefono], [IdPersona], [IdTipoNumero], [NumeroTelefono], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (11, 11, 12, N'88888888', 1, 1, CAST(N'2018-11-27 22:30:16.913' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaTelefono] ([IdPersonaTelefono], [IdPersona], [IdTipoNumero], [NumeroTelefono], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (12, 12, 11, N'22222222', 1, 1, CAST(N'2018-11-27 22:32:05.463' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaTelefono] ([IdPersonaTelefono], [IdPersona], [IdTipoNumero], [NumeroTelefono], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (13, 13, 11, N'22222222', 1, 1, CAST(N'2018-11-27 22:33:57.957' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaTelefono] ([IdPersonaTelefono], [IdPersona], [IdTipoNumero], [NumeroTelefono], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (14, 14, 12, N'88888888', 1, 1, CAST(N'2018-11-27 22:35:43.067' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaTelefono] ([IdPersonaTelefono], [IdPersona], [IdTipoNumero], [NumeroTelefono], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (15, 15, 11, N'22222222', 1, 1, CAST(N'2018-11-27 22:36:38.657' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaTelefono] ([IdPersonaTelefono], [IdPersona], [IdTipoNumero], [NumeroTelefono], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (16, 16, 11, N'22222222', 1, 1, CAST(N'2019-01-21 22:10:12.560' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaTelefono] ([IdPersonaTelefono], [IdPersona], [IdTipoNumero], [NumeroTelefono], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (17, 17, 12, N'89947284', 1, 1, CAST(N'2019-01-21 22:13:44.860' AS DateTime), NULL, NULL)
INSERT [config].[DetPersonaTelefono] ([IdPersonaTelefono], [IdPersona], [IdTipoNumero], [NumeroTelefono], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (18, 18, 12, N'89947284', 1, 1, CAST(N'2019-01-21 22:18:46.483' AS DateTime), NULL, NULL)
INSERT [config].[MstPersonas] ([IdPersona], [Identificacion], [NombreCompleto], [SexoPersona], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (1, N'0013005920049T', N'Fernando Tinoco', N'M', 1, 1, CAST(N'2016-12-29 17:12:37.040' AS DateTime), 1, CAST(N'2018-11-27 22:30:50.703' AS DateTime))
INSERT [config].[MstPersonas] ([IdPersona], [Identificacion], [NombreCompleto], [SexoPersona], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (3, N'0000000000012W', N'Rafael Valdés', N'M', 1, 1, CAST(N'2018-11-01 20:41:48.200' AS DateTime), 1, CAST(N'2018-11-01 20:48:57.377' AS DateTime))
INSERT [config].[MstPersonas] ([IdPersona], [Identificacion], [NombreCompleto], [SexoPersona], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (8, N'0000000000093K', N'Kiara Fabiola Masis Chavarría', N'M', 1, 1, CAST(N'2018-11-01 20:52:41.660' AS DateTime), 1, CAST(N'2018-11-01 20:55:04.450' AS DateTime))
INSERT [config].[MstPersonas] ([IdPersona], [Identificacion], [NombreCompleto], [SexoPersona], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (11, N'0013005920059X', N'Laura Galeano', N'F', 1, 1, CAST(N'2018-11-27 22:30:16.727' AS DateTime), 1, CAST(N'2018-11-27 22:32:18.813' AS DateTime))
INSERT [config].[MstPersonas] ([IdPersona], [Identificacion], [NombreCompleto], [SexoPersona], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (12, N'0013006820049Z', N'Mariela Bucardo', N'F', 1, 1, CAST(N'2018-11-27 22:32:05.443' AS DateTime), NULL, NULL)
INSERT [config].[MstPersonas] ([IdPersona], [Identificacion], [NombreCompleto], [SexoPersona], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (13, N'0010101910001W', N'Germán López', N'M', 1, 1, CAST(N'2018-11-27 22:33:57.937' AS DateTime), NULL, NULL)
INSERT [config].[MstPersonas] ([IdPersona], [Identificacion], [NombreCompleto], [SexoPersona], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (14, N'0012802900014Y', N'Isaac Rivera', N'M', 1, 1, CAST(N'2018-11-27 22:35:43.040' AS DateTime), NULL, NULL)
INSERT [config].[MstPersonas] ([IdPersona], [Identificacion], [NombreCompleto], [SexoPersona], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (15, N'0011812850045T', N'Regis Mairena', N'F', 1, 1, CAST(N'2018-11-27 22:36:38.637' AS DateTime), 15, CAST(N'2018-11-27 22:42:39.290' AS DateTime))
INSERT [config].[MstPersonas] ([IdPersona], [Identificacion], [NombreCompleto], [SexoPersona], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (16, N'0000000000000X', N'David Zepeda', N'M', 1, 1, CAST(N'2019-01-21 22:10:12.360' AS DateTime), NULL, NULL)
INSERT [config].[MstPersonas] ([IdPersona], [Identificacion], [NombreCompleto], [SexoPersona], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (17, N'0000000000000S', N'María de lo Angeles Perez Aguilera', N'F', 1, 1, CAST(N'2019-01-21 22:13:44.837' AS DateTime), NULL, NULL)
INSERT [config].[MstPersonas] ([IdPersona], [Identificacion], [NombreCompleto], [SexoPersona], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (18, N'0012503880034E', N'Arturo José Colindres Ramirez', N'M', 1, 1, CAST(N'2019-01-21 22:18:46.457' AS DateTime), NULL, NULL)
INSERT [config].[MstPrograma] ([IdPrograma], [NombrePrograma], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (1, N'PNL', N'Programa Nuevos Liderazgos', 1, 1, CAST(N'2016-12-29 15:23:02.337' AS DateTime), 1, CAST(N'2017-01-14 15:46:54.087' AS DateTime))
INSERT [config].[MstPrograma] ([IdPrograma], [NombrePrograma], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (2, N'Teología y géneros', N'Teología y géneros', 1, 1, CAST(N'2017-01-25 00:00:00.000' AS DateTime), 1, CAST(N'2017-05-01 19:51:44.960' AS DateTime))
INSERT [config].[MstPrograma] ([IdPrograma], [NombrePrograma], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (3, N'CAV', N'Centro Ecumenico Fray Antonio de Valdivieso', 1, 1, CAST(N'2017-05-01 00:00:00.000' AS DateTime), 1, CAST(N'2018-07-06 22:21:42.427' AS DateTime))
INSERT [config].[MstPrograma] ([IdPrograma], [NombrePrograma], [Descripcion], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (4, N'Seguridad Alimentaria', N'Seguridad Alimentaria', 1, 1, CAST(N'2018-11-01 20:54:37.937' AS DateTime), NULL, NULL)
INSERT [pre].[CatArticulos] ([IdArticulo], [IdRubro], [IdUnidadMedida], [IdMoneda], [Descripcion], [Precio], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (N'00001', N'00311', 1, 2, N'Atún', CAST(40.0000 AS Decimal(18, 4)), 1, 1, CAST(N'2016-12-29 17:01:19.380' AS DateTime), 1, CAST(N'2016-12-29 17:04:13.810' AS DateTime))
INSERT [pre].[CatArticulos] ([IdArticulo], [IdRubro], [IdUnidadMedida], [IdMoneda], [Descripcion], [Precio], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (N'00002', N'00311', 1, 1, N'Sardina', CAST(1.0000 AS Decimal(18, 4)), 1, 1, CAST(N'2017-01-28 18:52:48.763' AS DateTime), NULL, NULL)
INSERT [pre].[CatArticulos] ([IdArticulo], [IdRubro], [IdUnidadMedida], [IdMoneda], [Descripcion], [Precio], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (N'00003', N'00312', 31, 2, N'Coca Cola', CAST(28.0000 AS Decimal(18, 4)), 1, 2, CAST(N'2018-07-16 23:19:08.000' AS DateTime), 1, CAST(N'2018-10-25 11:28:03.440' AS DateTime))
INSERT [pre].[CatArticulos] ([IdArticulo], [IdRubro], [IdUnidadMedida], [IdMoneda], [Descripcion], [Precio], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (N'00004', N'00312', 31, 2, N'Pepsi', CAST(25.0000 AS Decimal(18, 4)), 1, 1, CAST(N'2018-10-25 11:04:05.827' AS DateTime), NULL, NULL)
INSERT [pre].[CatArticulos] ([IdArticulo], [IdRubro], [IdUnidadMedida], [IdMoneda], [Descripcion], [Precio], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (N'00005', N'00033', 32, 1, N'Recipiente Para Miel de Abeja', CAST(5.0000 AS Decimal(18, 4)), 0, 3, CAST(N'2018-11-04 18:04:35.000' AS DateTime), 3, CAST(N'2018-11-04 18:29:51.347' AS DateTime))
INSERT [pre].[CatMoneda] ([IdMoneda], [Nombre], [Simbolo], [EsLocal], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (1, N'Dólar', N'$  ', 0, 1, 1, CAST(N'2016-12-29 00:00:00.000' AS DateTime), 1, CAST(N'2017-05-03 22:07:26.583' AS DateTime))
INSERT [pre].[CatMoneda] ([IdMoneda], [Nombre], [Simbolo], [EsLocal], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (2, N'Cordoba', N'C$ ', 1, 1, 1, CAST(N'2016-12-29 00:00:00.000' AS DateTime), 1, CAST(N'2017-05-03 22:07:41.350' AS DateTime))
INSERT [pre].[CatMoneda] ([IdMoneda], [Nombre], [Simbolo], [EsLocal], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (3, N'Euro', N'€  ', 0, 0, 1, CAST(N'2017-03-12 00:00:00.000' AS DateTime), 1, CAST(N'2017-05-03 22:07:13.730' AS DateTime))
INSERT [pre].[CatMoneda] ([IdMoneda], [Nombre], [Simbolo], [EsLocal], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (4, N'Colón', N'¢  ', 0, 1, 1, CAST(N'2017-05-03 21:40:40.927' AS DateTime), NULL, NULL)
INSERT [pre].[CatRubros] ([IdRubro], [Nombre], [IdTipoRubro], [IdRubroParent], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (N'00001', N'SERVICIOS PROFESIONALES', 3, N'00000', 1, 1, CAST(N'2016-12-29 16:45:58.000' AS DateTime), 1, CAST(N'2018-11-01 21:17:39.900' AS DateTime))
INSERT [pre].[CatRubros] ([IdRubro], [Nombre], [IdTipoRubro], [IdRubroParent], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (N'00002', N'SERVICIOS NO PROFESIONALES', 3, N'00000', 1, 1, CAST(N'2016-12-29 16:46:59.000' AS DateTime), 1, CAST(N'2018-11-01 22:45:07.593' AS DateTime))
INSERT [pre].[CatRubros] ([IdRubro], [Nombre], [IdTipoRubro], [IdRubroParent], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (N'00003', N'MATERIALES Y SUMINISTROS', 3, N'00000', 1, 1, CAST(N'2016-12-29 16:47:16.857' AS DateTime), NULL, NULL)
INSERT [pre].[CatRubros] ([IdRubro], [Nombre], [IdTipoRubro], [IdRubroParent], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (N'00004', N'GASTOS VARIOS', 5, N'00000', 1, 3, CAST(N'2018-11-04 18:11:01.293' AS DateTime), NULL, NULL)
INSERT [pre].[CatRubros] ([IdRubro], [Nombre], [IdTipoRubro], [IdRubroParent], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (N'00011', N'PERSONAL PERMANENTE', 3, N'00001', 1, 1, CAST(N'2016-12-29 16:47:42.967' AS DateTime), NULL, NULL)
INSERT [pre].[CatRubros] ([IdRubro], [Nombre], [IdTipoRubro], [IdRubroParent], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (N'00021', N'SERVICIOS BASICOS', 3, N'00002', 1, 1, CAST(N'2016-12-29 16:48:08.377' AS DateTime), NULL, NULL)
INSERT [pre].[CatRubros] ([IdRubro], [Nombre], [IdTipoRubro], [IdRubroParent], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (N'00022', N'ARRENDAMIENTOS Y DERECHOS ', 3, N'00002', 1, 1, CAST(N'2017-05-19 00:10:55.633' AS DateTime), NULL, NULL)
INSERT [pre].[CatRubros] ([IdRubro], [Nombre], [IdTipoRubro], [IdRubroParent], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (N'00031', N'ALIMENTOS Y MATERIALES', 3, N'00003', 1, 1, CAST(N'2016-12-29 16:48:25.450' AS DateTime), NULL, NULL)
INSERT [pre].[CatRubros] ([IdRubro], [Nombre], [IdTipoRubro], [IdRubroParent], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (N'00032', N'EQUIPOS VARIOS', 5, N'00003', 1, 3, CAST(N'2018-11-04 17:51:01.000' AS DateTime), 3, CAST(N'2018-11-04 17:51:19.227' AS DateTime))
INSERT [pre].[CatRubros] ([IdRubro], [Nombre], [IdTipoRubro], [IdRubroParent], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (N'00033', N'MATERIALES VARIOS', 4, N'00003', 0, 3, CAST(N'2018-11-04 17:52:06.000' AS DateTime), 3, CAST(N'2018-11-04 18:31:17.567' AS DateTime))
INSERT [pre].[CatRubros] ([IdRubro], [Nombre], [IdTipoRubro], [IdRubroParent], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (N'00211', N'AGUA', 5, N'00021', 1, 1, CAST(N'2016-12-29 16:49:01.327' AS DateTime), NULL, NULL)
INSERT [pre].[CatRubros] ([IdRubro], [Nombre], [IdTipoRubro], [IdRubroParent], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (N'00212', N'ELECTRICIDAD', 5, N'00021', 1, 1, CAST(N'2016-12-29 16:49:51.760' AS DateTime), NULL, NULL)
INSERT [pre].[CatRubros] ([IdRubro], [Nombre], [IdTipoRubro], [IdRubroParent], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (N'00213', N'LINEA TELEFONICA E INTERNET', 5, N'00021', 1, 1, CAST(N'2018-11-01 21:14:26.720' AS DateTime), NULL, NULL)
INSERT [pre].[CatRubros] ([IdRubro], [Nombre], [IdTipoRubro], [IdRubroParent], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (N'00214', N'COMBUSTIBLE', 5, N'00021', 1, 1, CAST(N'2018-11-04 17:08:55.357' AS DateTime), NULL, NULL)
INSERT [pre].[CatRubros] ([IdRubro], [Nombre], [IdTipoRubro], [IdRubroParent], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (N'00221', N'ALQUILER VEHICULO', 5, N'00022', 1, 1, CAST(N'2018-11-04 17:07:49.990' AS DateTime), NULL, NULL)
INSERT [pre].[CatRubros] ([IdRubro], [Nombre], [IdTipoRubro], [IdRubroParent], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (N'00222', N'ALQUILER LOCAL', 5, N'00022', 1, 1, CAST(N'2018-11-04 17:09:31.597' AS DateTime), NULL, NULL)
INSERT [pre].[CatRubros] ([IdRubro], [Nombre], [IdTipoRubro], [IdRubroParent], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (N'00311', N'ALIMENTOS PARA PERSONAS', 4, N'00031', 0, 1, CAST(N'2016-12-29 16:50:22.000' AS DateTime), 3, CAST(N'2018-11-04 18:31:52.837' AS DateTime))
INSERT [pre].[CatRubros] ([IdRubro], [Nombre], [IdTipoRubro], [IdRubroParent], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (N'00312', N'BEBIDAS NO ALCOHOLICAS ', 4, N'00031', 1, 1, CAST(N'2017-05-19 00:23:05.057' AS DateTime), NULL, NULL)
INSERT [pre].[MstPresupuesto] ([IdPresupuesto], [IdProyecto], [IdEntregable], [IdMoneda], [PresupuestoAsignado], [PresupuestoActual], [PresupuestoEjecutado], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (37, 9, 61, 1, CAST(15000.0000 AS Decimal(18, 4)), CAST(15000.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 1, 15, CAST(N'2019-07-15 21:29:33.997' AS DateTime), 15, CAST(N'2019-08-30 10:54:04.613' AS DateTime))
INSERT [pre].[MstPresupuesto] ([IdPresupuesto], [IdProyecto], [IdEntregable], [IdMoneda], [PresupuestoAsignado], [PresupuestoActual], [PresupuestoEjecutado], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (38, 9, 62, 1, CAST(10000.0000 AS Decimal(18, 4)), CAST(10000.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 1, 15, CAST(N'2019-07-15 21:30:13.927' AS DateTime), 15, CAST(N'2019-08-30 10:56:06.350' AS DateTime))
INSERT [pre].[MstPresupuesto] ([IdPresupuesto], [IdProyecto], [IdEntregable], [IdMoneda], [PresupuestoAsignado], [PresupuestoActual], [PresupuestoEjecutado], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (39, 9, 63, 1, CAST(25000.0000 AS Decimal(18, 4)), CAST(25000.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 1, 15, CAST(N'2019-07-15 21:30:40.693' AS DateTime), 15, CAST(N'2019-08-30 10:56:38.807' AS DateTime))
INSERT [pre].[MstPresupuesto] ([IdPresupuesto], [IdProyecto], [IdEntregable], [IdMoneda], [PresupuestoAsignado], [PresupuestoActual], [PresupuestoEjecutado], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (40, 9, 64, 1, CAST(35000.0000 AS Decimal(18, 4)), CAST(35000.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 1, 15, CAST(N'2019-07-15 21:31:26.693' AS DateTime), 15, CAST(N'2019-08-30 10:57:01.590' AS DateTime))
INSERT [pre].[MstPresupuesto] ([IdPresupuesto], [IdProyecto], [IdEntregable], [IdMoneda], [PresupuestoAsignado], [PresupuestoActual], [PresupuestoEjecutado], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (41, 10, 65, 1, CAST(60000.0000 AS Decimal(18, 4)), CAST(60000.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 1, 15, CAST(N'2019-07-15 21:34:39.440' AS DateTime), 15, CAST(N'2019-08-30 10:59:04.257' AS DateTime))
INSERT [pre].[MstPresupuesto] ([IdPresupuesto], [IdProyecto], [IdEntregable], [IdMoneda], [PresupuestoAsignado], [PresupuestoActual], [PresupuestoEjecutado], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (42, 10, 66, 1, CAST(30000.0000 AS Decimal(18, 4)), CAST(30000.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 1, 15, CAST(N'2019-07-15 21:35:08.557' AS DateTime), 15, CAST(N'2019-08-30 10:59:30.823' AS DateTime))
INSERT [pre].[MstPresupuesto] ([IdPresupuesto], [IdProyecto], [IdEntregable], [IdMoneda], [PresupuestoAsignado], [PresupuestoActual], [PresupuestoEjecutado], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (43, 10, 67, 1, CAST(25000.0000 AS Decimal(18, 4)), CAST(25000.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 1, 15, CAST(N'2019-07-15 21:35:36.773' AS DateTime), 15, CAST(N'2019-08-30 11:10:16.360' AS DateTime))
INSERT [pre].[MstPresupuesto] ([IdPresupuesto], [IdProyecto], [IdEntregable], [IdMoneda], [PresupuestoAsignado], [PresupuestoActual], [PresupuestoEjecutado], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (44, 10, 68, 1, CAST(50000.0000 AS Decimal(18, 4)), CAST(50000.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 1, 15, CAST(N'2019-07-15 21:36:03.847' AS DateTime), NULL, NULL)
INSERT [pre].[MstPresupuesto] ([IdPresupuesto], [IdProyecto], [IdEntregable], [IdMoneda], [PresupuestoAsignado], [PresupuestoActual], [PresupuestoEjecutado], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (45, 10, 69, 1, CAST(60000.0000 AS Decimal(18, 4)), CAST(60000.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 1, 15, CAST(N'2019-07-15 21:36:42.130' AS DateTime), NULL, NULL)
INSERT [pre].[MstPresupuesto] ([IdPresupuesto], [IdProyecto], [IdEntregable], [IdMoneda], [PresupuestoAsignado], [PresupuestoActual], [PresupuestoEjecutado], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (46, 10, 70, 1, CAST(80000.0000 AS Decimal(18, 4)), CAST(80000.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 1, 15, CAST(N'2019-07-15 21:37:23.417' AS DateTime), NULL, NULL)
INSERT [pre].[MstPresupuesto] ([IdPresupuesto], [IdProyecto], [IdEntregable], [IdMoneda], [PresupuestoAsignado], [PresupuestoActual], [PresupuestoEjecutado], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (47, 10, 71, 1, CAST(30000.0000 AS Decimal(18, 4)), CAST(30000.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 1, 15, CAST(N'2019-07-15 21:37:55.293' AS DateTime), NULL, NULL)
INSERT [pre].[MstPresupuesto] ([IdPresupuesto], [IdProyecto], [IdEntregable], [IdMoneda], [PresupuestoAsignado], [PresupuestoActual], [PresupuestoEjecutado], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (48, 10, 72, 1, CAST(70000.0000 AS Decimal(18, 4)), CAST(70000.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 1, 15, CAST(N'2019-07-15 21:38:25.617' AS DateTime), NULL, NULL)
INSERT [pre].[MstPresupuesto] ([IdPresupuesto], [IdProyecto], [IdEntregable], [IdMoneda], [PresupuestoAsignado], [PresupuestoActual], [PresupuestoEjecutado], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (49, 11, 73, 1, CAST(33974.0000 AS Decimal(18, 4)), CAST(33974.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 1, 15, CAST(N'2019-07-15 21:39:45.187' AS DateTime), 15, CAST(N'2019-07-15 21:44:30.527' AS DateTime))
INSERT [pre].[MstPresupuesto] ([IdPresupuesto], [IdProyecto], [IdEntregable], [IdMoneda], [PresupuestoAsignado], [PresupuestoActual], [PresupuestoEjecutado], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (50, 11, 74, 1, CAST(40000.0000 AS Decimal(18, 4)), CAST(40000.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 1, 15, CAST(N'2019-07-15 21:40:18.507' AS DateTime), NULL, NULL)
INSERT [pre].[MstPresupuesto] ([IdPresupuesto], [IdProyecto], [IdEntregable], [IdMoneda], [PresupuestoAsignado], [PresupuestoActual], [PresupuestoEjecutado], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (51, 11, 75, 1, CAST(10000.0000 AS Decimal(18, 4)), CAST(10000.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 1, 15, CAST(N'2019-07-15 21:40:56.367' AS DateTime), NULL, NULL)
INSERT [pre].[MstPresupuesto] ([IdPresupuesto], [IdProyecto], [IdEntregable], [IdMoneda], [PresupuestoAsignado], [PresupuestoActual], [PresupuestoEjecutado], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (52, 11, 76, 1, CAST(17000.0000 AS Decimal(18, 4)), CAST(17000.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 1, 15, CAST(N'2019-07-15 21:41:30.547' AS DateTime), NULL, NULL)
INSERT [pre].[MstPresupuesto] ([IdPresupuesto], [IdProyecto], [IdEntregable], [IdMoneda], [PresupuestoAsignado], [PresupuestoActual], [PresupuestoEjecutado], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (53, 11, 77, 1, CAST(10000.0000 AS Decimal(18, 4)), CAST(10000.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 1, 15, CAST(N'2019-07-15 21:42:14.620' AS DateTime), NULL, NULL)
INSERT [pre].[MstPresupuesto] ([IdPresupuesto], [IdProyecto], [IdEntregable], [IdMoneda], [PresupuestoAsignado], [PresupuestoActual], [PresupuestoEjecutado], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (54, 11, 78, 1, CAST(17000.0000 AS Decimal(18, 4)), CAST(17000.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 1, 15, CAST(N'2019-07-15 21:42:45.307' AS DateTime), NULL, NULL)
INSERT [pre].[MstPresupuesto] ([IdPresupuesto], [IdProyecto], [IdEntregable], [IdMoneda], [PresupuestoAsignado], [PresupuestoActual], [PresupuestoEjecutado], [Activo], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (55, 11, 79, 1, CAST(10000.0000 AS Decimal(18, 4)), CAST(10000.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 1, 15, CAST(N'2019-07-15 21:43:07.883' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [pro].[BitacoraTareas] ON 

INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1625, 1602, 61, 9, 1, CAST(N'2019-10-09 22:02:33.273' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1626, 1603, 61, 9, 1, CAST(N'2019-10-09 22:02:33.337' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1627, 1604, 61, 9, 1, CAST(N'2019-10-09 22:02:33.367' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1628, 1605, 61, 9, 1, CAST(N'2019-10-09 22:02:33.400' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1629, 1606, 61, 9, 1, CAST(N'2019-10-09 22:02:33.430' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1630, 1607, 61, 9, 1, CAST(N'2019-10-09 22:02:33.463' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1631, 1608, 61, 9, 1, CAST(N'2019-10-09 22:02:33.493' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1632, 1609, 61, 9, 1, CAST(N'2019-10-09 22:02:33.523' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1633, 1610, 61, 9, 1, CAST(N'2019-10-09 22:02:33.557' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1634, 1611, 61, 9, 1, CAST(N'2019-10-09 22:02:33.587' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1635, 1612, 61, 9, 1, CAST(N'2019-10-09 22:02:33.617' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1636, 1613, 61, 9, 1, CAST(N'2019-10-09 22:02:33.650' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1637, 1614, 61, 9, 1, CAST(N'2019-10-09 22:02:33.680' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1638, 1615, 61, 9, 1, CAST(N'2019-10-09 22:02:33.713' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1639, 1616, 61, 9, 1, CAST(N'2019-10-09 22:02:33.743' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1640, 1617, 61, 9, 1, CAST(N'2019-10-09 22:02:33.790' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1641, 1618, 61, 9, 1, CAST(N'2019-10-09 22:02:33.883' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1642, 1619, 61, 9, 1, CAST(N'2019-10-09 22:02:33.917' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1643, 1620, 61, 9, 1, CAST(N'2019-10-09 22:02:33.963' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1644, 1621, 61, 9, 1, CAST(N'2019-10-09 22:02:33.993' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1645, 1622, 61, 9, 1, CAST(N'2019-10-09 22:02:34.040' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1646, 1623, 61, 9, 1, CAST(N'2019-10-09 22:02:34.070' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1647, 1624, 61, 9, 1, CAST(N'2019-10-09 22:02:34.117' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1648, 1625, 61, 9, 1, CAST(N'2019-10-09 22:02:34.150' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1649, 1626, 61, 9, 1, CAST(N'2019-10-09 22:02:34.180' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1650, 1627, 61, 9, 1, CAST(N'2019-10-09 22:02:34.227' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1651, 1628, 61, 9, 1, CAST(N'2019-10-09 22:02:34.257' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1652, 1629, 61, 9, 1, CAST(N'2019-10-09 22:02:34.290' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1653, 1630, 61, 9, 1, CAST(N'2019-10-09 22:02:34.337' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1654, 1631, 61, 9, 1, CAST(N'2019-10-09 22:02:34.430' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1655, 1632, 65, 10, 15, CAST(N'2019-10-12 19:18:42.947' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1656, 1633, 65, 10, 15, CAST(N'2019-10-12 19:21:18.663' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1657, 1634, 65, 10, 15, CAST(N'2019-10-12 19:23:23.743' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1658, 1635, 66, 10, 15, CAST(N'2019-10-12 19:25:24.533' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1659, 1636, 66, 10, 15, CAST(N'2019-10-12 19:26:52.643' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1660, 1637, 72, 10, 15, CAST(N'2019-10-12 19:30:16.423' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1661, 1638, 65, 10, 15, CAST(N'2019-10-12 19:48:10.520' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1662, 1639, 65, 10, 15, CAST(N'2019-10-12 19:49:25.790' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1663, 1640, 65, 10, 15, CAST(N'2019-10-12 19:51:22.583' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1664, 1641, 65, 10, 15, CAST(N'2019-10-12 19:55:06.727' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1665, 1642, 67, 10, 15, CAST(N'2019-10-12 19:59:57.100' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1666, 1643, 67, 10, 15, CAST(N'2019-10-12 20:02:12.020' AS DateTime), 28, N'Registrada', NULL, NULL, NULL, 1, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1667, 1637, 72, 10, 16, CAST(N'2019-10-12 20:08:28.237' AS DateTime), 29, N'En Ejecución', N'Capacitación en temas a abordar', NULL, NULL, 0, NULL)
INSERT [pro].[BitacoraTareas] ([IdBitacoraTarea], [IdTarea], [IdEntregable], [IdProyecto], [IdUsuario], [Fecha], [IdEstado], [EstadoDesc], [Comentarios], [Progreso], [NombreArchivo], [Creacion], [IdArchivoTarea]) VALUES (1668, 1637, 72, 10, 16, CAST(N'2019-10-12 20:09:21.193' AS DateTime), 30, N'Ejecutada', N'Ejecutada', CAST(100.00 AS Decimal(5, 2)), NULL, 0, NULL)
SET IDENTITY_INSERT [pro].[BitacoraTareas] OFF
INSERT [pro].[DetPersonal] ([IdPersonal], [IdPersona], [IdInstitucion], [IdPrograma], [IdCargo]) VALUES (1, 1, NULL, 1, 2)
INSERT [pro].[DetPersonal] ([IdPersonal], [IdPersona], [IdInstitucion], [IdPrograma], [IdCargo]) VALUES (3, 3, NULL, 3, 8)
INSERT [pro].[DetPersonal] ([IdPersonal], [IdPersona], [IdInstitucion], [IdPrograma], [IdCargo]) VALUES (8, 8, NULL, 4, 2)
INSERT [pro].[DetPersonal] ([IdPersonal], [IdPersona], [IdInstitucion], [IdPrograma], [IdCargo]) VALUES (11, 11, NULL, 2, 8)
INSERT [pro].[DetPersonal] ([IdPersonal], [IdPersona], [IdInstitucion], [IdPrograma], [IdCargo]) VALUES (12, 12, NULL, 2, 2)
INSERT [pro].[DetPersonal] ([IdPersonal], [IdPersona], [IdInstitucion], [IdPrograma], [IdCargo]) VALUES (13, 13, NULL, 4, 8)
INSERT [pro].[DetPersonal] ([IdPersonal], [IdPersona], [IdInstitucion], [IdPrograma], [IdCargo]) VALUES (14, 14, NULL, 1, 8)
INSERT [pro].[DetPersonal] ([IdPersonal], [IdPersona], [IdInstitucion], [IdPrograma], [IdCargo]) VALUES (15, 15, NULL, 1, 2)
INSERT [pro].[DetPersonal] ([IdPersonal], [IdPersona], [IdInstitucion], [IdPrograma], [IdCargo]) VALUES (16, 16, NULL, 1, 2)
INSERT [pro].[DetPersonal] ([IdPersonal], [IdPersona], [IdInstitucion], [IdPrograma], [IdCargo]) VALUES (17, 17, NULL, 1, 2)
INSERT [pro].[DetPersonal] ([IdPersonal], [IdPersona], [IdInstitucion], [IdPrograma], [IdCargo]) VALUES (18, 18, NULL, 1, 2)
INSERT [pro].[DetProgramaProyecto] ([IdProgramaProyecto], [IdPrograma], [IdProyecto]) VALUES (17, 1, 9)
INSERT [pro].[DetProgramaProyecto] ([IdProgramaProyecto], [IdPrograma], [IdProyecto]) VALUES (18, 1, 10)
INSERT [pro].[DetProgramaProyecto] ([IdProgramaProyecto], [IdPrograma], [IdProyecto]) VALUES (19, 2, 10)
INSERT [pro].[DetProgramaProyecto] ([IdProgramaProyecto], [IdPrograma], [IdProyecto]) VALUES (20, 3, 10)
INSERT [pro].[DetProgramaProyecto] ([IdProgramaProyecto], [IdPrograma], [IdProyecto]) VALUES (21, 4, 10)
INSERT [pro].[DetProgramaProyecto] ([IdProgramaProyecto], [IdPrograma], [IdProyecto]) VALUES (22, 1, 11)
INSERT [pro].[DetProyectoBeneficiarios] ([IdProyecto], [TipoDestinatario], [IdBeneficiario], [Descripcion]) VALUES (9, N'G', NULL, N'70 son mujeres jóvenes, con edades entre 15 y 35 años')
INSERT [pro].[DetProyectoBeneficiarios] ([IdProyecto], [TipoDestinatario], [IdBeneficiario], [Descripcion]) VALUES (10, N'G', NULL, N'Se beneficiarán de las distintas actividades programadas un total de 1.150 personas, 575 de  las cuales son mujeres.')
INSERT [pro].[DetProyectoBeneficiarios] ([IdProyecto], [TipoDestinatario], [IdBeneficiario], [Descripcion]) VALUES (11, N'G', NULL, N'· 45 jóvenes estudiantes de Técnico Básico en Agroecología.
· 300 Estudiantes de Educación Primaria.
· 15 docentes de educación primaria.
· 1800 beneficiarios indirectos.')
SET IDENTITY_INSERT [pro].[DetProyectoObjetivos] ON 

INSERT [pro].[DetProyectoObjetivos] ([IdProyectoObjetivo], [IdProyecto], [IdObjetivo]) VALUES (11, 9, 11)
INSERT [pro].[DetProyectoObjetivos] ([IdProyectoObjetivo], [IdProyecto], [IdObjetivo]) VALUES (12, 9, 12)
INSERT [pro].[DetProyectoObjetivos] ([IdProyectoObjetivo], [IdProyecto], [IdObjetivo]) VALUES (13, 10, 13)
INSERT [pro].[DetProyectoObjetivos] ([IdProyectoObjetivo], [IdProyecto], [IdObjetivo]) VALUES (14, 11, 14)
INSERT [pro].[DetProyectoObjetivos] ([IdProyectoObjetivo], [IdProyecto], [IdObjetivo]) VALUES (15, 11, 15)
SET IDENTITY_INSERT [pro].[DetProyectoObjetivos] OFF
INSERT [pro].[DetProyectoResponsable] ([IdProyectoResponsable], [IdProyecto], [IdPersonal]) VALUES (12, 9, 14)
INSERT [pro].[DetProyectoResponsable] ([IdProyectoResponsable], [IdProyecto], [IdPersonal]) VALUES (13, 10, 3)
INSERT [pro].[DetProyectoResponsable] ([IdProyectoResponsable], [IdProyecto], [IdPersonal]) VALUES (14, 11, 14)
SET IDENTITY_INSERT [pro].[DetTareaPersonal] ON 

INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4399, 9, 61, 1602, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4400, 9, 61, 1603, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4401, 9, 61, 1604, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4402, 9, 61, 1605, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4403, 9, 61, 1606, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4404, 9, 61, 1607, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4405, 9, 61, 1608, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4406, 9, 61, 1609, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4407, 9, 61, 1610, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4408, 9, 61, 1611, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4409, 9, 61, 1612, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4410, 9, 61, 1613, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4411, 9, 61, 1614, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4412, 9, 61, 1615, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4413, 9, 61, 1616, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4415, 9, 61, 1618, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4416, 9, 61, 1619, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4417, 9, 61, 1620, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4418, 9, 61, 1621, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4419, 9, 61, 1622, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4420, 9, 61, 1623, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4421, 9, 61, 1624, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4422, 9, 61, 1625, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4423, 9, 61, 1626, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4424, 9, 61, 1627, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4425, 9, 61, 1628, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4427, 9, 61, 1630, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4429, 9, 61, 1602, 16, N'C', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4430, 9, 61, 1603, 17, N'C', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4431, 9, 61, 1604, 1, N'C', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4432, 9, 61, 1605, 18, N'C', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4433, 9, 61, 1607, 15, N'C', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4434, 9, 61, 1609, 18, N'C', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4435, 9, 61, 1613, 17, N'C', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4436, 9, 61, 1617, 15, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4437, 9, 61, 1617, 17, N'C', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4438, 9, 61, 1629, 18, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4439, 9, 61, 1629, 15, N'C', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4440, 9, 61, 1631, 15, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4441, 9, 61, 1631, 16, N'C', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4442, 10, 65, 1632, 17, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4443, 10, 65, 1632, 8, N'C', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4444, 10, 65, 1632, 11, N'C', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4445, 10, 65, 1633, 16, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4446, 10, 65, 1633, 13, N'C', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4447, 10, 65, 1634, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4448, 10, 65, 1634, 12, N'C', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4449, 10, 66, 1635, 16, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4450, 10, 66, 1635, 8, N'C', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4451, 10, 66, 1636, 11, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4452, 10, 66, 1636, 13, N'C', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4453, 10, 72, 1637, 15, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4454, 10, 72, 1637, 3, N'C', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4455, 10, 65, 1638, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4456, 10, 65, 1638, 11, N'C', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4457, 10, 65, 1639, 17, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4458, 10, 65, 1639, 12, N'C', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4459, 10, 65, 1639, 13, N'C', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4460, 10, 65, 1640, 12, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4461, 10, 65, 1640, 3, N'C', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4462, 10, 65, 1640, 11, N'C', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4463, 10, 65, 1641, 1, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4464, 10, 65, 1641, 11, N'C', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4465, 10, 65, 1641, 14, N'C', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4466, 10, 67, 1642, 16, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4467, 10, 67, 1642, 12, N'C', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4468, 10, 67, 1643, 14, N'R', NULL, NULL)
INSERT [pro].[DetTareaPersonal] ([IdTareaPersonal], [IdProyecto], [IdEntregable], [IdTarea], [IdPersonal], [TipoPersonal], [Ciudad], [Lugar]) VALUES (4469, 10, 67, 1643, 11, N'C', NULL, NULL)
SET IDENTITY_INSERT [pro].[DetTareaPersonal] OFF
INSERT [pro].[MstEntregables] ([IdEntregable], [IdProyecto], [IdEntregableParent], [Descripcion], [FechaInicio], [FechaFin], [DuracionEstimadaHoras], [DuracionEstimadaDias], [HorasDedicadas], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (61, 9, NULL, N'Cursos de habilidades para la vida de 120 horas', CAST(N'2019-09-02 00:00:00.000' AS DateTime), CAST(N'2019-12-06 00:00:00.000' AS DateTime), CAST(604.00 AS Decimal(9, 2)), CAST(75.50 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1, NULL, 15, CAST(N'2019-07-15 21:29:33.960' AS DateTime), 15, CAST(N'2019-08-30 10:54:04.147' AS DateTime))
INSERT [pro].[MstEntregables] ([IdEntregable], [IdProyecto], [IdEntregableParent], [Descripcion], [FechaInicio], [FechaFin], [DuracionEstimadaHoras], [DuracionEstimadaDias], [HorasDedicadas], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (62, 9, NULL, N'1 curso de apicultura y diversificación de fincas, de 108 horas. ', CAST(N'2019-09-08 00:00:00.000' AS DateTime), CAST(N'2020-01-15 00:00:00.000' AS DateTime), CAST(808.00 AS Decimal(9, 2)), CAST(101.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1, NULL, 15, CAST(N'2019-07-15 21:30:13.913' AS DateTime), 15, CAST(N'2019-08-30 10:56:06.317' AS DateTime))
INSERT [pro].[MstEntregables] ([IdEntregable], [IdProyecto], [IdEntregableParent], [Descripcion], [FechaInicio], [FechaFin], [DuracionEstimadaHoras], [DuracionEstimadaDias], [HorasDedicadas], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (63, 9, NULL, N'1 curso de procesamiento artesanal de frutas, vegetales y dulces, de 112 horas.', CAST(N'2019-10-10 00:00:00.000' AS DateTime), CAST(N'2020-04-30 00:00:00.000' AS DateTime), CAST(1276.00 AS Decimal(9, 2)), CAST(159.50 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1, NULL, 15, CAST(N'2019-07-15 21:30:40.683' AS DateTime), 15, CAST(N'2019-08-30 10:56:38.777' AS DateTime))
INSERT [pro].[MstEntregables] ([IdEntregable], [IdProyecto], [IdEntregableParent], [Descripcion], [FechaInicio], [FechaFin], [DuracionEstimadaHoras], [DuracionEstimadaDias], [HorasDedicadas], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (64, 9, NULL, N'2 cursos de emprendedurismo y formulación de planes de negocios, de 171 horas.', CAST(N'2019-11-01 00:00:00.000' AS DateTime), CAST(N'2020-06-30 00:00:00.000' AS DateTime), CAST(1516.00 AS Decimal(9, 2)), CAST(189.50 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1, NULL, 15, CAST(N'2019-07-15 21:31:26.677' AS DateTime), 15, CAST(N'2019-08-30 10:57:01.540' AS DateTime))
INSERT [pro].[MstEntregables] ([IdEntregable], [IdProyecto], [IdEntregableParent], [Descripcion], [FechaInicio], [FechaFin], [DuracionEstimadaHoras], [DuracionEstimadaDias], [HorasDedicadas], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (65, 10, NULL, N'Construcción y equipamiento del Centro de Educación Alternativa Rural “Telpochcallí”. El CEAR Telpochcallí será un centro de referencia para la formación técnica en agricultura sostenible. Además, será un centro de experimentación e investigación de nuevas técnicas agropecuarias, en el que podrán realizar formación e intercambios personas de instituciones de otras zonas de Nicaragua.', CAST(N'2019-09-09 00:00:00.000' AS DateTime), CAST(N'2020-02-29 00:00:00.000' AS DateTime), CAST(1096.00 AS Decimal(9, 2)), CAST(137.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1, NULL, 15, CAST(N'2019-07-15 21:34:39.427' AS DateTime), 15, CAST(N'2019-08-30 10:59:04.223' AS DateTime))
INSERT [pro].[MstEntregables] ([IdEntregable], [IdProyecto], [IdEntregableParent], [Descripcion], [FechaInicio], [FechaFin], [DuracionEstimadaHoras], [DuracionEstimadaDias], [HorasDedicadas], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (66, 10, NULL, N'Formación de 70 adolescentes y jóvenes, con un nivel escolar mínimo de 6º grado de primaria, que recibirán 3 años de educación general y técnica en agroecología.', CAST(N'2019-09-30 00:00:00.000' AS DateTime), CAST(N'2020-03-02 00:00:00.000' AS DateTime), CAST(968.00 AS Decimal(9, 2)), CAST(121.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1, NULL, 15, CAST(N'2019-07-15 21:35:08.543' AS DateTime), 15, CAST(N'2019-08-30 10:59:30.797' AS DateTime))
INSERT [pro].[MstEntregables] ([IdEntregable], [IdProyecto], [IdEntregableParent], [Descripcion], [FechaInicio], [FechaFin], [DuracionEstimadaHoras], [DuracionEstimadaDias], [HorasDedicadas], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (67, 10, NULL, N'Realización de 10 Cursos Libres de Oficios en los que participarán 200 personas, sobre temas relacionados con la artesanía, el procesamiento de alimentos y la elaboración de conservas', CAST(N'2019-10-14 00:00:00.000' AS DateTime), CAST(N'2020-02-08 00:00:00.000' AS DateTime), CAST(744.00 AS Decimal(9, 2)), CAST(93.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1, NULL, 15, CAST(N'2019-07-15 21:35:36.757' AS DateTime), 15, CAST(N'2019-08-30 11:10:16.327' AS DateTime))
INSERT [pro].[MstEntregables] ([IdEntregable], [IdProyecto], [IdEntregableParent], [Descripcion], [FechaInicio], [FechaFin], [DuracionEstimadaHoras], [DuracionEstimadaDias], [HorasDedicadas], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (68, 10, NULL, N'Formación en Educación Ambiental a 488 niñas y niños de 7 escuelas de educación primaria de 12 comunidades rurales.', CAST(N'2019-10-07 00:00:00.000' AS DateTime), CAST(N'2020-06-30 00:00:00.000' AS DateTime), CAST(1680.00 AS Decimal(9, 2)), CAST(210.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1, NULL, 15, CAST(N'2019-07-15 21:36:03.833' AS DateTime), NULL, NULL)
INSERT [pro].[MstEntregables] ([IdEntregable], [IdProyecto], [IdEntregableParent], [Descripcion], [FechaInicio], [FechaFin], [DuracionEstimadaHoras], [DuracionEstimadaDias], [HorasDedicadas], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (69, 10, NULL, N'Organización de grupos de jóvenes para la protección del medio ambiente y la promoción de la cultura y los saberes locales, denominados Brigadas Ecológicas Comunitarias.', CAST(N'2019-10-21 00:00:00.000' AS DateTime), CAST(N'2020-10-31 00:00:00.000' AS DateTime), CAST(2372.00 AS Decimal(9, 2)), CAST(296.50 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1, NULL, 15, CAST(N'2019-07-15 21:36:42.100' AS DateTime), NULL, NULL)
INSERT [pro].[MstEntregables] ([IdEntregable], [IdProyecto], [IdEntregableParent], [Descripcion], [FechaInicio], [FechaFin], [DuracionEstimadaHoras], [DuracionEstimadaDias], [HorasDedicadas], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (70, 10, NULL, N'Organización en las escuelas de la zona de 7 Brigadas Ecológicas Escolares, que agruparán voluntariamente a niños y niñas para desarrollar acciones de protección del medio ambiente y de conservación de los recursos naturales', CAST(N'2019-11-04 00:00:00.000' AS DateTime), CAST(N'2020-12-25 00:00:00.000' AS DateTime), CAST(2628.00 AS Decimal(9, 2)), CAST(328.50 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1, NULL, 15, CAST(N'2019-07-15 21:37:23.387' AS DateTime), NULL, NULL)
INSERT [pro].[MstEntregables] ([IdEntregable], [IdProyecto], [IdEntregableParent], [Descripcion], [FechaInicio], [FechaFin], [DuracionEstimadaHoras], [DuracionEstimadaDias], [HorasDedicadas], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (71, 10, NULL, N'Puesta en marcha de Huertos Escolares en las 7 escuelas de la zona en los que participarán los 488 alumnos y alumnas y los 17 profesores.', CAST(N'2019-08-05 00:00:00.000' AS DateTime), CAST(N'2021-05-15 00:00:00.000' AS DateTime), CAST(4088.00 AS Decimal(9, 2)), CAST(511.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1, NULL, 15, CAST(N'2019-07-15 21:37:55.267' AS DateTime), NULL, NULL)
INSERT [pro].[MstEntregables] ([IdEntregable], [IdProyecto], [IdEntregableParent], [Descripcion], [FechaInicio], [FechaFin], [DuracionEstimadaHoras], [DuracionEstimadaDias], [HorasDedicadas], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (72, 10, NULL, N'Constitución de una Asociación por los estudiantes de agroecología que tendrá por objetivo fortalecer la capacidad de sus miembros para desarrollar microempresas agropecuarias.', CAST(N'2020-06-01 00:00:00.000' AS DateTime), CAST(N'2021-04-30 00:00:00.000' AS DateTime), CAST(2100.00 AS Decimal(9, 2)), CAST(262.50 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1, NULL, 15, CAST(N'2019-07-15 21:38:25.607' AS DateTime), NULL, NULL)
INSERT [pro].[MstEntregables] ([IdEntregable], [IdProyecto], [IdEntregableParent], [Descripcion], [FechaInicio], [FechaFin], [DuracionEstimadaHoras], [DuracionEstimadaDias], [HorasDedicadas], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (73, 11, NULL, N'45 jóvenes cursarán los programas de formación técnica y general de la carrera de Técnico Básico en Agroecología en el Centro de Educación Alternativa Rural Telpochcallí.', CAST(N'2019-08-05 00:00:00.000' AS DateTime), CAST(N'2020-02-29 00:00:00.000' AS DateTime), CAST(1316.00 AS Decimal(9, 2)), CAST(164.50 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1, NULL, 15, CAST(N'2019-07-15 21:39:45.160' AS DateTime), 15, CAST(N'2019-07-15 21:44:30.523' AS DateTime))
INSERT [pro].[MstEntregables] ([IdEntregable], [IdProyecto], [IdEntregableParent], [Descripcion], [FechaInicio], [FechaFin], [DuracionEstimadaHoras], [DuracionEstimadaDias], [HorasDedicadas], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (74, 11, NULL, N'Desarrollo tres Escuelas de Campo con 45 jóvenes estudiantes del centro.', CAST(N'2019-08-19 00:00:00.000' AS DateTime), CAST(N'2020-03-07 00:00:00.000' AS DateTime), CAST(1272.00 AS Decimal(9, 2)), CAST(159.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1, NULL, 15, CAST(N'2019-07-15 21:40:18.497' AS DateTime), NULL, NULL)
INSERT [pro].[MstEntregables] ([IdEntregable], [IdProyecto], [IdEntregableParent], [Descripcion], [FechaInicio], [FechaFin], [DuracionEstimadaHoras], [DuracionEstimadaDias], [HorasDedicadas], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (75, 11, NULL, N'Se realizarán 3 intercambios de experiencias, durante los cuales los participantes tendrán la posibilidad de visitar a productores exitosos que utilicen tecnologías novedosas y adecuadas de procesamiento de productos', CAST(N'2019-08-19 00:00:00.000' AS DateTime), CAST(N'2021-05-15 00:00:00.000' AS DateTime), CAST(4000.00 AS Decimal(9, 2)), CAST(500.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1, NULL, 15, CAST(N'2019-07-15 21:40:56.343' AS DateTime), NULL, NULL)
INSERT [pro].[MstEntregables] ([IdEntregable], [IdProyecto], [IdEntregableParent], [Descripcion], [FechaInicio], [FechaFin], [DuracionEstimadaHoras], [DuracionEstimadaDias], [HorasDedicadas], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (76, 11, NULL, N'Elaboración de 45 planes de finca, mediante diagnósticos de cada unidad de producción familiar de los estudiantes, a quienes también se bridará asistencia técnica en sus parcelas familiares.', CAST(N'2019-09-16 00:00:00.000' AS DateTime), CAST(N'2020-09-26 00:00:00.000' AS DateTime), CAST(2372.00 AS Decimal(9, 2)), CAST(296.50 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1, NULL, 15, CAST(N'2019-07-15 21:41:30.533' AS DateTime), NULL, NULL)
INSERT [pro].[MstEntregables] ([IdEntregable], [IdProyecto], [IdEntregableParent], [Descripcion], [FechaInicio], [FechaFin], [DuracionEstimadaHoras], [DuracionEstimadaDias], [HorasDedicadas], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (77, 11, NULL, N'Capacitación a los docentes de las escuelas de educación primaria de la zona en cuidado del medio ambiente y agricultura ecológica.', CAST(N'2019-09-09 00:00:00.000' AS DateTime), CAST(N'2020-02-29 00:00:00.000' AS DateTime), CAST(1096.00 AS Decimal(9, 2)), CAST(137.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1, NULL, 15, CAST(N'2019-07-15 21:42:14.597' AS DateTime), NULL, NULL)
INSERT [pro].[MstEntregables] ([IdEntregable], [IdProyecto], [IdEntregableParent], [Descripcion], [FechaInicio], [FechaFin], [DuracionEstimadaHoras], [DuracionEstimadaDias], [HorasDedicadas], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (78, 11, NULL, N'Se pondrán en marcha huertos escolares en 7 escuelas primarias de la zona, a las que se dotará de los materiales didácticos e insumos agrícolas necesarios para la enseñanza teórica y práctica sobre prácticas adecuadas de conservación del medio ambiente, agricultura ecológica y seguridad alimentaria.', CAST(N'2019-08-05 00:00:00.000' AS DateTime), CAST(N'2021-05-08 00:00:00.000' AS DateTime), CAST(4044.00 AS Decimal(9, 2)), CAST(505.50 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1, NULL, 15, CAST(N'2019-07-15 21:42:45.293' AS DateTime), NULL, NULL)
INSERT [pro].[MstEntregables] ([IdEntregable], [IdProyecto], [IdEntregableParent], [Descripcion], [FechaInicio], [FechaFin], [DuracionEstimadaHoras], [DuracionEstimadaDias], [HorasDedicadas], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd]) VALUES (79, 11, NULL, N'Procesamiento y Comercialización de los Productos de las Fincas. Se promoverá la comercialización de los excedentes de los productos que los estudiantes obtengan en sus parcelas familiares y de otros que puedan adquirir en fincas de su comunidad para su procesamiento y/o comercialización en el mercado local.', CAST(N'2020-12-07 00:00:00.000' AS DateTime), CAST(N'2021-05-22 00:00:00.000' AS DateTime), CAST(1052.00 AS Decimal(9, 2)), CAST(131.50 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 1, NULL, 15, CAST(N'2019-07-15 21:43:07.850' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [pro].[MstObjetivos] ON 

INSERT [pro].[MstObjetivos] ([IdObjetivo], [TipoObjetivo], [Descripcion], [Activo]) VALUES (3, N'G', N'Promover el emprendimiento de jovenes del departamento de Rivas, entre las edades de 16 a  25 años', 0)
INSERT [pro].[MstObjetivos] ([IdObjetivo], [TipoObjetivo], [Descripcion], [Activo]) VALUES (4, N'E', N'Desarrollar actividades que permitan crecer el espiritu de emprendimiento', 0)
INSERT [pro].[MstObjetivos] ([IdObjetivo], [TipoObjetivo], [Descripcion], [Activo]) VALUES (5, N'G', N'Mejorar la formación agrícola de los adolescentes del departamento de Madriz', 0)
INSERT [pro].[MstObjetivos] ([IdObjetivo], [TipoObjetivo], [Descripcion], [Activo]) VALUES (6, N'G', N'Fortalecer los derechos socioeconómicos de  70 mujeres jóvenes rurales, en situación de pobreza y exclusión social, de los municipios de Las Sabanas y San Lucas', 0)
INSERT [pro].[MstObjetivos] ([IdObjetivo], [TipoObjetivo], [Descripcion], [Activo]) VALUES (7, N'E', N'Poner en marcha 15 iniciativas de negocios, impulsadas por mujeres, que incorporan el enfoque medioambiental y la aplicación de técnicas de adaptación al cambio climático.', 0)
INSERT [pro].[MstObjetivos] ([IdObjetivo], [TipoObjetivo], [Descripcion], [Activo]) VALUES (8, N'G', N'Contribuir a la reducción de la pobreza en 12 comunidades campesinas del municipio de Totogalpa.', 0)
INSERT [pro].[MstObjetivos] ([IdObjetivo], [TipoObjetivo], [Descripcion], [Activo]) VALUES (9, N'E', N'Fortalecer la capacidad productiva agropecuaria y artesanal mediante la puesta en marcha de un modelo alternativo de educación técnica para jóvenes', 0)
INSERT [pro].[MstObjetivos] ([IdObjetivo], [TipoObjetivo], [Descripcion], [Activo]) VALUES (10, N'G', N'Contribuir a la reducción de la pobreza y al desarrollo integral, sostenible y equitativo de la población de 21 comunidades de Totogalpa y municipios aledaños, Nicaragua.', 0)
INSERT [pro].[MstObjetivos] ([IdObjetivo], [TipoObjetivo], [Descripcion], [Activo]) VALUES (11, N'G', N'Fortalecer los derechos socioeconómicos de  70 mujeres jóvenes rurales, en situación de pobreza y exclusión social, de los municipios de Las Sabanas y San Lucas', 0)
INSERT [pro].[MstObjetivos] ([IdObjetivo], [TipoObjetivo], [Descripcion], [Activo]) VALUES (12, N'E', N'Poner en marcha 15 iniciativas de negocios, impulsadas por mujeres, que incorporan el enfoque medioambiental y la aplicación de técnicas de adaptación al cambio climático.', 0)
INSERT [pro].[MstObjetivos] ([IdObjetivo], [TipoObjetivo], [Descripcion], [Activo]) VALUES (13, N'G', N'Contribuir a la reducción de la pobreza en 12 comunidades campesinas del municipio de Totogalpa.', 0)
INSERT [pro].[MstObjetivos] ([IdObjetivo], [TipoObjetivo], [Descripcion], [Activo]) VALUES (14, N'G', N'Contribuir a la reducción de la pobreza y al desarrollo integral, sostenible y equitativo de la población de 21 comunidades de Totogalpa y municipios aledaños, Nicaragua.', 0)
INSERT [pro].[MstObjetivos] ([IdObjetivo], [TipoObjetivo], [Descripcion], [Activo]) VALUES (15, N'E', N'Fortalecer la capacidad productiva agropecuaria y artesanal mediante la puesta en marcha de un modelo alternativo de educación técnica para jóvenes', 0)
SET IDENTITY_INSERT [pro].[MstObjetivos] OFF
INSERT [pro].[MstProyectos] ([IdProyecto], [TituloProyecto], [DescProyecto], [Justificacion], [FechaInicio], [FechaFinEstimada], [FechaLimite], [HorasDedicadas], [DuracionEstimadaHoras], [DuracionEstimadaDias], [Externo], [Presupuesto], [PresupuestoActual], [GastoEstimado], [IdTipoProyecto], [IdEstado], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [DiasLabSem], [IdMoneda]) VALUES (9, N'Educación Alternativa Rural para fortalecer los derechos socioeconómicos de mujeres jóvenes, en situación de pobreza y exclusión social, de los municipios de Las Sabanas y San Lucas, Departamento de Madriz, Nicaragua', N'La población beneficiaria directa del proyecto son 70 son mujeres jóvenes, con edades entre 15 y 35 años, de cuatro comunidades rurales de los municipios de Las Sabanas y San Lucas, que tengan aprobados el sexto grado de educación primaria y que demuestren interés y responsabilidad en participar en todas las actividades promovidas por el proyecto.', N'En la concepción del proyecto se ha tenido muy presente la necesidad de impulsar la formación técnica de las mujeres en actividades no tradicionales, como la apicultura, como un medio para fomentar la participación igualitaria de las mujeres en la sociedad, así como sensibilizar y educar a hombres y mujeres sobre la importancia de la intervención de la mujer en las actividades económicas y generadoras de ingresos y en la toma de decisiones a nivel familiar y comunitario.', CAST(N'2019-09-02 00:00:00.000' AS DateTime), CAST(N'2020-12-19 00:00:00.000' AS DateTime), CAST(N'2020-12-19 00:00:00.000' AS DateTime), 0, CAST(2988.00 AS Decimal(9, 2)), CAST(373.50 AS Decimal(9, 2)), 0, CAST(85470.7500 AS Decimal(18, 4)), CAST(85470.7500 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 7, 24, 15, CAST(N'2019-07-15 21:21:54.377' AS DateTime), 15, CAST(N'2019-10-09 19:11:07.203' AS DateTime), 15, 1)
INSERT [pro].[MstProyectos] ([IdProyecto], [TituloProyecto], [DescProyecto], [Justificacion], [FechaInicio], [FechaFinEstimada], [FechaLimite], [HorasDedicadas], [DuracionEstimadaHoras], [DuracionEstimadaDias], [Externo], [Presupuesto], [PresupuestoActual], [GastoEstimado], [IdTipoProyecto], [IdEstado], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [DiasLabSem], [IdMoneda]) VALUES (10, N'Educación Alternativa Rural como estrategia para la reducción de la pobreza en 12 comunidades del municipio de Totogalpa', N'Totogalpa es una de las zonas más pobres del departamento de Madriz, tiene una población de unos 2.000 habitantes (83% rural). Sus primeros pobladores fueron tribus Chorotegas que se dedicaban a la explotación de sus yacimientos minerales. Ahora esta región está dedicada básicamente a la agricultura, con el cultivo de granos básicos; frijol, sorgo y maiz, y en menor escala hortalizas. El 85% de la ganadería es ganado vacuno para carne y leche. También se desarrolla la artesanía.', N'En la concepción del proyecto se ha tenido muy presente la necesidad de impulsar la incorporación de las mujeres a las actividades de formación, económicas y generadoras de ingresos, de manera que sea un medio que permitirá su acceso a la toma de decisiones en el seno familiar y, por tanto, en la Comunidad. Si bien el proyecto beneficiará a las familias en general, se hará especial énfasis en facilitar la inclusión de las mujeres en el proceso de formación (el 46% del grupo de estudiantes son mujeres) con el objetivo de fomentar la participación igualitaria y de sensibilizar y educar a hombres y mujeres sobre la importancia de la intervención de la mujer en las actividades económicas y generadoras de ingresos y en la toma de decisiones a nivel familiar y comunitario.', CAST(N'2019-09-09 00:00:00.000' AS DateTime), CAST(N'2021-05-28 00:00:00.000' AS DateTime), CAST(N'2021-05-28 00:00:00.000' AS DateTime), 0, CAST(3948.00 AS Decimal(9, 2)), CAST(493.50 AS Decimal(9, 2)), 0, CAST(461042.4500 AS Decimal(18, 4)), CAST(461042.4500 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 7, 26, 3, CAST(N'2019-07-15 21:24:07.383' AS DateTime), 15, CAST(N'2019-08-27 20:45:23.523' AS DateTime), 15, 1)
INSERT [pro].[MstProyectos] ([IdProyecto], [TituloProyecto], [DescProyecto], [Justificacion], [FechaInicio], [FechaFinEstimada], [FechaLimite], [HorasDedicadas], [DuracionEstimadaHoras], [DuracionEstimadaDias], [Externo], [Presupuesto], [PresupuestoActual], [GastoEstimado], [IdTipoProyecto], [IdEstado], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [DiasLabSem], [IdMoneda]) VALUES (11, N'Educación Alternativa Rural como Estrategia para la reducción de la Pobreza en Comunidades Rurales de Totogalpa, Nicaragua', N'El Proyecto se desarrollará en 21 comunidades del Municipio de Totogalpa, perteneciente al departamento de Madriz, Nicaragua, y otros municipio aledaños.', N'--', CAST(N'2019-09-16 00:00:00.000' AS DateTime), CAST(N'2021-05-29 00:00:00.000' AS DateTime), CAST(N'2021-05-29 00:00:00.000' AS DateTime), 0, CAST(3912.00 AS Decimal(9, 2)), CAST(489.00 AS Decimal(9, 2)), 0, CAST(137974.0000 AS Decimal(18, 4)), CAST(137974.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), 7, 24, 15, CAST(N'2019-07-15 21:28:04.760' AS DateTime), 15, CAST(N'2019-08-27 20:46:00.900' AS DateTime), 15, 1)
SET IDENTITY_INSERT [pro].[MstTareas] ON 

INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1602, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-09-02 00:00:00.000' AS DateTime), CAST(N'2019-09-02 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:32.617' AS DateTime), NULL, CAST(N'2019-10-09 22:03:10.043' AS DateTime), NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1603, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-09-03 00:00:00.000' AS DateTime), CAST(N'2019-09-03 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:33.307' AS DateTime), NULL, CAST(N'2019-10-09 22:03:14.827' AS DateTime), NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1604, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-09-04 00:00:00.000' AS DateTime), CAST(N'2019-09-04 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:33.337' AS DateTime), NULL, CAST(N'2019-10-09 22:03:20.390' AS DateTime), NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1605, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-09-05 00:00:00.000' AS DateTime), CAST(N'2019-09-05 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:33.367' AS DateTime), NULL, CAST(N'2019-10-09 22:03:25.607' AS DateTime), NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1606, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-09-06 00:00:00.000' AS DateTime), CAST(N'2019-09-06 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:33.400' AS DateTime), NULL, NULL, NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1607, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-09-07 00:00:00.000' AS DateTime), CAST(N'2019-09-07 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:33.430' AS DateTime), NULL, CAST(N'2019-10-09 22:03:32.143' AS DateTime), NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1608, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-09-09 00:00:00.000' AS DateTime), CAST(N'2019-09-09 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:33.463' AS DateTime), NULL, NULL, NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1609, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-09-10 00:00:00.000' AS DateTime), CAST(N'2019-09-10 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:33.493' AS DateTime), NULL, CAST(N'2019-10-09 22:03:39.453' AS DateTime), NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1610, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-09-11 00:00:00.000' AS DateTime), CAST(N'2019-09-11 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:33.523' AS DateTime), NULL, NULL, NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1611, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-09-12 00:00:00.000' AS DateTime), CAST(N'2019-09-12 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:33.557' AS DateTime), NULL, NULL, NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1612, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-09-13 00:00:00.000' AS DateTime), CAST(N'2019-09-13 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:33.587' AS DateTime), NULL, NULL, NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1613, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-09-14 00:00:00.000' AS DateTime), CAST(N'2019-09-14 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:33.633' AS DateTime), NULL, CAST(N'2019-10-09 22:03:46.397' AS DateTime), NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1614, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-09-16 00:00:00.000' AS DateTime), CAST(N'2019-09-16 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:33.667' AS DateTime), NULL, NULL, NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1615, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-09-17 00:00:00.000' AS DateTime), CAST(N'2019-09-17 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:33.697' AS DateTime), NULL, NULL, NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1616, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-09-18 00:00:00.000' AS DateTime), CAST(N'2019-09-18 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:33.727' AS DateTime), NULL, NULL, NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1617, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-09-19 00:00:00.000' AS DateTime), CAST(N'2019-09-19 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:33.760' AS DateTime), NULL, CAST(N'2019-10-09 22:03:56.940' AS DateTime), NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1618, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-09-20 00:00:00.000' AS DateTime), CAST(N'2019-09-20 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:33.790' AS DateTime), NULL, NULL, NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1619, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-09-21 00:00:00.000' AS DateTime), CAST(N'2019-09-21 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:33.883' AS DateTime), NULL, NULL, NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1620, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-09-23 00:00:00.000' AS DateTime), CAST(N'2019-09-23 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:33.917' AS DateTime), NULL, NULL, NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1621, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-09-24 00:00:00.000' AS DateTime), CAST(N'2019-09-24 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:33.963' AS DateTime), NULL, NULL, NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1622, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-09-25 00:00:00.000' AS DateTime), CAST(N'2019-09-25 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:34.007' AS DateTime), NULL, NULL, NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1623, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-09-26 00:00:00.000' AS DateTime), CAST(N'2019-09-26 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:34.040' AS DateTime), NULL, NULL, NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1624, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-09-27 00:00:00.000' AS DateTime), CAST(N'2019-09-27 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:34.090' AS DateTime), NULL, NULL, NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1625, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-09-28 00:00:00.000' AS DateTime), CAST(N'2019-09-28 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:34.117' AS DateTime), NULL, NULL, NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1626, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-09-30 00:00:00.000' AS DateTime), CAST(N'2019-09-30 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:34.163' AS DateTime), NULL, NULL, NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1627, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-10-01 00:00:00.000' AS DateTime), CAST(N'2019-10-01 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:34.197' AS DateTime), NULL, NULL, NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1628, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-10-02 00:00:00.000' AS DateTime), CAST(N'2019-10-02 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:34.227' AS DateTime), NULL, NULL, NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1629, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-10-03 00:00:00.000' AS DateTime), CAST(N'2019-10-03 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:34.273' AS DateTime), NULL, CAST(N'2019-10-09 22:04:06.040' AS DateTime), NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1630, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-10-04 00:00:00.000' AS DateTime), CAST(N'2019-10-04 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:34.307' AS DateTime), NULL, NULL, NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1631, 61, 9, NULL, NULL, N'Reunión con las autoridades de la comunidad', CAST(N'2019-10-05 00:00:00.000' AS DateTime), CAST(N'2019-10-05 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, N'Se harán reunión periódicas para definar el modo de trabajo', 1, CAST(N'2019-10-09 22:02:34.337' AS DateTime), NULL, CAST(N'2019-10-09 22:04:14.290' AS DateTime), NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1632, 65, 10, NULL, NULL, N'Dar a conocer las tecnicas de agricultura ', CAST(N'2019-10-13 00:00:00.000' AS DateTime), CAST(N'2019-10-14 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, NULL, 15, CAST(N'2019-10-12 19:18:42.287' AS DateTime), NULL, CAST(N'2019-10-12 19:53:20.770' AS DateTime), NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1633, 65, 10, NULL, NULL, N'Implementar nuevas tecnicas de agricultura ', CAST(N'2019-10-12 00:00:00.000' AS DateTime), CAST(N'2019-10-20 00:00:00.000' AS DateTime), CAST(6.00 AS Decimal(9, 2)), CAST(48.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, NULL, 15, CAST(N'2019-10-12 19:21:18.613' AS DateTime), NULL, NULL, NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1634, 65, 10, NULL, NULL, N'Dar a conocer nuevas Herramientas para la agricultura', CAST(N'2019-10-14 00:00:00.000' AS DateTime), CAST(N'2019-10-29 00:00:00.000' AS DateTime), CAST(12.00 AS Decimal(9, 2)), CAST(96.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, NULL, 15, CAST(N'2019-10-12 19:23:23.687' AS DateTime), NULL, CAST(N'2019-10-12 19:52:13.213' AS DateTime), NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1635, 66, 10, NULL, NULL, N'Capacitar a los jovenes ', CAST(N'2019-10-16 00:00:00.000' AS DateTime), CAST(N'2019-10-16 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, NULL, 15, CAST(N'2019-10-12 19:25:24.487' AS DateTime), NULL, CAST(N'2019-10-12 19:58:23.800' AS DateTime), NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1636, 66, 10, NULL, NULL, N'conocer los conceptos Basicos de la metodologia  a los jovenes ', CAST(N'2019-10-18 00:00:00.000' AS DateTime), CAST(N'2019-10-18 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, NULL, 15, CAST(N'2019-10-12 19:26:52.593' AS DateTime), NULL, CAST(N'2019-10-12 19:58:57.157' AS DateTime), NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1637, 72, 10, NULL, NULL, N'implementar conceptos basico a estudiantes ', CAST(N'2019-10-15 00:00:00.000' AS DateTime), CAST(N'2019-10-16 00:00:00.000' AS DateTime), CAST(1.00 AS Decimal(9, 2)), CAST(8.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 30, NULL, 15, CAST(N'2019-10-12 19:30:16.370' AS DateTime), 16, NULL, NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1638, 65, 10, NULL, NULL, N'Dar a conocer los conceptos básicos ', CAST(N'2019-10-18 00:00:00.000' AS DateTime), CAST(N'2019-10-18 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, NULL, 15, CAST(N'2019-10-12 19:48:10.467' AS DateTime), NULL, CAST(N'2019-10-12 19:53:41.107' AS DateTime), NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1639, 65, 10, NULL, NULL, N'Captar nuevos Jovenes para el Programa', CAST(N'2019-10-17 00:00:00.000' AS DateTime), CAST(N'2019-10-17 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, NULL, 15, CAST(N'2019-10-12 19:49:25.737' AS DateTime), NULL, CAST(N'2019-10-12 19:56:38.283' AS DateTime), NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1640, 65, 10, NULL, NULL, N'Jovenes con nuevo Liderazgo', CAST(N'2019-10-18 00:00:00.000' AS DateTime), CAST(N'2019-10-19 00:00:00.000' AS DateTime), CAST(1.00 AS Decimal(9, 2)), CAST(8.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, NULL, 15, CAST(N'2019-10-12 19:51:22.533' AS DateTime), NULL, CAST(N'2019-10-12 19:52:38.323' AS DateTime), NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1641, 65, 10, NULL, NULL, N'Dar a conocer cada uno de los departamentos que implementan la metodologia', CAST(N'2019-10-15 00:00:00.000' AS DateTime), CAST(N'2020-02-29 00:00:00.000' AS DateTime), CAST(108.50 AS Decimal(9, 2)), CAST(868.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, NULL, 15, CAST(N'2019-10-12 19:55:06.680' AS DateTime), NULL, NULL, NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1642, 67, 10, NULL, NULL, N'Captar Nuevos Jovenes ', CAST(N'2019-10-18 00:00:00.000' AS DateTime), CAST(N'2020-02-08 00:00:00.000' AS DateTime), CAST(89.00 AS Decimal(9, 2)), CAST(712.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, NULL, 15, CAST(N'2019-10-12 19:59:57.057' AS DateTime), NULL, NULL, NULL)
INSERT [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto], [IdTareaParent], [IdTareaDependencia], [Nombre], [FechaInicio], [FechaFin], [DuracionEstimadaDias], [DuracionEstimadaHoras], [HorasDedicadas], [Hito], [Provisional], [Revisar], [IdUsuarioRevisor], [ReportarAvances], [FecuenciaAvance], [TareaPeriodica], [IdEstado], [Nota], [IdUsuarioIns], [FechaIns], [IdUsuarioUpd], [FechaUpd], [TimeLine]) VALUES (1643, 67, 10, NULL, NULL, N'Capacitación continua', CAST(N'2019-10-18 00:00:00.000' AS DateTime), CAST(N'2019-10-18 00:00:00.000' AS DateTime), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), CAST(0.00 AS Decimal(9, 2)), 0, 0, 0, NULL, 0, 0, 0, 28, NULL, 15, CAST(N'2019-10-12 20:02:11.973' AS DateTime), NULL, NULL, NULL)
SET IDENTITY_INSERT [pro].[MstTareas] OFF
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (1, 1, N'Index', N'Home', NULL, N'Página de inicio del sistema', 1, 1, CAST(N'2017-05-20 22:04:07.0200000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (2, 1, N'Index', N'Catalog', NULL, N'Catálogos del sistema', 1, 1, CAST(N'2017-05-27 12:46:54.1830000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (3, 1, N'Index', N'DetailCatalog', NULL, N'Detalle catálogos del sistema', 1, 1, CAST(N'2017-05-27 12:48:02.8200000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (4, 1, N'Index', N'Parameter', NULL, N'Parámetros del sistema', 1, 1, CAST(N'2017-05-27 12:49:05.6800000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (5, 1, N'Index', N'Article', NULL, N'Artículos', 1, 1, CAST(N'2017-06-03 11:06:40.5230000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (6, 1, N'Index', N'Entry', NULL, N'Rubros', 1, 1, CAST(N'2017-06-03 11:06:59.2470000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (7, 1, N'Index', N'Currency', NULL, N'Monedas', 1, 1, CAST(N'2017-06-03 11:07:21.6670000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (8, 1, N'Index', N'Holiday', NULL, N'Días feriados', 1, 1, CAST(N'2017-06-03 11:07:41.5170000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (9, 1, N'Index', N'Person', NULL, N'Personal', 1, 1, CAST(N'2017-06-03 11:08:07.2170000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (10, 1, N'Index', N'Program', NULL, N'Programas', 1, 1, CAST(N'2017-06-03 11:08:52.9900000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (11, 1, N'AddCatalog', N'Catalog', NULL, N'Agregar nuevo registro catálogo', 1, 1, CAST(N'2017-06-03 11:10:03.4570000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (12, 1, N'EditCatalog', N'Catalog', NULL, N'Editar registro catálogo', 1, 1, CAST(N'2017-06-03 11:10:24.9300000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (13, 1, N'AddDetailCatalog', N'DetailCatalog', NULL, N'Agregar nuevo registro de detalle catálogo', 1, 1, CAST(N'2017-06-03 11:12:44.9900000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (14, 1, N'EditDetailCatalog', N'DetailCatalog', NULL, N'Editar registro de detalle catálogo', 1, 1, CAST(N'2017-06-03 11:13:10.9000000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (15, 1, N'AddParameter', N'Parameter', NULL, N'Agregar nuevo registro de parámetro', 1, 1, CAST(N'2017-06-03 11:14:48.1900000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (16, 1, N'EditParameter', N'Parameter', NULL, N'Editar registro de parámetro', 1, 1, CAST(N'2017-06-03 11:15:12.1000000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (17, 1, N'AddArticle', N'Article', NULL, N'Agregar nuevo registro de artículo', 1, 1, CAST(N'2017-06-03 11:17:10.1570000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (18, 1, N'AddEntry', N'Entry', NULL, N'Agregar nuevo registro de rubro', 1, 1, CAST(N'2017-06-03 11:17:41.8170000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (19, 1, N'EditEntry', N'Entry', NULL, N'Editar registro de rubro', 1, 1, CAST(N'2017-06-03 11:18:42.3930000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (20, 1, N'AddCurrency', N'Currency', NULL, N'Agregar registro de moneda', 1, 1, CAST(N'2017-06-03 11:45:30.3330000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (21, 1, N'EditCurrency', N'Currency', NULL, N'Editar registro de moneda', 1, 1, CAST(N'2017-06-03 11:45:46.0670000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (22, 1, N'AddHoliday', N'Holiday', NULL, N'Agregar nuevo registro de día feriado', 1, 1, CAST(N'2017-06-03 11:46:43.1530000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (23, 1, N'EditHoliday', N'Holiday', NULL, N'Editar registro de día feriado', 1, 1, CAST(N'2017-06-03 11:46:59.1100000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (24, 1, N'AddPerson', N'Person', NULL, N'Agregar nuevo registro de personal', 1, 1, CAST(N'2017-06-03 11:47:46.4270000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (25, 1, N'AddProgram', N'Program', NULL, N'Agregar nuevo registro de programa', 1, 1, CAST(N'2017-06-03 11:48:24.8200000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (26, 1, N'EditProgram', N'Program', NULL, N'Editar registro de programa', 1, 1, CAST(N'2017-06-03 11:48:46.5430000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (27, 1, N'Index', N'Role', NULL, N'Roles de usuarios', 1, 1, CAST(N'2018-06-28 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (28, 1, N'AddRole', N'Role', NULL, N'Agregar roles de usuarios', 1, 1, CAST(N'2018-06-28 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (29, 1, N'EditRole', N'Role', NULL, N'Editar Rol de usuario', 1, 1, CAST(N'2018-06-28 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (30, 1, N'AddActions', N'Role', NULL, N'Vincular rol con acciones ', 1, 1, CAST(N'2018-06-28 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (31, 1, N'AddOptions', N'Role', NULL, N'Vincular rol con opciones', 1, 1, CAST(N'2018-06-28 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (32, 1, N'Index', N'User', NULL, N'Usuarios del sistema', 1, 1, CAST(N'2018-06-28 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (33, 1, N'AddUser', N'User', NULL, N'Agregar usuario del sistema', 1, 1, CAST(N'2018-06-28 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (34, 1, N'EditUser', N'User', NULL, N'Editar usuario de sistema', 1, 1, CAST(N'2018-06-28 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (35, 1, N'Index', N'Project', NULL, N'Proyectos del centro', 1, 1, CAST(N'2018-06-28 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (36, 1, N'AddProject', N'Project', NULL, N'Agregar proyectos', 1, 1, CAST(N'2018-06-28 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (37, 1, N'EditProject', N'Project', NULL, N'Editar proyecto', 1, 1, CAST(N'2018-06-28 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (38, 1, N'Index', N'Logger', NULL, N'Logs de errores', 1, 1, CAST(N'2019-07-01 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (39, 1, N'Planning', N'Project', NULL, N'Planificar proyectos', 1, 1, CAST(N'2018-07-08 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (40, 1, N'ApprovePlanning', N'Project', NULL, N'Aprobar planificación de proyectos', 1, 1, CAST(N'2018-07-08 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (41, 1, N'EditPerson', N'Person', NULL, N'Editar información de persona', 1, 1, CAST(N'2018-07-10 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[Actions] ([ActionId], [SystemId], [Action], [Controller], [Url], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (42, 1, N'EditArticle', N'Article', NULL, N'Editar información de articulo', 1, 1, CAST(N'2018-10-25 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[ActionsOptions] ([ActionOption], [OptionId], [ActionId]) VALUES (1, 18, 10)
INSERT [Security].[ActionsOptions] ([ActionOption], [OptionId], [ActionId]) VALUES (2, 19, 10)
INSERT [Security].[ActionsOptions] ([ActionOption], [OptionId], [ActionId]) VALUES (3, 18, 2)
INSERT [Security].[ActionsOptions] ([ActionOption], [OptionId], [ActionId]) VALUES (4, 19, 2)
INSERT [Security].[ActionsOptions] ([ActionOption], [OptionId], [ActionId]) VALUES (5, 18, 3)
INSERT [Security].[ActionsOptions] ([ActionOption], [OptionId], [ActionId]) VALUES (6, 19, 3)
INSERT [Security].[ActionsOptions] ([ActionOption], [OptionId], [ActionId]) VALUES (7, 18, 4)
INSERT [Security].[ActionsOptions] ([ActionOption], [OptionId], [ActionId]) VALUES (8, 19, 4)
INSERT [Security].[ActionsOptions] ([ActionOption], [OptionId], [ActionId]) VALUES (9, 18, 5)
INSERT [Security].[ActionsOptions] ([ActionOption], [OptionId], [ActionId]) VALUES (10, 18, 6)
INSERT [Security].[ActionsOptions] ([ActionOption], [OptionId], [ActionId]) VALUES (11, 19, 6)
INSERT [Security].[ActionsOptions] ([ActionOption], [OptionId], [ActionId]) VALUES (12, 18, 7)
INSERT [Security].[ActionsOptions] ([ActionOption], [OptionId], [ActionId]) VALUES (13, 19, 7)
INSERT [Security].[ActionsOptions] ([ActionOption], [OptionId], [ActionId]) VALUES (14, 18, 8)
INSERT [Security].[ActionsOptions] ([ActionOption], [OptionId], [ActionId]) VALUES (15, 19, 8)
INSERT [Security].[ActionsOptions] ([ActionOption], [OptionId], [ActionId]) VALUES (16, 18, 9)
INSERT [Security].[ActionsOptions] ([ActionOption], [OptionId], [ActionId]) VALUES (17, 19, 27)
INSERT [Security].[ActionsOptions] ([ActionOption], [OptionId], [ActionId]) VALUES (18, 18, 27)
INSERT [Security].[ActionsOptions] ([ActionOption], [OptionId], [ActionId]) VALUES (19, 19, 32)
INSERT [Security].[ActionsOptions] ([ActionOption], [OptionId], [ActionId]) VALUES (20, 18, 32)
INSERT [Security].[ActionsOptions] ([ActionOption], [OptionId], [ActionId]) VALUES (21, 18, 35)
INSERT [Security].[ActionsOptions] ([ActionOption], [OptionId], [ActionId]) VALUES (22, 19, 35)
INSERT [Security].[ActionsOptions] ([ActionOption], [OptionId], [ActionId]) VALUES (23, 19, 9)
INSERT [Security].[ActionsOptions] ([ActionOption], [OptionId], [ActionId]) VALUES (24, 19, 5)
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (232, 1, CAST(N'2018-11-27 22:20:05.8035271' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (235, 1, CAST(N'2018-11-27 22:43:08.4764469' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (238, 1, CAST(N'2018-12-02 08:34:23.3945497' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (239, 1, CAST(N'2018-12-05 19:09:12.1662015' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (249, 1, CAST(N'2019-01-06 13:40:54.3989905' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (254, 1, CAST(N'2019-01-21 22:06:40.2848651' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (261, 1, CAST(N'2019-02-09 09:35:52.5147310' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (267, 1, CAST(N'2019-04-20 09:56:14.2597185' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (274, 1, CAST(N'2019-06-18 19:55:08.6687891' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (292, 1, CAST(N'2019-07-11 22:07:18.0340643' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (309, 1, CAST(N'2019-10-09 21:57:00.3516665' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (311, 1, CAST(N'2019-10-12 19:35:15.0001111' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (316, 1, CAST(N'2019-10-12 20:05:30.7180597' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (319, 1, CAST(N'2019-10-13 16:49:27.0277247' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (240, 3, CAST(N'2018-12-05 19:24:16.8660927' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (241, 3, CAST(N'2018-12-05 19:24:36.6092445' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (244, 3, CAST(N'2019-01-06 11:10:13.3877764' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (256, 3, CAST(N'2019-02-09 06:58:04.7956371' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (258, 3, CAST(N'2019-02-09 09:24:26.6349276' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (277, 3, CAST(N'2019-06-19 21:38:11.4301218' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (282, 3, CAST(N'2019-06-25 20:15:56.2222403' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (290, 3, CAST(N'2019-07-11 21:40:41.9244735' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (303, 3, CAST(N'2019-07-15 21:22:08.4776266' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (312, 3, CAST(N'2019-10-12 19:35:33.1443597' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (314, 3, CAST(N'2019-10-12 20:03:40.6184669' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (245, 12, CAST(N'2019-01-06 11:20:50.4375414' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (294, 12, CAST(N'2019-07-11 22:08:32.4848720' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (295, 13, CAST(N'2019-07-11 22:08:42.2329717' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (299, 13, CAST(N'2019-07-11 22:32:07.7959577' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (242, 14, CAST(N'2018-12-05 19:30:02.8865180' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (243, 14, CAST(N'2018-12-05 19:32:43.1893209' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (233, 15, CAST(N'2018-11-27 22:41:34.6397414' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (234, 15, CAST(N'2018-11-27 22:41:49.0968789' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (247, 15, CAST(N'2019-01-06 11:21:36.4734864' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (248, 15, CAST(N'2019-01-06 13:34:58.7720117' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (251, 15, CAST(N'2019-01-20 09:48:40.2280915' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (252, 15, CAST(N'2019-01-20 10:09:26.6511870' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (253, 15, CAST(N'2019-01-21 21:25:20.5281182' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (255, 15, CAST(N'2019-02-09 06:56:51.4791130' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (257, 15, CAST(N'2019-02-09 06:58:43.2974566' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (258, 15, CAST(N'2019-02-09 09:24:26.6349276' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (259, 15, CAST(N'2019-02-09 09:27:19.9668955' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (262, 15, CAST(N'2019-02-09 09:37:06.5349699' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (263, 15, CAST(N'2019-02-11 20:38:54.8520710' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (264, 15, CAST(N'2019-02-26 21:14:32.0087851' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (265, 15, CAST(N'2019-03-11 19:19:32.7411694' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (266, 15, CAST(N'2019-04-20 07:59:31.0729685' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (268, 15, CAST(N'2019-04-20 22:05:08.2219584' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (269, 15, CAST(N'2019-04-27 13:55:10.8548647' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (270, 15, CAST(N'2019-05-20 20:42:27.6923385' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (271, 15, CAST(N'2019-06-08 13:25:21.7455275' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (272, 15, CAST(N'2019-06-09 14:16:37.8188817' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (273, 15, CAST(N'2019-06-18 19:21:46.5485462' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (275, 15, CAST(N'2019-06-18 19:56:13.1199899' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (276, 15, CAST(N'2019-06-19 20:27:42.2677870' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (278, 15, CAST(N'2019-06-19 21:38:43.7894107' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (279, 15, CAST(N'2019-06-23 19:30:38.7702273' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (280, 15, CAST(N'2019-06-24 19:55:57.8175756' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (281, 15, CAST(N'2019-06-25 19:33:36.4423206' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (283, 15, CAST(N'2019-06-25 20:53:14.8344007' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (284, 15, CAST(N'2019-07-02 20:38:54.8081684' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (285, 15, CAST(N'2019-07-04 19:25:04.5088163' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (286, 15, CAST(N'2019-07-04 19:55:09.7190043' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (287, 15, CAST(N'2019-07-05 21:46:34.4560068' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (288, 15, CAST(N'2019-07-08 19:32:52.5141314' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (289, 15, CAST(N'2019-07-10 20:05:55.2906154' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (291, 15, CAST(N'2019-07-11 21:40:56.1858849' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (293, 15, CAST(N'2019-07-11 22:08:21.8969049' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (297, 15, CAST(N'2019-07-11 22:11:29.5887592' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (300, 15, CAST(N'2019-07-15 20:56:02.1525005' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (301, 15, CAST(N'2019-07-15 21:00:43.1829520' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (302, 15, CAST(N'2019-07-15 21:18:51.7934353' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (304, 15, CAST(N'2019-08-11 20:54:07.2758466' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (305, 15, CAST(N'2019-08-20 06:42:48.7843709' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (306, 15, CAST(N'2019-08-27 20:15:11.7732618' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (307, 15, CAST(N'2019-08-30 10:41:39.6894181' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (308, 15, CAST(N'2019-10-09 18:58:22.3476649' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (310, 15, CAST(N'2019-10-12 18:42:24.5104638' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (313, 15, CAST(N'2019-10-12 19:37:22.7531790' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (315, 15, CAST(N'2019-10-12 20:04:34.9063111' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (318, 15, CAST(N'2019-10-13 16:30:39.8130881' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (236, 16, CAST(N'2018-11-27 22:45:12.9366760' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (237, 16, CAST(N'2018-11-27 22:45:29.3980559' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (246, 16, CAST(N'2019-01-06 11:21:24.0772745' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (250, 16, CAST(N'2019-01-06 13:42:36.2546154' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (260, 16, CAST(N'2019-02-09 09:29:57.4402671' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (296, 16, CAST(N'2019-07-11 22:08:51.3547595' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (298, 16, CAST(N'2019-07-11 22:29:49.3718551' AS DateTime2), NULL, N'1')
INSERT [Security].[LoginHistory] ([LoginHistoryId], [UserId], [LoginDate], [LogoffDate], [IP]) VALUES (317, 16, CAST(N'2019-10-12 20:07:43.2079348' AS DateTime2), NULL, N'1')
INSERT [Security].[Menu] ([MenuId], [SystemId], [MenuName], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (1, 1, N'Menú lateral', N'Menú de navegación lateral', 1, 1, CAST(N'2017-05-21 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[MenuItems] ([MenuItemId], [MenuId], [ItemParentId], [ActionId], [Label], [Icon], [CssClass], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (1, 1, NULL, 1, N'Inicio', NULL, N'glyphicon glyphicon-home', 1, 1, CAST(N'2017-05-21 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[MenuItems] ([MenuItemId], [MenuId], [ItemParentId], [ActionId], [Label], [Icon], [CssClass], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (2, 1, NULL, NULL, N'Administración', NULL, N'glyphicon glyphicon-cog', 1, 1, CAST(N'2017-05-21 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[MenuItems] ([MenuItemId], [MenuId], [ItemParentId], [ActionId], [Label], [Icon], [CssClass], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (3, 1, 2, 2, N'Catálogos', NULL, NULL, 1, 1, CAST(N'2017-05-21 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[MenuItems] ([MenuItemId], [MenuId], [ItemParentId], [ActionId], [Label], [Icon], [CssClass], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (4, 1, 2, 3, N'Detalles Catálogos', NULL, N'glyphicon glyphicon-align-right', 1, 1, CAST(N'2017-08-21 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[MenuItems] ([MenuItemId], [MenuId], [ItemParentId], [ActionId], [Label], [Icon], [CssClass], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (5, 1, 2, 4, N'Parámetros', NULL, NULL, 1, 1, CAST(N'2017-05-21 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[MenuItems] ([MenuItemId], [MenuId], [ItemParentId], [ActionId], [Label], [Icon], [CssClass], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (6, 1, NULL, NULL, N'Configuración', NULL, N'glyphicon glyphicon-wrench', 1, 1, CAST(N'2017-05-21 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[MenuItems] ([MenuItemId], [MenuId], [ItemParentId], [ActionId], [Label], [Icon], [CssClass], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (7, 1, 6, 8, N'Feriados', NULL, NULL, 1, 1, CAST(N'2017-06-03 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[MenuItems] ([MenuItemId], [MenuId], [ItemParentId], [ActionId], [Label], [Icon], [CssClass], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (8, 1, 6, 10, N'Programas', NULL, NULL, 1, 1, CAST(N'2017-06-03 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[MenuItems] ([MenuItemId], [MenuId], [ItemParentId], [ActionId], [Label], [Icon], [CssClass], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (9, 1, 6, 7, N'Monedas', NULL, NULL, 1, 1, CAST(N'2017-06-03 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[MenuItems] ([MenuItemId], [MenuId], [ItemParentId], [ActionId], [Label], [Icon], [CssClass], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (10, 1, 6, 9, N'Personal', NULL, NULL, 1, 1, CAST(N'2017-06-03 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[MenuItems] ([MenuItemId], [MenuId], [ItemParentId], [ActionId], [Label], [Icon], [CssClass], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (11, 1, NULL, NULL, N'Presupuesto', NULL, NULL, 1, 1, CAST(N'2017-06-03 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[MenuItems] ([MenuItemId], [MenuId], [ItemParentId], [ActionId], [Label], [Icon], [CssClass], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (12, 1, 11, 6, N'Rubros', NULL, NULL, 1, 1, CAST(N'2017-06-03 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[MenuItems] ([MenuItemId], [MenuId], [ItemParentId], [ActionId], [Label], [Icon], [CssClass], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (13, 1, 11, 5, N'Artículos', NULL, NULL, 1, 1, CAST(N'2017-06-03 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[MenuItems] ([MenuItemId], [MenuId], [ItemParentId], [ActionId], [Label], [Icon], [CssClass], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (14, 1, 2, 27, N'Roles', NULL, NULL, 1, 1, CAST(N'2018-06-28 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[MenuItems] ([MenuItemId], [MenuId], [ItemParentId], [ActionId], [Label], [Icon], [CssClass], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (15, 1, 2, 32, N'Usuarios', NULL, NULL, 1, 1, CAST(N'2018-06-28 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[MenuItems] ([MenuItemId], [MenuId], [ItemParentId], [ActionId], [Label], [Icon], [CssClass], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (16, 1, NULL, 35, N'Proyectos', NULL, NULL, 1, 1, CAST(N'2018-06-28 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[MenuItems] ([MenuItemId], [MenuId], [ItemParentId], [ActionId], [Label], [Icon], [CssClass], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (17, 1, 2, 38, N'Logs', NULL, NULL, 1, 1, CAST(N'2018-07-01 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[PasswordHistory] ([PasswordHistoryId], [UserId], [OldPassword], [NewPassword], [ChangeDate], [ReasonChange]) VALUES (17, 3, N'C7AD44CBAD762A5DA0A452F9E854FDC1E0E7A52A38015F23F3EAB1D80B931DD472634DFAC71CD34EBC35D16AB7FB8A90C81F975113D6C7538DC69DD8DE9077EC', N'CF9173831110305D2DBAF0A4978C4AF109B50EA0C9FE377D170EF089266669FBC4B36E67B113E92F8C4063666D8A0D9401A004E7FE3255D64F20244548C594E2', CAST(N'2018-12-05 19:23:25.9956542' AS DateTime2), N'Restauración de contraseña')
INSERT [Security].[PasswordHistory] ([PasswordHistoryId], [UserId], [OldPassword], [NewPassword], [ChangeDate], [ReasonChange]) VALUES (18, 3, N'CF9173831110305D2DBAF0A4978C4AF109B50EA0C9FE377D170EF089266669FBC4B36E67B113E92F8C4063666D8A0D9401A004E7FE3255D64F20244548C594E2', N'C7AD44CBAD762A5DA0A452F9E854FDC1E0E7A52A38015F23F3EAB1D80B931DD472634DFAC71CD34EBC35D16AB7FB8A90C81F975113D6C7538DC69DD8DE9077EC', CAST(N'2018-12-05 19:24:27.3843213' AS DateTime2), N'Restauración de contraseña')
INSERT [Security].[PasswordHistory] ([PasswordHistoryId], [UserId], [OldPassword], [NewPassword], [ChangeDate], [ReasonChange]) VALUES (21, 12, N'F978FA303814177F5F00D453D57B5AD5953B93F9DF30F42FA80728311AE8083BDF3C2DFE47986066DB4DDE199082A3714E87CBC6CAC9E5020F0BF34A9388686D', N'57320A9F70829701A0B3BC9FEFEEA4DB2AAFB77EEDBD81245FF08D2A26C10F12525AA00DE2BFC683C44C7D983A8A63B23CF22A7D5FD071C6EEC594BDEA6C28F8', CAST(N'2018-12-05 21:37:55.4476197' AS DateTime2), N'Restauración de contraseña')
INSERT [Security].[PasswordHistory] ([PasswordHistoryId], [UserId], [OldPassword], [NewPassword], [ChangeDate], [ReasonChange]) VALUES (22, 13, N'1AE52B722754F751E1F95B44B04B27222101AB62514AECE05BB2DF6E1406FFFD0E96B2CD9DAEF791F5F16DB94B15D4A2BC3FBD85A058D3DD22B25534B3CAEC98', N'C4DC6013C720109CD079B333E49F5F9848D28725B5B4CF088FA34EB847B15ECF0D14BFEB0BF8BC69998FEAA7E45C7035E79493B5E69687ED2E061F20830E3C33', CAST(N'2018-12-05 21:39:35.1766392' AS DateTime2), N'Restauración de contraseña')
INSERT [Security].[PasswordHistory] ([PasswordHistoryId], [UserId], [OldPassword], [NewPassword], [ChangeDate], [ReasonChange]) VALUES (19, 14, N'92BA925440E5F0E42056EA71F5CB3B3553BF3E049103B1AF9D64D9C0951CD6FA7B27AABE8D92534754EAB8C83F8EC81649D792CC4337422FA6D713BC653E1EF3', N'C7AD44CBAD762A5DA0A452F9E854FDC1E0E7A52A38015F23F3EAB1D80B931DD472634DFAC71CD34EBC35D16AB7FB8A90C81F975113D6C7538DC69DD8DE9077EC', CAST(N'2018-12-05 19:31:06.6080331' AS DateTime2), N'Restauración de contraseña')
INSERT [Security].[PasswordHistory] ([PasswordHistoryId], [UserId], [OldPassword], [NewPassword], [ChangeDate], [ReasonChange]) VALUES (14, 15, N'E79D0CCE026BCE3EBF12D6C158B5B94F3B94DE6FE890AB60F4024BC61807699D843C480B616DCE87A8B9721604D2F1B891A907C97C353432E68F38E185C0CCAD', N'C7AD44CBAD762A5DA0A452F9E854FDC1E0E7A52A38015F23F3EAB1D80B931DD472634DFAC71CD34EBC35D16AB7FB8A90C81F975113D6C7538DC69DD8DE9077EC', CAST(N'2018-11-27 22:41:41.8123977' AS DateTime2), N'Restauración de contraseña')
INSERT [Security].[PasswordHistory] ([PasswordHistoryId], [UserId], [OldPassword], [NewPassword], [ChangeDate], [ReasonChange]) VALUES (20, 15, N'C7AD44CBAD762A5DA0A452F9E854FDC1E0E7A52A38015F23F3EAB1D80B931DD472634DFAC71CD34EBC35D16AB7FB8A90C81F975113D6C7538DC69DD8DE9077EC', N'58A75C942612BF31AF98BC05F73FDD1CA37669F5F59102383D6B80A300E001E56B6C89962298CA38A043B140EE9EB2C53A2294F74B899BE17E03AEB15583B6F6', CAST(N'2018-12-05 21:37:23.1629621' AS DateTime2), N'Restauración de contraseña')
INSERT [Security].[PasswordHistory] ([PasswordHistoryId], [UserId], [OldPassword], [NewPassword], [ChangeDate], [ReasonChange]) VALUES (15, 16, N'C7AD44CBAD762A5DA0A452F9E854FDC1E0E7A52A38015F23F3EAB1D80B931DD472634DFAC71CD34EBC35D16AB7FB8A90C81F975113D6C7538DC69DD8DE9077EC', N'58B5444CF1B6253A4317FE12DAFF411A78BDA0A95279B1D5768EBF5CA60829E78DA944E8A9160A0B6D428CB213E813525A72650DAC67B88879394FF624DA482F', CAST(N'2018-11-27 22:45:21.3481395' AS DateTime2), N'Restauración de contraseña')
INSERT [Security].[PasswordHistory] ([PasswordHistoryId], [UserId], [OldPassword], [NewPassword], [ChangeDate], [ReasonChange]) VALUES (16, 16, N'58B5444CF1B6253A4317FE12DAFF411A78BDA0A95279B1D5768EBF5CA60829E78DA944E8A9160A0B6D428CB213E813525A72650DAC67B88879394FF624DA482F', N'75D3A6BD2C800EB2C6F748056A7985EAD019F188E3EEE57AB5A4485603C5C26ED8A0786DA0DC0A480975A52D72B154764DC074C171C3D5B4C1CC7DC8CDD73104', CAST(N'2018-12-02 08:40:03.1983334' AS DateTime2), N'Restauración de contraseña')
INSERT [Security].[PasswordHistory] ([PasswordHistoryId], [UserId], [OldPassword], [NewPassword], [ChangeDate], [ReasonChange]) VALUES (23, 16, N'75D3A6BD2C800EB2C6F748056A7985EAD019F188E3EEE57AB5A4485603C5C26ED8A0786DA0DC0A480975A52D72B154764DC074C171C3D5B4C1CC7DC8CDD73104', N'F9351FBB8306228F1EEA1028B51D9CB9309D8939C3C4A117E42BD2A75875D9DED04A598DE4B0B233CF702D7CE0AF78ACBA6E7AD1DDC01787AF7BC966E612CCE2', CAST(N'2018-12-05 21:39:54.2174106' AS DateTime2), N'Restauración de contraseña')
INSERT [Security].[PasswordHistory] ([PasswordHistoryId], [UserId], [OldPassword], [NewPassword], [ChangeDate], [ReasonChange]) VALUES (24, 16, N'F9351FBB8306228F1EEA1028B51D9CB9309D8939C3C4A117E42BD2A75875D9DED04A598DE4B0B233CF702D7CE0AF78ACBA6E7AD1DDC01787AF7BC966E612CCE2', N'DB719158C5121735737325C24BC0D174483763EF34943114BA15860C5DD6F8F4A24142A29A57C95DCDA7B86FCC3B4E87A340D6A17D8B5BC9A5018C44D16871E0', CAST(N'2018-12-05 21:42:45.5888016' AS DateTime2), N'Restauración de contraseña')
INSERT [Security].[PasswordHistory] ([PasswordHistoryId], [UserId], [OldPassword], [NewPassword], [ChangeDate], [ReasonChange]) VALUES (25, 16, N'DB719158C5121735737325C24BC0D174483763EF34943114BA15860C5DD6F8F4A24142A29A57C95DCDA7B86FCC3B4E87A340D6A17D8B5BC9A5018C44D16871E0', N'DB48F48CC779A88A8C8AEC66EEEAB23CD0E691FD48BFAF286BF746EC97877A5083A73B2E1F51F075F736E8A88AAF038F599445382C06306D2B2DFD5EF32197B9', CAST(N'2018-12-05 21:42:54.7417737' AS DateTime2), N'Restauración de contraseña')
INSERT [Security].[PasswordHistory] ([PasswordHistoryId], [UserId], [OldPassword], [NewPassword], [ChangeDate], [ReasonChange]) VALUES (26, 16, N'DB48F48CC779A88A8C8AEC66EEEAB23CD0E691FD48BFAF286BF746EC97877A5083A73B2E1F51F075F736E8A88AAF038F599445382C06306D2B2DFD5EF32197B9', N'669F0BC582EAE9A422178704C66F8DD3A5159E1B4F0319B3877F5915832CE7AA41B28C5B421FB5B11CCFF35DF6C1700E9D495E35A0E16531BC1532CE940CE8A5', CAST(N'2018-12-05 21:43:04.0991207' AS DateTime2), N'Restauración de contraseña')
SET IDENTITY_INSERT [Security].[RoleAction] ON 

INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (1, 1, 1, 1)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (2, 1, 1, 2)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (3, 1, 1, 3)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (4, 1, 1, 4)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (5, 1, 1, 5)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (6, 1, 1, 6)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (7, 1, 1, 7)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (8, 1, 1, 8)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (9, 1, 1, 9)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (10, 1, 1, 10)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (11, 1, 1, 11)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (12, 1, 1, 12)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (14, 1, 1, 13)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (15, 1, 1, 14)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (16, 1, 1, 15)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (17, 1, 1, 16)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (38, 1, 1, 17)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (19, 1, 1, 18)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (20, 1, 1, 19)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (21, 1, 1, 20)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (22, 1, 1, 21)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (23, 1, 1, 22)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (24, 1, 1, 23)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (25, 1, 1, 24)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (26, 1, 1, 25)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (27, 1, 1, 26)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (28, 1, 1, 27)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (29, 1, 1, 28)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (30, 1, 1, 29)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (31, 1, 1, 30)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (32, 1, 1, 31)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (33, 1, 1, 32)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (34, 1, 1, 33)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (35, 1, 1, 34)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (36, 1, 1, 35)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (37, 1, 1, 36)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (49, 1, 1, 37)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (40, 1, 1, 38)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (50, 1, 1, 39)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (62, 1, 1, 41)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (73, 1, 1, 42)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (54, 1, 2, 1)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (60, 1, 2, 5)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (61, 1, 2, 6)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (71, 1, 2, 7)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (66, 1, 2, 8)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (55, 1, 2, 9)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (58, 1, 2, 17)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (59, 1, 2, 18)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (70, 1, 2, 19)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (65, 1, 2, 20)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (69, 1, 2, 21)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (63, 1, 2, 22)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (68, 1, 2, 23)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (64, 1, 2, 24)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (74, 1, 2, 35)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (52, 1, 2, 36)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (53, 1, 2, 37)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (56, 1, 2, 39)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (67, 1, 2, 41)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (91, 1, 3, 1)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (80, 1, 3, 5)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (96, 1, 3, 6)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (90, 1, 3, 7)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (81, 1, 3, 8)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (92, 1, 3, 9)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (94, 1, 3, 10)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (97, 1, 3, 17)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (77, 1, 3, 18)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (88, 1, 3, 19)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (79, 1, 3, 20)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (86, 1, 3, 21)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (47, 1, 3, 22)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (85, 1, 3, 23)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (75, 1, 3, 24)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (76, 1, 3, 25)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (87, 1, 3, 26)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (89, 1, 3, 34)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (95, 1, 3, 35)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (78, 1, 3, 36)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (84, 1, 3, 37)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (93, 1, 3, 39)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (51, 1, 3, 40)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (83, 1, 3, 41)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (82, 1, 3, 42)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (98, 1, 4, 1)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (48, 1, 5, 11)
INSERT [Security].[RoleAction] ([RoleActionID], [SystemId], [RoleId], [ActionId]) VALUES (41, 1, 5, 38)
SET IDENTITY_INSERT [Security].[RoleAction] OFF
INSERT [Security].[RoleMenu] ([SystemId], [RoleId], [MenuId]) VALUES (1, 1, 1)
INSERT [Security].[RoleMenu] ([SystemId], [RoleId], [MenuId]) VALUES (1, 2, 1)
INSERT [Security].[RoleMenu] ([SystemId], [RoleId], [MenuId]) VALUES (1, 3, 1)
INSERT [Security].[RoleMenu] ([SystemId], [RoleId], [MenuId]) VALUES (1, 4, 1)
INSERT [Security].[RoleMenu] ([SystemId], [RoleId], [MenuId]) VALUES (1, 5, 1)
SET IDENTITY_INSERT [Security].[RoleMenuItem] ON 

INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (1, 1, 1, 1, 1)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (2, 1, 1, 1, 2)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (3, 1, 1, 1, 3)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (4, 1, 1, 1, 4)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (6, 1, 1, 1, 5)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (5, 1, 1, 1, 6)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (7, 1, 1, 1, 7)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (9, 1, 1, 1, 8)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (8, 1, 1, 1, 9)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (10, 1, 1, 1, 10)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (11, 1, 1, 1, 11)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (12, 1, 1, 1, 12)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (17, 1, 1, 1, 13)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (14, 1, 1, 1, 14)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (15, 1, 1, 1, 15)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (16, 1, 1, 1, 16)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (18, 1, 1, 1, 17)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (23, 1, 2, 1, 1)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (22, 1, 2, 1, 6)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (27, 1, 2, 1, 7)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (28, 1, 2, 1, 9)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (29, 1, 2, 1, 10)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (24, 1, 2, 1, 11)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (26, 1, 2, 1, 12)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (21, 1, 2, 1, 13)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (30, 1, 2, 1, 16)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (34, 1, 3, 1, 1)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (20, 1, 3, 1, 6)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (33, 1, 3, 1, 7)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (38, 1, 3, 1, 8)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (35, 1, 3, 1, 9)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (36, 1, 3, 1, 10)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (37, 1, 3, 1, 11)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (40, 1, 3, 1, 12)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (32, 1, 3, 1, 13)
INSERT [Security].[RoleMenuItem] ([RoleMenuItemId], [SystemId], [RoleId], [MenuId], [MenuItemId]) VALUES (39, 1, 3, 1, 16)
SET IDENTITY_INSERT [Security].[RoleMenuItem] OFF
INSERT [Security].[Roles] ([RoleId], [RolePerentId], [RoleName], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (1, NULL, N'SuperAdmin', N'Super administrador del sistema', 1, 1, CAST(N'2017-05-21 00:00:00.0000000' AS DateTime2), NULL, NULL)
INSERT [Security].[Roles] ([RoleId], [RolePerentId], [RoleName], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (2, NULL, N'Coodinador', N'Coordinador de proyectos', 1, 1, CAST(N'2018-06-28 20:21:12.5031623' AS DateTime2), NULL, NULL)
INSERT [Security].[Roles] ([RoleId], [RolePerentId], [RoleName], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (3, NULL, N'Director', N'Director del centro', 1, 1, CAST(N'2018-06-28 20:21:31.2554168' AS DateTime2), NULL, NULL)
INSERT [Security].[Roles] ([RoleId], [RolePerentId], [RoleName], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (4, NULL, N'Colaborador', N'Colaborador de proyecto', 1, 1, CAST(N'2018-06-28 20:22:14.5200678' AS DateTime2), NULL, NULL)
INSERT [Security].[Roles] ([RoleId], [RolePerentId], [RoleName], [Description], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (5, NULL, N'Invitado', N'Rol invitado del sistema', 1, 1, CAST(N'2018-07-05 21:13:59.6936764' AS DateTime2), NULL, NULL)
INSERT [Security].[SystemRoles] ([SystemId], [RoleId]) VALUES (1, 1)
INSERT [Security].[SystemRoles] ([SystemId], [RoleId]) VALUES (1, 2)
INSERT [Security].[SystemRoles] ([SystemId], [RoleId]) VALUES (1, 3)
INSERT [Security].[SystemRoles] ([SystemId], [RoleId]) VALUES (1, 4)
INSERT [Security].[SystemRoles] ([SystemId], [RoleId]) VALUES (1, 5)
INSERT [Security].[Systems] ([SystemId], [SystemName], [Description], [Acronym], [StartActionId], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (1, N'Sistema de Gestión de Proyectos', N'Sistema de gestión de proyectos del centro ecumenico fray antonio de valdivieso', N'SIGESPRO', N'1', 1, 1, CAST(N'2017-05-20 22:00:18.3100000' AS DateTime2), NULL, NULL)
SET IDENTITY_INSERT [Security].[UserRoles] ON 

INSERT [Security].[UserRoles] ([UserRoleId], [SystemId], [RoleId], [UserId], [StartActionId]) VALUES (1, 1, 1, 1, NULL)
INSERT [Security].[UserRoles] ([UserRoleId], [SystemId], [RoleId], [UserId], [StartActionId]) VALUES (3, 1, 3, 3, 1)
INSERT [Security].[UserRoles] ([UserRoleId], [SystemId], [RoleId], [UserId], [StartActionId]) VALUES (10, 1, 1, 10, 1)
INSERT [Security].[UserRoles] ([UserRoleId], [SystemId], [RoleId], [UserId], [StartActionId]) VALUES (12, 1, 2, 12, 1)
INSERT [Security].[UserRoles] ([UserRoleId], [SystemId], [RoleId], [UserId], [StartActionId]) VALUES (13, 1, 4, 13, 1)
INSERT [Security].[UserRoles] ([UserRoleId], [SystemId], [RoleId], [UserId], [StartActionId]) VALUES (14, 1, 2, 14, 1)
INSERT [Security].[UserRoles] ([UserRoleId], [SystemId], [RoleId], [UserId], [StartActionId]) VALUES (15, 1, 2, 15, 1)
INSERT [Security].[UserRoles] ([UserRoleId], [SystemId], [RoleId], [UserId], [StartActionId]) VALUES (16, 1, 4, 16, 1)
SET IDENTITY_INSERT [Security].[UserRoles] OFF
INSERT [Security].[Users] ([UserId], [ReferenceId], [UserName], [FullName], [Password], [Email], [PhoneNumber], [ChangePassword], [PasswordExpiration], [PasswordRepeatPeriod], [AttemptsPermitted], [FailedAttempts], [TypeAccessRecovery], [SecurityQuestion], [AnswerSecurity], [ExpirationSession], [ProfilePhoto], [Locked], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (1, 1, N'ftinoco', N'Fernando Tinoco', N'92F2014A2E39A9E59839D9254C810249CE13C0C0ECE6028A21DA36F08CFF7F944F3A977D5AD04748CC67F8EA493B11A0C39CE411C88A35F0A3ADB23603A38659', N'ftinoco_45@yahoo.com', N'89947284', 0, 0, NULL, 3, 0, 1, NULL, NULL, 30000, NULL, 0, 1, 1, CAST(N'2017-05-21 00:00:00.0000000' AS DateTime2), 1, CAST(N'2018-07-15 20:30:56.9341788' AS DateTime2))
INSERT [Security].[Users] ([UserId], [ReferenceId], [UserName], [FullName], [Password], [Email], [PhoneNumber], [ChangePassword], [PasswordExpiration], [PasswordRepeatPeriod], [AttemptsPermitted], [FailedAttempts], [TypeAccessRecovery], [SecurityQuestion], [AnswerSecurity], [ExpirationSession], [ProfilePhoto], [Locked], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (3, 3, N'rValdés', N'Rafael Valdés', N'C7AD44CBAD762A5DA0A452F9E854FDC1E0E7A52A38015F23F3EAB1D80B931DD472634DFAC71CD34EBC35D16AB7FB8A90C81F975113D6C7538DC69DD8DE9077EC', N'ftinoco_45@yahoo.com', N'88888888', 0, 0, NULL, 0, 0, 0, NULL, NULL, 32767, NULL, 0, 1, 1, CAST(N'2018-11-01 21:04:32.6989605' AS DateTime2), NULL, NULL)
INSERT [Security].[Users] ([UserId], [ReferenceId], [UserName], [FullName], [Password], [Email], [PhoneNumber], [ChangePassword], [PasswordExpiration], [PasswordRepeatPeriod], [AttemptsPermitted], [FailedAttempts], [TypeAccessRecovery], [SecurityQuestion], [AnswerSecurity], [ExpirationSession], [ProfilePhoto], [Locked], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (10, 8, N'kmasis', N'Kiara Fabiola Masis Chavarría', N'92F2014A2E39A9E59839D9254C810249CE13C0C0ECE6028A21DA36F08CFF7F944F3A977D5AD04748CC67F8EA493B11A0C39CE411C88A35F0A3ADB23603A38659', N'kiarafabiomasis@gmail.com', N'22222222', 0, 0, NULL, 0, 0, 0, NULL, NULL, 6000, NULL, 0, 1, 1, CAST(N'2018-11-01 21:11:21.5473991' AS DateTime2), NULL, NULL)
INSERT [Security].[Users] ([UserId], [ReferenceId], [UserName], [FullName], [Password], [Email], [PhoneNumber], [ChangePassword], [PasswordExpiration], [PasswordRepeatPeriod], [AttemptsPermitted], [FailedAttempts], [TypeAccessRecovery], [SecurityQuestion], [AnswerSecurity], [ExpirationSession], [ProfilePhoto], [Locked], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (12, 11, N'lgaleano', N'Laura Galeano', N'C7AD44CBAD762A5DA0A452F9E854FDC1E0E7A52A38015F23F3EAB1D80B931DD472634DFAC71CD34EBC35D16AB7FB8A90C81F975113D6C7538DC69DD8DE9077EC', N'ftinoco_45@yahoo.com', N'88888888', 0, 0, NULL, 0, 0, 0, NULL, NULL, 32767, NULL, 0, 1, 1, CAST(N'2018-11-27 22:38:21.4063827' AS DateTime2), NULL, NULL)
INSERT [Security].[Users] ([UserId], [ReferenceId], [UserName], [FullName], [Password], [Email], [PhoneNumber], [ChangePassword], [PasswordExpiration], [PasswordRepeatPeriod], [AttemptsPermitted], [FailedAttempts], [TypeAccessRecovery], [SecurityQuestion], [AnswerSecurity], [ExpirationSession], [ProfilePhoto], [Locked], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (13, 12, N'mbucardo', N'Mariela Bucardo', N'C7AD44CBAD762A5DA0A452F9E854FDC1E0E7A52A38015F23F3EAB1D80B931DD472634DFAC71CD34EBC35D16AB7FB8A90C81F975113D6C7538DC69DD8DE9077EC', N'ftinoco_45@yahoo.com', N'22222222', 0, 0, NULL, 0, 0, 0, NULL, NULL, 32767, NULL, 0, 1, 1, CAST(N'2018-11-27 22:39:07.9214038' AS DateTime2), NULL, NULL)
INSERT [Security].[Users] ([UserId], [ReferenceId], [UserName], [FullName], [Password], [Email], [PhoneNumber], [ChangePassword], [PasswordExpiration], [PasswordRepeatPeriod], [AttemptsPermitted], [FailedAttempts], [TypeAccessRecovery], [SecurityQuestion], [AnswerSecurity], [ExpirationSession], [ProfilePhoto], [Locked], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (14, 13, N'glopez', N'Germán López', N'C7AD44CBAD762A5DA0A452F9E854FDC1E0E7A52A38015F23F3EAB1D80B931DD472634DFAC71CD34EBC35D16AB7FB8A90C81F975113D6C7538DC69DD8DE9077EC', N'ftinococ.45@gmail.com', N'22222222', 0, 0, NULL, 0, 0, 0, NULL, NULL, 32767, NULL, 0, 1, 1, CAST(N'2018-11-27 22:39:41.7590354' AS DateTime2), NULL, NULL)
INSERT [Security].[Users] ([UserId], [ReferenceId], [UserName], [FullName], [Password], [Email], [PhoneNumber], [ChangePassword], [PasswordExpiration], [PasswordRepeatPeriod], [AttemptsPermitted], [FailedAttempts], [TypeAccessRecovery], [SecurityQuestion], [AnswerSecurity], [ExpirationSession], [ProfilePhoto], [Locked], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (15, 14, N'irivera', N'Isaac Rivera', N'C7AD44CBAD762A5DA0A452F9E854FDC1E0E7A52A38015F23F3EAB1D80B931DD472634DFAC71CD34EBC35D16AB7FB8A90C81F975113D6C7538DC69DD8DE9077EC', N'ftinoco_45@yahoo.com', N'88888888', 0, 0, NULL, 0, 0, 0, NULL, NULL, 32676, NULL, 0, 1, 1, CAST(N'2018-11-27 22:40:07.2898040' AS DateTime2), NULL, NULL)
INSERT [Security].[Users] ([UserId], [ReferenceId], [UserName], [FullName], [Password], [Email], [PhoneNumber], [ChangePassword], [PasswordExpiration], [PasswordRepeatPeriod], [AttemptsPermitted], [FailedAttempts], [TypeAccessRecovery], [SecurityQuestion], [AnswerSecurity], [ExpirationSession], [ProfilePhoto], [Locked], [Active], [UserInsert], [InsertDate], [UserUpdate], [UpdateDate]) VALUES (16, 15, N'rmairena', N'Regis Mairena', N'C7AD44CBAD762A5DA0A452F9E854FDC1E0E7A52A38015F23F3EAB1D80B931DD472634DFAC71CD34EBC35D16AB7FB8A90C81F975113D6C7538DC69DD8DE9077EC', N'ftinoco_45@yahoo.com', N'22222222', 0, 0, NULL, 0, 0, 0, NULL, NULL, 32767, NULL, 0, 1, 1, CAST(N'2018-11-27 22:40:41.1360481' AS DateTime2), NULL, NULL)
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ_NOMBRE_PARAMETRO]    Script Date: 19/10/13 6:57:32 PM ******/
ALTER TABLE [admin].[CatParametros] ADD  CONSTRAINT [UQ_NOMBRE_PARAMETRO] UNIQUE NONCLUSTERED 
(
	[NombreParametro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ_TABLE_NAME]    Script Date: 19/10/13 6:57:32 PM ******/
ALTER TABLE [admin].[MstCatalogo] ADD  CONSTRAINT [UQ_TABLE_NAME] UNIQUE NONCLUSTERED 
(
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UQ_RoleAction]    Script Date: 19/10/13 6:57:32 PM ******/
ALTER TABLE [Security].[RoleAction] ADD  CONSTRAINT [UQ_RoleAction] UNIQUE NONCLUSTERED 
(
	[SystemId] ASC,
	[RoleId] ASC,
	[ActionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UQ_RoleMenu]    Script Date: 19/10/13 6:57:32 PM ******/
ALTER TABLE [Security].[RoleMenu] ADD  CONSTRAINT [UQ_RoleMenu] UNIQUE NONCLUSTERED 
(
	[SystemId] ASC,
	[RoleId] ASC,
	[MenuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UQ_RoleMenuItem]    Script Date: 19/10/13 6:57:32 PM ******/
ALTER TABLE [Security].[RoleMenuItem] ADD  CONSTRAINT [UQ_RoleMenuItem] UNIQUE NONCLUSTERED 
(
	[SystemId] ASC,
	[RoleId] ASC,
	[MenuId] ASC,
	[MenuItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UQ_SystemRole]    Script Date: 19/10/13 6:57:32 PM ******/
ALTER TABLE [Security].[SystemRoles] ADD  CONSTRAINT [UQ_SystemRole] UNIQUE NONCLUSTERED 
(
	[SystemId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UQ_UserRole]    Script Date: 19/10/13 6:57:32 PM ******/
ALTER TABLE [Security].[UserRoles] ADD  CONSTRAINT [UQ_UserRole] UNIQUE NONCLUSTERED 
(
	[SystemId] ASC,
	[RoleId] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [pro].[ArchivosTareas] ADD  CONSTRAINT [DF_ArchivosTareas_Eliminado]  DEFAULT ((0)) FOR [Eliminado]
GO
ALTER TABLE [admin].[DetCatalogo]  WITH CHECK ADD  CONSTRAINT [IdTablaCatalogo] FOREIGN KEY([IdMstCatalogo])
REFERENCES [admin].[MstCatalogo] ([IdCatalogo])
GO
ALTER TABLE [admin].[DetCatalogo] CHECK CONSTRAINT [IdTablaCatalogo]
GO
ALTER TABLE [config].[DetContactoBeneficiario]  WITH CHECK ADD  CONSTRAINT [Beneficiario_Contacto] FOREIGN KEY([IdBeneficiario])
REFERENCES [pro].[MstBeneficiarios] ([IdBeneficiario])
GO
ALTER TABLE [config].[DetContactoBeneficiario] CHECK CONSTRAINT [Beneficiario_Contacto]
GO
ALTER TABLE [config].[DetContactoBeneficiario]  WITH CHECK ADD  CONSTRAINT [Contacto_Beneficiario] FOREIGN KEY([IdPersona])
REFERENCES [config].[MstPersonas] ([IdPersona])
GO
ALTER TABLE [config].[DetContactoBeneficiario] CHECK CONSTRAINT [Contacto_Beneficiario]
GO
ALTER TABLE [config].[DetContactoInstitucion]  WITH CHECK ADD  CONSTRAINT [Contacto_Institucion] FOREIGN KEY([IdPersona])
REFERENCES [config].[MstPersonas] ([IdPersona])
GO
ALTER TABLE [config].[DetContactoInstitucion] CHECK CONSTRAINT [Contacto_Institucion]
GO
ALTER TABLE [config].[DetContactoInstitucion]  WITH CHECK ADD  CONSTRAINT [Institucion_Contacto] FOREIGN KEY([IdInstitucion])
REFERENCES [config].[MstInstituciones] ([IdInstitucion])
GO
ALTER TABLE [config].[DetContactoInstitucion] CHECK CONSTRAINT [Institucion_Contacto]
GO
ALTER TABLE [config].[DetPersonaDireccion]  WITH CHECK ADD  CONSTRAINT [Persona_Direcciones] FOREIGN KEY([IdPersona])
REFERENCES [config].[MstPersonas] ([IdPersona])
GO
ALTER TABLE [config].[DetPersonaDireccion] CHECK CONSTRAINT [Persona_Direcciones]
GO
ALTER TABLE [config].[DetPersonaEmail]  WITH CHECK ADD  CONSTRAINT [Persona_Email] FOREIGN KEY([IdPersona])
REFERENCES [config].[MstPersonas] ([IdPersona])
GO
ALTER TABLE [config].[DetPersonaEmail] CHECK CONSTRAINT [Persona_Email]
GO
ALTER TABLE [config].[DetPersonaTelefono]  WITH CHECK ADD  CONSTRAINT [Persona_Telefonos] FOREIGN KEY([IdPersona])
REFERENCES [config].[MstPersonas] ([IdPersona])
GO
ALTER TABLE [config].[DetPersonaTelefono] CHECK CONSTRAINT [Persona_Telefonos]
GO
ALTER TABLE [pre].[CatArticulos]  WITH CHECK ADD  CONSTRAINT [Moneda_Articulo] FOREIGN KEY([IdMoneda])
REFERENCES [pre].[CatMoneda] ([IdMoneda])
GO
ALTER TABLE [pre].[CatArticulos] CHECK CONSTRAINT [Moneda_Articulo]
GO
ALTER TABLE [pre].[CatArticulos]  WITH CHECK ADD  CONSTRAINT [Rubro_Articulo] FOREIGN KEY([IdRubro])
REFERENCES [pre].[CatRubros] ([IdRubro])
GO
ALTER TABLE [pre].[CatArticulos] CHECK CONSTRAINT [Rubro_Articulo]
GO
ALTER TABLE [pre].[CatTasaCambio]  WITH CHECK ADD  CONSTRAINT [Moneda_TasaCambio] FOREIGN KEY([IdMoneda])
REFERENCES [pre].[CatMoneda] ([IdMoneda])
GO
ALTER TABLE [pre].[CatTasaCambio] CHECK CONSTRAINT [Moneda_TasaCambio]
GO
ALTER TABLE [pre].[MstPresupuesto]  WITH CHECK ADD  CONSTRAINT [Entregable_Presupuesto] FOREIGN KEY([IdEntregable], [IdProyecto])
REFERENCES [pro].[MstEntregables] ([IdEntregable], [IdProyecto])
GO
ALTER TABLE [pre].[MstPresupuesto] CHECK CONSTRAINT [Entregable_Presupuesto]
GO
ALTER TABLE [pre].[MstPresupuesto]  WITH CHECK ADD  CONSTRAINT [Moneda_Presupuesto] FOREIGN KEY([IdMoneda])
REFERENCES [pre].[CatMoneda] ([IdMoneda])
GO
ALTER TABLE [pre].[MstPresupuesto] CHECK CONSTRAINT [Moneda_Presupuesto]
GO
ALTER TABLE [pre].[MstPresupuestoFinanciero]  WITH CHECK ADD  CONSTRAINT [Presupuesto_Financiero] FOREIGN KEY([IdPresupuesto])
REFERENCES [pre].[MstPresupuesto] ([IdPresupuesto])
GO
ALTER TABLE [pre].[MstPresupuestoFinanciero] CHECK CONSTRAINT [Presupuesto_Financiero]
GO
ALTER TABLE [pre].[MstPresupuestoFinanciero]  WITH CHECK ADD  CONSTRAINT [Rubro_PresupuestoFinanciero] FOREIGN KEY([IdRubro])
REFERENCES [pre].[CatRubros] ([IdRubro])
GO
ALTER TABLE [pre].[MstPresupuestoFinanciero] CHECK CONSTRAINT [Rubro_PresupuestoFinanciero]
GO
ALTER TABLE [pre].[MstPresupuestoFinanciero]  WITH CHECK ADD  CONSTRAINT [Tarea_Presupuesto_Financiero] FOREIGN KEY([IdTarea], [IdEntregable], [IdProyecto])
REFERENCES [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto])
ON DELETE CASCADE
GO
ALTER TABLE [pre].[MstPresupuestoFinanciero] CHECK CONSTRAINT [Tarea_Presupuesto_Financiero]
GO
ALTER TABLE [pre].[MstPresupuestoFisico]  WITH CHECK ADD  CONSTRAINT [Articulo_Presupuesto] FOREIGN KEY([IdArticulo], [IdRubro])
REFERENCES [pre].[CatArticulos] ([IdArticulo], [IdRubro])
GO
ALTER TABLE [pre].[MstPresupuestoFisico] CHECK CONSTRAINT [Articulo_Presupuesto]
GO
ALTER TABLE [pre].[MstPresupuestoFisico]  WITH CHECK ADD  CONSTRAINT [Presupuesto_Fisico] FOREIGN KEY([IdPresupuesto])
REFERENCES [pre].[MstPresupuesto] ([IdPresupuesto])
GO
ALTER TABLE [pre].[MstPresupuestoFisico] CHECK CONSTRAINT [Presupuesto_Fisico]
GO
ALTER TABLE [pre].[MstPresupuestoFisico]  WITH CHECK ADD  CONSTRAINT [Rubro_PresupuestoFisico] FOREIGN KEY([IdRubro])
REFERENCES [pre].[CatRubros] ([IdRubro])
GO
ALTER TABLE [pre].[MstPresupuestoFisico] CHECK CONSTRAINT [Rubro_PresupuestoFisico]
GO
ALTER TABLE [pre].[MstPresupuestoFisico]  WITH CHECK ADD  CONSTRAINT [Tarea_Presupuesto_Fisico] FOREIGN KEY([IdTarea], [IdEntregable], [IdProyecto])
REFERENCES [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto])
ON DELETE CASCADE
GO
ALTER TABLE [pre].[MstPresupuestoFisico] CHECK CONSTRAINT [Tarea_Presupuesto_Fisico]
GO
ALTER TABLE [pro].[ArchivosTareas]  WITH CHECK ADD  CONSTRAINT [Archivos_Tarea] FOREIGN KEY([IdTarea], [IdEntregable], [IdProyecto])
REFERENCES [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto])
GO
ALTER TABLE [pro].[ArchivosTareas] CHECK CONSTRAINT [Archivos_Tarea]
GO
ALTER TABLE [pro].[BitacoraTareas]  WITH CHECK ADD  CONSTRAINT [Bitacora_Archivo_Tarea] FOREIGN KEY([IdArchivoTarea])
REFERENCES [pro].[ArchivosTareas] ([IdArchivoTarea])
GO
ALTER TABLE [pro].[BitacoraTareas] CHECK CONSTRAINT [Bitacora_Archivo_Tarea]
GO
ALTER TABLE [pro].[BitacoraTareas]  WITH CHECK ADD  CONSTRAINT [Bitacora_Tarea] FOREIGN KEY([IdTarea], [IdEntregable], [IdProyecto])
REFERENCES [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto])
ON DELETE CASCADE
GO
ALTER TABLE [pro].[BitacoraTareas] CHECK CONSTRAINT [Bitacora_Tarea]
GO
ALTER TABLE [pro].[DetAvancePreguntas]  WITH CHECK ADD  CONSTRAINT [Avance_Pregunta] FOREIGN KEY([IdAvance])
REFERENCES [pro].[MstAvances] ([IdAvance])
GO
ALTER TABLE [pro].[DetAvancePreguntas] CHECK CONSTRAINT [Avance_Pregunta]
GO
ALTER TABLE [pro].[DetAvancePreguntas]  WITH CHECK ADD  CONSTRAINT [Pregunta_Avance] FOREIGN KEY([IdPregunta])
REFERENCES [pro].[MstPreguntas] ([IdPregunta])
GO
ALTER TABLE [pro].[DetAvancePreguntas] CHECK CONSTRAINT [Pregunta_Avance]
GO
ALTER TABLE [pro].[DetConfiguracionTareaFrecuencia]  WITH CHECK ADD  CONSTRAINT [Tarea_ConfiguracionFrecuencia] FOREIGN KEY([IdTarea], [IdEntregable], [IdProyecto])
REFERENCES [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto])
GO
ALTER TABLE [pro].[DetConfiguracionTareaFrecuencia] CHECK CONSTRAINT [Tarea_ConfiguracionFrecuencia]
GO
ALTER TABLE [pro].[DetIntervaloFrecuencia]  WITH CHECK ADD  CONSTRAINT [Frecuencia_Intervalo] FOREIGN KEY([IdConfiguracionFrecuencia])
REFERENCES [pro].[DetConfiguracionTareaFrecuencia] ([IdConfiguracionFrecuencia])
GO
ALTER TABLE [pro].[DetIntervaloFrecuencia] CHECK CONSTRAINT [Frecuencia_Intervalo]
GO
ALTER TABLE [pro].[DetObjetivoPreguntas]  WITH CHECK ADD  CONSTRAINT [Objetivo_Pregunta] FOREIGN KEY([IdObjetivo])
REFERENCES [pro].[MstObjetivos] ([IdObjetivo])
GO
ALTER TABLE [pro].[DetObjetivoPreguntas] CHECK CONSTRAINT [Objetivo_Pregunta]
GO
ALTER TABLE [pro].[DetObjetivoPreguntas]  WITH CHECK ADD  CONSTRAINT [Pregunta_Objetivo] FOREIGN KEY([IdPregunta])
REFERENCES [pro].[MstPreguntas] ([IdPregunta])
GO
ALTER TABLE [pro].[DetObjetivoPreguntas] CHECK CONSTRAINT [Pregunta_Objetivo]
GO
ALTER TABLE [pro].[DetPersonal]  WITH CHECK ADD  CONSTRAINT [Institucion_Personal] FOREIGN KEY([IdInstitucion])
REFERENCES [config].[MstInstituciones] ([IdInstitucion])
GO
ALTER TABLE [pro].[DetPersonal] CHECK CONSTRAINT [Institucion_Personal]
GO
ALTER TABLE [pro].[DetPersonal]  WITH CHECK ADD  CONSTRAINT [Miembro_Proyecto] FOREIGN KEY([IdPersona])
REFERENCES [config].[MstPersonas] ([IdPersona])
GO
ALTER TABLE [pro].[DetPersonal] CHECK CONSTRAINT [Miembro_Proyecto]
GO
ALTER TABLE [pro].[DetPersonal]  WITH CHECK ADD  CONSTRAINT [Programa_Personal] FOREIGN KEY([IdPrograma])
REFERENCES [config].[MstPrograma] ([IdPrograma])
GO
ALTER TABLE [pro].[DetPersonal] CHECK CONSTRAINT [Programa_Personal]
GO
ALTER TABLE [pro].[DetProgramaProyecto]  WITH CHECK ADD  CONSTRAINT [Programa_Proyecto] FOREIGN KEY([IdPrograma])
REFERENCES [config].[MstPrograma] ([IdPrograma])
GO
ALTER TABLE [pro].[DetProgramaProyecto] CHECK CONSTRAINT [Programa_Proyecto]
GO
ALTER TABLE [pro].[DetProgramaProyecto]  WITH CHECK ADD  CONSTRAINT [Proyecto_Programa] FOREIGN KEY([IdProyecto])
REFERENCES [pro].[MstProyectos] ([IdProyecto])
GO
ALTER TABLE [pro].[DetProgramaProyecto] CHECK CONSTRAINT [Proyecto_Programa]
GO
ALTER TABLE [pro].[DetProyectoBeneficiarios]  WITH CHECK ADD  CONSTRAINT [Beneficiario_Proyecto] FOREIGN KEY([IdBeneficiario])
REFERENCES [pro].[MstBeneficiarios] ([IdBeneficiario])
GO
ALTER TABLE [pro].[DetProyectoBeneficiarios] CHECK CONSTRAINT [Beneficiario_Proyecto]
GO
ALTER TABLE [pro].[DetProyectoBeneficiarios]  WITH CHECK ADD  CONSTRAINT [Proyecto_Beneficiario] FOREIGN KEY([IdProyecto])
REFERENCES [pro].[MstProyectos] ([IdProyecto])
GO
ALTER TABLE [pro].[DetProyectoBeneficiarios] CHECK CONSTRAINT [Proyecto_Beneficiario]
GO
ALTER TABLE [pro].[DetProyectoObjetivos]  WITH CHECK ADD  CONSTRAINT [Objetivo_Proyecto] FOREIGN KEY([IdObjetivo])
REFERENCES [pro].[MstObjetivos] ([IdObjetivo])
GO
ALTER TABLE [pro].[DetProyectoObjetivos] CHECK CONSTRAINT [Objetivo_Proyecto]
GO
ALTER TABLE [pro].[DetProyectoObjetivos]  WITH CHECK ADD  CONSTRAINT [Proyecto_Objetivo] FOREIGN KEY([IdProyecto])
REFERENCES [pro].[MstProyectos] ([IdProyecto])
GO
ALTER TABLE [pro].[DetProyectoObjetivos] CHECK CONSTRAINT [Proyecto_Objetivo]
GO
ALTER TABLE [pro].[DetProyectoResponsable]  WITH CHECK ADD  CONSTRAINT [Personal_Responsable] FOREIGN KEY([IdPersonal])
REFERENCES [pro].[DetPersonal] ([IdPersonal])
GO
ALTER TABLE [pro].[DetProyectoResponsable] CHECK CONSTRAINT [Personal_Responsable]
GO
ALTER TABLE [pro].[DetProyectoResponsable]  WITH CHECK ADD  CONSTRAINT [Proyecto_Responsable] FOREIGN KEY([IdProyecto])
REFERENCES [pro].[MstProyectos] ([IdProyecto])
GO
ALTER TABLE [pro].[DetProyectoResponsable] CHECK CONSTRAINT [Proyecto_Responsable]
GO
ALTER TABLE [pro].[DetTareaPersonal]  WITH CHECK ADD  CONSTRAINT [Personal_Tarea] FOREIGN KEY([IdPersonal])
REFERENCES [pro].[DetPersonal] ([IdPersonal])
GO
ALTER TABLE [pro].[DetTareaPersonal] CHECK CONSTRAINT [Personal_Tarea]
GO
ALTER TABLE [pro].[DetTareaPersonal]  WITH CHECK ADD  CONSTRAINT [Tarea_Personal] FOREIGN KEY([IdTarea], [IdEntregable], [IdProyecto])
REFERENCES [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto])
ON DELETE CASCADE
GO
ALTER TABLE [pro].[DetTareaPersonal] CHECK CONSTRAINT [Tarea_Personal]
GO
ALTER TABLE [pro].[MstAvances]  WITH CHECK ADD  CONSTRAINT [Tarea_Avance] FOREIGN KEY([IdTarea], [IdEntregable], [IdProyecto])
REFERENCES [pro].[MstTareas] ([IdTarea], [IdEntregable], [IdProyecto])
GO
ALTER TABLE [pro].[MstAvances] CHECK CONSTRAINT [Tarea_Avance]
GO
ALTER TABLE [pro].[MstComentarios]  WITH CHECK ADD  CONSTRAINT [Mensaje_Comentario] FOREIGN KEY([IdMensaje])
REFERENCES [pro].[MstMensajes] ([IdMensaje])
GO
ALTER TABLE [pro].[MstComentarios] CHECK CONSTRAINT [Mensaje_Comentario]
GO
ALTER TABLE [pro].[MstEntregables]  WITH CHECK ADD  CONSTRAINT [Proyecto_Entregable] FOREIGN KEY([IdProyecto])
REFERENCES [pro].[MstProyectos] ([IdProyecto])
GO
ALTER TABLE [pro].[MstEntregables] CHECK CONSTRAINT [Proyecto_Entregable]
GO
ALTER TABLE [pro].[MstMensajes]  WITH CHECK ADD  CONSTRAINT [Proyecto_Mensaje] FOREIGN KEY([IdProyecto])
REFERENCES [pro].[MstProyectos] ([IdProyecto])
GO
ALTER TABLE [pro].[MstMensajes] CHECK CONSTRAINT [Proyecto_Mensaje]
GO
ALTER TABLE [pro].[MstPreguntas]  WITH CHECK ADD  CONSTRAINT [Pregunta_Respuesta] FOREIGN KEY([IdRespuesta], [IdGrupoRespuesta])
REFERENCES [pro].[CatRespuestas] ([IdRespuesta], [IdGrupoRespuesta])
GO
ALTER TABLE [pro].[MstPreguntas] CHECK CONSTRAINT [Pregunta_Respuesta]
GO
ALTER TABLE [pro].[MstTareas]  WITH CHECK ADD  CONSTRAINT [Tarea_Entregable] FOREIGN KEY([IdEntregable], [IdProyecto])
REFERENCES [pro].[MstEntregables] ([IdEntregable], [IdProyecto])
GO
ALTER TABLE [pro].[MstTareas] CHECK CONSTRAINT [Tarea_Entregable]
GO
ALTER TABLE [Security].[Actions]  WITH CHECK ADD  CONSTRAINT [System_Action] FOREIGN KEY([SystemId])
REFERENCES [Security].[Systems] ([SystemId])
GO
ALTER TABLE [Security].[Actions] CHECK CONSTRAINT [System_Action]
GO
ALTER TABLE [Security].[LoginHistory]  WITH CHECK ADD  CONSTRAINT [Usuario_Historial_Login] FOREIGN KEY([UserId])
REFERENCES [Security].[Users] ([UserId])
GO
ALTER TABLE [Security].[LoginHistory] CHECK CONSTRAINT [Usuario_Historial_Login]
GO
ALTER TABLE [Security].[Menu]  WITH CHECK ADD  CONSTRAINT [System_Menu] FOREIGN KEY([SystemId])
REFERENCES [Security].[Systems] ([SystemId])
GO
ALTER TABLE [Security].[Menu] CHECK CONSTRAINT [System_Menu]
GO
ALTER TABLE [Security].[MenuItems]  WITH CHECK ADD  CONSTRAINT [Action_MenuItem] FOREIGN KEY([ActionId])
REFERENCES [Security].[Actions] ([ActionId])
GO
ALTER TABLE [Security].[MenuItems] CHECK CONSTRAINT [Action_MenuItem]
GO
ALTER TABLE [Security].[MenuItems]  WITH CHECK ADD  CONSTRAINT [Item_Menu_Parent] FOREIGN KEY([ItemParentId])
REFERENCES [Security].[MenuItems] ([MenuItemId])
GO
ALTER TABLE [Security].[MenuItems] CHECK CONSTRAINT [Item_Menu_Parent]
GO
ALTER TABLE [Security].[MenuItems]  WITH CHECK ADD  CONSTRAINT [Menu_MenuItem] FOREIGN KEY([MenuId])
REFERENCES [Security].[Menu] ([MenuId])
GO
ALTER TABLE [Security].[MenuItems] CHECK CONSTRAINT [Menu_MenuItem]
GO
ALTER TABLE [Security].[PasswordHistory]  WITH CHECK ADD  CONSTRAINT [Usuario_Historial_Contrasenia] FOREIGN KEY([UserId])
REFERENCES [Security].[Users] ([UserId])
GO
ALTER TABLE [Security].[PasswordHistory] CHECK CONSTRAINT [Usuario_Historial_Contrasenia]
GO
ALTER TABLE [Security].[RoleAction]  WITH CHECK ADD  CONSTRAINT [Relationship5] FOREIGN KEY([ActionId])
REFERENCES [Security].[Actions] ([ActionId])
GO
ALTER TABLE [Security].[RoleAction] CHECK CONSTRAINT [Relationship5]
GO
ALTER TABLE [Security].[RoleAction]  WITH CHECK ADD  CONSTRAINT [Role_Action] FOREIGN KEY([RoleId], [SystemId])
REFERENCES [Security].[SystemRoles] ([RoleId], [SystemId])
GO
ALTER TABLE [Security].[RoleAction] CHECK CONSTRAINT [Role_Action]
GO
ALTER TABLE [Security].[RoleMenu]  WITH CHECK ADD  CONSTRAINT [Menu_Role] FOREIGN KEY([MenuId])
REFERENCES [Security].[Menu] ([MenuId])
GO
ALTER TABLE [Security].[RoleMenu] CHECK CONSTRAINT [Menu_Role]
GO
ALTER TABLE [Security].[RoleMenu]  WITH CHECK ADD  CONSTRAINT [Role_Menu] FOREIGN KEY([RoleId], [SystemId])
REFERENCES [Security].[SystemRoles] ([RoleId], [SystemId])
GO
ALTER TABLE [Security].[RoleMenu] CHECK CONSTRAINT [Role_Menu]
GO
ALTER TABLE [Security].[RoleMenuItem]  WITH CHECK ADD  CONSTRAINT [MenuItem_Role] FOREIGN KEY([MenuItemId])
REFERENCES [Security].[MenuItems] ([MenuItemId])
GO
ALTER TABLE [Security].[RoleMenuItem] CHECK CONSTRAINT [MenuItem_Role]
GO
ALTER TABLE [Security].[RoleMenuItem]  WITH CHECK ADD  CONSTRAINT [Role_MenuItem] FOREIGN KEY([MenuId], [RoleId], [SystemId])
REFERENCES [Security].[RoleMenu] ([MenuId], [RoleId], [SystemId])
GO
ALTER TABLE [Security].[RoleMenuItem] CHECK CONSTRAINT [Role_MenuItem]
GO
ALTER TABLE [Security].[Roles]  WITH CHECK ADD  CONSTRAINT [Role_Parent] FOREIGN KEY([RolePerentId])
REFERENCES [Security].[Roles] ([RoleId])
GO
ALTER TABLE [Security].[Roles] CHECK CONSTRAINT [Role_Parent]
GO
ALTER TABLE [Security].[SystemRoles]  WITH CHECK ADD  CONSTRAINT [Role_System] FOREIGN KEY([RoleId])
REFERENCES [Security].[Roles] ([RoleId])
GO
ALTER TABLE [Security].[SystemRoles] CHECK CONSTRAINT [Role_System]
GO
ALTER TABLE [Security].[SystemRoles]  WITH CHECK ADD  CONSTRAINT [System_Role] FOREIGN KEY([SystemId])
REFERENCES [Security].[Systems] ([SystemId])
GO
ALTER TABLE [Security].[SystemRoles] CHECK CONSTRAINT [System_Role]
GO
ALTER TABLE [Security].[UserRoles]  WITH CHECK ADD  CONSTRAINT [Relationship7] FOREIGN KEY([RoleId], [SystemId])
REFERENCES [Security].[SystemRoles] ([RoleId], [SystemId])
GO
ALTER TABLE [Security].[UserRoles] CHECK CONSTRAINT [Relationship7]
GO
ALTER TABLE [Security].[UserRoles]  WITH CHECK ADD  CONSTRAINT [Usuario_Usuario_Roles] FOREIGN KEY([UserId])
REFERENCES [Security].[Users] ([UserId])
GO
ALTER TABLE [Security].[UserRoles] CHECK CONSTRAINT [Usuario_Usuario_Roles]
GO
ALTER TABLE [Security].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Users] FOREIGN KEY([UserId])
REFERENCES [Security].[Users] ([UserId])
GO
ALTER TABLE [Security].[Users] CHECK CONSTRAINT [FK_Users_Users]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador parametros' , @level0type=N'SCHEMA',@level0name=N'admin', @level1type=N'TABLE',@level1name=N'CatParametros', @level2type=N'COLUMN',@level2name=N'IdPerametro'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre del parámetro' , @level0type=N'SCHEMA',@level0name=N'admin', @level1type=N'TABLE',@level1name=N'CatParametros', @level2type=N'COLUMN',@level2name=N'NombreParametro'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor parámetro' , @level0type=N'SCHEMA',@level0name=N'admin', @level1type=N'TABLE',@level1name=N'CatParametros', @level2type=N'COLUMN',@level2name=N'ValorParametro'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descripción del parámetro' , @level0type=N'SCHEMA',@level0name=N'admin', @level1type=N'TABLE',@level1name=N'CatParametros', @level2type=N'COLUMN',@level2name=N'DescParametro'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cátalogos de parámetros' , @level0type=N'SCHEMA',@level0name=N'admin', @level1type=N'TABLE',@level1name=N'CatParametros'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del detalle del catálogo' , @level0type=N'SCHEMA',@level0name=N'admin', @level1type=N'TABLE',@level1name=N'DetCatalogo', @level2type=N'COLUMN',@level2name=N'IdDetCatalogo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Llave foranea del id de la tabla maestro catálogo' , @level0type=N'SCHEMA',@level0name=N'admin', @level1type=N'TABLE',@level1name=N'DetCatalogo', @level2type=N'COLUMN',@level2name=N'IdMstCatalogo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor del catálogo' , @level0type=N'SCHEMA',@level0name=N'admin', @level1type=N'TABLE',@level1name=N'DetCatalogo', @level2type=N'COLUMN',@level2name=N'Valor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descripción del detalle catálogo' , @level0type=N'SCHEMA',@level0name=N'admin', @level1type=N'TABLE',@level1name=N'DetCatalogo', @level2type=N'COLUMN',@level2name=N'Descripcion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Estado del registro donde True => Activo; False => Inactivo' , @level0type=N'SCHEMA',@level0name=N'admin', @level1type=N'TABLE',@level1name=N'DetCatalogo', @level2type=N'COLUMN',@level2name=N'Activo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del usuario que ingresó el registro' , @level0type=N'SCHEMA',@level0name=N'admin', @level1type=N'TABLE',@level1name=N'DetCatalogo', @level2type=N'COLUMN',@level2name=N'IdUsuarioIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se realizó el ingreso del registro
' , @level0type=N'SCHEMA',@level0name=N'admin', @level1type=N'TABLE',@level1name=N'DetCatalogo', @level2type=N'COLUMN',@level2name=N'FechaIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del último usuario que modificó el registro' , @level0type=N'SCHEMA',@level0name=N'admin', @level1type=N'TABLE',@level1name=N'DetCatalogo', @level2type=N'COLUMN',@level2name=N'IdUsuarioUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de la última modificación del registro' , @level0type=N'SCHEMA',@level0name=N'admin', @level1type=N'TABLE',@level1name=N'DetCatalogo', @level2type=N'COLUMN',@level2name=N'FechaUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla donde se registran los datos de la tablas catátologos registradas en el maestro de tablas catálogos' , @level0type=N'SCHEMA',@level0name=N'admin', @level1type=N'TABLE',@level1name=N'DetCatalogo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador de la tabla catálogo' , @level0type=N'SCHEMA',@level0name=N'admin', @level1type=N'TABLE',@level1name=N'MstCatalogo', @level2type=N'COLUMN',@level2name=N'IdCatalogo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre de la tabla catálogo' , @level0type=N'SCHEMA',@level0name=N'admin', @level1type=N'TABLE',@level1name=N'MstCatalogo', @level2type=N'COLUMN',@level2name=N'Nombre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descripción de la tabla catálogo' , @level0type=N'SCHEMA',@level0name=N'admin', @level1type=N'TABLE',@level1name=N'MstCatalogo', @level2type=N'COLUMN',@level2name=N'Descripcion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera' , @level0type=N'SCHEMA',@level0name=N'admin', @level1type=N'TABLE',@level1name=N'MstCatalogo', @level2type=N'COLUMN',@level2name=N'Activo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del usuario que ingresó el registro' , @level0type=N'SCHEMA',@level0name=N'admin', @level1type=N'TABLE',@level1name=N'MstCatalogo', @level2type=N'COLUMN',@level2name=N'IdUsuarioIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se realizó el ingreso del registro' , @level0type=N'SCHEMA',@level0name=N'admin', @level1type=N'TABLE',@level1name=N'MstCatalogo', @level2type=N'COLUMN',@level2name=N'FechaIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del último usuario que modificó el registro' , @level0type=N'SCHEMA',@level0name=N'admin', @level1type=N'TABLE',@level1name=N'MstCatalogo', @level2type=N'COLUMN',@level2name=N'IdUsuarioUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de la última modificación del registro' , @level0type=N'SCHEMA',@level0name=N'admin', @level1type=N'TABLE',@level1name=N'MstCatalogo', @level2type=N'COLUMN',@level2name=N'FechaUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla donde se registran las tabla catálogos del sistema' , @level0type=N'SCHEMA',@level0name=N'admin', @level1type=N'TABLE',@level1name=N'MstCatalogo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre de la tabla catálogo' , @level0type=N'SCHEMA',@level0name=N'admin', @level1type=N'TABLE',@level1name=N'MstSecuencias', @level2type=N'COLUMN',@level2name=N'NombreTabla'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Secuencia de identificador de la tabla' , @level0type=N'SCHEMA',@level0name=N'admin', @level1type=N'TABLE',@level1name=N'MstSecuencias', @level2type=N'COLUMN',@level2name=N'SecuencialTabla'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descripción de la secuencia' , @level0type=N'SCHEMA',@level0name=N'admin', @level1type=N'TABLE',@level1name=N'MstSecuencias', @level2type=N'COLUMN',@level2name=N'DescTabla'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla de cotrol de secuencia de llaves de tablas de la base de datos' , @level0type=N'SCHEMA',@level0name=N'admin', @level1type=N'TABLE',@level1name=N'MstSecuencias'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador dia feriado' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'CatFeriados', @level2type=N'COLUMN',@level2name=N'IdFeriado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descripción o motivo feriado' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'CatFeriados', @level2type=N'COLUMN',@level2name=N'MotivoFeriado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha del día feriado' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'CatFeriados', @level2type=N'COLUMN',@level2name=N'FechaFeriado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bnadera que indica si el feriado esta activo o no' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'CatFeriados', @level2type=N'COLUMN',@level2name=N'Activo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del usuario que ingresó el registro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'CatFeriados', @level2type=N'COLUMN',@level2name=N'IdUsuarioIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha que se realizó el ingreso del registro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'CatFeriados', @level2type=N'COLUMN',@level2name=N'FechaIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del último usuario que modificó el registro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'CatFeriados', @level2type=N'COLUMN',@level2name=N'IdUsuarioUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de la última modificación del registro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'CatFeriados', @level2type=N'COLUMN',@level2name=N'FechaUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Catálogos de feriados' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'CatFeriados'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id contacto beneficiario' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'DetContactoBeneficiario', @level2type=N'COLUMN',@level2name=N'IdContactoBeneficiario'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla contacto de beneficiario' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'DetContactoBeneficiario'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla de contacto de instituciones' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'DetContactoInstitucion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador de tabla' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'DetPersonaDireccion', @level2type=N'COLUMN',@level2name=N'IdPersonaDireccion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador de la tabla catálogo CatTipoDireccion ' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'DetPersonaDireccion', @level2type=N'COLUMN',@level2name=N'IdTipoDireccion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dirección' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'DetPersonaDireccion', @level2type=N'COLUMN',@level2name=N'Direccion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera que indica si la dirección está activa o no' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'DetPersonaDireccion', @level2type=N'COLUMN',@level2name=N'Activo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del usuario que ingresó el registro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'DetPersonaDireccion', @level2type=N'COLUMN',@level2name=N'IdUsuarioIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se realizó el ingreso del registro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'DetPersonaDireccion', @level2type=N'COLUMN',@level2name=N'FechaIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del último usuario que modificó el registro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'DetPersonaDireccion', @level2type=N'COLUMN',@level2name=N'IdUsuarioUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de la última modificación del registro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'DetPersonaDireccion', @level2type=N'COLUMN',@level2name=N'FechaUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla detalle de direcciones de personas' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'DetPersonaDireccion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador de tabla' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'DetPersonaEmail', @level2type=N'COLUMN',@level2name=N'IdPersonaEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dirección de correo electrónico' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'DetPersonaEmail', @level2type=N'COLUMN',@level2name=N'Email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera que indica si el email esta activo' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'DetPersonaEmail', @level2type=N'COLUMN',@level2name=N'Activo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del usuario que ingresó el registro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'DetPersonaEmail', @level2type=N'COLUMN',@level2name=N'IdUsuarioIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se realizó el ingreso del registro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'DetPersonaEmail', @level2type=N'COLUMN',@level2name=N'FechaIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del último usuario que modificó el registro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'DetPersonaEmail', @level2type=N'COLUMN',@level2name=N'IdUsuarioUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de la última modificación del registro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'DetPersonaEmail', @level2type=N'COLUMN',@level2name=N'FechaUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla detalle de correos electrónico de personas' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'DetPersonaEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador de tabla' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'DetPersonaTelefono', @level2type=N'COLUMN',@level2name=N'IdPersonaTelefono'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador de la tabla catálogo TipoTelefono' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'DetPersonaTelefono', @level2type=N'COLUMN',@level2name=N'IdTipoNumero'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número telefónico' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'DetPersonaTelefono', @level2type=N'COLUMN',@level2name=N'NumeroTelefono'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera que indica si el telefono está activo' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'DetPersonaTelefono', @level2type=N'COLUMN',@level2name=N'Activo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del usuario que ingresó el registro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'DetPersonaTelefono', @level2type=N'COLUMN',@level2name=N'IdUsuarioIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se realizó el ingreso del registro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'DetPersonaTelefono', @level2type=N'COLUMN',@level2name=N'FechaIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del último usuario que modificó el registro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'DetPersonaTelefono', @level2type=N'COLUMN',@level2name=N'IdUsuarioUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de la última modificación del registro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'DetPersonaTelefono', @level2type=N'COLUMN',@level2name=N'FechaUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador de la tabla' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstInstituciones', @level2type=N'COLUMN',@level2name=N'IdInstitucion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador de tabla catálogo, que indica el tipo de institución, Para este proyecto los tipos son => Alianza, Cooperante' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstInstituciones', @level2type=N'COLUMN',@level2name=N'IdTipoInstitucion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre de la institucion' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstInstituciones', @level2type=N'COLUMN',@level2name=N'Nombre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Siglas de la institucion' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstInstituciones', @level2type=N'COLUMN',@level2name=N'Siglas'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sitio web de la institucion' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstInstituciones', @level2type=N'COLUMN',@level2name=N'SitioWeb'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número telefónico de la institución' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstInstituciones', @level2type=N'COLUMN',@level2name=N'Telefono'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ubicación de logo de la institución' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstInstituciones', @level2type=N'COLUMN',@level2name=N'Logo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera que indica si el registro esta activo o inactivo' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstInstituciones', @level2type=N'COLUMN',@level2name=N'Activo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del usuario que ingresó el registro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstInstituciones', @level2type=N'COLUMN',@level2name=N'IdUsuarioIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se realizó el ingreso del registro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstInstituciones', @level2type=N'COLUMN',@level2name=N'FechaIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del último usuario que modificó el registro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstInstituciones', @level2type=N'COLUMN',@level2name=N'IdUsuarioUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de la última modificación del registro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstInstituciones', @level2type=N'COLUMN',@level2name=N'FechaUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla de instituciones aliadas o acompañantes y de instituciones cooperante al centro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstInstituciones'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador de personas' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstPersonas', @level2type=N'COLUMN',@level2name=N'IdPersona'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número de cédula u otra identificación de la persona' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstPersonas', @level2type=N'COLUMN',@level2name=N'Identificacion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre completo de la persona' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstPersonas', @level2type=N'COLUMN',@level2name=N'NombreCompleto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sexo de la persona M => masculino F => femenino' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstPersonas', @level2type=N'COLUMN',@level2name=N'SexoPersona'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Estado del registro True => Activo; False => Inactivo' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstPersonas', @level2type=N'COLUMN',@level2name=N'Activo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del usuario que ingresó el registro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstPersonas', @level2type=N'COLUMN',@level2name=N'IdUsuarioIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se realizó el ingreso del registro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstPersonas', @level2type=N'COLUMN',@level2name=N'FechaIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del último usuario que modificó el registro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstPersonas', @level2type=N'COLUMN',@level2name=N'IdUsuarioUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de la última modificación del registro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstPersonas', @level2type=N'COLUMN',@level2name=N'FechaUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla general de personas donde contiene los principales datos de personas que estan relacionadas con las ejecuciones de los proyectos' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstPersonas'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id de programa' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstPrograma', @level2type=N'COLUMN',@level2name=N'IdPrograma'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre del programa o división del centro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstPrograma', @level2type=N'COLUMN',@level2name=N'NombrePrograma'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descripción del programa' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstPrograma', @level2type=N'COLUMN',@level2name=N'Descripcion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Estado de la división' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstPrograma', @level2type=N'COLUMN',@level2name=N'Activo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del usuario que ingresó el registro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstPrograma', @level2type=N'COLUMN',@level2name=N'IdUsuarioIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se realizó el ingreso del registro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstPrograma', @level2type=N'COLUMN',@level2name=N'FechaIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del último usuario que modificó el registro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstPrograma', @level2type=N'COLUMN',@level2name=N'IdUsuarioUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de la última modificación del registro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstPrograma', @level2type=N'COLUMN',@level2name=N'FechaUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla de programas o de division organizativa de la institución o centro' , @level0type=N'SCHEMA',@level0name=N'config', @level1type=N'TABLE',@level1name=N'MstPrograma'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del articulo' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatArticulos', @level2type=N'COLUMN',@level2name=N'IdArticulo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador de la tabla catalogo unidad de medidas' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatArticulos', @level2type=N'COLUMN',@level2name=N'IdUnidadMedida'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id de moneda en la que esta definido el precio del articulo' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatArticulos', @level2type=N'COLUMN',@level2name=N'IdMoneda'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descripción del artículo' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatArticulos', @level2type=N'COLUMN',@level2name=N'Descripcion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Precio del artículo' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatArticulos', @level2type=N'COLUMN',@level2name=N'Precio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera para el control de borrado lógico de registros' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatArticulos', @level2type=N'COLUMN',@level2name=N'Activo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del usuario que ingresó el registro' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatArticulos', @level2type=N'COLUMN',@level2name=N'IdUsuarioIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se realizó el ingreso del registro' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatArticulos', @level2type=N'COLUMN',@level2name=N'FechaIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del último usuario que modificó el registro' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatArticulos', @level2type=N'COLUMN',@level2name=N'IdUsuarioUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de la última modificación del registro' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatArticulos', @level2type=N'COLUMN',@level2name=N'FechaUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla de artículos con el que se detalla el presupuesto fisico' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatArticulos'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id de moneda' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatMoneda', @level2type=N'COLUMN',@level2name=N'IdMoneda'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre moneda' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatMoneda', @level2type=N'COLUMN',@level2name=N'Nombre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Simbolo de la moneda' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatMoneda', @level2type=N'COLUMN',@level2name=N'Simbolo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera que indica si es la moneda local del pais o no' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatMoneda', @level2type=N'COLUMN',@level2name=N'EsLocal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera para el control de borrado lógico de registros' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatMoneda', @level2type=N'COLUMN',@level2name=N'Activo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del usuario que ingresó el registro' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatMoneda', @level2type=N'COLUMN',@level2name=N'IdUsuarioIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se realizó el ingreso del registro' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatMoneda', @level2type=N'COLUMN',@level2name=N'FechaIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del último usuario que modificó el registro' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatMoneda', @level2type=N'COLUMN',@level2name=N'IdUsuarioUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de la última modificación del registro' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatMoneda', @level2type=N'COLUMN',@level2name=N'FechaUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Catálogo de monedas, las que se manejaran en los proyectos' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatMoneda'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador de rubros' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatRubros', @level2type=N'COLUMN',@level2name=N'IdRubro'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre del rubro' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatRubros', @level2type=N'COLUMN',@level2name=N'Nombre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador de tipos de rubros' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatRubros', @level2type=N'COLUMN',@level2name=N'IdTipoRubro'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del grupo de rubro al cual pertenece el rubro actual' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatRubros', @level2type=N'COLUMN',@level2name=N'IdRubroParent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera para el control de borrado lógico de registros' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatRubros', @level2type=N'COLUMN',@level2name=N'Activo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N' Identificador del usuario que ingresó el registro' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatRubros', @level2type=N'COLUMN',@level2name=N'IdUsuarioIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se realizó el ingreso del registro' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatRubros', @level2type=N'COLUMN',@level2name=N'FechaIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del último usuario que modificó el registro' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatRubros', @level2type=N'COLUMN',@level2name=N'IdUsuarioUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de la última modificación del registro' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatRubros', @level2type=N'COLUMN',@level2name=N'FechaUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla de rubros para presupuestar el proyecto' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatRubros'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador de la tasa de cambio' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatTasaCambio', @level2type=N'COLUMN',@level2name=N'IdTasaCambio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id de la moneda ' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatTasaCambio', @level2type=N'COLUMN',@level2name=N'IdMoneda'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha a la que corresponde el cambio' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatTasaCambio', @level2type=N'COLUMN',@level2name=N'Fecha'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor de la tasa de cambio' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatTasaCambio', @level2type=N'COLUMN',@level2name=N'Valor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla de tasa de cambio que se cargará en referencia a la moneda local' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'CatTasaCambio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id consolidado de presupuesto por rubro, tarea y division' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'MstPresupuesto', @level2type=N'COLUMN',@level2name=N'IdPresupuesto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Monto presupuestario planificado o formulado' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'MstPresupuesto', @level2type=N'COLUMN',@level2name=N'PresupuestoAsignado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Monto que se actualizará conforme se ejecute el proyecto de modo que al registrar la tarea tendrá el mismo presupuesto asignado' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'MstPresupuesto', @level2type=N'COLUMN',@level2name=N'PresupuestoActual'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Monto presupuestario ejecutado una vez finalizado el proyecto' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'MstPresupuesto', @level2type=N'COLUMN',@level2name=N'PresupuestoEjecutado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera para el control de borrado lógico de registros' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'MstPresupuesto', @level2type=N'COLUMN',@level2name=N'Activo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del usuario que ingresó el registro' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'MstPresupuesto', @level2type=N'COLUMN',@level2name=N'IdUsuarioIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se realizó el ingreso del registro' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'MstPresupuesto', @level2type=N'COLUMN',@level2name=N'FechaIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del último usuario que modificó el registro' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'MstPresupuesto', @level2type=N'COLUMN',@level2name=N'IdUsuarioUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de la última modificación del registro' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'MstPresupuesto', @level2type=N'COLUMN',@level2name=N'FechaUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla maestra donde se guarda de forma generar el presupuesto para los proyectos por entregable' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'MstPresupuesto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del presupuesto financiero' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'MstPresupuestoFinanciero', @level2type=N'COLUMN',@level2name=N'IdPresupuestoFinanciero'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Monto del presupuesto' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'MstPresupuestoFinanciero', @level2type=N'COLUMN',@level2name=N'MontoPresupuestario'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera para el control de borrado lógico de registros' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'MstPresupuestoFinanciero', @level2type=N'COLUMN',@level2name=N'Activo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del usuario que ingresó el registro' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'MstPresupuestoFinanciero', @level2type=N'COLUMN',@level2name=N'IdUsuarioIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se realizó el ingreso del registro' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'MstPresupuestoFinanciero', @level2type=N'COLUMN',@level2name=N'FechaIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del último usuario que modificó el registro' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'MstPresupuestoFinanciero', @level2type=N'COLUMN',@level2name=N'IdUsuarioUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de la última modificación del registro' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'MstPresupuestoFinanciero', @level2type=N'COLUMN',@level2name=N'FechaUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla de presupuesto financiero por tarea' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'MstPresupuestoFinanciero'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador de presupuesto fisico' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'MstPresupuestoFisico', @level2type=N'COLUMN',@level2name=N'IdPresupuestoFisico'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cantidad de articulos' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'MstPresupuestoFisico', @level2type=N'COLUMN',@level2name=N'CantidadArticulos'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Precio de articulo por la cantidad necesitada' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'MstPresupuestoFisico', @level2type=N'COLUMN',@level2name=N'CostoArticulos'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera para el control de borrado lógico de registros' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'MstPresupuestoFisico', @level2type=N'COLUMN',@level2name=N'Activo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del usuario que ingresó el registro' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'MstPresupuestoFisico', @level2type=N'COLUMN',@level2name=N'IdUsuarioIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se realizó el ingreso del registro' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'MstPresupuestoFisico', @level2type=N'COLUMN',@level2name=N'FechaIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del último usuario que modificó el registro' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'MstPresupuestoFisico', @level2type=N'COLUMN',@level2name=N'IdUsuarioUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de la última modificación del registro' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'MstPresupuestoFisico', @level2type=N'COLUMN',@level2name=N'FechaUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla para presupuestar fisicamente las tareas del proyecto' , @level0type=N'SCHEMA',@level0name=N'pre', @level1type=N'TABLE',@level1name=N'MstPresupuestoFisico'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador de la respuesta' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'CatRespuestas', @level2type=N'COLUMN',@level2name=N'IdRespuesta'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del catalogo del Grupo de respuesta' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'CatRespuestas', @level2type=N'COLUMN',@level2name=N'IdGrupoRespuesta'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descripción de la respuesta
' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'CatRespuestas', @level2type=N'COLUMN',@level2name=N'Descripcion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor porcentual de la respuesta' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'CatRespuestas', @level2type=N'COLUMN',@level2name=N'Valor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Catálogo de respuestas  permitidas para responder a las preguntas de control de objetivos cuantitativos' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'CatRespuestas'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetAvancePreguntas', @level2type=N'COLUMN',@level2name=N'IdAvancePregunta'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla relacional de preguntas con avances para el control de estos' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetAvancePreguntas'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Llave unica para la configuracion' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetConfiguracionTareaFrecuencia', @level2type=N'COLUMN',@level2name=N'IdConfiguracionFrecuencia'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indica con que freecuencia se repetira la tarea. Donde 1 => Diaria, 2 => Semanal, 3 => Mensual, 4 => Anual' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetConfiguracionTareaFrecuencia', @level2type=N'COLUMN',@level2name=N'TipoFrecuencia'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Periodo en dias que repetira la tarea en caso de ser diara la frecuencia' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetConfiguracionTareaFrecuencia', @level2type=N'COLUMN',@level2name=N'CantidadDias'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hora de inicio de la actividad en caso de ser diara la frecuencia' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetConfiguracionTareaFrecuencia', @level2type=N'COLUMN',@level2name=N'HoraInicio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hora fin de tarea en caso de ser diaria la frecuenca' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetConfiguracionTareaFrecuencia', @level2type=N'COLUMN',@level2name=N'HoraFin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Periodo en semanas en el que se repetira la tarea en caso de ser semanal la frecuencia' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetConfiguracionTareaFrecuencia', @level2type=N'COLUMN',@level2name=N'CantidadSemanas'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Día de la semana en la que iniciara la tarea en caso de ser semanal la frecuencia' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetConfiguracionTareaFrecuencia', @level2type=N'COLUMN',@level2name=N'DiaInicio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Periodo en meses en el que se repetira la tarea en caso de ser mensual la frecuencia' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetConfiguracionTareaFrecuencia', @level2type=N'COLUMN',@level2name=N'CantidadMeses'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numero de la semana de la cual se repetira el dia elegido en la frecuencia mensual' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetConfiguracionTareaFrecuencia', @level2type=N'COLUMN',@level2name=N'NumeroSemana'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Periodo en años en el cual la tarea se repitara en caso de anual la frecuencia' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetConfiguracionTareaFrecuencia', @level2type=N'COLUMN',@level2name=N'CantidadAnios'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Mes en el cual se llevara acabo la tarea en caso de ser anual la frecuencia' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetConfiguracionTareaFrecuencia', @level2type=N'COLUMN',@level2name=N'Mes'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cantidad de días que durara la tarea en caso de ser semanal, mensual o anual la frecuencia' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetConfiguracionTareaFrecuencia', @level2type=N'COLUMN',@level2name=N'DuracionDias'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Día del mes en que iniciara la tarea de 1 - 31' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetConfiguracionTareaFrecuencia', @level2type=N'COLUMN',@level2name=N'DiaDelMes'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla de configuración par las tareas que serán de ejecución frecuente' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetConfiguracionTareaFrecuencia'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Llave del intervalo para una frecuenci en especifico' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetIntervaloFrecuencia', @level2type=N'COLUMN',@level2name=N'IdIntervaloFrecuencia'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de inicio de la tarea periodica' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetIntervaloFrecuencia', @level2type=N'COLUMN',@level2name=N'FechaInicio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Inidica que tipo de finalizacion tendra la tarea periodica. Donde 1 => Con el final del proyecto, 2 => o	Después de una cantidad definida de repeticiones, 3 =>  Fecha en específico' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetIntervaloFrecuencia', @level2type=N'COLUMN',@level2name=N'TipoFinalizacion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cantidad de repeticiones de la tarea periodica' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetIntervaloFrecuencia', @level2type=N'COLUMN',@level2name=N'CantidadRepeticiones'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en especifico en cual deben de finalizar las tareas peridicas relacionada' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetIntervaloFrecuencia', @level2type=N'COLUMN',@level2name=N'FechaFin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla de intervalos para las tareas frecuentes' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetIntervaloFrecuencia'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetObjetivoPreguntas', @level2type=N'COLUMN',@level2name=N'IdObjetivoPregunta'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla relacional de objetivos y preguntas' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetObjetivoPreguntas'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador de personal' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetPersonal', @level2type=N'COLUMN',@level2name=N'IdPersonal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador de cargo del detalle catálogo' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetPersonal', @level2type=N'COLUMN',@level2name=N'IdCargo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla de personal de los áreas de la institución o de las instituciones acompañantes de los proyecto' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetPersonal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id detalle programa proyecto' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetProgramaProyecto', @level2type=N'COLUMN',@level2name=N'IdProgramaProyecto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla relacional Programas - Proyectos' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetProgramaProyecto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indica que tipo de destinatario esta enfocado el proyecto donde M => Mismo cliente,  G => Grupo meta y P => Organización, Empresa, institución, etc...' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetProyectoBeneficiarios', @level2type=N'COLUMN',@level2name=N'TipoDestinatario'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del beneficiario puede ser nulo, ya que el beneficiario puede ser un grupo de persona y se abordaría en la descripción' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetProyectoBeneficiarios', @level2type=N'COLUMN',@level2name=N'IdBeneficiario'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'En caso de ser grupo meta se registrara una descripción a quienes va abocado en proyecto' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetProyectoBeneficiarios', @level2type=N'COLUMN',@level2name=N'Descripcion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla relacional proyecto - beneficiarios' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetProyectoBeneficiarios'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetProyectoObjetivos', @level2type=N'COLUMN',@level2name=N'IdProyectoObjetivo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla relacional proyecto - objetivo' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetProyectoObjetivos'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla de responsables de proyectos' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetProyectoResponsable'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetTareaPersonal', @level2type=N'COLUMN',@level2name=N'IdTareaPersonal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera que indica el rol de la persona en la tarea, donde R =>  responsable y C => colaborador' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetTareaPersonal', @level2type=N'COLUMN',@level2name=N'TipoPersonal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ciudad/Provincia/Estado donde se llevará acabo la tarea' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetTareaPersonal', @level2type=N'COLUMN',@level2name=N'Ciudad'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Lugar espécifico donde se realizará la tarea (parque, escuela, iglesia, etc..)' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetTareaPersonal', @level2type=N'COLUMN',@level2name=N'Lugar'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla de asignación de tareas a personal que son miembros del proyecto' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'DetTareaPersonal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador de avances' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstAvances', @level2type=N'COLUMN',@level2name=N'IdAvance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descripción del avance' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstAvances', @level2type=N'COLUMN',@level2name=N'Descripcion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha hasta la cual se realizó el avance' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstAvances', @level2type=N'COLUMN',@level2name=N'FechaAvance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Presupuesto utilizado en la tarea, hasta la fecha de avance' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstAvances', @level2type=N'COLUMN',@level2name=N'PresupuestoUtilizado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del usuario que ingresó el registro' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstAvances', @level2type=N'COLUMN',@level2name=N'IdUsuarioIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se realizó el ingreso del registro' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstAvances', @level2type=N'COLUMN',@level2name=N'FechaIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del último usuario que modificó el registro' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstAvances', @level2type=N'COLUMN',@level2name=N'IdUsuarioUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de la última modificación del registro' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstAvances', @level2type=N'COLUMN',@level2name=N'FechaUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla de avances de tareas' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstAvances'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del beneficiario
' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstBeneficiarios', @level2type=N'COLUMN',@level2name=N'IdBeneficiario'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre del beneficiario' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstBeneficiarios', @level2type=N'COLUMN',@level2name=N'Nombre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dirección de la ubicación del beneficiario' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstBeneficiarios', @level2type=N'COLUMN',@level2name=N'Direccion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Se debe registrar aunque sea un número telefónico' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstBeneficiarios', @level2type=N'COLUMN',@level2name=N'Telefono'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dirección de correo electrónico' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstBeneficiarios', @level2type=N'COLUMN',@level2name=N'Email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sitio web si es que el beneficiario posee' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstBeneficiarios', @level2type=N'COLUMN',@level2name=N'WebSite'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Directorio del logo o imagen del beneficiario' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstBeneficiarios', @level2type=N'COLUMN',@level2name=N'Logo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del usuario que ingresó el registro' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstBeneficiarios', @level2type=N'COLUMN',@level2name=N'IdUsuarioIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se realizó el ingreso del registro' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstBeneficiarios', @level2type=N'COLUMN',@level2name=N'FechaIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del último usuario que modificó el registro' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstBeneficiarios', @level2type=N'COLUMN',@level2name=N'IdUsuarioUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de la última modificación del registro' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstBeneficiarios', @level2type=N'COLUMN',@level2name=N'FechaUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla de benificiarios de proyectos' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstBeneficiarios'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del comentario' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstComentarios', @level2type=N'COLUMN',@level2name=N'IdComentario'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cuerpo del comentario' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstComentarios', @level2type=N'COLUMN',@level2name=N'Descripcion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del usuario que realizó el comentario' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstComentarios', @level2type=N'COLUMN',@level2name=N'IdUsuario'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de creación del comentario' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstComentarios', @level2type=N'COLUMN',@level2name=N'Fecha'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla de comentarios de los  mensajes en el foro del sistema' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstComentarios'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del entrgable' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstEntregables', @level2type=N'COLUMN',@level2name=N'IdEntregable'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del entregable parent, en caso de existir de lo contrario este campo es nulo' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstEntregables', @level2type=N'COLUMN',@level2name=N'IdEntregableParent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descripción del entregable' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstEntregables', @level2type=N'COLUMN',@level2name=N'Descripcion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha Inicio del entregable' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstEntregables', @level2type=N'COLUMN',@level2name=N'FechaInicio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha Fin del entregable' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstEntregables', @level2type=N'COLUMN',@level2name=N'FechaFin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Duración del entregable en horas' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstEntregables', @level2type=N'COLUMN',@level2name=N'DuracionEstimadaHoras'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Duración en dias del entregable' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstEntregables', @level2type=N'COLUMN',@level2name=N'DuracionEstimadaDias'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Horas dedicadas al entregable' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstEntregables', @level2type=N'COLUMN',@level2name=N'HorasDedicadas'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador de la tabla estados' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstEntregables', @level2type=N'COLUMN',@level2name=N'IdEstado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nota opcional del entregable' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstEntregables', @level2type=N'COLUMN',@level2name=N'Nota'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del usuario que ingresó el registro' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstEntregables', @level2type=N'COLUMN',@level2name=N'IdUsuarioIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se realizó el ingreso del registro' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstEntregables', @level2type=N'COLUMN',@level2name=N'FechaIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del último usuario que modificó el registro' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstEntregables', @level2type=N'COLUMN',@level2name=N'IdUsuarioUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de la última modificación del registro' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstEntregables', @level2type=N'COLUMN',@level2name=N'FechaUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla de entregables de proyecto' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstEntregables'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del mensaje' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstMensajes', @level2type=N'COLUMN',@level2name=N'IdMensaje'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Asunto del mensaje' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstMensajes', @level2type=N'COLUMN',@level2name=N'Asunto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cuerpo del mensaje' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstMensajes', @level2type=N'COLUMN',@level2name=N'Descripcion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del usuario que creo el mensaje' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstMensajes', @level2type=N'COLUMN',@level2name=N'IdUsuario'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de creación del mensaje' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstMensajes', @level2type=N'COLUMN',@level2name=N'Fecha'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del registro del objetivo' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstObjetivos', @level2type=N'COLUMN',@level2name=N'IdObjetivo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera que indica que tipo de objetivo es G => General o E => Especifico' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstObjetivos', @level2type=N'COLUMN',@level2name=N'TipoObjetivo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descripción del objetivo' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstObjetivos', @level2type=N'COLUMN',@level2name=N'Descripcion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera que indica el estado del registro' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstObjetivos', @level2type=N'COLUMN',@level2name=N'Activo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla de objetivos' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstObjetivos'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id de la tabla pregunta' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstPreguntas', @level2type=N'COLUMN',@level2name=N'IdPregunta'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descripción de la pregunta' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstPreguntas', @level2type=N'COLUMN',@level2name=N'Descripcion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla de preguntas para control de objetivos cuantitativos' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstPreguntas'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador único de los proyectos' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstProyectos', @level2type=N'COLUMN',@level2name=N'IdProyecto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Titúlo del proyecto' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstProyectos', @level2type=N'COLUMN',@level2name=N'TituloProyecto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descripción del proyecto' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstProyectos', @level2type=N'COLUMN',@level2name=N'DescProyecto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'En este campo se describe el porque de la necesidad de llevar acabo el proyecto' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstProyectos', @level2type=N'COLUMN',@level2name=N'Justificacion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha inicio del proyecto' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstProyectos', @level2type=N'COLUMN',@level2name=N'FechaInicio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Esta fecha será la misma de la fecha fin de la última tarea del proyecto' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstProyectos', @level2type=N'COLUMN',@level2name=N'FechaFinEstimada'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha límite de culminación del proyecto' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstProyectos', @level2type=N'COLUMN',@level2name=N'FechaLimite'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Se actualizara conforme los ejecutantes de las actividades actualicen los estados de las mismas' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstProyectos', @level2type=N'COLUMN',@level2name=N'HorasDedicadas'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Será calculado por el sistema en base al rango de fecha seleccionado por usuario, tomando como referencia las configuraciones del cliente de horas laborales por días ' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstProyectos', @level2type=N'COLUMN',@level2name=N'DuracionEstimadaHoras'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera que indica si el proyecto será externo al cliente o no' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstProyectos', @level2type=N'COLUMN',@level2name=N'Externo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cantidad presupuestaria con la que el proyecto contara' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstProyectos', @level2type=N'COLUMN',@level2name=N'Presupuesto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Al registrar el proyecto tendrá el mismo presupuesto asignado conforme se ejecuten las actividades se actualizara este campo' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstProyectos', @level2type=N'COLUMN',@level2name=N'PresupuestoActual'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Este será la sumatoria de todos los gastos de las tareas planificadas ' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstProyectos', @level2type=N'COLUMN',@level2name=N'GastoEstimado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del tipo de proyecto ' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstProyectos', @level2type=N'COLUMN',@level2name=N'IdTipoProyecto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id que hace referencia a la tabla catálogo de estados de proyectos' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstProyectos', @level2type=N'COLUMN',@level2name=N'IdEstado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del usuario que ingresó el registro' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstProyectos', @level2type=N'COLUMN',@level2name=N'IdUsuarioIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se realizó el ingreso del registro' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstProyectos', @level2type=N'COLUMN',@level2name=N'FechaIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del último usuario que modificó el registro' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstProyectos', @level2type=N'COLUMN',@level2name=N'IdUsuarioUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de la última modificación del registro' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstProyectos', @level2type=N'COLUMN',@level2name=N'FechaUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador de la tabla maestro catálogo, este campo se define para calcular las horas dedicadas y los tiempos estimados' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstProyectos', @level2type=N'COLUMN',@level2name=N'DiasLabSem'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla de proyectos' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstProyectos'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id de la tarea' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstTareas', @level2type=N'COLUMN',@level2name=N'IdTarea'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id de la tarea parent, si este campo es null significa que la tarea no tiene padre' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstTareas', @level2type=N'COLUMN',@level2name=N'IdTareaParent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador de la tarea predecesora (Tarea que se debe concluir para comenzar la tarea actual)' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstTareas', @level2type=N'COLUMN',@level2name=N'IdTareaDependencia'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre de la tarea' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstTareas', @level2type=N'COLUMN',@level2name=N'Nombre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de inicio de la tarea' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstTareas', @level2type=N'COLUMN',@level2name=N'FechaInicio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha fin de la tarea' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstTareas', @level2type=N'COLUMN',@level2name=N'FechaFin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Duración de la tarea en días' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstTareas', @level2type=N'COLUMN',@level2name=N'DuracionEstimadaDias'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Duración en hora que se estima se tomara la ejecución de la tarea' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstTareas', @level2type=N'COLUMN',@level2name=N'DuracionEstimadaHoras'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Horas dedicadas a la ejecucion de la tarea' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstTareas', @level2type=N'COLUMN',@level2name=N'HorasDedicadas'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera que indica si la tarea será un hito' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstTareas', @level2type=N'COLUMN',@level2name=N'Hito'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera que indica si no es seguro la realización de la tarea' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstTareas', @level2type=N'COLUMN',@level2name=N'Provisional'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera que indica si se requiera de revisión de la tarea para darla como completada' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstTareas', @level2type=N'COLUMN',@level2name=N'Revisar'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'En caso que se requiera que alguien revise el estado de la actividad para darla como completada se proporcionara un a un usuario miembro del equipo del proyecto como revisor' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstTareas', @level2type=N'COLUMN',@level2name=N'IdUsuarioRevisor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indica si se deberan reportar avances de la ejecucion de la actividad o tarea' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstTareas', @level2type=N'COLUMN',@level2name=N'ReportarAvances'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indica con que frecuencia se reportaran avances para las tareas. Donde 1 => Diario, 2 => Semanal, 3 => Mensual' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstTareas', @level2type=N'COLUMN',@level2name=N'FecuenciaAvance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera que indica si la tarea será periodica' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstTareas', @level2type=N'COLUMN',@level2name=N'TareaPeriodica'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del estado de la tarea' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstTareas', @level2type=N'COLUMN',@level2name=N'IdEstado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nota opcional del la tarea' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstTareas', @level2type=N'COLUMN',@level2name=N'Nota'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del usuario que ingresó el registro' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstTareas', @level2type=N'COLUMN',@level2name=N'IdUsuarioIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se realizó el ingreso del registro' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstTareas', @level2type=N'COLUMN',@level2name=N'FechaIns'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del último usuario que modificó el registro' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstTareas', @level2type=N'COLUMN',@level2name=N'IdUsuarioUpd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera que indica que si la tarea será incluida en el timeline del proyecto' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstTareas', @level2type=N'COLUMN',@level2name=N'TimeLine'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla de tareas' , @level0type=N'SCHEMA',@level0name=N'pro', @level1type=N'TABLE',@level1name=N'MstTareas'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del cada acción, es un número consecutivo' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Actions', @level2type=N'COLUMN',@level2name=N'ActionId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id externo de la tabla maestro de sistema, al cual pertenece la acción o página' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Actions', @level2type=N'COLUMN',@level2name=N'SystemId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre de la acción si el sistema es MVC de lo contrario es nulo' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Actions', @level2type=N'COLUMN',@level2name=N'Action'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre del controlador en caso que el sistema sea MVC' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Actions', @level2type=N'COLUMN',@level2name=N'Controller'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'En el caso que el sistema no sea MVC se debe especificar este campo' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Actions', @level2type=N'COLUMN',@level2name=N'Url'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descripción de la página o acción' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Actions', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera que indica si la acción o la página está activa' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Actions', @level2type=N'COLUMN',@level2name=N'Active'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del usuario que insertó el registro' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Actions', @level2type=N'COLUMN',@level2name=N'UserInsert'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la se insertó el registro' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Actions', @level2type=N'COLUMN',@level2name=N'InsertDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del último usuario que modificó el registro' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Actions', @level2type=N'COLUMN',@level2name=N'UserUpdate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de la última actualización del registro' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Actions', @level2type=N'COLUMN',@level2name=N'UpdateDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla catálogo donde se registran las acciones (MVC) o páginas del sistema' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Actions'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id de la tabla' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'LoginHistory', @level2type=N'COLUMN',@level2name=N'LoginHistoryId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del usuario' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'LoginHistory', @level2type=N'COLUMN',@level2name=N'UserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se inició sesión' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'LoginHistory', @level2type=N'COLUMN',@level2name=N'LoginDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha y hora en la que se finalizó la sesión' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'LoginHistory', @level2type=N'COLUMN',@level2name=N'LogoffDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IP de la computadora desde donde accedió al sistema el usuario' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'LoginHistory', @level2type=N'COLUMN',@level2name=N'IP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id de menú' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Menu', @level2type=N'COLUMN',@level2name=N'MenuId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del sistema al cual pertenece el menú' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Menu', @level2type=N'COLUMN',@level2name=N'SystemId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre del menú' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Menu', @level2type=N'COLUMN',@level2name=N'MenuName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descripción del menú' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Menu', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera que indica si el menú está activo o no' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Menu', @level2type=N'COLUMN',@level2name=N'Active'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del usuario que insertó el registro' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Menu', @level2type=N'COLUMN',@level2name=N'UserInsert'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se insertó el registro' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Menu', @level2type=N'COLUMN',@level2name=N'InsertDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del usuario que realizó la última actualización del registro' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Menu', @level2type=N'COLUMN',@level2name=N'UserUpdate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se actualizó por última vez el registro' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Menu', @level2type=N'COLUMN',@level2name=N'UpdateDate'
GO
EXEC sys.sp_addextendedproperty @name=N'ExtendedProperty0', @value=N'' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Menu', @level2type=N'CONSTRAINT',@level2name=N'Menu_PK'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id de la opción del menú, este campo con el idmenuparent componen la llave de la tabla, si el id parent es nulo el item es principal' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'MenuItems', @level2type=N'COLUMN',@level2name=N'MenuItemId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del menú al cual pertenece el item' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'MenuItems', @level2type=N'COLUMN',@level2name=N'MenuId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del item de menú padre. Si este campo es nulo indica que el item es principal o padre de otro' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'MenuItems', @level2type=N'COLUMN',@level2name=N'ItemParentId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre o texto que se mostrará en el item del menú' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'MenuItems', @level2type=N'COLUMN',@level2name=N'Label'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Directorio dentro del servidor dónde se aloja la imagen para ícono del item del menú' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'MenuItems', @level2type=N'COLUMN',@level2name=N'Icon'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre de la clase css que tendrá el item del menú' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'MenuItems', @level2type=N'COLUMN',@level2name=N'CssClass'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera que indica si la item del menú está activo o inactivo' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'MenuItems', @level2type=N'COLUMN',@level2name=N'Active'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del usuario que insertó el registro' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'MenuItems', @level2type=N'COLUMN',@level2name=N'UserInsert'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se insertó el registro' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'MenuItems', @level2type=N'COLUMN',@level2name=N'InsertDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del usuario que realizó la última actualización del registro' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'MenuItems', @level2type=N'COLUMN',@level2name=N'UserUpdate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se actualizó por última vez el registro' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'MenuItems', @level2type=N'COLUMN',@level2name=N'UpdateDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla catálogo donde se regitran los items del menú de los sitemas' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'MenuItems'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id de la tabla' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'PasswordHistory', @level2type=N'COLUMN',@level2name=N'PasswordHistoryId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del usuario' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'PasswordHistory', @level2type=N'COLUMN',@level2name=N'UserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Última contraseña o contraseña que se está cambiando, hash "sha512"' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'PasswordHistory', @level2type=N'COLUMN',@level2name=N'OldPassword'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contraseña nueva para acceder al sistema, con hash "sha512"' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'PasswordHistory', @level2type=N'COLUMN',@level2name=N'NewPassword'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se realizó el cambio de contraseña' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'PasswordHistory', @level2type=N'COLUMN',@level2name=N'ChangeDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Breve descripción de la razón de cambio de contraseña' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'PasswordHistory', @level2type=N'COLUMN',@level2name=N'ReasonChange'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del rol, si el id parent es NULL el rol es padre o principal' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Roles', @level2type=N'COLUMN',@level2name=N'RoleId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del rol padre, esta tabla es recursiva de modo que un rol puede tener varios roles hijo y heredar permisos' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Roles', @level2type=N'COLUMN',@level2name=N'RolePerentId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre del rol' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Roles', @level2type=N'COLUMN',@level2name=N'RoleName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Breve descripción del rol' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Roles', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera que indica si el rol está activo o inactivo' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Roles', @level2type=N'COLUMN',@level2name=N'Active'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del usuario que insertó el registro' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Roles', @level2type=N'COLUMN',@level2name=N'UserInsert'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se insertó el registro' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Roles', @level2type=N'COLUMN',@level2name=N'InsertDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del usuario que realizó la última actualización del registro
' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Roles', @level2type=N'COLUMN',@level2name=N'UserUpdate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se actualizó por última vez el registro' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Roles', @level2type=N'COLUMN',@level2name=N'UpdateDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla catálogo de roles' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Roles'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del sistema número consecutivo' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Systems', @level2type=N'COLUMN',@level2name=N'SystemId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre del sistema' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Systems', @level2type=N'COLUMN',@level2name=N'SystemName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descripción de funcionalidad, caracteristicas o fines del sistema' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Systems', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Siglas, acrónimo o nombre corto del sistema' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Systems', @level2type=N'COLUMN',@level2name=N'Acronym'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id de la acción de inicio por defecto al ingresar al sistema' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Systems', @level2type=N'COLUMN',@level2name=N'StartActionId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera que indica si el sistema está activo o inactivo' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Systems', @level2type=N'COLUMN',@level2name=N'Active'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del usuario que insertó el registro' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Systems', @level2type=N'COLUMN',@level2name=N'UserInsert'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la se insertó el registro' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Systems', @level2type=N'COLUMN',@level2name=N'InsertDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del último usuario que actualizó el registro' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Systems', @level2type=N'COLUMN',@level2name=N'UserUpdate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de la última actualización del registro' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Systems', @level2type=N'COLUMN',@level2name=N'UpdateDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla de sistemas, en esta tabla se registran todos los sistema a los cuales se les llevará control de seguridad en este módulo' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Systems'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id de la página de inicio de usuario por el rol especificado. Si es nulo se redirecciona a la página de incio del sistema por defecto
' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'UserRoles', @level2type=N'COLUMN',@level2name=N'StartActionId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id de usuario, este identificador es un secuencial controlado en la tabla de secuenciales' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'UserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador que hace referencia a una tabla externa del sistema, el cual vincula al usuario con un determinado registro. Ej: TablaEmpleado donde el codigo de empleado estaría en este campo, indicando que el empleado tiene un usuario en el sistema.' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'ReferenceId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre de usuario para acceder al sistema' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'UserName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre completo del usuario' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'FullName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contraseña para acceso al sistema, con hash "sha512"' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'Password'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Correo del usuario, es obligatorio para la recuperación de acceso al sistema es vía correo electrónico, si se configura de ese modo. Y para acceder al sistema en vez del nombre de usuario.' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'Email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número telefónico del usuario, obligatorio para poder acceder al sistema. Permitiendo que el usuario pueda acceder con su nombre de usuario, correo o número telefónico' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'PhoneNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera que indica si el usuario debera cambiar la contraseña periodicamente o no' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'ChangePassword'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número que indica cada cuanto se cambiara la contraseña, si el campo de cambio de contraseña  es false este debe ser 0' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'PasswordExpiration'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Periodo en cual es permitido que el usuario repita una contraseña ya utilizada, si PasswordExpiration es 0 este campo también' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'PasswordRepeatPeriod'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cantidad de intentos permitidos por logeo para que el usuario acceda al sistema' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'AttemptsPermitted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cantidad de intentos fallido para acceder al sistema, si este excede la cantidad de intentos permitidos el usuario será bloqueado' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'FailedAttempts'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera que indica como recuperar acceso al sistema, 1 => correo externo; 2 => pregunta y respuesta de seguridad' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'TypeAccessRecovery'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pregunta de seguridad con la que el usuario puede recuperar el acceso al sistema, hash "sha512"' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'SecurityQuestion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Respuesta a la pregunta de seguridad para recuperar el acceso al sistema, hash "sha512"' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'AnswerSecurity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tiempo en segundos de cuanto el usuario puede estar logeado si realizar acciones en el sistema' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'ExpirationSession'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Direcctorio físico de la imagen de perfil del usuario
' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'ProfilePhoto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera que indica si el usuario está bloqueado por exceder la cantidad de intentos permitidos al sistema' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'Locked'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bandera que indica si el usuario está activo o inactivo' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'Active'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del usuario que insertó el registro' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'UserInsert'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se insertó el registro' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'InsertDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id del usuario que realizó la última actualización del registro' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'UserUpdate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se actualizó por última vez el registro' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'UpdateDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla de usuarios' , @level0type=N'SCHEMA',@level0name=N'Security', @level1type=N'TABLE',@level1name=N'Users'
GO
USE [master]
GO
ALTER DATABASE [SIGESPRO_PRESENTACION] SET  READ_WRITE 
GO
