﻿using Common.Helpers;
using Sigespro.DAL.Implementations;
using Sigespro.Entity;
using Sigespro.Entity.Resource;
using Sigespro.Service.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigespro.Service.Pre
{
    public class BudgetService : Service
    {
        private Repository<MstPresupuesto> Repository { get; set; }

        public BudgetService() : base()
        {
            Repository = _unitOfWork.GetRepository<MstPresupuesto>();
        }

        public BudgetService(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            Repository = _unitOfWork.GetRepository<MstPresupuesto>();
        }

        public Result<MstPresupuesto> GetBudgetByDeliverable(int deliverableID, int projectID)
        {
            Result<MstPresupuesto> result = new Result<MstPresupuesto>();

            try
            {
                result.MessageType = MessageType.INFO;
                result.Data = Repository.Get(p => p.IdEntregable == deliverableID && p.IdProyecto == projectID);
                result.Message = Messages.SuccessGetResult;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public void GetBudgetByTask(int taskID, int deliverableID, int projectID, ref decimal budgetMount)
        {
            try
            {
                decimal budget = 0;

                var financialBudgets = _unitOfWork.GetRepository<MstPresupuestoFinanciero>().GetMany(x => x.IdEntregable == deliverableID && x.IdProyecto == projectID && x.IdTarea == taskID);
                if (financialBudgets != null && financialBudgets.Count() > 0)
                {
                    foreach (var item in financialBudgets)
                    {
                        budget = budget + item.MontoPresupuestario;
                    }
                }

                var physicalBudgets = _unitOfWork.GetRepository<MstPresupuestoFisico>().GetMany(x => x.IdEntregable == deliverableID && x.IdProyecto == projectID && x.IdTarea == taskID);
                if (physicalBudgets != null && physicalBudgets.Count() > 0)
                {
                    foreach (var item in physicalBudgets)
                    {
                        budget = budget + (item.CantidadArticulos * item.CostoArticulos);
                    }
                }

                budgetMount = budget;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ResultBase InsertBudget(MstPresupuesto budget, bool commit = true)
        {
            ResultBase result = new ResultBase();

            try
            {
                // Obteniendo el identificador del nuevo registro de presupuesto
                var sequencesService = new SequenceService(_unitOfWork.GetRepository<MstSecuencias>());
                MstSecuencias seq = sequencesService.GetNextSequenceByTableName("[pre].[MstPresupuesto]");

                budget.IdPresupuesto = seq.SecuencialTabla;

                sequencesService.InsertSequence(seq);

                // Se valida la información del registro de presupuesto
                if (budget.PresupuestoAsignado <= 0)
                {
                    result.Message = "El presupuesto asignado a la fase del proyecto debe ser mayor a 0";
                    result.DetailException = new Exception(result.Message);
                    result.MessageType = MessageType.ERROR;
                    return result;
                }

                if (budget.IdEntregable <= 0)
                {
                    result.Message = "El identificador de la fase del proyecto no puede ser 0";
                    result.DetailException = new Exception(result.Message);
                    result.MessageType = MessageType.ERROR;
                    return result;
                }

                if (budget.IdProyecto <= 0)
                {
                    result.Message = "El identificador del proyecto no puede ser 0";
                    result.DetailException = new Exception(result.Message);
                    result.MessageType = MessageType.ERROR;
                    return result;
                }

                if (budget.IdMoneda <= 0)
                {
                    result.Message = "El identificador de la moneda del presupuesto no puede ser 0";
                    result.DetailException = new Exception(result.Message);
                    result.MessageType = MessageType.ERROR;
                    return result;
                }

                if (budget.IdUsuarioIns <= 0)
                {
                    result.Message = "Se debe proporcionar el usuario que está ingresando el registro";
                    result.DetailException = new Exception(result.Message);
                    result.MessageType = MessageType.ERROR;
                    return result;
                }

                Repository.Add(budget);

                if (commit)
                    _unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorUpdateData, "presupuesto");
                result.MessageType = MessageType.ERROR;

                if (ex.GetType().Name == "DbEntityValidationException")
                    result.DetailException = new Exception(Utilities.GetEntityValidationErrors(ex as System.Data.Entity.Validation.DbEntityValidationException));
                else
                    result.DetailException = ex;
            }

            return result;
        }

        public ResultBase UpdateBudget(MstPresupuesto budget, bool commit = true)
        {
            ResultBase result = new ResultBase();

            try
            {
                // Se valida la información del registro de presupuesto
                if (budget.PresupuestoAsignado <= 0)
                {
                    result.Message = "El presupuesto asignado a la fase del proyecto debe ser mayor a 0";
                    result.DetailException = new Exception(result.Message);
                    result.MessageType = MessageType.ERROR;
                    return result;
                }

                if (budget.IdEntregable <= 0)
                {
                    result.Message = "El identificador de la fase del proyecto no puede ser 0";
                    result.DetailException = new Exception(result.Message);
                    result.MessageType = MessageType.ERROR;
                    return result;
                }

                if (budget.IdProyecto <= 0)
                {
                    result.Message = "El identificador del proyecto no puede ser 0";
                    result.DetailException = new Exception(result.Message);
                    result.MessageType = MessageType.ERROR;
                    return result;
                }

                if (budget.IdMoneda <= 0)
                {
                    result.Message = "El identificador de la moneda del presupuesto no puede ser 0";
                    result.DetailException = new Exception(result.Message);
                    result.MessageType = MessageType.ERROR;
                    return result;
                }

                if (!budget.IdUsuarioUpd.HasValue)
                {
                    result.Message = "Se debe proporcionar el usuario que está actualizando el registro";
                    result.DetailException = new Exception(result.Message);
                    result.MessageType = MessageType.ERROR;
                    return result;
                }

                if (!budget.FechaUpd.HasValue)
                {
                    result.Message = "Se debe proporcionar la fecha en la que está actualizando el registro";
                    result.DetailException = new Exception(result.Message);
                    result.MessageType = MessageType.ERROR;
                    return result;
                }

                Repository.Update(budget);

                if (commit)
                    _unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorUpdateData, "presupuesto");
                result.MessageType = MessageType.ERROR;

                if (ex.GetType().Name == "DbEntityValidationException")
                    result.DetailException = new Exception(Utilities.GetEntityValidationErrors(ex as System.Data.Entity.Validation.DbEntityValidationException));
                else
                    result.DetailException = ex;
            }

            return result;
        }

        public ResultBase DeleteBudget(int deliverableID, int projectID, bool commit = true)
        {
            ResultBase result = new ResultBase();

            try
            {
                if (projectID > 0)
                {
                    if (deliverableID > 0)
                    {
                        Repository.Delete(e => e.IdEntregable == deliverableID && e.IdProyecto == projectID);

                        if (commit)
                            _unitOfWork.Commit();
                    }
                    else
                    {
                        result.Message = "Se debe proporcionar el identificador de la fase de proyecto";
                        result.DetailException = new Exception(result.Message);
                        result.MessageType = MessageType.ERROR;
                        return result;
                    }
                }
                else
                {
                    result.Message = "Se debe proporcionar el identificador del proyecto";
                    result.DetailException = new Exception(result.Message);
                    result.MessageType = MessageType.ERROR;
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorDeleteData, "presupuesto");
                result.MessageType = MessageType.ERROR;

                if (ex.GetType().Name == "DbEntityValidationException")
                    result.DetailException = new Exception(Utilities.GetEntityValidationErrors(ex as System.Data.Entity.Validation.DbEntityValidationException));
                else
                    result.DetailException = ex;
            }

            return result;
        }

        public ResultBase InsertBudgetDatail(int deliverableID, int projectID, List<MstPresupuestoFinanciero> lstFinancialBudget,
                                        List<MstPresupuestoFisico> lstPhysicalBudget)
        {
            ResultBase result = new ResultBase();

            try
            {
                // Se valida el estado del proyecto
                var projectRep = _unitOfWork.GetRepository<MstProyectos>();
                // Se obtiene el registro de proyecto por el id proporcionado
                MstProyectos project = projectRep.GetById(projectID);
                if (project != null)
                {
                    // Se obtiene el detalle catálogo del estado del proyecto para verificarlo
                    var dcRepository = _unitOfWork.GetRepository<DetCatalogo>();
                    var detailCatalog = dcRepository.Get(p => p.IdDetCatalogo == project.IdEstado);

                    if (detailCatalog.Valor != Catalog.ProjectStatusInPlanning)
                    {
                        result.Message = "El proyecto debe estar en planificación para poder realizar esta acción";
                        result.DetailException = new Exception(result.Message);
                        result.MessageType = MessageType.ERROR;
                        return result;
                    }

                    // Se obtiene el registro de presupuesto
                    MstPresupuesto budget = Repository.Get(p => p.IdEntregable == deliverableID && p.IdProyecto == projectID);

                    if (budget != null)
                    {
                        bool save = false;
                        decimal budgetMount = 0;
                        bool thereArePhysicalBudget = false;
                        bool thereAreFinancialBudget = false;

                        if (lstFinancialBudget != null && lstFinancialBudget.Count > 0)
                            thereArePhysicalBudget = true;

                        if (lstPhysicalBudget != null && lstPhysicalBudget.Count > 0)
                            thereAreFinancialBudget = true;

                        if (thereArePhysicalBudget && thereAreFinancialBudget)
                        {
                            // Se valida la sumatoria de los presupuestos asignados a cada tarea
                            budgetMount = lstFinancialBudget.Sum(x => x.MontoPresupuestario);
                            // Se valida la sumatorio de los presupuesto asignados a cada tarea
                            budgetMount = budgetMount + lstPhysicalBudget.Sum(item => (item.CantidadArticulos * item.CostoArticulos));

                            if (budgetMount > budget.PresupuestoAsignado)
                            {
                                result.Message = "La sumatoria del presupuesto financiero y el presupuesto físico excede el presupuesto asignado al entregable";
                                result.DetailException = new Exception(result.Message);
                                result.MessageType = MessageType.ERROR;
                                return result;
                            } 
                        }
                        else if (!thereArePhysicalBudget && thereAreFinancialBudget)
                        {
                            // Se valida la sumatoria de los presupuestos asignados a cada tarea
                            budgetMount = lstFinancialBudget.Sum(x => x.MontoPresupuestario);
                            if (budgetMount > budget.PresupuestoAsignado)
                            {
                                result.Message = "La sumatoria del presupuesto financiero excede el presupuesto asignado al entregable";
                                result.DetailException = new Exception(result.Message);
                                result.MessageType = MessageType.ERROR;
                                return result;
                            }
                        }
                        else if (thereArePhysicalBudget && !thereAreFinancialBudget)
                        {
                            // Se valida la sumatorio de los presupuesto asignados a cada tarea
                            budgetMount = budgetMount + lstPhysicalBudget.Sum(item => (item.CantidadArticulos * item.CostoArticulos));
                            if (budgetMount > budget.PresupuestoAsignado)
                            {
                                result.Message = "La sumatoria del presupuesto físico excede el presupuesto asignado al entregable";
                                result.DetailException = new Exception(result.Message);
                                result.MessageType = MessageType.ERROR;
                                return result;
                            }
                        }


                        /*************** Presupuesto Financiero ***************/
                        if (thereArePhysicalBudget)
                        {
                            save = true;
                            var financialBudgetRep = _unitOfWork.GetRepository<MstPresupuestoFinanciero>();

                            foreach (var item in lstFinancialBudget)
                            {
                                // Se valida que el registro sea único (Tarea - Rubro)
                                if (lstFinancialBudget.Count(x => x.IdRubro == item.IdRubro && x.IdTarea == item.IdTarea &&
                                                             x.IdPresupuestoFinanciero == item.IdPresupuestoFinanciero) == 1)
                                {
                                    if (item.MontoPresupuestario > budget.PresupuestoAsignado)
                                    {
                                        result.Message = string.Format("El presupuesto financiero asignado a la tarea {0} excede el presupuesto asignado al entregable", item.IdTarea);
                                        result.DetailException = new Exception(result.Message);
                                        result.MessageType = MessageType.ERROR;
                                        return result;
                                    }
                                }
                                else
                                {
                                    result.Message = string.Format("A la tarea {0} ya se le asignó presupuesto por el rubro {1}", item.IdTarea, item.IdRubro);
                                    result.DetailException = new Exception(result.Message);
                                    result.MessageType = MessageType.ERROR;
                                    return result;
                                }

                                // insert
                                if (item.IdPresupuestoFinanciero == 0)
                                {
                                    item.Activo = true;
                                    item.FechaIns = DateTime.Now;
                                    financialBudgetRep.Add(item);
                                }
                                else
                                {
                                    var temp = financialBudgetRep.GetById(item.IdPresupuestoFinanciero);
                                    if (!item.Activo)
                                        financialBudgetRep.Delete(temp);
                                    else
                                    {
                                        temp.MontoPresupuestario = item.MontoPresupuestario;
                                        temp.FechaUpd = DateTime.Now;
                                        financialBudgetRep.Update(temp);
                                    }
                                }
                            }
                        }


                        /*************** Presupuesto Físico ***************/
                        if (thereAreFinancialBudget)
                        {
                            save = true;
                            var physicalBudgetRep = _unitOfWork.GetRepository<MstPresupuestoFisico>();
                            
                            foreach (var item in lstPhysicalBudget)
                            {
                                decimal tempBudget = (item.CantidadArticulos * item.CostoArticulos);

                                // Se valida que el registro sea único (Tarea - Rubro - Articulo)
                                if (lstPhysicalBudget.Count(x => x.IdRubro == item.IdRubro && x.IdTarea == item.IdTarea &&
                                                                 x.IdArticulo == item.IdArticulo && x.IdPresupuestoFisico == item.IdPresupuestoFisico) == 1)
                                {
                                    if (tempBudget > budget.PresupuestoAsignado)
                                    {
                                        result.Message = string.Format("El presupuesto físico asignado a la tarea {0} excede el presupuesto asignado al entregable", item.IdTarea);
                                        result.DetailException = new Exception(result.Message);
                                        result.MessageType = MessageType.ERROR;
                                        return result;
                                    }
                                }
                                else
                                {
                                    result.Message = string.Format("A la tarea {0} ya se le asignó presupuesto por el rubro {1} y el artículo {2}", item.IdTarea, item.IdRubro, item.IdArticulo);
                                    result.DetailException = new Exception(result.Message);
                                    result.MessageType = MessageType.ERROR;
                                    return result;
                                }

                                // insert
                                if (item.IdPresupuestoFisico == 0)
                                {
                                    item.Activo = true;
                                    item.FechaIns = DateTime.Now;
                                    physicalBudgetRep.Add(item);
                                }
                                else
                                {
                                    var temp = physicalBudgetRep.GetById(item.IdPresupuestoFisico);
                                    if (!item.Activo)
                                        physicalBudgetRep.Delete(temp);
                                    else
                                    {
                                        temp.CantidadArticulos = item.CantidadArticulos;
                                        temp.CostoArticulos = item.CostoArticulos;
                                        temp.FechaUpd = DateTime.Now;
                                        physicalBudgetRep.Update(temp);
                                    }
                                }
                            }
                        }

                        if (save)
                            _unitOfWork.Commit();
                    }
                    else
                    {
                        result.Message = "No se encontró registro de presupuesto vinculado al entregable";
                        result.DetailException = new Exception(result.Message);
                        result.MessageType = MessageType.ERROR;
                        return result;
                    }
                }
                else
                {
                    result.Message = "No se encontró registro de proyecto con identificador indicado";
                    result.DetailException = new Exception(result.Message);
                    result.MessageType = MessageType.ERROR;
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorUpdateData, "presupuesto");
                result.MessageType = MessageType.ERROR;

                if (ex.GetType().Name == "DbEntityValidationException")
                    result.DetailException = new Exception(Utilities.GetEntityValidationErrors(ex as System.Data.Entity.Validation.DbEntityValidationException));
                else
                    result.DetailException = ex;
            }

            return result;
        }
    }
}
