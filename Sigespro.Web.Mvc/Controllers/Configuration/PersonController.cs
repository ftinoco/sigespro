﻿using Common.Helpers;
using Common.Log;
using Newtonsoft.Json;
using Sigespro.Entity;
using Sigespro.Service.Admin;
using Sigespro.Service.Config;
using Sigespro.Service.Pro;
using Sigespro.Web.Mvc.Models.Administration;
using Sigespro.Web.Mvc.Models.Configuration;
using Sigespro.Web.Mvc.Resources;
using Sigespro.Web.Mvc.Utilities.Attributes.Security;
using Sigespro.Web.Mvc.Utilities.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace Sigespro.Web.Mvc.Controllers.Configuration
{
    [RoutePrefix("Configuration/Personal")]
    public class PersonController : Controller
    {
        private readonly IUserSession _userSession;

        public PersonalService Service { get; set; }
        
        public string UrlToRedirectUpdate
        {
            get { return Session["UrlToRedirectUpdate"].ToString(); }
            set { Session["UrlToRedirectUpdate"] = value; }
        }
        
        public PersonController()
        {
            Service = new PersonalService();
            _userSession = new UserSession();
        }

        [Route]
        public ActionResult Index(QueryOptions<IEnumerable<ListPersonalViewModel>> queryOpt)
        {
            string message = string.Empty;
            IEnumerable<ListPersonalViewModel> model = new List<ListPersonalViewModel>();

            try
            {
                string strSerialize = JsonConvert.SerializeObject(queryOpt);

                QueryOptions<IEnumerable<vwListadoPersonal>> filter = JsonConvert.DeserializeObject<QueryOptions<IEnumerable<vwListadoPersonal>>>(strSerialize);
                Service.GetPersonalForList(ref filter);

                if (filter.ResultData.MessageType != MessageType.INFO)
                {
                    message = filter.ResultData.Message;
                    Logger.Log(filter.ResultData.MessageType, filter.ResultData.DetailException, _userSession.Username,
                               MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                {
                    strSerialize = JsonConvert.SerializeObject(filter);

                    queryOpt = JsonConvert.DeserializeObject<QueryOptions<IEnumerable<ListPersonalViewModel>>>(strSerialize);

                    model = queryOpt.ResultData.Data;
                }
            }
            catch (Exception ex)
            {
                message = Messages.LoadPageError;
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            if (!string.IsNullOrEmpty(message))
                ModelState.AddModelError(Messages.AlertDanger, message);

            ViewBag.Filter = true;
            ViewBag.QueryOptions = queryOpt;

            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [Route("Add")]
        public ActionResult AddPerson()
        {
            PersonalViewModel model = new PersonalViewModel();

            ViewBag.IsEdit = false;
            ViewBag.Action = "SavePerson";
            ViewBag.Title = "Agregar Personal";

            model.MstPersonas = new PersonViewModel();
            model.MstPersonas.SexoPersona = "M";

            model.MstPersonas.DetPersonaDireccion = new List<PersonAddressViewModel>();
            model.MstPersonas.DetPersonaDireccion.Add(new PersonAddressViewModel());

            model.MstPersonas.DetPersonaTelefono = new List<PersonPhoneViewModel>();
            model.MstPersonas.DetPersonaTelefono.Add(new PersonPhoneViewModel());

            model.MstPersonas.DetPersonaEmail = new List<PersonEmailViewModel>();
            model.MstPersonas.DetPersonaEmail.Add(new PersonEmailViewModel());

            return AddEditPerson(model);
        }

        [Route("Edit/{personalId}")]
        public ActionResult EditPerson(int personalId)
        {
            var model = GetPersonalForEdit(personalId);
               
            ViewBag.IsEdit = true;
            ViewBag.Action = "UpdatePerson";
            ViewBag.Title = "Editar Personal";

            UrlToRedirectUpdate = Url.Action("Index", "Person");

            return AddEditPerson(model);
        }

        [NonAction]
        private ActionResult AddEditPerson(PersonalViewModel model)
        {
            string message = string.Empty;

            try
            {
                ProgramService programService = new ProgramService();

                // Se carga el listado de programas
                var resultPrograms = programService.GetPrograms("activo");

                List<SelectListItem> selectList = new List<SelectListItem>();

                selectList.Add(new SelectListItem { Value = "0", Text = Labels.Select });

                resultPrograms.Data.ToList().ForEach(c => selectList.Add(
                    new SelectListItem
                    {
                        Value = c.IdPrograma.ToString(),
                        Text = string.Join(" - ", c.NombrePrograma, c.Descripcion)
                    }
                ));

                ViewBag.Programs = new SelectList(selectList, "Value", "Text");

                DetailCatalogService dtService = new DetailCatalogService();

                // Se carga el listado de cargos
                var resultDegrees = dtService.GetDetailsByTable("CargosProgramas");

                List<SelectListItem> selectListItem = new List<SelectListItem>();

                selectListItem.Add(new SelectListItem { Value = "0", Text = Labels.Select });

                resultDegrees.Data.ToList().ForEach(c => selectListItem.Add(
                    new SelectListItem
                    {
                        Value = c.IdDetCatalogo.ToString(),
                        Text = c.Valor
                    }
                ));

                ViewBag.Degrees = new SelectList(selectListItem, "Value", "Text");

            }
            catch (Exception ex)
            {
                message = Messages.ErrorApi;
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                           MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            if (!string.IsNullOrEmpty(message))
                ModelState.AddModelError(Messages.AlertDanger, message);

            return View("AddPerson", model);
        }

        #region Otras Acciones 

        private PersonalViewModel GetPersonalForEdit(int personalId)
        {
            PersonalViewModel model = new PersonalViewModel();

            try
            {
                var resultPersonal = Service.GetPersonalById(personalId);

                if (resultPersonal.MessageType == MessageType.ERROR)
                {
                    ModelState.AddModelError(Messages.AlertDanger, resultPersonal.Message);
                    Logger.Log(resultPersonal.MessageType, resultPersonal.DetailException, _userSession.Username,
                               MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                {
                    model = new PersonalViewModel
                    {
                        IdCargo = resultPersonal.Data.IdCargo,
                        IdInstitucion = resultPersonal.Data.IdInstitucion,
                        IdPersona = resultPersonal.Data.IdPersona,
                        IdPersonal = resultPersonal.Data.IdPersonal,
                        IdPrograma = resultPersonal.Data.IdPrograma,
                        MstPersonas = new PersonViewModel
                        {
                            IdPersona = resultPersonal.Data.IdPersona,
                            Activo = resultPersonal.Data.MstPersonas.Activo,
                            FechaIns = resultPersonal.Data.MstPersonas.FechaIns,
                            FechaUpd = resultPersonal.Data.MstPersonas.FechaUpd,
                            Identificacion = resultPersonal.Data.MstPersonas.Identificacion,
                            IdUsuarioIns = resultPersonal.Data.MstPersonas.IdUsuarioIns,
                            IdUsuarioUpd = resultPersonal.Data.MstPersonas.IdUsuarioUpd,
                            NombreCompleto = resultPersonal.Data.MstPersonas.NombreCompleto,
                            SexoPersona = resultPersonal.Data.MstPersonas.SexoPersona
                        }
                    };

                    model.MstPersonas.DetPersonaDireccion = new List<PersonAddressViewModel>();
                    foreach (var address in resultPersonal.Data.MstPersonas.DetPersonaDireccion)
                    {
                        model.MstPersonas.DetPersonaDireccion.Add(new PersonAddressViewModel
                        {
                            Activo = address.Activo,
                            Direccion = address.Direccion,
                            FechaIns = address.FechaIns,
                            FechaUpd = address.FechaUpd,
                            IdPersona = address.IdPersona,
                            IdPersonaDireccion = address.IdPersonaDireccion,
                            IdTipoDireccion = address.IdTipoDireccion,
                            IdUsuarioUpd = address.IdUsuarioUpd,
                            IdUsuarioIns = address.IdUsuarioIns
                        });
                    }

                    model.MstPersonas.DetPersonaEmail = new List<PersonEmailViewModel>();
                    foreach (var email in resultPersonal.Data.MstPersonas.DetPersonaEmail)
                    {
                        model.MstPersonas.DetPersonaEmail.Add(new PersonEmailViewModel
                        {
                            Activo = email.Activo,
                            FechaIns = email.FechaIns,
                            FechaUpd = email.FechaUpd,
                            IdPersona = email.IdPersona,
                            IdUsuarioUpd = email.IdUsuarioUpd,
                            IdUsuarioIns = email.IdUsuarioIns,
                            Email = email.Email,
                            IdPersonaEmail = email.IdPersonaEmail
                        });
                    }

                    model.MstPersonas.DetPersonaTelefono = new List<PersonPhoneViewModel>();
                    foreach (var phone in resultPersonal.Data.MstPersonas.DetPersonaTelefono)
                    {
                        model.MstPersonas.DetPersonaTelefono.Add(new PersonPhoneViewModel
                        {
                            Activo = phone.Activo,
                            FechaIns = phone.FechaIns,
                            FechaUpd = phone.FechaUpd,
                            IdPersona = phone.IdPersona,
                            IdUsuarioUpd = phone.IdUsuarioUpd,
                            IdUsuarioIns = phone.IdUsuarioIns,
                            IdPersonaTelefono = phone.IdPersonaTelefono,
                            IdTipoNumero = phone.IdTipoNumero,
                            NumeroTelefono = phone.NumeroTelefono
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(Messages.AlertDanger, Messages.ErrorApi);
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return model;
        }

        [AllowAnonymous]
        public ActionResult ListAddress(PersonalViewModel model)
        {
            string message = string.Empty;

            try
            {
                ModelState.Clear();
                List<DetCatalogViewModel> addressTypeDb = new List<DetCatalogViewModel>();
                List<DetCatalogViewModel> addressTypeSelected = new List<DetCatalogViewModel>();

                DetailCatalogService dtService = new DetailCatalogService();

                var result = dtService.GetDetailsByTable("TipoDireccion");

                if (result.MessageType != MessageType.INFO)
                    Logger.Log(MessageType.ERROR, result.DetailException, _userSession.Username,
                               MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                else
                {
                    // Se carga el listado de programas 
                    result.Data.ToList().ForEach(p => addressTypeDb.Add(new DetCatalogViewModel
                    {
                        Activo = p.Activo,
                        Descripcion = p.Descripcion,
                        FechaIns = p.FechaIns,
                        FechaUpd = p.FechaUpd,
                        IdDetCatalogo = p.IdDetCatalogo,
                        IdMstCatalogo = p.IdMstCatalogo,
                        IdUsuarioIns = p.IdUsuarioIns,
                        IdUsuarioUpd = p.IdUsuarioUpd,
                        Valor = p.Valor
                    }));

                    foreach (var item in model.MstPersonas.DetPersonaDireccion)
                    {
                        if (item.IdTipoDireccion > 0)
                        {
                            // Se agrega a las lista de tipo de dirección seleccionado
                            if (!addressTypeSelected.Exists(d => d.IdDetCatalogo == item.IdTipoDireccion))
                                addressTypeSelected.Add(addressTypeDb.SingleOrDefault(a => a.IdDetCatalogo == item.IdTipoDireccion));

                            // Si existe mas de un tipo de dirección seleccionada del mismo tipo
                            if (model.MstPersonas.DetPersonaDireccion.Count(d => d.IdTipoDireccion == item.IdTipoDireccion) > 1)
                            {
                                // Se establece en 0 el tipo de dirección los demás registros
                                for (int i = 0; i < model.MstPersonas.DetPersonaDireccion.Count; i++)
                                {
                                    if (i > 0)
                                        model.MstPersonas.DetPersonaDireccion[i].IdTipoDireccion = 0;
                                }
                            }
                        }
                    }

                    // Si la cantidad de tipos de dirección es igual a la cantidad de dirección 
                    // se inhabilita el botón de agregar
                    if (model.MstPersonas.DetPersonaDireccion.Count == addressTypeDb.Count)
                        ViewBag.Disabled = true;
                    else
                        ViewBag.Disabled = false;
                }

                ViewBag.AddressType = addressTypeDb;
                ViewBag.AddressTypeSelected = addressTypeSelected;
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                           MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return PartialView("Partials/Person/_addresses", model);
        }

        [AllowAnonymous]
        public ActionResult AddAddress(PersonalViewModel model, bool changeTypeAddress)
        {
            ModelState.Clear();
            if (model.MstPersonas.DetPersonaDireccion.All(d => d.IdTipoDireccion > 0))
            {
                if (!changeTypeAddress)
                    model.MstPersonas.DetPersonaDireccion.Add(new PersonAddressViewModel
                    {
                        Activo = true,
                        IdPersona = model.MstPersonas.IdPersona
                    });
            }

            return ListAddress(model);
        }

        [AllowAnonymous]
        public ActionResult DeleteAddress(PersonalViewModel model, int addressTypeId)
        {
            ModelState.Clear();
            //var personAddress = model.MstPersonas.DetPersonaDireccion.FirstOrDefault(a => a.IdTipoDireccion == addressTypeId);

            //if (personAddress != null)
            //    personAddress.Activo = false;
            model.MstPersonas.DetPersonaDireccion.RemoveAll(a => a.IdTipoDireccion == addressTypeId);
            return ListAddress(model);
        }


        [AllowAnonymous]
        public ActionResult ListPhone(PersonalViewModel model)
        {
            try
            {
                ModelState.Clear();
                List<DetCatalogViewModel> phoneTypeDb = new List<DetCatalogViewModel>();
                List<DetCatalogViewModel> phoneTypeSelected = new List<DetCatalogViewModel>();

                DetailCatalogService dtService = new DetailCatalogService();

                var result = dtService.GetDetailsByTable("TipoTelefono");

                if (result.MessageType != MessageType.INFO)
                    Logger.Log(MessageType.ERROR, result.DetailException, _userSession.Username,
                               MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                else
                {
                    // Se carga el listado de programas 
                    result.Data.ToList().ForEach(p => phoneTypeDb.Add(new DetCatalogViewModel
                    {
                        Activo = p.Activo,
                        Descripcion = p.Descripcion,
                        FechaIns = p.FechaIns,
                        FechaUpd = p.FechaUpd,
                        IdDetCatalogo = p.IdDetCatalogo,
                        IdMstCatalogo = p.IdMstCatalogo,
                        IdUsuarioIns = p.IdUsuarioIns,
                        IdUsuarioUpd = p.IdUsuarioUpd,
                        Valor = p.Valor
                    }));

                    foreach (var item in model.MstPersonas.DetPersonaTelefono)
                    {
                        if (item.IdTipoNumero > 0)
                        {
                            // Se agrega a las lista de tipo de teléfono seleccionado
                            if (!phoneTypeSelected.Exists(d => d.IdDetCatalogo == item.IdTipoNumero))
                                phoneTypeSelected.Add(phoneTypeDb.SingleOrDefault(a => a.IdDetCatalogo == item.IdTipoNumero));

                            // Si existe mas de un tipo de teléfono seleccionada del mismo tipo
                            if (model.MstPersonas.DetPersonaTelefono.Count(d => d.IdTipoNumero == item.IdTipoNumero) > 1)
                            {
                                // Se establece en 0 el tipo de teléfono los demás registros
                                for (int i = 0; i < model.MstPersonas.DetPersonaTelefono.Count; i++)
                                {
                                    if (i > 0)
                                        model.MstPersonas.DetPersonaTelefono[i].IdTipoNumero = 0;
                                }
                            }
                        }
                    }

                    // Si la cantidad de tipos de teléfono es igual a la cantidad de teléfono 
                    // se inhabilita el botón de agregar
                    if (model.MstPersonas.DetPersonaTelefono.Count == phoneTypeDb.Count)
                        ViewBag.Disabled = true;
                    else
                        ViewBag.Disabled = false;
                }

                //List<SelectListItem> selectListItem = new List<SelectListItem>();

                //selectListItem.Add(new SelectListItem { Value = "0", Text = Labels.Select });

                ////
                //foreach (var item in phoneTypeDb)
                //{
                //    bool selected = false;
                //    if (phoneTypeSelected.Any(p => p.IdDetCatalogo == item.IdDetCatalogo))
                //        selected = true;

                //    selectListItem.Add(new SelectListItem
                //    {
                //        Value = item.IdDetCatalogo.ToString(),
                //        Text = item.Valor,
                //        Selected = selected
                //    });
                //}

                //ViewBag.PhoneType = new SelectList(selectListItem, "Value", "Text");

                ViewBag.PhoneType = phoneTypeDb;
                ViewBag.PhoneTypeSelected = phoneTypeSelected;
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                           MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            //ViewBag.PhonePersonCount = model.MstPersonas.DetPersonaTelefono.Count;

            return PartialView("Partials/Person/_phones", model);
        }

        [AllowAnonymous]
        public ActionResult AddPhone(PersonalViewModel model, bool changeTypePhone)
        {
            ModelState.Clear();
            if (model.MstPersonas.DetPersonaTelefono.All(d => d.IdTipoNumero > 0))
            {
                if (!changeTypePhone)
                {
                    model.MstPersonas.DetPersonaTelefono.Add(new PersonPhoneViewModel {
                        Activo = true,
                        IdPersona = model.MstPersonas.IdPersona
                    });
                }
            }

            return ListPhone(model);
        }

        [AllowAnonymous]
        public ActionResult DeletePhone(PersonalViewModel model, int phoneTypeId)
        {
            ModelState.Clear();
            //var personPhone = model.MstPersonas.DetPersonaTelefono.FirstOrDefault(a => a.IdTipoNumero == phoneTypeId);

            //if (personPhone != null)
            //    personPhone.Activo = false;
            model.MstPersonas.DetPersonaTelefono.RemoveAll(a => a.IdTipoNumero == phoneTypeId);
            return ListPhone(model);
        }


        [AllowAnonymous]
        public ActionResult ListEmail(PersonalViewModel model)
        {
            ModelState.Clear();

            return PartialView("Partials/Person/_emails", model);
        }

        [AllowAnonymous]
        public ActionResult AddEmail(PersonalViewModel model)
        {
            ModelState.Clear();

            if (model.MstPersonas.DetPersonaEmail.All(d => !string.IsNullOrEmpty(d.Email)))
                model.MstPersonas.DetPersonaEmail.Add(new PersonEmailViewModel
                {
                    Activo = true,
                    IdPersona = model.MstPersonas.IdPersona
                });

            return ListEmail(model);
        }

        [AllowAnonymous]
        public ActionResult DeleteEmail(PersonalViewModel model, string email)
        {
            ModelState.Clear();
            //var personEmail =  model.MstPersonas.DetPersonaEmail.FirstOrDefault(a => a.Email == email);

            //if (personEmail != null)
            //    personEmail.Activo = false;

            var personEmail = model.MstPersonas.DetPersonaEmail.RemoveAll(a => a.Email == email);
            return ListEmail(model);
        }

        #endregion

        [AjaxOnly]
        [HttpPost]
        [Route("Save")]
        [AllowAnonymous]
        public ActionResult SavePerson(PersonalViewModel person)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                if (ModelState.IsValid)
                {
                    person.MstPersonas.IdUsuarioIns = _userSession.UserId;

                    string strSerialize = JsonConvert.SerializeObject(person);

                    var result = Service.InsertPersonal(JsonConvert.DeserializeObject<DetPersonal>(strSerialize));

                    if (result.MessageType != MessageType.SUCCESS)
                    {
                        message = result.Message;
                        status = result.MessageType.ToString();
                        Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                                MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                    }
                    else
                        message = string.Format(Messages.SaveSuccess, "persona");
                }
                else
                {
                    status = "ERROR";
                    message = Messages.DataRequired;

                    // Se validan las direcciones proporcionada
                    if (person.MstPersonas.DetPersonaDireccion.Count == 0)
                        message = "Debe proporcionar al menos una dirección";
                    else
                    {
                        foreach (var item in person.MstPersonas.DetPersonaDireccion)
                        {
                            if (item.IdTipoDireccion == 0)
                            {
                                message = string.Format("Debe proporcionar el tipo de dirección para la siguente dirección: « {0} »", item.Direccion);
                                return new JsonResult
                                {
                                    Data = new { status, message },
                                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                                };
                            }
                        }
                    }

                    // Se validan los números telefónicos proporcionados
                    if (person.MstPersonas.DetPersonaTelefono.Count == 0)
                        message = "Debe proporcionar al menos un número telefónico";
                    else
                    {
                        foreach (var item in person.MstPersonas.DetPersonaTelefono)
                        {
                            if (item.IdTipoNumero == 0)
                            {
                                message = string.Format("Debe proporcionar el tipo de teléfono para el siguente número: « {0} »", item.NumeroTelefono);
                                return new JsonResult
                                {
                                    Data = new { status, message },
                                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                                };
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message, action = Url.Action("Index", "Person") },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        [AjaxOnly]
        [AllowAnonymous]
        [Route("Update")]
        public ActionResult UpdatePerson(PersonalViewModel model)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                model.MstPersonas.IdUsuarioUpd = _userSession.UserId; 
                string strSerialize = JsonConvert.SerializeObject(model);

                var result = Service.UpdatePersonal(JsonConvert.DeserializeObject<DetPersonal>(strSerialize));

                if (result.MessageType != MessageType.SUCCESS)
                {
                    message = result.Message;
                    status = result.MessageType.ToString();
                    Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                    message = string.Format(Messages.UpdateSuccess, "persona");
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                             MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message, action = UrlToRedirectUpdate },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        #region Perfil de Usuario

        [AllowAnonymous]
        [ChildActionOnly]
        public ActionResult UserProfile()
        {

            ViewBag.UserName = _userSession.FullName.Split(' ')[0];
            
            var model = GetPersonalForEdit(_userSession.PersonalId);

            if (model != null)
            {
                string message = string.Empty;

                try
                {
                    ProgramService programService = new ProgramService();

                    // Se carga el listado de programas
                    var resultPrograms = programService.GetPrograms("activo");

                    List<SelectListItem> selectList = new List<SelectListItem>();

                    selectList.Add(new SelectListItem { Value = "0", Text = Labels.Select });

                    resultPrograms.Data.ToList().ForEach(c => selectList.Add(
                        new SelectListItem
                        {
                            Value = c.IdPrograma.ToString(),
                            Text = string.Join(" - ", c.NombrePrograma, c.Descripcion)
                        }
                    ));

                    ViewBag.Programs = new SelectList(selectList, "Value", "Text");

                    DetailCatalogService dtService = new DetailCatalogService();

                    // Se carga el listado de cargos
                    var resultDegrees = dtService.GetDetailsByTable("CargosProgramas");

                    List<SelectListItem> selectListItem = new List<SelectListItem>();

                    selectListItem.Add(new SelectListItem { Value = "0", Text = Labels.Select });

                    resultDegrees.Data.ToList().ForEach(c => selectListItem.Add(
                        new SelectListItem
                        {
                            Value = c.IdDetCatalogo.ToString(),
                            Text = c.Valor
                        }
                    ));

                    ViewBag.Degrees = new SelectList(selectListItem, "Value", "Text");

                }
                catch (Exception ex)
                {
                    message = Messages.ErrorApi;
                    Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                               MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }

                if (!string.IsNullOrEmpty(message))
                    ModelState.AddModelError(Messages.AlertDanger, message);
            }

            ViewBag.Action = "UpdatePerson";
            UrlToRedirectUpdate = Url.Action("UserProfile", "Account");

            return View(model);
        }

        #endregion

    }
}
