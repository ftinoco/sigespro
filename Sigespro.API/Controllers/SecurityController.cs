﻿using Common.Extensions;
using Common.Helpers;
using Common.Log;
using Sigespro.Entity;
using Sigespro.Service;
using Sigespro.Service.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Sigespro.API.Controllers
{
    [RoutePrefix("Api/Security")]
    public class SecurityController : ApiController
    {
        SecurityService _service;

        public SecurityController(Func<string, IService> serviceFactory)
        {
            _service = (SecurityService)serviceFactory("Security");
        }

        [HttpGet]
        [Route("Get/MenuItems/{username}")]
        public List<vwMenuItems> GetMenuItemByUser(string username)
        {
            var result = _service.GetMenuItemByUser(username);

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data;
        }

        [HttpGet]
        //[Route("Get/Options")]
        public List<vwOptionsList> GetOptionListByUser()
        {
            var allUrlKeyValues = ControllerContext.Request.GetQueryNameValuePairs();

            string url = "";
            string username = allUrlKeyValues.FirstOrDefault(q => q.Key.CompareWith("UserName")).Value;
            string actionName = allUrlKeyValues.FirstOrDefault(q => q.Key.CompareWith("ActionName")).Value;
            string controllerName = allUrlKeyValues.FirstOrDefault(q => q.Key.CompareWith("ControllerName")).Value;

            var result = _service.GetOptionListByUser(username, actionName, controllerName, url);

            if (result.MessageType == MessageType.ERROR)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data.ToList();
        }

    }
}
