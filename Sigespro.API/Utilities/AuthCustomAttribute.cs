﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Sigespro.API.Utilities
{
    public class AuthCustomAttribute: AuthorizeAttribute
    {
        public AuthCustomAttribute()
        {
        }
         
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            base.OnAuthorization(actionContext);

            var skipAuthorization = actionContext.ActionDescriptor;
            
        }

    }
}