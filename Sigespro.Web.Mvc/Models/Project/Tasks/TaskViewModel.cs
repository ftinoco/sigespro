﻿using Sigespro.Web.Mvc.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sigespro.Web.Mvc.Models.Project.Tasks
{
    public class TaskViewModel
    {
        public int IdTarea { get; set; }

        public int IdEntregable { get; set; }

        public int IdProyecto { get; set; }

        public int? IdTareaParent { get; set; }

        [Display(Name = "Nombre Tarea")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string Nombre { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "StartDate", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public DateTime FechaInicio { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "EndDate", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public DateTime FechaFin { get; set; }

        public decimal DuracionEstimadaDias { get; set; }

        public decimal DuracionEstimadaHoras { get; set; }

        public decimal HorasDedicadas { get; set; }

        public bool Hito { get; set; }
         
        public bool Timeline { get; set; }

        public bool Provisional { get; set; }

        public bool Revisar { get; set; }

        public int? IdUsuarioRevisor { get; set; }

        public bool ReportarAvances { get; set; }

        public int FecuenciaAvance { get; set; }

        public bool TareaPeriodica { get; set; }

        public int IdEstado { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Observations", ResourceType = typeof(Labels))]
        public string Nota { get; set; }

        //[Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        private decimal myVar;

        public decimal Presupuesto
        {
            get { return myVar; }
            set { myVar = value; }
        }


        //public decimal Presupuesto { get; set; }

        public int IdUsuarioIns { get; set; }

        public DateTime FechaIns { get; set; }

        public int? IdUsuarioUpd { get; set; }

        public DateTime? FechaUpd { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public int Responsable { get; set; }

        public List<string> Colaboradores { get; set; }

        public ConfigFrequencyViewModel DetConfiguracionTareaFrecuencia { get; set; }
    }
}