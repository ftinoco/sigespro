﻿using Common.Extensions;
using Common.Helpers;

using Sigespro.DAL.Implementations;
using Sigespro.Entity;
using Sigespro.Entity.Resource;
using Sigespro.Service.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using static Common.Helpers.Utilities;

namespace Sigespro.Service.Pre
{
    public class CurrencyService : Service
    {
        private Repository<CatMoneda> Repository { get; set; }

        public CurrencyService() : base()
        {
            //UnitOfWork = new UnitOfWork();
            Repository = _unitOfWork.GetRepository<CatMoneda>();

            //var sequenceRepository = UnitOfWork.GetRepository<MstSecuencias>();

            //SequencesService = new SequenceService(sequenceRepository);

            //Result = new string[] { "0", "Correcto" };
        }

        public Result<IEnumerable<CatMoneda>> GetCurrencies()
        {
            Result<IEnumerable<CatMoneda>> result = new Result<IEnumerable<CatMoneda>>();

            try
            {
                result.Data = Repository.GetAll();
                result.MessageType = MessageType.INFO;
                result.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<CatMoneda> GetCurrencyById(int idCurrency)
        {
            Result<CatMoneda> result = new Result<CatMoneda>();

            try
            {
                result.Data = Repository.GetById(idCurrency);
                result.Message = Messages.SuccessGetResult;
                result.MessageType = MessageType.INFO;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<IEnumerable<CatMoneda>> GetCurrencies(string filter)
        {
            Result<IEnumerable<CatMoneda>> result = new Result<IEnumerable<CatMoneda>>();

            try
            {
                bool isLocal = false;
                bool active = false;

                string filterToLower = filter.Trim().ToLower();

                if (filterToLower == "activo" || filterToLower == "inactivo")
                {
                    active = filterToLower == "activo" ? true : false;
                    result.Data = Repository.GetMany(c => c.Activo == active);
                }
                else if (filterToLower == "local" || filterToLower == "extranjera")
                {
                    isLocal = filterToLower == "local" ? true : false;
                    result.Data = Repository.GetMany(c => c.EsLocal == isLocal);
                }
                else
                    result.Data = Repository.GetMany(c => c.Nombre.Trim().ToLower().Contains(filterToLower) ||
                                                   c.Simbolo.ToLower().Trim().Contains(filterToLower));

                result.MessageType = MessageType.INFO;
                result.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public void GetCurrenciesForList(ref QueryOptions<IEnumerable<CatMoneda>> queryOpt)
        {
            Result<IEnumerable<CatMoneda>> result = new Result<IEnumerable<CatMoneda>>();

            try
            {
                string orderBy = "Nombre";

                var predicate = PredicateBuilder.True<CatMoneda>();

                var filter = queryOpt.Filters.FirstOrDefault(f => f.Name.Equals("Search"));

                if (!IsNull(filter) && !string.IsNullOrEmpty(filter.Value))
                {
                    int intFilter;

                    if (filter.Value.CompareWith(Labels.ForeignCurrency))
                        predicate = predicate.And(p => !p.EsLocal);
                    else if (filter.Value.CompareWith(Labels.LocalCurrency))
                        predicate = predicate.And(p => p.EsLocal);

                    predicate = predicate.And(p => p.Nombre.ToLower().Trim().Contains(filter.Value.ToLower().Trim()) || 
                                                   p.Simbolo.ToLower().Trim().Contains(filter.Value.ToLower().Trim()));
                    
                    if (int.TryParse(filter.Value, out intFilter))
                        predicate = predicate.Or(p => p.IdMoneda == intFilter);
                    
                    queryOpt.TotalRecords = _unitOfWork.GetRepository<CatMoneda>().Count(predicate);
                }
                else
                    queryOpt.TotalRecords = _unitOfWork.GetRepository<CatMoneda>().Count();

                if (!string.IsNullOrEmpty(queryOpt.SortField))
                    orderBy = queryOpt.SortField;

                result.Data = _unitOfWork.GetRepository<CatMoneda>().GetManyOnDemand(predicate, orderBy, queryOpt.CurrentPageIndex, queryOpt.RowsPerPage, queryOpt.SortOrder);

                result.Message = Messages.SuccessGetResultSet;

                result.MessageType = MessageType.INFO;
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorGetResultSet, "monedas");
                result.MessageType = MessageType.ERROR;
                result.DetailException = ex;
            }

            queryOpt.ResultData = result;
        }

        public ResultBase InsertCurrency(CatMoneda currency)
        {
            ResultBase result = new ResultBase();

            try
            {
                currency.FechaIns = DateTime.Now;

                if (currency.IdMoneda == 0)
                {
                    var resultValidate = Utilities.ValidateEntity(currency);

                    if (resultValidate.MessageType == MessageType.WARNING)
                        result = resultValidate;
                    else
                    {
                        var sequencesService = new SequenceService(_unitOfWork.GetRepository<MstSecuencias>());
                        MstSecuencias seq = sequencesService.GetNextSequenceByTableName("[pre].[CatMoneda]");

                        currency.IdMoneda = seq.SecuencialTabla;
                        Repository.Add(currency);

                        sequencesService.InsertSequence(seq);

                        _unitOfWork.Commit();
                    }
                }

                result.Message = Messages.SuccessSaveData;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorSaveData, "moneda");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase UpdateCurrency(CatMoneda currency)
        {
            ResultBase result = new ResultBase();

            try
            {
                var temp = Repository.Get(c => c.IdMoneda == currency.IdMoneda);

                if (temp == null)
                {
                    result.MessageType = MessageType.WARNING;
                    result.Message = "El registro que intenta actualizar no existe.";
                    result.DetailException = new Exception(result.Message);
                }
                else
                {
                    var resultValidate = Utilities.ValidateEntity(currency);

                    if (resultValidate.MessageType == MessageType.WARNING)
                        result = resultValidate;
                    else
                    {
                        currency.FechaUpd = DateTime.Now;

                        Repository.Update(currency);

                        _unitOfWork.Commit();
                    }
                }

                result.Message = Messages.SuccessUpdateData;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorUpdateData, "moneda");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

    }
}
