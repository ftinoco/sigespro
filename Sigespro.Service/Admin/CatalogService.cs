﻿using Common.Extensions;
using Common.Helpers;
using Sigespro.DAL.Implementations;
using Sigespro.Entity;
using Sigespro.Entity.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using static Common.Helpers.Utilities;

namespace Sigespro.Service.Admin
{
    public interface ICatalogService : IDisposable
    {
        ResultBase UpdateCatalog(MstCatalogo catalog);
        ResultBase InsertCatalog(MstCatalogo catalog);
        Result<IEnumerable<MstCatalogo>> GetCatalogs();
        Result<MstCatalogo> GetCatalogById(int catalogId);
        Result<IEnumerable<MstCatalogo>> GetCatalogs(string filter);
        void GetCatalogsForList(ref QueryOptions<IEnumerable<MstCatalogo>> queryOpt);
    }

    public class CatalogService : ICatalogService
    {
        protected UnitOfWork _unitOfWork;

        public CatalogService()
        {
            _unitOfWork = new UnitOfWork();
        }

        public Result<IEnumerable<MstCatalogo>> GetCatalogs()
        {
            Result<IEnumerable<MstCatalogo>> result = new Result<IEnumerable<MstCatalogo>>();

            try
            {
                result.Data = _unitOfWork.GetRepository<MstCatalogo>().GetAll();

                result.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public void GetCatalogsForList(ref QueryOptions<IEnumerable<MstCatalogo>> queryOpt)
        {
            Result<IEnumerable<MstCatalogo>> result = new Result<IEnumerable<MstCatalogo>>();

            try
            {
                string orderBy = "Nombre";

                var predicate = PredicateBuilder.True<MstCatalogo>();

                var filter = queryOpt.Filters.FirstOrDefault(f => f.Name.Equals("Search"));

                if (!IsNull(filter) && !string.IsNullOrEmpty(filter.Value))
                {
                    int intFilter;

                    predicate = predicate.And(p => p.Nombre.ToLower().Trim().Contains(filter.Value.ToLower().Trim()) || 
                                                   p.Descripcion.ToLower().Trim().Contains(filter.Value.ToLower().Trim()));

                    if (int.TryParse(filter.Value, out intFilter))
                    {
                        predicate = predicate.Or(p => p.IdCatalogo == intFilter);
                    }

                    queryOpt.TotalRecords = _unitOfWork.GetRepository<MstCatalogo>().Count(predicate);
                }
                else
                    queryOpt.TotalRecords = _unitOfWork.GetRepository<MstCatalogo>().Count();

                if (!string.IsNullOrEmpty(queryOpt.SortField))
                    orderBy = queryOpt.SortField;

                result.Data = _unitOfWork.GetRepository<MstCatalogo>().GetManyOnDemand(predicate, orderBy, queryOpt.CurrentPageIndex, queryOpt.RowsPerPage, queryOpt.SortOrder);

                result.Message = Messages.SuccessGetResultSet;

                result.MessageType = MessageType.INFO;
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorGetResultSet, "registro de catálogos");
                result.MessageType = MessageType.ERROR;
                result.DetailException = ex;
            }

            queryOpt.ResultData = result;
        }

        public Result<MstCatalogo> GetCatalogById(int catalogId)
        {
            Result<MstCatalogo> result = new Result<MstCatalogo>();

            try
            {
                result.Data = _unitOfWork.GetRepository<MstCatalogo>().GetById(catalogId);
                result.Message = Messages.SuccessGetResult;
                result.MessageType = MessageType.INFO;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorGetResult, "registro de catálogo");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<IEnumerable<MstCatalogo>> GetCatalogs(string filter)
        {
            Result<IEnumerable<MstCatalogo>> result = new Result<IEnumerable<MstCatalogo>>();

            try
            {
                string filterToLower = filter.Trim().ToLower();

                var repository = _unitOfWork.GetRepository<MstCatalogo>();

                if (filterToLower == "activo" || filterToLower == "inactivo")
                {
                    bool active = filterToLower == "activo" ? true : false;

                    result.Data = repository.GetMany(c => c.Activo == active).OrderBy(c => c.Nombre);
                }
                else
                    result.Data = repository.GetMany(c => c.Descripcion.Trim().ToLower().Contains(filterToLower) ||
                                                    c.Nombre.Trim().ToLower().Contains(filterToLower))
                                      .OrderBy(c => c.Nombre);

                result.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase InsertCatalog(MstCatalogo catalog)
        {
            ResultBase result = new ResultBase();

            try
            {
                var sequencesService = new SequenceService(_unitOfWork.GetRepository<MstSecuencias>());

                if (catalog.IdCatalogo == 0)
                {
                    if (string.IsNullOrEmpty(catalog.Nombre))
                    {
                        result.MessageType = MessageType.INFO;
                        result.DetailException = new Exception(result.Message);
                        result.Message = string.Format(Messages.Required, "Nombre Catálogo");
                    }
                    else
                    {
                        catalog.FechaIns = DateTime.Now;
                        //catalog.IdUsuarioIns = 1;

                        MstSecuencias seq = sequencesService.GetNextSequenceByTableName("[admin].[MstCatalogo]");

                        catalog.IdCatalogo = seq.SecuencialTabla;
                        _unitOfWork.GetRepository<MstCatalogo>().Add(catalog);

                        sequencesService.InsertSequence(seq);

                        _unitOfWork.Commit();
                    }
                }

                result.Message = Messages.SuccessSaveData;
            }
            catch (Exception ex)
            { 
                result.Message = string.Format(Messages.ErrorSaveData, "registro de catálogo");
                result.MessageType = MessageType.ERROR;
                if (ex.GetType().Name == "DbEntityValidationException")
                    result.DetailException = new Exception(Utilities.GetEntityValidationErrors(ex as System.Data.Entity.Validation.DbEntityValidationException));
                else
                    result.DetailException = ex;
            }

            return result;
        }

        public ResultBase UpdateCatalog(MstCatalogo catalog)
        {
            ResultBase result = new ResultBase();

            try
            {
                Repository<DetCatalogo> repositoryDet = _unitOfWork.GetRepository<DetCatalogo>();

                if (catalog.IdCatalogo == 0)
                {
                    result.Message = Messages.NotExist;
                    result.MessageType = MessageType.WARNING;
                    result.DetailException = new Exception(result.Message);
                }
                else
                {
                    if (string.IsNullOrEmpty(catalog.Nombre))
                    {
                        result.MessageType = MessageType.INFO;
                        result.DetailException = new Exception(result.Message);
                        result.Message = string.Format(Messages.Required, "Nombre Catálogo");
                    }
                    else
                    {
                        if (!catalog.Activo)
                        {
                            var details = repositoryDet.GetMany(d => d.IdMstCatalogo == catalog.IdCatalogo && d.Activo);

                            if (details != null)
                            {
                                result.Message = Messages.WithDependencies;
                                result.MessageType = MessageType.ERROR;
                                result.DetailException = new Exception(Messages.WithDependencies);
                                return result;
                            }
                        }

                        var catalogDB = _unitOfWork.GetRepository<MstCatalogo>().GetById(catalog.IdCatalogo);

                        if (catalogDB != null)
                        {
                            //catalogDB.IdUsuarioUpd = 1;
                            catalogDB.Activo = catalog.Activo;
                            catalogDB.FechaUpd = DateTime.Now;
                            catalogDB.Nombre = catalog.Nombre;
                            catalogDB.Descripcion = catalog.Descripcion;
                            
                            _unitOfWork.GetRepository<MstCatalogo>().Update(catalogDB);

                            _unitOfWork.Commit();

                            result.Message = Messages.SuccessUpdateData;
                        }
                        else
                        {
                            result.Message = Messages.NotExist;
                            result.MessageType = MessageType.WARNING;
                            result.DetailException = new Exception(result.Message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorUpdateData, "registro de catálogo");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
        }
    }
}
