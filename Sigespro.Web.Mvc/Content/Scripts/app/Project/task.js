﻿(function (app) {

    var module = {};

    module.init = function () {

        $('#FechaInicio').datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true,
            todayBtn: "linked",
            startDate: $('#DeliverableStartDate').val(),
            endDate: $('#DeliverableEndDate').val()
        }).on('changeDate', function (e) {

            if ($("#Hito").is(":checked")) {
                $('#FechaFin').unbind().removeData();
                module.initEndDate($('#FechaInicio').val(), $('#FechaInicio').val());
                $('#FechaFin').val($('#FechaInicio').val());
                $('#hdfFechaFin').val($('#FechaInicio').val());
            }
            else {
                if ($('#hdfFechaFin').length > 0) {
                    $('#hdfFechaFin').val(e.date);
                    $('#FechaFin').val($('#FechaInicio').val());
                }
                else {
                    //$('#FechaFin').val("");
                    $('#FechaFin').unbind().removeData();
                    module.initEndDate(e.date, $('#DeliverableEndDate').val());
                }
            }
        });
        
        if ($("#Hito").is(":checked")) {
            $('#FechaFin').unbind().removeData();
            module.initEndDate($('#FechaInicio').val(), $('#FechaInicio').val());
            $('#FechaFin').val($('#FechaInicio').val());
            $('#hdfFechaFin').val($('#FechaInicio').val());
        }
        else {
            module.initEndDate($('#FechaInicio').val(), $('#DeliverableEndDate').val());
            $('#FechaFin').val($('#DeliverableEndDate').val());
        }
    
        if ($("#frmTask").length > 0)
            $("#frmTask").resetValidation();
        /*
        if (jQuery().select2) {
            $('#Colaboradores').select2();
        }*/
        module.loadCollaborators();        
        app.init();
        app.formater();
        //$("#DetConfiguracionTareaFrecuencia_HoraInicio").inputmask({ "alias": "Regex", "regex": "^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$" });
    };

    module.initEndDate = function (startDate, endDate) {
        setDateGlobalizeOptions();

        //console.log(startDate);
        $('#FechaFin').datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true,
            clearBtn: true,
            startDate: startDate,
            endDate: endDate 
        });

    };

    module.saveTask = function (e) {

        $("#btnAcceptConfirm").prop("disabled", true);

        if ($("#frmTask").ajaxValidate()) {

            var frmData = new FormData(document.getElementById('frmTask'));

            $.ajax({
                url: $("#frmTask").attr("action"),
                type: $("#frmTask").attr("method"),
                data: frmData,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    app.toggleLoadingModal();
                },
                success: function (response) {
                    app.showAlert("task-alert", response.status, response.message);

                    if (response.status === app.STATUS_CODE.SUCCESS)
                        $("#frmTask").data("save", true);
                    else
                        $("#btnAcceptConfirm").prop("disabled", false);
                },
                error: function (data) {
                    console.log(data.responseText);
                    app.showAlert("task-alert", app.STATUS_CODE.ERROR, app.MESSAGE_ERROR);
                },
                statusCode: {
                    401: function () {
                        window.location.replace(app.URL_BASE + "Account/LogOut");
                    }
                }
            }).always(function () {
                app.toggleLoadingModal();
            });
        }
        else
            $("#btnAcceptConfirm").prop("disabled", false);
    };

    module.checkOption = function (e) {

        var frmData = new FormData(document.getElementById('frmTask'));

        $.ajax({
            url: $(e.target).data("action"),
            type: "POST",
            data: frmData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                app.toggleLoadingModal();
            },
            success: function (response) {
                $("#frmTask").html(response);
                module.loadCollaborators();
                /*
                if (jQuery().multiselect) {
                    $('#Colaboradores').multiselect({
                        columns: 1,
                        placeholder: 'Identificación - Nombre',
                        search: true,
                        selectAll: true
                    });
                }*/
                //$("#frmTask").resetValidation(); 
                $('#FechaFin').val("");
                $('#FechaFin').unbind().removeData();
                module.init();

                if ($("#DetConfiguracionTareaFrecuencia_DetIntervaloFrecuencia_FechaInicio")) {
                    $("#DetConfiguracionTareaFrecuencia_DetIntervaloFrecuencia_FechaInicio").datepicker({
                        language: "es",
                        autoclose: true,
                        todayHighlight: true,
                        clearBtn: true,
                        startDate: $('#DeliverableStartDate').val(),
                        endDate: $('#DeliverableEndDate').val()
                    });

                    $("#DetConfiguracionTareaFrecuencia_DetIntervaloFrecuencia_FechaInicio").val($('#DeliverableStartDate').val());
                }

                if ($("#DetConfiguracionTareaFrecuencia_HoraInicio")) {
                    $('#DetConfiguracionTareaFrecuencia_HoraInicio').datetimepicker({
                        format: 'HH:mm a'
                    });

                    $('#DetConfiguracionTareaFrecuencia_HoraFin').datetimepicker({
                        format: 'HH:mm a'
                    });
                }
            },
            error: function (data) {
                console.log(data.responseText);
                app.showAlert("task-alert", app.STATUS_CODE.ERROR, app.MESSAGE_ERROR);
            },
            statusCode: {
                401: function () {
                    console.log(app.URL_BASE);
                    window.location.replace(app.URL_BASE + "Account/LogOut");
                }
            }
        }).always(function () {
            app.toggleLoadingModal();
        });
    }

    module.editTask = function (e) {

        $.ajax({
            type: "POST",
            url: $(e.target).data("action"),
            beforeSend: function () {
                app.toggleLoadingModal();
            },
            success: function (response) {
                if (response.status !== undefined)
                    app.showModalMessage(response.status, response.message, app.MODAL_SIZE.SM);
                else {
                    app.showModalDialog("Editar Tarea", response, module.updateTask, null, module.closeDialogAddTask, null, app.MODAL_SIZE.LG);

                    $("#btnAcceptConfirm").html('<i class="material-icons" style="pointer-events: none;">save</i> Guardar');
                    $("#btnAcceptConfirm").data('close', false);

                    $("#btnCancelConfirm").html('<i class="material-icons" style="pointer-events: none;">cancel</i> Cerrar');

                    $('#FechaInicio').datepicker({
                        language: "es",
                        autoclose: true,
                        todayHighlight: true,
                        todayBtn: "linked",
                        startDate: $('#DeliverableStartDate').val(),
                        endDate: $('#DeliverableEndDate').val()
                    }).on('changeDate', function (e) {
                        $('#FechaFin').val("");
                        $('#FechaFin').unbind().removeData();
                        module.initEndDate(e.date);
                    });

                    module.initEndDate($('#FechaInicio').val());

                    $("#frmTask").resetValidation();

                    module.loadCollaborators();

                    app.init();
                    app.formater();
                }
            },
            error: function (data) {
                console.log(data.responseText);
                app.showModalMessage(app.STATUS_CODE.ERROR, app.MESSAGE_ERROR, app.MODAL_SIZE.MD);
            },
            statusCode: {
                401: function () {
                    window.location.replace(app.URL_BASE + "Account/LogOut");
                }
            }
        }).always(function (jqXHR, textStatus, error) {
            app.toggleLoadingModal();
        });

    };

    module.closeDialogAddTask = function () {
        if ($("#frmTask").data("save")) {
            app.toggleLoadingModal();
            window.location.reload(true);
        }
    };

    module.submitForm = function (e) {
        e.preventDefault();

        $(e.target).prop("disabled", true);

        if ($("#frmTask").ajaxValidate()) {

            var frmData = new FormData(document.getElementById('frmTask'));

            $.ajax({
                url: $("#frmTask").attr("action"),
                type: $("#frmTask").attr("method"),
                data: frmData,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    app.toggleLoadingModal();
                },
                success: function (response) {
                    if (response.status === app.STATUS_CODE.SUCCESS) {
                        app.showModalMessage(response.status, response.message, app.MODAL_SIZE.MD, app.redirectTo, [response.action]);
                        $("#btnModalMessageClose").focus();
                    }
                    else {
                        app.showModalMessage(response.status, response.message);
                        $(e.target).prop("disabled", false);
                    }
                },
                error: function (data) {
                    console.log(data.responseText);
                    $('#modalMessage').modal('show');
                    $(e.target).prop("disabled", false);
                    $('#modalMessage .modal-body').html(app.MESSAGE_ERROR);
                },
                statusCode: {
                    401: function () {
                        window.location.replace(app.URL_BASE + "Account/LogOut");
                    }
                }
            }).always(function () {
                app.toggleLoadingModal();
            });
        }
        else
            $(e.target).prop("disabled", false);
    };

    module.updateTask = function () {
        $("#btnAcceptConfirm").prop("disabled", true);

        if ($("#frmTask").ajaxValidate()) {

            var frmData = new FormData(document.getElementById('frmTask'));

            $.ajax({
                url: $("#frmTask").attr("action"),
                type: $("#frmTask").attr("method"),
                data: frmData,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    app.toggleLoadingModal();
                },
                success: function (response) {
                    if (response.status === app.STATUS_CODE.SUCCESS) {
                        $("#frmTask").data("save", true);
                        $("#modalConfirm").modal("hide");
                        app.showModalMessage(response.status, response.message, app.MODAL_SIZE.MD, app.project.refreshPlanning, [frmData.get("IdProyecto"), frmData.get("UrlRefresh")]);
                    }
                    else
                        app.showAlert("task-alert", response.status, response.message);
                },
                error: function (data) {
                    console.log(data.responseText);
                    app.showAlert("deliverable-alert", app.STATUS_CODE.ERROR, app.MESSAGE_ERROR);
                },
                statusCode: {
                    401: function () {
                        window.location.replace(app.URL_BASE + "Account/LogOut");
                    }
                }
            }).always(function () {
                app.toggleLoadingModal();
                $("#btnAcceptConfirm").prop("disabled", false);
            });
        }
        else
            $("#btnAcceptConfirm").prop("disabled", false);
    };

    module.changeResponsable = function (e) {
        var select = $(e.target);
        console.log(select);
        if (select.val() > 0) {
            var frmData = new FormData(document.getElementById('frmTask'));
         
            $.ajax({
                url: select.data("action"),
                type: "POST",
                data: frmData,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    app.toggleLoadingModal();
                },
                success: function (response) {
                    $("#collaboratorContainer").html(response);

                    module.loadCollaborators();
                    //$.material.init();
                    /*
                    if (jQuery().select2) {
                        $('#Colaboradores').select2();
                        $.material.init();
                    }*/
                },
                error: function (data) {
                    //console.log(data.responseText);
                    $('#modalMessage').modal('show');
                    $(e.target).prop("disabled", false);
                    $('#modalMessage .modal-body').html(app.MESSAGE_ERROR);
                },
                statusCode: {
                    401: function () {
                        window.location.replace(app.URL_BASE + "Account/LogOut");
                    }
                }
            }).always(function () {
                app.toggleLoadingModal();
            });
        } 
    };

    module.loadCollaborators = function () {
        // Cargar listado de colaboradores
        if (jQuery().select2) {
            $('#collaborator').select2({
                ajax: {
                    url: $("#collaborator").data("action"),
                    dataType: 'json'
                },
                placeholder: {
                    id: '0',
                    text: '-- Seleccione --'
                }
            });

            var collaboratorSelect = $('#collaborator');
            $.ajax({
                type: 'GET',
                url: $("#collaborator").data("action"),
            }).then(function (data) {
                console.log(data.results);
                data.results.forEach(function (item) {
                    var option = new Option(item.text, item.id, item.selected, item.selected);
                    collaboratorSelect.append(option).trigger('change');
                    collaboratorSelect.trigger({
                        type: 'select2:select',
                        params: {
                            data: item
                        }
                    });
                });
            });
        }
    };

    app.task = module;

})(sigesproApp || {});

sigesproApp.task.init();