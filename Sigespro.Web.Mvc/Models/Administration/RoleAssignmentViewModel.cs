﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sigespro.Web.Mvc.Models.Administration
{
    public class RoleAssignmentViewModel
    {
        public int RoleID { get; set; }

        public int AssignmentID { get; set; }

        public string Description { get; set; }

        public bool IsChecked { get; set; }
    }
}