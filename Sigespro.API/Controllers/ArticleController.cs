﻿using Common.Extensions;
using Common.Log;
using Common.Helpers;
using Sigespro.Entity;
using Sigespro.Entity.Resource;
using Sigespro.Service;
using Sigespro.Service.Pre;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Sigespro.API.Controllers
{
    [RoutePrefix("Api/Article")]
    public class ArticleController : ApiController
    {
        ArticleService _service;

        public ArticleController(Func<string, IService> serviceFactory)
        {
            _service = (ArticleService)serviceFactory("Article");
        }

        [HttpGet]
        [Route("")]
        public IEnumerable<object> Get()
        {
            var result = _service.GetArticles();

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data;
        }

        [HttpGet]
        [Route("{ArticleCode}/{EntryCode}")]
        public CatArticulos Get(string ArticleCode, string EntryCode)
        {
            var result = _service.GetArticleById(ArticleCode, EntryCode);

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data;
        }

        [HttpGet]
        [Route("Filter/{Filter}")]
        public IEnumerable<object> GetArticles(string Filter)
        {
            var result = _service.GetArticles(Filter);

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data;
        }

        [HttpPost]
        [Route("List")]
        public HttpResponseMessage ListArticles(QueryOptions<IEnumerable<vwArticulos>> filter)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            _service.GetArticlesForList(ref filter);

            if (filter.ResultData.MessageType == MessageType.INFO)
                response = Request.CreateResponse(HttpStatusCode.OK, filter, "application/json");
            else
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, filter.ResultData.Message);
                Logger.Log(filter.ResultData.MessageType, filter.ResultData.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());
            }

            return response;
        }

        [HttpPost]
        [Route("")]
        public HttpResponseMessage Post(CatArticulos article)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            if (ModelState.IsValid)
            {
                ResultBase result = new ResultBase();

                try
                {
                    result = _service.InsertArticle(article);

                    if (result.MessageType == MessageType.SUCCESS)
                    {
                        response = Request.CreateResponse(HttpStatusCode.Created, article);

                        string uri = Url.Link("TwoKeyApi", new
                        {
                            parameter1 = article.IdArticulo,
                            parameter2 = article.IdRubro
                        });

                        response.Headers.Location = new Uri(uri);
                    }
                    else
                    {
                        if (result.MessageType == MessageType.WARNING)
                            response = Request.CreateErrorResponse(HttpStatusCode.Conflict, result.Message);
                        else
                            response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, result.Message);

                        throw result.DetailException;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log(result.MessageType, ex, "Yo xD", GetType().Name, Request.GetClientIpAddress());
                }
            }
            else
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.RequiredData);

            return response;
        }

        [HttpPut]
        [Route("{ArticleCode}/{EntryCode}")]
        public HttpResponseMessage Put(string ArticleCode, string EntryCode, [FromBody]CatArticulos article)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            if (ModelState.IsValid)
            {
                ResultBase result = new ResultBase();

                try
                {
                    if (!ArticleCode.Equals(article.IdArticulo) && !EntryCode.Equals(article.IdRubro))
                        response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.NoMatchesUpdate);
                    else
                    {
                        result = _service.UpdateArticle(article);

                        if (result.MessageType == MessageType.SUCCESS)
                            response = Request.CreateResponse(HttpStatusCode.OK);
                        else
                        {
                            if (result.MessageType == MessageType.WARNING)
                                response = Request.CreateErrorResponse(HttpStatusCode.NotFound, result.Message);

                            else if (result.MessageType == MessageType.ERROR)
                                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, result.Message);

                            throw result.DetailException;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log(result.MessageType, ex, "Yo xD", GetType().Name, Request.GetClientIpAddress());
                }
            }
            else
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.RequiredData);

            return response;
        }

    }
}
