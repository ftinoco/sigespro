﻿using Sigespro.Entity.Resource;
using System;
using System.ComponentModel.DataAnnotations;

namespace Sigespro.Entity.Metadata.Config
{
    public class CatTasaCambioMetadata
    { 
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public int IdMoneda { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public DateTime Fecha { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public decimal Valor { get; set; }
    }
}
