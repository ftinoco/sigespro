﻿using Common.Extensions;
using Common.Helpers;
using log4net;
using System;
using System.Reflection;

namespace Common.Log
{
    public static class Logger
    {
        static Logger()
        {
            log4net.Config.XmlConfigurator.Configure();
        }

        private static readonly ILog customLog = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static void LogInfo(string message, string user, string @class, string ip)
        {
            Log(MessageType.INFO, new Exception(message), user, @class, ip);
        }

        public static void LogError(string message, string user, string @class, string ip)
        {
            Log(MessageType.ERROR, new Exception(message), user, @class, ip);
        }

        public static void LogWarning(string message, string user, string @class, string ip)
        {
            Log(MessageType.WARNING, new Exception(message), user, @class, ip);
        }

        /// <summary>
        /// <para>Log de errores</para>
        /// </summary>
        /// <param name="tipoMensaje"></param>
        /// <param name="ex"></param>
        /// <param name="usuario"></param>
        /// <param name="clase"></param>
        /// <param name="ip"></param>
        public static void Log(MessageType tipoMensaje, Exception ex, string user, string @class, string ip)
        {
            GlobalContext.Properties["user"] = user;

            GlobalContext.Properties["class"] = @class;

            GlobalContext.Properties["ip"] = ip;

            Exception e = new Exception();

            if (ex != null)
                e = ex.GetInnerException();

            switch (tipoMensaje)
            {
                case MessageType.INFO:
                    customLog.Info(e.Message, e);
                    break;
                case MessageType.ERROR:
                    customLog.Error(e.Message, e);
                    break;
                case MessageType.WARNING:
                    customLog.Warn(e.Message, e);
                    break;
                default:
                    customLog.Error(e.Message, e);
                    break;
            }
        }

    }
}
