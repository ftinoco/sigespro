﻿using Sigespro.Web.Mvc.Models.Budget;
using Sigespro.Web.Mvc.Models.Project.Tasks;
using Sigespro.Web.Mvc.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sigespro.Web.Mvc.Models.Project.Deliverables
{
    public class DeliverableViewModel
    {
        public int IdEntregable { get; set; }

        public int IdProyecto { get; set; }

        //public int? IdEntregableParent { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Description", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string Descripcion { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "StartDate", ResourceType = typeof(Labels))]
        public DateTime FechaInicio { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "EndDate", ResourceType = typeof(Labels))]
        public DateTime FechaFin { get; set; }

        public decimal DuracionEstimadaHoras { get; set; }

        public decimal DuracionEstimadaDias { get; set; }

        public decimal HorasDedicadas { get; set; }

        public int IdEstado { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Observations", ResourceType = typeof(Labels))]
        public string Nota { get; set; }

        public int PresupuestoID { get; set; }

        public decimal Presupuesto { get; set; }

        public int IdUsuarioIns { get; set; }

        public DateTime FechaIns { get; set; }

        public int? IdUsuarioUpd { get; set; }
        
        public DateTime? FechaUpd { get; set; }

        public List<TaskViewModel> Tasks { get; set; }

        public List<BudgetModel> FinancialBudget { get; set; }

        public List<PhysicalBudget> PhysicalBudget { get; set; }
    }
}