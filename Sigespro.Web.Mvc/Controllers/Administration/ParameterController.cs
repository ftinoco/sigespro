﻿using Common.Helpers;
using Common.Log;
using Newtonsoft.Json;
using Sigespro.Entity;
using Sigespro.Service.Admin;
using Sigespro.Web.Mvc.Models.Administration;
using Sigespro.Web.Mvc.Resources;
using Sigespro.Web.Mvc.Utilities.Attributes.Security;
using Sigespro.Web.Mvc.Utilities.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Sigespro.Web.Mvc.Controllers.Administration
{
    [RoutePrefix("Administration/Parameters")] 
    public class ParameterController : Controller
    {
        private readonly IUserSession _userSession;

        public ParameterService Service { get; set; }
         
        public ParameterController()
        {
            Service = new ParameterService();
            _userSession = new UserSession();
        }

        [Route]
        public ActionResult Index(QueryOptions<IEnumerable<ParameterViewModel>> queryOpt)
        {
            string message = string.Empty;
            IEnumerable<ParameterViewModel> model = new List<ParameterViewModel>();
             
            string strSerialize = JsonConvert.SerializeObject(queryOpt);

            QueryOptions<IEnumerable<CatParametros>> filter = JsonConvert.DeserializeObject<QueryOptions<IEnumerable<CatParametros>>>(strSerialize);
            Service.GetParametersForList(ref filter);

            if (filter.ResultData.MessageType == MessageType.ERROR)
            {
                message = filter.ResultData.Message;
                Logger.Log(filter.ResultData.MessageType, filter.ResultData.DetailException, _userSession.Username,
                           MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }
            else
            {
                strSerialize = JsonConvert.SerializeObject(filter);

                queryOpt = JsonConvert.DeserializeObject<QueryOptions<IEnumerable<ParameterViewModel>>>(strSerialize);

                model = queryOpt.ResultData.Data;
            }

            if (!string.IsNullOrEmpty(message))
                ModelState.AddModelError(Messages.AlertDanger, message);

            ViewBag.Filter = true;
            ViewBag.QueryOptions = queryOpt;
            
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [Route("Add")]
        public ActionResult AddParameter()
        {
            ViewBag.IsEdit = false;
            ViewBag.Action = "SaveParameter";
            ViewBag.Title = "Nuevo Parámetro";

            return View();
        }

        [Route("Edit/{parameterId}")]
        public ActionResult EditParameter(int parameterId)
        {
            string message = string.Empty;
            ParameterViewModel model = new ParameterViewModel();

            try
            {
                var result = Service.GetParameterById(parameterId); 

                if (result.MessageType == MessageType.ERROR)
                {
                    message = result.Message;
                    Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                                       MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }   
                else
                { 
                    model = new ParameterViewModel
                    {
                        DescParametro = result.Data.DescParametro,
                        IdPerametro = result.Data.IdPerametro,
                        NombreParametro = result.Data.NombreParametro,
                        ValorParametro = result.Data.ValorParametro
                    }; 
                }
            }
            catch (Exception ex)
            {
                message = Messages.ErrorApi;
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                           MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            if (!string.IsNullOrEmpty(message))
                ModelState.AddModelError(Messages.AlertDanger, message);

            ViewBag.IsEdit = true;
            ViewBag.Action = "UpdateParameter";
            ViewBag.Title = "Editar Parámetro";

            return View("AddParameter", model);
        }

        [AjaxOnly]
        [HttpPost]
        [Route("Save")]
        [AllowAnonymous]
        public ActionResult SaveParameter(ParameterViewModel model)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                string strSerialize = JsonConvert.SerializeObject(model);

                var result = Service.InsertParameter(JsonConvert.DeserializeObject<CatParametros>(strSerialize));

                if (result.MessageType != MessageType.SUCCESS)
                {
                    message = result.Message;
                    status = result.MessageType.ToString();
                    Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                    message = string.Format(Messages.SaveSuccess, "parámetro");
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                             MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message, action = Url.Action("Index", "Parameter") },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [AjaxOnly]
        [HttpPost]
        [AllowAnonymous]
        [Route("Update")]
        public ActionResult UpdateParameter(ParameterViewModel model)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                string strSerialize = JsonConvert.SerializeObject(model);

                var result = Service.UpdateParameter(JsonConvert.DeserializeObject<CatParametros>(strSerialize));

                if (result.MessageType != MessageType.SUCCESS)
                {
                    message = result.Message;
                    status = result.MessageType.ToString();
                    Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                    message = string.Format(Messages.UpdateSuccess, "parámetro");
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                             MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message, action = Url.Action("Index", "Parameter") },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

    }
}