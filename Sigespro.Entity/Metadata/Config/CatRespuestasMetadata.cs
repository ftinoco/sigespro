﻿using Sigespro.Entity.Resource;
using System.ComponentModel.DataAnnotations;

namespace Sigespro.Entity.Metadata.Config
{
    public class CatRespuestasMetadata
    {
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public int IdRespuesta { get; set; } 

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public int IdGrupoRespuesta { get; set; }

        [Display(Name = "Descripcion", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string Descripcion { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public decimal Valor { get; set; }
    }
}
