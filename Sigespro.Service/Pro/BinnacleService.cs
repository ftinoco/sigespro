﻿using Common.Helpers;
using Sigespro.DAL.Implementations;
using Sigespro.Entity;
using Sigespro.Entity.Custom;
using Sigespro.Entity.Resource;
using Sigespro.Service.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigespro.Service.Pro
{
    public class BinnacleService : Service
    {
        private Repository<BitacoraTareas> Repository { get; set; }

        public BinnacleService() : base()
        {
            Repository = _unitOfWork.GetRepository<BitacoraTareas>();
        }

        public BinnacleService(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            Repository = _unitOfWork.GetRepository<BitacoraTareas>();
        }

        public ResultBase InsertBinnacle(BitacoraTareas binnacle, bool submit = true)
        {
            ResultBase result = new ResultBase();

            try
            {
                bool insert = false;

                // Se valida y se establece la decripción del estado
                if (binnacle.IdEstado.HasValue)
                {
                    DetailCatalogService catalogService = new DetailCatalogService();
                    var catalogResult = catalogService.GetDetailsByTable("EstadoTareas");

                    if (catalogResult.MessageType == MessageType.INFO)
                    {
                        var status = catalogResult.Data.FirstOrDefault(x => x.IdDetCatalogo == binnacle.IdEstado.Value);
                        if (status != null)
                        {
                            // Se obtiene el ultimo registro de bitacora en donde se actualizó el estado de la tarea
                            var temp = Repository.GetMany(x => x.IdTarea == binnacle.IdTarea && x.IdEstado.HasValue).OrderByDescending(x => x.Fecha).FirstOrDefault();

                            // Se valida que sea diferente el estado
                            if (temp == null || (temp != null && temp.IdEstado != binnacle.IdEstado))
                            {
                                binnacle.EstadoDesc = status.Descripcion;
                                // Si el estado de la tarea es ejecutada, el progreso se establece 100%
                                if (binnacle.IdEstado == catalogResult.Data.FirstOrDefault(x => x.Valor == "Ejecutada").IdDetCatalogo)
                                    binnacle.Progreso = 100;
                                insert = true;
                            }
                            else
                            {
                                binnacle.IdEstado = null;
                                binnacle.EstadoDesc = null;
                            }
                        }
                        else
                        {
                            result.MessageType = MessageType.ERROR;
                            result.Message = "Identificador de estado de tarea no valido";
                            result.DetailException = new Exception(result.Message);
                        }
                    }
                    else
                    {
                        result.MessageType = MessageType.ERROR;
                        result.Message = string.Format(Messages.ErrorSaveData, "bitácora tarea");
                        result.DetailException = catalogResult.DetailException;
                    }
                }

                // Se valida el progreso de la tarea
                if (binnacle.Progreso.HasValue)
                {
                    if (binnacle.Progreso.Value > 100 || binnacle.Progreso.Value < 0)
                    {
                        result.MessageType = MessageType.ERROR;
                        result.Message = "El progreso de la tarea no puede ser mayor a 100% ni menor a 0%";
                        result.DetailException = new Exception(result.Message);
                    }
                    else
                    {
                        // Se obtiene el ultimo registro de bitacora en donde se actualizó el pregreso
                        var temp = Repository.GetMany(x => x.IdTarea == binnacle.IdTarea && x.Progreso.HasValue).OrderByDescending(x => x.Fecha).FirstOrDefault();

                        if (temp == null || (temp != null && temp.Progreso.Value != binnacle.Progreso))
                            insert = true;
                        else
                            binnacle.Progreso = null;
                    }
                }

                // Si tiene archivo vinculado
                if (binnacle.IdArchivoTarea.HasValue && !string.IsNullOrWhiteSpace(binnacle.NombreArchivo))
                    insert = true;

                if (!string.IsNullOrWhiteSpace(binnacle.Comentarios))
                    insert = true;

                if (insert)
                {
                    if (binnacle.IdUsuario <= 0)
                    {
                        result.MessageType = MessageType.ERROR;
                        result.Message = "Identificador de usuario no valido";
                        result.DetailException = new Exception(result.Message);
                    }

                    // Se valida que solo exista un registro en la bitácora de la creación de la tarea
                    if (binnacle.Creacion)
                    {
                        var exist = Repository.Get(x => x.IdTarea == binnacle.IdTarea && x.IdEntregable == binnacle.IdEntregable &&
                                                        x.IdProyecto == binnacle.IdProyecto && x.Creacion);
                        if (exist != null)
                        {
                            result.MessageType = MessageType.ERROR;
                            result.Message = "Ya existe un registro de creación para la tarea " + binnacle.IdTarea;
                            result.DetailException = new Exception(result.Message);
                        }
                    }

                    binnacle.Fecha = DateTime.Now;
                    Repository.Add(binnacle);

                    if (submit)
                        _unitOfWork.Commit();
                }
                else
                {
                    result.MessageType = MessageType.ERROR;
                    result.Message = "No se hicieron cambio en la tarea";
                    result.DetailException = new Exception(result.Message + " Por ende no se registra en la bitácora");
                }
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorSaveData, "bitácora tarea");
                result.MessageType = MessageType.ERROR;

                if (ex.GetType().Name == "DbEntityValidationException")
                    result.DetailException = new Exception(Utilities.GetEntityValidationErrors(ex as System.Data.Entity.Validation.DbEntityValidationException));
                else
                    result.DetailException = ex;
            }

            return result;
        }

        public Result<List<BinnacleTask>> GetActivity(int taskID)
        {
            Result<List<BinnacleTask>> result = new Result<List<BinnacleTask>>();

            try
            {
                var taskRep = _unitOfWork.GetRepository<vwActividad>();
                //var userRep = _unitOfWork.GetRepository<Users>();

                List<BinnacleTask> binnacles = /*Repository.GetAll()
                                .Join(taskRep.GetAll(), b => new { b.IdTarea, b.IdEntregable, b.IdProyecto },
                                                        t => new { t.IdTarea, t.IdEntregable, t.IdProyecto },
                                    (b, t) => new { b, t })
                                .Join(userRep.GetAll(), x => x.b.IdUsuario,
                                                        u => u.UserId,
                                    (x, u) => new { x, u })*/
                               taskRep.GetMany(p => p.IdTarea == taskID)
                                .Select(y => new BinnacleTask
                                {
                                    Comments = y.Comentarios,
                                    Creation = y.Creacion,
                                    Date = y.Fecha,
                                    FileName = y.NombreArchivo,
                                    Progress = y.Progreso,
                                    Status = y.EstadoDesc,
                                    TaskID = y.IdTarea,
                                    TaskName = y.Tarea,
                                    User = y.NombreCompleto,
                                    FileID = y.IdArchivoTarea,
                                    FileDeleted = y.Eliminado,
                                    //FilePath = y.NombreEnRuta,
                                    ProjectID = y.IdProyecto
                                }).OrderByDescending(z => z.Date).ToList();

                if (binnacles != null && binnacles.Count > 0)
                {
                    result.Data = binnacles;
                    result.MessageType = MessageType.INFO;
                    result.Message = Messages.SuccessGetResultSet;
                }
                else
                {
                    result.MessageType = MessageType.WARNING;
                    result.Message = Messages.NoFoundResult;
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<TaskInfo> GetTaskInfo(int taskID, int personalID)
        {
            Result<TaskInfo> result = new Result<TaskInfo>();

            try
            {
                var taskRep = _unitOfWork.GetRepository<vwTareasEnEjecucion>();
                var tasks = taskRep.GetMany(t => t.IdTarea == taskID).ToList();

                DetailCatalogService catalogService = new DetailCatalogService();
                var catalogResult = catalogService.GetDetailsByTable("EstadoTareas");
                int finished = catalogResult.Data.FirstOrDefault(x => x.Valor == "Ejecutada").IdDetCatalogo;
                int canceledId = catalogResult.Data.FirstOrDefault(x => x.Valor == "Cancelada").IdDetCatalogo;

                vwTareasEnEjecucion taskInfo = tasks.FirstOrDefault();
                result.Data = new TaskInfo
                {
                    TaskID = taskInfo.IdTarea,
                    TaskName = taskInfo.Nombre,
                    Author = taskInfo.Autor,
                    LastUpdate = taskInfo.UltimaActualizacion,
                    Manager = tasks.FirstOrDefault(x => x.TipoPersonal == "R" && x.IdTarea == taskInfo.IdTarea).NombrePersona,
                    Status = taskInfo.Estado,
                    StatusID = taskInfo.IdEstado,
                    Progress = taskInfo.Progreso,
                    AddAdvance = (tasks.Any(x => x.TipoPersonal == "R" && x.IdPersonal == personalID && x.IdTarea == taskInfo.IdTarea)
                                  && tasks.Any(x => (x.IdEstado != finished && x.IdEstado != canceledId) && x.IdTarea == taskInfo.IdTarea)),
                    DeliverableID = taskInfo.IdEntregable,
                    ProjectID = taskInfo.IdProyecto
                };
                result.MessageType = MessageType.INFO;
                result.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase InsertTaskFile(ArchivosTareas taskFile, int userID, ref string filename)
        {
            ResultBase result = new ResultBase();

            try
            {
                var taskRep = _unitOfWork.GetRepository<MstTareas>();
                var tempTask = taskRep.Get(t => t.IdTarea == taskFile.IdTarea);

                if (tempTask != null)
                {
                    taskFile.IdEntregable = tempTask.IdEntregable;
                    taskFile.IdProyecto = tempTask.IdProyecto;

                    filename = DateTime.Now.Ticks + "_" + taskFile.NombreArchivo;
                    taskFile.NombreEnRuta = System.IO.Path.Combine(taskFile.NombreEnRuta, filename);

                    var taskFileRep = _unitOfWork.GetRepository<ArchivosTareas>();
                    taskFileRep.Add(taskFile);

                    // Se registra la creación de la tarea en la bitacora 
                    var resultBinnacle = this.InsertBinnacle(new BitacoraTareas
                    {
                        IdTarea = taskFile.IdTarea,
                        IdEntregable = taskFile.IdEntregable,
                        IdProyecto = taskFile.IdProyecto,
                        IdUsuario = userID,
                        Creacion = false,
                        IdArchivoTarea = taskFile.IdArchivoTarea,
                        NombreArchivo = taskFile.NombreArchivo
                    }, false);

                    if (resultBinnacle.MessageType == MessageType.SUCCESS)
                        _unitOfWork.Commit();
                    else
                        result = resultBinnacle;
                }
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorSaveData, "archivo");
                result.MessageType = MessageType.ERROR;

                if (ex.GetType().Name == "DbEntityValidationException")
                    result.DetailException = new Exception(Utilities.GetEntityValidationErrors(ex as System.Data.Entity.Validation.DbEntityValidationException));
                else
                    result.DetailException = ex;
            }

            return result;
        }
        
        public Result<ArchivosTareas> DeleteTaskFile(int taskFileID, int userID)
        {
            Result<ArchivosTareas> result = new Result<ArchivosTareas>();

            try
            {
                var taskFileRep = _unitOfWork.GetRepository<ArchivosTareas>();
                var taskFile = taskFileRep.GetById(taskFileID);

                if (taskFile != null)
                {
                    taskFile.Eliminado = true;
                    taskFileRep.Update(taskFile);

                    var binnacle = Repository.Get(x => x.IdArchivoTarea == taskFile.IdArchivoTarea);

                    if (binnacle != null)
                    {
                        binnacle.Fecha = DateTime.Now;
                        binnacle.IdUsuario = userID;
                        Repository.Update(binnacle);
                        _unitOfWork.Commit();

                        result.Data = taskFile;
                    }
                    else
                    {
                        //manejar error
                    }
                }
                else
                {
                    //manejar error
                }
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorSaveData, "archivo");
                result.MessageType = MessageType.ERROR;

                if (ex.GetType().Name == "DbEntityValidationException")
                    result.DetailException = new Exception(Utilities.GetEntityValidationErrors(ex as System.Data.Entity.Validation.DbEntityValidationException));
                else
                    result.DetailException = ex;
            }

            return result;
        }



        public Result<ArchivosTareas> GetFileInfo(int taskFileID)
        {
            Result<ArchivosTareas> result = new Result<ArchivosTareas>();

            try
            {
                result.MessageType = MessageType.INFO;

                var repository = _unitOfWork.GetRepository<ArchivosTareas>();

                result.Data = repository.GetById(taskFileID);

                result.Message = Messages.SuccessGetResult;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<List<ArchivosTareas>> GetTaskFiles(int taskID)
        {
            Result<List<ArchivosTareas>> result = new Result<List<ArchivosTareas>>();

            try
            {
                result.MessageType = MessageType.INFO;

                var repository = _unitOfWork.GetRepository<ArchivosTareas>();

                result.Data = repository.GetMany(x => x.IdTarea == taskID).ToList();

                result.Message = Messages.SuccessGetResult;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

    }
}
