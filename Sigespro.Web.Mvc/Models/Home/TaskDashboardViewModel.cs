﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sigespro.Web.Mvc.Models.Home
{
    public class TaskDashboardViewModel
    {
        public string Header { get; set; }

        public int TasksCount { get; set; }

        public bool ShowFooter { get; set; }

        public int TasksCanceled { get; set; }

        public int TasksInAction { get; set; }

        public int TasksFinished { get; set; }

        public bool Overdue { get; set; }
    }
}