﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Sigespro.Web.Mvc
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                name: "Custom",
                url: "{controller}/{module}/{idmodule}/{submodule}/{action}/{idsubmodule}",
                defaults: new { action = UrlParameter.Optional, idsubmodule = UrlParameter.Optional },
                constraints: new { module = "\\[a-zA-Z] +", submodule = "\\[a-zA-Z] +" }
           );


            routes.MapRoute(
                name: "TwoParameter",
                url: "{controller}/{action}/{id1}/{id2}",
                defaults: new { id1 = UrlParameter.Optional, id2 = UrlParameter.Optional }
           );

            routes.MapRoute(
                name: "Pseudonym",
                url: "{controller}/{pseudonym}/{action}/{id}",
                defaults: new { id = UrlParameter.Optional },
                constraints: new { pseudonym = "\\[a-zA-Z] +" }
           );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
