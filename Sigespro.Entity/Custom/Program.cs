﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigespro.Entity.Custom
{
    public class Program
    {
        public int IdPersonal { get; set; }

        public int IdPersona { get; set; }

        public int? IdInstitucion { get; set; }

        public int? IdPrograma { get; set; }

        public int IdCargo { get; set; }

        public string Cargo { get; set; }

        public MstInstituciones MstInstituciones { get; set; }

        public MstPersonas MstPersonas { get; set; }

        public MstPrograma MstPrograma { get; set; }

    }
}
