﻿using Common.Extensions;
using Common.Log;
using Common.Helpers;
using Sigespro.Entity;
using Sigespro.Entity.Resource;
using Sigespro.Service;
using Sigespro.Service.Pre;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace Sigespro.API.Controllers
{
    [RoutePrefix("Api/Currency")]
    public class CurrencyController : ApiController
    {
        CurrencyService _service;

        public CurrencyController(Func<string, IService> serviceFactory)
        {
            _service = (CurrencyService)serviceFactory("Currency");
        }

        [HttpGet]
        [Route("")]
        public IEnumerable<CatMoneda> Get()
        {
            return Currencies();
        }

        [HttpGet]
        [Route("{CurrencyId}")]
        public CatMoneda GetCurrency(int CurrencyId)
        {
            var result = _service.GetCurrencyById(CurrencyId);

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data;
        }

        [HttpGet]
        [Route("Filter/{Filter}")]
        public IEnumerable<CatMoneda> GetCurrencies(string Filter)
        {
            return Currencies(Filter);
        }

        private IEnumerable<CatMoneda> Currencies(string filter = "")
        {
            Result<IEnumerable<CatMoneda>> result = new Result<IEnumerable<CatMoneda>>();

            if (string.IsNullOrEmpty(filter))
                result = _service.GetCurrencies();
            else
                result = _service.GetCurrencies(filter);

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo XD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data.ToList();
        }

        [HttpPost]
        [Route("List")]
        public HttpResponseMessage ListCurrencies(QueryOptions<IEnumerable<CatMoneda>> filter)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            _service.GetCurrenciesForList(ref filter);

            if (filter.ResultData.MessageType == MessageType.INFO)
                response = Request.CreateResponse(HttpStatusCode.OK, filter, "application/json");
            else
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, filter.ResultData.Message);
                Logger.Log(filter.ResultData.MessageType, filter.ResultData.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());
            }

            return response;
        }

        [HttpPost]
        [Route("")]
        public HttpResponseMessage Post(CatMoneda currency)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            if (ModelState.IsValid)
            {
                ResultBase result = new ResultBase();

                try
                {
                    result = _service.InsertCurrency(currency);

                    if (result.MessageType.Equals(MessageType.SUCCESS))
                    {
                        response = Request.CreateResponse(HttpStatusCode.Created, currency);

                        //string uri = Url.Link("DefaultApi", new { id = currency.IdMoneda });

                        //response.Headers.Location = new Uri(uri);
                    }
                    else
                    {
                        response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, result.Message);
                        throw result.DetailException;
                    }

                }
                catch (Exception ex)
                {
                    Logger.Log(result.MessageType, ex, "Yo xD", GetType().Name, Request.GetClientIpAddress());
                }
            }
            else
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.RequiredData);

            return response;
        }

        [HttpPut]
        [Route("{CurrencyId}")]
        public HttpResponseMessage Put(int CurrencyId, CatMoneda currency)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            if (ModelState.IsValid)
            {
                ResultBase result = new ResultBase();

                try
                {
                    if (CurrencyId != currency.IdMoneda)
                        response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "La información no coincide con el identificador proporcionado.");
                    else
                    {
                        result = _service.UpdateCurrency(currency);

                        if (result.MessageType == MessageType.SUCCESS)
                            response = Request.CreateResponse(HttpStatusCode.OK);
                        else
                        {
                            if (result.MessageType == MessageType.WARNING)
                                return Request.CreateErrorResponse(HttpStatusCode.NotFound, result.Message);

                            else if (result.MessageType == MessageType.ERROR)
                                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result.Message);

                            throw result.DetailException;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log(result.MessageType, ex, "Yo xD", GetType().Name, Request.GetClientIpAddress());
                }
            }
            else
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.RequiredData);

            return response;
        }

    }
}
