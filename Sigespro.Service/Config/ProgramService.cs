﻿using Common.Extensions;
using Common.Helpers;
using Common.Log;
using Sigespro.DAL.Implementations;
using Sigespro.Entity;
using Sigespro.Entity.Resource;
using Sigespro.Service.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 
using static Common.Helpers.Utilities;

namespace Sigespro.Service.Config
{
    public class ProgramService : Service
    {
        private Repository<MstPrograma> Repository { get; set; }

        public ProgramService() : base()
        {
            Repository = _unitOfWork.GetRepository<MstPrograma>();
        }

        public Result<IEnumerable<MstPrograma>> GetPrograms()
        {
            Result<IEnumerable<MstPrograma>> result = new Result<IEnumerable<MstPrograma>>();

            try
            {
                result.Data = Repository.GetAll();

                result.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<MstPrograma> GetProgramById(int idProgram)
        {
            Result<MstPrograma> result = new Result<MstPrograma>();

            try
            {
                result.Data = Repository.GetById(idProgram);
                result.Message = Messages.SuccessGetResult;
                result.MessageType = MessageType.INFO;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<IEnumerable<MstPrograma>> GetPrograms(string filter)
        {
            Result<IEnumerable<MstPrograma>> result = new Result<IEnumerable<MstPrograma>>();

            try
            {
                bool active = false;

                string filterToLower = filter.Trim().ToLower();

                if (filterToLower == Catalog.Active.ToLower() || filterToLower == Catalog.Inactive.ToLower())
                {
                    active = filterToLower == "activo" ? true : false;
                    result.Data = Repository.GetMany(p => p.Activo == active);
                }
                else
                    result.Data = Repository.GetMany(p => p.NombrePrograma.Trim().ToLower().Contains(filterToLower) ||
                                               p.Descripcion.Trim().ToLower().Contains(filterToLower));

                result.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<IEnumerable<MstPrograma>> GetProgramsForBindToProject(int programId)
        {
            Result<IEnumerable<MstPrograma>> result = new Result<IEnumerable<MstPrograma>>();

            try
            {
                var parameter = _unitOfWork.GetRepository<CatParametros>().Get(p => p.NombreParametro.Equals("PROGRAMA_CENTRO"));

                if (programId == int.Parse(parameter.ValorParametro))
                    result.Data = Repository.GetMany(p => p.Activo);
                else
                {
                    var programs = new List<MstPrograma>();
                    programs.Add(Repository.GetById(programId));

                    result.Data = programs;
                }

                result.Message = Messages.SuccessGetResultSet;

                result.MessageType = MessageType.INFO;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public void GetProgramsForList(ref QueryOptions<IEnumerable<MstPrograma>> queryOpt)
        {
            Result<IEnumerable<MstPrograma>> result = new Result<IEnumerable<MstPrograma>>();

            try
            {
                string orderBy = "NombrePrograma";

                var predicate = PredicateBuilder.True<MstPrograma>();

                var filter = queryOpt.Filters.FirstOrDefault(f => f.Name.Equals("Search"));

                if (!IsNull(filter) && !string.IsNullOrEmpty(filter.Value))
                {
                    int intFilter;

                        predicate = predicate.And(p => p.NombrePrograma.ToLower().Trim().Contains(filter.Value.ToLower().Trim()) || 
                                                       p.Descripcion.ToLower().Trim().Contains(filter.Value.ToLower().Trim()));

                    if (int.TryParse(filter.Value, out intFilter))
                        predicate = predicate.Or(p => p.IdPrograma == intFilter);
 
                    queryOpt.TotalRecords = _unitOfWork.GetRepository<MstPrograma>().Count(predicate);
                }
                else
                    queryOpt.TotalRecords = _unitOfWork.GetRepository<MstPrograma>().Count();

                if (!string.IsNullOrEmpty(queryOpt.SortField))
                    orderBy = queryOpt.SortField;

                result.Data = _unitOfWork.GetRepository<MstPrograma>().GetManyOnDemand(predicate, orderBy, queryOpt.CurrentPageIndex, queryOpt.RowsPerPage, queryOpt.SortOrder);

                result.Message = Messages.SuccessGetResultSet;

                result.MessageType = MessageType.INFO;
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorGetResultSet, "programas");
                result.MessageType = MessageType.ERROR;
                result.DetailException = ex;
            }

            queryOpt.ResultData = result;
        }

        public Result<IEnumerable<MstPrograma>> GetAllPrograms()
        {
            Result<IEnumerable<MstPrograma>> result = new Result<IEnumerable<MstPrograma>>();

            try
            {
                result.Data = Repository.GetAll().Where(p => p.Activo);

                result.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<IEnumerable<MstPrograma>> GetProjectPrograms(int projectId)
        {
            Result<IEnumerable<MstPrograma>> result = new Result<IEnumerable<MstPrograma>>();

            try
            {
                var repository = _unitOfWork.GetRepository<DetProgramaProyecto>();

                result.Data = Repository.GetAll().Join(repository.GetAll(),
                                        p => p.IdPrograma,
                                        d => d.IdPrograma,
                                        (p, d) => new { p, d})
                                .Where(x => x.p.Activo && x.d.IdProyecto == projectId)
                                .Select(p => p.p).ToList();

                result.MessageType = MessageType.INFO;
                result.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }
        
        public ResultBase InsertProgram(MstPrograma program)
        {
            ResultBase result = new ResultBase();

            try
            {
                //program.IdUsuarioIns = 1;
                program.FechaIns = DateTime.Now;

                var sequencesService = new SequenceService(_unitOfWork.GetRepository<MstSecuencias>());

                var resultValidate = Utilities.ValidateEntity(program);

                if (resultValidate.MessageType == MessageType.WARNING)
                    result = resultValidate;
                else
                {
                    MstSecuencias seq = sequencesService.GetNextSequenceByTableName("[config].[MstPrograma]");

                    program.IdPrograma = seq.SecuencialTabla;
                    Repository.Add(program);

                    sequencesService.InsertSequence(seq);

                    _unitOfWork.Commit();

                    result.Message = Messages.SuccessSaveData;
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format( Messages.ErrorSaveData, "programa");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase UpdateProgram(MstPrograma program)
        {
            ResultBase result = new ResultBase();

            try
            {
                var temp = Repository.Get(p => p.IdPrograma == program.IdPrograma);

                if (temp == null)
                {
                    result.Message = Messages.NotExist;
                    result.MessageType = MessageType.WARNING;
                    result.DetailException = new Exception(result.Message);
                }
                else
                {
                    var resultValidate = Utilities.ValidateEntity(program);

                    if (resultValidate.MessageType == MessageType.WARNING)
                        result = resultValidate;
                    else
                    {
                        //program.IdUsuarioUpd = 1;
                        program.FechaUpd = DateTime.Now;

                        Repository.Update(program);

                        _unitOfWork.Commit();

                        result.Message = Messages.SuccessUpdateData;
                    }
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorUpdateData, "programa" );
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }
    }
}
