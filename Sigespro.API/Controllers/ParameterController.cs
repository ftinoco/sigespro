﻿using Common.Extensions;
using Common.Log;
using Common.Helpers;
using Sigespro.Entity;
using Sigespro.Entity.Resource;
using Sigespro.Service;
using Sigespro.Service.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Sigespro.API.Controllers
{
    [RoutePrefix("Api/Parameter")]
    public class ParameterController : ApiController
    {
        ParameterService _service;

        public ParameterController(Func<string, IService> serviceFactory)
        {
            _service = (ParameterService)serviceFactory("Parameter");
        }

        [HttpGet]
        [Route("")]
        public IEnumerable<CatParametros> GetParameters()
        {
            var result = _service.GetParameters();

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data;
        }

        [HttpPost]
        [Route("List")]
        public HttpResponseMessage ListParameter(QueryOptions<IEnumerable<CatParametros>> filter)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            _service.GetParametersForList(ref filter);

            if (filter.ResultData.MessageType == MessageType.INFO)
                response = Request.CreateResponse(HttpStatusCode.OK, filter, "application/json");
            else
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, filter.ResultData.Message);
                Logger.Log(filter.ResultData.MessageType, filter.ResultData.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());
            }

            return response;
        }

        [HttpGet]
        [Route("{parameterId}")]
        public CatParametros GetParameterById(int parameterId)
        {
            var result = _service.GetParameterById(parameterId);

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data;
        }

        [HttpGet]
        [Route("list/{filter}")]
        public IEnumerable<CatParametros> GetParameters(string filter)
        {
            var result = _service.GetParameters(filter);

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data;
        }

        [HttpPost]
        [Route("")]
        public HttpResponseMessage AddParameter(CatParametros parameter)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            if (ModelState.IsValid)
            {
                ResultBase result = new ResultBase();

                try
                {
                    result = _service.InsertParameter(parameter);

                    if (result.MessageType == MessageType.SUCCESS)
                    {
                        response = Request.CreateResponse(HttpStatusCode.Created, parameter);

                        string uri = Url.Link("DefaultApi", new
                        {
                            parameter1 = parameter.IdPerametro, 
                        });

                        response.Headers.Location = new Uri(uri);
                    }
                    else
                    {
                        if (result.MessageType == MessageType.WARNING)
                            response = Request.CreateErrorResponse(HttpStatusCode.Conflict, result.Message);
                        else
                            response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, result.Message);

                        throw result.DetailException;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log(result.MessageType, ex, "Yo xD", GetType().Name, Request.GetClientIpAddress());
                }
            }
            else
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.RequiredData);

            return response;
        }

        [HttpPut]
        [Route("{parameterId}")]
        public HttpResponseMessage UpdateParameter(int parameterId, CatParametros parameter)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            if (ModelState.IsValid)
            {
                ResultBase result = new ResultBase();

                try
                {
                    if (!parameterId.Equals(parameter.IdPerametro))
                        response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.NoMatchesUpdate);
                    else
                    {
                        result = _service.UpdateParameter(parameter);

                        if (result.MessageType == MessageType.SUCCESS)
                            response = Request.CreateResponse(HttpStatusCode.OK);
                        else
                        {
                            if (result.MessageType == MessageType.WARNING)
                                response = Request.CreateErrorResponse(HttpStatusCode.NotFound, result.Message);

                            else if (result.MessageType == MessageType.ERROR)
                                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, result.Message);

                            throw result.DetailException;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log(result.MessageType, ex, "Yo xD", GetType().Name, Request.GetClientIpAddress());
                }
            }
            else
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.RequiredData);

            return response;
        }
        
    }
}
