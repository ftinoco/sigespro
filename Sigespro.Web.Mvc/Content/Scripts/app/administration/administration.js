﻿/**
 * FUNCIONES DE CATALOGO
 */
(function (app) {

    var module = {};

    module.postIsOk = function (response) {
        if (response.status !== app.STATUS_CODE.ERROR)
            app.showModalMessage(response.status, response.message, null, module.redirectTo);
        else
            app.showModalMessage(response.status, response.message);
    };

    module.postError = function (response) {
        console.log(response.responseText);
        app.showModalMessage(app.STATUS_CODE.ERROR, app.MESSAGE_ERROR);
    };

    module.redirectTo = function () {
        window.location.href = app.URL_BASE + "Catalog";
    }

    app.catalog = module;

}(sigesproApp || {}));

/**
 * FUNCIONES DE DETALLE CATALOGO
 */
(function (app) {

    var module = {};

    module.postIsOk = function (response) {
        if (response.status !== app.STATUS_CODE.ERROR)
            app.showModalMessage(response.status, response.message, null, module.redirectTo);
        else
            app.showModalMessage(response.status, response.message);
    };

    module.postError = function (response) {
        console.log(response.responseText);
        app.showModalMessage(app.STATUS_CODE.ERROR, app.MESSAGE_ERROR);
    };

    module.redirectTo = function () {
        window.location.href = app.URL_BASE + "Catalog/Detail";
    }

    app.detailCatalog = module;

}(sigesproApp || {}));

/**
 * FUNCIONES DE PARAMETROS
 */
(function (app) {

    var module = {};

    module.postIsOk = function (response) {
        if (response.status !== app.STATUS_CODE.ERROR)
            app.showModalMessage(response.status, response.message, null, module.redirectTo);
        else
            app.showModalMessage(response.status, response.message);
    };

    module.postError = function (response) {
        console.log(response.responseText);
        app.showModalMessage(app.STATUS_CODE.ERROR, app.MESSAGE_ERROR);
    };

    module.redirectTo = function () {
        window.location.href = app.URL_BASE + "Parameter";
    }

    app.parameter = module;

}(sigesproApp || {}));


/**
 * FUNCIONES DE USUARIOS
 */
(function (app) {

    var module = {};

    module.init = function () {

        app.formater();
    };

    module.selectPerson = function (e) {
        var action = $(e.target).data("action");

        var frmData = new FormData(e.target.form);

        $.ajax({
            type: "POST",
            url: action,
            data: frmData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                app.toggleLoadingModal();
            },
            success: function (response) {

                $(e.target.form).resetValidation();
                $("#main-panel-body").html(response);

                app.init();
                module.init();
            },
            error: function (data) {
                console.log(data.responseText);
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body').html(app.MESSAGE_ERROR);
            },
            statusCode: {
                401: function () {
                    window.location.replace(urlBase + "Account/LogOut");
                }
            }
        }).always(function () {
            app.toggleLoadingModal();
        });
    };

    module.resetPassword = function (e) {
        e.preventDefault();
         
        $.ajax({
            type: "POST",
            url: $(e.target).attr("href"),
            beforeSend: function () {
                app.toggleLoadingModal();
            },
            success: function (response) {
                app.showModalMessage(response.status, response.message);
            },
            error: function (data) {
                console.log(data.responseText);
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body').html(app.MESSAGE_ERROR);
            },
            statusCode: {
                401: function () {
                    window.location.replace(urlBase + "Account/LogOut");
                }
            }
        }).always(function () {
            app.toggleLoadingModal();
        });
    };

    app.user = module;

})(sigesproApp || {});

sigesproApp.user.init();