//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sigespro.Entity
{
    using System;
    using System.Collections.Generic;
     
    public partial class DetContactoBeneficiario
    {
        public int IdContactoBeneficiario { get; set; }
        public int IdPersona { get; set; }
        public int IdBeneficiario { get; set; }
    
        public virtual MstBeneficiarios MstBeneficiarios { get; set; }
        public virtual MstPersonas MstPersonas { get; set; }
    }
}
