//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sigespro.Entity
{
    using System;
    using System.Collections.Generic;
     
    public partial class RoleMenu
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public RoleMenu()
        {
            this.RoleMenuItem = new HashSet<RoleMenuItem>();
        }
    
        public int SystemId { get; set; }
        public int RoleId { get; set; }
        public int MenuId { get; set; }
    
        public virtual Menu Menu { get; set; }
        public virtual SystemRoles SystemRoles { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RoleMenuItem> RoleMenuItem { get; set; }
    }
}
