﻿using Common.Helpers;
using Common.Log;
using Newtonsoft.Json;
using Sigespro.Service.Pro;
using Sigespro.Web.Mvc.Models.Project.Tasks;
using Sigespro.Web.Mvc.Resources;
using Sigespro.Web.Mvc.Utilities.Attributes.Security;
using Sigespro.Web.Mvc.Utilities.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace Sigespro.Web.Mvc.Controllers.Project
{
    [RoutePrefix("Task")]
    public class TaskController : Controller
    {
        private readonly IUserSession _userSession;

        public TaskService Service { get; set; }

        public TaskController()
        {
            Service = new TaskService();
            _userSession = new UserSession();
        }

        [AllowAnonymous]
        [Route("Add/{projectID}/{deliverableID}")]
        public ActionResult Add(int projectID, int deliverableID)
        {
            TaskViewModel model = new TaskViewModel();
            DateTime startDate = DateTime.Now, endDate = DateTime.Now;

            model.IdProyecto = projectID;
            model.IdEntregable = deliverableID;

            model.FechaInicio = DateTime.Now;

            try
            {
                DeliverableService delService = new DeliverableService();
                var resultDeliverable = delService.GetDeliverableByID(deliverableID, projectID);

                // Se establece por defecto la fecha inicio del entregable
                model.FechaInicio = resultDeliverable.Data.FechaInicio;

                startDate = resultDeliverable.Data.FechaInicio;
                endDate = resultDeliverable.Data.FechaFin;

                // Se carga el listado del personal disponible para el proyecto
                PersonalService perService = new PersonalService();

                List<SelectListItem> selectList = new List<SelectListItem>();
                selectList.Add(new SelectListItem { Value = "0", Text = Labels.Select });

                var personalResult = perService.GetPersonalByProjectID(projectID);

                if (personalResult.MessageType == MessageType.SUCCESS)
                {
                    personalResult.Data.ForEach(p => selectList.Add(new SelectListItem()
                    {
                        Value = p.IdPersonal.ToString(),
                        Text = p.Nombre
                    }));

                    ViewBag.PersonalList = new SelectList(selectList, "Value", "Text");
                }
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            model.FechaFin = model.FechaInicio;

            ViewBag.StartDate = startDate;
            ViewBag.EndDate = endDate;

            ViewBag.Action = "SaveTask";
            ViewBag.Title = "Agregar Tarea";

            return View(model);
        }

        [AjaxOnly]
        [AllowAnonymous]
        public ActionResult CheckOption(TaskViewModel model)
        {
            DeliverableService delService = new DeliverableService();
            var resultDeliverable = delService.GetDeliverableByID(model.IdEntregable, model.IdProyecto);

            // Se establece por defecto la fecha inicio del entregable
            model.FechaInicio = resultDeliverable.Data.FechaInicio;
            model.FechaFin = resultDeliverable.Data.FechaInicio;

            ViewBag.StartDate = resultDeliverable.Data.FechaInicio;
            ViewBag.EndDate = resultDeliverable.Data.FechaFin;

            // Se carga el listado del personal disponible para el proyecto
            PersonalService perService = new PersonalService();

            List<SelectListItem> selectList = new List<SelectListItem>();
            selectList.Add(new SelectListItem { Value = "0", Text = Labels.Select });

            var personalResult = perService.GetPersonalByProjectID(model.IdProyecto);

            if (personalResult.MessageType == MessageType.SUCCESS)
            {
                personalResult.Data.ForEach(p => selectList.Add(new SelectListItem()
                {
                    Value = p.IdPersonal.ToString(),
                    Text = p.Nombre
                }));

                ViewBag.PersonalList = new SelectList(selectList, "Value", "Text");
            }

            if (model.DetConfiguracionTareaFrecuencia != null)
            {
                int countMaxDays = 0;
                ProjectService projectService = new ProjectService();
                var resultWorkingDaysPerWeek = projectService.GetWorkingDaysPerWeek(model.IdProyecto, ref countMaxDays);

                if (resultWorkingDaysPerWeek.MessageType == MessageType.INFO)
                {
                    if (model.DetConfiguracionTareaFrecuencia.TipoFrecuencia == 2)
                    {
                        var dic = resultWorkingDaysPerWeek.Data;

                        selectList = new List<SelectListItem>();
                        selectList.Add(new SelectListItem { Value = "0", Text = Labels.Select });

                        foreach (KeyValuePair<DayOfWeek, string> kvp in dic)
                        {
                            selectList.Add(new SelectListItem { Value = kvp.Key.ToString(), Text = kvp.Value });
                        }

                        ViewBag.DayOfWeek = new SelectList(selectList, "Value", "Text");
                        ViewBag.DurationDaysWeekly = countMaxDays;
                    }
                }
            }
            else
            {
                model.DetConfiguracionTareaFrecuencia = new ConfigFrequencyViewModel();
                model.DetConfiguracionTareaFrecuencia.TipoFrecuencia = 1;
            }

            model.DetConfiguracionTareaFrecuencia.DetIntervaloFrecuencia = new IntervaloFrecuency();
            model.DetConfiguracionTareaFrecuencia.DetIntervaloFrecuencia.TipoFinalizacion = 1;

            return PartialView("Add", model);
        }

        [AjaxOnly]
        [AllowAnonymous]
        public ActionResult LoadCollaboratorSection(TaskViewModel model)
        {
          return  PartialView("Partials/Task/_collaboratorList", model);
        }

        [AjaxOnly]
        [AllowAnonymous]
        public JsonResult GetCollaborator(int projectId, string selected, int manager)
        {
            ViewBag.Disabled = true;
            List<dynamic> data = new List<dynamic>();
            List<SelectListItem> selectList = new List<SelectListItem>();

            if (manager > 0)
            {
                ViewBag.Disabled = false;

                // Se carga el listado del personal disponible para el proyecto
                PersonalService perService = new PersonalService();

                //selectList.Add(new SelectListItem { Value = "0", Text = Labels.Select });

                var personalResult = perService.GetPersonalByProjectID(projectId);

                if (personalResult.MessageType == MessageType.SUCCESS)
                {
                    foreach (var item in personalResult.Data)
                    {
                        if (manager != item.IdPersonal)
                        {
                            dynamic info = new
                            {
                                id = item.IdPersonal.ToString(),
                                text = string.Join("-", item.Identificacion, item.Nombre),
                                disabled = ViewBag.Disabled,
                                selected = (selected != null && selected.Split(',').Contains(item.IdPersonal.ToString())) ? true : false
                            };

                            data.Add(info);

                            /*   selectList.Add(new SelectListItem()
                               {
                                   Value = item.IdPersonal.ToString(),
                                   Text = string.Join("-", item.Identificacion, item.Nombre)
                               });*/
                        }
                    }

                }
            }

            //  ViewBag.PersonalList = new SelectList(selectList, "Value", "Text");

            return new JsonResult
            {
                Data = new
                {
                    results = data
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            }; //PartialView("Partials/Task/_collaboratorList", model);
        }

        [AjaxOnly]
        [HttpPost]
        [Route("Save")]
        [AllowAnonymous]
        public ActionResult SaveTask(TaskViewModel model)
        {
            string message = string.Empty, status = "ERROR";

            try
            {
                if (model.DetConfiguracionTareaFrecuencia != null)
                {
                    if (model.DetConfiguracionTareaFrecuencia.TipoFrecuencia == 1)
                    {
                        ModelState.Remove("DetConfiguracionTareaFrecuencia.CantidadSemanas");
                        ModelState.Remove("DetConfiguracionTareaFrecuencia.DiaInicio");
                        ModelState.Remove("DetConfiguracionTareaFrecuencia.DuracionDias");
                    }
                    else
                    {
                        ModelState.Remove("DetConfiguracionTareaFrecuencia.CantidadDias");
                        ModelState.Remove("DetConfiguracionTareaFrecuencia.HoraInicio");
                        ModelState.Remove("DetConfiguracionTareaFrecuencia.HoraFin");
                    }
                }

                if (ModelState.IsValid)
                {
                    model.IdUsuarioIns = _userSession.UserId;

                    Entity.MstTareas task = new Entity.MstTareas
                    {
                        IdEntregable = model.IdEntregable,
                        IdProyecto = model.IdProyecto,
                        Nombre = model.Nombre,
                        FechaInicio = model.FechaInicio,
                        FechaFin = model.FechaFin,
                        Hito = model.Hito,
                        TareaPeriodica = model.TareaPeriodica,
                        Nota = model.Nota,
                        IdEstado = model.IdEstado,
                        IdUsuarioIns = model.IdUsuarioIns,
                        FechaIns = model.FechaIns
                    };

                    task.DetTareaPersonal = new List<Entity.DetTareaPersonal>();

                    if (model.Responsable > 0)
                    {
                        task.DetTareaPersonal.Add(new Entity.DetTareaPersonal
                        {
                            IdPersonal = model.Responsable,
                            IdProyecto = model.IdProyecto,
                            IdEntregable = model.IdEntregable,
                            TipoPersonal = "R"
                        });
                    }
                    else
                    {
                        if (!model.Hito)
                        {
                            message = "Se debe proporcionar el responsable de la tarea";
                            status = "ERROR";
                            goto @return;
                        }
                    }

                    if (model.Colaboradores != null && model.Colaboradores.Count > 0)
                    {
                        model.Colaboradores.ForEach(id => task.DetTareaPersonal.Add(new Entity.DetTareaPersonal
                        {
                            IdPersonal = int.Parse(id),
                            IdProyecto = model.IdProyecto,
                            IdEntregable = model.IdEntregable,
                            TipoPersonal = "C"
                        }));
                    }

                    //task.MstPresupuesto = new List<Entity.MstPresupuesto>();
                    //task.MstPresupuesto.Add(new Entity.MstPresupuesto
                    //{
                    //    PresupuestoAsignado = model.TareaPeriodica ? 0 : model.Presupuesto,
                    //    IdProyecto = model.IdProyecto,
                    //    IdEntregable = model.IdEntregable,
                    //    IdUsuarioIns = _userSession.UserId,
                    //    FechaIns = DateTime.Now,
                    //    Activo = true
                    //});

                    if (model.DetConfiguracionTareaFrecuencia != null)
                    {
                        if (model.DetConfiguracionTareaFrecuencia.HoraFin.HasValue &&
                            model.DetConfiguracionTareaFrecuencia.HoraInicio.HasValue)
                        {
                            if (model.DetConfiguracionTareaFrecuencia.HoraFin.Value.CompareTo(model.DetConfiguracionTareaFrecuencia.HoraInicio.Value) <= 0)
                            {
                                message = "La hora fin debe ser mayor a la hora inicio";
                                status = "ERROR";
                                goto @return;
                            }
                        }

                        task.DetConfiguracionTareaFrecuencia = new List<Entity.DetConfiguracionTareaFrecuencia>();
                        task.DetConfiguracionTareaFrecuencia.Add(new Entity.DetConfiguracionTareaFrecuencia
                        {
                            CantidadAnios = model.DetConfiguracionTareaFrecuencia.CantidadAnios,
                            CantidadDias = model.DetConfiguracionTareaFrecuencia.CantidadDias,
                            CantidadMeses = model.DetConfiguracionTareaFrecuencia.CantidadMeses,
                            CantidadSemanas = model.DetConfiguracionTareaFrecuencia.CantidadSemanas,
                            DiaDelMes = model.DetConfiguracionTareaFrecuencia.DiaDelMes,
                            DiaInicio = model.DetConfiguracionTareaFrecuencia.DiaInicio,
                            DuracionDias = model.DetConfiguracionTareaFrecuencia.DuracionDias,
                            HoraFin = model.DetConfiguracionTareaFrecuencia.HoraFin,
                            HoraInicio = model.DetConfiguracionTareaFrecuencia.HoraInicio,
                            IdEntregable = model.IdEntregable,
                            IdProyecto = model.IdProyecto,
                            Mes = model.DetConfiguracionTareaFrecuencia.Mes,
                            NumeroSemana = model.DetConfiguracionTareaFrecuencia.NumeroSemana,
                            TipoFrecuencia = model.DetConfiguracionTareaFrecuencia.TipoFrecuencia
                        });

                        task.DetConfiguracionTareaFrecuencia.First().DetIntervaloFrecuencia = new List<Entity.DetIntervaloFrecuencia>();
                        task.DetConfiguracionTareaFrecuencia.First().DetIntervaloFrecuencia.Add(new Entity.DetIntervaloFrecuencia
                        {
                            CantidadRepeticiones = model.DetConfiguracionTareaFrecuencia.DetIntervaloFrecuencia.CantidadRepeticiones,
                            FechaFin = model.DetConfiguracionTareaFrecuencia.DetIntervaloFrecuencia.FechaFin,
                            FechaInicio = model.DetConfiguracionTareaFrecuencia.DetIntervaloFrecuencia.FechaInicio,
                            TipoFinalizacion = model.DetConfiguracionTareaFrecuencia.DetIntervaloFrecuencia.TipoFinalizacion
                        });
                    }

                    var result = Service.InsertTask(task);

                    if (result.MessageType != MessageType.SUCCESS)
                    {
                        message = result.Message;
                        Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                                MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                    }
                    else
                        message = string.Format(Messages.SaveSuccess, "tarea");

                    status = result.MessageType.ToString();
                }
                else
                {
                    message = Messages.DataRequired;
                    status = "ERROR";
                }
            }
            catch (Exception ex)
            {
                message = "Ocurrió un error al intentar guardar la tarea";
                status = "ERROR";
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

        @return:
            return new JsonResult
            {
                Data = new { status, message, action = Url.Action("Planning", "Project", new { projectID = model.IdProyecto }) },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [AjaxOnly]
        [HttpPost]
        [Route("Delete/{taskID}")]
        [AllowAnonymous]
        public ActionResult DeleteTask(int taskID)
        {
            TaskService service = new TaskService();

            var result = service.DeleteTask(taskID);

            if (result.MessageType == MessageType.SUCCESS)
                TempData["MessageDel"] = result.Message;
            else
                Logger.Log(MessageType.ERROR, result.DetailException, _userSession.Username,
                    MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);

            return new JsonResult
            {
                Data = new { status = result.MessageType.ToString(), message = result.Message },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [AllowAnonymous]
        [Route("Edit/{taskID}/{deliverableID}/{projectID}")]
        public ActionResult EditPartial(int taskID, int deliverableID, int projectID)
        {
            TaskViewModel model = new TaskViewModel();
            DateTime startDate = DateTime.Now, endDate = DateTime.Now;

            try
            {
                var result = Service.GetTaskById(taskID, deliverableID, projectID);

                if (result.MessageType == MessageType.INFO)
                {
                    model = new TaskViewModel
                    {
                        Nombre = result.Data.Nombre,
                        FechaInicio = result.Data.FechaInicio,
                        FechaFin = result.Data.FechaFin,
                        Nota = result.Data.Nota,
                        IdProyecto = result.Data.IdProyecto,
                        IdEntregable = result.Data.IdEntregable,
                        IdTarea = result.Data.IdTarea,
                        Hito = result.Data.Hito
                    };

                    DeliverableService dService = new DeliverableService();
                    var resultDeliverable = dService.GetDeliverableByID(deliverableID, projectID);

                    startDate = resultDeliverable.Data.FechaInicio;
                    endDate = resultDeliverable.Data.FechaFin;

                    if (result.Data.DetTareaPersonal != null && result.Data.DetTareaPersonal.Count > 0)
                    {
                        if (result.Data.DetTareaPersonal.Any(p => p.TipoPersonal == "R"))
                            model.Responsable = result.Data.DetTareaPersonal.SingleOrDefault(p => p.TipoPersonal == "R").IdPersonal;

                        if (result.Data.DetTareaPersonal.Any(p => p.TipoPersonal == "C"))
                        {
                            model.Colaboradores = new List<string>();
                            foreach (var item in result.Data.DetTareaPersonal)
                            {
                                model.Colaboradores.Add(item.IdPersonal.ToString());
                            }
                        }
                    }


                    // Se carga el listado del personal disponible para el proyecto
                    PersonalService perService = new PersonalService();

                    List<SelectListItem> selectList = new List<SelectListItem>();
                    selectList.Add(new SelectListItem { Value = "0", Text = Labels.Select });

                    var personalResult = perService.GetPersonalByProjectID(projectID);

                    if (personalResult.MessageType == MessageType.SUCCESS)
                    {
                        personalResult.Data.ForEach(p => selectList.Add(new SelectListItem()
                        {
                            Value = p.IdPersonal.ToString(),
                            Text = p.Nombre
                        }));

                        ViewBag.PersonalList = new SelectList(selectList, "Value", "Text");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            ViewBag.StartDate = startDate;
            ViewBag.EndDate = endDate;
            ViewBag.Action = "Update";

            return PartialView("Partials/Task/_addTask", model);
        }

        [AjaxOnly]
        [HttpPost]
        [AllowAnonymous]
        [Route("Update")]
        public ActionResult UpdateTask(TaskViewModel model)
        {
            string message = string.Empty, status = "ERROR";

            try
            {
                if (model.DetConfiguracionTareaFrecuencia != null)
                {
                    if (model.DetConfiguracionTareaFrecuencia.TipoFrecuencia == 1)
                    {
                        ModelState.Remove("DetConfiguracionTareaFrecuencia.CantidadSemanas");
                        ModelState.Remove("DetConfiguracionTareaFrecuencia.DiaInicio");
                        ModelState.Remove("DetConfiguracionTareaFrecuencia.DuracionDias");
                    }
                    else
                    {
                        ModelState.Remove("DetConfiguracionTareaFrecuencia.CantidadDias");
                        ModelState.Remove("DetConfiguracionTareaFrecuencia.HoraInicio");
                        ModelState.Remove("DetConfiguracionTareaFrecuencia.HoraFin");
                    }
                }

                if (ModelState.IsValid)
                {
                    model.IdUsuarioIns = _userSession.UserId;

                    Entity.MstTareas task = new Entity.MstTareas
                    {
                        IdEntregable = model.IdEntregable,
                        IdProyecto = model.IdProyecto,
                        IdTarea = model.IdTarea,
                        Nombre = model.Nombre,
                        FechaInicio = model.FechaInicio,
                        FechaFin = model.FechaFin,
                        Hito = model.Hito,
                        TareaPeriodica = model.TareaPeriodica,
                        Nota = model.Nota,
                        IdEstado = model.IdEstado,
                        IdUsuarioIns = model.IdUsuarioIns,
                        FechaIns = model.FechaIns
                    };

                    task.DetTareaPersonal = new List<Entity.DetTareaPersonal>();

                    if (model.Responsable > 0)
                    {
                        task.DetTareaPersonal.Add(new Entity.DetTareaPersonal
                        {
                            IdPersonal = model.Responsable,
                            IdProyecto = model.IdProyecto,
                            IdTarea = model.IdTarea,
                            IdEntregable = model.IdEntregable,
                            TipoPersonal = "R"
                        });
                    }
                    else
                    {
                        if (!model.Hito)
                        {
                            message = "Se debe proporcionar el responsable de la tarea";
                            status = "ERROR";
                            goto @return;
                        }
                        else
                            task.FechaFin = model.FechaInicio;
                    }

                    if (model.Colaboradores != null && model.Colaboradores.Count > 0)
                    {
                        model.Colaboradores.ForEach(id => task.DetTareaPersonal.Add(new Entity.DetTareaPersonal
                        {
                            IdPersonal = int.Parse(id),
                            IdTarea = model.IdTarea,
                            IdProyecto = model.IdProyecto,
                            IdEntregable = model.IdEntregable,
                            TipoPersonal = "C"
                        }));
                    }

                    //task.MstPresupuesto = new List<Entity.MstPresupuesto>();
                    //task.MstPresupuesto.Add(new Entity.MstPresupuesto
                    //{
                    //    PresupuestoAsignado = model.TareaPeriodica ? 0 : model.Presupuesto,
                    //    IdProyecto = model.IdProyecto,
                    //    IdEntregable = model.IdEntregable,
                    //    IdUsuarioIns = _userSession.UserId,
                    //    FechaIns = DateTime.Now,
                    //    Activo = true
                    //});

                    if (model.DetConfiguracionTareaFrecuencia != null)
                    {
                        if (model.DetConfiguracionTareaFrecuencia.HoraFin.HasValue &&
                            model.DetConfiguracionTareaFrecuencia.HoraInicio.HasValue)
                        {
                            if (model.DetConfiguracionTareaFrecuencia.HoraFin.Value.CompareTo(model.DetConfiguracionTareaFrecuencia.HoraInicio.Value) <= 0)
                            {
                                message = "La hora fin debe ser mayor a la hora inicio";
                                status = "ERROR";
                                goto @return;
                            }
                        }

                        task.DetConfiguracionTareaFrecuencia = new List<Entity.DetConfiguracionTareaFrecuencia>();
                        task.DetConfiguracionTareaFrecuencia.Add(new Entity.DetConfiguracionTareaFrecuencia
                        {
                            CantidadAnios = model.DetConfiguracionTareaFrecuencia.CantidadAnios,
                            CantidadDias = model.DetConfiguracionTareaFrecuencia.CantidadDias,
                            CantidadMeses = model.DetConfiguracionTareaFrecuencia.CantidadMeses,
                            CantidadSemanas = model.DetConfiguracionTareaFrecuencia.CantidadSemanas,
                            DiaDelMes = model.DetConfiguracionTareaFrecuencia.DiaDelMes,
                            DiaInicio = model.DetConfiguracionTareaFrecuencia.DiaInicio,
                            DuracionDias = model.DetConfiguracionTareaFrecuencia.DuracionDias,
                            HoraFin = model.DetConfiguracionTareaFrecuencia.HoraFin,
                            HoraInicio = model.DetConfiguracionTareaFrecuencia.HoraInicio,
                            IdEntregable = model.IdEntregable,
                            IdProyecto = model.IdProyecto,
                            Mes = model.DetConfiguracionTareaFrecuencia.Mes,
                            NumeroSemana = model.DetConfiguracionTareaFrecuencia.NumeroSemana,
                            TipoFrecuencia = model.DetConfiguracionTareaFrecuencia.TipoFrecuencia
                        });

                        task.DetConfiguracionTareaFrecuencia.First().DetIntervaloFrecuencia = new List<Entity.DetIntervaloFrecuencia>();
                        task.DetConfiguracionTareaFrecuencia.First().DetIntervaloFrecuencia.Add(new Entity.DetIntervaloFrecuencia
                        {
                            CantidadRepeticiones = model.DetConfiguracionTareaFrecuencia.DetIntervaloFrecuencia.CantidadRepeticiones,
                            FechaFin = model.DetConfiguracionTareaFrecuencia.DetIntervaloFrecuencia.FechaFin,
                            FechaInicio = model.DetConfiguracionTareaFrecuencia.DetIntervaloFrecuencia.FechaInicio,
                            TipoFinalizacion = model.DetConfiguracionTareaFrecuencia.DetIntervaloFrecuencia.TipoFinalizacion
                        });
                    }

                    var result = Service.UpdateTask(task);

                    if (result.MessageType != MessageType.SUCCESS)
                    {
                        message = result.Message;
                        Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                                MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                    }
                    else
                        message = string.Format(Messages.UpdateSuccess, "tarea");

                    status = result.MessageType.ToString();
                }
                else
                {
                    message = Messages.DataRequired;
                    status = "ERROR";
                }

            }
            catch (Exception ex)
            {
                message = "Ocurrió un error al intentar actualizar la tarea";
                status = "ERROR";
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

        @return:
            return new JsonResult
            {
                Data = new { status, message, action = Url.Action("Planning", "Project", new { projectID = model.IdProyecto }) },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

    }
}