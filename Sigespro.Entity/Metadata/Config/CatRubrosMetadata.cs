﻿using Sigespro.Entity.Resource;
using System.ComponentModel.DataAnnotations;

namespace Sigespro.Entity.Metadata.Config
{
    public class CatRubrosMetadata
    {
        //[Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [Key]
        [StringLength(5)]
        public string IdRubro { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string Nombre { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public int IdTipoRubro { get; set; }

        [StringLength(5)]
        public string IdRubroParent { get; set; }
    }
}
