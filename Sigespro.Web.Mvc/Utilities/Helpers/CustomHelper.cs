﻿using Common.Extensions;
using Common.Helpers;
using Common.Log;
using Sigespro.Web.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sigespro.Web.Mvc.Utilities.Helpers
{
    public static class CustomHelper
    {
        public static MvcHtmlString IsDisabled(this HtmlHelper htmlString, bool disabled)
        {
            if (disabled)
                return new MvcHtmlString("disabled=\"disabled\" ");
            else
                return new MvcHtmlString("");
        }

        /// <summary>
        /// Establece la el atributo disabled en la etiqueta html
        /// </summary>
        /// <param name="htmlString"></param>
        /// <param name="disabled"></param>
        /// <returns></returns>
        public static MvcHtmlString IsDisabled(this MvcHtmlString htmlString, bool disabled)
        {
            string rawstring = htmlString.ToString();

            if (disabled)
            {
                string tag = rawstring.Substring(1, rawstring.IndexOf(" ")).Trim().ToLower();

                if (tag == "select")
                    rawstring = rawstring.Replace("<select", "<select disabled=\"disabled\" ");
                else
                    rawstring = rawstring.Insert(rawstring.IndexOf(" ") + 1, "disabled=\"disabled\" ");
            }

            return new MvcHtmlString(rawstring);
        }

        /// <summary>
        /// Establece la el atributo readonly en la etiqueta html
        /// </summary>
        /// <param name="htmlString"></param>
        /// <param name="readonly"></param>
        /// <returns></returns>
        public static MvcHtmlString IsReadonly(this MvcHtmlString htmlString, bool @readonly)
        {
            string rawstring = htmlString.ToString();

            if (@readonly)
            {
                string tag = rawstring.Substring(1, rawstring.IndexOf(" ")).Trim().ToLower();

                if (tag == "select")
                    rawstring = rawstring.Replace("option", "option readonly=\"readonly\" ");
                else
                    rawstring = rawstring.Insert(rawstring.IndexOf(" ") + 1, "readonly=\"readonly\" ");
            }
            return new MvcHtmlString(rawstring);
        }

        public static MvcHtmlString SetProperty(this HtmlHelper htmlString, bool condition, string propertyName, string propertyValue = "")
        {
            if (condition)
                return new MvcHtmlString(propertyName + "='" + propertyValue + "' ");
            else
                return new MvcHtmlString("");
        }

        /// <summary>
        /// Establece una o mas clases css a un elemento por una condicion dada
        /// </summary>
        /// <param name="htmlString"></param>
        /// <param name="condition">Condición a cumplir</param>
        /// <param name="ccsClass">Clase a establecer si la condición es verdadera</param>
        /// <param name="isNotValidCssClass">Parámetro opcional, en caso que no sea verdadera la condición se establece esta clase css</param>
        /// <returns>String => class:"clase"</returns>
        public static MvcHtmlString SetCssClass(this HtmlHelper htmlString, bool condition, string ccsClass, string isNotValidCssClass = "")
        {
            if (condition)
                return new MvcHtmlString("class='" + ccsClass + "' ");
            else
                return new MvcHtmlString("class='" + isNotValidCssClass + "' ");
        }

        public static bool ValidateAccess(this HtmlHelper htmlHelper, string action)
        {
            try
            {
                if (HttpContext.Current.Session["UserPermission"] != null)
                {
                    List<OptionsViewModel> listUserPermission = (List<OptionsViewModel>)HttpContext.Current.Session["UserPermission"];

                    if (listUserPermission.Any(p => !string.IsNullOrEmpty(p.OptionName) && p.OptionName.Equals(action)))
                        return true;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message, "", "ValidateAccess - CustomHtmlHelper", HttpContext.Current.Request.UserHostAddress);
            }

            return false;
        }

        public static MvcHtmlString Sidebar(this HtmlHelper html, IEnumerable<MenuItemViewModel> items)
        {
            TagBuilder ulModule = new TagBuilder("ul");
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);

            foreach (var module in items)
            {
                if (module.ItemParentId.HasValue)
                    continue;

                TagBuilder liModule = new TagBuilder("li");

                TagBuilder hiperLink = new TagBuilder("a");
                /*
                if (!string.IsNullOrWhiteSpace(module.CssClass))
                {
                    TagBuilder iTag = new TagBuilder("i");
                    iTag.AddCssClass(module.CssClass);
                    hiperLink.InnerHtml = iTag.ToString();
                    hiperLink.InnerHtml += " " + module.Label;
                }
                else*/
                    hiperLink.SetInnerText(module.Label);

                var children = items.Where(i => i.ItemParentId == module.MenuItemId).ToList();

                if (children.Count > 0)
                {
                    TagBuilder ul = new TagBuilder("ul");
                    TagBuilder span = new TagBuilder("span");

                    liModule.AddCssClass("dropdown");

                    hiperLink.AddCssClass("main-item dropdown-toggle collapsed");
                    hiperLink.Attributes.Add("href", "javascript:void(0)");
                    hiperLink.Attributes.Add("data-target", "#" + module.Label.ClearString());
                    hiperLink.Attributes.Add("data-toggle", "collapse");

                    ul.Attributes.Add("id", module.Label.ClearString());
                    ul.AddCssClass("nav nav-stacked collapse left-submenu");

                    span.AddCssClass("glyphicon");

                    hiperLink.InnerHtml += span.ToString();

                    foreach (var child in children)
                    {
                        TagBuilder liChildItem = new TagBuilder("li");
                        TagBuilder hiperChildLink = new TagBuilder("a");

                        hiperChildLink.SetInnerText(child.Label);

                        if (string.IsNullOrEmpty(child.Url))
                            hiperChildLink.Attributes.Add("href", urlHelper.Action(child.Action, child.Controller));
                        else
                            hiperChildLink.Attributes.Add("href", child.Url);

                        liChildItem.InnerHtml = hiperChildLink.ToString();

                        ul.InnerHtml += liChildItem.ToString();
                    }

                    liModule.InnerHtml = hiperLink.ToString();
                    liModule.InnerHtml += ul;
                }
                else
                {
                    hiperLink.AddCssClass("main-item");
                    if (string.IsNullOrEmpty(module.Url))
                        hiperLink.Attributes.Add("href", urlHelper.Action(module.Action, module.Controller));
                    else
                        hiperLink.Attributes.Add("href", module.Url);

                    liModule.InnerHtml = hiperLink.ToString();
                }

                ulModule.InnerHtml += liModule.ToString();
            }

            ulModule.AddCssClass("sidebar-nav");

            return new MvcHtmlString(ulModule.ToString());
        }
    }
}