﻿using Common.Helpers;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security.OAuth;
using Sigespro.Entity;
using Sigespro.Service.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace Sigespro.API.Utilities.Provider
{
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
        //    Result<Users> result = null;
        //    context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

        //    using (var service = new UserService())
        //    {
        //        result = await service.Authenticate(context.UserName, context.Password, context.Request.LocalIpAddress);

        //        if (result.MessageType != MessageType.SUCCESS)
        //        {
        //            context.SetError("invalid_grant", result.Message);
        //            return;
        //        }
        //    }

        //    var identity = new ClaimsIdentity(context.Options.AuthenticationType);
        //    identity.AddClaim(new Claim("role", "user"));
        //    identity.AddClaim(new Claim("email", result.Data.Email));
        //    identity.AddClaim(new Claim("sub", result.Data.UserName));
        //    identity.AddClaim(new Claim("fullname", result.Data.FullName));

        //    context.Validated(identity);

        }
    }
}