﻿using Sigespro.Entity.Metadata.Config;
using Sigespro.Entity.Metadata.Pro;
using System.ComponentModel.DataAnnotations;

namespace Sigespro.Entity
{
    [MetadataType(typeof(CatArticulosMetadata))]
    public partial class CatArticulos
    {
    }

    [MetadataType(typeof(CatFeriadosMetadata))]
    public partial class CatFeriados
    {
    }

    [MetadataType(typeof(CatMonedaMetadata))]
    public partial class CatMoneda
    {
    }

    [MetadataType(typeof(CatRespuestasMetadata))]
    public partial class CatRespuestas
    {
    }

    [MetadataType(typeof(CatRubrosMetadata))]
    public partial class CatRubros
    {
    }

    [MetadataType(typeof(CatTasaCambioMetadata))]
    public partial class CatTasaCambio
    {
    }

    [MetadataType(typeof(DetCatalogoMetadata))]
    public partial class DetCatalogo
    {
    }

    [MetadataType(typeof(MstProyectosMetadata))]
    public partial class MstProyectos
    {
    }

}
