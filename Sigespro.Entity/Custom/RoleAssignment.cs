﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigespro.Entity.Custom
{
    public class RoleAssignment
    {
        public int RoleID { get; set; }

        public int AssignmentID { get; set; }

        public string Description { get; set; }

        public bool IsChecked { get; set; }
    }
}
