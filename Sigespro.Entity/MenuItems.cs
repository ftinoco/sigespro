//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sigespro.Entity
{
    using System;
    using System.Collections.Generic;
     
    public partial class MenuItems
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MenuItems()
        {
            this.MenuItems1 = new HashSet<MenuItems>();
            this.RoleMenuItem = new HashSet<RoleMenuItem>();
        }
    
        public int MenuItemId { get; set; }
        public int MenuId { get; set; }
        public Nullable<int> ItemParentId { get; set; }
        public Nullable<int> ActionId { get; set; }
        public string Label { get; set; }
        public string Icon { get; set; }
        public string CssClass { get; set; }
        public bool Active { get; set; }
        public int UserInsert { get; set; }
        public System.DateTime InsertDate { get; set; }
        public Nullable<int> UserUpdate { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
    
        public virtual Actions Actions { get; set; }
        public virtual Menu Menu { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MenuItems> MenuItems1 { get; set; }
        public virtual MenuItems MenuItems2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RoleMenuItem> RoleMenuItem { get; set; }
    }
}
