﻿using Common.Helpers;
using Common.Log;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Newtonsoft.Json;
using Sigespro.Entity;
using Sigespro.Entity.Custom;
using Sigespro.Service.Admin;
using Sigespro.Service.Config;
using Sigespro.Service.Pro;
using Sigespro.Service.Security;
using Sigespro.Web.Mvc.Models;
using Sigespro.Web.Mvc.Models.Home;
using Sigespro.Web.Mvc.Models.Project;
using Sigespro.Web.Mvc.Models.Project.Tasks;
using Sigespro.Web.Mvc.Utilities.Attributes.Security;
using Sigespro.Web.Mvc.Utilities.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Sigespro.Web.Mvc.Controllers
{
    [RoutePrefix("home")]
    public class HomeController : Controller
    {
        private readonly IUserSession _userSession;

        public HomeController()
        {
            _userSession = new UserSession();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Index(int projectId)
        {
            TempData["ProjectID"] = projectId;

            return RedirectToActionPermanent("Index");
        }

        public ActionResult Index()
        {
            try
            {
                //Response.AddHeader("Refresh", "35");

                // Se obtiene el nombre del usuario para el mensaje de bienvenida
                ViewBag.UserName = _userSession.FullName.Split(new char[1] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[0];

                ProjectService projectService = new ProjectService();

                // Se obtiene el listado de proyectos en ejecución en los cuales el usuario conectado tiene asignaciones
                var result = projectService.GetProjectsByPersonalID(_userSession.PersonalId);

                if (result.MessageType == MessageType.INFO)
                {
                    string strSerialize = JsonConvert.SerializeObject(result.Data);

                    var listProject = JsonConvert.DeserializeObject<List<ProjectViewModel>>(strSerialize);

                    ProjectViewModel firstProject = null;
                    if (TempData["ProjectID"] != null)
                        firstProject = listProject.FirstOrDefault(x => x.IdProyecto == (int)TempData["ProjectID"]);
                    else
                        firstProject = listProject.FirstOrDefault();
                    ViewBag.SelectedProject = firstProject;

                    List<ProjectViewModel> lstTemp = new List<ProjectViewModel>();
                    listProject.ForEach(x =>
                    {
                        if (!lstTemp.Exists(y => y.IdProyecto == x.IdProyecto))
                            lstTemp.Add(x);
                    });
                    ViewBag.ListProject = lstTemp;

                    // Se obtiene el listado del personal involucrado en el proyecto
                    PersonalService personalService = new PersonalService();
                    var personalResult = personalService.GetPersonalByProjectID(firstProject.IdProyecto);

                    if (personalResult.MessageType == MessageType.SUCCESS)
                        ViewBag.StaffList = personalResult.Data;

                    TaskService taskService = new TaskService();
                    var taskResult = taskService.GetTaskByProjectId(firstProject.IdProyecto);

                    if (taskResult.MessageType == MessageType.INFO)
                    {
                        ViewBag.TaskCount = taskResult.Data.Count;

                        int taskComplete = 0;

                        DetailCatalogService catalogService = new DetailCatalogService();
                        var catalogResult = catalogService.GetDetailsByTableAndValue("EstadoTareas", "Ejecutada");

                        if (catalogResult.MessageType == MessageType.INFO)
                        {
                            foreach (var item in taskResult.Data)
                            {
                                if (item.IdEstado == catalogResult.Data.IdDetCatalogo)
                                    taskComplete = taskComplete + 1;
                            }
                        }

                        ViewBag.TaskComplete = taskComplete;
                    }

                }
                // Si no se encontraron proyectos en ejecución en los que el usuario actual este vinculado
                else if (result.MessageType == MessageType.WARNING)
                {
                    // Se redirecciona a la pantalla de perfil de usuario
                    return RedirectToActionPermanent("UserProfile", "Account");
                }
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return View();
        }

        [AllowAnonymous]
        [ChildActionOnly]
        public PartialViewResult CreateSidebar()
        {
            List<MenuItemViewModel> menuItems = new List<MenuItemViewModel>();

            SecurityService service = new SecurityService();

            var resultMenuItems = service.GetMenuItemByUser(_userSession.Username);

            if (resultMenuItems.MessageType == MessageType.SUCCESS)
            {
                resultMenuItems.Data.ForEach(p => menuItems.Add(new MenuItemViewModel
                {
                    Action = p.Action,
                    ActionId = p.ActionId,
                    Controller = p.Controller,
                    CssClass = p.CssClass,
                    Icon = p.Icon,
                    ItemParentId = p.ItemParentId,
                    Label = p.Label,
                    MenuId = p.MenuId,
                    MenuItemId = p.MenuItemId,
                    MenuName = p.MenuName,
                    RoleId = p.RoleId,
                    SystemId = p.SystemId,
                    Url = p.Url,
                    UserId = p.UserId,
                    UserName = p.UserName
                }));
            }

            return PartialView("Partials/Sidebar", menuItems);
        }

        #region Funciones para dashboard de pagina de inicio

        [AllowAnonymous]
        public ActionResult ProjectExecution(int projectID)
        {
            List<DeliverableInfo> model = new List<DeliverableInfo>();

            // Se carga el listado de tareas en ejecución
            DeliverableService deliverableService = new DeliverableService();
            var executionResult = deliverableService.GetDeliverablesInExecution(projectID, _userSession.PersonalId);

            if (executionResult.MessageType == MessageType.INFO)
            {
                model = executionResult.Data;
            }

            return PartialView("Partials/Home/_projectExecution", model);
        }


        #region Tareas

        [AjaxOnly]
        [AllowAnonymous]
        public JsonResult GetTaskByDeliverables(int projectID)
        {
            List<dynamic> data = new List<dynamic>();

            try
            {
                TaskService taskService = new TaskService();

                // Se obtiene la información de la ejecución del proyecto
                var result = taskService.GetVwTaskByProject(projectID);

                if (result.MessageType == MessageType.INFO)
                {
                    var y = new List<Entity.vwTareasPorProyecto>();
                    var z = result.Data;
                    z.OrderBy(x => x.Row_).ToList().ForEach(c =>
                    {
                        if (!y.Exists(a => a.Entregable == c.Entregable))
                        {
                            y.Add(c);
                            data.Add(new
                            {
                                text = new string(c.Entregable.Take(25).ToArray()),
                                count = z.Where(w => w.Entregable == c.Entregable).Max(b => b.Row_)
                            });

                        }
                    });
                }
                else
                    return new JsonResult { Data = new { message = "Ocurrió un error al intentar obtener la información del proyecto", status = "Error" } };
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult { Data = data, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [AllowAnonymous]
        public PartialViewResult GetYesterdayTasks(int projectID)
        {
            TaskDashboardViewModel model = new TaskDashboardViewModel();
            model.ShowFooter = true;
            model.Header = "Ayer";

            TaskService taskService = new TaskService();
            var taskResult = taskService.GetCountTaskByProjectAndDate(projectID, (DateTime.Now.AddDays(-1)));

            if (taskResult.MessageType == MessageType.INFO)
            {
                foreach (var item in taskResult.Data)
                {
                    if (item.Key == "Canceled")
                        model.TasksCanceled = item.Value;

                    if (item.Key == "Finished")
                        model.TasksFinished = item.Value;

                    if (item.Key == "InAction")
                        model.TasksInAction = item.Value;

                    if (item.Key == "Task")
                        model.TasksCount = item.Value;
                }
            }

            return PartialView("Partials/Home/_taskInfo", model);
        }

        [AllowAnonymous]
        public PartialViewResult GetTodayTasks(int projectID)
        {
            TaskDashboardViewModel model = new TaskDashboardViewModel();
            model.ShowFooter = true;
            model.Header = "Hoy";

            TaskService taskService = new TaskService();
            var taskResult = taskService.GetCountTaskByProjectAndDate(projectID, DateTime.Now);

            if (taskResult.MessageType == MessageType.INFO)
            {
                foreach (var item in taskResult.Data)
                {
                    if (item.Key == "Canceled")
                        model.TasksCanceled = item.Value;

                    if (item.Key == "Finished")
                        model.TasksFinished = item.Value;

                    if (item.Key == "InAction")
                        model.TasksInAction = item.Value;

                    if (item.Key == "Task")
                        model.TasksCount = item.Value;
                }
            }

            return PartialView("Partials/Home/_taskInfo", model);
        }

        [AllowAnonymous]
        public PartialViewResult GetTomorrowTasks(int projectID)
        {
            TaskDashboardViewModel model = new TaskDashboardViewModel();
            model.Header = "Mañana";

            TaskService taskService = new TaskService();
            var taskResult = taskService.GetCountTaskByProjectAndDate(projectID, DateTime.Now.AddDays(1));

            if (taskResult.MessageType == MessageType.INFO)
            {
                foreach (var item in taskResult.Data)
                {
                    if (item.Key == "Task")
                        model.TasksCount = item.Value;
                }
            }

            return PartialView("Partials/Home/_taskInfo", model);
        }

        [AllowAnonymous]
        public PartialViewResult GetOverdueTasks(int projectID)
        {
            TaskDashboardViewModel model = new TaskDashboardViewModel();
            model.Header = "Retrasadas";
            model.ShowFooter = false;
            model.Overdue = true;

            ViewBag.CssClass = "danger";
            ViewBag.ProjectID = projectID;

            TaskService taskService = new TaskService();
            var taskResult = taskService.GetCountOverdueTask(projectID);

            if (taskResult.MessageType == MessageType.INFO)
                model.TasksCount = taskResult.Data;

            return PartialView("Partials/Home/_taskInfo", model);
        }

        [AjaxOnly]
        [AllowAnonymous]
        public JsonResult GetTaskFinishedWeekly(int projectID)
        {
            dynamic data = new { };

            try
            {
                List<string> lstDays = new List<string>();
                TaskService taskService = new TaskService();

                var result = taskService.GetTasksFinishedWeekly(projectID, ref lstDays);

                List<dynamic> source = new List<dynamic>();

                if (result.MessageType == MessageType.SUCCESS)
                {
                    foreach (KeyValuePair<string, List<int>> kvp in result.Data)
                    {
                        source.Add(new
                        {
                            name = kvp.Key,
                            data = kvp.Value
                        });
                    }

                    data = new
                    {
                        daysOfWeek = lstDays,
                        source = source
                    };
                }
                else
                    return new JsonResult { Data = new { message = "Ocurrió un error al intentar obtener la información del proyecto", status = "Error" } };
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult { Data = data, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpGet]
        [AllowAnonymous]
        public void ShowReportOverdueTasks(int projectId)
        {
            try
            {
                string strRptPath = System.IO.Path.Combine(Server.MapPath("~/Reports/Project"), "rptOverdueTask.rpt");

                ReportDocument reportDocument = new ReportDocument();

                reportDocument.Load(strRptPath);

                ProjectService service = new ProjectService();
                var result = service.GetProjectByIdForReport(projectId);

                if (result.MessageType == MessageType.INFO)
                {
                    TaskService taskService = new TaskService();
                    var rs = taskService.GetOverdueTaskByProject(projectId);
                    List<ListOverdueTaskViewModel> lst = new List<ListOverdueTaskViewModel>();
                    foreach (var item in rs.Data)
                    {
                        lst.Add(new ListOverdueTaskViewModel()
                        {
                            Descripcion = item.Descripcion,
                            DiasRetrasados = item.DiasRetrasados.HasValue ? item.DiasRetrasados.Value : 0,
                            FechaFin = item.FechaFin,
                            FechaInicio = item.FechaInicio,
                            IdEntregable = item.IdEntregable,
                            IdProyecto = item.IdProyecto,
                            IdTarea = item.IdTarea,
                            Nombre = item.Nombre,
                            NombrePersona = item.NombrePersona
                        });
                    }
                    reportDocument.SetDataSource(lst);

                    reportDocument.SetParameterValue("pProjectName", result.Data.TituloProyecto);

                    ProgramService programService = new ProgramService();
                    var programResult = programService.GetProgramById(_userSession.ProgramId);

                    if (programResult.MessageType == MessageType.INFO)
                    {
                        reportDocument.SetParameterValue("programName", programResult.Data.Descripcion);
                        reportDocument.SetParameterValue("programAcronym", programResult.Data.NombrePrograma);
                    }

                    reportDocument.SetParameterValue("userName", _userSession.Username);

                    reportDocument.ExportToHttpResponse(new ExportOptions
                    {
                        ExportFormatType = ExportFormatType.PortableDocFormat
                    }, System.Web.HttpContext.Current.Response, false, ("Tareas_Retrasadas_" + result.Data.IdProyecto));

                }
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, HttpContext.User.Identity.Name,
                           MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }
        }

        #endregion

        #region Presupuesto

        [AjaxOnly]
        [AllowAnonymous]
        public JsonResult GetBudgetDistribution(int projectID)
        {
            List<dynamic> data = new List<dynamic>();

            try
            {
                ProjectService service = new ProjectService();
                var projectResult = service.GetProjectById(projectID);

                if (projectResult.MessageType != MessageType.INFO)
                    return new JsonResult { Data = new { message = "Ocurrió un error al intentar obtener la información del proyecto", status = "Error" } };

                decimal budget = projectResult.Data.Presupuesto;

                DeliverableService deliverableService = new DeliverableService();

                // Se obtiene la información de la ejecución del proyecto
                var result = deliverableService.GetDeliverablesByProjectID(projectID);

                if (result.MessageType == MessageType.INFO)
                {
                    var maxDel = result.Data.OrderByDescending(p => p.MstPresupuesto.FirstOrDefault().PresupuestoAsignado).FirstOrDefault();

                    foreach (var item in result.Data)
                    {
                        if (item.IdEntregable == maxDel.IdEntregable)
                        {
                            dynamic info = new
                            {
                                name = item.Descripcion,
                                y = (item.MstPresupuesto.FirstOrDefault().PresupuestoAsignado * 100) / budget,
                                sliced = true,
                                selected = true
                            };

                            data.Add(info);
                        }
                        else
                        {
                            dynamic info = new
                            {
                                name = item.Descripcion,
                                y = (item.MstPresupuesto.FirstOrDefault().PresupuestoAsignado * 100) / budget
                            };

                            data.Add(info);

                        }
                    }
                }
                else
                    return new JsonResult { Data = new { message = "Ocurrió un error al intentar obtener la información del proyecto", status = "Error" } };
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult { Data = data, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        #endregion

        #endregion


        [AjaxOnly]
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Timeline(int projectId)
        {
            List<TimeLine> model = new List<TimeLine>();

            TaskService service = new TaskService();
            var result = service.GetTimeLineProject(projectId);
            if (result.MessageType == MessageType.INFO)
            {
                model = result.Data;
            }
            ViewBag.ProjectID = projectId;

            return PartialView("Partials/Home/_timeline", model);
        }

        [AjaxOnly]
        [AllowAnonymous]
        public JsonResult BackupDatabase()
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                SecurityService service = new SecurityService();

                var result = service.BackupDatabase();

                if (result.MessageType != MessageType.SUCCESS)
                {
                    status = result.MessageType.ToString();
                    Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }

                message = result.Message;
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }


        [AjaxOnly]
        [AllowAnonymous]
        public JsonResult RestoreDatabase()
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                foreach (string file in Request.Files)
                {
                    //Checking file is available to save.  
                    if (file != null)
                    {
                        var fileContent = Request.Files[file];
                        if (fileContent != null && fileContent.ContentLength > 0)
                        {
                            // string fileName = System.IO.Path.GetFileName(file);
                            SecurityService service = new SecurityService();

                            var result = service.RestoreDatabase(fileContent.FileName);

                            if (result.MessageType != MessageType.SUCCESS)
                            {
                                status = result.MessageType.ToString();
                                Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                                        MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                            }

                            message = result.Message;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

    }
}
