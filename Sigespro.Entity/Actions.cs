//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sigespro.Entity
{
    using System;
    using System.Collections.Generic;
     
    public partial class Actions
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Actions()
        {
            this.MenuItems = new HashSet<MenuItems>();
            this.RoleAction = new HashSet<RoleAction>();
        }
    
        public int ActionId { get; set; }
        public int SystemId { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public int UserInsert { get; set; }
        public System.DateTime InsertDate { get; set; }
        public Nullable<int> UserUpdate { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MenuItems> MenuItems { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RoleAction> RoleAction { get; set; }
        public virtual Systems Systems { get; set; }
    }
}
