﻿using Common.Extensions;
using Common.Helpers;
using Sigespro.DAL.Implementations;
using Sigespro.Entity;
using Sigespro.Entity.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigespro.Service.Admin
{
    public class LoggerService: Service
    {
        private Repository<vwLogger> Repository { get; set; }

        public LoggerService() : base()
        {
            Repository = _unitOfWork.GetRepository<vwLogger>();
        }

        public void GetLogsForList(ref QueryOptions<IEnumerable<vwLogger>> queryOpt)
        {
            Result<IEnumerable<vwLogger>> result = new Result<IEnumerable<vwLogger>>();

            try
            {
                string orderBy = "Id";

                var predicate = PredicateBuilder.True<vwLogger>();

                var filter = queryOpt.Filters.FirstOrDefault(f => f.Name.Equals("Search"));

                if (filter != null && !string.IsNullOrEmpty(filter.Value))
                {
                    int intFilter;

                    predicate = predicate.And(p => p.Exception.ToLower().Trim().Contains(filter.Value.ToLower().Trim()) ||
                                                   p.Message.ToLower().Trim().Contains(filter.Value.ToLower().Trim()));

                    if (int.TryParse(filter.Value, out intFilter))
                    {
                        predicate = predicate.Or(p => p.Id == intFilter);
                    }

                    queryOpt.TotalRecords = _unitOfWork.GetRepository<vwLogger>().Count(predicate);
                }
                else
                    queryOpt.TotalRecords = _unitOfWork.GetRepository<vwLogger>().Count();

                if (!string.IsNullOrEmpty(queryOpt.SortField))
                    orderBy = queryOpt.SortField;

                result.Data = _unitOfWork.GetRepository<vwLogger>().GetManyOnDemand(predicate, orderBy, queryOpt.CurrentPageIndex, queryOpt.RowsPerPage, queryOpt.SortOrder);

                result.Message = Messages.SuccessGetResultSet;

                result.MessageType = MessageType.INFO;
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorGetResultSet, "registro de rol");
                result.MessageType = MessageType.ERROR;
                result.DetailException = ex;
            }

            queryOpt.ResultData = result;
        }

    }
}
