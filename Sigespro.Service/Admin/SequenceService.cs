﻿using Common.Log;
using Sigespro.DAL.Implementations;
using Sigespro.Entity;
using System;

namespace Sigespro.Service.Admin
{
    public class SequenceService
    {
        Repository<MstSecuencias> _repository;
        
        public SequenceService(Repository<MstSecuencias> repository)
        {
            _repository = repository;
        }

        public MstSecuencias GetNextSequenceByTableName(string tableName)
        {
            MstSecuencias seq = _repository.Get(s => s.NombreTabla == tableName);

            if (seq != null)
                seq.SecuencialTabla = seq.SecuencialTabla + 1;
            else
            {
                seq = new MstSecuencias();
                seq.NombreTabla = tableName;
                seq.SecuencialTabla = 1;
            }

            return seq;
        }

        public bool InsertSequence(MstSecuencias sequence, string IpAddress = "")
        {
            try
            {
                if (sequence.NombreTabla == string.Empty)
                    return false;

                if (sequence.SecuencialTabla == 1)
                    _repository.Add(sequence);
                else
                    _repository.Update(sequence);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }
    }
}
