﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace Sigespro.Web.Mvc.Utilities.Security
{
    public interface IUserSession
    {
        string Email { get; }
        string Username { get; }
        string FullName { get; }
        int UserId { get; }
        int ProgramId { get; }
        int PersonalId { get; }
        bool ChangePassword { get; }
    }

    public class UserSession : IUserSession
    {

        public string Username
        {
            get { return ((ClaimsPrincipal)HttpContext.Current.User).FindFirst(ClaimTypes.Name).Value; }
        }

        //public string BearerToken
        //{
        //    get { return ((ClaimsPrincipal)HttpContext.Current.User).FindFirst("AcessToken").Value; }
        //}

        public string Email
        {
            get { return ((ClaimsPrincipal)HttpContext.Current.User).FindFirst(ClaimTypes.Email).Value; }
        }

        public string FullName
        {
            get { return ((ClaimsPrincipal)HttpContext.Current.User).FindFirst("FullName").Value; }
        }

        public int UserId
        {
            get { return int.Parse(((ClaimsPrincipal)HttpContext.Current.User).FindFirst("UserId").Value); }
        }

        public int ProgramId
        {
            get
            {
                return int.Parse(((ClaimsPrincipal)HttpContext.Current.User).FindFirst("ProgramId").Value);
            }
        }

        public int PersonalId
        {
            get
            {
                return int.Parse(((ClaimsPrincipal)HttpContext.Current.User).FindFirst("PersonalId").Value);
            }
        }

        public bool ChangePassword
        {
            get
            {
                return bool.Parse(((ClaimsPrincipal)HttpContext.Current.User).FindFirst("ChangePassword").Value);
            }
        }
    }
}