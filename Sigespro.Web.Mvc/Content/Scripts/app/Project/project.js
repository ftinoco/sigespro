﻿(function (app) {

    var module = {};

    var strLinkTab = '<a href="{0}" data-toggle="tab" aria-expanded="false" data-next="{1}" data-next-title="{2}" \
                         data-action="{3}" data-before="{4}"> \
                            {5} <div class="ripple-container"></div>\
                      </a>';

    module.init = function () {
        app.formater();

        $('#FechaInicio').datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true,
            todayBtn: "linked",
            startDate: '01/01/1900',
            endDate: '31/12/3000'
        }).on('changeDate', function (e) {
            $('#FechaLimite').val("");
            $('#FechaLimite').unbind().removeData();
            module.initDeadline(e.date);
        });

        module.initDeadline($('#FechaInicio').val());

        // Se setean los eventos del próximo tab a cada tab
        $.each($("a[data-toggle*='tab']"), function (index, a) {
            $(a).on('show.bs.tab', function (e) {
                if (e.target.parentNode.id !== "header-tab-generalInformation") {
                    e.preventDefault();
                    module.next(e);
                }
            });
        });
    };

    module.initDeadline = function (startDate) {
        $('#FechaLimite').datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true,
            clearBtn: true,
            startDate: startDate,
            endDate: '31/12/3000'
        });
    }

    module.next = function (e) {
        e.preventDefault();

        var action = $(e.target).data("action");
        var nextOption = $(e.target).data("next");
        var beforeOption = $(e.target).data("before");
        var nextTitle = $(e.target).data("next-title");

        if ($("#frmProject").ajaxValidate()) {

            var strHeaderTab = strLinkTab.replace("{0}", "#" + nextOption).replace("{1}", nextOption).replace("{2}", nextTitle);

            $("#header-tab-" + nextOption).html(strHeaderTab.replace("{3}", action).replace("{4}", beforeOption).replace("{5}", nextTitle));

            var frmData = new FormData(document.getElementById("frmProject"));

            $.ajax({
                type: "POST",
                url: action,
                data: frmData,
                contentType: false,
                processData: false,
                cache: false,
                beforeSend: function () {
                    app.toggleLoadingModal();
                },
                success: function (response) {

                    $("#" + nextOption).html(response);

                    if ($("#header-tab-" + nextOption).hasClass("disabled"))
                        $("#header-tab-" + nextOption).removeClass("disabled")

                    $("#header-tab-" + nextOption + " a").tab('show');

                    $("#header-tab-" + nextOption + " a").on('show.bs.tab', function (e) {
                        if (e.target.parentNode.id !== "header-tab-generalInformation") {
                            e.preventDefault();
                            module.next(e);
                        }
                    });

                    $("#frmProject").resetValidation();
                },
                error: function (data) {
                    console.log(data.responseText);
                    $('#modalMessage').modal('show');
                    $('#modalMessage .modal-body').html(app.MESSAGE_ERROR);
                },
                statusCode: {
                    401: function () {
                        window.location.replace(app.URL_BASE + "Account/LogOut");
                    }
                }
            }).always(function (jqXHR, textStatus, error) {
                app.toggleLoadingModal();
            });
        }
        else {
            $("#header-tab-" + beforeOption + " a").tab('show');

            if (!$("#header-tab-" + nextOption).hasClass("disabled"))
                $("#header-tab-" + nextOption).addClass("disabled")

            $("#header-tab-" + nextOption).html("<label>" + nextTitle + "</label>");

            //$("#" + nextOption).html(""); 
        }
    }

    module.addGoal = function (e) {
        e.preventDefault();

        var valid = false;
        var frm = e.target.form;

        var form = $(frm).removeData("validator").removeData("unobtrusiveValidation");

        $.validator.unobtrusive.parse(form);
        var validator = form.validate();

        validator.settings.ignore = '.valid';

        $('input, select').not('#goals textarea, #goals select').addClass('valid');

        $.each($("textarea[id*='MstObjetivos_Descripcion']"), function (index, input) {
            if (index > 0)
                valid = validator.element(input) && valid;
            else
                valid = validator.element(input);
        });

        $.each($("input[id*='MstObjetivos_TipoObjetivo']"), function (index, select) {
            valid = validator.element(select) && valid;
        });

        if (valid) {
            this.sendRequestGoal(e);
        }
    };

    module.sendRequestGoal = function (e) {
        var action = $(e.target).data("action");

        var frmData = new FormData(e.target.form);

        //for (var key of frmData.keys()) {
        //    console.log(key + " => " + frmData.get(key));
        //}

        $.ajax({
            type: "POST",
            url: action,
            data: frmData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                app.toggleLoadingModal();
            },
            success: function (response) {

                $(e.target.form).resetValidation();
                $('input, select').removeClass('valid');
                $("#goals").html(response);

            },
            error: function (data) {
                console.log(data.responseText);
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body').html(app.MESSAGE_ERROR);
            },
            statusCode: {
                401: function () {
                    window.location.replace(app.URL_BASE + "Account/LogOut");
                }
            }
        }).always(function () {
            app.toggleLoadingModal();
        });
    };

    module.deleteGoal = function (e) {
        e.preventDefault();

        this.sendRequestGoal(e);
    };

    module.checkAllPrograms = function (e) {
        var data = [];
        var cheched = $(e.target).is(":checked");

        $("input[name*=chk_Programs_]").each(function (index, check) {
            data[index] = ($(check).data("item"));
            $(check).prop("checked", cheched);
        });

        var action = $(e.target).data("action");

        if (!cheched)
            action = action + "?checked=false";

        $.ajax({
            url: action,
            type: "POST",
            //dataType: 'json',
            data: JSON.stringify({ "programs": data }),
            contentType: 'application/json; charset=utf-8',
            beforeSend: function () {
                app.toggleLoadingModal();
            },
            success: function (response) {
                //console.log(response); 
                $("#programs").html(response);

            },
            error: function (data) {
                console.log(data.responseText);
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body').html(app.MESSAGE_ERROR);
            },
            statusCode: {
                401: function () {
                    window.location.replace(app.URL_BASE + "Account/LogOut");
                }
            }
        }).always(function () {
            app.toggleLoadingModal();
        });
        //console.log(data);
    };

    module.checkProgram = function (e) {
        var data = [];
        data[0] = $(e.target).data("item");

        var action = $(e.target).data("action");

        if (!$(e.target).is(":checked"))
            action = action + "?checked=false";

        $.ajax({
            url: action,
            type: "POST",
            //dataType: 'json',
            data: JSON.stringify({ "programs": data }),
            contentType: 'application/json; charset=utf-8',
            beforeSend: function () {
                app.toggleLoadingModal();
            },
            success: function (response) {
                //console.log(response);
                $("#programs").html(response);

            },
            error: function (data) {
                console.log(data.responseText);
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body').html(app.MESSAGE_ERROR);
            },
            statusCode: {
                401: function () {
                    window.location.replace(app.URL_BASE + "Account/LogOut");
                }
            }
        }).always(function () {
            app.toggleLoadingModal();
        });

        $("input[name*=chk_Programs_]").each(function (index, check) {
            if (!$(check).is(":checked"))
                $("#chk_All_Programs_").prop("checked", false);
        });
    }

    module.checkAllPerson = function (e) {
        var data = [];
        var cheched = $(e.target).is(":checked");

        $("input[name*=chk_Personal_]").each(function (index, check) {
            data[index] = ($(check).data("item"));
            $(check).prop("checked", cheched);
        });

        var action = $(e.target).data("action");

        if (!cheched)
            action = action + "?checked=false";

        $.ajax({
            url: action,
            type: "POST",
            //dataType: 'json',
            data: JSON.stringify({ "model": data }),
            contentType: 'application/json; charset=utf-8',
            beforeSend: function () {
                app.toggleLoadingModal();
            },
            success: function (response) {
                $("#managers").html(response);
            },
            error: function (data) {
                console.log(data.responseText);
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body').html(app.MESSAGE_ERROR);
            },
            statusCode: {
                401: function () {
                    window.location.replace(app.URL_BASE + "Account/LogOut");
                }
            }
        }).always(function () {
            app.toggleLoadingModal();
        });
        //console.log(data);
    };

    module.checkPerson = function (e) {
        var data = [];
        data[0] = $(e.target).data("item");

        var action = $(e.target).data("action");

        if (!$(e.target).is(":checked"))
            action = action + "?checked=false";

        $.ajax({
            url: action,
            type: "POST",
            //dataType: 'json',
            data: JSON.stringify({ "model": data }),
            contentType: 'application/json; charset=utf-8',
            beforeSend: function () {
                app.toggleLoadingModal();
            },
            success: function (response) {
                $("#managers").html(response);
            },
            error: function (data) {
                console.log(data.responseText);
                $('#modalMessage').modal('show');
                $('#modalMessage .modal-body').html(app.MESSAGE_ERROR);
            },
            statusCode: {
                401: function () {
                    window.location.replace(app.URL_BASE + "Account/LogOut");
                }
            }
        }).always(function () {
            app.toggleLoadingModal();
        });

        $("input[name*=chk_Programs_]").each(function (index, check) {
            if (!$(check).is(":checked"))
                $("#chk_All_Programs_").prop("checked", false);
        });
    };

    module.submitForm = function (e) {

        $(e.target).prop("disabled", true);

        if ($("#frmProject").ajaxValidate()) {

            var frmData = new FormData(document.getElementById('frmProject'));

            $.ajax({
                url: $("#frmProject").attr("action"),
                type: $("#frmProject").attr("method"),
                data: frmData,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    app.toggleLoadingModal();
                },
                success: function (response) {
                    if (response.status === app.STATUS_CODE.SUCCESS) {
                        app.showModalMessage(response.status, response.message, app.MODAL_SIZE.MD, app.redirectTo, [response.action]);
                        $("#btnModalMessageClose").focus();
                    }
                    else {
                        app.showModalMessage(response.status, response.message);
                        $(e.target).prop("disabled", false);
                    }
                },
                error: function (data) {
                    console.log(data.responseText);
                    $('#modalMessage').modal('show');
                    $(e.target).prop("disabled", false);
                    $('#modalMessage .modal-body').html(app.MESSAGE_ERROR);
                },
                statusCode: {
                    401: function () {
                        window.location.replace(app.URL_BASE + "Account/LogOut");
                    }
                }
            }).always(function () {
                app.toggleLoadingModal();
            });
        }

    }

    module.confirmApprovePlanning = function (e) {

        //$(e.target).prop("disabled", true);

        app.showModalDialog("Mensaje de Confirmación", "¿Desea aprobar la planificación del proyecto? Si lo hace ya no será posible modificarla",
                            module.approvePlanning, [e], null, null, app.MODAL_SIZE.MD);

    };

    module.approvePlanning = function (e) {

        $.ajax({
            type: "POST",
            url: $(e.target).data("action"),
            beforeSend: function () {
                app.toggleLoadingModal();
            },
            success: function (response) {
                if (response.status !== undefined) {
                    app.showModalMessage(response.status, response.message, app.MODAL_SIZE.MD, module.refreshBeforeModal, [response.status]);
                }
            },
            error: function (data) {
                console.log(data.responseText);
                app.showModalMessage(app.STATUS_CODE.ERROR, app.MESSAGE_ERROR, app.MODAL_SIZE.MD);
            },
            statusCode: {
                401: function () {
                    window.location.replace(app.URL_BASE + "Account/LogOut");
                }
            }
        }).always(function (jqXHR, textStatus, error) {
            app.toggleLoadingModal();
        });
    };

    module.refreshBeforeModal = function (status) {
        if (status === app.STATUS_CODE.SUCCESS) {
            app.reload();
        }
    };

    module.refreshPlanning = function (projectID, urlRefresh) {
        if (projectID !== undefined && projectID !== null) {
            $.ajax({
                type: "GET",
                url: urlRefresh,
                beforeSend: function () {
                    app.toggleLoadingModal();
                },
                //data: { projectId: projectID },
                success: function (response) {
                    if (response.status === undefined) {
                        if ($("#planning-table-content")) {
                            $("#planning-table-content").remove();
                            $("#empty-table").html('');
                            $("#tbl").append(response);
                        } else {
                            $("#empty-table").html(response );
                        }
                    }
                    else {
                        $("#planning-table-content").html('');
                        $("#empty-table").html('<div class="planning-table-cell colspan" style="text-align: center">' + response.message + '</div>');
                    }
                },
                error: function (data) {
                    console.log(data.responseText);
                    app.showModalMessage(app.STATUS_CODE.ERROR, app.MESSAGE_ERROR, app.MODAL_SIZE.MD);
                },
                statusCode: {
                    401: function () {
                        window.location.replace(app.URL_BASE + "Account/LogOut");
                    }
                }
            }).always(function () {
                app.toggleLoadingModal(); 
            });
        }
    };

    app.project = module;

}(sigesproApp || {}));

sigesproApp.project.init();