﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace Common.Mailer
{
    public class EmailManager
    {
        private ConfigDataMailing _configEmail;

        #region Propiedades

        public string EmailFrom { get; set; }

        public string Body{ get; set; }

        public string Subject { get; set; }

        public bool IsBodyHTML { get; set; }

        public int TimeOut { get; set; }

        /// <summary>
        /// Nombre visible al enviar el correo
        /// </summary>
        public string EmailFromDisplayName { get; set; }

        /// <summary>
        /// Nombre a mostrar como remitente (por defecto vacio, si es asi se muestra la cuenta de correo)
        /// </summary>
        public string DisplayName { get; set; }

        public ArrayList EmailTO { get; set; }

        public ArrayList EmailCC { get; set; }

        public ArrayList EmailBCC { get; set; }

        public ArrayList Files { get; set; }

        #endregion

        public EmailManager(ConfigDataMailing ConfigEmail)
        {
            _configEmail = ConfigEmail;

            EmailTO = new ArrayList();
            EmailCC = new ArrayList();
            EmailBCC = new ArrayList();
            Files = new ArrayList();
        }

        public bool SendEmail()
        {
            try
            {
                using (System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage())
                {

                    mail.From = new MailAddress(EmailFrom, EmailFromDisplayName, System.Text.Encoding.UTF8);

                    mail.Subject = Subject;

                    mail.IsBodyHtml = IsBodyHTML;
                    
                    if (IsBodyHTML)
                    {
                        AlternateView htmlView = AlternateView.CreateAlternateViewFromString(Body, System.Text.Encoding.UTF8, MediaTypeNames.Text.Html);
                        
                        //LinkedResource img = new LinkedResource(HttpContext.Current.Server.MapPath(@"~/Imagenes/Logo CAV.png"), MediaTypeNames.Image);
                        //img.ContentId = "logo";
                        //htmlView.LinkedResources.Add(img);
                        
                        mail.AlternateViews.Add(htmlView);
                    }
                    else
                    {
                        AlternateView plainView = AlternateView.CreateAlternateViewFromString(Body, System.Text.Encoding.UTF8, MediaTypeNames.Text.Plain);
                        mail.AlternateViews.Add(plainView);
                        mail.Body = Body;
                    }

                    foreach (string targetEmail in EmailTO)
                    {
                        mail.To.Add(targetEmail);
                    }

                    foreach (string targetEmail in EmailCC)
                    {
                        mail.CC.Add(targetEmail);
                    }

                    foreach (string targetEmail in EmailBCC)
                    {
                        mail.Bcc.Add(targetEmail);
                    }
                    
                    mail.Priority = MailPriority.High;


                    foreach (EmailAttachment file in Files)
                    {
                        if (file.PathFile != null)
                        {
                            mail.Attachments.Add(new Attachment(file.PathFile));
                        }
                        else if (file.File != null)
                        {
                            file.File.Seek(0, SeekOrigin.Begin);
                            mail.Attachments.Add(new Attachment(file.File, file.Name, file.Type));
                        }
                    }

                    ////SE ESTABLECE EL CORREO DEL USUARIO ACTUAL DEL SISTEMA COMO CUENTA QUE RECIBIRA LA RESPUESTA DEL CORREO
                    //if (Utilidad.UsuarioEmail != string.Empty) { mail.ReplyToList.Add(Utilidad.UsuarioEmail); }

                    ////SE ENVIA COPIA DEL CORREO A LA CUENTA SERVIDOR
                    //mail.CC.Add(from);

                    using (System.Net.Mail.SmtpClient SMTP = new System.Net.Mail.SmtpClient(_configEmail.SMTP))
                    {
                        if (_configEmail.EnableSSL)
                        {
                            NetworkCredential credenciales = new NetworkCredential(_configEmail.EmailAddress, _configEmail.Password);
                            SMTP.Credentials = credenciales;
                        }
                        else
                        {
                            SMTP.Credentials = CredentialCache.DefaultNetworkCredentials;
                            SMTP.DeliveryMethod = SmtpDeliveryMethod.Network;
                        }
                        
                        SMTP.Timeout = TimeOut;
                        SMTP.Port = _configEmail.Port;
                        SMTP.EnableSsl = _configEmail.EnableSSL;

                        SMTP.Send(mail);


                    } // SMTP
                }// MailSetup

                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
