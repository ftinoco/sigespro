﻿using Common.Helpers;
using Common.Log;
using Sigespro.DAL.Implementations;
using Sigespro.Entity;
using Sigespro.Entity.Resource;
using Sigespro.Service.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigespro.Service.Pro
{
    public class GoalService : Service
    {
        public bool CommitExternal { get; set; }

        private Repository<MstObjetivos> Repository { get; set; }

        public GoalService() : this(new UnitOfWork())
        {
            Repository = _unitOfWork.GetRepository<MstObjetivos>();
        }

        public GoalService(UnitOfWork UnitOfWork) : base()
        {
            _unitOfWork = UnitOfWork;
            Repository = _unitOfWork.GetRepository<MstObjetivos>();
        }

        public Result<IEnumerable<MstObjetivos>> GetGoals()
        {
            Result<IEnumerable<MstObjetivos>> result = new Result<IEnumerable<MstObjetivos>>();

            try
            {
                result.Data = Repository.GetAll();

                result.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<MstObjetivos> GetGoalById(int idGoal)
        {
            Result<MstObjetivos> result = new Result<MstObjetivos>();

            try
            {
                result.MessageType = MessageType.INFO;
                result.Data = Repository.GetById(idGoal);
                result.Message = Messages.SuccessGetResult;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<List<MstObjetivos>> GetGoalsByProjectId(int projectId)
        {
            Result<List<MstObjetivos>> result = new Result<List<MstObjetivos>>();

            try
            {
                var repository = _unitOfWork.GetRepository<DetProyectoObjetivos>();

                result.MessageType = MessageType.INFO;
                result.Data = Repository.GetAll().Join(repository.GetAll(),
                                    g => g.IdObjetivo,
                                    d => d.IdObjetivo,
                                    (g, d) => new { g, d})
                                .Where(p => p.d.IdProyecto == projectId)
                                .Select(p => p.g).ToList();

                result.Message = Messages.SuccessGetResult;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<IEnumerable<MstObjetivos>> GetGoals(string parameter)
        {
            Result<IEnumerable<MstObjetivos>> result = new Result<IEnumerable<MstObjetivos>>();

            try
            {
                result.Data = Repository.GetAll();

                result.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase InsertGoal(MstObjetivos goal, UnitOfWork unitOfWork = null)
        {
            ResultBase result = new ResultBase();

            try
            {
                var unitOfWorkTEmp = unitOfWork == null ? _unitOfWork : unitOfWork;

                var resultValidate = Utilities.ValidateEntity(goal);

                if (resultValidate.MessageType == MessageType.WARNING)
                    result = resultValidate;
                else
                {
                    var temp = Repository.Get(g => g.Descripcion == goal.Descripcion);

                    if (temp == null)
                    {/*
                        var sequencesService = new SequenceService(unitOfWorkTEmp.GetRepository<MstSecuencias>());

                        MstSecuencias seq = sequencesService.GetNextSequenceByTableName("[pro].[MstObjetivos]");

                        goal.IdObjetivo = seq.SecuencialTabla;
                        */
                        Repository.Add(goal);

                        //sequencesService.InsertSequence(seq);

                        if (!CommitExternal)
                            unitOfWorkTEmp.Commit();
                    }
                }

                result.Message = Messages.SuccessSaveData;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorSaveData, "objetivo");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase UpdateGoal(MstObjetivos goal, UnitOfWork unitOfWork = null)
        {
            ResultBase result = new ResultBase();

            try
            {
                var unitOfWorkTemp = unitOfWork == null ? _unitOfWork : unitOfWork;

                Repository = unitOfWorkTemp.GetRepository<MstObjetivos>();

                var goalTemp = Repository.Get(g => g.IdObjetivo == goal.IdObjetivo);
                
                if (goalTemp != null)
                { 
                    goalTemp.Descripcion = goal.Descripcion;
                    goalTemp.TipoObjetivo = goal.TipoObjetivo;

                    Repository.Update(goalTemp);

                    if (!CommitExternal)
                        unitOfWorkTemp.Commit();
                }

                result.Message = Messages.SuccessUpdateData;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorUpdateData, "objetivo");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        /// <summary>
        /// Método para insertar la vinculación de proyecto con objetivos
        /// </summary>
        /// <param name="goals">Listado de objetivos</param>
        /// <returns></returns>
        public  ResultBase InsertUpdateGoals(ICollection<DetProyectoObjetivos> goals, int projectID, UnitOfWork unitOfWork)
        {
            ResultBase result = new ResultBase();

            try
            {
                int count = 0;
                Repository<DetProyectoObjetivos> detRepository = unitOfWork.GetRepository<DetProyectoObjetivos>();

                int generalGoals = goals.Count(g => g.MstObjetivos.TipoObjetivo.ToUpper().Equals("G"));

                // Se valida la cantidad de objetivos generales
                if (generalGoals > 1)
                {
                    result.Message = "Solo se puede asignar un objetivo general al proyecto.";
                    result.DetailException = new Exception(result.Message);
                    result.MessageType = MessageType.ERROR;
                    return result;
                }

                // Para cada objetivo
                foreach (DetProyectoObjetivos detGoal in goals)
                {
                    if (detGoal.MstObjetivos == null)
                    {
                        result.Message = "Debe proporcionar los datos requeridos para ingresar los objetivos del proyecto.";
                        result.DetailException = new Exception(result.Message);
                        result.MessageType = MessageType.ERROR;
                        return result;
                    }
                    else
                    {
                        CommitExternal = true;

                        // Si el objetivo ya existe se actualiza
                        if (detGoal.MstObjetivos.IdObjetivo > 0)
                        {
                            result = UpdateGoal(detGoal.MstObjetivos, unitOfWork);

                            if (result.MessageType != MessageType.SUCCESS)
                                throw result.DetailException;
                        }
                        else // De lo contrario se ingresa
                        {
                            result = InsertGoal(detGoal.MstObjetivos, unitOfWork);

                            if (result.MessageType != MessageType.SUCCESS)
                                throw result.DetailException;

                            var goalFromList = goals.Where(g => g.MstObjetivos.Descripcion == detGoal.MstObjetivos.Descripcion);

                            if (goalFromList.Count() == 1)
                            {
                                detGoal.IdObjetivo = detGoal.MstObjetivos.IdObjetivo;
                                detGoal.IdProyecto = projectID;
                                /*
                                var sequencesService = new SequenceService(unitOfWork.GetRepository<MstSecuencias>());
                                MstSecuencias seq = sequencesService.GetNextSequenceByTableName("[pro].[DetProyectoObjetivos]");

                                // Se le suma el contador debido a que estando en la misma transaccion la tabla de secuencia no actualiza el registro
                                detGoal.IdProyectoObjetivo = seq.SecuencialTabla + count;
                                
                                // Solo se va a actualizar la tabla secuencial hasta que se hayan procesado todos los registros
                                if (goals.Last().MstObjetivos.Descripcion == detGoal.MstObjetivos.Descripcion) // (count == goals.Count - 1)
                                {
                                    seq.SecuencialTabla = seq.SecuencialTabla + count;
                                    sequencesService.InsertSequence(seq);
                                }
                                */
                                detRepository.Add(detGoal);
                                count++;
                            }
                            else
                            {
                                result.Message = "No se pueden guardar dos objetivos con la misma descripción.";
                                result.DetailException = new Exception(result.Message);
                                result.MessageType = MessageType.ERROR;
                                return result;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorSaveData, "proyecto");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase DeleteGoals(MstProyectos project, UnitOfWork unitOfWork)
        {
            ResultBase result = new ResultBase();

            try
            {
                var repository = unitOfWork.GetRepository<DetProyectoObjetivos>();
                var repositoryGoal = unitOfWork.GetRepository<MstObjetivos>();

                List<DetProyectoObjetivos> projectGoals = new List<DetProyectoObjetivos>();
                project.DetProyectoObjetivos.ToList().ForEach(p => projectGoals.Add(new DetProyectoObjetivos
                {
                    IdObjetivo = p.IdObjetivo,
                    IdProyecto = p.IdProyecto,
                    IdProyectoObjetivo = p.IdProyectoObjetivo
                }));

                var projectGoalsInDB = repository.GetMany(p => p.IdProyecto == project.IdProyecto);

                foreach (var item in projectGoalsInDB)
                {
                    // Se elimina el registro DetProyectoObjetivos
                    if (!projectGoals.Any(p => p.IdObjetivo == item.IdObjetivo && p.IdProyectoObjetivo == item.IdProyectoObjetivo))
                    {
                        repository.Delete(item);

                        // Se elimina el registro de la tabla MstObjetivos
                        var goal = repositoryGoal.GetById(item.IdObjetivo);

                        if (goal != null)
                            repositoryGoal.Delete(goal);
                    } 
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

    }
}
