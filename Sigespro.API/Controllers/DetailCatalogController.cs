﻿using Common.Extensions;
using Common.Log;
using Common.Helpers;
using Sigespro.Entity;
using Sigespro.Entity.Resource;
using Sigespro.Service;
using Sigespro.Service.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace Sigespro.API.Controllers
{
    [RoutePrefix("Api/DetailCatalog")]
    public class DetailCatalogController : ApiController
    {
        DetailCatalogService _service;

        public DetailCatalogController(Func<string, IService> serviceFactory)
        {
            _service = (DetailCatalogService)serviceFactory("DetailCatalog");
        }

        #region Nueva Implementación

        [HttpPost]
        [Route("List")]
        public HttpResponseMessage ListDetailCatalog(QueryOptions<IEnumerable<DetCatalogo>> filter)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            _service.GetDetailCatalogsForList(ref filter);

            if (filter.ResultData.MessageType == MessageType.INFO)
                response = Request.CreateResponse(HttpStatusCode.OK, filter, "application/json");
            else
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, filter.ResultData.Message);
                Logger.Log(filter.ResultData.MessageType, filter.ResultData.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());
            }

            return response;
        }

        #endregion

        [HttpGet]
        [Route("")]
        public IEnumerable<DetCatalogo> Get()
        {
            return GetDetails();
        }

        [HttpGet]
        [Route("GetByValue/{value}")]
        public DetCatalogo GetDetailByValue(string value)
        {
            return GetDetail(value);
        }

        [HttpGet]
        [Route("{DetailId}/{CatalogId}")]
        public DetCatalogo GetDetailCatalog(int DetailId, int CatalogId)
        {
            return GetDetail(DetailId.ToString(), CatalogId.ToString());
        }

        [HttpGet]
        [Route("Filter/{Filter}")]
        public IEnumerable<DetCatalogo> GetDetailsCatalog(string Filter)
        {
            return GetDetails(Filter);
        }

        [HttpGet]
        [Route("GetByTable/{TableName}")]
        public IEnumerable<DetCatalogo> GetDetailByTable(string TableName)
        {
            return GetDetails(TableName, true);
        }

        private DetCatalogo GetDetail(string parameter1, string parameter2 = "")
        {
            Result<DetCatalogo> result = new Result<DetCatalogo>();

            if (string.IsNullOrEmpty(parameter2))
                result = _service.GetDetailByValue(parameter1);
            else
                result = _service.GetDetailCatalogById(Convert.ToInt32(parameter1), Convert.ToInt32(parameter2));

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data;
        }

        private IEnumerable<DetCatalogo> GetDetails(string filter = "", bool byTable = false)
        {
            Result<IEnumerable<DetCatalogo>> result = new Result<IEnumerable<DetCatalogo>>();

            if (string.IsNullOrEmpty(filter))
                result = _service.GetDetailsCatalog();
            else
            {
                if (byTable)
                    result = _service.GetDetailsByTable(filter);
                else
                    result = _service.GetDetailsCatalog(filter);
            }

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo XD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data.ToList();
        }

        [HttpPost]
        [Route("")]
        public HttpResponseMessage Post(DetCatalogo detail)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            if (ModelState.IsValid)
            {
                ResultBase result = new ResultBase();

                try
                {

                    result = _service.InsertDetailCatalog(detail);

                    if (result.MessageType.Equals(MessageType.SUCCESS))
                    {
                        response = Request.CreateResponse(HttpStatusCode.Created, detail);

                        //string uri = Url.Link("TwoKeyApi", new
                        //{
                        //    parameter1 = detail.IdDetCatalogo,
                        //    parameter2 = detail.IdMstCatalogo
                        //});

                        //response.Headers.Location = new Uri(uri);
                    }
                    else
                    {
                        response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, result.Message);
                        throw result.DetailException;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log(result.MessageType, ex, "Yo xD", GetType().Name, Request.GetClientIpAddress());
                }
            }
            else
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.RequiredData);

            return response;
        }

        [HttpPut]
        [Route("{DetailId}/{CatalogId}")]
        public HttpResponseMessage Put(int DetailId, int CatalogId, DetCatalogo detail)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            if (!ModelState.IsValid)
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.RequiredData);
            else
            {
                ResultBase result = new ResultBase();

                try
                {
                    if (DetailId != detail.IdDetCatalogo && CatalogId != detail.IdMstCatalogo)
                        response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "La información no coincide con el identificador proporcionado.");
                    else
                    {
                        result = _service.UpdateDetailCatalog(detail);

                        if (result.MessageType == MessageType.SUCCESS)
                            response = Request.CreateResponse(HttpStatusCode.OK);
                        else
                        {
                            if (result.MessageType == MessageType.WARNING)
                                return Request.CreateErrorResponse(HttpStatusCode.NotFound, result.Message);

                            else if (result.MessageType == MessageType.ERROR)
                                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result.Message);

                            throw result.DetailException;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log(result.MessageType, ex, "Yo xD", GetType().Name, Request.GetClientIpAddress());
                }
            }

            return response;
        }
    }
}
