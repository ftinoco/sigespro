using Microsoft.Practices.Unity;
using Sigespro.Service;
using Sigespro.Service.Admin;
using Sigespro.Service.Config;
using Sigespro.Service.Pre;
using Sigespro.Service.Pro;
using Sigespro.Service.Security;
using System;
using System.Web.Http;
using Unity.WebApi;

namespace Sigespro.API
{
    public static class UnityConfig
    {
        public static void RegisterComponents(HttpConfiguration config)
        {
			var container = BuildUnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

            //GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);

            config.DependencyResolver = new UnityDependencyResolver(container);
        }

        public static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            container.RegisterType<ICatalogService, CatalogService>();
           // container.RegisterType<ITokenService, TokenService>();

            container.RegisterType<IService, DetailCatalogService>("DetailCatalog", new HierarchicalLifetimeManager())
                        .RegisterType<IService, HolidayService>("Holiday", new HierarchicalLifetimeManager())
                        .RegisterType<IService, PersonService>("Person", new HierarchicalLifetimeManager())
                        .RegisterType<IService, ProgramService>("Program", new HierarchicalLifetimeManager())
                        .RegisterType<IService, ArticleService>("Article", new HierarchicalLifetimeManager())
                        .RegisterType<IService, CurrencyService>("Currency", new HierarchicalLifetimeManager())
                        .RegisterType<IService, EntryService>("Entry", new HierarchicalLifetimeManager())
                        .RegisterType<IService, BeneficiaryService>("Beneficiary", new HierarchicalLifetimeManager())
                        .RegisterType<IService, GoalService>("Goal", new HierarchicalLifetimeManager())
                        .RegisterType<IService, PersonalService>("Personal", new HierarchicalLifetimeManager())
                        .RegisterType<IService, ProjectService>("Project", new HierarchicalLifetimeManager())
                        //.RegisterType<IService, UserService>("User", new HierarchicalLifetimeManager())
                        .RegisterType<IService, ParameterService>("Parameter", new HierarchicalLifetimeManager())
                        .RegisterType<IService, SecurityService>("Security", new HierarchicalLifetimeManager());

            container.RegisterType<Func<string, IService>>(
                        new InjectionFactory(c =>
                        new Func<string, IService>(name => c.Resolve<IService>(name))));

            return container;
        }
    }
}