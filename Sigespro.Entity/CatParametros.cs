//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sigespro.Entity
{
    using System;
    using System.Collections.Generic;
     
    public partial class CatParametros
    {
        public int IdPerametro { get; set; }
        public string NombreParametro { get; set; }
        public string ValorParametro { get; set; }
        public string DescParametro { get; set; }
    }
}
