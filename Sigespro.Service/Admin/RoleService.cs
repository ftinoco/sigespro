﻿using Common.Extensions;
using Common.Helpers;
using Sigespro.DAL.Implementations;
using Sigespro.Entity;
using Sigespro.Entity.Custom;
using Sigespro.Entity.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using static Common.Helpers.Utilities;

namespace Sigespro.Service.Admin
{
    public class RoleService : Service
    {
        private Repository<Roles> Repository { get; set; }

        public RoleService() : base()
        {
            Repository = _unitOfWork.GetRepository<Roles>();
        }

        public Result<IEnumerable<Roles>> GetRoles()
        {
            Result<IEnumerable<Roles>> result = new Result<IEnumerable<Roles>>();

            try
            {
                result.Data = Repository.GetAll().OrderBy(d => d.RoleName);

                result.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<Roles> GetRolesById(int RoleId)
        {
            Result<Roles> result = new Result<Roles>();

            try
            {
                result.Data = Repository.GetById(RoleId);
                result.Message = Messages.SuccessGetResult;
                result.MessageType = MessageType.INFO;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public void GetRolesForList(ref QueryOptions<IEnumerable<Roles>> queryOpt)
        {
            Result<IEnumerable<Roles>> result = new Result<IEnumerable<Roles>>();

            try
            {
                string orderBy = "RoleName";

                var predicate = PredicateBuilder.True<Roles>();

                var filter = queryOpt.Filters.FirstOrDefault(f => f.Name.Equals("Search"));

                if (!IsNull(filter) && !string.IsNullOrEmpty(filter.Value))
                {
                    int intFilter;

                    predicate = predicate.And(p => p.RoleName.ToLower().Trim().Contains(filter.Value.ToLower().Trim()) ||
                                                   p.Description.ToLower().Trim().Contains(filter.Value.ToLower().Trim()));

                    if (int.TryParse(filter.Value, out intFilter))
                    {
                        predicate = predicate.Or(p => p.RoleId == intFilter);
                    }

                    queryOpt.TotalRecords = _unitOfWork.GetRepository<Roles>().Count(predicate);
                }
                else
                    queryOpt.TotalRecords = _unitOfWork.GetRepository<Roles>().Count();

                if (!string.IsNullOrEmpty(queryOpt.SortField))
                    orderBy = queryOpt.SortField;

                result.Data = _unitOfWork.GetRepository<Roles>().GetManyOnDemand(predicate, orderBy, queryOpt.CurrentPageIndex, queryOpt.RowsPerPage, queryOpt.SortOrder);

                result.Message = Messages.SuccessGetResultSet;

                result.MessageType = MessageType.INFO;
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorGetResultSet, "registro de rol");
                result.MessageType = MessageType.ERROR;
                result.DetailException = ex;
            }

            queryOpt.ResultData = result;
        }

        public Result<IEnumerable<Roles>> GetRoles(string filter)
        {
            string filterToLower = filter.Trim().ToLower();

            Result<IEnumerable<Roles>> result = new Result<IEnumerable<Roles>>();

            try
            {
                result.Data = Repository.GetMany(p => p.RoleName.Trim().ToLower().Contains(filterToLower) ||
                                                      p.Description.Trim().ToLower().Contains(filterToLower))
                                        .OrderBy(d => d.RoleName);

                result.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase InsertRole(Roles rol)
        {
            ResultBase result = new ResultBase();

            try
            {
                if (rol.RoleId == 0)
                {
                    var sequencesService = new SequenceService(_unitOfWork.GetRepository<MstSecuencias>());

                    if (string.IsNullOrEmpty(rol.RoleName) || string.IsNullOrEmpty(rol.Description))
                    {
                        result.Message = Messages.RequiredData;
                        result.MessageType = MessageType.WARNING;
                        result.DetailException = new Exception(result.Message);
                    }
                    else
                    {
                        rol.InsertDate = DateTime.Now;

                        MstSecuencias seq = sequencesService.GetNextSequenceByTableName("[security].[Roles]");

                        rol.RoleId = seq.SecuencialTabla;
                        Repository.Add(rol);

                        sequencesService.InsertSequence(seq);

                        _unitOfWork.Commit();

                    }
                }

                result.Message = Messages.SuccessSaveData;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorSaveData, "rol");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase UpdateRole(Roles rol)
        {
            ResultBase result = new ResultBase();

            try
            {
                var temp = Repository.Get(p => p.RoleId == rol.RoleId);

                if (IsNull(temp))
                {
                    result.Message = Messages.NotExist;
                    result.MessageType = MessageType.WARNING;
                    result.DetailException = new Exception(result.Message);
                }
                else
                {
                    if (string.IsNullOrEmpty(rol.RoleName) || string.IsNullOrEmpty(rol.Description))
                    {
                        result.Message = Messages.RequiredData;
                        result.MessageType = MessageType.INFO;
                        result.DetailException = new Exception(result.Message);
                    }
                    else
                    {
                        rol.UpdateDate = DateTime.Now;
                        rol.InsertDate = temp.InsertDate;
                        rol.UserInsert = temp.UserInsert;
                        Repository.Update(rol);

                        _unitOfWork.Commit();
                    }
                }

                result.Message = Messages.SuccessUpdateData;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorUpdateData, "rol");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase InsertRoleAction(List<RoleAssignment> roleActions, int systemID)
        {
            ResultBase result = new ResultBase();

            try
            {
                var newRepository = _unitOfWork.GetRepository<RoleAction>();

                foreach (var item in roleActions)
                {
                    if (!item.IsChecked)
                    {
                        if (newRepository.Any(r => r.RoleId == item.RoleID && r.ActionId == item.AssignmentID && r.SystemId == systemID))
                            newRepository.Delete(r => r.RoleId == item.RoleID && r.ActionId == item.AssignmentID && r.SystemId == systemID);
                    }
                    else
                    {
                        if (!newRepository.Any(r => r.RoleId == item.RoleID && r.ActionId == item.AssignmentID && r.SystemId == systemID))
                            newRepository.Add(new RoleAction
                            {
                                ActionId = item.AssignmentID,
                                RoleId = item.RoleID,
                                SystemId = systemID
                            });
                    }
                }

                _unitOfWork.Commit();

                result.MessageType = MessageType.SUCCESS;
                result.Message = Messages.SuccessSaveData;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorSaveData, "vinculación del rol con las acciones del sistema");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase InsertRoleMenuItem(List<RoleAssignment> roleActions, int systemID)
        {
            ResultBase result = new ResultBase();

            try
            {
                var newRepository = _unitOfWork.GetRepository<RoleMenuItem>();

                foreach (var item in roleActions)
                {
                    if (!item.IsChecked)
                    {
                        if (newRepository.Any(r => r.RoleId == item.RoleID && r.MenuItemId == item.AssignmentID && r.SystemId == systemID))
                            newRepository.Delete(r => r.RoleId == item.RoleID && r.MenuItemId == item.AssignmentID && r.SystemId == systemID);
                    }
                    else
                    {
                        if (!newRepository.Any(r => r.RoleId == item.RoleID && r.MenuItemId == item.AssignmentID && r.SystemId == systemID))
                            newRepository.Add(new RoleMenuItem
                            {
                                MenuItemId = item.AssignmentID,
                                RoleId = item.RoleID,
                                SystemId = systemID,
                                MenuId = 1 // Por defecto, ya que solo un menú deberia estar registrado en la base de datos
                            });
                    }
                }

                _unitOfWork.Commit();

                result.MessageType = MessageType.SUCCESS;
                result.Message = Messages.SuccessSaveData;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorSaveData, "de la vinculación del rol con las opciones de menú");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }
        
    }
}
