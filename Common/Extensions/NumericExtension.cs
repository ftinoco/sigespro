﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Extensions
{
    public static class NumericExtension
    {
        public static string FormatCode(this int number, int length)
        {
            string code = "";

            int size = length - number.ToString().Length;

            for (int i = 0; i < size; i++)
                code += "0";

            code += number;

            return code;
        }
    }
}
