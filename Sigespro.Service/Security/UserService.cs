﻿using Common.Extensions;
using Common.Helpers;
using Common.Mailer;
using Sigespro.DAL.Implementations;
using Sigespro.Entity;
using Sigespro.Entity.Custom;
using Sigespro.Entity.Resource;
using Sigespro.Service.Admin;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Sigespro.Service.Security
{
    public class UserService : Service
    {
        private Repository<Users> Repository { get; set; }

        public UserService() : base()
        {
            Repository = _unitOfWork.GetRepository<Users>();
        }
        
        public Result<Users> GetUserByID(int userID)
        {
            Result<Users> result = new Result<Users>();

            try
            {
                result.MessageType = MessageType.INFO;
                result.Data = Repository.GetById(userID);
                result.Message = Messages.SuccessGetResult;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorGetResultSet, "usuario");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<Users> GetUserByName(string userName)
        {
            Result<Users> result = new Result<Users>();

            try
            {
                var temp = Repository.Get(u => u.UserName == userName);

                if (temp != null)
                {
                    result.Data = temp;
                    result.MessageType = MessageType.INFO;
                    result.Message = Messages.SuccessGetResult;
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorGetResultSet, "usuario");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<List<UserRoles>> GetRolesByUserID(int userID)
        {
            Result<List<UserRoles>> result = new Result<List<UserRoles>>();

            try
            {
                var userRoles = _unitOfWork.GetRepository<UserRoles>().GetMany(r => r.UserId == userID);

                result.Data = userRoles.ToList();

            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorGetResultSet, "roles de usuario");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public void GetUsersForList(ref QueryOptions<IEnumerable<Users>> queryOpt)
        {
            Result<IEnumerable<Users>> result = new Result<IEnumerable<Users>>();

            try
            {
                string orderBy = "UserName";

                var predicate = PredicateBuilder.True<Users>();

                var filter = queryOpt.Filters.FirstOrDefault(f => f.Name.Equals("Search"));

                if (filter != null && !string.IsNullOrEmpty(filter.Value))
                {
                    int intFilter;

                    predicate = predicate.And(p => p.FullName.ToLower().Trim().Contains(filter.Value.ToLower().Trim()) ||
                                                   p.UserName.ToLower().Trim().Contains(filter.Value.ToLower().Trim()));

                    if (int.TryParse(filter.Value, out intFilter))
                    {
                        predicate = predicate.Or(p => p.UserId == intFilter);
                    }

                    queryOpt.TotalRecords = _unitOfWork.GetRepository<Users>().Count(predicate);
                }
                else
                    queryOpt.TotalRecords = _unitOfWork.GetRepository<Users>().Count();

                if (!string.IsNullOrEmpty(queryOpt.SortField))
                    orderBy = queryOpt.SortField;

                result.Data = _unitOfWork.GetRepository<Users>().GetManyOnDemand(predicate, orderBy, queryOpt.CurrentPageIndex, queryOpt.RowsPerPage, queryOpt.SortOrder);

                result.Message = Messages.SuccessGetResultSet;

                result.MessageType = MessageType.INFO;
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorGetResultSet, "usuarios");
                result.MessageType = MessageType.ERROR;
                result.DetailException = ex;
            }

            queryOpt.ResultData = result;
        }

        public Result<UserInfo> Authenticate(string username, string password, string ip)
        {
            Result<UserInfo> result = new Result<UserInfo>();

            try
            {
                Users user;
                UserInfo userInfo;

                string hashPwsd = Utilities.SHA512(password);

                if (!username.IsEmail())
                    user = Repository.Get(u => u.UserName == username && u.Password == hashPwsd);
                else
                    user = Repository.Get(u => u.Email == username && u.Password == hashPwsd);

                if (Utilities.IsNull(user))
                {
                    if (!username.IsEmail())
                        result.Message = "Usuario o contraseña invalidos";
                    else
                        result.Message = "Correo o contraseña invalidos";

                    result.MessageType = MessageType.INFO;
                }
                else
                {
                    LoginHistory loginHistory = new LoginHistory
                    {
                        UserId = user.UserId,
                        IP = ip,
                        LoginDate = DateTime.Now,
                    };

                    var sequencesService = new SequenceService(_unitOfWork.GetRepository<MstSecuencias>());

                    MstSecuencias seq = sequencesService.GetNextSequenceByTableName("[Security].[LoginHistory]");

                    loginHistory.LoginHistoryId = seq.SecuencialTabla;
                    var repositoryHistory = _unitOfWork.GetRepository<LoginHistory>();
                    repositoryHistory.Add(loginHistory);

                    sequencesService.InsertSequence(seq);

                    _unitOfWork.Commit();

                    var detPersonal = _unitOfWork.GetRepository<DetPersonal>().GetById(user.ReferenceId.Value);

                    userInfo = new UserInfo
                    {
                        Email = user.Email,
                        FullName = user.FullName,
                        PersonalId = detPersonal.IdPersonal,
                        ProgramId = detPersonal.IdPrograma.Value,
                        UserId = user.UserId,
                        UserName = user.UserName,
                        ExpirationSession = user.ExpirationSession,
                        ChangePassword = user.ChangePassword
                    };

                    result.Data = userInfo;
                    result.Message = Messages.SuccessGetResult;
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorGetResultSet, "autenticación");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase ChangePassword(int userID, string currentPassword, string newPassword, string newPasswordRepeat)
        {
            ResultBase result = new ResultBase();

            try
            {
                if (!newPassword.Equals(newPasswordRepeat))
                {
                    result.MessageType = MessageType.ERROR;
                    result.Message = "La nueva contraseña y la confirmación no coinciden";
                    result.DetailException = new Exception(result.Message);
                    return result;
                }

                var userInfo = Repository.GetById(userID);

                if (userInfo == null)
                {
                    result.MessageType = MessageType.ERROR;
                    result.Message = "No existe el usuario con el identificador proporcionado";
                    result.DetailException = new Exception(result.Message);
                    return result;
                }
                else
                {
                    if (userInfo.Password == Utilities.SHA512(currentPassword))
                    {
                        string hashPassword = Utilities.SHA512(newPassword);

                        var newRepository = _unitOfWork.GetRepository<PasswordHistory>();

                        var pswHistory = new PasswordHistory
                        {
                            ChangeDate = DateTime.Now,
                            NewPassword = hashPassword,
                            OldPassword = userInfo.Password,
                            UserId = userInfo.UserId,
                            ReasonChange = "Restauración de contraseña"
                        };

                        var sequencesService = new SequenceService(_unitOfWork.GetRepository<MstSecuencias>());
                        MstSecuencias seq = sequencesService.GetNextSequenceByTableName("[Security].[PasswordHistory]");

                        pswHistory.PasswordHistoryId = seq.SecuencialTabla;

                        sequencesService.InsertSequence(seq);

                        newRepository.Add(pswHistory);

                        userInfo.Password = hashPassword;

                        if (userInfo.ChangePassword)
                            userInfo.ChangePassword = false;
                        
                        _unitOfWork.Commit();

                        result.Message = "La contraseña ha sido actualizada exitosamente";
                    }
                    else
                    {
                        result.MessageType = MessageType.ERROR;
                        result.Message = "La contraseña actual es incorrecta";
                        result.DetailException = new Exception(result.Message);
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = "Ocurrió un error al intentar actualizar la contraseña de usuario";
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase InsertUser(Users user, int rolID)
        {
            ResultBase result = new ResultBase();

            try
            {
                if (string.IsNullOrWhiteSpace(user.UserName))
                {
                    result.MessageType = MessageType.ERROR;
                    result.Message = "Se debe proporcionar el nombre de usuario";
                    result.DetailException = new Exception("Nombre de usuario no proporcionado");
                    return result;
                }

                if (user.ReferenceId <= 0)
                {
                    result.MessageType = MessageType.ERROR;
                    result.Message = "Se debe selecionar un personal del centro al cual se le vinculará el usuario";
                    result.DetailException = new Exception("No se se seleccionó ninguna persona");
                    return result;
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(user.FullName))
                    {
                        result.MessageType = MessageType.ERROR;
                        result.Message = "Se debe proporcionar el nombre de la persona";
                        result.DetailException = new Exception("Se seleccionó la persona pero no se estableció el nombre completo");
                        return result;
                    }

                    if (string.IsNullOrWhiteSpace(user.Email))
                    {
                        result.MessageType = MessageType.ERROR;
                        result.Message = "Se debe proporcionar el correo electrónico";
                        result.DetailException = new Exception("Se seleccionó la persona pero no se estableció el correo electrónico");
                        return result;
                    }

                    if (string.IsNullOrWhiteSpace(user.PhoneNumber))
                    {
                        result.MessageType = MessageType.ERROR;
                        result.Message = "Se debe proporcionar el número telefónico";
                        result.DetailException = new Exception("Se seleccionó la persona pero no se estableció el número telefónico");
                        return result;
                    }

                }

                if (user.ExpirationSession <= 0)
                {
                    result.MessageType = MessageType.ERROR;
                    result.Message = "La expiración de sesión debe ser mayor a 0";
                    result.DetailException = new Exception(result.Message);
                    return result;
                }

                if (rolID <= 0)
                {
                    result.MessageType = MessageType.ERROR;
                    result.Message = "Se debe seleccionar un rol para el usuario";
                    result.DetailException = new Exception(result.Message);
                    return result;
                }

                // Se valida que el nombre de usuario no exista
                var userByLogin = Repository.Get(u => u.UserName == user.UserName);

                if (userByLogin != null)
                {
                    result.MessageType = MessageType.ERROR;
                    result.Message = "Ya existe un usuario con el nombre digitado";
                    result.DetailException = new Exception(result.Message);
                    return result;
                }

                Users newUser = new Users
                {
                    Email = user.Email,
                    ChangePassword = true,
                    FullName = user.FullName,
                    InsertDate = DateTime.Now,
                    PhoneNumber = user.PhoneNumber,
                    ReferenceId = user.ReferenceId,
                    UserName = user.UserName,
                    UserInsert = user.UserInsert,
                    Active = true,
                    ExpirationSession = user.ExpirationSession
                };

                var sequencesService = new SequenceService(_unitOfWork.GetRepository<MstSecuencias>());

                MstSecuencias seq = sequencesService.GetNextSequenceByTableName("[Security].[Users]");

                // Se obtiene la contraseña temporal del usuario y que sera enviada por correo
                string tempPassword = Utilities.CreateTemporaryPassword(12);
                newUser.Password = Utilities.SHA512(tempPassword);

                newUser.UserId = seq.SecuencialTabla;

                sequencesService.InsertSequence(seq);

                Repository.Add(newUser);

                // Se agrega la relación usuario - rol
                var repositoryUserRole = _unitOfWork.GetRepository<UserRoles>();

                repositoryUserRole.Add(new UserRoles
                {
                    RoleId = rolID,
                    StartActionId = 1, // No se usa este campo
                    SystemId = 1,
                    UserId = newUser.UserId
                }); 

                // Se intenta enviar el correo con la contraseña y el nombre de usuario a la persona
                ConfigDataMailing configEmail = new ConfigDataMailing
                {
                    EmailAddress = Labels.EmailAddress,
                    EnableSSL = true,
                    Port = 587,
                    Password = Labels.EmailPassword,
                    SMTP = Labels.SMTPServer
                };


                string body = @"<div style='height: 150px; background-color: #fff;'>
                                    <br />
	                                <p style='font-size: 18x; text-indent: 15px; text-align: justify;'>Acceder al sistema con la siguiente cuenta de usuario:</p>  <strong>{0}</strong>
                                    <br />
	                                <p  style='font-size: 18x; text-indent: 15px; text-align: justify;'>Usando la siguiente contraseña:</p>  <strong>{1}</strong>
                                    <br />
                                    <p style='font-size: 18x; text-indent: 15px; text-align: justify; width: 98%;'>
	                                    Esta contraseña es temporal por lo que deberá cambiarla al momento de iniciar sesión
	                                    con su cuenta de usuario.
                                    </p>
                                </div>";

                body = string.Format(body, newUser.UserName, tempPassword);

                bool sentEmail = Utilities.SendEmail(user.Email, "Sistema de Planificación CAV", "Acceso al sistema", body, configEmail);

                if (sentEmail)
                {
                    _unitOfWork.Commit();
                    result.Message = Messages.SuccessSaveData;
                }
                else
                {
                    result.MessageType = MessageType.ERROR;
                    result.Message = "Ocurrió un error al intentar enviar el correo electrónico. Por favor, intente mas tarde";
                    result.DetailException = new Exception(result.Message);
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorSaveData, "usuario");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase ResetPassword(int userID)
        {
            ResultBase result = new ResultBase();

            try
            {
                Users user = Repository.GetById(userID);

                if (user != null)
                {
                    string tempPassword = Utilities.CreateTemporaryPassword(12);

                    var newRepository = _unitOfWork.GetRepository<PasswordHistory>();

                    var pswHistory = new PasswordHistory
                    {
                        ChangeDate = DateTime.Now,
                        NewPassword = Utilities.SHA512(tempPassword),
                        OldPassword = user.Password,
                        UserId = user.UserId,
                        ReasonChange = "Restauración de contraseña"
                    };

                    user.Password = Utilities.SHA512(tempPassword);
                    user.ChangePassword = true;

                    var sequencesService = new SequenceService(_unitOfWork.GetRepository<MstSecuencias>());
                    MstSecuencias seq = sequencesService.GetNextSequenceByTableName("[Security].[PasswordHistory]");

                    pswHistory.PasswordHistoryId = seq.SecuencialTabla;

                    sequencesService.InsertSequence(seq);

                    newRepository.Add(pswHistory);

                    Repository.Update(user);

                    // Se intenta enviar el correo con la contraseña y el nombre de usuario a la persona
                    ConfigDataMailing configEmail = new ConfigDataMailing
                    {
                        EmailAddress = Labels.EmailAddress,
                        EnableSSL = true,
                        Port = 587,
                        Password = Labels.EmailPassword,
                        SMTP = Labels.SMTPServer
                    };
                    
                    string body = @"<div style='height: 150px; background-color: #fff;'>
                                    <br />
	                                <p style='font-size: 18x; text-indent: 15px; text-align: justify;'>Acceder al sistema con la siguiente cuenta de usuario:</p>  <strong>{0}</strong>
                                    <br />
	                                <p  style='font-size: 18x; text-indent: 15px; text-align: justify;'>Usando la siguiente contraseña:</p>  <strong>{1}</strong>
                                    <br />
                                    <p style='font-size: 18x; text-indent: 15px; text-align: justify; width: 98%;'>
	                                    Esta contraseña es temporal por lo que deberá cambiarla al momento de iniciar sesión
	                                    con su cuenta de usuario.
                                    </p>
                                </div>";

                    body = string.Format(body, user.UserName, tempPassword);

                    bool sentEmail = Utilities.SendEmail(user.Email, "Sistema de Planificación CAV", "Acceso al sistema", body, configEmail);

                    if (sentEmail)
                    {
                        _unitOfWork.Commit();
                        result.Message = "La contraseña de usuario ha sido restaurada exitosamente. Las credenciales fueron enviadas al correo";
                    }
                    else
                    {
                        result.MessageType = MessageType.ERROR;
                        result.Message = "Ocurrió un error al intentar enviar el correo electrónico. Por favor, intente mas tarde";
                        result.DetailException = new Exception(result.Message);
                        return result;
                    } 
                }
                else
                {
                    result.MessageType = MessageType.ERROR;
                    result.Message = "El usuario no existe o el identificador es incorrecto";
                    result.DetailException = new Exception("No se encontró usuario con id especificado");
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorSaveData, "usuario");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase UpdateUser(Users user, int rolID)
        {
            ResultBase result = new ResultBase();

            try
            {
                var userDb = Repository.GetById(user.UserId);

                if (userDb != null)
                {
                    userDb.UserUpdate = user.UserUpdate;
                    userDb.UpdateDate = DateTime.Now;
                    userDb.Active = user.Active;
                    userDb.ExpirationSession = user.ExpirationSession;


                    Repository.Update(userDb);

                    // Se editar la relación usuario - rol
                    var repositoryUserRole = _unitOfWork.GetRepository<UserRoles>();

                    var userRole = repositoryUserRole.Get(r => r.UserId == userDb.UserId);

                    if (userRole != null)
                    {
                        if (userRole.RoleId != rolID)
                        {
                            userRole.RoleId = rolID;

                            repositoryUserRole.Update(userRole);
                        }
                    }
                    else
                    {
                        result.MessageType = MessageType.ERROR;
                        result.Message = "Ocurrió un error al validar la información del usuario";
                        result.DetailException = new Exception("No se encontró registro de usuario-rol");
                        return result;
                    }

                    _unitOfWork.Commit();
                }
                else
                {
                    result.MessageType = MessageType.ERROR;
                    result.Message = "El usuario no existe o el identificador es incorrecto";
                    result.DetailException = new Exception("No se encontró usuario con id especificado");
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorSaveData, "usuario");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        //public ResultBase ValidateDirectorUser(int userID)
        //{
        //    ResultBase result = new ResultBase();

        //    try
        //    {
        //        Users user = Repository.GetById(userID);

        //        var userRoles = _unitOfWork.GetRepository<UserRoles>().GetMany(r => r.UserId == userID);

        //        ParameterService paramService = new ParameterService();
        //        var directorRole = paramService.GetParameterByName()


        //        if ()

        //        result.Data = userRoles.ToList();


        //    }
        //    catch (Exception ex)
        //    {
        //        result.DetailException = ex;
        //        result.Message = Messages.ErrorException;
        //        result.MessageType = MessageType.ERROR;
        //    }

        //    return result;
        //}

    }
}
