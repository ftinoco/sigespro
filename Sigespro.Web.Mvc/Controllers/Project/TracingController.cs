﻿using Common.Helpers;
using Common.Log;
using Newtonsoft.Json;
using Sigespro.Entity.Custom;
using Sigespro.Entity.Resource;
using Sigespro.Service.Admin;
using Sigespro.Service.Pro;
using Sigespro.Web.Mvc.Models.Project.Tasks;
using Sigespro.Web.Mvc.Utilities.Attributes.Security;
using Sigespro.Web.Mvc.Utilities.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace Sigespro.Web.Mvc.Controllers.Project
{
    [RoutePrefix("Tracing")]
    public class TracingController : Controller
    {
        private readonly IUserSession _userSession;

        public BinnacleService Service { get; set; }

        public TracingController()
        {
            Service = new BinnacleService();
            _userSession = new UserSession();
        }

        [AjaxOnly]
        [HttpPost]
        [AllowAnonymous]
        [Route("{projectID}/{taskID}")]
        public ActionResult Index(int projectID, int taskID)
        {
            List<BinnacleTask> model = new List<BinnacleTask>();

            var result = Service.GetActivity(taskID);

            if (result.MessageType == MessageType.INFO)
            {
                model = result.Data;
                ViewBag.TaskName = model.FirstOrDefault().TaskName;
            }

            ViewBag.ProjectID = projectID;

            return PartialView(model);
        }

        [AjaxOnly]
        [HttpPost]
        [AllowAnonymous]
        public ActionResult TaskAdvance(int projectID, int taskID)
        {
            TaskInfoViewModel model = new TaskInfoViewModel();
            var result = Service.GetTaskInfo(taskID, _userSession.PersonalId);

            if (result.MessageType == MessageType.INFO)
            {
                string serialized = JsonConvert.SerializeObject(result.Data);
                model = JsonConvert.DeserializeObject<TaskInfoViewModel>(serialized);

                if (string.IsNullOrWhiteSpace(result.Data.Comments))
                    model.Comments = null;

                // Se carga el listado de estado de la tarea
                DetailCatalogService detService = new DetailCatalogService();

                List<SelectListItem> selectList = new List<SelectListItem>();
                // selectList.Add(new SelectListItem { Value = "0", Text = Labels.Select });

                var detResult = detService.GetDetailsByTable("EstadoTareas");

                if (detResult.MessageType == MessageType.INFO)
                {
                    detResult.Data.ToList().ForEach(p => selectList.Add(new SelectListItem()
                    {
                        Value = p.IdDetCatalogo.ToString(),
                        Text = p.Valor
                    }));

                    ViewBag.Status = new SelectList(selectList, "Value", "Text");
                }

                // Se valida, si se mostrará el campo de progreso
                ViewBag.ShowProgress = false;
                ParameterService parameterService = new ParameterService();
                var parameterResult = parameterService.GetParameterByName("ESTADO_TAREA_EN_EJECUCION");
                if (parameterResult.MessageType == MessageType.SUCCESS)
                {
                    if (parameterResult.Data.ValorParametro.Equals(model.StatusID.ToString()))
                        ViewBag.ShowProgress = true;
                }
                else
                {
                    //manejar error
                }
            }
            else
            {
                //manejar error
            }

            ViewBag.ProjectID = projectID;
            return PartialView(model);
        }

        [AjaxOnly]
        [HttpPost]
        [AllowAnonymous]
        public ActionResult AttachFiles(int projectID, int taskID)
        {
            TaskInfoViewModel model = new TaskInfoViewModel();
            var result = Service.GetTaskInfo(taskID, _userSession.PersonalId);

            if (result.MessageType == MessageType.INFO)
            {
                string serialized = JsonConvert.SerializeObject(result.Data);
                model = JsonConvert.DeserializeObject<TaskInfoViewModel>(serialized);
            }
            else
            {
                //manejar error
            }

            ViewBag.ProjectID = projectID;
            return PartialView(model);
        }

        [AllowAnonymous]
        public ActionResult ListTaskFiles(int taskID)
        {
            List<Entity.ArchivosTareas> model = new List<Entity.ArchivosTareas>();

            var resultTaskFiles = Service.GetTaskFiles(taskID);

            if (resultTaskFiles.MessageType == MessageType.INFO)
                model = resultTaskFiles.Data;
            else
            {
                //manejar error
            }
            return PartialView("Partials/Tracing/_listTaskFiles", model);
        }

        [AjaxOnly]
        [HttpPost]
        [AllowAnonymous]
        public ActionResult AddBinnacle(TaskInfoViewModel model, bool updateTask)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                ResultBase result = new ResultBase();

                if (updateTask)
                {
                    TaskService taskService = new TaskService();
                    result = taskService.UpdateStatus(model.TaskID, model.StatusID, _userSession.UserId, model.Comments, model.Progress);
                }
                else
                {
                    BinnacleService binnacleService = new BinnacleService();
                    result = binnacleService.InsertBinnacle(new Entity.BitacoraTareas
                    {
                        IdTarea = model.TaskID,
                        IdEntregable = model.DeliverableID,
                        IdProyecto = model.ProjectID,
                        IdUsuario = _userSession.UserId,
                        Creacion = false,
                        Comentarios = model.Comments
                    });
                }

                if (result.MessageType != MessageType.SUCCESS)
                {
                    message = result.Message;
                    status = result.MessageType.ToString();
                    Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                    message = string.Format("Se registró la actividad");
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                             MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message, action = Url.Action("Index", "Home") },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [AjaxOnly]
        [HttpPost]
        [AllowAnonymous]
        public ActionResult UploadFiles(int taskID)
        {
            try
            {
                foreach (string file in Request.Files)
                {
                    //Checking file is available to save.  
                    if (file != null)
                    {
                        var fileContent = Request.Files[file];
                        if (fileContent != null && fileContent.ContentLength > 0)
                        {
                            // and optionally write the file to disk
                            //string fileName = Path.GetFileName(file);
                            string tempFileName = "";

                            // Se guarda el registro de archivo
                            var result = Service.InsertTaskFile(new Entity.ArchivosTareas
                            {
                                IdTarea = taskID,
                                NombreArchivo = fileContent.FileName,
                                NombreEnRuta = Server.MapPath("~/App_Data/Images")
                            }, _userSession.UserId, ref tempFileName);

                            var path = Path.Combine(Server.MapPath("~/App_Data/Images"), tempFileName);
                            if (result.MessageType == MessageType.SUCCESS)
                            {
                                // get a stream
                                var stream = fileContent.InputStream;
                                using (var fileStream = System.IO.File.Create(path))
                                {
                                    stream.CopyTo(fileStream);
                                }
                            }



                            /*
                                var InputFileName = Path.GetFileName(file.FileName);
                                var ServerSavePath = Path.Combine(Server.MapPath("~/UploadedFiles/") + InputFileName);*/
                            //Save file to server folder  
                            // file.SaveAs(ServerSavePath);
                            //assigning file uploaded status to ViewBag for showing message to user.  
                            // ViewBag.UploadStatus = files.Count().ToString() + " files uploaded successfully.";
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Upload failed");
            }

            return ListTaskFiles(taskID);
        }

        [AllowAnonymous]
        [Route("Download/{fileId}")]
        public ActionResult DownloadFile(int fileId)
        {
            try
            {
                var result = Service.GetFileInfo(fileId);

                if (result.MessageType == MessageType.INFO)
                {
                    var taskFile = result.Data;
                    byte[] fileBytes = System.IO.File.ReadAllBytes(taskFile.NombreEnRuta);
                    string fileName = taskFile.NombreArchivo;
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName); 
                }
            }
            catch (Exception)
            {
                throw;
            }

            return View();
        }

        [AllowAnonymous]
        public ActionResult DeleteFile(int fileID)
        {
            int taskID = 0;
            try
            {
                var deleteFileResult = Service.DeleteTaskFile(fileID, _userSession.UserId);

                if(deleteFileResult.MessageType == MessageType.SUCCESS)
                {
                    taskID = deleteFileResult.Data.IdTarea;
                    /*
                    string[] arr = deleteFileResult.Data.NombreEnRuta.Split('\\');
                    string fileName = arr[arr.Length - 1];

                    string fullPath = Request.MapPath("~/App_Data/Images/" + fileName);*/
                    if (System.IO.File.Exists(deleteFileResult.Data.NombreEnRuta))
                        System.IO.File.Delete(deleteFileResult.Data.NombreEnRuta);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return ListTaskFiles(taskID);
        }

    }
}