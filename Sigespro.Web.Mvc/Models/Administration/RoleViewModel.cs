﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sigespro.Web.Mvc.Resources;
using System.ComponentModel.DataAnnotations;
namespace Sigespro.Web.Mvc.Models.Administration
{
    public class RoleViewModel
    {
        public int RoleId { get; set; }
        public Nullable<int> RolePerentId { get; set; }
        [Display(Name = "RoleName", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string RoleName { get; set; }
        [Display(Name = "Description", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string Description { get; set; }
        public bool Active { get; set; }

        public int UserInsert { get; set; }
        public Nullable<int> UserUpdate { get; set; }

    }
}