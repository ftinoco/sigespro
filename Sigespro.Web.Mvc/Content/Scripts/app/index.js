﻿(function (app) {

    var module = {};
      
    module.changeView = function (action, e) {
        e.preventDefault();
        $.ajax({
            url: action,
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            beforeSend: function () {
                app.toggleLoadingModal();
            },
            success: function (response) {
                if (response.status) {
                    app.showModalMessage(response.status, response.message, app.MODAL_SIZE.MD);
                }
                else {
                    //app.showModalMessage(app.STATUS_CODE.INFO, response, app.MODAL_SIZE.LG, null, null, true);
                    $("#tasks-execution").html(response);
                }
            },
            error: function (data) {
                console.log(data.responseText);
                app.showModalMessage(app.STATUS_CODE.ERROR, app.MESSAGE_ERROR, app.MODAL_SIZE.MD);
            },
            statusCode: {
                401: function () {
                    window.location.replace(app.URL_BASE + "Account/LogOut");
                }
            }
        }).always(function () {
            app.toggleLoadingModal();
        });
    }

    module.loadTaskWeekly = function (action) {
        $.ajax({
            url: action,
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                if (response.status) {
                    app.showModalMessage(response.status, response.message, app.MODAL_SIZE.MD);
                }
                else {
                    Highcharts.chart('weekly-tasks', {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: ''
                        },
                        xAxis: {
                            categories: response.daysOfWeek
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Número de tareas ejecutadas'
                            }
                        },
                        tooltip: {
                            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
                            shared: true
                        },
                        credits: {
                            enabled: false
                        },
                        plotOptions: {
                            column: {
                                stacking: 'percent'
                            }
                        },
                        series: response.source
                    });
                }
            },
            error: function (data) {
                console.log(data.responseText);
                app.showModalMessage(app.STATUS_CODE.ERROR, app.MESSAGE_ERROR, app.MODAL_SIZE.MD);
            },
            statusCode: {
                401: function () {
                    window.location.replace(app.URL_BASE + "Account/LogOut");
                }
            }
        });
    }

    module.loadTaskByDeliverables = function (action) {
        $.ajax({
            url: action,
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            success: function (response) { 
                if (response.status) {
                    app.showModalMessage(response.status, response.message, app.MODAL_SIZE.MD);
                }
                else {
                    var source = [];
                    response.forEach(function (x) {
                        source.push([x.text, x.count]);
                    }); 
                    Highcharts.chart('container_d', {
                        chart: {
                            type: 'pie',
                            options3d: {
                                enabled: true,
                                alpha: 45
                            }
                        },
                        title: {
                            text: ''
                        },
                        subtitle: {
                            //text: '3D donut in Highcharts'
                        },
                        plotOptions: {
                            pie: {
                                innerSize: 100,
                                depth: 45
                            }
                        },
                        credits: {
                            enabled: false
                        },
                        series: [{
                            name: 'Cantidad de Tareas',
                            data: source
                        }]
                    });
                }
            },
            error: function (data) {
                console.log(data.responseText);
                app.showModalMessage(app.STATUS_CODE.ERROR, app.MESSAGE_ERROR, app.MODAL_SIZE.MD);
            },
            statusCode: {
                401: function () {
                    window.location.replace(app.URL_BASE + "Account/LogOut");
                }
            }
        });
    }

    module.loadBudgetDistribution = function (action) {
        $.ajax({
            url: action,
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            success: function (response) { 
                if (response.status) {
                    app.showModalMessage(response.status, response.message, app.MODAL_SIZE.MD);
                }
                else { 
                    Highcharts.chart('container', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie',
                            options3d: {
                                enabled: true,
                                alpha: 45
                            }
                        },
                        title: {
                            text: ''//Distribución de Presupuesto'
                        },
                        tooltip: {
                            pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                depth: 35,
                                dataLabels: {
                                    enabled: false
                                },
                                showInLegend: true
                            }
                        },
                        credits: {
                            enabled: false
                        },
                        series: [{
                            name: 'Porcentaje Presupestario',
                            colorByPoint: true,
                            data: response   
                        }]
                    });
                }
            },
            error: function (data) {
                console.log(data.responseText);
                app.showModalMessage(app.STATUS_CODE.ERROR, app.MESSAGE_ERROR, app.MODAL_SIZE.MD); 
            },
            statusCode: {
                401: function () {
                    window.location.replace(app.URL_BASE + "Account/LogOut");
                }
            }
        });
    }

    module.changePassword = function (e) {
        e.preventDefault();

        $.ajax({
            url: $(e.target).data("action"),
            type: "POST",
            data: {
                userID: $("#userID").val(),
                currentPassword: $("#currentPassword").val(),
                newPassword: $("#newPassword").val(),
                newPasswordRepeat: $("#newPasswordRepeat").val()
            },
            beforeSend: function () {
                app.toggleLoadingModal();
            },
            success: function (response) {
                console.log(response);

                if (response.status === app.STATUS_CODE.SUCCESS) {
                    app.showModalMessage(response.status, response.message, app.MODAL_SIZE.MD, app.redirectTo, [response.action]);
                    setTimeout(function () {
                        window.location.replace(app.URL_BASE + "Account/LogOut");
                    }, 1000);
                }
                else {
                    app.showModalMessage(response.status, response.message);
                    $("#btnModalMessageClose").focus();
                } 
            },
            error: function (data) {
                console.log(data.responseText);
                app.showModalMessage(app.STATUS_CODE.ERROR, app.MESSAGE_ERROR, app.MODAL_SIZE.MD);
            },
            statusCode: {
                401: function () {
                    window.location.replace(app.URL_BASE + "Account/LogOut");
                }
            }
        }).always(function () {
            app.toggleLoadingModal();
        });
    }

    module.returnExecution = function (e) {
        e.preventDefault();

        console.log(e.target); 

        $.ajax({
            url: $(e.target).data("url"),
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            beforeSend: function () {
                app.toggleLoadingModal();
            },
            success: function (response) {
                if (response.status) {
                    app.showModalMessage(response.status, response.message, app.MODAL_SIZE.MD);
                }
                else {
                    console.log(response);
                    $("#tasks-execution").html(response);
                }
            },
            error: function (data) {
                console.log(data.responseText);
                app.showModalMessage(app.STATUS_CODE.ERROR, app.MESSAGE_ERROR, app.MODAL_SIZE.MD);
            },
            statusCode: {
                401: function () {
                    window.location.replace(app.URL_BASE + "Account/LogOut");
                }
            }
        }).always(function () {
            app.toggleLoadingModal();
        });

    }

    module.uploadFile = function (files) {
 
        var action = $("#frmUploadFile").attr("action");

        var frmData = new FormData();

        if (files) {
            frmData.append("file", files[0]);

            $.ajax({
                type: "POST",
                url: action,
                data: frmData,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    app.toggleLoadingModal();
                },
                success: function (response) {
                    $("#list-task-files").html(response);
                },
                error: function (data) {
                    console.log(data.responseText);
                    $('#modalMessage').modal('show');
                    $('#modalMessage .modal-body').html(app.MESSAGE_ERROR);
                },
                statusCode: {
                    401: function () {
                        window.location.replace(urlBase + "Account/LogOut");
                    }
                }
            }).always(function () {
                app.toggleLoadingModal();
            });
        } 
    }
     
    module.confirmDeleteFile = function (e) {

        //$(e.target).prop("disabled", true);

        app.showModalDialog("Mensaje de Confirmación", "¿Desea eliminar el archivo? Si lo hace no se podré recuperar",
                            module.deleteFile, [e], null, null, app.MODAL_SIZE.SM);

    };

    module.deleteFile = function (e) {
        $.ajax({
            type: "POST",
            url: $(e.target).data("action"),
            beforeSend: function () {
                app.toggleLoadingModal();
            },
            success: function (response) {
                $("#list-task-files").html(response);
                /*if (response.status !== undefined) {
                    app.showModalMessage(response.status, response.message, app.MODAL_SIZE.MD, module.refreshBeforeModal, [response.status]);
                }*/
            },
            error: function (data) {
                console.log(data.responseText);
                app.showModalMessage(app.STATUS_CODE.ERROR, app.MESSAGE_ERROR, app.MODAL_SIZE.MD);
            },
            statusCode: {
                401: function () {
                    window.location.replace(app.URL_BASE + "Account/LogOut");
                }
            }
        }).always(function (jqXHR, textStatus, error) {
            app.toggleLoadingModal();
        });
    }


    app.index = module;

}(sigesproApp || {}));
