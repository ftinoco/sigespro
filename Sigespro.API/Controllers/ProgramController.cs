﻿using Common.Extensions;
using Common.Log;
using Common.Helpers;
using Sigespro.Entity;
using Sigespro.Entity.Resource;
using Sigespro.Service;
using Sigespro.Service.Config;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace Sigespro.API.Controllers
{
    [RoutePrefix("Api/Program")]
    public class ProgramController : ApiController
    {
        ProgramService _service;

        public ProgramController(Func<string, IService> serviceFactory)
        {
            _service = (ProgramService)serviceFactory("Program");
        }

        [HttpGet]
        [Route("")]
        public IEnumerable<MstPrograma> Get()
        {
            var result = _service.GetPrograms();

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data;
        }

        [HttpGet]
        [Route("{ProgramId}")]
        public MstPrograma GetProgram(int ProgramId)
        {
            var result = _service.GetProgramById(ProgramId);

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data;
        }

        [HttpGet]
        [Route("Filter/{Filter}")]
        public IEnumerable<MstPrograma> GetPrograms(string Filter)
        {
            var result = _service.GetPrograms(Filter);

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data;
        }


        [HttpPost]
        [Route("List")]
        public HttpResponseMessage ListPrograms(QueryOptions<IEnumerable<MstPrograma>> filter)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            _service.GetProgramsForList(ref filter);

            if (filter.ResultData.MessageType == MessageType.INFO)
                response = Request.CreateResponse(HttpStatusCode.OK, filter, "application/json");
            else
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, filter.ResultData.Message);
                Logger.Log(filter.ResultData.MessageType, filter.ResultData.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());
            }

            return response;
        }

        [HttpGet]
        [Route("Actives")]
        public IEnumerable<MstPrograma> GetAll()
        {
            var result = _service.GetAllPrograms();

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data;
        }
        


        [HttpPost]
        [Route("")]
        public HttpResponseMessage Post(MstPrograma program)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            if (ModelState.IsValid)
            {
                ResultBase result = new ResultBase();
                try
                {
                    _service.InsertProgram(program);

                    if (result.MessageType == MessageType.SUCCESS)
                    {
                        response = Request.CreateResponse(HttpStatusCode.Created, program);

                        //string uri = Url.Link("DefaultApi", new { id = program.IdPrograma });

                        //response.Headers.Location = new Uri(uri);
                    }
                    else
                    {
                        response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, result.Message);

                        throw result.DetailException;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log(result.MessageType, ex, "Yo xD", GetType().Name, Request.GetClientIpAddress());
                }
            }
            else
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.RequiredData);

            return response;
        }

        [HttpPut]
        [Route("{ProgramId}")]
        public HttpResponseMessage Put(int ProgramId, [FromBody]MstPrograma program)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            if (!ModelState.IsValid)
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.RequiredData);
            else
            {
                ResultBase result = new ResultBase();

                try
                {
                    if (ProgramId != program.IdPrograma)
                        response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "La información no coincide con el identificador proporcionado.");
                    else
                    {
                        _service.UpdateProgram(program);

                        if (result.MessageType == MessageType.SUCCESS)
                            response = Request.CreateResponse(HttpStatusCode.OK);
                        else
                        {
                            if (result.MessageType == MessageType.WARNING)
                                return Request.CreateErrorResponse(HttpStatusCode.NotFound, result.Message);

                            else if (result.MessageType == MessageType.ERROR)
                                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result.Message);

                            throw result.DetailException;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log(result.MessageType, ex, "Yo xD", GetType().Name, Request.GetClientIpAddress());
                }
            }

            return response;
        }
    }
}
