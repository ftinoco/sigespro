﻿using Common.Helpers;
using Common.Log;
using Newtonsoft.Json;
using Sigespro.Service.Security;
using Sigespro.Web.Mvc.Models;
using Sigespro.Web.Mvc.Utilities.Attributes.Security;
using Sigespro.Web.Mvc.Utilities.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace Sigespro.Web.Mvc.Utilities.Attributes.Security
{
    public class AuthCustomAttribute : AuthorizeAttribute
    {
        public string TypeResult
        {
            get
            {
                if (FilterContext != null)
                {
                    var typeResult = (((ReflectedActionDescriptor)FilterContext.ActionDescriptor).MethodInfo).ReturnType;

                    return typeResult.Name;
                }

                return string.Empty;
            }
        }

        private readonly IUserSession _userSession;

        HttpClientApi httpClientApi = new HttpClientApi(ConfigurationManager.AppSettings["SigesproApi"]);

        public AuthorizationContext FilterContext { get; set; }

        public AuthCustomAttribute()
        {
            _userSession = new UserSession();
        }

        /// <summary>
        /// Valida la autorización
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            FilterContext = filterContext;

            base.OnAuthorization(filterContext);

            var skipAuthorization = filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true) ||
                                    filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true);

            // Se el usuario está autenticado se procede a validar el acceso a la acción
            if (((ClaimsPrincipal)HttpContext.Current.User).Identity.IsAuthenticated)
            {
                // Si la acción permite acceso anonimo no se valida que la autorización
                if (!skipAuthorization)
                {
                    // Se valida que el usuario no tenga en true la bandera para cambio de contraseña
                    if (((ClaimsPrincipal)HttpContext.Current.User).FindFirst("ChangePassword") != null)
                    {
                        string action = string.Empty;
                        string controller = string.Empty;
                        GetCurrentUrl(out action, out controller);

                        if (action != "ChangePassword" && controller != "Account")
                        {
                            if (bool.Parse(((ClaimsPrincipal)HttpContext.Current.User).FindFirst("ChangePassword").Value))
                            {
                                FilterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary {
                                       { "action", "ChangePassword" },
                                       { "controller", "Account" }
                                   });
                                return;
                            }
                        }
                    }

                    IsUserAuthorized();
                }
            }
            else // De lo contrario redirecciona a la página de login
            {
                if (!ValidateAuthExcludedActions())
                {
                    if (FilterContext.HttpContext.Request.IsAjaxRequest())
                    {
                        filterContext.HttpContext.Response.StatusCode = 401;
                        filterContext.HttpContext.Response.End();
                    }

                    base.HandleUnauthorizedRequest(filterContext);
                }
            }
        }

        /// <summary>
        /// Metodos para validar si el usuario tiene permiso a la acción o no.
        /// Si tiene acceso continua la ejecución de lo contrario redirecciona a la pagina de error.
        /// </summary>
        /// <param name="filterContext"></param>
        private void IsUserAuthorized()
        {
            string action = string.Empty;
            string controller = string.Empty;
            GetCurrentUrl(out action, out controller);

            string userName = FilterContext.HttpContext.User.Identity.Name;

            try
            {
                if (!string.IsNullOrEmpty(action) && !string.IsNullOrEmpty(controller) && !string.IsNullOrEmpty(userName))
                {

                    SecurityService service = new SecurityService();

                    var resultSecurity = service.GetOptionListByUser(userName, action, controller);

                    if (resultSecurity.MessageType == MessageType.SUCCESS)
                    {
                        string strSerialize = JsonConvert.SerializeObject(resultSecurity.Data);

                        var options = JsonConvert.DeserializeObject<List<OptionsViewModel>>(strSerialize);

                        if (options != null)
                        {
                            if (FilterContext.HttpContext.Session["UserPermission"] != null)
                                FilterContext.HttpContext.Session.Remove("UserPermission");

                            FilterContext.HttpContext.Session["UserPermission"] = options;
                        }

                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.INFO, ex, userName, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, 
                                FilterContext.HttpContext.Request.UserHostAddress);
            }

            RedirectToAuthorizeFailedView();
        }

        /// <summary>
        /// Obtiene la URL de la pagina actual
        /// </summary>
        /// <param name="filterContext"></param>
        /// <returns></returns>
        private string GetCurrentUrl(out string action, out string controller)
        {
            string url = string.Empty;
            action = string.Empty;
            controller = string.Empty;

            object objController = string.Empty,
                    objAction = string.Empty;

            var route = FilterContext.RequestContext.RouteData.Values;

            route.TryGetValue("controller", out objController);
            route.TryGetValue("action", out objAction);

            if (!string.IsNullOrEmpty(objController.ToString()) && !string.IsNullOrEmpty(objAction.ToString()))
            {
                action = objAction.ToString();
                controller = objController.ToString();
                url = "/" + controller.ToString() + "/" + action.ToString();
            }

            return url;
        }

        /// <summary>
        /// Redirecciona o muestra el mensaje de acceso prohibido
        /// </summary>
        /// <param name="filterContext"></param>
        private void RedirectToAuthorizeFailedView()//AuthorizationContext filterContext)
        {
            // Si la petición es ajax, se retorna un objeto JSON
            if (FilterContext.HttpContext.Request.IsAjaxRequest())
            {
                FilterContext.Result = new JsonResult
                {
                    Data = new
                    {
                        status = "Error",
                        message = Resources.Messages.WithoutAuthorizationAjax,
                        action = TypeResult == "PartialViewResult" ? null : System.Web.Security.FormsAuthentication.LoginUrl
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                // Si es una vista parcial se agrega como vista parcial el result
                if (TypeResult == "PartialViewResult")
                {
                    FilterContext.Result = new PartialViewResult()
                    {
                        ViewName = "Partials/Forbidden",
                        ViewData = new ViewDataDictionary()
                            {
                                new KeyValuePair<string, object>("Message", Resources.Messages.WithoutAuthorizationPartial)
                            }
                    };
                }
                else // De lo contrario redirecciona a una pantalla con el mensaje de acceso prohibido
                {
                    FilterContext.Result = new ViewResult()
                    {
                        ViewName = "Partials/Forbidden",
                        ViewData = new ViewDataDictionary()
                            {
                                new KeyValuePair<string, object>("Message", Resources.Messages.WithoutAuthorization)
                            }
                    };
                }
            }
        }

        /// <summary>
        /// Valida las acciones que se encuentren registradas en el web config en la sección de appSettings con el nombre AuthExcludedActions.
        /// Las acciones se deben de separar por |
        /// </summary>
        /// <returns>True si se excluye de la Autorizacion de lo contrario false</returns>
        private bool ValidateAuthExcludedActions()
        {
            bool result = false;
            string action = string.Empty;
            string controller = string.Empty;
            GetCurrentUrl(out action, out controller);
            string currentUrl = GetCurrentUrl(out action, out controller);

            string strAuthExcludedActions = ConfigurationManager.AppSettings["AuthExcludedActions"].ToString();

            string[] arrAuthExcludedActions = strAuthExcludedActions.Split('|');

            foreach (string excludedAction in arrAuthExcludedActions)
            {
                if (excludedAction.Trim().ToLower() == currentUrl.Trim().ToLower())
                {
                    result = true;
                    break;
                }
            }

            return result;
        }
    }
}