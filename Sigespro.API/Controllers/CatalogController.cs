﻿using Common.Extensions;
using Common.Log;
using Common.Helpers;
using Newtonsoft.Json;
using Sigespro.Entity;
using Sigespro.Entity.Resource;
using Sigespro.Service;
using Sigespro.Service.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Net.Http.Formatting;

namespace Sigespro.API.Controllers
{
    //[ApiAuthenticationFilter]
    [RoutePrefix("Api/Catalog")]
    public class CatalogController : ApiController
    {
        private readonly ICatalogService _service;

        public CatalogController(ICatalogService service)
        {
            _service = service;
        }

        #region Nueva Implementación

        [HttpPost]
        [Route("List")]
        public HttpResponseMessage ListCatalog(QueryOptions<IEnumerable<MstCatalogo>> filter)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            _service.GetCatalogsForList(ref filter);

            if (filter.ResultData.MessageType == MessageType.INFO)
                response = Request.CreateResponse(HttpStatusCode.OK, filter, "application/json");
            else
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, filter.ResultData.Message);
                Logger.Log(filter.ResultData.MessageType, filter.ResultData.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());
            }

            return response;
        }
        
        #endregion

        #region Implementación Inicial

        [HttpGet]
        [Route("")]
        public IEnumerable<MstCatalogo> Get()
        {
            var result = _service.GetCatalogs();

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo XD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data.ToList();
        }

        [HttpGet]
        [Route("{CatalogId}")]
        public MstCatalogo GetCatalogById(int CatalogId)
        {
            var result = _service.GetCatalogById(CatalogId);

            if (result.MessageType != MessageType.INFO)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data;
        }

        [HttpGet]
        [Route("Filter/{filter}")]
        public IEnumerable<MstCatalogo> Get(string filter)
        {
            var result = _service.GetCatalogs(filter);

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo XD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data.ToList();
        }

        [HttpPost]
        [Route("")]
        public HttpResponseMessage SaveCatalog(MstCatalogo catalog)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            if (ModelState.IsValid)
            {
                ResultBase result = new ResultBase();

                try
                {
                    result = _service.InsertCatalog(catalog);

                    if (result.MessageType == MessageType.SUCCESS)
                    {
                        response = Request.CreateResponse(HttpStatusCode.Created, catalog);

                        string uri = Url.Link("DefaultApi", new { id = catalog.IdCatalogo });

                        response.Headers.Location = new Uri(uri);
                    }
                    else
                    {
                        response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, result.Message);

                        throw result.DetailException;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log(result.MessageType, ex, "Yo xD", GetType().Name, Request.GetClientIpAddress());
                }
            }
            else
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.RequiredData);

            return response;
        }

        [HttpPut]
        [Route("{CatalogId}")]
        public HttpResponseMessage UpdateCatalog(int CatalogId, MstCatalogo catalog)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            if (ModelState.IsValid)
            {
                ResultBase result = new ResultBase();

                try
                {
                    if (CatalogId != catalog.IdCatalogo)
                        response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "La información no coincide con el identificador proporcionado.");
                    else
                    {
                        result = _service.UpdateCatalog(catalog);

                        if (result.MessageType == MessageType.SUCCESS)
                            response = Request.CreateResponse(HttpStatusCode.OK);
                        else
                        {
                            if (result.MessageType == MessageType.WARNING)
                                response = Request.CreateErrorResponse(HttpStatusCode.NotFound, result.Message);

                            else if (result.MessageType == MessageType.ERROR)
                                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, result.Message);

                            throw result.DetailException;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log(result.MessageType, ex, "Yo xD", GetType().Name, Request.GetClientIpAddress());
                }
            }
            else
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.RequiredData);

            return response;
        }

        #endregion
    }
}
