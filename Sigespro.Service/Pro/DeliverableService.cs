﻿using Common.Helpers;
using Sigespro.DAL.Implementations;
using Sigespro.Entity;
using Sigespro.Entity.Custom;
using Sigespro.Entity.Resource;
using Sigespro.Service.Admin;
using Sigespro.Service.Pre;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigespro.Service.Pro
{
    public class DeliverableService : Service
    {
        private Repository<MstEntregables> Repository { get; set; }

        public DeliverableService() : base()
        {
            Repository = _unitOfWork.GetRepository<MstEntregables>();
        }

        public Result<MstEntregables> GetDeliverableByID(int deliverableID, int projectID)
        {
            Result<MstEntregables> result = new Result<MstEntregables>();

            try
            {
                result.MessageType = MessageType.INFO;
                result.Data = Repository.GetById(deliverableID, projectID);
                result.Message = Messages.SuccessGetResult;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<IEnumerable<MstEntregables>> GetDeliverablesByProjectID(int projectID)
        {
            Result<IEnumerable<MstEntregables>> result = new Result<IEnumerable<MstEntregables>>();

            try
            {
                var deliverables = Repository.GetMany(e => e.IdProyecto == projectID, false).ToList();

                if (deliverables != null)
                {
                    var taskRep = _unitOfWork.GetRepository<MstTareas>();
                    var budgetRepo = _unitOfWork.GetRepository<MstPresupuesto>();
                    foreach (var item in deliverables)
                    {
                        item.MstTareas = new List<MstTareas>();
                        var tasks = taskRep.GetMany(t => t.IdProyecto == projectID && t.IdEntregable == item.IdEntregable)
                                            .OrderBy(x => x.FechaInicio).ToList();

                        foreach (var task in tasks)
                        {
                            item.MstTareas.Add(task);
                        }

                        var budget = budgetRepo.Get(p => p.IdProyecto == projectID && p.IdEntregable == item.IdEntregable);
                        if (budget != null)
                            item.MstPresupuesto.Add(budget);

                    }
                    result.Data = deliverables;
                    result.MessageType = MessageType.INFO;
                    result.Message = Messages.SuccessGetResultSet;
                }
                else
                {
                    result.Data = null;
                    result.MessageType = MessageType.WARNING;
                    result.Message = Messages.NoFoundResult;
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<List<DeliverableInfo>> GetDeliverablesInExecution(int projectID, int personalID)
        {
            Result<List<DeliverableInfo>> result = new Result<List<DeliverableInfo>>();

            try
            {
                var deliverables = Repository.GetMany(e => e.IdProyecto == projectID, false).ToList();
                if (deliverables != null)
                {

                    DetailCatalogService catalogService = new DetailCatalogService();
                    var catalogResult = catalogService.GetDetailsByTable("EstadoTareas");
                    int finished = catalogResult.Data.FirstOrDefault(x => x.Valor == "Ejecutada").IdDetCatalogo;
                    int canceledId = catalogResult.Data.FirstOrDefault(x => x.Valor == "Cancelada").IdDetCatalogo;

                    result.Data = new List<DeliverableInfo>();
                    List<DeliverableInfo> deliverablesInfo = new List<DeliverableInfo>();
                    foreach (var item in deliverables)
                    {
                        DeliverableInfo deliverable = new DeliverableInfo()
                        {
                            DeliverableID = item.IdEntregable,
                            Description = item.Descripcion,
                            ProjectID = item.IdProyecto
                        };

                        var taskRep = _unitOfWork.GetRepository<vwTareasEnEjecucion>();
                        var tasks = taskRep.GetMany(t => t.IdProyecto == projectID && t.IdEntregable == item.IdEntregable).ToList();

                        deliverable.Tasks = new List<TaskInfo>();
                        foreach (var task in tasks)
                        {
                            if (!deliverable.Tasks.Any(t => t.TaskID == task.IdTarea))
                            {
                                deliverable.Tasks.Add(new TaskInfo
                                {
                                    TaskID = task.IdTarea,
                                    TaskName = task.Nombre,
                                    Author = task.Autor,
                                    LastUpdate = task.UltimaActualizacion,
                                    Manager = tasks.FirstOrDefault(x => x.TipoPersonal == "R" && x.IdTarea == task.IdTarea).NombrePersona,
                                    Status = task.Estado,
                                    AddAdvance = (tasks.Any(x => x.IdPersonal == personalID && x.IdTarea == task.IdTarea)
                                     && tasks.Any(x => (x.IdEstado != finished && x.IdEstado != canceledId) && x.IdTarea == task.IdTarea))
                                });
                            }
                        }
                        result.Data.Add(deliverable);
                    }
                    result.MessageType = MessageType.INFO;
                    result.Message = Messages.SuccessGetResultSet;
                }
                else
                { 
                    result.MessageType = MessageType.WARNING;
                    result.Message = Messages.NoFoundResult;
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase InsertDeliverable(Entity.Custom.Deliverable deliverable)
        {
            ResultBase result = new ResultBase();

            try
            {
                MstProyectos project = new MstProyectos();

                var resultValidate = ValidateDeliverableInfo(deliverable, ref project);

                if (resultValidate.MessageType == MessageType.ERROR)
                    result = resultValidate;
                else
                {
                    var deliverableExist = Repository.Any(p => p.Descripcion == deliverable.Descripcion && p.IdProyecto == deliverable.IdProyecto);

                    if (!deliverableExist)
                    {
                        deliverable.FechaIns = DateTime.Now;

                        // Se consideró tener registros catálogos para este campo pero al final se decidió manejarlo como bit
                        deliverable.IdEstado = 1;

                        // Obteniendo el identificador del nuevo entregable
                        var sequencesService = new SequenceService(_unitOfWork.GetRepository<MstSecuencias>());
                        MstSecuencias seq = sequencesService.GetNextSequenceByTableName("[pro].[MstEntregables]");

                        deliverable.IdEntregable = seq.SecuencialTabla;

                        sequencesService.InsertSequence(seq);

                        MstEntregables deliverableTemp = new MstEntregables
                        {
                            Descripcion = deliverable.Descripcion,
                            DuracionEstimadaDias = deliverable.DuracionEstimadaDias,
                            DuracionEstimadaHoras = deliverable.DuracionEstimadaHoras,
                            FechaFin = deliverable.FechaFin,
                            FechaInicio = deliverable.FechaInicio,
                            FechaIns = deliverable.FechaIns,
                            FechaUpd = deliverable.FechaUpd,
                            HorasDedicadas = deliverable.HorasDedicadas,
                            IdEntregable = deliverable.IdEntregable,
                            IdEstado = deliverable.IdEstado,
                            IdProyecto = deliverable.IdProyecto,
                            IdUsuarioIns = deliverable.IdUsuarioIns,
                            IdUsuarioUpd = deliverable.IdUsuarioUpd,
                            Nota = deliverable.Nota
                        };

                        // Se calculan la duración en días y horas de la fase del proyecto
                        result = CalculateEstimatedDurationInDaysAndHours(ref deliverableTemp, project);
                        if (result.MessageType != MessageType.SUCCESS)
                            return result;

                        //// Se obtiene los entregables por proyecto que no hayan sido editados
                        //var deliverablesNotEditing = Repository.GetMany(e => e.IdProyecto == project.IdProyecto && e.FechaUpd == null).ToList();

                        //// Se le suma uno por el registro nuevo que se esta agregando
                        //int deliverablesCount = deliverablesNotEditing.Count + 1;

                        //// Se actualizan los entregables
                        //foreach (MstEntregables item in deliverablesNotEditing)
                        //{
                        //    // No se le setea la fecha ni el usuario que actualiza, ya que este proceso solo se aplica 
                        //    // cuando se está planificando el proyecto
                        //    item.Presupuesto = project.Presupuesto / deliverablesCount;
                        //    Repository.Update(item);
                        //}

                        //// Se establece el presupuesto del entregable
                        //deliverable.Presupuesto = project.Presupuesto / deliverablesCount;

                        // Guardando el nuevo registros
                        Repository.Add(deliverableTemp);


                        // Se inserta el registro de presupuesto vinculado al entregable
                        BudgetService budgetService = new BudgetService(_unitOfWork);
                        var budgetResult = budgetService.InsertBudget(new MstPresupuesto
                        {
                            Activo = true,
                            FechaIns = DateTime.Now,
                            IdEntregable = deliverableTemp.IdEntregable,
                            IdMoneda = project.IdMoneda,
                            IdProyecto = project.IdProyecto,
                            IdUsuarioIns = deliverableTemp.IdUsuarioIns,
                            PresupuestoActual = deliverable.Presupuesto,
                            PresupuestoAsignado = deliverable.Presupuesto,
                            PresupuestoEjecutado = 0
                        }, false);

                        if (budgetResult.MessageType != MessageType.SUCCESS)
                            throw budgetResult.DetailException;


                        // Se actualiza el estado del proyecto 
                        var deliverables = Repository.GetMany(e => e.IdProyecto == project.IdProyecto).ToList();

                        // Si es el primer entregable
                        if (deliverables.Count == 0)
                        {
                            ProjectService projectService = new ProjectService();

                            // Se establece el estado en planificación del proyecto
                            var resultProjectStatus = projectService.UpdateProjectStatus(Catalog.ProjectStatusInPlanning, project, _unitOfWork);
                            if (resultProjectStatus.MessageType != MessageType.SUCCESS)
                                throw resultProjectStatus.DetailException;
                        }

                        _unitOfWork.Commit();
                    }
                    else
                    {
                        result.Message = "La fase de proyecto ya se encuentra registrada";
                        result.DetailException = new Exception(result.Message);
                        result.MessageType = MessageType.ERROR;
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorSaveData, "fase de proyecto");
                result.MessageType = MessageType.ERROR;

                if (ex.GetType().Name == "DbEntityValidationException")
                    result.DetailException = new Exception(Utilities.GetEntityValidationErrors(ex as System.Data.Entity.Validation.DbEntityValidationException));
                else
                    result.DetailException = ex;
            }

            return result;
        }

        public ResultBase UpdateDeliverable(Entity.Custom.Deliverable deliverable)
        {
            ResultBase result = new ResultBase();

            try
            {
                MstProyectos project = new MstProyectos();

                var resultValidate = ValidateDeliverableInfo(deliverable, ref project, true);

                if (resultValidate.MessageType == MessageType.ERROR)
                    result = resultValidate;
                else
                {
                    // Se valida que no existe un entregable con la misma descripción
                    bool deliverableExist = Repository.Any(p => p.Descripcion == deliverable.Descripcion &&
                                                                p.IdProyecto == deliverable.IdProyecto &&
                                                                p.IdEntregable != deliverable.IdEntregable);

                    if (!deliverableExist)
                    {
                        // Se valida que exista el registro de entregable
                        MstEntregables deliverableTemp = Repository.GetById(deliverable.IdEntregable, deliverable.IdProyecto);

                        if (deliverableTemp != null)
                        {
                            deliverableTemp.Descripcion = deliverable.Descripcion;
                            deliverableTemp.Nota = deliverable.Nota;

                            deliverableTemp.IdUsuarioUpd = deliverable.IdUsuarioUpd;
                            deliverableTemp.FechaUpd = DateTime.Now;

                            if (deliverableTemp.FechaInicio != deliverable.FechaInicio || deliverableTemp.FechaFin != deliverable.FechaFin)
                            {
                                deliverableTemp.FechaInicio = deliverable.FechaInicio;
                                deliverableTemp.FechaFin = deliverable.FechaFin;

                                // Se calculan la duración en días y horas de la fase del proyecto
                                result = CalculateEstimatedDurationInDaysAndHours(ref deliverableTemp, project);
                                if (result.MessageType != MessageType.SUCCESS)
                                    return result;
                            }

                            Repository.Update(deliverableTemp);

                            // Se actualiza el registro de presupuesto
                            BudgetService budgetService = new BudgetService(_unitOfWork);
                            var budgetResult = budgetService.GetBudgetByDeliverable(deliverableTemp.IdEntregable, deliverableTemp.IdProyecto);

                            if (budgetResult.MessageType == MessageType.INFO)
                            {
                                var budget = budgetResult.Data;
                                budget.PresupuestoAsignado = deliverable.Presupuesto;
                                budget.PresupuestoActual = deliverable.Presupuesto;
                                budget.IdUsuarioUpd = deliverable.IdUsuarioUpd;
                                budget.FechaUpd = DateTime.Now;

                                var updateBudgetResult = budgetService.UpdateBudget(budget, false);
                                if (updateBudgetResult.MessageType != MessageType.SUCCESS)
                                    throw updateBudgetResult.DetailException;
                            }
                            else
                                throw budgetResult.DetailException;

                            _unitOfWork.Commit();
                        }
                        else
                        {
                            result.Message = "La fase de proyecto que está intentando actualizar no existe";
                            result.DetailException = new Exception(result.Message);
                            result.MessageType = MessageType.ERROR;
                            return result;
                        }
                    }
                    else
                    {
                        result.Message = "Ya existe una fase de proyecto registrada con la misma descripción";
                        result.DetailException = new Exception(result.Message);
                        result.MessageType = MessageType.ERROR;
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorUpdateData, "fase de proyecto");
                result.MessageType = MessageType.ERROR;

                if (ex.GetType().Name == "DbEntityValidationException")
                    result.DetailException = new Exception(Utilities.GetEntityValidationErrors(ex as System.Data.Entity.Validation.DbEntityValidationException));
                else
                    result.DetailException = ex;
            }

            return result;
        }

        public ResultBase DeleteDeliverable(int deliverableID, int projectID)
        {
            ResultBase result = new ResultBase();

            try
            {
                if (projectID > 0)
                {
                    var projectRep = _unitOfWork.GetRepository<MstProyectos>();

                    // Se verifica el estado del proyecto
                    MstProyectos project = projectRep.GetById(projectID);

                    // Se obtiene el detalle catálogo del estado del proyecto para verificarlo
                    var dcRepository = _unitOfWork.GetRepository<DetCatalogo>();
                    var detailCatalog = dcRepository.Get(p => p.IdDetCatalogo == project.IdEstado);

                    // Solo se puede eliminar entregables si el proyecto está en planificación
                    if (detailCatalog.Valor != Catalog.ProjectStatusInPlanning)
                    {
                        result.Message = "El proyecto debe estar en planificación para poder realizar esta acción";
                        result.DetailException = new Exception(result.Message);
                        result.MessageType = MessageType.ERROR;
                        return result;
                    }

                    if (deliverableID > 0)
                    {
                        // Se valida que el entregable no tenga tareas 
                        int taskCount = Repository.GetById(deliverableID, projectID).MstTareas.Count;

                        if (taskCount == 0)
                        {
                            // Se actualiza el estado del proyecto (Devolviendolo a pendiente de planificar)
                            var deliverables = Repository.GetMany(e => e.IdProyecto == project.IdProyecto).ToList();

                            // Si es el único entregable
                            if (deliverables.Count == 1)
                            {
                                ProjectService projectService = new ProjectService();

                                // Se establece el estado en pendiente de planificar
                                var resultProjectStatus = projectService.UpdateProjectStatus(Catalog.ProjectStatusPendingToBePlanned, project, _unitOfWork);
                                if (resultProjectStatus.MessageType != MessageType.SUCCESS)
                                    throw resultProjectStatus.DetailException;
                            }

                            // Se elimina el registro de presupuesto
                            BudgetService budgetService = new BudgetService(_unitOfWork);
                            var budgetResult = budgetService.DeleteBudget(deliverableID, projectID, false);

                            if (budgetResult.MessageType != MessageType.SUCCESS)
                                throw budgetResult.DetailException;

                            // Se elimina el registro de entregable
                            Repository.Delete(e => e.IdEntregable == deliverableID && e.IdProyecto == projectID);

                            _unitOfWork.Commit();
                        }
                        else
                        {
                            result.Message = "La fase de proyecto contiene tareas dependientes, para continuar con esta acción debe eliminar las tareas";
                            result.DetailException = new Exception(result.Message);
                            result.MessageType = MessageType.ERROR;
                            return result;
                        }
                    }
                    else
                    {
                        result.Message = "Se debe proporcionar el identificador de la fase de proyecto";
                        result.DetailException = new Exception(result.Message);
                        result.MessageType = MessageType.ERROR;
                        return result;
                    }
                }
                else
                {
                    result.Message = "Se debe proporcionar el identificador del proyecto";
                    result.DetailException = new Exception(result.Message);
                    result.MessageType = MessageType.ERROR;
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorDeleteData, "fase de proyecto");
                result.MessageType = MessageType.ERROR;

                if (ex.GetType().Name == "DbEntityValidationException")
                    result.DetailException = new Exception(Utilities.GetEntityValidationErrors(ex as System.Data.Entity.Validation.DbEntityValidationException));
                else
                    result.DetailException = ex;
            }

            return result;
        }

        #region Otros Métodos

        private ResultBase ValidateDeliverableInfo(Entity.Custom.Deliverable deliverable, ref MstProyectos project, bool isUpdate = false)
        {
            ResultBase result = new ResultBase();

            if (deliverable == null)
            {
                result.MessageType = MessageType.ERROR;
                result.Message = string.Format(Messages.WithoutValidate, "fase de proyecto");
                result.DetailException = new Exception(result.Message + ". Debido a que el objeto MstProyectos es nulo");
                goto @return;
            }

            if (isUpdate)
            {
                if (!deliverable.IdUsuarioUpd.HasValue || (deliverable.IdUsuarioUpd.HasValue && deliverable.IdUsuarioUpd.Value == 0))
                {
                    result.MessageType = MessageType.ERROR;
                    result.Message = Messages.UserInsUpdInvalid;
                    result.DetailException = new Exception("Id de usuario que actualiza no proporcionado");
                    goto @return;
                }
            }
            else
            {
                // Se valida que se haya proporcionado el usuario que inserta el registro
                if (deliverable.IdUsuarioIns == 0)
                {
                    result.MessageType = MessageType.ERROR;
                    result.Message = Messages.UserInsUpdInvalid;
                    result.DetailException = new Exception("Id de usuario que inserta no proporcionado");
                    goto @return;
                }
            }

            // Se valida el id del proyecto
            if (deliverable.IdProyecto == 0)
            {
                result.MessageType = MessageType.ERROR;
                result.Message = "Se debe proporcionar el identificador del proyecto al cual pertenece la fase";
                result.DetailException = new Exception(result.Message);
                goto @return;
            }
            else
            {
                var projectRep = _unitOfWork.GetRepository<MstProyectos>();
                project = projectRep.GetById(deliverable.IdProyecto);
            }

            if (string.IsNullOrEmpty(deliverable.Descripcion))
            {
                result.MessageType = MessageType.ERROR;
                result.Message = "Se debe proporcionar la descripción de la fase del proyecto";
                result.DetailException = new Exception(result.Message);
                goto @return;
            }

            // Se validan que la fecha fin de la fase de proyecto no sea mayor a la fecha de inicio
            if (deliverable.FechaFin.CompareTo(deliverable.FechaInicio) < 0)
            {
                result.MessageType = MessageType.ERROR;
                result.Message = "La fecha fin de la fase de proyecto debe ser mayor a la inicial";
                result.DetailException = new Exception(result.Message);
                goto @return;
            }


            // Se valida el estado del proyecto
            if (deliverable.IdEstado <= 0)
            {
                result.MessageType = MessageType.ERROR;
                result.Message = "Se debe proporcionar el estado de la fase de proyecto";
                result.DetailException = new Exception(result.Message);
                goto @return;
            }

            // Se validan las fecha de la fase contra las fechas del proyecto
            if (project != null)
            {
                // Se valida que la fecha inicio del entregable sea mayor a la fecha inicio del proyecto
                if (deliverable.FechaInicio.CompareTo(project.FechaInicio) < 0)
                {
                    result.MessageType = MessageType.ERROR;
                    result.Message = "La fecha inicio de la fase debe ser mayor o igual a la fecha de inicio del proyecto";
                    result.DetailException = new Exception(result.Message);
                    goto @return;
                }

                // Se valida que la fecha inicio del entregable sea menor a la fecha límite del proyecto
                if (deliverable.FechaInicio.CompareTo(project.FechaLimite) > 0 || deliverable.FechaInicio.CompareTo(project.FechaLimite) == 0)
                {
                    result.MessageType = MessageType.ERROR;
                    result.Message = "La fecha inicio de la fase debe ser menor a la fecha fin del proyecto";
                    result.DetailException = new Exception(result.Message);
                    goto @return;
                }

                // Se valida que la fecha fin del entregable sea mayor a la fecha inicio del proyecto
                if (deliverable.FechaFin.CompareTo(project.FechaInicio) < 0 || deliverable.FechaFin.CompareTo(project.FechaInicio) == 0)
                {
                    result.MessageType = MessageType.ERROR;
                    result.Message = "La fecha fin de la fase debe ser mayor a la fecha de inicio del proyecto";
                    result.DetailException = new Exception(result.Message);
                    goto @return;
                }

                // Se valida que la fecha fin del entregable sea menor o igual a la fecha fin del proyecto
                if (deliverable.FechaFin.CompareTo(project.FechaLimite) > 0)
                {
                    result.MessageType = MessageType.ERROR;
                    result.Message = "La fecha fin de la fase debe ser menor o igual a la fecha de fin del proyecto";
                    result.DetailException = new Exception(result.Message);
                    goto @return;
                }
            }

        @return:
            return result;
        }

        #region Cálculos

        private ResultBase CalculateEstimatedDurationInDaysAndHours(ref MstEntregables deliverable, MstProyectos project)
        {
            ResultBase result = new ResultBase();

            try
            {
                // Se obtiene la cantidad de horas laborales para un horario de medio tiempo
                var parameter = _unitOfWork.GetRepository<CatParametros>().Get(p => p.NombreParametro.Equals("CANT_HORAS_LABORALES_MT"));
                double hoursMiddleTimeCount = double.Parse(parameter.ValorParametro);

                if (project != null)
                {
                    int workingDaysPerWeekID = project.DiasLabSem;

                    // Se obtiene las jornada laborales
                    var detailCatalogService = new DetailCatalogService();

                    var resultDetailCatalog = detailCatalogService.GetDetailsByTable("JornadaLaboralSemanal");

                    if (resultDetailCatalog.MessageType != MessageType.INFO)
                        throw resultDetailCatalog.DetailException;

                    var workday = resultDetailCatalog.Data.FirstOrDefault(w => w.IdDetCatalogo == workingDaysPerWeekID);

                    int strHorarioId = int.Parse(workday.Valor);

                    // Se obtienen los días laborales que el horario posee 
                    var workdayCount = _unitOfWork.GetRepository<DetHorario>().GetMany(s => s.IdHorario == strHorarioId);

                    // Se cargan en un listado de dias de la semana
                    Dictionary<DayOfWeek, double> lstDaysOfWeek = new Dictionary<DayOfWeek, double>();
                    foreach (var day in workdayCount)
                    {
                        double workingHours = day.HoraSalida.Subtract(day.HoraEntrada).TotalHours;

                        // Si el horario excede la cantidad de horas de medio tiempo se le resta una hora (Almuerzo)
                        if (workingHours > hoursMiddleTimeCount)
                            workingHours = workingHours - 1;

                        lstDaysOfWeek.Add((DayOfWeek)day.Dia, workingHours);
                    }

                    // Calculando los dias laborables
                    double estimatedDurationInDays = 0, estimatedDurationInHours = 0;
                    TimeSpan ts = deliverable.FechaFin - deliverable.FechaInicio;

                    for (int i = 0; i < ts.Days; i++)
                    {
                        // Si el dia de la semana está en la lista de dias de la semana laborables, se suma un día
                        if (lstDaysOfWeek.Keys.Contains((deliverable.FechaInicio.AddDays(i)).DayOfWeek))
                        {
                            double workingHours = lstDaysOfWeek[(deliverable.FechaInicio.AddDays(i)).DayOfWeek];

                            // Si el día tiene horario de tiempo completo
                            if (workingHours > hoursMiddleTimeCount)
                                estimatedDurationInDays = estimatedDurationInDays + 1; // Se le suma un día
                            else
                                estimatedDurationInDays = estimatedDurationInDays + 0.5; // De lo contrario se le suma medio dia

                            // Se suman las horas
                            estimatedDurationInHours = estimatedDurationInHours + lstDaysOfWeek[(deliverable.FechaInicio.AddDays(i)).DayOfWeek];
                        }
                    }

                    // Restando los días feriados 
                    var listHolidayActive = _unitOfWork.GetRepository<CatFeriados>().GetMany(h => h.Activo);

                    // Se recorren todos los días feriado
                    foreach (var holiday in listHolidayActive)
                    {
                        // Se arma una nueva fecha con el dia del mes feriado pero con el año actual
                        DateTime newDate = new DateTime(DateTime.Now.Year, holiday.FechaFeriado.Month, holiday.FechaFeriado.Day);

                        // Si el día feriado está entre las fechas del inicio y fin del proyecto, se resta el día y las horas laborables de ese día
                        if ((newDate.CompareTo(deliverable.FechaInicio) == 0 || newDate.CompareTo(deliverable.FechaInicio) > 0) &&
                            (newDate.CompareTo(deliverable.FechaFin) == 0 || newDate.CompareTo(deliverable.FechaFin) < 0))
                        {
                            estimatedDurationInDays = estimatedDurationInDays - 1;
                            estimatedDurationInHours = estimatedDurationInHours + lstDaysOfWeek[newDate.DayOfWeek];
                        }
                    }

                    // Se establece la duración estimada en días y horas del proyecto
                    deliverable.DuracionEstimadaDias = Convert.ToDecimal(estimatedDurationInDays);
                    deliverable.DuracionEstimadaHoras = Convert.ToDecimal(estimatedDurationInHours);

                    //// Se actualiza el registro de proyecto
                    //Repository.Update(project);
                }
                else
                {
                    result.Message = string.Format(Messages.NotExist, "proyecto");
                    result.DetailException = new Exception("No se encontró registro de proyecto con el id: " + project.IdProyecto);
                    result.MessageType = MessageType.ERROR;
                }
            }
            catch (Exception ex)
            {
                result.Message = "Ocurrió un error al intentar calcular la duración estimada en días del proyecto";
                result.MessageType = MessageType.ERROR;
                result.DetailException = ex;
            }

            return result;
        }

        #endregion

        #endregion

    }
}
