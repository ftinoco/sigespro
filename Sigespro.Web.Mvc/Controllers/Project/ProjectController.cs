﻿using Common.Helpers;
using Common.Log;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Newtonsoft.Json;
using Sigespro.Entity;
using Sigespro.Service.Admin;
using Sigespro.Service.Config;
using Sigespro.Service.Pre;
using Sigespro.Service.Pro;
using Sigespro.Web.Mvc.Models.Configuration;
using Sigespro.Web.Mvc.Models.Project.Deliverables;
using Sigespro.Web.Mvc.Models.Project;
using Sigespro.Web.Mvc.Resources;
using Sigespro.Web.Mvc.Utilities.Attributes.Security;
using Sigespro.Web.Mvc.Utilities.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using static Common.Helpers.Utilities;
using Sigespro.Web.Mvc.Models.Project.Tasks;

namespace Sigespro.Web.Mvc.Controllers.Project
{
    [RoutePrefix("Project")]
    public class ProjectController : Controller
    {
        private readonly IUserSession _userSession;

        public ProjectService Service { get; set; }

        #region Propiedades

        public List<ProgramProject> ProgramsSelected
        {
            get
            {
                if (TempData.Keys.Contains("ProgramsSelected"))
                    return TempData.Peek("ProgramsSelected") as List<ProgramProject>;
                else
                    return new List<ProgramProject>();
            }
            set { TempData["ProgramsSelected"] = value; }
        }

        public List<ProjectManager> ManagersSelected
        {
            get
            {
                if (TempData.Keys.Contains("ManagersSelected"))
                    return TempData.Peek("ManagersSelected") as List<ProjectManager>;
                else
                    return new List<ProjectManager>();
            }
            set { TempData["ManagersSelected"] = value; }
        }

        public List<GoalsProject> GoalsInsertedProject
        {
            get
            {
                if (TempData.Keys.Contains("GoalsInsertedProject"))
                    return TempData.Peek("GoalsInsertedProject") as List<GoalsProject>;
                else
                    return new List<GoalsProject>();
            }
            set { TempData["GoalsInsertedProject"] = value; }
        }

        #endregion

        public ProjectController()
        {
            Service = new ProjectService();
            _userSession = new UserSession();
        }

        [Route]
        public ActionResult Index(QueryOptions<IEnumerable<ListProjectViewModel>> queryOpt)
        {
            TempData.Clear();

            string message = string.Empty;
            IEnumerable<ListProjectViewModel> model = new List<ListProjectViewModel>();

            try
            {
                string strSerialize = JsonConvert.SerializeObject(queryOpt);

                QueryOptions<IEnumerable<vwProyectos>> filter = JsonConvert.DeserializeObject<QueryOptions<IEnumerable<vwProyectos>>>(strSerialize);
                Service.GetProjectsForList(ref filter);

                if (filter.ResultData.MessageType != MessageType.INFO)
                {
                    message = filter.ResultData.Message;
                    Logger.Log(filter.ResultData.MessageType, filter.ResultData.DetailException, _userSession.Username,
                               MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                {
                    strSerialize = JsonConvert.SerializeObject(filter);

                    queryOpt = JsonConvert.DeserializeObject<QueryOptions<IEnumerable<ListProjectViewModel>>>(strSerialize);

                    model = queryOpt.ResultData.Data;
                }
            }
            catch (Exception ex)
            {
                message = Messages.LoadPageError;
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            if (!string.IsNullOrEmpty(message))
                ModelState.AddModelError(Messages.AlertDanger, message);

            ViewBag.Filter = true;
            ViewBag.QueryOptions = queryOpt;

            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [Route("Add")]
        public ActionResult AddProject()
        {
            ProjectViewModel model = new ProjectViewModel();

            ViewBag.IsEdit = false;
            ViewBag.Action = "SaveProject";
            ViewBag.Title = "Agregar Proyecto";
            ViewBag.ActiveTab = Tabs.GeneralInformation;

            model.FechaInicio = DateTime.Now;
            model.FechaLimite = DateTime.Now;

            return AddEditProject(model);
        }

        [Route("Edit/{projectId}")]
        public ActionResult EditProject(int projectId)
        {
            TempData.Clear();
            ProjectViewModel model = new ProjectViewModel();

            try
            {
                var result = Service.GetProjectById(projectId);

                if (result.MessageType == MessageType.INFO)
                {
                    var temp = result.Data;

                    // Se carga la información general del proyecto
                    model = new ProjectViewModel
                    {
                        DescProyecto = temp.DescProyecto,
                        DiasLabSem = temp.DiasLabSem,
                        DuracionEstimadaDias = temp.DuracionEstimadaDias,
                        DuracionEstimadaHoras = temp.DuracionEstimadaHoras,
                        Externo = temp.Externo,
                        FechaFinEstimada = temp.FechaFinEstimada,
                        FechaInicio = temp.FechaInicio,
                        FechaIns = temp.FechaIns,
                        FechaLimite = temp.FechaLimite,
                        FechaUpd = temp.FechaUpd,
                        GastoEstimado = temp.GastoEstimado,
                        HorasDedicadas = temp.HorasDedicadas,
                        IdEstado = temp.IdEstado,
                        IdMoneda = temp.IdMoneda,
                        IdProyecto = temp.IdProyecto,
                        IdTipoProyecto = temp.IdTipoProyecto,
                        IdUsuarioIns = temp.IdUsuarioIns,
                        IdUsuarioUpd = temp.IdUsuarioUpd,
                        Justificacion = temp.Justificacion,
                        Presupuesto = temp.Presupuesto,
                        PresupuestoActual = temp.PresupuestoActual,
                        TituloProyecto = temp.TituloProyecto
                    };

                    // Se carga el listado de programas vinculados al proyecto y el listado en memoria
                    var programs = new List<ProgramProject>();
                    //model.DetProgramaProyecto = new List<ProgramProject>();
                    foreach (var item in temp.DetProgramaProyecto)
                    {
                        // Se carga el listado local
                        programs.Add(new ProgramProject
                        {
                            IdProyecto = item.IdProyecto,
                            IdProgramaProyecto = item.IdProgramaProyecto,
                            IdPrograma = item.IdPrograma
                        });

                        //model.DetProgramaProyecto.Add(new ProgramProject
                        //{
                        //    IdPrograma = item.IdPrograma,
                        //    IdProgramaProyecto = item.IdProgramaProyecto,
                        //    IdProyecto = item.IdProyecto
                        //});
                    }
                    // Se asigna al listado de programas en memoria
                    ProgramsSelected = programs;

                    // Se carga el listado de responsables del proyecto
                    var managers = new List<ProjectManager>();
                    //model.DetProyectoResponsable = new List<ProjectManager>();
                    foreach (var item in temp.DetProyectoResponsable)
                    {
                        // Se carga el listado en la variable local
                        managers.Add(new ProjectManager
                        {
                            IdProyecto = item.IdProyecto,
                            IdPersonal = item.IdPersonal,
                            IdProyectoResponsable = item.IdProyectoResponsable
                        });

                        //model.DetProyectoResponsable.Add(new ProjectManager
                        //{
                        //    IdProyecto = item.IdProyecto,
                        //    IdPersonal = item.IdPersonal,
                        //    IdProyectoResponsable = item.IdProyectoResponsable
                        //});
                    }
                    // Se asigna al listado de responsables en memoria
                    ManagersSelected = managers;

                    // Se carga el listado de objetivos del proyecto
                    var goalsProject = new List<GoalsProject>();
                    //model.DetProyectoObjetivos = new List<GoalsProject>();
                    foreach (var item in temp.DetProyectoObjetivos)
                    {
                        goalsProject.Add(new GoalsProject
                        {
                            IdObjetivo = item.IdObjetivo,
                            IdProyecto = item.IdProyecto,
                            IdProyectoObjetivo = item.IdProyectoObjetivo,
                            MstObjetivos = new GoalsViewModel
                            {
                                Activo = item.MstObjetivos.Activo,
                                IdObjetivo = item.MstObjetivos.IdObjetivo,
                                Descripcion = item.MstObjetivos.Descripcion,
                                TipoObjetivo = item.MstObjetivos.TipoObjetivo
                            }
                        });
                    }
                    // Se asigna al listado de objetivos en memoria
                    GoalsInsertedProject = goalsProject;

                    // Se carga la información del grupo meta
                    model.DetProyectoBeneficiarios = new BeneficiaryProject
                    {
                        Descripcion = temp.DetProyectoBeneficiarios.Descripcion,
                        IdBeneficiario = temp.DetProyectoBeneficiarios.IdBeneficiario,
                        IdProyecto = temp.DetProyectoBeneficiarios.IdProyecto,
                        TipoDestinatario = temp.DetProyectoBeneficiarios.TipoDestinatario
                    };

                }
                else
                {
                    ModelState.AddModelError(Messages.AlertDanger, result.Message);
                    Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                               MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(Messages.AlertDanger, Messages.ErrorApi);
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            ViewBag.IsEdit = true;
            ViewBag.Action = "UpdateProject";
            ViewBag.Title = "Editar Proyecto";
            ViewBag.ActiveTab = Tabs.GeneralInformation;

            return AddEditProject(model);
        }

        [NonAction]
        private ActionResult AddEditProject(ProjectViewModel model)
        {
            string message = string.Empty;

            try
            {
                DetailCatalogService dtService = new DetailCatalogService();
                List<SelectListItem> selectListItem = new List<SelectListItem>();
                Result<IEnumerable<DetCatalogo>> resultDetCat = new Result<IEnumerable<DetCatalogo>>();

                // Obteniendo estados de proyecto
                resultDetCat = dtService.GetDetailsByTable("EstadoProyecto");

                selectListItem.Add(new SelectListItem { Value = "0", Text = Labels.Select });

                foreach (var c in resultDetCat.Data.ToList())
                {
                    bool selected = false;

                    if (c.Valor == Entity.Resource.Catalog.ProjectStatusPendingToBePlanned)
                    {
                        selected = true;
                        model.IdEstado = c.IdDetCatalogo;
                    }

                    selectListItem.Add(new SelectListItem
                    {
                        Value = c.IdDetCatalogo.ToString(),
                        Text = c.Valor,
                        Selected = selected
                    });
                }

                ViewBag.ListOfProjectStatus = new SelectList(selectListItem, "Value", "Text");

                // Obteniendo tipos de proyecto
                selectListItem = new List<SelectListItem>();
                resultDetCat = dtService.GetDetailsByTable("TipoProyecto");

                selectListItem.Add(new SelectListItem { Value = "0", Text = Labels.Select });

                resultDetCat.Data.ToList().ForEach(c => selectListItem.Add(
                    new SelectListItem
                    {
                        Value = c.IdDetCatalogo.ToString(),
                        Text = c.Valor
                    }
                ));

                ViewBag.ListOfProjectType = new SelectList(selectListItem, "Value", "Text");

                // Obteniendo jornadas laborales semanales
                selectListItem = new List<SelectListItem>();
                resultDetCat = dtService.GetDetailsByTable("JornadaLaboralSemanal");

                selectListItem.Add(new SelectListItem { Value = "0", Text = Labels.Select });

                resultDetCat.Data.ToList().ForEach(c => selectListItem.Add(
                    new SelectListItem
                    {
                        Value = c.IdDetCatalogo.ToString(),
                        Text = c.Descripcion
                    }
                ));

                ViewBag.ListOfWeeklyWorkday = new SelectList(selectListItem, "Value", "Text");

                // Se obtienen los monedas
                CurrencyService currencyService = new CurrencyService();
                var resultCurrency = currencyService.GetCurrencies("activo");
                if (resultCurrency.MessageType == MessageType.INFO)
                //    message = resultCurrency.Message;
                //else
                {
                    selectListItem = new List<SelectListItem>();

                    selectListItem.Add(new SelectListItem { Value = "0", Text = Labels.Select });

                    resultCurrency.Data.ToList().ForEach(c => selectListItem.Add(
                        new SelectListItem
                        {
                            Value = c.IdMoneda.ToString(),
                            Text = c.Nombre
                        }
                    ));
                }

                ViewBag.Currencies = new SelectList(selectListItem, "Value", "Text");
            }
            catch (Exception ex)
            {
                message = Messages.ErrorApi;
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                           MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            if (!string.IsNullOrEmpty(message))
                ModelState.AddModelError(Messages.AlertDanger, message);

            return View("AddProject", model);
        }

        [AjaxOnly]
        [HttpPost]
        [Route("Save")]
        [AllowAnonymous]
        public ActionResult SaveProject(ProjectViewModel model)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                if (ModelState.IsValid)
                {
                    model.IdUsuarioIns = _userSession.UserId;

                    // Se carga el listado de programas
                    if (!IsNull(ProgramsSelected))
                    {
                        model.DetProgramaProyecto = new List<ProgramProject>();
                        foreach (var item in ProgramsSelected)
                        {
                            model.DetProgramaProyecto.Add(new ProgramProject
                            {
                                IdPrograma = item.IdPrograma,
                                IdProyecto = model.IdProyecto
                            });
                        }
                    }

                    if (!IsNull(ManagersSelected))
                    {
                        model.DetProyectoResponsable = new List<ProjectManager>();
                        foreach (var item in ManagersSelected)
                        {
                            model.DetProyectoResponsable.Add(new ProjectManager
                            {
                                IdPersonal = item.IdPersonal,
                                IdProyecto = model.IdProyecto
                            });
                        }
                    }

                    string strSerialize = JsonConvert.SerializeObject(model);

                    var result = Service.InsertProject(JsonConvert.DeserializeObject<MstProyectos>(strSerialize));

                    if (result.MessageType != MessageType.SUCCESS)
                    {
                        message = result.Message;
                        status = result.MessageType.ToString();
                        Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                                MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                    }
                    else
                        message = string.Format(Messages.SaveSuccess, "proyecto");
                }
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message, action = Url.Action("Index", "Project") },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [AjaxOnly]
        [HttpPost]
        [Route("Update")]
        [AllowAnonymous]
        public ActionResult UpdateProject(ProjectViewModel model)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                if (ModelState.IsValid)
                {
                    model.IdUsuarioUpd = _userSession.UserId;

                    // Se carga el listado de programas
                    if (!IsNull(ProgramsSelected))
                    {
                        model.DetProgramaProyecto = new List<ProgramProject>();
                        foreach (var item in ProgramsSelected)
                        {
                            model.DetProgramaProyecto.Add(new ProgramProject
                            {
                                IdPrograma = item.IdPrograma,
                                IdProyecto = model.IdProyecto
                            });
                        }
                    }

                    // Se cargan los responsables cargados
                    if (!IsNull(ManagersSelected))
                    {
                        model.DetProyectoResponsable = new List<ProjectManager>();
                        foreach (var item in ManagersSelected)
                        {
                            model.DetProyectoResponsable.Add(new ProjectManager
                            {
                                IdPersonal = item.IdPersonal,
                                IdProyecto = model.IdProyecto
                            });
                        }
                    }

                    // Si el usuario no seleccionó la pestaña de objetivos se establecen los objetivos cargados
                    if (IsNull(model.DetProyectoObjetivos))
                        model.DetProyectoObjetivos = GoalsInsertedProject;

                    string strSerialize = JsonConvert.SerializeObject(model);

                    var result = Service.UpdateProject(JsonConvert.DeserializeObject<MstProyectos>(strSerialize));

                    if (result.MessageType != MessageType.SUCCESS)
                    {
                        message = result.Message;
                        status = result.MessageType.ToString();
                        Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                    }
                    else
                        message = string.Format(Messages.SaveSuccess, "proyecto");
                }
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message, action = Url.Action("Index", "Project") },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        #region Planificación

        [Route("Planning/{projectId}")]
        public ActionResult Planning(int projectId)
        {
            decimal budgetCount = 0;
            ViewBag.InPlanning = false;
            ViewBag.ProjectID = projectId;
            ViewBag.ApprovePlanning = false;
            ViewBag.Title = "Planificación de Proyecto";

            List<DeliverableViewModel> model = new List<DeliverableViewModel>();

            try
            {
                if (TempData.Keys.Contains("MessageDel"))
                    ModelState.AddModelError(Messages.AlertSuccess, TempData["MessageDel"].ToString());

                var project = Service.GetProjectById(projectId);

                if (budgetCount > project.Data.Presupuesto)
                {
                    string budgetMessageError = "El total del presupuesto de las fases es mayor al presupuesto del proyecto.";
                    budgetMessageError += "Ajustar el presupuesto de las fase o extender el del proyecto, para poder pasar al siguente estado.";

                    ViewBag.BudgetMessageError = budgetMessageError;
                }
                bool inPlanning = false, approvePlanning = false;
                ValidateProject(project.Data.IdEstado, ref inPlanning, ref approvePlanning);

                ViewBag.InPlanning = inPlanning;
                ViewBag.ApprovePlanning = approvePlanning;

                BudgetService budgetService = new BudgetService();
                DeliverableService service = new DeliverableService();

                var result = service.GetDeliverablesByProjectID(projectId);

                if (result.MessageType == MessageType.INFO)
                {
                    //budgetCount = result.Data.Sum(e => e.Presupuesto);

                    //string dSerialize = JsonConvert.SerializeObject(result.Data);
                    //model = JsonConvert.DeserializeObject<List<DeliverableViewModel>>(dSerialize);

                    foreach (var item in result.Data)
                    {
                        List<TaskViewModel> tasks = new List<TaskViewModel>();

                        if (item.MstTareas != null)
                        {
                            foreach (var task in item.MstTareas)
                            {
                                var newTask = new TaskViewModel
                                {
                                    FechaFin = task.FechaFin,
                                    FechaInicio = task.FechaInicio,
                                    Hito = task.Hito,
                                    TareaPeriodica = task.TareaPeriodica,
                                    HorasDedicadas = task.HorasDedicadas,
                                    FecuenciaAvance = task.FecuenciaAvance,
                                    IdEntregable = task.IdEntregable,
                                    IdEstado = task.IdEstado,
                                    IdProyecto = task.IdProyecto,
                                    IdTarea = task.IdTarea,
                                    Nombre = task.Nombre,
                                    Nota = task.Nota,
                                };

                                decimal budget = 0;
                                budgetService.GetBudgetByTask(task.IdTarea, item.IdEntregable, item.IdProyecto, ref budget);
                                newTask.Presupuesto = budget;
                                //if (task.MstPresupuesto != null && task.MstPresupuesto.Count > 0)
                                //    newTask.Presupuesto = task.MstPresupuesto.FirstOrDefault().PresupuestoAsignado;

                                tasks.Add(newTask);
                            }
                        }

                        model.Add(new DeliverableViewModel
                        {
                            Descripcion = item.Descripcion,
                            DuracionEstimadaDias = item.DuracionEstimadaDias,
                            DuracionEstimadaHoras = item.DuracionEstimadaHoras,
                            FechaFin = item.FechaFin,
                            FechaInicio = item.FechaInicio,
                            FechaIns = item.FechaIns,
                            FechaUpd = item.FechaUpd,
                            HorasDedicadas = item.HorasDedicadas,
                            IdEntregable = item.IdEntregable,
                            //IdEntregableParent = item.IdEntregableParent,
                            IdEstado = item.IdEstado,
                            IdProyecto = item.IdProyecto,
                            IdUsuarioIns = item.IdUsuarioIns,
                            IdUsuarioUpd = item.IdUsuarioUpd,
                            Nota = item.Nota,
                            Presupuesto = item.MstPresupuesto.FirstOrDefault().PresupuestoAsignado,
                            Tasks = tasks
                        });
                    }
                }
                else
                {/*

                    model.Add(new DeliverableViewModel
                    {
                        //IdEntregableParent = item.IdEntregableParent,
                        IdProyecto = item.IdProyecto,
                        IdUsuarioIns = item.IdUsuarioIns,
                        IdUsuarioUpd = item.IdUsuarioUpd,
                        Nota = item.Nota,
                        Presupuesto = item.MstPresupuesto.FirstOrDefault().PresupuestoAsignado,
                        Tasks = tasks
                    });*/
                }
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                    MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return View(model);
        }

        //[AjaxOnly]
        //[HttpPost]
        [AllowAnonymous]
        public ActionResult PartialPlanning(int projectId)
        {
            ViewData["InPlanning"] = false;

            BudgetService budgetService = new BudgetService();
            DeliverableService service = new DeliverableService();

            var result = service.GetDeliverablesByProjectID(projectId);

            List<DeliverableViewModel> model = null;

            if (result.MessageType == MessageType.INFO)
            {
                model = new List<DeliverableViewModel>();

                foreach (var item in result.Data)
                {
                    List<TaskViewModel> tasks = new List<TaskViewModel>();

                    if (item.MstTareas != null)
                    {
                        foreach (var task in item.MstTareas)
                        {
                            var newTask = new TaskViewModel
                            {
                                FechaFin = task.FechaFin,
                                FechaInicio = task.FechaInicio,
                                Hito = task.Hito,
                                TareaPeriodica = task.TareaPeriodica,
                                HorasDedicadas = task.HorasDedicadas,
                                FecuenciaAvance = task.FecuenciaAvance,
                                IdEntregable = task.IdEntregable,
                                IdEstado = task.IdEstado,
                                IdProyecto = task.IdProyecto,
                                IdTarea = task.IdTarea,
                                Nombre = task.Nombre,
                                Nota = task.Nota
                            };

                            decimal budget = 0;
                            budgetService.GetBudgetByTask(task.IdTarea, item.IdEntregable, item.IdProyecto, ref budget);
                            newTask.Presupuesto = budget;
                            //if (task.MstPresupuesto != null && task.MstPresupuesto.Count > 0)
                            //    newTask.Presupuesto = task.MstPresupuesto.FirstOrDefault().PresupuestoAsignado;

                            tasks.Add(newTask);
                        }
                    }

                    model.Add(new DeliverableViewModel
                    {
                        Descripcion = item.Descripcion,
                        DuracionEstimadaDias = item.DuracionEstimadaDias,
                        DuracionEstimadaHoras = item.DuracionEstimadaHoras,
                        FechaFin = item.FechaFin,
                        FechaInicio = item.FechaInicio,
                        FechaIns = item.FechaIns,
                        FechaUpd = item.FechaUpd,
                        HorasDedicadas = item.HorasDedicadas,
                        IdEntregable = item.IdEntregable,
                        //IdEntregableParent = item.IdEntregableParent,
                        IdEstado = item.IdEstado,
                        IdProyecto = item.IdProyecto,
                        IdUsuarioIns = item.IdUsuarioIns,
                        IdUsuarioUpd = item.IdUsuarioUpd,
                        Nota = item.Nota,
                        Presupuesto = item.MstPresupuesto.FirstOrDefault().PresupuestoAsignado,
                        Tasks = tasks
                    });
                }

                var project = Service.GetProjectById(projectId);
                bool inPlanning = false, approvePlanning = false;
                ValidateProject(project.Data.IdEstado, ref inPlanning, ref approvePlanning);

                ViewData["InPlanning"] = inPlanning;
            }

            //if (model.Count > 0)
            //{
                ViewBag.ProjectID = projectId;

                return View("Partials/Project/_projectPlanning", model);
            //}
            //else
            //{
            //    return new JsonResult
            //    {
            //        Data = new { status = "SUCCESS", message = Messages.ProjectNoTask },
            //        JsonRequestBehavior = JsonRequestBehavior.AllowGet
            //    };
            //}
        }

        [AjaxOnly]
        [HttpPost]
        [AllowAnonymous]
        public ActionResult ApprovePlanning(int projectId)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                var result = Service.ApprovePlanningProject(projectId);

                if (result.MessageType != MessageType.SUCCESS)
                {
                    message = result.Message;
                    status = result.MessageType.ToString();
                    Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                    message = "La planificación del proyecto fue aprobada exitosamente";

            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        #endregion

        #region Reportes

        [AllowAnonymous]
        [Route("View/{projectId}")]
        public ActionResult ViewProject(int projectId)
        {
            string mensaje = string.Empty;

            ViewBag.UrlReport = Url.Action("ShowReport", "Project", new { projectId });
            ViewBag.ProjectID = projectId;

            return PartialView("Partials/Report");
        }

        [HttpGet]
        [AllowAnonymous]
        public void ShowReport(int projectId)
        {
            ExportReport(projectId, ExportFormatType.PortableDocFormat);
        }

        [AllowAnonymous]
        public void ExportReportToExcel(int projectId)
        {
            ExportReport(projectId, ExportFormatType.Excel);
        }

        [AllowAnonymous]
        public void ExportReportToWord(int projectId)
        {
            ExportReport(projectId, ExportFormatType.WordForWindows);
        }

        [NonAction]
        private void ExportReport(int projectId, ExportFormatType exportFormat)
        {
            try
            {
                string strRptPath = Path.Combine(Server.MapPath("~/Reports/Project"), "project.rpt");

                ReportDocument reportDocument = new ReportDocument();

                reportDocument.Load(strRptPath);

                ProgramService programService = new ProgramService();
                var result = Service.GetProjectByIdForReport(projectId);

                if (result.MessageType == MessageType.INFO)
                {
                    var listProjects = new List<vwProyectos>();
                    listProjects.Add(result.Data);

                    reportDocument.SetDataSource(listProjects);

                    // Se carga los objetivos del proyecto
                    GoalService goalService = new GoalService();
                    var resultGoals = goalService.GetGoalsByProjectId(result.Data.IdProyecto);

                    if (resultGoals.MessageType == MessageType.INFO)
                    {
                        reportDocument.Subreports["ProjectGoals"].SetDataSource(resultGoals.Data);
                    }

                    // Se cargan los programas que ejecutaran el proyecto
                    var resultProjectPrograms = programService.GetProjectPrograms(result.Data.IdProyecto);

                    if (resultProjectPrograms.MessageType == MessageType.INFO)
                    {
                        List<RptProjectPrograms> projectPrograms = new List<RptProjectPrograms>();
                        resultProjectPrograms.Data.ToList().ForEach(p => projectPrograms.Add(new RptProjectPrograms
                        {
                            Activo = p.Activo,
                            Descripcion = p.Descripcion,
                            NombrePrograma = p.NombrePrograma
                        }));

                        reportDocument.Subreports["ProjectPrograms"].SetDataSource(projectPrograms);

                        // Se cargan los reposables del proyecto
                        if (resultProjectPrograms.Data.Count() > 0)
                        {
                            List<RptProjectManagers> projectManagers = new List<RptProjectManagers>();
                            int[] programsId = new int[resultProjectPrograms.Data.Count()];

                            for (int i = 0; i < resultProjectPrograms.Data.Count(); i++)
                            {
                                programsId[i] = resultProjectPrograms.Data.ToList()[i].IdPrograma;
                            }

                            var lisProjectManagers = GetPersonalByPrograms(programsId);
                            lisProjectManagers.ForEach(p => projectManagers.Add(new RptProjectManagers
                            {
                                Activo = p.Activo,
                                NombrePrograma = p.NombrePrograma,
                                Cargo = p.Cargo,
                                IdCargo = p.IdCargo,
                                Identificacion = p.Identificacion,
                                IdPersona = p.IdPersona,
                                IdPersonal = p.IdPersonal,
                                IdPrograma = p.IdPrograma.Value,
                                Nombre = p.Nombre
                            }));

                            reportDocument.Subreports["ProjectManagers"].SetDataSource(projectManagers);
                        }
                    }
                }

                var programResult = programService.GetProgramById(_userSession.ProgramId);

                if (programResult.MessageType == MessageType.INFO)
                {
                    reportDocument.SetParameterValue("programName", programResult.Data.Descripcion);
                    reportDocument.SetParameterValue("programAcronym", programResult.Data.NombrePrograma);
                }

                reportDocument.SetParameterValue("userName", _userSession.Username);

                reportDocument.ExportToHttpResponse(new ExportOptions
                {
                    ExportFormatType = exportFormat //ExportFormatType.PortableDocFormat
                }, System.Web.HttpContext.Current.Response, false, ("Proyecto_" + result.Data.IdProyecto));

            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, HttpContext.User.Identity.Name,
                           MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }
        }

        #endregion

        #region Otras Acciones

        [AllowAnonymous]
        public ActionResult LoadGoals(ProjectViewModel model)
        {
            bool selectedG = false;
            List<SelectListItem> selectList = new List<SelectListItem>();

            if (IsNull(model.DetProyectoObjetivos))
            {
                if (IsNull(GoalsInsertedProject) || (!IsNull(GoalsInsertedProject) && GoalsInsertedProject.Count == 0))
                {
                    model.DetProyectoObjetivos = new List<GoalsProject>();
                    model.DetProyectoObjetivos.Add(new GoalsProject
                    {
                        MstObjetivos = new GoalsViewModel
                        {
                            Descripcion = null,
                            TipoObjetivo = "G"
                        }
                    });

                    selectedG = true;
                }
                else
                {
                    model.DetProyectoObjetivos = GoalsInsertedProject;
                }
            }

            selectList.Add(new SelectListItem { Value = "G", Text = "General", Selected = selectedG });
            selectList.Add(new SelectListItem { Value = "E", Text = "Específico" });
            ViewBag.GoalsType = new SelectList(selectList, "Value", "Text");

            return PartialView("Partials/Project/_goals", model);
        }

        [AllowAnonymous]
        public ActionResult AddGoal(ProjectViewModel model)
        {
            ModelState.Clear();
            model.DetProyectoObjetivos.Add(new GoalsProject
            {
                MstObjetivos = new GoalsViewModel
                {
                    Descripcion = null,
                    TipoObjetivo = "E"
                }
            });

            return LoadGoals(model);
        }

        [AllowAnonymous]
        public ActionResult DeleteGoal(ProjectViewModel model, int index)
        {
            ModelState.Clear();

            var temp = model.DetProyectoObjetivos.Skip(index).Take(1).FirstOrDefault();

            if (!IsNull(temp) && temp.MstObjetivos.TipoObjetivo == "G")
            {
                var nextGoal = model.DetProyectoObjetivos.Skip(index + 1).Take(1).FirstOrDefault();
                nextGoal.MstObjetivos.TipoObjetivo = "G";
            }

            model.DetProyectoObjetivos.RemoveAt(index);

            return LoadGoals(model);
        }


        [AllowAnonymous]
        public ActionResult LoadPrograms()
        {
            List<ProgramViewModel> model = new List<ProgramViewModel>();

            try
            {
                ProgramService programService = new ProgramService();

                var result = programService.GetProgramsForBindToProject(_userSession.ProgramId);

                if (result.MessageType == MessageType.INFO)
                {
                    foreach (var item in result.Data)
                    {
                        bool selected = false;

                        if (ProgramsSelected.Count > 0)
                            selected = ProgramsSelected.Exists(p => p.IdPrograma == item.IdPrograma);

                        model.Add(new ProgramViewModel
                        {
                            Activo = item.Activo,
                            Descripcion = item.Descripcion,
                            FechaIns = item.FechaIns,
                            FechaUpd = item.FechaUpd,
                            IdPrograma = item.IdPrograma,
                            IdUsuarioIns = item.IdUsuarioIns,
                            IdUsuarioUpd = item.IdUsuarioUpd,
                            NombrePrograma = item.NombrePrograma,
                            Selected = selected
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return PartialView("Partials/project/_programs", model);
        }

        [AllowAnonymous]
        public ActionResult SelectProgram(List<ProgramViewModel> programs, bool @checked = true)
        {
            if (ProgramsSelected.Count == 0)
                ProgramsSelected = new List<ProgramProject>();

            foreach (var item in programs)
            {
                var program = ProgramsSelected.FirstOrDefault(p => p.IdPrograma == item.IdPrograma);

                if (IsNull(program))
                {
                    ProgramsSelected.Add(new ProgramProject
                    {
                        IdPrograma = item.IdPrograma
                    });
                }
                else
                {
                    if (!@checked)
                        ProgramsSelected.RemoveAll(p => p.IdPrograma == item.IdPrograma);
                }
            }

            return LoadPrograms();
        }


        [AllowAnonymous]
        public ActionResult LoadPersonal()
        {
            List<ListPersonalViewModel> model = new List<ListPersonalViewModel>();

            if (ProgramsSelected.Count > 0)
            {
                int[] programsId = new int[ProgramsSelected.Count];

                for (int i = 0; i < ProgramsSelected.Count; i++)
                {
                    programsId[i] = ProgramsSelected[i].IdPrograma;
                }

                model = GetPersonalByPrograms(programsId);
            }

            return PartialView("Partials/Project/_managers", model);
        }

        [AllowAnonymous]
        public ActionResult SelectPerson(List<ListPersonalViewModel> model, bool @checked = true)
        {
            if (ManagersSelected.Count == 0)
                ManagersSelected = new List<ProjectManager>();

            foreach (var item in model)
            {
                var person = ManagersSelected.FirstOrDefault(p => p.IdPersonal == item.IdPersonal);

                if (IsNull(person))
                {
                    ManagersSelected.Add(new ProjectManager
                    {
                        IdPersonal = item.IdPersonal
                    });
                }
                else
                {
                    if (!@checked)
                        ManagersSelected.RemoveAll(p => p.IdPersonal == item.IdPersonal);
                }
            }

            return LoadPersonal();
        }


        [AllowAnonymous]
        public ActionResult GetProjectInfo(int projectID)
        {
            ListProjectViewModel model = new ListProjectViewModel();

            var result = Service.GetProjectByIdForReport(projectID);

            if (result.MessageType == MessageType.INFO)
            {
                var temp = result.Data;

                // Se carga la información general del proyecto
                model = new ListProjectViewModel
                {
                    DescProyecto = temp.DescProyecto,
                    DuracionEstimadaDias = temp.DuracionEstimadaDias,
                    DuracionEstimadaHoras = temp.DuracionEstimadaHoras,
                    FechaFinEstimada = temp.FechaFinEstimada,
                    FechaInicio = temp.FechaInicio,
                    FechaLimite = temp.FechaLimite,
                    GastoEstimado = temp.GastoEstimado,
                    HorasDedicadas = temp.HorasDedicadas,
                    IdEstado = temp.IdEstado,
                    IdMoneda = temp.IdMoneda,
                    IdProyecto = temp.IdProyecto,
                    Justificacion = temp.Justificacion,
                    Presupuesto = temp.Presupuesto,
                    PresupuestoActual = temp.PresupuestoActual,
                    TituloProyecto = temp.TituloProyecto
                };
            }

            return PartialView("Partials/Project/_projectInfo", model);
        }

        #endregion

        #region Otros Métodos

        private List<ListPersonalViewModel> GetPersonalByPrograms(int[] programsId)
        {
            List<ListPersonalViewModel> model = new List<ListPersonalViewModel>();
            PersonalService personalService = new PersonalService();

            var result = personalService.GetPersonalByPrograms(programsId);

            if (result.MessageType == MessageType.SUCCESS)
            {
                foreach (var item in result.Data)
                {
                    bool selected = false;

                    if (ManagersSelected.Count > 0)
                        selected = ManagersSelected.Exists(p => p.IdPersonal == item.IdPersonal);

                    // Se valida que la persona sea coordinador de alguno de los programas seleccionados
                    if (personalService.IsCoordinator(item.IdPersonal, programsId).MessageType == MessageType.SUCCESS)
                    {
                        model.Add(new ListPersonalViewModel
                        {
                            Activo = item.MstPersonas.Activo,
                            Cargo = item.Cargo,
                            IdCargo = item.IdCargo,
                            Identificacion = item.MstPersonas.Identificacion,
                            IdPersona = item.IdPersona,
                            IdPersonal = item.IdPersonal,
                            IdPrograma = item.IdPrograma,
                            Nombre = item.MstPersonas.NombreCompleto,
                            NombrePrograma = item.MstPrograma.NombrePrograma,
                            Selected = selected
                        });
                    }
                }
            }

            return model;
        }

        private void ValidateProject(int projectStatusId, ref bool inPlanning, ref bool approvePlanning)
        {
            ParameterService pService = new ParameterService();
            var parameter = pService.GetParameterByName("ROL_DIRECTOR");

            if (parameter.MessageType == MessageType.SUCCESS)
            {
                Service.Security.UserService uService = new Service.Security.UserService();
                var roles = uService.GetRolesByUserID(_userSession.UserId);
                if (roles.MessageType == MessageType.SUCCESS)
                {
                    int rolDirector = int.Parse(parameter.Data.ValorParametro);

                    DetailCatalogService catService = new DetailCatalogService();
                    var lstCatalog = catService.GetDetailsCatalog();
                    if (lstCatalog.MessageType == MessageType.SUCCESS)
                    {
                        var dtCatalog = lstCatalog.Data.SingleOrDefault(d => d.IdDetCatalogo == projectStatusId);
                        if (dtCatalog.Valor == Entity.Resource.Catalog.ProjectStatusInPlanning)
                            if (roles.Data.Any(r => r.RoleId == rolDirector))
                                approvePlanning = true;

                        if (dtCatalog.Valor == Entity.Resource.Catalog.ProjectStatusPendingToBePlanned ||
                            dtCatalog.Valor == Entity.Resource.Catalog.ProjectStatusInPlanning)
                            inPlanning = true;
                    }
                }
            }
        }
        #endregion
    }
}