﻿(function ($) {

    setDateGlobalizeOptions();

    /**
     * Sobre escribe el método de validación para que tomará cantidades formateadas con comas
     * 
     * @param {type} value 
     * 
     * @param {type} element
     * 
     * @param {type} param
     * 
     * @returns {type} 
     */
    $.validator.methods.range = function (value, element, param) {

        var globalizedValue = value.replace(/\,/g, ''); //value.replace(",", ".");

        return this.optional(element) || (globalizedValue >= param[0] && globalizedValue <= param[1]);
    }

    $.fn.resetValidation = function () {

        var $form = this.closest('form');

        //reset jQuery Validate's internals
        $form.validate().resetForm();

        //reset unobtrusive validation summary, if it exists
        $form.find("[data-valmsg-summary=true]")
            .removeClass("validation-summary-errors")
            .addClass("validation-summary-valid")
            .find("ul").empty();

        //reset unobtrusive field level, if it exists
        $form.find("[data-valmsg-replace]")
            .removeClass("field-validation-error")
            .addClass("field-validation-valid")
            .empty();

        return $form;
    };

    $.fn.ajaxValidate = function () {
        var $this = $(this);
        var form = $this.closest("form")
            .removeData("validator")
            .removeData("unobtrusiveValidation");

        $.validator.unobtrusive.parse(form);

        return $this.valid();
    };

    $(document).ajaxError(function (jQXHR, request, settings) {
        console.log(request.status);
        console.log(request.statusText);
        console.log(request.responseText);

        console.log(JSON.stringify(jQXHR));
        //console.log(request);
        //console.log(settings);
    });

}(jQuery));

document.ondragover = document.ondragenter = document.ondrop = function (e) {
    e.preventDefault();
    return false;
}

function setDateGlobalizeOptions() {
    // Globalize options - initially just the date format used for parsing
    // Users can customise this to suit them
    $.validator.methods.dateGlobalizeOptions = {
        dateParseFormat: "DD/MM/YYYY",
        dateTimeParseFormat: "DD/MM/YYYY hh:mm:ss a"
    };

    // Tell the validator that we want dates parsed using Globalize
    $.validator.methods.date = function (value, element) {
        var ynValidDate = true;

        try {

            ynValidDate = moment(value, $.validator.methods.dateGlobalizeOptions.dateParseFormat, true).isValid();

            // Si no es valido el formato de fecha, se valida con el formato Fecha/Hora
            if (!ynValidDate)
                ynValidDate = moment(value, $.validator.methods.dateGlobalizeOptions.dateTimeParseFormat, true).isValid();
        }
        catch (err) {
            ynValidDate = false;
        }

        return this.optional(element) || (ynValidDate);
    };
}
