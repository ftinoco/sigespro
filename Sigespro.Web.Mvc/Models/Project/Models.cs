﻿using Sigespro.Web.Mvc.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sigespro.Web.Mvc.Models.Project
{
    public enum Tabs
    {
        GeneralInformation = 1,
        Goals = 2,
        Programs = 3,
        Managers = 4
    }

    public class ProgramProject
    {
        public int IdProgramaProyecto { get; set; }

        public int IdPrograma { get; set; }

        public int IdProyecto { get; set; }
    }

    public class BeneficiaryProject
    {
        public int IdProyecto { get; set; }

        public string TipoDestinatario { get; set; }

        public int? IdBeneficiario { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "TargetGroup", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string Descripcion { get; set; }
    }

    public class GoalsProject
    {
        public int IdProyectoObjetivo { get; set; }

        public int IdProyecto { get; set; }

        public int IdObjetivo { get; set; }

        public GoalsViewModel MstObjetivos { get; set; }
    }
    
    public class ProjectManager
    {
        public int IdProyectoResponsable { get; set; }

        public int IdProyecto { get; set; }

        public int? IdPersonal { get; set; }
    }
}