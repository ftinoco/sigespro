﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Sigespro.DAL.Interface
{
    public interface IRepository<T> where T : class
    {
        void Add(T entity);

        void Update(T entity);

        void Delete(T entity);

        void Delete(Expression<Func<T, bool>> where);

        T GetById(object Id, object Id2 = null);
        
        T Get(Expression<Func<T, bool>> where);

        IEnumerable<T> GetAll();

        IEnumerable<T> GetMany(Expression<Func<T, bool>> where);

        long Count(Expression<Func<T, bool>> where);

        long Count();
    }
}
