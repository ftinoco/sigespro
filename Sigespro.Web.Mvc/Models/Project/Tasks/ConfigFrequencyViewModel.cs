﻿using Sigespro.Web.Mvc.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sigespro.Web.Mvc.Models.Project.Tasks
{
    public class ConfigFrequencyViewModel
    {
        public int TipoFrecuencia { get; set; }

        [Display(Name = "Cada Cuantos Días")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public int? CantidadDias { get; set; }

        [Display(Name = "StartHour", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public TimeSpan? HoraInicio { get; set; }

        [Display(Name = "EndHour", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public TimeSpan? HoraFin { get; set; }

        [Display(Name = "Cada Cuantas Semanas")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public int? CantidadSemanas { get; set; }

        [Display(Name = "Día de inicio")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string DiaInicio { get; set; }

        [Display(Name = "Cada Cuantos Meses")]
        public int? CantidadMeses { get; set; }

        public int? NumeroSemana { get; set; }

        public int? CantidadAnios { get; set; }

        public string Mes { get; set; }

        [Display(Name = "Duración en días")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public int? DuracionDias { get; set; }

        [Display(Name = "Día del Mes")]
        public int? DiaDelMes { get; set; }

        public IntervaloFrecuency DetIntervaloFrecuencia { get; set; }
    }
}