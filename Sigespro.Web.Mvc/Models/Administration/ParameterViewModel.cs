﻿using Sigespro.Web.Mvc.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sigespro.Web.Mvc.Models.Administration
{
    public class ParameterViewModel
    {
        public int IdPerametro { get; set; }

        [Display(Name = "ParameterName", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string NombreParametro { get; set; }

        [Display(Name = "ParameterValue", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string ValorParametro { get; set; }

        [Display(Name = "Description", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string DescParametro { get; set; }
    }
}