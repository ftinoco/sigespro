﻿using Sigespro.Entity.Resource;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigespro.Entity.Metadata.Pro
{
    public class MstProyectosMetadata
    {
        [Key]
        public int IdProyecto { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string TituloProyecto { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string DescProyecto { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public System.DateTime FechaInicio { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public System.DateTime FechaLimite { get; set; }

        [DataType(DataType.Currency)]
        [Range(1, double.MaxValue, ErrorMessage = "El presupuesto debe ser mayor a 0")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public decimal Presupuesto { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public int IdTipoProyecto { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public int IdEstado { get; set; }

    }
}
