﻿/**
 * FUNCIONES DE DIAS FERIADOS
 */
(function (app) {

    var module = {};

    module.postIsOk = function (response) {
        if (response.status !== app.STATUS_CODE.ERROR)
            app.showModalMessage(response.status, response.message, null, module.redirectTo);
        else
            app.showModalMessage(response.status, response.message);
    };

    module.postError = function (response) {
        console.log(response.responseText);
        app.showModalMessage(app.STATUS_CODE.ERROR, app.MESSAGE_ERROR);
    };

    module.redirectTo = function () {
        window.location.href = app.URL_BASE + "Holiday";
    }

    app.holiday = module;

}(sigesproApp || {}));