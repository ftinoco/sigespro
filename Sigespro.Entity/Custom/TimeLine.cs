﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigespro.Entity.Custom
{
    public class TimeLine
    {
        public string Tarea { get; set; }
        public DateTime FechaEjecucion { get; set; }
        public string Comentarios { get; set; }
    }
}
