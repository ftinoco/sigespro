﻿namespace Sigespro.DAL.Interface
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}
