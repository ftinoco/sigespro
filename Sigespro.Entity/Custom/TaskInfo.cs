﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigespro.Entity.Custom
{
    public class TaskInfo
    {
        public int TaskID { get; set; }
         
        public int DeliverableID { get; set; }

        public int ProjectID { get; set; }

        public string TaskName { get; set; }

        public int StatusID { get; set; }

        public string Status { get; set; }

        public string Author { get; set; }

        public string Manager { get; set; }

        public DateTime? LastUpdate { get; set; }

        public decimal? Progress { get; set; }

        public string Comments { get; set; }

        public bool AddAdvance { get; set; }
    }
}
