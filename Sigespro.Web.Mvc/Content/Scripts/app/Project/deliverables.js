﻿(function (app) {

    var module = {};

    module.addDeliverable = function (e) {
      
        $.ajax({
            type: "POST",
            url: $(e.target).data("action"),
            beforeSend: function () {
                app.toggleLoadingModal();
            },
            success: function (response) {
                if (response.status !== undefined)
                    app.showModalMessage(response.status, response.message, app.MODAL_SIZE.SM);
                else {
                    app.showModalDialog("Agregar Fase de Proyecto", response, module.saveDeliverable, null, module.closeDialogAddDeliverable, null, app.MODAL_SIZE.LG);

                    $("#btnAcceptConfirm").html('<i class="material-icons" style="pointer-events: none;">save</i> Guardar');
                    $("#btnAcceptConfirm").data('close', false);

                    $("#btnCancelConfirm").html('<i class="material-icons" style="pointer-events: none;">cancel</i> Cerrar');
                     
                    $('#FechaInicio').datepicker({
                        language: "es",
                        autoclose: true,
                        todayHighlight: true,
                        todayBtn: "linked",
                        startDate: $('#ProjectStartDate').val(),
                        endDate: $('#ProjectEndDate').val()
                    }).on('changeDate', function (e) {
                        $('#FechaFin').val("");
                        $('#FechaFin').unbind().removeData();
                        module.initEndDate(e.date); 
                    });

                    module.initEndDate($('#FechaInicio').val());

                    $("#frmDeliverable").resetValidation();

                    app.init();
                    app.formater();
                }
            },
            error: function (data) {
                console.log(data.responseText);
                app.showModalMessage(app.STATUS_CODE.ERROR, app.MESSAGE_ERROR, app.MODAL_SIZE.MD);
            },
            statusCode: {
                401: function () {
                    window.location.replace(app.URL_BASE + "Account/LogOut");
                }
            }
        }).always(function (jqXHR, textStatus, error) {
            app.toggleLoadingModal();
        });

    };

    module.initEndDate = function (startDate) {
        console.log(startDate);
        $('#FechaFin').datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true,
            clearBtn: true,
            startDate: startDate,
            endDate: $('#ProjectEndDate').val()
        });
    };

    module.saveDeliverable = function (e) {

        $("#btnAcceptConfirm").prop("disabled", true);

        if ($("#frmDeliverable").ajaxValidate()) {

            var frmData = new FormData(document.getElementById('frmDeliverable'));

            $.ajax({
                url: $("#frmDeliverable").attr("action"),
                type: $("#frmDeliverable").attr("method"),
                data: frmData,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    app.toggleLoadingModal();
                },
                success: function (response) {
                    if (response.status === app.STATUS_CODE.SUCCESS){
                        $("#frmDeliverable").data("save", true);
                        $("#modalConfirm").modal("hide");
                        app.showModalMessage(response.status, response.message, app.MODAL_SIZE.MD, app.project.refreshPlanning, [frmData.get("IdProyecto"), frmData.get("UrlRefresh")]);
                    }
                    else {
                        app.showAlert("deliverable-alert", response.status, response.message);
                        //$("#btnAcceptConfirm").prop("disabled", false);
                    }
                },
                error: function (data) {
                    console.log(data.responseText);
                    app.showAlert("deliverable-alert", app.STATUS_CODE.ERROR, app.MESSAGE_ERROR);
                },
                statusCode: {
                    401: function () {
                        window.location.replace(app.URL_BASE + "Account/LogOut");
                    }
                }
            }).always(function () {
                app.toggleLoadingModal();
                $("#btnAcceptConfirm").prop("disabled", false);
            });
        } 
    };

    module.closeDialogAddDeliverable = function () {
        if ($("#frmDeliverable").data("save")) {
            window.location.reload(true);
        }
    };

    module.confirmDeleteDeliverable = function (e, projectID, url) {
        $("#btnAcceptConfirm").html('Aceptar');
        $("#btnAcceptConfirm").data('close', true);

        $("#btnCancelConfirm").html('Cerrar');
        app.showModalDialog("Mensaje de Confirmación", "¿Desea eliminar esta fase del proyecto?", module.deleteDeliverable, [e, projectID, url], null, null, app.MODAL_SIZE.SM);
    };

    module.deleteDeliverable = function (e, projectID, url) {

        $.ajax({
            type: "POST",
            url: $(e.target).data("action"),
            beforeSend: function () {
                app.toggleLoadingModal();
            },
            success: function (response) {
                if (response.status !== undefined) {
                    console.log(projectID);
                    app.showModalMessage(response.status, response.message, app.MODAL_SIZE.MD, app.project.refreshPlanning, [projectID, url]);
                }
            },
            error: function (data) {
                console.log(data.responseText);
                app.showModalMessage(app.STATUS_CODE.ERROR, app.MESSAGE_ERROR, app.MODAL_SIZE.MD, null, null);
            },
            statusCode: {
                401: function () {
                    window.location.replace(app.URL_BASE + "Account/LogOut");
                }
            }
        }).always(function (jqXHR, textStatus, error) {
            app.toggleLoadingModal();
        });
    };

    module.addDelFinancialBudget = function (e) {
        e.preventDefault();

        var data = {};
        var frmData = new FormData(document.getElementById('frmBudget'));
        for (var key of frmData.keys()) {
            if (key === "FinancialBudget") {
                data["FinancialBudget"] = JSON.parse(frmData.get(key));
            }
            else
                data[key] = frmData.get(key);
        }

        console.log($(e.target).data("action"));
        $.ajax({
            type: "POST",
            url: $(e.target).data("action"),
            data: JSON.stringify({ "model": data }),
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                app.toggleLoadingModal();
            },
            success: function (response) {
                if (response) {
                    $("#financialbudget").html(response);
                    app.formater();
                }
            },
            error: function (data) {
                console.log(data.responseText);
                app.showModalMessage(app.STATUS_CODE.ERROR, app.MESSAGE_ERROR, app.MODAL_SIZE.MD, null, null);
            },
            statusCode: {
                401: function () {
                    window.location.replace(app.URL_BASE + "Account/LogOut");
                }
            }
        }).always(function (jqXHR, textStatus, error) {
            app.toggleLoadingModal();
        });
    };
    
    module.addRowFinancialBudget = function (e) {
        e.preventDefault();
        console.log($("#financialbudget"));
        var valid = false;
        var form = $("#financialbudget").removeData("validator").removeData("unobtrusiveValidation");

        $.validator.unobtrusive.parse(form);
        var validator = form.validate();

        validator.settings.ignore = '.valid';

        valid = validator.element($("select[id*='__TaskID']"));

        valid = validator.element($("select[id*='__EntryID']")) && valid;

        valid = validator.element($("input[id*='__BudgetMount']")) && valid;
        if (valid) {
            this.addDelFinancialBudget(e);
        }
    };

    module.addDelPhysicalBudget = function (e) {
        e.preventDefault();

        var data = {};
        var frmData = new FormData(document.getElementById('frmBudget'));
        for (var key of frmData.keys()) {
            if (key === "PhysicalBudget") {
                data["PhysicalBudget"] = JSON.parse(frmData.get(key));
            }
            else
                data[key] = frmData.get(key);
        }

        //console.log(data);
        $.ajax({
            type: "POST",
            url: $(e.target).data("action"),
            data: JSON.stringify({ "model": data }),
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                app.toggleLoadingModal();
            },
            success: function (response) {
                if (response) {
                    $("#physicalbudget").html(response);
                    app.formater();
                }
            },
            error: function (data) {
                console.log(data.responseText);
                app.showModalMessage(app.STATUS_CODE.ERROR, app.MESSAGE_ERROR, app.MODAL_SIZE.MD, null, null);
            },
            statusCode: {
                401: function () {
                    window.location.replace(app.URL_BASE + "Account/LogOut");
                }
            }
        }).always(function (jqXHR, textStatus, error) {
            app.toggleLoadingModal();
        });
    };

    module.addRowPhysicalBudget = function (e) {
        e.preventDefault();

        var valid = false;
        var form = $("#physicalbudget").removeData("validator").removeData("unobtrusiveValidation");

        $.validator.unobtrusive.parse(form);
        var validator = form.validate();

        validator.settings.ignore = '.valid';

        valid = validator.element($("select[id*='__TaskID']"));

        valid = validator.element($("select[id*='__EntryID']")) && valid;

        valid = validator.element($("select[id*='__ArticleID']")) && valid;
        
        valid = validator.element($("input[id*='__Quantity']")) && valid;
        
        valid = validator.element($("input[id*='__Cost']")) && valid;
        
        valid = validator.element($("input[id*='__BudgetMount']")) && valid;

        if (valid) {
            this.addDelPhysicalBudget(e);
        }
    };

    module.onChangeSelect = function (e) {
        var element = $(e.target).data("dependency");
        $('#' + element).val($('#' + e.target.id + " option:selected").text());
    }

    module.onChangeQuantity = function (e) {

        var elBudget = $(e.target).data("budget");
        var cost = $("input[name='" + $(e.target).data("cost") + "']").val();

        if ((cost.match(/^-?\d*(\.\d+)?$/)) && ($(e.target).val().match(/^-?\d*(\.\d+)?$/)))
            $("input[name='" + elBudget + "']").val(cost * $(e.target).val());
    }

    app.deliverables = module;

})(sigesproApp || {});