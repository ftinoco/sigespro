﻿using Sigespro.Web.Mvc.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sigespro.Web.Mvc.Models.Administration
{
    public class UserViewModel
    {
        public int UserId { get; set; }

        [Display(Name = "Persona")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public int? ReferenceId { get; set; }

        [Display(Name = "Nombre de Usuario")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string UserName { get; set; }

        [Display(Name = "Name", ResourceType = typeof(Labels))]
        public string FullName { get; set; }

        public string Password { get; set; }

        [Display(Name = "Email", ResourceType = typeof(Labels))]
        public string Email { get; set; }

        [Display(Name = "PhoneNumber", ResourceType = typeof(Labels))]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public short ExpirationSession { get; set; }

        [Display(Name = "Active", ResourceType = typeof(Labels))]
        public bool Active { get; set; }

        public int UserInsert { get; set; }

        public DateTime InsertDate { get; set; }

        public int? UserUpdate { get; set; }

        public DateTime? UpdateDate { get; set; }

        public int RoleId { get; set; }
    }
}