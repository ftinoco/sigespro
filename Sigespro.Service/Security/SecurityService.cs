﻿using Common.Extensions;
using Common.Helpers;
using Sigespro.DAL.Implementations;
using Sigespro.Entity;
using Sigespro.Entity.Resource;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigespro.Service.Security
{
    public class SecurityService : Service
    {
        private Repository<Systems> Repository { get; set; }

        public SecurityService()
        {
            Repository = _unitOfWork.GetRepository<Systems>();
        }

        public ResultBase BackupDatabase()
        {
            ResultBase result = new ResultBase();

            try
            {

                using (var context = new SigesproEntities())
                {
                    string dbname = context.Database.Connection.Database;

                    var parameter = context.CatParametros.FirstOrDefault(p => p.NombreParametro == "BACKUP_FOLDER");

                    if (parameter != null)
                    {

                        string backupname = string.Format("sigespro_{0}.bak", DateTime.Now.ToString("yyyyMMddHHmm"));
                        backupname = parameter.ValorParametro + backupname;

                        parameter = context.CatParametros.FirstOrDefault(p => p.NombreParametro == "BACKUP_SCRIPT");

                        if (parameter != null)
                        {
                            string sqlCommand = string.Format(parameter.ValorParametro, dbname, backupname);

                            int path = context.Database.ExecuteSqlCommand(System.Data.Entity.TransactionalBehavior.DoNotEnsureTransaction, sqlCommand);
                            result.Message = "Se respaldó exitosamente la base de datos";
                        }
                        else
                        {
                            result.Message = "No se encontró el registro de parámetro con el nombre : «BACKUP_SCRIPT»";
                            result.DetailException = new Exception(result.Message);
                            result.MessageType = MessageType.ERROR;
                        }

                    }
                    else
                    {
                        result.Message = "No se encontró el registro de parámetro con el nombre : «BACKUP_FOLDER»";
                        result.DetailException = new Exception(result.Message);
                        result.MessageType = MessageType.ERROR;
                    }
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = "Ocurrió un error al crear el respaldo de base de datos";
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public ResultBase RestoreDatabase(string backupname)
        {
            ResultBase result = new ResultBase();

            try
            {
                string sqlCommand = "";
                string strCnn = "";
                using (var context = new SigesproEntities())
                {
                    strCnn = context.Database.Connection.ConnectionString;
                    string dbname = context.Database.Connection.Database;

                    var parameter = context.CatParametros.FirstOrDefault(p => p.NombreParametro == "BACKUP_FOLDER");

                    if (parameter != null)
                    {
                        //string backupname = string.Format(parameter.ValorParametro, DateTime.Now.ToString("yyyyMMddHHmm"));
                        backupname = parameter.ValorParametro + backupname;
                        var extension = System.IO.Path.GetExtension(backupname).ToUpper();

                        if (extension == ".BAK")
                        {
                            parameter = context.CatParametros.FirstOrDefault(p => p.NombreParametro == "RESTORE_BACKUP");

                            if (parameter != null) 
                                sqlCommand = string.Format(parameter.ValorParametro, dbname, backupname); 
                            else
                            {
                                result.Message = "No se encontró el registro de parámetro con el nombre : «RESTORE_BACKUP»";
                                result.DetailException = new Exception(result.Message);
                                result.MessageType = MessageType.ERROR;
                            }
                        }
                        else
                        {
                            result.Message = "Tipo de archivo no válido";
                            result.DetailException = new Exception(result.Message);
                            result.MessageType = MessageType.ERROR;
                        }
                    }
                    else
                    {
                        result.Message = "No se encontró el registro de parámetro con el nombre : «BACKUP_FOLDER»";
                        result.DetailException = new Exception(result.Message);
                        result.MessageType = MessageType.ERROR;
                    }
                }

                string connectionString = strCnn.Replace("catalog=SIGESPRO;", "catalog=master;");
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand(sqlCommand, connection);
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    result.Message = "Se restauró exitosamente la base de datos";

                }

            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = "Ocurrió un error al restaurar la base de datos";
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<List<Actions>> GetAllActions()
        {
            Result<List<Actions>> result = new Result<List<Actions>>();

            try
            {
                var newRepository = _unitOfWork.GetRepository<Actions>();

                result.Data = newRepository.GetMany(M => M.Active).OrderBy(m => m.Description).ToList();
                result.MessageType = MessageType.INFO;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<List<MenuItems>> GetAllMenuItem()
        {
            Result<List<MenuItems>> result = new Result<List<MenuItems>>();

            try
            {
                var newRepository = _unitOfWork.GetRepository<MenuItems>();

                result.Data = newRepository.GetMany(M => M.Active).OrderBy(m => m.Label).ToList();
                result.MessageType = MessageType.INFO;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<Systems> GetSystemById(int systemId)
        {
            Result<Systems> result = new Result<Systems>();

            try
            {
                result.Data = Repository.GetById(systemId);

                result.Message = Messages.SuccessGetResult;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<List<vwMenuItems>> GetMenuItemByRole(int roleID)
        {
            Result<List<vwMenuItems>> result = new Result<List<vwMenuItems>>();

            try
            {
                var newRepository = _unitOfWork.GetRepository<vwMenuItems>();

                List<vwMenuItems> menuItems = null;

                menuItems = newRepository.GetMany(o => o.RoleId == roleID).OrderBy(m => m.Label).ToList();

                if (menuItems != null && menuItems.Count > 0)
                {
                    result.Data = menuItems;
                    result.Message = Messages.SuccessGetResult;
                }
                else
                {
                    result.Message = Messages.AuthenticateError;
                    result.MessageType = MessageType.INFO;
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<List<vwActionList>> GetActionListByRole(int roleID)
        {
            Result<List<vwActionList>> result = new Result<List<vwActionList>>();

            try
            {
                var newRepository = _unitOfWork.GetRepository<vwActionList>();

                List<vwActionList> options = new List<vwActionList>();

                options = newRepository.GetMany(o => o.RoleId == roleID).OrderBy(m => m.Description).ToList();

                if (options != null && options.Count > 0)
                {
                    result.Data = options;
                    result.Message = Messages.SuccessGetResult;
                }
                else
                {
                    result.Message = Messages.NoFoundResult;
                    result.MessageType = MessageType.INFO;
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<List<vwMenuItems>> GetMenuItemByUser(string username)
        {
            Result<List<vwMenuItems>> result = new Result<List<vwMenuItems>>();

            try
            {
                var newRepository = _unitOfWork.GetRepository<vwMenuItems>();

                List<vwMenuItems> menuItems = null;

                if (!string.IsNullOrEmpty(username))
                    menuItems = newRepository.GetMany(o => o.UserName.ToLower().Trim().Equals(username.ToLower().Trim())).ToList();

                if (menuItems != null && menuItems.Count > 0)
                {
                    result.Data = menuItems;
                    result.Message = Messages.SuccessGetResult;
                }
                else
                {
                    result.Message = Messages.AuthenticateError;
                    result.MessageType = MessageType.INFO;
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<List<vwOptionsList>> GetOptionListByUser(string username, string action = "", string controller = "", string url = "")
        {
            Result<List<vwOptionsList>> result = new Result<List<vwOptionsList>>();

            try
            {
                var newRepository = _unitOfWork.GetRepository<vwOptionsList>();

                List<vwOptionsList> options = new List<vwOptionsList>();

                if (!string.IsNullOrEmpty(url) && (string.IsNullOrEmpty(action) && string.IsNullOrEmpty(controller)))
                    options = newRepository.GetMany(o => o.Url.CompareWith(url) && o.UserName.CompareWith(username)).ToList();
                else
                    options = newRepository.GetMany(o => o.Action == action && o.Controller == controller && o.UserName == username).ToList();

                if (options != null && options.Count > 0)
                {
                    result.Data = options;
                    result.Message = Messages.SuccessGetResult;
                }
                else
                {
                    result.Data = new List<vwOptionsList>();
                    result.Message = "El usuario " + username + " no tiene acceso a ninguna opción";
                    result.MessageType = MessageType.INFO;
                    result.DetailException = new Exception(result.Message);
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResultSet;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }


    }
}

//public Result<List<vwMenuItems>> ValidateActionAccess(string username, string action, string controller)
//{
//    Result<List<vwMenuItems>> result = new Result<List<vwMenuItems>>();

//    try
//    {
//        var newRepository = _unitOfWork.GetRepository<Actions>();
//        var actionRoleRepository = _unitOfWork.GetRepository<role>();

//        List<vwMenuItems> menuItems = null;

//        if (!string.IsNullOrEmpty(username))
//            menuItems = newRepository.GetMany(o => o.Action.ToLower().Trim().Equals(action.ToLower().Trim()) &&
//                                                   o.Controller.ToLower().Trim().Equals(controller.ToLower().Trim()))
//                                     .Join()
//                                                   .ToList();

//        if (menuItems != null && menuItems.Count > 0)
//        {
//            result.Data = menuItems;
//            result.Message = Messages.SuccessGetResult;
//        }
//        else
//        {
//            result.Message = Messages.AuthenticateError;
//            result.MessageType = MessageType.INFO;
//        }
//    }
//    catch (Exception ex)
//    {
//        result.DetailException = ex;
//        result.Message = Messages.ErrorGetResultSet;
//        result.MessageType = MessageType.ERROR;
//    }

//    return result;
//}
