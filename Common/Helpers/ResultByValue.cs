﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Helpers
{
    public class ResultByValue<T> : ResultBase where T : struct
    {
        public ResultByValue() : base()
        {
        }

        public T Data { get; set; }

    }
}
