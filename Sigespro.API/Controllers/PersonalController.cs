﻿using Common.Extensions;
using Common.Log;
using Common.Helpers;
using Sigespro.Entity;
using Sigespro.Entity.Custom;
using Sigespro.Entity.Resource;
using Sigespro.Service;
using Sigespro.Service.Pro;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace Sigespro.API.Controllers
{
    [RoutePrefix("Api/Personal")]
    public class PersonalController : ApiController
    {
        PersonalService _service;

        public PersonalController(Func<string, IService> serviceFactory)
        {
            _service = (PersonalService)serviceFactory("Personal");
        }

        [HttpGet]
        [Route("")]
        public IEnumerable<object> Get()
        {
            var result = _service.GetPersonal();

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data;
        }

        [HttpGet]
        [Route("{PersonalId}")]
        public DetPersonal Get(int PersonalId)
        {
            var result = _service.GetPersonalById(PersonalId);

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data;
        }

        [HttpGet]
        [Route("Filter/{Filter}")]
        public IEnumerable<Program> GetPersonal(string Filter)
        {
            var result = _service.GetPersonal(Filter);

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data;
        }

        [HttpGet]
        [Route("ByPrograms")]
        [Route("ByPrograms/{ProgramsId}")]
        public IEnumerable<Program> GetPersonalByPrograms(string ProgramsId = "")
        {
            if (!string.IsNullOrEmpty(ProgramsId))
            {
                int[] arrInt = Array.ConvertAll(ProgramsId.Split(','), int.Parse);

                var result = _service.GetPersonalByPrograms(arrInt);

                if (result.MessageType != MessageType.SUCCESS)
                {
                    var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                    {
                        Content = new StringContent(result.Message)
                    };

                    Logger.Log(result.MessageType, result.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());

                    throw new HttpResponseException(resp);
                }

                return result.Data;
            }
            else
                return new List<Program>();
        }

        [HttpPost]
        [Route("List")]
        public HttpResponseMessage ListPersonal(QueryOptions<IEnumerable<vwListadoPersonal>> filter)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            _service.GetPersonalForList(ref filter);

            if (filter.ResultData.MessageType == MessageType.INFO)
                response = Request.CreateResponse(HttpStatusCode.OK, filter, "application/json");
            else
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, filter.ResultData.Message);
                Logger.Log(filter.ResultData.MessageType, filter.ResultData.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());
            }

            return response;
        }

        [HttpPost]
        [Route("")]
        public HttpResponseMessage Post(DetPersonal personal)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            if (ModelState.IsValid)
            {
                ResultBase result = new ResultBase();

                try
                {
                    _service.InsertPersonal(personal);

                    if (result.MessageType == MessageType.SUCCESS)
                    {
                        response = Request.CreateResponse(HttpStatusCode.Created, personal);

                        //string uri = Url.Link("DefaultApi", new { id = personal.IdPersonal });

                        //response.Headers.Location = new Uri(uri);
                    }
                    else
                    {
                        if (result.MessageType == MessageType.WARNING)
                            response = Request.CreateErrorResponse(HttpStatusCode.Conflict, result.Message);

                        else if (result.MessageType == MessageType.ERROR)
                            response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, result.Message);

                        throw result.DetailException;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log(result.MessageType, ex, "Yo xD", GetType().Name, Request.GetClientIpAddress());
                }
            }
            else
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.RequiredData);

            return response;
        }

        [HttpPut]
        [Route("{PersonalId}")]
        public HttpResponseMessage Put(int PersonalId, [FromBody]DetPersonal personal)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            if (!ModelState.IsValid)
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.RequiredData);
            else
            {
                ResultBase result = new ResultBase();

                try
                {
                    if (PersonalId != personal.IdPersonal)
                        response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "La información no coincide con el identificador proporcionado.");
                    else
                    {
                        _service.UpdatePersonal(personal);

                        if (result.MessageType == MessageType.SUCCESS)
                            response = Request.CreateResponse(HttpStatusCode.OK);
                        else
                        {
                            if (result.MessageType == MessageType.WARNING)
                                response = Request.CreateErrorResponse(HttpStatusCode.NotFound, result.Message);

                            else if (result.MessageType == MessageType.ERROR)
                                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, result.Message);

                            throw result.DetailException;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log(result.MessageType, ex, "Yo xD", GetType().Name, Request.GetClientIpAddress());
                }
            }

            return response;
        }
    }
}
