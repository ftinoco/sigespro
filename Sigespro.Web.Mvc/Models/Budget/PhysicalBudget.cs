﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sigespro.Web.Mvc.Models.Budget
{
    public class PhysicalBudget: BudgetModel
    {
        public string ArticleID { get; set; }

        public string ArticleName { get; set; }

        public decimal Quantity { get; set; }

        public decimal Cost { get; set; }
    }
}