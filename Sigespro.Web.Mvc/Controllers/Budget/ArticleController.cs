﻿using Common.Helpers;
using Common.Log;
using Newtonsoft.Json;
using Sigespro.Entity;
using Sigespro.Service.Admin;
using Sigespro.Service.Pre;
using Sigespro.Web.Mvc.Models.Budget;
using Sigespro.Web.Mvc.Resources;
using Sigespro.Web.Mvc.Utilities;
using Sigespro.Web.Mvc.Utilities.Attributes.Security;
using Sigespro.Web.Mvc.Utilities.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Sigespro.Web.Mvc.Controllers.Budget
{
    [RoutePrefix("Budget/Articles")] 
    public class ArticleController : Controller
    {
        private readonly IUserSession _userSession;

        public ArticleService Service { get; set; }
         
        public ArticleController()
        {
            Service = new ArticleService();
            _userSession = new UserSession();
        }

        [Route]
        public ActionResult Index(QueryOptions<IEnumerable<ArticlesViewModel>> queryOpt)
        {
            string message = string.Empty;
            IEnumerable<ArticlesViewModel> model = new List<ArticlesViewModel>();

            try
            {
                string strSerialize = JsonConvert.SerializeObject(queryOpt);

                QueryOptions<IEnumerable<vwArticulos>> filter = JsonConvert.DeserializeObject<QueryOptions<IEnumerable<vwArticulos>>>(strSerialize);
                Service.GetArticlesForList(ref filter);

                if (filter.ResultData.MessageType != MessageType.INFO)
                {
                    message = filter.ResultData.Message;
                    Logger.Log(filter.ResultData.MessageType, filter.ResultData.DetailException, _userSession.Username,
                               MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                {
                    strSerialize = JsonConvert.SerializeObject(filter);

                    queryOpt = JsonConvert.DeserializeObject<QueryOptions<IEnumerable<ArticlesViewModel>>>(strSerialize);

                    model = queryOpt.ResultData.Data;
                }
            }
            catch (Exception ex)
            {
                message = Messages.LoadPageError;
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            if (!string.IsNullOrEmpty(message))
                ModelState.AddModelError(Messages.AlertDanger, message);

            ViewBag.Filter = true;
            ViewBag.QueryOptions = queryOpt;

            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [Route("Add")]
        public ActionResult AddArticle()
        {
            return AddEditArticle();
        }

        [Route("Edit/{articleId}/{entryId}")]
        public ActionResult EditArticle(string articleId, string entryId)
        {
            return AddEditArticle(articleId, entryId);
        }

        [NonAction]
        private ActionResult AddEditArticle(string articleId = "", string entryId = "")
        {
            string message = string.Empty;

            ArticlesViewModel model = new ArticlesViewModel();
            model.FechaIns = DateTime.Now;

            try
            {
                List<SelectListItem> selectListItem = new List<SelectListItem>();

                // Se obtienen los rubro que sean de tipo "Rubro árticulos"
                EntryService service = new EntryService();
                var result = service.GetEntriesByType(0.ToString(), "RubroArticulo");

                if (result.MessageType != MessageType.INFO)
                    message = result.Message;
                else
                {
                    selectListItem.Add(new SelectListItem { Value = "0", Text = Labels.Select });

                    result.Data.ToList().ForEach(c => selectListItem.Add(
                        new SelectListItem
                        {
                            Value = c.IdRubro.ToString(),
                            Text = c.IdRubro + " - " + c.Nombre
                        }
                    ));
                }

                ViewBag.EntryGroups = new SelectList(selectListItem, "Value", "Text");

                // Se obtienen los monedas
                CurrencyService currencyService = new CurrencyService();
                var resultCurrency = currencyService.GetCurrencies("activo");
                if (resultCurrency.MessageType != MessageType.INFO)
                    message = resultCurrency.Message;
                else
                {
                    selectListItem = new List<SelectListItem>();

                    selectListItem.Add(new SelectListItem { Value = "0", Text = Labels.Select });

                    resultCurrency.Data.ToList().ForEach(c => selectListItem.Add(
                        new SelectListItem
                        {
                            Value = c.IdMoneda.ToString(),
                            Text = c.Nombre
                        }
                    ));
                }

                ViewBag.Currencies = new SelectList(selectListItem, "Value", "Text");

                // Se obtienen las unidades de medidas
                DetailCatalogService dcService = new DetailCatalogService();
                var unitMeasurementResult = dcService.GetDetailsByTable("UnidadMedida");

                if (unitMeasurementResult.MessageType != MessageType.INFO)
                    message = unitMeasurementResult.Message;
                else
                {
                    selectListItem = new List<SelectListItem>();

                    selectListItem.Add(new SelectListItem { Value = "0", Text = Labels.Select });

                    unitMeasurementResult.Data.ToList().ForEach(c => selectListItem.Add(
                        new SelectListItem
                        {
                            Value = c.IdDetCatalogo.ToString(),
                            Text = c.Descripcion
                        }
                    ));
                }

                ViewBag.UnitMeasurements = new SelectList(selectListItem, "Value", "Text");

                if (!string.IsNullOrEmpty(articleId) && !string.IsNullOrEmpty(entryId))
                {
                    var resultModel = Service.GetArticleById(articleId, entryId);

                    if (resultModel.MessageType == MessageType.ERROR)
                    {
                        message = resultModel.Message;
                        Logger.Log(resultModel.MessageType, resultModel.DetailException, _userSession.Username,
                                   MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                    }
                    else
                    {
                        model = new ArticlesViewModel
                        {
                            Activo = resultModel.Data.Activo,
                            Descripcion = resultModel.Data.Descripcion,
                            FechaIns = resultModel.Data.FechaIns,
                            FechaUpd = resultModel.Data.FechaUpd,
                            IdArticulo = resultModel.Data.IdArticulo,
                            IdMoneda = resultModel.Data.IdMoneda,
                            IdRubro = resultModel.Data.IdRubro,
                            IdUnidadMedida = resultModel.Data.IdUnidadMedida,
                            IdUsuarioIns = resultModel.Data.IdUsuarioIns,
                            IdUsuarioUpd = resultModel.Data.IdUsuarioUpd,
                            Precio = resultModel.Data.Precio
                        };
                    }
                }

            }
            catch (Exception ex)
            {
                message = Messages.ErrorApi;
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            if (!string.IsNullOrEmpty(message))
                ModelState.AddModelError(Messages.AlertDanger, message);

            if (string.IsNullOrEmpty(articleId) && string.IsNullOrEmpty(entryId))
            {
                ViewBag.IsEdit = false;
                ViewBag.Action = "SaveArticle";
                ViewBag.Title = "Agregar Artículo";
            }
            else
            {
                ViewBag.IsEdit = true;
                ViewBag.Action = "UpdateArticle";
                ViewBag.Title = "Editar Artículo";
            }

            return View("AddArticle", model);
        }

        [AjaxOnly]
        [HttpPost]
        [Route("Save")]
        [AllowAnonymous]
        public ActionResult SaveArticle(ArticlesViewModel model)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                model.IdUsuarioIns = _userSession.UserId;

                string strSerialize = JsonConvert.SerializeObject(model);

                var result = Service.InsertArticle(JsonConvert.DeserializeObject<CatArticulos>(strSerialize));

                if (result.MessageType != MessageType.SUCCESS)
                {
                    message = result.Message;
                    status = result.MessageType.ToString();
                    Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                    message = string.Format(Messages.SaveSuccess, "artículo");
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username, MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message, action = Url.Action("Index", "Article") },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

        [AjaxOnly]
        [HttpPost]
        [AllowAnonymous]
        [Route("Update")]
        public ActionResult UpdateArticle(ArticlesViewModel model)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                model.IdUsuarioUpd = _userSession.UserId;
                string strSerialize = JsonConvert.SerializeObject(model);

                var result = Service.UpdateArticle(JsonConvert.DeserializeObject<CatArticulos>(strSerialize));

                if (result.MessageType != MessageType.SUCCESS)
                {
                    message = result.Message;
                    status = result.MessageType.ToString();
                    Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                    message = string.Format(Messages.UpdateSuccess, "artículo");
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                             MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message, action = Url.Action("Index", "Article") },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }
}