//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sigespro.Entity
{
    using System;
    using System.Collections.Generic;
     
    public partial class DetContactoInstitucion
    {
        public int IdContactoInstitucion { get; set; }
        public int IdPersona { get; set; }
        public int IdInstitucion { get; set; }
    
        public virtual MstPersonas MstPersonas { get; set; }
        public virtual MstInstituciones MstInstituciones { get; set; }
    }
}
