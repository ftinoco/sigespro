﻿using Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Helpers
{
    public class ResultBase
    {
        public ResultBase()
        {
            Message = "Success";
            DetailException = null;
            MessageType = MessageType.SUCCESS;
        }

        public string Message { get; set; }

        public Exception DetailException { get; set; }

        public MessageType MessageType { get; set; }
    }
}
