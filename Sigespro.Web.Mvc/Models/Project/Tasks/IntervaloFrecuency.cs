﻿using Sigespro.Web.Mvc.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sigespro.Web.Mvc.Models.Project.Tasks
{
    public class IntervaloFrecuency
    {
        public int IdIntervaloFrecuencia { get; set; }

        public int? IdConfiguracionFrecuencia { get; set; }

        [Display(Name = "StartDate", ResourceType = typeof(Labels))]
        public DateTime FechaInicio { get; set; }

        public int TipoFinalizacion { get; set; }

        public int? CantidadRepeticiones { get; set; }

        public DateTime? FechaFin { get; set; }
    }
}