﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sigespro.Web.Mvc.Models
{
    public class MenuItemViewModel
    {
        public string MenuName { get; set; }

        public int MenuItemId { get; set; }

        public int MenuId { get; set; }

        public int? ItemParentId { get; set; }

        public int? ActionId { get; set; }

        public string Action { get; set; }

        public string Controller { get; set; }

        public string Url { get; set; }

        public string Label { get; set; }

        public string Icon { get; set; }
        
        public string CssClass { get; set; }

        public int UserId { get; set; }

        public int SystemId { get; set; }

        public int RoleId { get; set; }

        public string UserName { get; set; }
    }
}