﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sigespro.Web.Mvc.Models.Project.Tasks
{
    public class ListOverdueTaskViewModel
    {
        public int IdTarea { get; set; }

        public int IdEntregable { get; set; }

        public string Descripcion { get; set; }

        public int IdProyecto { get; set; }

        public string Nombre { get; set; }

        public string NombrePersona { get; set; }

        public System.DateTime FechaInicio { get; set; }

        public System.DateTime FechaFin { get; set; }

        public int DiasRetrasados { get; set; }
    }
}