//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sigespro.Entity
{
    using System;
    using System.Collections.Generic;
     
    public partial class MstObjetivos
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MstObjetivos()
        {
            this.DetObjetivoPreguntas = new HashSet<DetObjetivoPreguntas>();
            this.DetProyectoObjetivos = new HashSet<DetProyectoObjetivos>();
        }
    
        public int IdObjetivo { get; set; }
        public string TipoObjetivo { get; set; }
        public string Descripcion { get; set; }
        public bool Activo { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetObjetivoPreguntas> DetObjetivoPreguntas { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetProyectoObjetivos> DetProyectoObjetivos { get; set; }
    }
}
