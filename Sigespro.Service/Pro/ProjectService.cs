﻿using Common.Extensions;
using Common.Helpers;
using Sigespro.DAL.Implementations;
using Sigespro.Entity;
using Sigespro.Entity.Resource;
using Sigespro.Entity.Custom;
using Sigespro.Service.Admin;
using Sigespro.Service.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using static Common.Helpers.Utilities;

namespace Sigespro.Service.Pro
{
    public class ProjectService : Service
    {
        private Repository<MstProyectos> Repository { get; set; }

        public ProjectService() : base()
        {
            Repository = _unitOfWork.GetRepository<MstProyectos>();
        }

        //public Result<IEnumerable<Project>> GetProjects()
        //{
        //    Result<IEnumerable<Project>> resultado = new Result<IEnumerable<Project>>();

        //    try
        //    {
        //        var repProjectType = _unitOfWork.GetRepository<DetCatalogo>();
        //        var repProjectStatus = _unitOfWork.GetRepository<DetCatalogo>();

        //        resultado.Data = repProjectType.GetAll()
        //                        .Join(Repository.GetAll(), type => type.IdDetCatalogo, proj => proj.IdTipoProyecto,
        //                                (type, proj) => new { type, proj })
        //                        .Join(repProjectStatus.GetAll(), p => p.proj.IdEstado, s => s.IdDetCatalogo,
        //                                (p, s) => new { p, s })
        //                        .Select((x) => new Project
        //                        {
        //                            Estado = x.s,
        //                            TipoProyecto = x.p.type,
        //                            Externo = x.p.proj.Externo,
        //                            IdProyecto = x.p.proj.IdProyecto,
        //                            FechaInicio = x.p.proj.FechaInicio,
        //                            FechaLimite = x.p.proj.FechaLimite,
        //                            Presupuesto = x.p.proj.Presupuesto,
        //                            DescProyecto = x.p.proj.DescProyecto,
        //                            Justificacion = x.p.proj.Justificacion,
        //                            GastoEstimado = x.p.proj.GastoEstimado,
        //                            TituloProyecto = x.p.proj.TituloProyecto,
        //                            HorasDedicadas = x.p.proj.HorasDedicadas,
        //                            FechaFinEstimada = x.p.proj.FechaFinEstimada,
        //                            PresupuestoActual = x.p.proj.PresupuestoActual,
        //                            DuraciónEstimadaDias = x.p.proj.DuracionEstimadaDias,
        //                            DuracionEstimadaHoras = x.p.proj.DuracionEstimadaHoras,
        //                            DetProyectoBeneficiarios = x.p.proj.DetProyectoBeneficiarios,
        //                            Beneficiario = (x.p.proj.DetProyectoBeneficiarios.IdBeneficiario == null ? x.p.proj.DetProyectoBeneficiarios.Descripcion : x.p.proj.DetProyectoBeneficiarios.MstBeneficiarios.Nombre)
        //                        }).OrderByDescending(z => z.FechaInicio);

        //        resultado.Message = Messages.SuccessGetResultSet;
        //    }
        //    catch (Exception ex)
        //    {
        //        resultado.DetailException = ex;
        //        resultado.Message = Messages.ErrorGetResultSet;
        //        resultado.MessageType = MessageType.ERROR;
        //    }

        //    return resultado;
        //}

        public Result<List<MstProyectos>> GetProjectsPlanned()
        {
            Result<List<MstProyectos>> result = new Result<List<MstProyectos>>();

            try
            {
                result.MessageType = MessageType.INFO;

                var dcRepository = _unitOfWork.GetRepository<DetCatalogo>();
                var detailCatalog = dcRepository.Get(p => p.Valor == Catalog.ProjectStatusPlanned);

                result.Data = Repository.GetMany(p => p.IdEstado == detailCatalog.IdDetCatalogo).ToList();
                result.Message = Messages.SuccessGetResult;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorGetResult, "proyecto");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<MstProyectos> GetProjectById(int idProject)
        {
            Result<MstProyectos> result = new Result<MstProyectos>();

            try
            {
                result.MessageType = MessageType.INFO;
                result.Data = Repository.GetById(idProject);
                result.Message = Messages.SuccessGetResult;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorGetResult, "proyecto");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<vwProyectos> GetProjectByIdForReport(int projectId)
        {
            Result<vwProyectos> result = new Result<vwProyectos>();

            try
            {
                var repository = _unitOfWork.GetRepository<vwProyectos>();

                result.MessageType = MessageType.INFO;

                result.Data = repository.Get(p => p.IdProyecto == projectId);

                result.Message = Messages.SuccessGetResult;
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        public Result<List<Project>> GetProjectsByPersonalID(int personalID)
        {
            Result<List<Project>> result = new Result<List<Project>>();

            try
            {
                var repProjectStatus = _unitOfWork.GetRepository<DetCatalogo>();
                var repDetTask = _unitOfWork.GetRepository<DetTareaPersonal>();

                result.Data = Repository.GetAll()
                              .Join(repDetTask.GetAll(), p => p.IdProyecto, dtp => dtp.IdProyecto,
                                        (p, dtp) => new { p, dtp })
                              .Join(repProjectStatus.GetAll(), x => x.p.IdEstado, dc => dc.IdDetCatalogo,
                                        (x, dc) => new { x, dc })
                              .Where(y => y.x.dtp.IdPersonal == personalID && y.dc.Valor == Catalog.ProjectStatusInAction)
                              .Select(z => new Project
                              {
                                  Externo = z.x.p.Externo,
                                  IdProyecto = z.x.p.IdProyecto,
                                  FechaInicio = z.x.p.FechaInicio,
                                  FechaLimite = z.x.p.FechaLimite,
                                  Presupuesto = z.x.p.Presupuesto,
                                  DescProyecto = z.x.p.DescProyecto,
                                  Justificacion = z.x.p.Justificacion,
                                  GastoEstimado = z.x.p.GastoEstimado,
                                  TituloProyecto = z.x.p.TituloProyecto,
                                  HorasDedicadas = z.x.p.HorasDedicadas,
                                  FechaFinEstimada = z.x.p.FechaFinEstimada,
                                  PresupuestoActual = z.x.p.PresupuestoActual,
                                  DuraciónEstimadaDias = z.x.p.DuracionEstimadaDias,
                                  DuracionEstimadaHoras = z.x.p.DuracionEstimadaHoras
                              }).OrderByDescending(w => w.IdProyecto).ToList();


                if (result.Data == null || (result.Data != null && result.Data.Count == 0))
                {
                    result.MessageType = MessageType.WARNING;
                    result.Message = Messages.NoFoundResult;
                }
                else
                {
                    result.MessageType = MessageType.INFO;
                    result.Message = Messages.SuccessGetResult;
                }
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorGetResultSet, "proyecto");
                result.MessageType = MessageType.ERROR;
                result.DetailException = ex;
            }

            return result;
        }

        public Result<IEnumerable<Project>> GetProjects(string filter)
        {
            Result<IEnumerable<Project>> resultado = new Result<IEnumerable<Project>>();

            try
            {
                var repProjectType = _unitOfWork.GetRepository<DetCatalogo>();
                var repProjectStatus = _unitOfWork.GetRepository<DetCatalogo>();

                resultado.Data = repProjectType.GetAll()
                                .Join(Repository.GetAll(), type => type.IdDetCatalogo, proj => proj.IdTipoProyecto,
                                        (type, proj) => new { type, proj })
                                .Join(repProjectStatus.GetAll(), p => p.proj.IdEstado, s => s.IdDetCatalogo,
                                        (p, s) => new { p, s })
                                .Where(p => p.p.proj.DescProyecto.Contains(filter) ||
                                            p.p.proj.DetProyectoBeneficiarios.MstBeneficiarios.Nombre.Contains(filter) ||
                                            p.p.type.Descripcion.Contains(filter) ||
                                            p.s.Descripcion.Contains(filter))
                                .Select((x) => new Project
                                {
                                    Estado = x.s,
                                    TipoProyecto = x.p.type,
                                    Externo = x.p.proj.Externo,
                                    IdProyecto = x.p.proj.IdProyecto,
                                    FechaInicio = x.p.proj.FechaInicio,
                                    FechaLimite = x.p.proj.FechaLimite,
                                    Presupuesto = x.p.proj.Presupuesto,
                                    DescProyecto = x.p.proj.DescProyecto,
                                    Justificacion = x.p.proj.Justificacion,
                                    GastoEstimado = x.p.proj.GastoEstimado,
                                    TituloProyecto = x.p.proj.TituloProyecto,
                                    HorasDedicadas = x.p.proj.HorasDedicadas,
                                    FechaFinEstimada = x.p.proj.FechaFinEstimada,
                                    PresupuestoActual = x.p.proj.PresupuestoActual,
                                    DuraciónEstimadaDias = x.p.proj.DuracionEstimadaDias,
                                    DuracionEstimadaHoras = x.p.proj.DuracionEstimadaHoras,
                                    DetProyectoBeneficiarios = x.p.proj.DetProyectoBeneficiarios,
                                    Beneficiario = (x.p.proj.DetProyectoBeneficiarios.IdBeneficiario == null ? x.p.proj.DetProyectoBeneficiarios.Descripcion :
                                                                                                               x.p.proj.DetProyectoBeneficiarios.MstBeneficiarios.Nombre
                                                    )
                                }).OrderByDescending(z => z.FechaInicio);

                resultado.Message = Messages.SuccessGetResultSet;
            }
            catch (Exception ex)
            {
                resultado.DetailException = ex;
                resultado.Message = Messages.ErrorGetResultSet;
                resultado.MessageType = MessageType.ERROR;
            }

            return resultado;
        }

        public void GetProjectsForList(ref QueryOptions<IEnumerable<vwProyectos>> queryOpt)
        {
            Result<IEnumerable<vwProyectos>> result = new Result<IEnumerable<vwProyectos>>();

            try
            {
                string orderBy = "FechaInicio";

                var predicate = PredicateBuilder.True<vwProyectos>();

                var filter = queryOpt.Filters.FirstOrDefault(f => f.Name.Equals("Search"));

                if (!IsNull(filter) && !string.IsNullOrEmpty(filter.Value))
                {
                    int intFilter;

                    if (int.TryParse(filter.Value, out intFilter))
                        predicate = predicate.And(p => p.IdProyecto == intFilter);

                    // AGREGAR VALIDACIONES DE FILTROS
                    predicate = predicate.And(p => p.DescProyecto.ToLower().Trim().Contains(filter.Value.ToLower().Trim()) ||
                                                   p.TituloProyecto.ToLower().Trim().Contains(filter.Value.ToLower().Trim()) ||
                                                   p.Justificacion.ToLower().Trim().Contains(filter.Value.ToLower().Trim()) ||
                                                   p.Estado.ToLower().Trim().Contains(filter.Value.ToLower().Trim()) ||
                                                   p.TipoProyecto.ToLower().Trim().Contains(filter.Value.ToLower().Trim()));
                }
                else
                    queryOpt.TotalRecords = _unitOfWork.GetRepository<vwProyectos>().Count();

                if (!string.IsNullOrEmpty(queryOpt.SortField))
                    orderBy = queryOpt.SortField;

                result.Data = _unitOfWork.GetRepository<vwProyectos>().GetManyOnDemand(predicate, orderBy, queryOpt.CurrentPageIndex, queryOpt.RowsPerPage, queryOpt.SortOrder);

                result.Message = Messages.SuccessGetResultSet;

                result.MessageType = MessageType.INFO;
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorGetResultSet, "proyecto");
                result.MessageType = MessageType.ERROR;
                result.DetailException = ex;
            }

            queryOpt.ResultData = result;
        }

        public ResultBase InsertProject(MstProyectos project)
        {
            ResultBase result = new ResultBase();

            try
            {
                var resultValidate = ValidateProjectData(project);

                if (resultValidate.MessageType == MessageType.ERROR)
                    result = resultValidate;
                else
                {
                    var temp = Repository.Get(p => p.TituloProyecto == project.TituloProyecto && p.IdProyecto != 0);

                    if (IsNull(temp))
                    {
                        project.FechaIns = DateTime.Now;

                        project.PresupuestoActual = project.Presupuesto;
                        project.FechaFinEstimada = project.FechaLimite;

                        // Obteniendo el identificador del nuevo proyecto
                        var sequencesService = new SequenceService(_unitOfWork.GetRepository<MstSecuencias>());
                        MstSecuencias seq = sequencesService.GetNextSequenceByTableName("[pro].[MstProyectos]");

                        project.IdProyecto = seq.SecuencialTabla;

                        sequencesService.InsertSequence(seq);

                        // Se calculan la duración en días y horas del proyecto
                        result = CalculateEstimatedDurationInDaysAndHours(ref project);
                        if (result.MessageType != MessageType.SUCCESS)
                            return result;

                        // Guardando el nuevo registros
                        Repository.Add(project);

                        // Se guarda el grupo meta del proyecto
                        Repository<DetProyectoBeneficiarios> detRepository = _unitOfWork.GetRepository<DetProyectoBeneficiarios>();

                        var beneficiary = project.DetProyectoBeneficiarios;
                        beneficiary.TipoDestinatario = "G";
                        detRepository.Add(beneficiary);


                        GoalService goalService = new GoalService();
                        // Se ingresan los objetivos del projecto
                        result = goalService.InsertUpdateGoals(project.DetProyectoObjetivos, project.IdProyecto, _unitOfWork);
                        if (result.MessageType != MessageType.SUCCESS)
                            return result;

                        // Se ingresan los programas vinculados al proyecto
                        result = InsertProgramProject(project.DetProgramaProyecto, project.IdProyecto);
                        if (result.MessageType != MessageType.SUCCESS)
                            return result;

                        // Se ingresan los responsables del projecto
                        result = InsertManagerProject(project.DetProyectoResponsable, project, project.IdProyecto);
                        if (result.MessageType != MessageType.SUCCESS)
                            return result;

                        _unitOfWork.Commit();
                    }
                    else
                    {
                        result.Message = "El proyecto con el título especificado ya existe.";
                        result.DetailException = new Exception(result.Message);
                        result.MessageType = MessageType.ERROR;
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorSaveData, "proyecto");
                result.MessageType = MessageType.ERROR;

                if (ex.GetType().Name == "DbEntityValidationException")
                    result.DetailException = new Exception(GetEntityValidationErrors(ex as System.Data.Entity.Validation.DbEntityValidationException));
                else
                    result.DetailException = ex;
            }

            return result;
        }

        public ResultBase UpdateProject(MstProyectos project)
        {
            ResultBase result = new ResultBase();

            try
            {
                var resultValidate = ValidateProjectData(project, true);

                if (resultValidate.MessageType == MessageType.ERROR)
                    result = resultValidate;
                else
                {
                    /************** SE OBTIENE LA INFORMACIÓN DEL ESTADO DEL PROYECTO **************/
                    // Obteniendo el identificador de estado de proyecto
                    var parameter = _unitOfWork.GetRepository<CatParametros>().Get(p => p.NombreParametro.Equals("ESTADO_PROYECTO"));
                    int projectStatusCatID = int.Parse(parameter.ValorParametro);

                    DetailCatalogService dtService = new DetailCatalogService();
                    var resultDetailCatalog = dtService.GetDetailCatalogById(project.IdEstado, projectStatusCatID);

                    if (resultDetailCatalog.MessageType != MessageType.INFO)
                        throw resultDetailCatalog.DetailException;

                    var projectStatus = resultDetailCatalog.Data;

                    /************** SE VALIDA QUE EL PROYECTO NO ESTÉ FINALIZADO NI CANCELADO **************/
                    if (projectStatus.Valor == Catalog.ProjectStatusCanceled || projectStatus.Valor == Catalog.ProjectStatusFinalized)
                    {
                        result.MessageType = MessageType.ERROR;
                        result.Message = "No puede modificar un proyecto en estado «Finalizado» o «Cancelado»";
                        result.DetailException = new Exception(result.Message);
                        return result;
                    }

                    /************** SE VALIDA QUE EL USUARIO QUE ESTÉ REALIZANDO LA ACTUALIZACIÓN SEA COORDINADOR **************/
                    // Obteniendo el codigo de rol coordinador
                    parameter = _unitOfWork.GetRepository<CatParametros>().Get(p => p.NombreParametro.Equals("COORDINADOR"));
                    int coordinator = int.Parse(parameter.ValorParametro);

                    // Se obtiene los roles asignados al usuario
                    UserService userServices = new UserService();
                    var userRoles = userServices.GetRolesByUserID(project.IdUsuarioUpd.Value);

                    if (!userRoles.Data.Any(r => r.RoleId == coordinator))
                    {
                        result.MessageType = MessageType.ERROR;
                        result.Message = "Su usuario no cuenta con los permisos suficientes para realizar esta acción";
                        result.DetailException = new Exception("El usuario no tiene el rol de coordinador");
                        return result;
                    }

                    /************** ACTUALIZANDO EL REGISTRO DE PROYECTO **************/
                    // Se obtiene el registro de proyecto por el id
                    var projectFromDB = Repository.GetById(project.IdProyecto);

                    if (!IsNull(projectFromDB))
                    {
                        // Se actualizan los datos generales del proyecto
                        projectFromDB.TituloProyecto = project.TituloProyecto;
                        projectFromDB.IdTipoProyecto = project.IdTipoProyecto;
                        projectFromDB.DescProyecto = project.DescProyecto;
                        projectFromDB.Justificacion = project.Justificacion;

                        // Si el proyecto está pendiente de planificar o en planificación
                        if (projectStatus.Valor == Catalog.ProjectStatusPendingToBePlanned ||
                            projectStatus.Valor == Catalog.ProjectStatusInPlanning)
                        {
                            /*********************************************************************
                             ********* AQUI SE DEBE ACTUALIZAR EN CASCADA LOS MONTOS Y      ******
                             ********* LAS FECHAS DE LA TAREAS Y ENTREGABLES, EN CASO QUE   ******
                             ********* SE ACTUALICE LA MONEDA O EL PRESUPUESTO DEL PROYECTO ******
                             ********* ASI COMO LAS FECHAS DE PROYECTO                      ******
                            /* *******************************************************************/

                            if (project.FechaLimite.CompareTo(project.FechaFinEstimada) == 1)
                                projectFromDB.FechaFinEstimada = project.FechaLimite;

                            // ESTO DEBE CAMBIAR CUANDO SE HAGA LA ACTUALIZACION EN CASCADA 
                            projectFromDB.FechaLimite = project.FechaLimite;
                            projectFromDB.FechaInicio = project.FechaInicio;

                            // ESTO DEBE CAMBIAR CUANDO SE HAGA LA ACTUALIZACION EN CASCADA 
                            projectFromDB.Presupuesto = project.Presupuesto;
                            projectFromDB.PresupuestoActual = project.Presupuesto;
                            projectFromDB.IdMoneda = project.IdMoneda;

                            // Se calculan la duración en días y horas del proyecto
                            result = CalculateEstimatedDurationInDaysAndHours(ref projectFromDB);
                            if (result.MessageType != MessageType.SUCCESS)
                                return result;

                            projectFromDB.FechaUpd = DateTime.Now;
                            projectFromDB.IdUsuarioUpd = project.IdUsuarioUpd;

                            // Se actualiza el registro de proyecto
                            Repository.Update(projectFromDB);


                            // Se actualiza la informacion  el grupo meta del proyecto
                            Repository<DetProyectoBeneficiarios> detRepository = _unitOfWork.GetRepository<DetProyectoBeneficiarios>();

                            var beneficiary = detRepository.Get(b => b.IdProyecto == projectFromDB.IdProyecto);
                            beneficiary.TipoDestinatario = "G";
                            beneficiary.Descripcion = project.DetProyectoBeneficiarios.Descripcion;
                            detRepository.Update(beneficiary);

                            // Se actualizan los objetivos existente y se insertan los nuevos objetivos del projecto
                            GoalService goalService = new GoalService();
                            result = goalService.InsertUpdateGoals(project.DetProyectoObjetivos, projectFromDB.IdProyecto, _unitOfWork);
                            if (result.MessageType != MessageType.SUCCESS)
                                return result;

                            // Se asignan los nuevos programas al proyecto
                            result = InsertProgramProject(project.DetProgramaProyecto, projectFromDB.IdProyecto);
                            if (result.MessageType != MessageType.SUCCESS)
                                return result;

                            // Se asignan los nuevos responsables al proyecto
                            result = InsertManagerProject(project.DetProyectoResponsable, projectFromDB, projectFromDB.IdProyecto);
                            if (result.MessageType != MessageType.SUCCESS)
                                return result;


                            // Se eliminan los objetivos que no se encuentran en el registro de proyecto
                            result = goalService.DeleteGoals(project, _unitOfWork);
                            if (result.MessageType != MessageType.SUCCESS)
                                return result;

                            // Se eliminan los programas que no se encuentran en el registro del proyecto
                            result = DeleteProgramProject(project, _unitOfWork);
                            if (result.MessageType != MessageType.SUCCESS)
                                return result;

                            // Se eliminan los responsables que no se encuentran en el registro del proyecto
                            result = DeleteManagerProject(project, _unitOfWork);
                            if (result.MessageType != MessageType.SUCCESS)
                                return result;

                            _unitOfWork.Commit();
                        }
                    }
                    else
                    {
                        result.Message = string.Format(Messages.NotExist, "proyecto");
                        result.DetailException = new Exception("No se encontró registro de proyecto con el id: " + project.IdProyecto);
                        result.MessageType = MessageType.ERROR;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorUpdateData, "proyecto");
                result.MessageType = MessageType.ERROR;

                if (ex.GetType().Name == "DbEntityValidationException")
                    result.DetailException = new Exception(GetEntityValidationErrors(ex as System.Data.Entity.Validation.DbEntityValidationException));
                else
                    result.DetailException = ex;
            }

            return result;
        }

        public ResultBase UpdateProjectStatus(string status, MstProyectos project)
        {
            ResultBase result = new ResultBase();

            try
            {
                if (project != null)
                {
                    if (!string.IsNullOrWhiteSpace(status))
                    {
                        result = UpdateProjectStatus(status, project, _unitOfWork);

                        if (result.MessageType == MessageType.SUCCESS)
                            _unitOfWork.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorSaveData, "fase de proyecto");
                result.MessageType = MessageType.ERROR;

                if (ex.GetType().Name == "DbEntityValidationException")
                    result.DetailException = new Exception(GetEntityValidationErrors(ex as System.Data.Entity.Validation.DbEntityValidationException));
                else
                    result.DetailException = ex;
            }

            return result;
        }

        internal ResultBase UpdateProjectStatus(string status, MstProyectos project, UnitOfWork unitOfWork)
        {
            ResultBase result = new ResultBase();

            try
            {
                if (IsNull(project))
                {
                    result.MessageType = MessageType.WARNING;
                    result.Message = "Se debe proporcionar la información del proyecto";
                    result.DetailException = new Exception(result.Message);
                }
                else
                {
                    // Se obtiene la información del estado actual del proyecto
                    var dcRepository = unitOfWork.GetRepository<DetCatalogo>();
                    var detailCatalog = dcRepository.Get(p => p.IdDetCatalogo == project.IdEstado);

                    if (!IsNull(detailCatalog))
                    {
                        switch (status)
                        {
                            case "Pendiente de Planificar":
                                if (detailCatalog.Valor != Catalog.ProjectStatusInPlanning)
                                {
                                    result.MessageType = MessageType.WARNING;
                                    result.Message = "El proyecto debe estar en planificación para poder regresar al estado pendiente de planificar";
                                    result.DetailException = new Exception(result.Message);
                                    return result;
                                }
                                break;
                            case "En Planificación":
                                if (detailCatalog.Valor != Catalog.ProjectStatusPendingToBePlanned)
                                {
                                    result.MessageType = MessageType.WARNING;
                                    result.Message = "El proyecto debe estar pendiente de planificar para poderlo planificar";
                                    result.DetailException = new Exception(result.Message);
                                    return result;
                                }
                                break;
                            case "Planificado":
                                if (detailCatalog.Valor != Catalog.ProjectStatusInPlanning)
                                {
                                    result.MessageType = MessageType.WARNING;
                                    result.Message = "El proyecto debe estar en planificación para poder darlo como planificado";
                                    result.DetailException = new Exception(result.Message);
                                    return result;
                                }
                                break;
                            //case "Finalizado":
                            //    break;
                            case "En Ejecución":
                                if (detailCatalog.Valor != Catalog.ProjectStatusPlanned)
                                {
                                    result.MessageType = MessageType.WARNING;
                                    result.Message = "El proyecto debe estar planificado para poder ejecutarlo";
                                    result.DetailException = new Exception(result.Message);
                                    return result;
                                }
                                break;
                            default:
                                result.MessageType = MessageType.WARNING;
                                result.Message = "El estado proporcionado no es un estado válido de proyecto";
                                result.DetailException = new Exception(result.Message);
                                return result;
                        }

                        var newStatus = dcRepository.Get(p => p.Valor == status);

                        project.IdEstado = newStatus.IdDetCatalogo;
                    }
                    else
                    {
                        result.MessageType = MessageType.WARNING;
                        result.Message = "No se encontró registro de estado";
                        result.DetailException = new Exception(result.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorUpdateData, "proyecto");
                result.MessageType = MessageType.ERROR;

                if (ex.GetType().Name == "DbEntityValidationException")
                    result.DetailException = new Exception(GetEntityValidationErrors(ex as System.Data.Entity.Validation.DbEntityValidationException));
                else
                    result.DetailException = ex;
            }

            return result;
        }

        public ResultBase ApprovePlanningProject(int projectID)
        {
            ResultBase result = new ResultBase();

            try
            {
                if (projectID > 0)
                {
                    //var projectRep = _unitOfWork.GetRepository<MstProyectos>(); 
                    MstProyectos project = Repository.GetById(projectID);

                    if (project != null)
                    {
                        // Se obtiene el detalle catálogo del estado del proyecto para verificarlo
                        var dcRepository = _unitOfWork.GetRepository<DetCatalogo>();
                        var detailCatalog = dcRepository.Get(p => p.IdDetCatalogo == project.IdEstado);

                        if (detailCatalog.Valor != Catalog.ProjectStatusInPlanning)
                        {
                            result.Message = "El proyecto debe estar en planificación para poder realizar esta acción";
                            result.DetailException = new Exception(result.Message);
                            result.MessageType = MessageType.ERROR;
                            return result;
                        }

                        // Se establece el estado en planificado
                        var resultProjectStatus = UpdateProjectStatus(Catalog.ProjectStatusPlanned, project, _unitOfWork);

                        if (resultProjectStatus.MessageType == MessageType.SUCCESS)
                            _unitOfWork.Commit();

                        return resultProjectStatus;
                    }
                    else
                    {
                        result.Message = "No se encontró registro de proyecto con identificador indicado";
                        result.DetailException = new Exception(result.Message);
                        result.MessageType = MessageType.ERROR;
                        return result;
                    }
                }
                else
                {
                    result.Message = "Se debe proporcionar el identificador del proyecto";
                    result.DetailException = new Exception(result.Message);
                    result.MessageType = MessageType.ERROR;
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.Message = string.Format(Messages.ErrorUpdateData, "proyecto");
                result.MessageType = MessageType.ERROR;

                if (ex.GetType().Name == "DbEntityValidationException")
                    result.DetailException = new Exception(GetEntityValidationErrors(ex as System.Data.Entity.Validation.DbEntityValidationException));
                else
                    result.DetailException = ex;
            }

            return result;
        }

        public Result<Dictionary<DayOfWeek, string>> GetWorkingDaysPerWeek(int projectID, ref int countMaxDays)
        {
            Result<Dictionary<DayOfWeek, string>> result = new Result<Dictionary<DayOfWeek, string>>();

            try
            {
                //var projectRep = _unitOfWork.GetRepository<MstProyectos>();
                var project = Repository.GetById(projectID);

                // Se obtiene las jornada laborales
                var detailCatalogService = new DetailCatalogService();

                var resultDetailCatalog = detailCatalogService.GetDetailsByTable("JornadaLaboralSemanal");

                if (resultDetailCatalog.MessageType != MessageType.INFO)
                    throw resultDetailCatalog.DetailException;

                var workday = resultDetailCatalog.Data.FirstOrDefault(w => w.IdDetCatalogo == project.DiasLabSem);

                int strHorarioId = int.Parse(workday.Valor);

                // Se obtienen los días laborales que el horario posee 
                var workdayCount = _unitOfWork.GetRepository<DetHorario>().GetMany(s => s.IdHorario == strHorarioId);

                // Se cargan en un listado de dias de la semana
                Dictionary<DayOfWeek, string> lstDaysOfWeek = new Dictionary<DayOfWeek, string>();

                countMaxDays = workdayCount.Count();

                foreach (var day in workdayCount)
                {
                    switch (((DayOfWeek)day.Dia))
                    {
                        case DayOfWeek.Monday:
                            lstDaysOfWeek.Add(DayOfWeek.Monday, "Lunes");
                            break;
                        case DayOfWeek.Tuesday:
                            lstDaysOfWeek.Add(DayOfWeek.Tuesday, "Martes");
                            break;
                        case DayOfWeek.Wednesday:
                            lstDaysOfWeek.Add(DayOfWeek.Wednesday, "Miércoles");
                            break;
                        case DayOfWeek.Thursday:
                            lstDaysOfWeek.Add(DayOfWeek.Thursday, "Jueves");
                            break;
                        case DayOfWeek.Friday:
                            lstDaysOfWeek.Add(DayOfWeek.Friday, "Viernes");
                            break;
                        case DayOfWeek.Saturday:
                            lstDaysOfWeek.Add(DayOfWeek.Saturday, "Sábado");
                            break;
                        case DayOfWeek.Sunday:
                            lstDaysOfWeek.Add(DayOfWeek.Sunday, "Domingo");
                            break;
                    }
                    result.MessageType = MessageType.INFO;
                    result.Data = lstDaysOfWeek;
                }
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = Messages.ErrorGetResult;
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        #region Otros Métodos

        //private ResultBase InsertUpdateGoals(ICollection<DetProyectoObjetivos> goals)
        //{
        //    ResultBase result = new ResultBase();

        //    try
        //    {
        //        GoalService goalService = new GoalService(_unitOfWork);
        //        Repository<DetProyectoObjetivos> detRepository = _unitOfWork.GetRepository<DetProyectoObjetivos>();

        //        int generalGoals = goals.Count(g => g.MstObjetivos.TipoObjetivo.ToUpper().Equals("G"));

        //        // Se valida a cantidad e objetivos generales
        //        if (generalGoals > 1)
        //        {
        //            result.Message = "Solo se puede asignar un objetivo general al proyecto.";
        //            result.DetailException = new Exception(result.Message);
        //            result.MessageType = MessageType.ERROR;
        //            return result;
        //        }

        //        // Para cada objetivo
        //        foreach (DetProyectoObjetivos detGoal in goals)
        //        {
        //            if (detGoal.MstObjetivos == null)
        //            {
        //                result.Message = "Debe proporcionar los datos requeridos para ingresar los objetivos del proyecto.";
        //                result.DetailException = new Exception(result.Message);
        //                result.MessageType = MessageType.ERROR;
        //                return result;
        //            }
        //            else
        //            {
        //                goalService.CommitExternal = true;

        //                // Si el objetivo ya existe se actualiza
        //                if (detGoal.MstObjetivos.IdObjetivo > 0)
        //                {
        //                    result = goalService.UpdateGoal(detGoal.MstObjetivos);

        //                    if (result.MessageType != MessageType.SUCCESS)
        //                        throw result.DetailException;
        //                }
        //                else // De lo contrario se ingresa
        //                {
        //                    result = goalService.InsertGoal(detGoal.MstObjetivos);

        //                    if (result.MessageType != MessageType.SUCCESS)
        //                        throw result.DetailException;

        //                    var goalFromList = goals.Where(g => g.MstObjetivos.Descripcion == detGoal.MstObjetivos.Descripcion);

        //                    if (goalFromList.Count() == 1)
        //                    {
        //                        detGoal.IdObjetivo = detGoal.MstObjetivos.IdObjetivo;
        //                        detGoal.IdProyecto = detGoal.MstProyectos.IdProyecto;

        //                        var sequencesService = new SequenceService(_unitOfWork.GetRepository<MstSecuencias>());
        //                        MstSecuencias seq = sequencesService.GetNextSequenceByTableName("[pro].[DetProyectoObjetivos]");

        //                        detGoal.IdProyectoObjetivo = seq.SecuencialTabla;

        //                        sequencesService.InsertSequence(seq);

        //                        detRepository.Add(detGoal);
        //                    }
        //                    else
        //                    {
        //                        result.Message = "No se pueden guardar dos objetivos con la misma descripción.";
        //                        result.DetailException = new Exception(result.Message);
        //                        result.MessageType = MessageType.ERROR;
        //                        return result;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        result.DetailException = ex;
        //        result.Message = string.Format(Messages.ErrorSaveData, "proyecto");
        //        result.MessageType = MessageType.ERROR;
        //    }

        //    return result;
        //}

        private void InsertUpdateBeneficiary(DetProyectoBeneficiarios beneficiary)
        {
            //try
            //{
            //    Repository<DetProyectoBeneficiarios> detRepository = UnitOfWork.GetRepository<DetProyectoBeneficiarios>();

            //    // Se valida que el proyecto sea interno para establecerlo como beneficiario
            //    if (beneficiary.MstProyectos.Externo && beneficiary.TipoDestinatario.ToUpper().Equals("M"))
            //    {
            //        Result = new string[] { "1", "El proyecto debe ser interno para establecer al centro como beneficiario." };
            //        Logger.LogWarning(Result[1], "Yo XD", GetType().Name + ": InsertUpdateBeneficiaries", IpAddress);
            //        return;
            //    }

            //    // Si es grupo meta
            //    if (beneficiary.TipoDestinatario.ToUpper().Equals("G"))
            //    {
            //        if (beneficiary.Descripcion.Equals(string.Empty))
            //        {
            //            Result = new string[] { "1", "Debe proporcionar la descripción del grupo meta." };
            //            Logger.LogWarning(Result[1], "Yo XD", GetType().Name + ": InsertUpdateBeneficiaries", IpAddress);
            //            return;
            //        }
            //        else
            //        {
            //            // Se valida que no exista el grupo meta en el proyecto
            //            var fromDb = detRepository.Get(b => b.Descripcion == beneficiary.Descripcion &&
            //                                                b.TipoDestinatario.ToUpper().Equals("G") &&
            //                                                b.IdProyecto == beneficiary.MstProyectos.IdProyecto);

            //            if (fromDb == null)
            //            {
            //                beneficiary.IdProyecto = beneficiary.MstProyectos.IdProyecto;

            //                detRepository.Add(beneficiary);
            //            }
            //            else
            //            {
            //                Result = new string[] { "1", "No se pueden guardar dos grupos metas con la misma descripción." };
            //                Logger.LogWarning(Result[1], "Yo XD", GetType().Name + ": InsertUpdateBeneficiaries", IpAddress);
            //                return;
            //            }
            //        }
            //    }
            //    //Si es privado
            //    else if (beneficiary.TipoDestinatario.ToUpper().Equals("P"))
            //    {
            //        if (beneficiary.MstBeneficiarios == null)
            //        {
            //            Result = new string[] { "1", "Debe proporcionar los datos requeridos del beneficiario." };
            //            Logger.LogWarning(Result[1], "Yo XD", GetType().Name + ": InsertUpdateBeneficiaries", IpAddress);
            //            return;
            //        }
            //        else
            //        {
            //            // Se valida que no exista asignacion de beneficiario al proyecto
            //            var fromDb = detRepository.Get(b => b.IdBeneficiario == beneficiary.IdBeneficiario &&
            //                                                b.TipoDestinatario.ToUpper().Equals("P") &&
            //                                                b.IdProyecto == beneficiary.MstProyectos.IdProyecto);

            //            if (fromDb == null)
            //            {
            //                beneficiary.IdProyecto = beneficiary.MstProyectos.IdProyecto;

            //                detRepository.Add(beneficiary);
            //            }
            //            else
            //            {
            //                Result = new string[] { "1", "No se puede vincular un beneficiario dos veces al mismo proyecto." };
            //                Logger.LogWarning(Result[1], "Yo XD", GetType().Name + ": InsertUpdateBeneficiaries", IpAddress);
            //                return;
            //            }
            //        }
            //    }
            //    //Si es proyecto interno
            //    else
            //    {

            //    }
            //}
            //catch (Exception ex)
            //{
            //    Logger.Log(Logger.MessageType.ERROR, ex, "Yo XD", GetType().Name + ": InsertUpdateBeneficiaries", IpAddress);

            //    Result = new string[] { "2", "Ocurrió un error interno en el sistema. Por favor, contacte al administrador." };
            //}
        }

        private ResultBase InsertProgramProject(ICollection<DetProgramaProyecto> programs, int projectID)
        {
            ResultBase result = new ResultBase();

            try
            {
                int count = 0;
                Repository<DetProgramaProyecto> detRepository = _unitOfWork.GetRepository<DetProgramaProyecto>();

                foreach (DetProgramaProyecto program in programs)
                {
                    // Se valida que no esté asignado el programa al proyecto
                    var temp = detRepository.Get(p => p.IdPrograma == program.IdPrograma &&
                                                      p.IdProyecto == program.IdProyecto);

                    if (temp == null)
                    {
                        program.IdProyecto = projectID;

                        var sequencesService = new SequenceService(_unitOfWork.GetRepository<MstSecuencias>());
                        MstSecuencias seq = sequencesService.GetNextSequenceByTableName("[pro].[DetProgramaProyecto]");

                        // Se le suma el contador debido a que estando en la misma transaccion la tabla de secuencia no actualiza el registro
                        program.IdProgramaProyecto = seq.SecuencialTabla + count;

                        // Solo se va actualizar hasta que se hayan procesado todos los registros
                        if (count == programs.Count - 1)
                        {
                            seq.SecuencialTabla = seq.SecuencialTabla + count;
                            sequencesService.InsertSequence(seq);
                        }

                        detRepository.Add(program);
                        count++;
                    }
                    else
                        count++;
                }

                //// Eliminando los programas 
                //var programsToDelete = detRepository.GetAll().GroupJoin(programs,
                //                                  db => db.IdProyecto,
                //                            lst => lst.IdProyecto,
                //                            (db, lst) => new { db, lst }
                //                        ).SelectMany(p => p.lst.DefaultIfEmpty(),
                //                            (list, program) => new { list.db, program }
                //                        ).Where(p => p.program == null).Select(p => p.db);

                //foreach (var item in programsToDelete)
                //{
                //    detRepository.Delete(item);
                //}
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorSaveData, "proyecto");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        private ResultBase InsertManagerProject(ICollection<DetProyectoResponsable> managers, MstProyectos project, int projectID)
        {
            ResultBase result = new ResultBase();

            try
            {
                int count = 0;
                //MstProyectos project = managers.FirstOrDefault().MstProyectos;

                //Repository<DetPersonal> repositoryPersonal = _unitOfWork.GetRepository<DetPersonal>();
                Repository<DetProyectoResponsable> detRepository = _unitOfWork.GetRepository<DetProyectoResponsable>();

                foreach (DetProyectoResponsable item in managers)
                {
                    //Se verifica que la persona no se encuentre vinculada al proyecto
                    var temp = detRepository.Get(r => r.DetPersonal.IdPersonal == item.IdPersonal &&
                                                      r.MstProyectos.IdProyecto == projectID);

                    if (temp == null)
                    {
                        PersonalService personalService = new PersonalService();
                        int[] programsID = project.DetProgramaProyecto.Select(p => p.IdPrograma).ToArray();

                        // Se valida que el responsable seleccionado sea coordinador de algún programa vinculado al proyecto
                        if (personalService.IsCoordinator(item.IdPersonal.Value, programsID).MessageType == MessageType.SUCCESS)
                        {
                            item.IdProyecto = projectID;

                            var sequencesService = new SequenceService(_unitOfWork.GetRepository<MstSecuencias>());
                            MstSecuencias seq = sequencesService.GetNextSequenceByTableName("[pro].[DetProyectoResponsable]");

                            item.IdProyectoResponsable = seq.SecuencialTabla + count;

                            // Solo se va actualizar hasta que se hayan procesado todos los registros
                            if (count == managers.Count - 1)
                            {
                                seq.SecuencialTabla = seq.SecuencialTabla + count;
                                sequencesService.InsertSequence(seq);
                            }

                            detRepository.Add(item);
                            count++;
                        }
                        else
                        {
                            result.Message = "No se puede asignar como responsable de proyecto a una persona que no es coordinador de programa.";
                            result.DetailException = new Exception(result.Message);
                            result.MessageType = MessageType.ERROR;
                            return result;
                        }
                    }
                    else
                        count++;
                }

                //delete con left join
            }
            catch (Exception ex)
            {
                result.DetailException = ex;
                result.Message = string.Format(Messages.ErrorSaveData, "proyecto");
                result.MessageType = MessageType.ERROR;
            }

            return result;
        }

        //private ResultBase DeleteGoals(MstProyectos project, UnitOfWork unitOfWork)
        //{
        //    ResultBase result = new ResultBase();

        //    try
        //    {
        //        var repository = unitOfWork.GetRepository<DetProyectoObjetivos>();
        //        var repositoryGoal = unitOfWork.GetRepository<MstObjetivos>();

        //        List<DetProyectoObjetivos> projectGoals = new List<DetProyectoObjetivos>();
        //        project.DetProyectoObjetivos.ToList().ForEach(p => projectGoals.Add(new DetProyectoObjetivos
        //        {
        //            IdObjetivo = p.IdObjetivo,
        //            IdProyecto = p.IdProyecto,
        //            IdProyectoObjetivo = p.IdProyectoObjetivo
        //        }));

        //        var projectGoalsInDB = repository.GetMany(p => p.IdProyecto == project.IdProyecto);

        //        foreach (var item in projectGoalsInDB)
        //        {
        //            // Se elimina el registro DetProyectoObjetivos
        //            if (!projectGoals.Any(p => p.IdObjetivo == item.IdObjetivo && p.IdProyectoObjetivo == item.IdProyectoObjetivo))
        //                repository.Delete(item);

        //            // Se elimina el registro de la tabla MstObjetivos
        //            var goal = repositoryGoal.GetById(item.IdObjetivo);

        //            if (!IsNull(goal))
        //                repositoryGoal.Delete(goal);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return result;
        //}

        private ResultBase DeleteProgramProject(MstProyectos project, UnitOfWork unitOfWork)
        {
            ResultBase result = new ResultBase();

            try
            {
                var repository = unitOfWork.GetRepository<DetProgramaProyecto>();

                List<DetProgramaProyecto> programsProject = new List<DetProgramaProyecto>();
                project.DetProgramaProyecto.ToList().ForEach(p => programsProject.Add(new DetProgramaProyecto
                {
                    IdPrograma = p.IdPrograma,
                    IdProgramaProyecto = p.IdProgramaProyecto,
                    IdProyecto = p.IdProyecto
                }));

                var programsProjectInDB = repository.GetMany(p => p.IdProyecto == project.IdProyecto);

                var repositoryPersonal = unitOfWork.GetRepository<DetPersonal>();
                var repositoryPersonalTask = unitOfWork.GetRepository<DetTareaPersonal>();

                foreach (var item in programsProjectInDB)
                {
                    bool existDependency = ValidateProgramDependencyProject(item.IdPrograma, unitOfWork);

                    if (!existDependency)
                    {
                        // Se elimina el registro DetProgramaProyecto
                        if (!programsProject.Any(p => p.IdPrograma == item.IdPrograma))
                            repository.Delete(item);
                    }
                    else
                        new Exception("El programa que esta intentando desvincular del proyecto ya tiene personal asignado a las tareas");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        private ResultBase DeleteManagerProject(MstProyectos project, UnitOfWork unitOfWork)
        {
            ResultBase result = new ResultBase();

            try
            {
                var repository = unitOfWork.GetRepository<DetProyectoResponsable>();

                List<DetProyectoResponsable> projectManagers = new List<DetProyectoResponsable>();
                project.DetProyectoResponsable.ToList().ForEach(p => projectManagers.Add(new DetProyectoResponsable
                {
                    IdProyecto = p.IdProyecto,
                    IdPersonal = p.IdPersonal,
                    IdProyectoResponsable = p.IdProyectoResponsable
                }));

                var projectManagersInDB = repository.GetMany(p => p.IdProyecto == project.IdProyecto);

                var repositoryPersonalTask = unitOfWork.GetRepository<DetTareaPersonal>();

                foreach (var item in projectManagersInDB)
                {
                    if (repositoryPersonalTask.Any(r => r.IdPersonal == item.IdPersonal && r.IdProyecto == item.IdProyecto))
                    {
                        string strMessage = "A {0} ya se le asignó una tarea, por ende no se puede desvincular del proyecto.";
                        throw new Exception(string.Format(strMessage, item.DetPersonal.MstPersonas.NombreCompleto));
                    }
                    else
                    {
                        // Se elimina el registro DetProgramaProyecto
                        if (!projectManagers.Any(p => p.IdPersonal == item.IdPersonal))
                            repository.Delete(item);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        #region Cálculos

        private ResultBase CalculateEstimatedDurationInDaysAndHours(ref MstProyectos project)
        {
            ResultBase result = new ResultBase();

            try
            {
                // Se obtiene la cantidad de horas laborales para un horario de medio tiempo
                var parameter = _unitOfWork.GetRepository<CatParametros>().Get(p => p.NombreParametro.Equals("CANT_HORAS_LABORALES_MT"));
                double hoursMiddleTimeCount = double.Parse(parameter.ValorParametro);

                if (!IsNull(project))
                {
                    //var project = Repository.GetById(projectID);
                    int workingDaysPerWeekID = project.DiasLabSem;

                    // Se obtiene las jornada laborales
                    var detailCatalogService = new DetailCatalogService();

                    var resultDetailCatalog = detailCatalogService.GetDetailsByTable("JornadaLaboralSemanal");

                    if (resultDetailCatalog.MessageType != MessageType.INFO)
                        throw resultDetailCatalog.DetailException;

                    var workday = resultDetailCatalog.Data.FirstOrDefault(w => w.IdDetCatalogo == workingDaysPerWeekID);

                    int strHorarioId = int.Parse(workday.Valor);

                    // Se obtienen los días laborales que el horario posee 
                    var workdayCount = _unitOfWork.GetRepository<DetHorario>().GetMany(s => s.IdHorario == strHorarioId);

                    // Se cargan en un listado de dias de la semana
                    Dictionary<DayOfWeek, double> lstDaysOfWeek = new Dictionary<DayOfWeek, double>();
                    foreach (var day in workdayCount)
                    {
                        double workingHours = day.HoraSalida.Subtract(day.HoraEntrada).TotalHours;

                        // Si el horario excede la cantidad de horas de medio tiempo se le resta una hora (Almuerzo)
                        if (workingHours > hoursMiddleTimeCount)
                            workingHours = workingHours - 1;

                        lstDaysOfWeek.Add((DayOfWeek)day.Dia, workingHours);
                    }

                    // Calculando los dias laborables
                    double estimatedDurationInDays = 0, estimatedDurationInHours = 0;
                    TimeSpan ts = project.FechaFinEstimada - project.FechaInicio;

                    for (int i = 0; i < ts.Days; i++)
                    {
                        // Si el dia de la semana está en la lista de dias de la semana laborables, se suma un día
                        if (lstDaysOfWeek.Keys.Contains((project.FechaInicio.AddDays(i)).DayOfWeek))
                        {
                            double workingHours = lstDaysOfWeek[(project.FechaInicio.AddDays(i)).DayOfWeek];

                            // Si el día tiene horario de tiempo completo
                            if (workingHours > hoursMiddleTimeCount)
                                estimatedDurationInDays = estimatedDurationInDays + 1; // Se le suma un día
                            else
                                estimatedDurationInDays = estimatedDurationInDays + 0.5; // De lo contrario se le suma medio dia

                            // Se suman las horas
                            estimatedDurationInHours = estimatedDurationInHours + lstDaysOfWeek[(project.FechaInicio.AddDays(i)).DayOfWeek];
                        }
                    }

                    // Restando los días feriados 
                    var listHolidayActive = _unitOfWork.GetRepository<CatFeriados>().GetMany(h => h.Activo);

                    // Se recorren todos los días feriado
                    foreach (var holiday in listHolidayActive)
                    {
                        // Se arma una nueva fecha con el dia del mes feriado pero con el año actual
                        DateTime newDate = new DateTime(DateTime.Now.Year, holiday.FechaFeriado.Month, holiday.FechaFeriado.Day);

                        // Si el día feriado está entre las fechas del inicio y fin del proyecto, se resta el día y las horas laborables de ese día
                        if ((newDate.CompareTo(project.FechaInicio) == 0 || newDate.CompareTo(project.FechaInicio) > 0) &&
                            (newDate.CompareTo(project.FechaFinEstimada) == 0 || newDate.CompareTo(project.FechaFinEstimada) < 0))
                        {
                            estimatedDurationInDays = estimatedDurationInDays - 1;
                            estimatedDurationInHours = estimatedDurationInHours + lstDaysOfWeek[newDate.DayOfWeek];
                        }
                    }

                    // Se establece la duración estimada en días y horas del proyecto
                    project.DuracionEstimadaDias = Convert.ToDecimal(estimatedDurationInDays);
                    project.DuracionEstimadaHoras = Convert.ToDecimal(estimatedDurationInHours);

                    //// Se actualiza el registro de proyecto
                    //Repository.Update(project);
                }
                else
                {
                    result.Message = string.Format(Messages.NotExist, "proyecto");
                    result.DetailException = new Exception("No se encontró registro de proyecto con el id: " + project.IdProyecto);
                    result.MessageType = MessageType.ERROR;
                }
            }
            catch (Exception ex)
            {
                result.Message = "Ocurrió un error al intentar calcular la duración estimada en días del proyecto";
                result.MessageType = MessageType.ERROR;
                result.DetailException = ex;
            }

            return result;
        }

        private ResultBase CalculateEstimatedEndDate(ref MstProyectos project)
        {
            ResultBase result = new ResultBase();

            try
            {
                int projectID = project.IdProyecto;

                var lastTask = _unitOfWork.GetRepository<MstTareas>().GetMany(t => t.IdProyecto == projectID, "FechaFinEstimada", SortingOrder.DESC).FirstOrDefault();

                if (!IsNull(lastTask))
                    project.FechaFinEstimada = lastTask.FechaFin;
                else
                {
                    result.Message = "No existen tareas registrada para el proyecto";
                    result.DetailException = new Exception(result.Message);
                    result.MessageType = MessageType.ERROR;
                }
            }
            catch (Exception ex)
            {
                result.Message = "Ocurrió un error al intentar calcular la fecha fin estimada del proyecto";
                result.MessageType = MessageType.ERROR;
                result.DetailException = ex;
            }

            return result;
        }

        #endregion

        #region Validaciones

        private ResultBase ValidateProjectData(MstProyectos project, bool update = false)
        {
            ResultBase result = new ResultBase();

            /***************************************** VALIDACION DE LA INFORMACIÓN DEL PROYECTO ******************************************/

            // Se valida que el objeto de MstProyectos no sea nulo
            if (IsNull(project))
            {
                result.MessageType = MessageType.ERROR;
                result.Message = string.Format(Messages.WithoutValidate, "proyecto");
                result.DetailException = new Exception(result.Message + ". Debido a que el objeto MstProyectos es nulo");
                goto @return;
            }

            // Si es actualización
            if (update)
            {
                // Se valida el id del proyecto
                if (project.IdProyecto == 0)
                {
                    result.MessageType = MessageType.ERROR;
                    result.Message = string.Format(Messages.WithoutValidate, "proyecto");
                    result.DetailException = new Exception(result.Message + ". Debido a que no se proporcionó el identificador del proyecto en la actualización");
                    goto @return;
                }

                // Se valida que se haya proporcionado el usuario que actualiza el registro
                if (!project.IdUsuarioUpd.HasValue || (project.IdUsuarioUpd.HasValue && project.IdUsuarioUpd.Value == 0))
                {
                    result.MessageType = MessageType.ERROR;
                    result.Message = Messages.UserInsUpdInvalid;
                    result.DetailException = new Exception("Id de usuario que actualiza no proporcionado");
                    goto @return;
                }
            }
            else // Si es inserción
            {
                // Se valida que se haya proporcionado el usuario que inserta el registro
                if (project.IdUsuarioIns == 0)
                {
                    result.MessageType = MessageType.ERROR;
                    result.Message = Messages.UserInsUpdInvalid;
                    result.DetailException = new Exception("Id de usuario que inserta no proporcionado");
                    goto @return;
                }

                // Se valida el estado del proyecto que por defecto debe ser  «Pendiente de Planificar»
                if (project.IdEstado > 0)
                {
                    var parameter = _unitOfWork.GetRepository<CatParametros>().Get(p => p.NombreParametro.Equals("ESTADO_PROYECTO_PEN_PLAN"));

                    if (project.IdEstado != int.Parse(parameter.ValorParametro))
                    {
                        result.MessageType = MessageType.ERROR;
                        result.Message = "El estado inicial del proyecto debe ser «Pendiente de Planificar»";
                        result.DetailException = new Exception(result.Message);
                        goto @return;
                    }
                }
            }

            // Se valida el estado del proyecto
            if (project.IdEstado <= 0)
            {
                result.MessageType = MessageType.ERROR;
                result.Message = "Se debe proporcionar el estado del proyecto";
                result.DetailException = new Exception(result.Message);
                goto @return;
            }

            // Se valida el título del proyecto
            if (string.IsNullOrEmpty(project.TituloProyecto))
            {
                result.MessageType = MessageType.ERROR;
                result.Message = "Se debe proporcionar el título del proyecto";
                result.DetailException = new Exception(result.Message);
                goto @return;
            }

            // Se valida la descripción del proyecto
            if (string.IsNullOrEmpty(project.DescProyecto))
            {
                result.MessageType = MessageType.ERROR;
                result.Message = "Se debe proporcionar la descripción del proyecto";
                result.DetailException = new Exception(result.Message);
                goto @return;
            }

            // Se valida el tipo de proyecto
            if (project.IdTipoProyecto <= 0)
            {
                result.MessageType = MessageType.ERROR;
                result.Message = "Se debe proporcionar el tipo de proyecto";
                result.DetailException = new Exception(result.Message);
                goto @return;
            }

            // Se valida la moneda del proyecto
            if (project.IdMoneda <= 0)
            {
                result.MessageType = MessageType.ERROR;
                result.Message = "Se debe proporcionar la moneda con la que se manejaran el presupuesto del proyecto";
                result.DetailException = new Exception(result.Message);
                goto @return;
            }

            // Se valida el presupuesto del proyecto
            if (project.Presupuesto <= 0)
            {
                result.MessageType = MessageType.ERROR;
                result.Message = "El presupuesto del proyecto debe ser mayor a 0";
                result.DetailException = new Exception(result.Message);
                goto @return;
            }

            // Se validan los dias laborales del proyecto (Esto es para calcular lo más preciso posible el tiempo invertido en el proyecto)
            if (project.DiasLabSem <= 0)
            {
                result.MessageType = MessageType.ERROR;
                result.Message = "Se debe proporcionar los días laborales que se trabajaran en el proyecto";
                result.DetailException = new Exception(result.Message);
                goto @return;
            }

            // Se valida la justificación del proyecto
            if (string.IsNullOrEmpty(project.Justificacion))
            {
                result.MessageType = MessageType.ERROR;
                result.Message = "Se debe proporcionar la justificación del proyecto";
                result.DetailException = new Exception(result.Message);
                goto @return;
            }

            // Se validan que la fecha límite del proyecto no sea mayor a la fecha de inicio del mismo
            if (project.FechaLimite.CompareTo(project.FechaInicio) < 0)
            {
                result.MessageType = MessageType.ERROR;
                result.Message = "La fecha límite de cierre del proyecto debe ser mayor a la inicial";
                result.DetailException = new Exception(result.Message);
                goto @return;
            }

            /***************************************** VALIDACION DE LOS BENEFICIARIOS (GRUPO META) ******************************************/
            if (IsNull(project.DetProyectoBeneficiarios))
            {
                result.MessageType = MessageType.ERROR;
                result.Message = "Se debe proporcionar el grupo meta del proyecto";
                result.DetailException = new Exception(result.Message);
                goto @return;
            }
            else
            {
                // Esta validación solo se hizo para la mono porque el centro no tiene beneficiarios como tal, sino que es una descripción general
                if (string.IsNullOrEmpty(project.DetProyectoBeneficiarios.Descripcion))
                {
                    result.MessageType = MessageType.ERROR;
                    result.Message = "Se debe proporcionar el grupo meta del proyecto";
                    result.DetailException = new Exception(result.Message);
                    goto @return;
                }
            }

            /***************************************** VALIDACION DE LOS OBJETIVOS DEL PROYECTO ******************************************/
            if (IsNull(project.DetProyectoObjetivos) || (!IsNull(project.DetProyectoObjetivos) && project.DetProyectoObjetivos.Count <= 0))
            {
                result.MessageType = MessageType.ERROR;
                result.Message = "Se debe proporcionar al menos un objetivo para el proyecto";
                result.DetailException = new Exception(result.Message);
                goto @return;
            }

            /***************************************** VALIDACION DE LOS PROGRAMAS DEL PROYECTO ******************************************/
            if (IsNull(project.DetProgramaProyecto) || (!IsNull(project.DetProgramaProyecto) && project.DetProgramaProyecto.Count <= 0))
            {
                result.MessageType = MessageType.ERROR;
                result.Message = "Se debe vincular al menos un programa ejecutor al proyecto";
                result.DetailException = new Exception(result.Message);
                goto @return;
            }

            /***************************************** VALIDACION DE LOS RESPONSABLES DEL PROYECTO ******************************************/
            if (IsNull(project.DetProyectoResponsable) || (!IsNull(project.DetProyectoResponsable) && project.DetProyectoResponsable.Count <= 0))
            {
                result.MessageType = MessageType.ERROR;
                result.Message = "Se debe vincular al menos un responsable de proyecto";
                result.DetailException = new Exception(result.Message);
                goto @return;
            }

        @return:
            return result;
        }

        public bool ValidateProgramDependencyProject(int programId, UnitOfWork unitOfWork)
        {
            bool exist = false;

            try
            {
                var repository = unitOfWork.GetRepository<DetProgramaProyecto>();

                exist = repository.GetAll().Join(Repository.GetAll(),
                                            pp => pp.IdProyecto, p => p.IdProyecto, (pp, p) => new { pp, p })
                                        .Join(unitOfWork.GetRepository<MstTareas>().GetAll(),
                                            x => x.p.IdProyecto, t => t.IdProyecto, (x, t) => new { x, t })
                                        .Join(unitOfWork.GetRepository<DetTareaPersonal>().GetAll(),
                                            y => y.t.IdTarea, dtp => dtp.IdTarea, (y, dtp) => new { y, dtp })
                                        .Any(p => p.y.x.pp.IdPrograma == programId);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return exist;
        }

        #endregion

        #endregion
    }
}
