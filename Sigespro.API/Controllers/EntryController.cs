﻿using Common.Extensions;
using Common.Log;
using Common.Helpers;
using Sigespro.Entity;
using Sigespro.Entity.Resource;
using Sigespro.Entity.Custom;
using Sigespro.Service;
using Sigespro.Service.Pre;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Sigespro.API.Controllers
{
    [RoutePrefix("Api/Entry")]
    public class EntryController : ApiController
    {
        EntryService _service;

        public EntryController(Func<string, IService> serviceFactory)
        {
            _service = (EntryService)serviceFactory("Entry");
        }

        [HttpGet]
        [Route("")]
        public IEnumerable<Entry> Get()
        {
            return Entries();
        }

        [HttpGet]
        [Route("{EntryCode}")]
        public CatRubros GetEntry(string EntryCode)
        {
            var result = _service.GetEntryById(EntryCode);

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data;
        }

        [HttpGet]
        [Route("Filter/{Filter}")]
        public IEnumerable<Entry> GetEntries(string Filter)
        {
            return Entries(Filter);
        }

        private IEnumerable<Entry> Entries(string parameter = "")
        {
            Result<IEnumerable<Entry>> result = new Result<IEnumerable<Entry>>();

            if (string.IsNullOrEmpty(parameter))
                result = _service.GetEntries();
            else
                result = _service.GetEntries(parameter);

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data;
        }

        [HttpGet]
        [Route("GetByTypes/{EntryCode}/{EntryType}")] 
        public IEnumerable<CatRubros> GetEntriesByTypes(string EntryCode, string EntryType)
        {
            var result = _service.GetEntriesByType(EntryCode, EntryType);

            if (result.MessageType != MessageType.SUCCESS)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(result.Message)
                };

                Logger.Log(result.MessageType, result.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());

                throw new HttpResponseException(resp);
            }

            return result.Data;
        }

        [HttpPost]
        [Route("List")]
        public HttpResponseMessage ListEntries(QueryOptions<IEnumerable<vwRubros>> filter)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            _service.GetEntriesForList(ref filter);

            if (filter.ResultData.MessageType == MessageType.INFO)
                response = Request.CreateResponse(HttpStatusCode.OK, filter, "application/json");
            else
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, filter.ResultData.Message);
                Logger.Log(filter.ResultData.MessageType, filter.ResultData.DetailException, "Yo xD", GetType().Name, Request.GetClientIpAddress());
            }

            return response;
        }

        [HttpPost]
        [Route("")]
        public HttpResponseMessage Post(CatRubros entry)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            if (ModelState.IsValid)
            {
                ResultBase result = new ResultBase();

                try
                {
                    result = _service.InsertEntry(entry);

                    if (result.MessageType == MessageType.SUCCESS)
                    {
                        response = Request.CreateResponse(HttpStatusCode.Created, entry);

                        //string uri = Url.Link("DefaultApi", new { id = entry.IdRubro });

                        //response.Headers.Location = new Uri(uri);
                    }
                    else
                    {
                        if (result.MessageType == MessageType.WARNING)
                            response = Request.CreateErrorResponse(HttpStatusCode.Conflict, result.Message);
                        else
                            response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, result.Message);

                        throw result.DetailException;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log(result.MessageType, ex, "Yo xD", GetType().Name, Request.GetClientIpAddress());
                }
            }
            else
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.RequiredData);

            return response;
        }

        [HttpPut]
        [Route("{EntryCode}")]
        public HttpResponseMessage Put(string EntryCode, CatRubros entry)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            if (!ModelState.IsValid)
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.RequiredData);
            else
            {
                ResultBase result = new ResultBase();

                try
                {
                    if (!EntryCode.Equals(entry.IdRubro))
                        response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "La información no coincide con el identificador proporcionado.");
                    else
                    {
                        result = _service.UpdateEntry(entry);

                        if (result.MessageType == MessageType.SUCCESS)
                            response = Request.CreateResponse(HttpStatusCode.OK);
                        else
                        {
                            if (result.MessageType == MessageType.WARNING)
                                return Request.CreateErrorResponse(HttpStatusCode.NotFound, result.Message);

                            else if (result.MessageType == MessageType.ERROR)
                                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result.Message);

                            throw result.DetailException;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log(result.MessageType, ex, "Yo xD", GetType().Name, Request.GetClientIpAddress());
                }
            }

            return response;
        }
    }
}
