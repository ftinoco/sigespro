﻿using Sigespro.Web.Mvc.Utilities.Attributes.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Sigespro.Web.Mvc
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalFilters.Filters.Add(new AuthCustomAttribute());
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
                
            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.Name;
            AntiForgeryConfig.UniqueClaimTypeIdentifier = "FullName";
            AntiForgeryConfig.UniqueClaimTypeIdentifier = "UserId";

            ModelBinders.Binders.Add(typeof(decimal), new Utilities.Binder.DecimalModelBinder());
            ModelBinders.Binders.Add(typeof(decimal?), new Utilities.Binder.DecimalNullableModelBinder());
        }

        protected void Application_BeginRequest()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();
        }
    }
}
