﻿using Common.Helpers;
using Sigespro.Web.Mvc.Resources;
using Sigespro.Web.Mvc.Models.Administration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using Newtonsoft.Json;
using Common.Log;
using System.Reflection;
using Sigespro.Web.Mvc.Utilities.Attributes.Security;
using Sigespro.Web.Mvc.Utilities.Security;
using Sigespro.Service.Admin;
using Sigespro.Entity;
using Common.Extensions;

namespace Sigespro.Web.Mvc.Controllers.Administration
{
    [RoutePrefix("Administration/Catalogs")] 
    public class CatalogController : Controller
    {
        private readonly IUserSession _userSession;

        public CatalogService Service { get; set; }
         
        public CatalogController()
        {
            _userSession = new UserSession();
            Service = new CatalogService();
        }

        [Route]
        public ActionResult Index(QueryOptions<IEnumerable<CatalogViewModel>> queryOpt)
        {
            string message = string.Empty;
            IEnumerable<CatalogViewModel> model = new List<CatalogViewModel>();
             
            string strSerialize = JsonConvert.SerializeObject(queryOpt);

            QueryOptions<IEnumerable<MstCatalogo>> filter = JsonConvert.DeserializeObject<QueryOptions<IEnumerable<MstCatalogo>>>(strSerialize);
            Service.GetCatalogsForList(ref filter);

            if (filter.ResultData.MessageType != MessageType.INFO)
            {
                message = filter.ResultData.Message;
                Logger.Log(filter.ResultData.MessageType, filter.ResultData.DetailException, _userSession.Username,
                           MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }
            else
            {
                strSerialize = JsonConvert.SerializeObject(filter);

                queryOpt = JsonConvert.DeserializeObject<QueryOptions<IEnumerable<CatalogViewModel>>>(strSerialize);

                model = queryOpt.ResultData.Data;
            }

            if (!string.IsNullOrEmpty(message))
                ModelState.AddModelError(Messages.AlertDanger, message);

            ViewBag.Filter = true;
            ViewBag.QueryOptions = queryOpt;

            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [Route("Add")]
        public ActionResult AddCatalog()
        {
            ViewBag.IsEdit = false;
            ViewBag.Action = "SaveCatalog";
            ViewBag.Title = "Nuevo Catálogo";

            return View();
        }

        [Route("Edit/{catalogId}")]
        public ActionResult EditCatalog(int catalogId)
        {
            string message = string.Empty;
            CatalogViewModel model = new CatalogViewModel();

            try
            {
                var result = Service.GetCatalogById(catalogId); 

                if (result.MessageType != MessageType.INFO)
                    message = result.Message;
                else
                {
                    model = new CatalogViewModel
                    {
                        Activo = result.Data.Activo,
                        Descripcion = result.Data.Descripcion,
                        FechaIns = result.Data.FechaIns,
                        FechaUpd = result.Data.FechaUpd,
                        IdCatalogo = result.Data.IdCatalogo,
                        IdUsuarioIns = result.Data.IdUsuarioIns,
                        IdUsuarioUpd = result.Data.IdUsuarioUpd,
                        Nombre = result.Data.Nombre
                    };
                }
            }
            catch (Exception ex)
            {
                message = Messages.ErrorApi;
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                           MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            if (!string.IsNullOrEmpty(message))
                ModelState.AddModelError(Messages.AlertDanger, message);

            ViewBag.IsEdit = true;
            ViewBag.Action = "UpdateCatalog";
            ViewBag.Title = "Editar Catálogo";

            return View("AddCatalog", model);
        }

        [AjaxOnly]
        [HttpPost]
        [Route("Save")]
        [AllowAnonymous]
        public ActionResult SaveCatalog(CatalogViewModel model)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                model.IdUsuarioIns = _userSession.UserId;
                string strSerialize = JsonConvert.SerializeObject(model);

                var result = Service.InsertCatalog(JsonConvert.DeserializeObject<MstCatalogo>(strSerialize));

                if (result.MessageType != MessageType.SUCCESS)
                {
                    message = result.Message;
                    status = result.MessageType.ToString();
                    Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                    message = string.Format(Messages.SaveSuccess, "catálogo");
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                             MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message, action = Url.Action("Index", "Catalog") },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [AjaxOnly]
        [HttpPost]
        [AllowAnonymous]
        [Route("Update")]
        public ActionResult UpdateCatalog(CatalogViewModel model)
        {
            string message = string.Empty, status = "SUCCESS";

            try
            {
                model.IdUsuarioUpd = _userSession.UserId;
                string strSerialize = JsonConvert.SerializeObject(model);

                var result = Service.UpdateCatalog(JsonConvert.DeserializeObject<MstCatalogo>(strSerialize)); 

                if (result.MessageType != MessageType.SUCCESS)
                { 
                    message = result.Message;
                    status = result.MessageType.ToString();
                    Logger.Log(result.MessageType, result.DetailException, _userSession.Username,
                            MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
                }
                else
                    message = string.Format(Messages.UpdateSuccess, "catálogo");
            }
            catch (Exception ex)
            {
                Logger.Log(MessageType.ERROR, ex, _userSession.Username,
                             MethodBase.GetCurrentMethod().Name + " - " + GetType().Name, Request.UserHostAddress);
            }

            return new JsonResult
            {
                Data = new { status, message, action = Url.Action("Index", "Catalog") },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }
}
