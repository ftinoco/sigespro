﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigespro.Entity.Custom
{
    public class DeliverableInfo
    {
        public int DeliverableID { get; set; }

        public int ProjectID { get; set; }

        public string Description { get; set; }

        public List<TaskInfo> Tasks { get; set; }
    }
}
