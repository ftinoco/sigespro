﻿using Sigespro.Web.Mvc.Models.Administration;
using Sigespro.Web.Mvc.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sigespro.Web.Mvc.Models.Project
{
    public class ProjectViewModel
    {
        public int IdProyecto { get; set; }

        [Display(Name = "ProjectTitle", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string TituloProyecto { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Description", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string DescProyecto { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Justification", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public string Justificacion { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "StartDate", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public DateTime FechaInicio { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "EstimatedEndDate", ResourceType = typeof(Labels))]
        public DateTime FechaFinEstimada { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Deadline", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public DateTime FechaLimite { get; set; }

        public decimal DuracionEstimadaHoras { get; set; }

        public int HorasDedicadas { get; set; }

        public bool Externo { get; set; }

        [Display(Name = "Budget", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [Range(typeof(decimal), "1", "99999999999999", ErrorMessageResourceName = "RengeNumeric", ErrorMessageResourceType = typeof(Messages))]
        public decimal Presupuesto { get; set; }

        public decimal PresupuestoActual { get; set; }

        public decimal GastoEstimado { get; set; }

        [Display(Name = "ProjectType", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public int IdTipoProyecto { get; set; }

        [Display(Name = "ProjectStatus", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public int IdEstado { get; set; }

        public int IdUsuarioIns { get; set; }

        public DateTime FechaIns { get; set; }

        public int? IdUsuarioUpd { get; set; }

        public DateTime? FechaUpd { get; set; }

        [Display(Name = "WeeklyWorkday", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public int DiasLabSem { get; set; }

        [Display(Name = "Currency", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        public int IdMoneda { get; set; }

        public decimal DuracionEstimadaDias { get; set; }

        public List<ProgramProject> DetProgramaProyecto { get; set; }

        public BeneficiaryProject DetProyectoBeneficiarios { get; set; }

        public List<GoalsProject> DetProyectoObjetivos { get; set; }

        public List<ProjectManager> DetProyectoResponsable { get; set; }
    }

}