﻿using Common.Extensions;
using Common.Mailer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Helpers
{
    public class Utilities
    {
        public static bool IsNull(object o)
        {
            return o == null;
        }

        public static ResultBase ValidateEntity(object entity)
        {
            ResultBase result = new ResultBase();

            var context = new ValidationContext(entity, serviceProvider: null, items: null);

            var results = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(entity, context, results, true);

            if (!isValid)
            {
                foreach (var validationResult in results)
                {
                    Console.WriteLine(validationResult.ErrorMessage);
                }

                result.MessageType = MessageType.INFO;
                result.Message = results[0].ErrorMessage;
                result.DetailException = new Exception(result.Message);
            }

            return result;
        }

        public static string GetEntityValidationErrors(System.Data.Entity.Validation.DbEntityValidationException ex)
        {
            StringBuilder objbuilder = new StringBuilder();

            objbuilder.AppendLine("Mensaje: " + ex.Message);
            objbuilder.AppendLine("StackTrace: " + ex.StackTrace);

            foreach (var eve in ex.EntityValidationErrors)
            {
                objbuilder.AppendLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State));

                foreach (var ve in eve.ValidationErrors)
                {
                    objbuilder.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
                }
            }

            return objbuilder.ToString();
        }

        public static string SHA512(string input)
        {
            var bytes = Encoding.UTF8.GetBytes(input);
            using (var hash = System.Security.Cryptography.SHA512.Create())
            {
                var hashedInputBytes = hash.ComputeHash(bytes);

                var hashedInputStringBuilder = new StringBuilder(128);

                foreach (var b in hashedInputBytes)
                    hashedInputStringBuilder.Append(b.ToString("X2"));

                return hashedInputStringBuilder.ToString();
            }
        }

        public static string CreateTemporaryPassword(int passwordLength)
        {
            string caracteresPermitidos = "abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789";

            byte[] randomBytes = new byte[passwordLength];

            char[] caracteres = new char[passwordLength];

            int caracteresPermitidosCount = caracteresPermitidos.Length;

            for (int i = 0; i < passwordLength; i++)
            {
                Random objRandom = new Random();

                objRandom.NextBytes(randomBytes);

                caracteres[i] = caracteresPermitidos[randomBytes[i] % caracteresPermitidosCount];
            }

            return new string(caracteres);
        }

        private static void WriteEventLogEntry(string message, string method, string source)
        {
            // Create an instance of EventLog
            System.Diagnostics.EventLog eventLog = new System.Diagnostics.EventLog();

            // Check if the event source exists. If not create it.
            if (!System.Diagnostics.EventLog.SourceExists(source))
            {
                System.Diagnostics.EventLog.CreateEventSource(source, "Application");
            }

            // Set the source name for writing log entries.
            eventLog.Source = source;

            // Create an event ID to add to the event log
            int eventID = 8;

            // Write an entry to the event log.
            eventLog.WriteEntry(method + ":" + message,
                                System.Diagnostics.EventLogEntryType.Error,
                                eventID);

            // Close the Event Log
            eventLog.Close();
        }

        public static bool SendEmail(string emailTo, string emailFromDisplayName, string Subject, string BodyEmail, ConfigDataMailing ConfigEmail)
        {
            try
            {
                //Crear el objeto de envio de mensajes
                if (!string.IsNullOrEmpty(emailTo))
                {
                    emailTo = emailTo.Replace(" ", "");
                    string[] EmailDestinos = emailTo.Split(';');

                    if (EmailDestinos.Length > 0) //si tiene destinatarios
                    {

                        EmailManager _emailManager = new EmailManager(ConfigEmail);
                        _emailManager.EmailFrom = ConfigEmail.EmailAddress;
                        _emailManager.EmailFromDisplayName = emailFromDisplayName;
                        foreach (string ItemsEmailsTo in EmailDestinos) //Agregamos todos los destinatarios
                        {
                            if (!string.IsNullOrWhiteSpace(ItemsEmailsTo) && ItemsEmailsTo.IsEmail()) //Si es un email valido
                            {
                                _emailManager.EmailTO.Add(ItemsEmailsTo);
                            }
                        }
                        _emailManager.TimeOut = 20000;
                        _emailManager.Subject = Subject;
                        _emailManager.IsBodyHTML = true;
                        _emailManager.Body = string.Format("<HTML><BODY>{0}</BODY></HTML>", BodyEmail);

                        ////Si se proporcionó un archivo
                        //if (Archivo != null || !string.IsNullOrWhiteSpace(FilePath))
                        //{
                        //    EmailAttachment _emailAttachment = new EmailAttachment(NameFile, MIMETYPE, Archivo, FilePath);
                        //    _emailManager.Files.Add(_emailAttachment);
                        //}

                        return _emailManager.SendEmail();
                    }
                    else
                        throw new Exception("No se proporcionaron destinatarios");
                }
                else
                    throw new Exception("Correo que envía es nulo");
            }
            catch (Exception ex)
            {
                WriteEventLogEntry(ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name + " - SendEmail", "Common");
                throw ex;
            }
        }
    }
}
