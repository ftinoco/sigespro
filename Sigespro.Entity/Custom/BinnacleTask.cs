﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sigespro.Entity.Custom
{
    public class BinnacleTask
    {
        public int TaskID { get; set; }

        public int ProjectID { get; set; }

        public string TaskName { get; set; }

        public DateTime Date { get; set; }

        public string User { get; set; }

        public bool Creation { get; set; }

        public string Status { get; set; }

        public string Comments { get; set; }

        public decimal? Progress { get; set; }

        public int? FileID { get; set; }

        public string FileName { get; set; } 

        public bool? FileDeleted { get; set; }
    }
}
