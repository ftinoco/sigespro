﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using Common.Log;

namespace Common.Helpers
{
    public class HttpClientApi
    {
        private string _urlApi;

        private MessageType _messageType;
        public MessageType MessageType
        {
            get { return _messageType; }
        }

        //private Exception _exception;
        //public Exception Exception
        //{
        //    get { return _exception; }
        //}

        //public HttpResponseMessage ResponseMessage { get; set; }

        public HttpClientApi(string urlApi)
        {
            _urlApi = urlApi;
        }

        /// <summary>
        /// Ejecuta petición GET al servicion web
        /// </summary>
        /// <typeparam name="T">Objeto que se va a retornar</typeparam>
        /// <param name="uriActionString">Url a la cual se va a hacer la petición</param>
        /// <returns>
        ///     Objeto del tipo T que se esta obteniendo. Si ocurre un error se dispara la excepción
        /// </returns>
        public T GetAsyncObject<T>(string uriActionString, string token = "")
        {
            T returnValue = default(T);
            _messageType = MessageType.INFO;

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_urlApi);

                    client.DefaultRequestHeaders.Accept.Clear();

                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    if (!string.IsNullOrEmpty(token))
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                    HttpResponseMessage response = client.GetAsync(uriActionString).Result;

                    string resultContent = response.Content.ReadAsStringAsync().Result;

//                    response.EnsureSuccessStatusCode();

                    returnValue = JsonConvert.DeserializeObject<T>(resultContent);
                }

                return returnValue;
            }
            catch (Exception e)
            {
                _messageType = MessageType.WARNING;
                //Logger.Log(MessageType.ERROR, e, "Yo xD", "HttpClientApi - GetAsyncObject", "");
                throw e;
            }
        }

        /// <summary>
        /// Ejecuta petición GET al servicion web
        /// </summary>
        /// <typeparam name="T">Objeto que se va a retornar</typeparam>
        /// <param name="uriActionString">Url a la cual se va a hacer la petición</param>
        /// <returns>
        ///     Objeto del tipo T que se esta obteniendo. Si ocurre un error se dispara la excepción
        /// </returns>
        public async Task<HttpResponseMessage> GetAsyncObject(string uriActionString, string token = "")
        {
            _messageType = MessageType.INFO;
            HttpResponseMessage response = new HttpResponseMessage();
            
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_urlApi);

                //client.DefaultRequestHeaders.Accept.Clear();

                //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                if (!string.IsNullOrEmpty(token))
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                response = await client.GetAsync(uriActionString);

                if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                    _messageType = MessageType.ERROR;
                else if (response.StatusCode == System.Net.HttpStatusCode.NotFound ||
                         response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    _messageType = MessageType.WARNING;
            }

            return response;
        }

        /// <summary>
        /// Ejecuta petición POST al servicio web
        /// </summary>
        /// <typeparam name="T">Objeto que se va a enviar a la petición vía POST</typeparam>
        /// <param name="uriActionString">Url a la cual se va a hacer la petición</param>
        /// <param name="objectToSave">Objeto a guardar</param>
        /// <returns>
        /// HttpResponseMessage =>  Sin el Código Http es diferente a BadRequest se retorna el objeto recién
        ///                         guardado en el content de response. De los contrario se retorna el mensaje
        ///                         de error
        /// </returns>
        public async Task<HttpResponseMessage> PostAsync<T>(string uriActionString, T objectToSave, string token = "")
        {
            _messageType = MessageType.SUCCESS;
            HttpResponseMessage response = new HttpResponseMessage();

            //try
            //{
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_urlApi);

                if (!string.IsNullOrEmpty(token))
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                response = await client.PostAsJsonAsync(uriActionString, objectToSave);

                if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                    _messageType = MessageType.ERROR;
                else if (response.StatusCode == System.Net.HttpStatusCode.NotFound ||
                         response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    _messageType = MessageType.WARNING;

                //response.EnsureSuccessStatusCode();
            }
            //}
            //catch (Exception ex)
            //{
            //    _exception = ex;
            //    _messageType = MessageType.ERROR;
            //    // Logger.Log(MessageType.ERROR, ex, "Yo xD", "HttpClientApi - PostAsync", "");
            //}

            return response;
        }

        /// <summary>
        /// Ejecuta petición PUT al sevicio web
        /// </summary>
        /// <typeparam name="T">Objeto que se va a enviar a la petición vía PUT</typeparam>
        /// <param name="uriActionString">Url a la cual se va a hacer la petición</param>
        /// <param name="objectToSave">Objeto a actualizar</param>
        /// <returns>
        /// HttpResponseMessage =>  Sin el Código Http es diferente a BadRequest se retorna el objeto recién
        ///                         actualizado en el content de response. De los contrario se retorna el mensaje
        ///                         de error
        /// </returns>
        public async Task<HttpResponseMessage> PutAsync<T>(string uriActionString, T objectToUpdate, string token = "")
        {
            _messageType = MessageType.SUCCESS;
            HttpResponseMessage response = new HttpResponseMessage();

            //try
            //{
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_urlApi);

                if (!string.IsNullOrEmpty(token))
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                response = await client.PutAsJsonAsync(uriActionString, objectToUpdate);

                if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                    _messageType = MessageType.ERROR;
                else if (response.StatusCode == System.Net.HttpStatusCode.NotFound ||
                         response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    _messageType = MessageType.WARNING;

                //response.EnsureSuccessStatusCode();
            }
            //}
            //catch (Exception ex)
            //{
            //    _exception = ex;

            //    // Logger.Log(MessageType.ERROR, ex, "Yo xD", "HttpClientApi - PostAsync", "");
            //}

            return response;
        }

        /// <summary>
        /// Ejecuta petición DELETE al servicio web
        /// </summary>
        /// <param name="uriActionString">Url a la cual se va a hacer la petición</param>
        /// <returns>
        /// HttpResponseMessage =>  Sin el Código Http es diferente a BadRequest se retorna el objeto recién
        ///                         actualizado en el content de response. De los contrario se retorna el mensaje
        ///                         de error
        /// </returns>
        public async Task<HttpResponseMessage> DeleteAsync(string uriActionString, string token = "")
        {
            _messageType = MessageType.SUCCESS;
            HttpResponseMessage response = new HttpResponseMessage();

            //try
            //{
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_urlApi);

                if (!string.IsNullOrEmpty(token))
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                response = await client.DeleteAsync(uriActionString);

                if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                    _messageType = MessageType.ERROR;
                else if (response.StatusCode == System.Net.HttpStatusCode.NotFound ||
                         response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    _messageType = MessageType.WARNING;

                //response.EnsureSuccessStatusCode();
            }
            //}
            //catch (Exception ex)
            //{
            //    _exception = ex;
            //    _messageType = MessageType.ERROR;
            //    // Logger.Log(MessageType.ERROR, ex, "Yo xD", "HttpClientApi - PostAsync", "");
            //}

            return response;
        }
    }
}
