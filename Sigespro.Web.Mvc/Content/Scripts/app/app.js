﻿'use strict';
var sigesproApp = (function () {

    var settings;
    var headerPanelContent;

    var statusCode = {
        INFO: "INFO",
        ERROR: "ERROR",
        SUCCESS: "SUCCESS",
        WARNING: "WARNING"
    };

    var modalSize = {
        SM: "modal-sm",
        MD: "modal-md",
        LG: "modal-lg"
    };

    var urlBase = "http://localhost/Sigespro/";

    var messageError = "Ocurrió un error en la operación. Consulte la consola para mas detalles";

    return {

        URL_BASE: urlBase,

        STATUS_CODE: statusCode,

        MESSAGE_ERROR: messageError,

        MODAL_SIZE: modalSize,

        init: function () {
            $.material.init();
            //console.log($("label[class*=required]"));
            $("label[class*=required]").each(function (index) {
                console.log(index + ": " + $(this).text());
         
                if (!($(this).children().length > 0)) {
                    $(this).append('&nbsp;<span>*</span>');
                }
            });
            
        },

        /**
         * Muestra u oculta el menú lateral 
         * 
         * @param {type} e - 
         *                  Handler del evento click
         */
        toggleMenu: function (e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");

            var mediaquery = window.matchMedia("(max-width: 768px)");

            // Se va quitar el scroll solo cuando el tamaño de la pantalla sea menor a 768
            if (mediaquery.matches)
                $("body").toggleClass("overflow-hidden");
        },

        /**
      * Muestra u oculta el menú lateral 
      * 
      * @param {type} e - 
      *                  Handler del evento click
      */
        toggleRightMenu: function (e) {
            e.preventDefault();
            $("#sidebar-right-wrapper").toggleClass("toggled");

            var mediaquery = window.matchMedia("(max-width: 768px)");

            // Se va quitar el scroll solo cuando el tamaño de la pantalla sea menor a 768
            if (mediaquery.matches)
                $("body").toggleClass("overflow-hidden");
        },

        /**
         * Muestra u oculta la ventana de carga
         */
        toggleLoadingModal: function () {
            var loadingModal = $("#loadingModal");

            if (loadingModal.hasClass("wrapper-show"))
                loadingModal.removeClass("wrapper-show");
            else
                loadingModal.addClass("wrapper-show");
        },

        /**
         * Ordena las filas de una tabla
         * 
         * @param {type} e - 
         *                  Handler del evento click
         * @param {type} form - 
         *                  Nombre del formulario en el que esta contenida la tabla
         */
        sortingTable: function (e, form) {
            e.preventDefault();

            var strForm = form === undefined || form === null ? "#frmList" : "#" + form;

            var sortfield = $(e.target).data("sortfield");

            if ($(strForm + " #SortField").val() === sortfield) {
                if ($(strForm + " #SortOrder").val() === "ASC") {
                    $(strForm + " #SortOrder").val("DESC");
                }
                else {
                    $(strForm + " #SortOrder").val("ASC");
                }
            }
            else {
                $(strForm + " #SortField").val(sortfield);
                $(strForm + " #SortOrder").val("ASC");
            }

            $(strForm).submit();
        },

        /**
         * Pagina listado de registros en una tabla
         * 
         * @param {type} evt -
         *                      Handler del evento click
         * @param {type} pageindex - 
         *                      Indice de la pagina de la cual se comenzará la paginación
         * @param {string} form - 
         *                      Nombre de formulario que contiene a la tabla
         */
        paging: function (evt, pageindex, form) {
            evt.preventDefault();

            var strForm = form === undefined || form === null ? "#frmList" : "#" + form;

            var parent = $(evt.target).parents(".disabled");

            $(strForm + " #IsPaging").val(true);

            if (parent.length === 0) {
                $(strForm + " #CurrentPageIndex").val(pageindex);

                $(strForm).submit();
            }
        },

        /**
         * Muestra el modal de mensaje 
         * 
         * @param {string} type - 
         *                      Tipo de mensaje (Constante STATUS_CODE)
         * @param {string} message - 
         *                      Mensaje a mostrar
         * @param {function} onCloseFn - {Opcional} 
         *                      Función a ejecutar cuando se cierra el modal
         * @param {type} argsOnCloseFn - {Opcional}
         *                      Parámetros de la función a ejecutar cuando se cierra el modal
         * @param {string}  size - define el tamaño del modal. Los valores permitidos son: modal-sm, modal-md, modal-lg
         * 
         */
        showModalMessage: function (type, message, size, onCloseFn, argsOnCloseFn, withoutHeader) {
            var title = "", classButton = "btn btn-raised ";

            switch (type) {
                case statusCode.ERROR:
                    classButton += "btn-danger";
                    title = "Mensaje de Error";
                    break;
                case statusCode.INFO:
                    classButton += "btn-primary";
                    title = "Mensaje de Informaci&oacute;n";
                    break;
                case statusCode.SUCCESS:
                    classButton += "btn-success";
                    title = "Mensaje de Informaci&oacute;n";
                    break;
                case statusCode.WARNING:
                    classButton += "btn-warning";
                    title = "Mensaje de Advertencia";
                    break;
                default:
                    classButton += "btn-danger";
                    title = "Mensaje de Error";
                    break;
            }

            if (withoutHeader && withoutHeader === true) 
                $("#modalMessage .modal-header").css("display", "none");
            else {
                $("#modalMessageTitle").html(title);
                $("#modalMessage .modal-header").css("display", "");
            }
            

            if (size !== undefined && size !== null)
                $('#modalMessage > div').attr("class", "modal-dialog " + size);
            else
                $('#modalMessage > div').attr("class", "modal-dialog " + modalSize.SM);

            $("#btnModalMessageClose").attr("class", classButton);

            $("#modalMessage .modal-body").html(message);
            $("#modalMessage").modal('show');

            if (onCloseFn && (typeof onCloseFn === "function")) {
                $('#modalMessage').on('hidden.bs.modal', function (e) {
                    onCloseFn.apply(this, argsOnCloseFn);
                });
            }
        },

        /**
         * Muestra un modal para confirmación de operaciones
         * 
         * @param {type} title - Título del modal
         * 
         * @param {type} message - Mensaje a mostrar
         * 
         * @param {type} acceptFn - Función a ejecutar si el usuario acepta la confirmación
         * 
         * @param {type} argsAcceptFn - Parámetros de la función de accept
         * 
         * @param {type} cancelFn - Función a ejecutar si el usuario cancela la confirmación
         *  
         * @param {type} argsCancelFn - Parámetros de la función de cancel
         * 
         * @param {string}  size - define el tamaño del modal. Los valores permitidos son: modal-sm, modal-md, modal-lg
         * 
         */
        showModalDialog: function (title, message, acceptFn, argsAcceptFn, cancelFn, argsCancelFn, size) {

            var confirm = $("#modalConfirm");

            confirm.modal({
                backdrop: 'static',
                keyboard: false
            });

            if (title !== "")
                $("#lblTitleConfirm").html(title);

            $("#lblMessageConfirm").html(message);

            if (size !== undefined && size !== null)
                $("#modalConfirm > div").attr("class", "modal-dialog " + size);
            else
                $("#modalConfirm > div").attr("class", "modal-dialog " + modalSize.SM);

            $("#btnAcceptConfirm").off('click').click(function (e) {
                e.preventDefault();

                if (acceptFn && (typeof acceptFn === "function"))
                    acceptFn.apply(this, argsAcceptFn);

                if ($(e.target).data("close") === true)
                    confirm.modal("hide");
            });

            $("#btnCancelConfirm").off('click').click(function (e) {
                e.preventDefault();

                if (cancelFn && (typeof cancelFn === "function"))
                    cancelFn.apply(this, argsCancelFn);

                confirm.modal("hide");
            });
        },

        /**
         * Muestra una alerta Bootstrap
         * 
         * @param {type} container - Id del contenedor del alert (identificador si signo numeral)
         * 
         * @param {type} type - Tipo de mensaje: ERROR, INFO, SUCCESS, WARNING (Constante STATUS_CODE)
         * 
         * @param {type} message - Mensaje a mostrar
         * 
         * @param {type} append - Contenido HTML o texto plano extra
         * 
         */
        showAlert: function (container, type, message, append) {
            var cssType = "";

            switch (type) {
                case statusCode.ERROR:
                    cssType = "alert-danger";
                    break;
                case statusCode.INFO:
                    cssType = "alert-info";
                    break;
                case statusCode.SUCCESS:
                    cssType = "alert-success";
                    break;
                case statusCode.WARNING:
                    cssType = "alert-warning";
                    break;
                default:
                    cssType = "alert-info";
                    break;
            }

            if (append === undefined)
                append = '';

            $('#' + container).html('<div class="alert ' + cssType + '" role="alert"> \
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> \
                    <p id="error-alert">' + message + '</p>' + append + '</div>');
        },

        /**
         * Muestra un modal con el mensaje de exito o error en dependecia de una respuesta ajax
         * 
         * @param {type} response - 
         *                          Respuesta ajax
         */
        postIsOk: function (response) {
            if (response.status === sigesproApp.STATUS_CODE.SUCCESS) {
                sigesproApp.showModalMessage(response.status, response.message, null, sigesproApp.redirectTo, [response.action]);
                $("#btnModalMessageClose").focus();
                $("input[type*=submit],button[type*=submit]").prop("disabled", true);
            }
            else
                sigesproApp.showModalMessage(response.status, response.message);
        },

        /**
         * Registra en la consola la respuesta de error de una petición ajax y muestra un mensaje generico
         * 
         * @param {type} response - 
         *                          Respuesta ajax
         */
        postError: function (response) {
            console.log(response.responseText);
            sigesproApp.showModalMessage(sigesproApp.STATUS_CODE.ERROR, sigesproApp.MESSAGE_ERROR);
        },

        /**
         * Redirecciona del lado del cliente a una acción especifica
         * 
         * @param {type} action - 
         *                      Acción donde se redireccionará
         */
        redirectTo: function (action) {
            window.location.href = action;
        },

        /**
         * Recarga la página actual desde el cliente
         */
        reload: function () { 
            window.location.reload(true);
        },

        formater: function () {
            //window.onload = function () {
                $("input").each(function (i, input) {
                    var $this = $(input);

                    if ($this.data("decimal") === true) {
                        $this.inputmask({
                            'alias': 'decimal',
                            'digits': 2,
                            'digitsOptional': false,
                            'placeholder': '0.00',
                            rightAlign: true,
                            'repeat': 14,
                            'groupSeparator': ',',
                            'autoGroup': true,
                            clearMaskOnLostFocus: !1
                        });
                    }
                    else if ($this.data("integer") === true) {
                        $(this).inputmask({
                            'alias': 'numeric',
                            'min': 1,
                            'max': 2147483647,
                            'allowMinus': false,
                            'repeat': 10,
                            rightAlign: true,
                            clearMaskOnLostFocus: !1
                        });
                    }
                    else if ($this.data("datetime") === true) {
                        $(this).inputmask({
                            mask: "1/2/9999 h:s:s t\\m",
                            placeholder: "__/__/____ __:__:__ _",
                            alias: "datetime",
                            hourFormat: "12",
                            rightAlign: true
                        });
                    }
                    else if ($this.data("date") === true) {
                        $(this).inputmask({
                            mask: "1/2/9999",
                            placeholder: "__/__/____",
                            alias: "date",
                            rightAlign: true
                        });
                    }
                    else if ($this.data("time") === true) {
                        $(this).inputmask({
                            regex: "^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$",
                            placeholder: "__:__:__ _",
                            alias: "Regex",
                            rightAlign: true
                        });
                    }
                });
            //}
        },

        /**
         * Muestra un modal para confirmar la eliminación de registros
         * 
         * @param {type} e - Hndler del evento
         * 
         * @param {type} action - Accion/url donde se procesará la eliminación
         * 
         */
        confirmDelete: function (e, action) {
            e.preventDefault();

            sigesproApp.showModalDialog("Mensaje de Confirmación", "¿Desea eliminar el registro pemanentemente? Si lo hace no podrá recuperar la información.", sigesproApp.acceptDelete, [action]);

            $("#btnAcceptConfirm").html('Aceptar');
            $("#btnAcceptConfirm").data('close', true);

            $("#btnCancelConfirm").html('Cerrar');
        },

        acceptDelete: function(action){
            $.ajax({
                type: "POST",
                url: action,
                beforeSend: function () {
                    sigesproApp.toggleLoadingModal();
                },
                success: function (response) {
                    console.log(response);
                    if (response.status !== undefined) {
                        if (response.status !== sigesproApp.STATUS_CODE.ERROR) {
                            if (response.action !== undefined && response.action !== null)
                                window.location.replace(response.action);
                            else
                                window.location.reload(true);
                        }
                        else
                            sigesproApp.showModalMessage(response.status, response.message, sigesproApp.MODAL_SIZE.MD);
                    }
                },
                error: function (data) {
                    console.log(data.responseText);
                    sigesproApp.showModalMessage(sigesproApp.STATUS_CODE.ERROR, sigesproApp.MESSAGE_ERROR, sigesproApp.MODAL_SIZE.MD);
                },
                statusCode: {
                    401: function () {
                        window.location.replace(loginUrl);
                    }
                }
            }).always(function () {
                sigesproApp.toggleLoadingModal();
            });
        },
        
        backup: function (e) {
            e.preventDefault();

            $.ajax({
                url: $(e.target).data("action"),
                type: "POST",
                beforeSend: function () {
                    sigesproApp.toggleLoadingModal();
                },
                success: function (response) {
                    console.log(response);

                    if (response.status === sigesproApp.STATUS_CODE.SUCCESS) {
                        sigesproApp.showModalMessage(response.status, response.message, sigesproApp.MODAL_SIZE.MD);
                    }
                    else {
                        sigesproApp.showModalMessage(response.status, response.message);
                        $("#btnModalMessageClose").focus();
                    }
                },
                error: function (data) {
                    console.log(data.responseText);
                    sigesproApp.showModalMessage(sigesproApp.STATUS_CODE.ERROR, sigesproApp.MESSAGE_ERROR, sigesproApp.MODAL_SIZE.MD);
                },
                statusCode: {
                    401: function () {
                        window.location.replace(sigesproApp.URL_BASE + "Account/LogOut");
                    }
                }
            }).always(function () {
                sigesproApp.toggleLoadingModal();
            });
        },

        restore: function (e, files) {
            e.preventDefault();
             
            var action = $(e.target).data("action");

            var frmData = new FormData();

            if (files) {
                frmData.append("file", files[0]);

                $.ajax({
                    type: "POST",
                    url: action,
                    data: frmData,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        sigesproApp.toggleLoadingModal();
                    },
                    success: function (response) {
                        console.log(response);

                        if (response.status === sigesproApp.STATUS_CODE.SUCCESS) {
                            sigesproApp.showModalMessage(response.status, response.message, sigesproApp.MODAL_SIZE.MD);
                        }
                        else {
                            sigesproApp.showModalMessage(response.status, response.message);
                            $("#btnModalMessageClose").focus();
                        }
                    },
                    error: function (data) {
                        console.log(data.responseText);
                        sigesproApp.showModalMessage(sigesproApp.STATUS_CODE.ERROR, sigesproApp.MESSAGE_ERROR, sigesproApp.MODAL_SIZE.MD);
                    },
                    statusCode: {
                        401: function () {
                            window.location.replace(urlBase + "Account/LogOut");
                        }
                    }
                }).always(function () {
                    sigesproApp.toggleLoadingModal();
                });
            }
        },


        /*OPCIONES DE BUSQUEDA*/

        filterList: function () {
            var strFilterHTML = '<div class="form-group"> \
                    <div class="input-group"><span class="input-group-addon"><i class="material-icons" style="pointer-events: none;">search</i></span>  \
                    <input id="addonSearch" placeholder="Buscar..." class="form-control" type="text" onblur="sigesproApp.search(event);"> \
                    <span class="input-group-btn"><button type="button" class="btn" onclick="sigesproApp.clearSearch()"> \
                    <i class="material-icons" style="pointer-events: none;">clear</i></button></span></div></div>';

            headerPanelContent = $("#headerPanel").html();

            $("#headerPanel").html(strFilterHTML);
        },

        search: function (e) {
            var form = $("form");

            $("#hdfSearch").val($(e.target).val());

            form.submit();
        },

        clearSearch: function () {
            var form = $("form");
            $("#hdfSearch").val("");
            $("#headerPanel").html(headerPanelContent);
            form.submit();
        },

        showPartial: function (e) {
            sigesproApp.toggleLoadingModal();
            $.post($(e.target).data("action"), function (response) {
                $("#main-panel-body").html(response);
                sigesproApp.toggleLoadingModal();
            });
        }
    };
}());

sigesproApp.init();